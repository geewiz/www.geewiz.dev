---
layout: post
title: "Mega-Spielautomat"
microblog: false
guid: http://geewiz.micro.blog/2005/12/05/megaspielautomat.html
date: 2005-12-05T21:11:25+0200
type: post
url: /2005/12/05/megaspielautomat.html
---

<p>Letzte Woche hab ich mal mit ein paar Kollegen <a href="http://www.retrogames.info">Retrogames</a> besucht. Dort finden klassische Spielautomaten (Donkey Kong, Galaxians, Street Fighter, Outrun) und Flipper (Addams Family, Star Trek TNG) einen w&uuml;rdigen Altersruhesitz. Gegen einen moderaten Eintrittspreis erh&auml;lt man kostenlosen Zugang zu allen Maschinen. Mit Andy hab ich an dem Abend Bubble Bobble bis Level 74 durchgezockt.</p>
<p>Mit dem <a href="http://www.techeblog.com/index.php/tech-gadget/2006-ultimate-arcade-machine">2006 Ultimate Arcade</a> w&uuml;rde der Retrogames e.V. deutlich weniger Stellfl&auml;che ben&ouml;tigen: das Ger&auml;t vereinigt in sich satte 81 Spieleklassiker! Daf&uuml;r w&uuml;rde sogar ich noch Platz finden...</p>
