---
layout: post
title: "Tests are hygiene"
microblog: false
guid: http://geewiz.micro.blog/2019/12/04/tests-are-hygiene.html
date: 2019-12-04T17:43:22+0200
type: post
categories:
- "DevOps"
tags: [devops]
url: /2019/12/04/tests-are-hygiene.html
---
Because I started programming a long, long time ago, I have to admit that I spent most of my career without writing tests to accompany my code. It was only in recent years that I learned how to use a test suite to my advantage. In hindsight, I should have started earlier because having tests completely changed my life as a software developer.

Tests create peace of mind. The amount of confidence with which I now release my code to production is worlds apart from the trepidation of past deployments when I was on edge for hours waiting for the other shoe to drop. Beginning of this year, I wrote about this in my article ["Test-Driven"](/2019/01/15/testdriven.html).

I remember how difficult it was for me to get into writing tests. Honestly, once in a while I still struggle with building clean and efficient tests. That’s why I can relate to developers who are turned off writing tests by the additional effort it takes.

Decades of me coding without a test suite prove that you can not have tests and still do a decent job. However, that doesn’t mean that it’s a good idea. Or that it’s good practice in 2019. I’d even go so far as to state that building tests for your code has become basic professional behaviour.

Martin Thompson draws an interesting parallel to the medical profession on the episode "Protocols and Sympathy" of the [Arrested DevOps podcast](https://overcast.fm/+BvUWI7mbo/31:15) (link jumps right to the related point in the conversation):

> "A surgeon will not consider performing an operation without washing their hands."

But did you know that it wasn’t until the efforts of people like [Ignaz Semmelweis](https://en.wikipedia.org/wiki/Ignaz_Semmelweis) in the 1800s that antiseptic procedures were even considered par for the course by doctors? Setting higher standards for hygiene transformed medical practice.

I’ve come to the realisation that spending time on writing tests before coding business logic is like washing your hands ahead of performing surgery. In the long run, it saves time and money because it make sure that “done” means “done”. Better hygiene in medicine reduced the number of deaths and minimised the need for antibiotics and urgent follow-up operations. A robust test suite will reduce the number of outages and minimise the need for workarounds and urgent debugging sessions.

In both cases, there’s going to be far less suffering for everyone involved.
