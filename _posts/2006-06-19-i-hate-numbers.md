---
layout: post
title: "I hate numbers"
microblog: false
guid: http://geewiz.micro.blog/2006/06/19/i-hate-numbers.html
date: 2006-06-19T18:34:55+0200
type: post
url: /2006/06/19/i-hate-numbers.html
---

<p>After weeks of procrastination, I decided yesterday afternoon not to stop at thinking about doing my taxes, but to actually do them. I really loathe that part of my work, probably due to the traumatic effects of some not-so-good years gone by. But after the first heap of paper shifted, my motivation increased by an order of magnitude when I saw what QuickSteuer displayed as estimated tax refund.</p>
<p>So, at about midnight, the cover letter was the last piece of paper running out of my printer, and this morning, a weary-eyed Jochen dropped a big envelope into the mailbox on the way to the railway station. Talk about getting things done...</p>
<p>As always, I'm not sure if the Finanzamt (the german IRS) will just do a tax calculation based on my numbers or write a response like "Hey, we had a great laugh. Oh, and by the way, maybe you want to take a drug test." We'll see.</p>
