---
layout: post
title: "Nicht M&ouml;nche, sondern T&ouml;pfer"
microblog: false
guid: http://geewiz.micro.blog/2005/11/07/nicht-moumlnche-sondern.html
date: 2005-11-07T06:26:15+0200
type: post
url: /2005/11/07/nicht-moumlnche-sondern.html
---

<p>Anfang der Neunziger schlug das Buch <a href="http://www.spiegel.de/spiegel/0">Verschlusssache Jesus" gro&szlig;e Wellen. Seine Autoren stellten darin die These auf, dass die Schriften, die Mitte des 20. Jahrhundertes bei Qumran gefunden worden waren, Z&uuml;ndstoff f&uuml;r die christlichen Kirchen enthielten und deshalb einige Schriften vom Vatikan unter Verschluss gehalten w&uuml;rden. Das wurde jedoch schnell widerlegt. Nach einigen Jahren der Ruhe krempeln jetzt aber "neue wissenschaftliche Erkenntnisse</a>,1518,383477-2,00.html die bisherigen Analysen wieder komplett um.Der ganze damalige Rummel um die angebliche "Verschlusssache" interessierte mich und ich legte mir einiges an Literatur zu dem Thema zu.</p>
<p>Die Verschw&ouml;rungstheorie wurde schnell von kompetenten Wissenschafltern widerlegt. Gro&szlig;e Bedeutung schrieben aber auch diese dem Fundort zu, wenn auch in anderem Sinne: er habe ein Kloster der Essener, einer j&uuml;dischen Sekte, beherbergt. Diese (durchweg m&auml;nnlichen) Essenerm&ouml;nche h&auml;tten nach strengen Regeln gelebt und im Akkord Schriften verfasst, die wichtige Einblicke in die Sekte gew&auml;hrten.</p>
<p>Nun wirft das neu erscheinende Buch "Qumran -  die ganze Wahrheit" von Yizhar Hirschfeld, einem Forscher der Universit&auml;t Jerusalem, ein g&auml;nzlich anderes Licht auf das vermeintliche geistlichee Zentrum: demzufolge sei Qumran St&auml;tte eines Gutshofes gewesen, nicht eines Klosters. Auch die anderen bisher aufgestellten Vermutungen gingen v&ouml;llig in die falsche Richtung, zum Beispiel h&auml;tten die vielen gefundenen Becken nicht Taufriten gedient, sondern T&ouml;pfern, die darin ihre Werke s&auml;uberten. Die Schriften seien deshalb nach Qumran gelangt, weil sie dort vor den r&ouml;mischen Besatzern versteckt werden sollten. Sie enthielten auch Lyrik und nichtreligi&ouml;se Abhandlungen, was der Annahme widerspricht, dass es sich &uuml;berhaupt um die Bibliothek einer j&uuml;dischen Sekte handele.</p>
<p>Generell widersprechen diese neuen Erkenntnisse enorm der bisherigen Interpretation der Funde vom Toten Meer. Ich finde es interessant, wie sich ganze Teams von Wissenschaftlern irren k&ouml;nnen. Aber ich f&uuml;rchte, ich werde nicht mehr die Zeit haben wie vor etwa 10 Jahren, mich weiter in das Thema einzulesen.</p>
