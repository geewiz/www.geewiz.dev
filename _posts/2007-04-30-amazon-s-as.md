---
layout: post
title: "Amazon S3 as storage backend for MySQL"
microblog: false
guid: http://geewiz.micro.blog/2007/04/30/amazon-s-as.html
date: 2007-04-30T18:11:00+0200
type: post
url: /2007/04/30/amazon-s-as.html
---

<p>I found on the agenda of the MySQL Conference &amp; Expo 2007 that Mark Atwood presented in his talk <a href="http://www.mysqlconf.com/cs/mysqluc2007/view/e_sess/10822">A Storage Engine for Amazon S3</a>,how to use the <a href="http://www.amazon.com/gp/browse.html?node=16427261">Simple Storage Service</a> to store the data of a MySQL database.</p>
<p>That MySQL allows to exchange the storage engine with plugins is a well-known fact. That's how the transition from MyISAM to the transaction-capable InnoDB storage engine was made. But to move the storage completely on the internet by using S3 is an unusual, yet interesting idea.</p>
<p>Amazon offers S3 as</p>
<blockquote class="posterous_short_quote">
<p>a simple web services interface that can be used to store and retrieve any amount of data, at any time, from anywhere on the web.</p>
</blockquote>
<p>S3 users don't have to deal with RAIDs, storage volumes, scaling capacity, doing backup or any other technical details. It's all taken care of at the Amazon data center.</p>
<p>I'd be interested how well this internet storage system can be used as the storage backend for MySQL -- what about latency, bandwidth and other issues? For example, what happens if I do a full table scan over a big table that's stored on S3?</p>
<p>Maybe there's some MySQL expert out there that can shed a bit more light on that topic?</p>
