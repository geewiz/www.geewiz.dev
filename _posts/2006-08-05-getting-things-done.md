---
layout: post
title: "Getting Things Done on the Web"
microblog: false
guid: http://geewiz.micro.blog/2006/08/05/getting-things-done.html
date: 2006-08-06T00:35:06+0200
type: post
url: /2006/08/05/getting-things-done.html
---

<p>In <a href="http://blog.jochen-lillich.de/archives/454-Working-on-tasks-in-the-right-context.html">Working on tasks in the right context</a>, I already mentioned ways of managing to-do-lists online. I'm using the more universal <a href="http://backpackit.com/?referrer=BPBMZZ9">Backpack</a>, but there also are web services that are especially geared towards the "Getting Things Done" method of task management.</p>
<p><a href="http://www.zenlist.com/">Zenlist</a> is based on the free GTD software <a href="http://www.rousette.org.uk/projects/.">Tracks</a> Usage of Zenlist is also free and the registration process is painless. It looks quite handy to me and I'll give it a test ride.</p>
<p>Another free service is <a href="http://icommit.eu">iCommit</a> which also uses the GTD nomenclature of tasks, contexts, projects and so on. To me it doesn't look quite as smooth as Zenlist. That may be one of the reasons that iCommit is getting a complete overhaul at the moment. But it undoubtedly does the job, too.</p>
<p>I'm looking forward to more productivity tools on the web. The beginnings certainly are promising.</p>
