---
layout: post
title: "A picture does say more"
microblog: false
guid: http://geewiz.micro.blog/2006/10/04/a-picture-does.html
date: 2006-10-04T23:09:41+0200
type: post
url: /2006/10/04/a-picture-does.html
---

<p>"Explaining" something doesn't have to mean filling sheets of paper or presentation slides with text. Setting words aside helps preventing language problems. It spares the author from making necessary translations (and the subsequent corrections) and the reader the effort of comprehending a written description.</p>
<p>The <a href="http://www.flickr.com/photos/laurenbugeja/242224749/in/pool-69453349">wordless pancake recipe</a>@N00 is a good example. I'll give it a try as soon as our new kitchen is ready.</p>
<p>(via <a href="http://37signals.com/svn/archives2/wordless_pancake_recipe.php">Signal vs. Noise</a>)</p>
