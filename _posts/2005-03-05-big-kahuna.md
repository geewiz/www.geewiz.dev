---
layout: post
title: "Big Kahuna"
microblog: false
guid: http://geewiz.micro.blog/2005/03/05/big-kahuna.html
date: 2005-03-05T02:41:41+0200
type: post
url: /2005/03/05/big-kahuna.html
---

<p>Man muss sich gar nicht unbedingt selbst&auml;ndig machen, um sein eigener Chef zu sein. Es reicht schon, die Urlaubsvertretung des IT-Leiters zu &uuml;bernehmen. Das tat ich heute zum ersten Mal.</p>
<p>Dass ich Herausforderungen liebe, ist eines. Aber wer rechnet schon damit, dass er den ersten Krisenanruf noch auf dem gestrigen Heimweg bekommt? Okay, letzten Endes haben wir innerhalb von 2 Stunden das Kind in einer sauberen Teamleistung geschaukelt. Heute war wieder alles im gr&uuml;nen Bereich, am Ende schulden nur ein paar Leute ihren Kollegen eine Ladung Schokobons respektive einen Nuck Gr&ouml;&szlig;e S. :)</p>
<p>Falls das Schicksal mitliest: Danke, reicht wieder mit Herausforderungen, jetzt ist Wochenende.</p>
