---
layout: post
title: "Danke f&uuml;rs Danken!"
microblog: false
guid: http://geewiz.micro.blog/2005/04/18/danke-fuumlrs-danken.html
date: 2005-04-18T20:54:09+0200
type: post
url: /2005/04/18/danke-fuumlrs-danken.html
---

<p>In den letzten Wochen bekomme ich verst&auml;rkt Lob von Kollegen wie auch Fremden f&uuml;r die Artikel auf meiner Website. Die einen freuen sich &uuml;ber die Bluetooth-Infos, andere &uuml;ber das WebGUI-Handbuch, den Regex-Artikel oder was sonst noch von mir online ist.</p>
<p>Ich will ja anderen einen Gefallen tun, indem ich meine Erkenntnisse &ouml;ffentlich machen. Deshalb freut es mich nat&uuml;rlich sehr, wenn sich meine M&uuml;he lohnt.</p>
<p>Umso mehr schade, dass ich schon so lange keine Zeit mehr finde, neue Artikel zu schreiben... :-(</p>
