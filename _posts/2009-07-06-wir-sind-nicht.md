---
layout: post
title: "\"Wir sind nicht die Partei der IT-Nerds\""
microblog: false
guid: http://geewiz.micro.blog/2009/07/06/wir-sind-nicht.html
date: 2009-07-06T11:45:59+0200
type: post
url: /2009/07/06/wir-sind-nicht.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_short_quote"><p>Die Piratenpartei gilt als spektakul&auml;rste Parteigr&uuml;ndung seit den Gr&uuml;nen und schwimmt auf der Welle des Erfolges. Doch anders als ihre &ouml;kologischen Vorg&auml;nger ist die Partei schnell im Polit-Alltag gelandet - und k&auml;mpft dort f&uuml;r digitale B&uuml;rgerrechte.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.heute.de/ZDFheute/inhalt/18/0,3672,7601746,00.html">heute.de</a></div>
<p>heute.de r&uuml;ckt die Kernzeile der Piratenpartei ins rechte Licht.</p>
</div>
