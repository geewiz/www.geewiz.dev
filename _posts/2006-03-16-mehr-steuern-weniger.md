---
layout: post
title: "Mehr Steuern, weniger Rente"
microblog: false
guid: http://geewiz.micro.blog/2006/03/16/mehr-steuern-weniger.html
date: 2006-03-17T01:31:21+0200
type: post
url: /2006/03/16/mehr-steuern-weniger.html
---

<p>Der Direktor des Instituts f&uuml;r Wirtschaftspolitik der Universit&auml;t K&ouml;ln hat herausgefunden, wer an der Renten-Misere schuld ist. Die BILD-Zeitung zitiert Johann Eekhoff mit den Worten: "Die Renten von Kinderlosen m&uuml;ssten um die H&auml;lfte gesenkt werden".</p>
<p>Es beruhigt mich, dass ich nicht der einzige bin, der diese Idee f&uuml;r wahnsinnig -clever- h&auml;lt.<br />
Mal nachdenken: ich geh&ouml;re zur Bildungselite, bezahle Steuern zum Spitzensatz und dementsprechend hohe Beitr&auml;ge zur Rentenversicherung. Dass ich bisher keine eigene Familie hatte, hat das durchaus beg&uuml;nstigt. Nat&uuml;rlich komme ich (auch aufgrund der Studienzeit) nicht umhin, zus&auml;tzlich eine private Altersvorsorge zu betreiben, da mein Rentenanspruch schon heute v&ouml;llig unzureichend ist.</p>
<p>Umgekehrt sind es ja naturgem&auml;&szlig; vor allem die Akademiker, die erst sp&auml;t eine Familie gr&uuml;nden. Wenn &uuml;berhaupt, denn besonders mit hochqualifizierten Berufen lassen sich Kinder nur dann vereinbaren, wenn sich Wege finden, die fehlenden Kindergartenpl&auml;tze zu kompensieren. Die Variante, dass die Mutter dazu ihren Beruf aufgibt, geh&ouml;rt hier aber halt nicht mehr zu den bevorzugten Modellen. Und dass auch die Gro&szlig;eltern mit umziehen m&uuml;ssen, um die Kinderbetreuung zu gew&auml;hrleisten, ist eine eher praxisferne Interpretation von "mobile Gesellschaft".</p>
<p>Die weibliche Sicht kann ich selbst nicht einbringen, aber ich kann dazu auf den Artikel <a href="http://www.zeit.de/2006/12/Titel_2fMtter_12?page=1">Der Preis des Gl&uuml;cks</a> in der "Zeit" verweisen.</p>
<p>Anstatt nun aber den st&auml;rksten Beitragszahlern geeignete Unterst&uuml;tzung und Anreize zur Familiengr&uuml;ndung zu schaffen, kommt man auf keine bessere Idee, als ihnen die Rente zu halbieren.</p>
<p>Es mag ja B&uuml;rger geben, die daraus die gew&uuml;nschten Konsequenzen ziehen werden (vorbereitete <a href="http://sushee.geekheim.de/?p=909">Kontaktanzeige</a> bei Su-Shee), aber mir pers&ouml;nlich dr&auml;ngt sich eher eine <a href="http://blog.koehntopp.de/archives/1192-Forscher-Forderung-Beisst-die-Hand">Reaktion wie die Kristians</a>,-die-Euch-fuettert!.html auf.</p>
<p>Wenn ich ohnehin komplett f&uuml;r mich selbst (und meine zuk&uuml;nftige Familie) sorgen muss, weil mein Land mich dabei nicht unterst&uuml;tzt, dann sehe ich auch keinen Grund, mein Land zu unterst&uuml;tzen.</p>
<p>Falls mich jemand sucht: ich bin in Irland.</p>
