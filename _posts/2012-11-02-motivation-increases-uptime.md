---
layout: post
title: "Motivation increases uptime"
microblog: false
guid: http://geewiz.micro.blog/2012/11/02/motivation-increases-uptime.html
date: 2012-11-02T14:58:56+0200
type: post
url: /2012/11/02/motivation-increases-uptime.html
---
<p>We IT operations people love our disaster porn and exchanging war stories is always a great reason to have some drinks together. Recent hurricane Sandy certainly let the book of Ops Tales grow quite a bit. This morning, I came upon the story of how the folks at Squarespace, Fog Creek and Peer1 <a href="http://techcrunch.com/2012/11/01/squarespace-fog-creek-peer1-kept-ny-data-center-alive-by-carrying-fuel-buckets-to-the-17th-floor-in-the-dark/">carried generator fuel up 17 stories</a> to keep things running. I think that's awesome from a lot of perspectives:</p>
<ul>
<li>Customer care: They could have just said "Don't blame us, blame the elements.", publish a status page and be done with it until the water was gone. Instead, they did everything they could come up with to keep their services running.</li>
<li>Team spirit: Their people could just have said "There's nothing in my contract about hauling buckets of fuel around in the dark". Instead, they pulled up their sleeves and went at it.</li>
<li>Leadership: Getting people to volunteer for this work is already a great leadership achievement. Keeping this up without people dropping out left and right even more.</li>
<li>Communication: They kept customers in the loop, didn't sugarcoat impending outages and finally delivered much more than they promised. That's perfect PR.</li>
</ul>
<p>Guys, I bow before you in respect.</p>
