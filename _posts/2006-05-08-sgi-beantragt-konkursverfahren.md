---
layout: post
title: "SGI beantragt Konkursverfahren"
microblog: false
guid: http://geewiz.micro.blog/2006/05/08/sgi-beantragt-konkursverfahren.html
date: 2006-05-08T23:54:41+0200
type: post
url: /2006/05/08/sgi-beantragt-konkursverfahren.html
---

<p>Nachdem SGI (fr&uuml;her <a href="http://blog.jochen-lillich.de/archives/267-SGI-steht-vor-dem-Aus.html">Silicon Graphics") im Februar bereits "Liquidit&auml;tsprobleme angek&uuml;ndigt</a> hatte, ist es heute soweit: das Unternehmen hat vor Gericht das <a href="http://www.channelregister.co.uk/2006/05/08/silicon_graphics_chapter_11/.">Konkursverfahren beantragt</a></p>
<p>Als Gr&uuml;nde nennt SGI Verz&ouml;gerungen bei der Einf&uuml;hrung neuer Technologien, die Konzentration auf spezialisierte M&auml;rkte und den wachsenden Wettbewerb.</p>
<p>Damit geht jetzt wohl die &Auml;ra gut aussehender Hochleistungsrechner aus dem Hause SGI zu Ende. :-(</p>
