---
layout: post
title: "I really don't want to be a Telekom customer"
microblog: false
guid: http://geewiz.micro.blog/2006/10/11/i-really-dont.html
date: 2006-10-11T22:20:00+0200
type: post
url: /2006/10/11/i-really-dont.html
---

<p>But I don't have much choice. Because I'd like to get the new <a href="http://www.1und1.de">1&amp;1 3DSL</a> (broadband including a flat fee for data volume and VOIP-to-landline time as well as Maxdome movie downloads), I had to get at least a basic analog landline connection from German Telekom for my new flat.</p>
<p>Of course, you can't order a landline connection online (what a strange thought!). So I went to the "T-Punkt" shop last Monday. I had to wait in line for 25 minutes, thus having the opportunity to watch the "service promise" promo on a big screen for five times that assured me that I won't have to wait for more than 5 minutes.</p>
<p>After a whole week now, I don't even have an order confirmation. (Oh, I almost forgot to mention that delivery date is Wednesday.) I still don't know which phone number I'll get -- an information necessary to order my DSL connection. Seems like I won't be online immediately after moving over the coming weekend.</p>
<p>It really makes me angry that my money might help Telekom in keeping up their crappy service. High time their almost complete monopoly on the german phone and broadband infrastructure comes to an end.</p>
<p>(Originally from 2006-10-02 19:35)</p>
<p><em>Update 1 (2006-10-05):</em> Of course, my order is FUBAR. This is gonna be a looong story... It's been almost two weeks and not until now they told me that they couldn't locate the right line to our flat. I gave them the apartment number and was promised that I'll get called back ASAP. </p>
<p><em>Update 2 (2006-10-06):</em> Of course, they didn't call back. So, when I called the "customer service" (ha!) line for the fifth time, I asked what god I had to sacrificy a chicken to if I wanted to get a telephone line from Telekom (well, that was actually not my literal question, but something in that sense). And -- tadaa -- after a few hours, a technician actually called me telling me that he'll activate the line immediately. Should that be the happy end? Tune in next time to find out!</p>
<p><em>Update 3 (2006-10-11):</em> As expected, it wasn't the happy ending. When we plugged in our phone, all we could hear was crackling and humming. So I called the service hotline on Sunday afternoon. A very nice agent did a connection analysis and concluded that technical service would be necessary. We arranged that the technician should come by between 8 and 12 the next day and call us 30 minutes in advance. Which he did, no complaints here. Finally, after he rewired the phone line to our appartment, we had a working connection. So, in the end, it took Telekom 2 weeks and me 7 calls to different call centers just to get a normal analogue phone line. What's left is the impression that Deutsche Telekom is much better in fixing technical difficulties than in getting new and satisfied customers.</p>
