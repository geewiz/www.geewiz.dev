---
layout: post
title: "Linux tip: Coloring terminal output"
microblog: false
guid: http://geewiz.micro.blog/2010/11/24/linux-tip-coloring.html
date: 2010-11-25T00:05:00+0200
type: post
url: /2010/11/24/linux-tip-coloring.html
---

<p>Coloring output by using escape sequences is a technique I've known before I started using Linux. That's about two decades now.</p>
<p>But, as I learned today, the tput command provides a much more versatile approach:</p>
<div class="CodeRay">
<div class="code">
<pre>bold=$(tput bold)
normal=$(tput sgr0)
echo This is ${bold}bold${normal} and this is not.</pre>
</div>
</div>
<p>tput uses information about the actual terminal to generate the right escape sequences, so it's much better to use tput than to hardcode those.</p>
