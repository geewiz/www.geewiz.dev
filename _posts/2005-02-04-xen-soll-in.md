---
layout: post
title: "Xen soll in Linux &uuml;bernommen werden"
microblog: false
guid: http://geewiz.micro.blog/2005/02/04/xen-soll-in.html
date: 2005-02-05T01:12:00+0200
type: post
url: /2005/02/04/xen-soll-in.html
---

<p>Wie bei <a href="http://www.pro-linux.de/news/2005/7784.html">Pro-Linux</a> zu lesen ist, will Andrew Morton die Virtualisierungssoftware Xen in den Linux-Kernel &uuml;bernehmen. Der Kernel-Hacker deutete das jedenfalls auf dem Enterprise Linux Summit an.</p>
<p>Dem steigenden Bedarf an Konsolidierung und Flexibilit&auml;t in Rechenzentren und Entwicklungsabteilungen steht die Verf&uuml;gbarkeit immer leistungsf&auml;higerer Hardware gegen&uuml;ber.  Software wie Xen kann die Br&uuml;cke dazwischen schlagen, was den auff&auml;lligen R&uuml;ckenwind des Projekts durch die Linux-Entwickler und Firmen wie IBM erkl&auml;rt.</p>
