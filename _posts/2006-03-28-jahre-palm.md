---
layout: post
title: "10 Jahre Palm"
microblog: false
guid: http://geewiz.micro.blog/2006/03/28/jahre-palm.html
date: 2006-03-28T07:22:47+0200
type: post
url: /2006/03/28/jahre-palm.html
---

<p><img width='108' height='144' style=<a href="http://www.heise.de/newsticker/meldung/71306.">float: left; border: 0px; padding-left: 5px; padding-right: 5px;" src="/uploads/palmpilot.jpg" alt="" />Die kleine Firma Palm Computing wollte mit vergleichsweise winzigem Budget ein hohes Ziel erreichen, n&auml;mlich einen PDA zu erschaffen, der weniger als 300 Dollar kostete, einfach zu bedienen war, seine Daten mit einem PC abgleichen konnte und in eine Hosentasche passte. Nach dem Kauf durch US Robotics lieferte sie im April 1996 die ersten PDAs aus, die diese Kriterien erf&uuml;llten: die "Palm Pilot"-Serie. Wie sich die Firma &uuml;ber die folgenden 10 Jahre zu einem f&uuml;hrenden PDA-Hersteller mauserte, erkl&auml;rt "Heise Online</a><br />
Mein erster PDA war das Nachfolgemodell "Pilot 5000". Damals benutzte ich ein Zeitplanbuch, und ich verstand zun&auml;chst nicht, wie die elektronischen Varianten eines Kalenders und Adressbuchs, die auf einem kleinen Display angezeigt wurden und die Eingabe von Texten Buchstabe f&uuml;r Buchstabe erforderte, meinem Tempus &uuml;berlegen sein sollte. Bis ich entdeckte, dass im Internet daf&uuml;r noch viele Erweiterungen und zus&auml;tzliche Anwendungsprogramme zur Verf&uuml;gung standen. So wurde ich begeisterter Palm-Anwender und schaffte mir sp&auml;ter noch einen Palm IIIx und einen Palm Vx an.</p>
<p>Auch wenn den beiden Palms noch ein Zaurus 5500 folgte, verwende ich f&uuml;r meine Organisation inzwischen wieder &uuml;berzeugt ein Zeitplanbuch. Es erlaubt mir, Eingaben schnell mal hinzukritzeln, stellt mir Termine ganzer Monate &uuml;bersichtlich auf einen Blick dar, verdaut auch Visitenkarten und andere Unterlagen. Ausserdem hat das handschriftliche Erfassen in meinem Selbstmanagement auch einen psychologischen Aspekt.</p>
<p>Aber so ganz habe ich Palm nicht abgeschworen: ich suche immer wieder nach Argumenten daf&uuml;r, mir einen Treo 650 anzuschaffen. :-)</p>
