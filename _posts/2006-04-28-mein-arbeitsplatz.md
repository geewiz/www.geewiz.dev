---
layout: post
title: "Mein Arbeitsplatz"
microblog: false
guid: http://geewiz.micro.blog/2006/04/28/mein-arbeitsplatz.html
date: 2006-04-28T23:39:03+0200
type: post
images:
- /uploads/arbeitsplatz.serendipityThumb.jpg
photos:
- /uploads/arbeitsplatz.serendipityThumb.jpg
url: /2006/04/28/mein-arbeitsplatz.html
---

<p>Nachdem ich jetzt per <a href="http://moto4lin.sourceforge.net/wiki/Main_Page">moto4lin</a> an die Bilder meines Handys komme und heute so sch&ouml;n die Sonne auf meinen Schreibtisch scheint, habe ich mal ein Foto davon gemacht. So sieht also mein Arbeitsplatz aus:</p>
<div class="serendipity_imageComment_center">
<div class="serendipity_imageComment_img">
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/arbeitsplatz.serendipityThumb.jpg" />
</div>
</div>
<div class="serendipity_imageComment_txt">Mein Arbeitsplatz bei WEB.DE</div></p>
</div>
