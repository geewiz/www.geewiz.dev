---
layout: post
title: "Auf zur Insel"
microblog: false
guid: http://geewiz.micro.blog/2005/12/28/auf-zur-insel.html
date: 2005-12-28T21:50:00+0200
type: post
url: /2005/12/28/auf-zur-insel.html
---

<p>Gestern habe ich mein erstes Enterprise Java Bean in einem JBoss-Server von einem HTTP-Servlet aus angesprochen. Es hat einige Stunden gedauert, bis ich die gesamten Zusammenh&auml;nge zwischen Eclipse, XDoclet und JBoss entwirrt und meine Fehler verstanden hatte, aber am Ende war ich tief beeindruckt: wie sehr Eclipse mit geeigneten Plugins die Entwicklung von J2EE-Anwendungen unterst&uuml;tzt, h&auml;tte ich mir nicht tr&auml;umen lassen.</p>
<p>Ich sehe f&uuml;r meine Entwicklungsumgebung eine Zweiteilung kommen: Eclipse f&uuml;r Java, Emacs f&uuml;r Perl und Shell. Bez&uuml;glich der Pflege von DocBook-Dokumenten bin ich noch unentschieden...</p>
<p>In den kommenden Tagen werde ich mich jetzt der weiteren Einarbeitung und der Planung meines ersten J2EE-Projekts widmen.</p>
