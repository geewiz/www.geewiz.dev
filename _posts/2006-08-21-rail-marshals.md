---
layout: post
title: "Rail Marshals"
microblog: false
guid: http://geewiz.micro.blog/2006/08/21/rail-marshals.html
date: 2006-08-21T21:27:06+0200
type: post
url: /2006/08/21/rail-marshals.html
---

<p>When I read Kristian's blog entry about politicians thinking aloud about <a href="http://blog.koehntopp.de/archives/1376-Mach-mir-den-Rail-Marshall.html">installing armed railway guards</a>, the absurdity of that train of thought (if you pardon the pun) made me chuckle.</p>
<p>But I couldn't help but laugh out loud when I read <a href="http://blog.koehntopp.de/archives/1376-Mach-mir-den-Rail-Marshall.html">Olav's explanation</a>#c10755 (translated):</p>
<blockquote class="posterous_short_quote">
<p>It must under all costs be prevented that terrorists hijack trains and, for example, steer them into a Frankfurt skyscraper.</p>
</blockquote>
