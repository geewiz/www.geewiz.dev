---
layout: post
title: "Daten &uuml;berwintern"
microblog: false
guid: http://geewiz.micro.blog/2006/01/23/daten-uumlberwintern.html
date: 2006-01-23T02:42:00+0200
type: post
url: /2006/01/23/daten-uumlberwintern.html
---

<p>Auf meiner Forschungsreise durchs Java-Land habe ich jetzt die Anwendung von <a href="http://www.hibernate.org">Hibernate</a> gelernt. Es handelt sich dabei um ein Persistence framework, also um eine Softwareschicht zur dauerhaften Datenspeicherung in Datenbanken. Das Tolle an Hibernate ist, wie die Zugriffe auf die Datenbank gekapselt werden.Zun&auml;chst programmiert man einfache Javabeans (auch POJO(Plain Old Java Object) genannt), also gew&ouml;hnliche Klassen mit privaten Attributen und entsprechenden Setter- und Getter-Methoden. Zum Speichern eines Objekts &uuml;bergibt man dieses einfach an eine Hibernate-Session, die f&uuml;r die entsprechende Datenbank konfiguriert ist. Auch das Lesen von Daten erfolgt nicht durch SQL-Abfragen, sondern durch Methodenaufrufe der POJOs oder der Hibernate-Session.</p>
<p>Und woher weiss Hibernate, welche Tabellen und Spalten den Objektattributen zugeordnet sind? Aus einfachen XML-Dateien, die diese Abbildung beschreiben. Ein Beispiel: Objekte der Klasse <code>Person</code> kommen in die Tabelle <code>PERSON</code>, wobei die Attribute <code>firstname</code> und <code>lastname</code> in die gleichnamigen Spalten der Tabelle geschrieben werden sollen. Zus&auml;tzlich gibt es einen Prim&auml;rschl&uuml;ssel <code>PERSON_ID</code>, der automatisch erzeugt werden und dem Attribut <code>id</code> zugeordnet sein soll:</p>
<p />
<p>Das wars schon. Und richtig cool wird es, wenn man diese Mappingdateien gar nicht von Hand pflegt, sondern durch <a href="http://xdoclet.sourceforge.net/">Xdoclet</a> automatisch aus Kommentar-Attributen der Javaklasse erzeugen l&auml;sst. So kann man sich voll auf die Gesch&auml;ftslogik seiner Klassen konzentrieren, anstatt sich mit SQL rumzuschlagen.</p>
