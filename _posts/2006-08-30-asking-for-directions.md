---
layout: post
title: "Asking for directions"
microblog: false
guid: http://geewiz.micro.blog/2006/08/30/asking-for-directions.html
date: 2006-08-30T23:18:35+0200
type: post
url: /2006/08/30/asking-for-directions.html
---

<p>Many men, yours truly included, have difficulties with asking for directions. </p>
<p>And it's not just because we like to drive a few rounds in the neighborhood. We have our <a href="http://video.google.com/videoplay?docid=-200960761624017150">reasons</a>, you know!</p>
