---
layout: post
title: "Happiness boosts productivity"
microblog: false
guid: http://geewiz.micro.blog/2007/04/02/happiness-boosts-productivity.html
date: 2007-04-02T06:59:00+0200
type: post
url: /2007/04/02/happiness-boosts-productivity.html
---

<p>Last week, I added another card to the board that displays my team's tasks for the coming weeks, saying "Having Fun". It stands out because it's green, in contrast to the white task cards.</p>
<p>And tomorrow, I'm going to ask them what things there are that rain on their parade. We will talk about sources of unhappiness at their job and how we can get rid of them.</p>
<p>Yes, I've been on a kind of happiness trip over the last weeks. But it's not because I've joined some joyful cult but because I get paid to maximize my team's productivity. And there is a direct relation between happiness and productivity, as Alexander Kjerulf points out in his blog entry <a href="http://positivesharing.com/2007/03/top-10-reasons-why-happiness-at-work-is-the-ultimate-productivity-booster/.">Top 10 reasons why happiness at work is the ultimate productivity booster</a></p>
<p>The 10 reasons he explicates on his blog are:</p>
<h1>Happy people work better with others</h1>
<h1>Happy people are more creative</h1>
<h1>Happy people fix problems instead of complaining about them</h1>
<h1>Happy people have more energy</h1>
<h1>Happy people are more optimistic</h1>
<h1>Happy people are way more motivated</h1>
<h1>Happy people get sick less often</h1>
<h1>Happy people learn faster</h1>
<h1>Happy people worry less about making mistakes - and consequently make fewer mistakes</h1>
<h1>Happy people make better decisions</h1>
<p>That's a buttload of advantages happy people have over their unhappy colleagues, isn't it? Therefore, I find it one of my foremost duties to take care of my team's happiness. Let's see how well they do on the "I feel good" scale.</p>
