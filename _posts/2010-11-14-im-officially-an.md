---
layout: post
title: "I'm officially an expert on chaos"
microblog: false
guid: http://geewiz.micro.blog/2010/11/14/im-officially-an.html
date: 2010-11-14T12:36:00+0200
type: post
url: /2010/11/14/im-officially-an.html
---

<p>I just got an invitation to the <a href="http://www.cmsim.org">4th Chaotic Modeling and Simulation <br />International Conference</a> in Greece.
<p /> Since I'm not an active member of the international physics community, <br />I can only assume Google Street View now also publishes workplace <br />photos.</p>
