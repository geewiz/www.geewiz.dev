---
layout: post
title: "Gott ist die Liebe"
microblog: false
guid: http://geewiz.micro.blog/2006/01/26/gott-ist-die.html
date: 2006-01-26T07:15:21+0200
type: post
url: /2006/01/26/gott-ist-die.html
---

<p>So lautet &uuml;bersetzt der Titel der ersten Enzyklika Papst Benedikts XVI. Wobei das deutsche Wort "Liebe" mehrdeutig ist: "Sei lieb", "Ich liebe dich" und "Lass uns schmutzig Liebe machen" sprechen von durchaus verschiedenen Dingen. Dementsprechend differenziert auch der Papst in "Deus caritas est" und erkl&auml;rt, wie er "eros", "agape" und "caritas" verkn&uuml;pft sieht.</p>
<p>Die <a href="http://www.spiegel.de/panorama/0">W&uuml;rdigung der Enzyklika</a>,1518,397245,00.html beim Spiegel hinterl&auml;sst bei mir den Eindruck, dass wir mit Joseph Ratzinger vielleicht doch gar kein so schlechtes Kirchenoberhaupt bekommen haben. Der Geist weht, wo er will...</p>
