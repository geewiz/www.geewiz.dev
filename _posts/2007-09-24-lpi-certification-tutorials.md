---
layout: post
title: "LPI certification tutorials from IBM"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/lpi-certification-tutorials.html
date: 2007-09-24T22:00:00+0200
type: post
url: /2007/09/24/lpi-certification-tutorials.html
---
<p>Today, I found two series of tutorials for the Linux Professional Institute (LPI) certification programme on IBM's DeveloperWorks website.</p>
<p>In my opinion, a certification is a good thing because the employer can instantly see that you've put some work into your qualifications. But just answering some questionnaire will not bring you far when you've got to do real work, so you'd better add quite some practical experience to the mix.</p>
<p>Those tutorials provide you with the means to get some of this practice. Therefore, if you're interested in getting an LPI certification (and I know quite a few folks who do), check them out:</p>
<ul>
<li>
<a href="http://del.icio.us/geewiz/lpic1">Tutorials for LPIC-1</a> (LPI Certificate Level 1)</li>
<li>
<a href="http://del.icio.us/geewiz/lpic2">Tutorials for LPIC-2</a> (LPI Certificate Level 2)</li>
</ul>
