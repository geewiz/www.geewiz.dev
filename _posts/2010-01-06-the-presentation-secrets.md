---
layout: post
title: "The Presentation Secrets of Steve Jobs"
microblog: false
guid: http://geewiz.micro.blog/2010/01/06/the-presentation-secrets.html
date: 2010-01-06T21:54:05+0200
type: post
url: /2010/01/06/the-presentation-secrets.html
---

<p>I enjoy a good presentation that makes it easy for me to learn new things or that transfers a message in an entertaining way. And noone I know of does it better than Steve Jobs. His keynote at MacWorld 2008 where he introduced the MacBook Air is legen... (wait for it) ...dary!</p>
<p>Carmine Gallo even wrote a book about his presentation style named "The Presentation Secrets of Steve Jobs". And in this SlideShare presentation, he summarizes the key points which push Steve's presentations from good to great. </p>
<div style="text-align:left;">
<a href="http://www.slideshare.net/cvgallo/the-presentation-secrets-of-steve-jobs-2609477" title="The Presentation Secrets of Steve Jobs" style="font:14px Helvetica,Arial,Sans-serif;display:block;text-decoration:underline;margin:12px 0 3px;">The Presentation Secrets of Steve Jobs</a>
<div style="font-size:11px;font-family:tahoma,arial;height:26px;padding-top:2px;">View more <a href="http://www.slideshare.net/" style="text-decoration:underline;">presentations</a> from <a href="http://www.slideshare.net/cvgallo" style="text-decoration:underline;">Carmine  Gallo</a>.</div>
</div>
<p>Interesting, right? I recommend you take the time to watch his <a href="http://www.apple.com/quicktime/qtv/mwsf08/">MacWorld 2008 keynote</a> (again) and learn how he not only gets his message across, but how he really hammers it home.</p>
