---
layout: post
title: "Starting my first online course"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/starting-my-first.html
date: 2007-09-24T22:09:59+0200
type: post
url: /2007/09/24/starting-my-first.html
---

<p>A week ago, I announced my first public online course called "<a href="http://www.freistil-consulting.de/perl-20071015">Perl-Meisterkurs</a>". It's a web-based seminar that teaches all the basics you need to know to start programming in Perl.</p>
<p>I'm very excited that people are already enrolling in the course. I take this as a gesture of trust and I'm determined not to disappoint.</p>
<p>I'm so happy that I even decided to offer a <a href="http://www.freistil-consulting.de/rabatt-f-r-den-perl-meisterkurs">rebate</a> for bringing in other people.</p>
<p>Online courses provide me with the means to live my passion for teaching without having to travel or take days off my day job. On the other hand, I have to invest much time and care into putting together the course materials because they not only have to transfer knowledge but also to enable and motivate participants to learn single-handedly as well as in the group.</p>
<p>As a platform for web-based training, I installed a <a href="http://www.moodle.org">Moodle</a> course content management system on <a href="http://www.it-dojo.de">IT-Dojo</a>. There, participants are provided with the course materials, forums to ask and discuss questions and other helpful features.</p>
<p>What I find great about being a trainer and coach is that you get to learn new things yourself all the time. For example, I'm learning to improve my course materials, to use audio and video to transfer knowledge,  and to use a web-based training platform. I'm learning to help people learning, so to speak. That's such a great thing.</p>
