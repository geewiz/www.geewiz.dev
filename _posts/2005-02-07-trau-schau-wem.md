---
layout: post
title: "Trau schau wem"
microblog: false
guid: http://geewiz.micro.blog/2005/02/07/trau-schau-wem.html
date: 2005-02-07T20:20:00+0200
type: post
url: /2005/02/07/trau-schau-wem.html
---

<p>In der PDF-Datei <a href="http://www.thinkcomputer.com/corporate/news/southstation.pdf">How to Take Over a Train Station</a> beschreibt der Autor, wie er einige simple Administrations- und Programmierfehler ausnutzen konnte, um in das WLAN eines Bostoner Bahnhofs einzudringen.</p>
<p>Klar: dass schlecht administrierte WLANs riesige Sicherheitsl&ouml;cher haben, ist nichts Neues. Aber der Text macht noch etwas anderes sehr deutlich: immer wieder sind auch vermeintlich professionell gebaute Webknoten offen wie ein Scheunentor. Wir tun gut daran, mehrmals nachzudenken, bevor wir unsere empfindlichen Daten irgendwo im Netz hinterlegen.</p>
