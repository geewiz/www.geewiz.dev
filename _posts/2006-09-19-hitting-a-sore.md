---
layout: post
title: "Hitting a sore spot"
microblog: false
guid: http://geewiz.micro.blog/2006/09/19/hitting-a-sore.html
date: 2006-09-19T20:13:51+0200
type: post
url: /2006/09/19/hitting-a-sore.html
---

<p>Weird Al Yankovic did it again: <a href="http://www.actsofvolition.com/archives/2006/september/lyricswhite.">White and Nerdy</a></p>
<p>Hey, what's so bad about admiring M.C. Escher as a great artist, reading Stephen Hawking and being really good at Pascal? You're all just jealous, that's what you are!</p>
