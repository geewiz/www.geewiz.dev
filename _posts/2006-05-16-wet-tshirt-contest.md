---
layout: post
title: "Wet T-Shirt Contest"
microblog: false
guid: http://geewiz.micro.blog/2006/05/16/wet-tshirt-contest.html
date: 2006-05-16T18:29:15+0200
type: post
url: /2006/05/16/wet-tshirt-contest.html
---

<p>Gedanken am fr&uuml;hen Morgen:</p>
<ul>
<li>Hm, regnerisch heute. Ist auch nass draussen. Naja, scheint schon vorbeigezogen zu sein.</li>
<li>Ah, frische, pollenfreie Luft. Super zum Joggen!</li>
<li>Ui, weiter weg donnerts noch. Und ich lauf &uuml;bers Feld...</li>
<li>(10 Minuten sp&auml;ter)</li>
<li>Jetzt nieselts doch. Na, Mann oder Memme?</li>
<li>Ein kurzer Schauer macht nichts.</li>
<li>Es sei, es handelt sich um einen Graupelschauer!</li>
<li>Schon bl&ouml;d, wenn es einem ins Ohr hagelt...</li>
<li>Okay, es sch&uuml;ttet. Ich dreh dann mal besser um.</li>
<li>Ja, jetzt ist die Unterhose auch nass.</li>
<li>Sieh an, Jogginghosen verdreifachen bei Regen ihr Gewicht!</li>
<li>Jetzt fehlt bloss noch, dass mich der Blitz trifft.</li>
<li>Uff, geschafft. Jetzt h&auml;tt ich gern ne <em>warme</em> Dusche.</li>
</ul>
