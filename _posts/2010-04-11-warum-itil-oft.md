---
layout: post
title: "Warum ITIL oft nicht (komplett) umgesetzt wird"
microblog: false
guid: http://geewiz.micro.blog/2010/04/11/warum-itil-oft.html
date: 2010-04-11T10:53:00+0200
type: post
url: /2010/04/11/warum-itil-oft.html
---

<p>In einem kurzen Video z&auml;hlt&nbsp;Malcolm Fry, Autor des Buchs "ITIL Lite", einige g&auml;ngige H&uuml;rden auf, an denen eine ITIL-Einf&uuml;hrung scheitern kann:</p></p>
<ul>
<li>Kosten</li>
<li>Fehlende Unterst&uuml;tzung durch die Kunden</li>
<li>Beschr&auml;nkungen (z.B. durch eine ISO-20000-Zertifizierung)</li>
<li>Zeitknappheit</li>
<li>Fehlender Einfluss</li>
<li>Verlust des Antriebs</li>
<li>Zu hohe Komplexit&auml;t</li>
<li>ITIL V2 bereits eingef&uuml;hrt</li>
<li>Konflikt mit anderen Management-Initiativen</li>
</ul>
<div>[youtube [www.youtube.com/watch](http://www.youtube.com/watch?v=PPBvkiEZExQ])</div>
