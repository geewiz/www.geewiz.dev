---
layout: post
title: "Urlaub... Ah ja.."
microblog: false
guid: http://geewiz.micro.blog/2005/08/22/urlaub-ah-ja.html
date: 2005-08-22T20:43:45+0200
type: post
url: /2005/08/22/urlaub-ah-ja.html
---

<p>Stimmt, das wollte ich doch mal ausprobieren. Und gestern fiel mir gl&uuml;cklicherweise noch rechtzeitig ein, dass ich diese Woche als Zeitpunkt daf&uuml;r gew&auml;hlt hatte. Sonst w&auml;re ich heute morgen wahrscheinlich ins Gesch&auml;ft gefahren.</p>
<p>So aber war erst mal Ausschlafen und gem&uuml;tliches Fr&uuml;hst&uuml;cken angesagt. Und jetzt mache ich mir Gedanken, was ich mit der freien Zeit anfangen werde. </p>
<p>F&uuml;r Vorschl&auml;ge bin ich offen! ;)</p>
