---
layout: post
title: "USB cradle for the Nokia E61"
microblog: false
guid: http://geewiz.micro.blog/2006/09/01/usb-cradle-for.html
date: 2006-09-01T23:53:21+0200
type: post
url: /2006/09/01/usb-cradle-for.html
---

<p>Years ago, when I bought my Palm Pilot 5000 or the Palm Vx later, it went without saying that there also was a cradle in the package that connected to the PC to synchronize data and load the batteries. Today, you can count yourself happy if you don't have to buy the USB cable additionally. Nokia actually puts only a USB connection cable into the E61 package -- Nokia will charge you extra for a cable that in turn charges your phone.</p>
<p>If you want some cradle goodness that gives your phone a nice home while it's not that mobile, then the <a href="http://www.e-series.org/archives/96">review of the ORA USB Cradle for Nokia E61</a>  on e-series.org may interest you.</p>
