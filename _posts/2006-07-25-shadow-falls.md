---
layout: post
title: "Shadow Falls"
microblog: false
guid: http://geewiz.micro.blog/2006/07/25/shadow-falls.html
date: 2006-07-25T07:38:25+0200
type: post
url: /2006/07/25/shadow-falls.html
---

<p>I like radio drama plays, especially on my iPod when I'm commuting. My favourite is the "Paul Temple" detective series written by Francis Durbridge. Although (or because?) West German Broadcasting (WDR) produced the series[1] already in the Fifties, it's great entertainment to listen to.</p>
<p>From this month on, the PodShow crew around Adam Curry takes its shot at a drama series podcast: <a href="http://shadowfalls.podshow.com/.">Shadow Falls</a> </p>
<blockquote class="posterous_medium_quote">
<p>Shadow Falls tells the eerie tale of a remote Northeastern town that holds secrets some will kill to protect and others will die to expose. Settled 400 years ago by the survivors of a mysterious tradgedy, the people of Shadow Falls have become pawns being used by two supernatural forces seeking to win a war older than time itself, a war which many believe could trigger the Battle of Armageddon, and wipe out humanity as we know it.</p>
</blockquote>
<p>The characters are played by professional actors and, much like in <a href="http://mollysdiary.podshow.com/">Desperate Housewives", there's also an in-story narrator, Molly Hammacher, a girl that vanished in the past. Molly even has her own "Diary Blog</a> giving additional background information.</p>
<p>The first part of the series was released a few days ago. It's great and I'm looking forward to the next one!</p>
<p>Oh, and the title track, <a href="http://music.podshow.com/music/listeners/artistdetails.php?BandHash=2673377414907b3d6d7de87470c872ad">I am -- Molly's Theme</a> by Munk, is great as well!</p>
<p>fn1. Obviously, it's a german translation. There's also an english radio drama series of "Paul Temple" available on CD, but I didn't try that yet.</p>
