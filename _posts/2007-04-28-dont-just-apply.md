---
layout: post
title: "Don't just apply, communicate!"
microblog: false
guid: http://geewiz.micro.blog/2007/04/28/dont-just-apply.html
date: 2007-04-28T06:56:59+0200
type: post
url: /2007/04/28/dont-just-apply.html
---

<p>I still am looking for Linux sysadmins because my responsibilities grow faster than my team. Unfortunately, good Linux admins seem to be hard to find. Of all the applicants, fewer than the half get an appointment to present themselves.</p>
<p>But what really surprises me is that not even 10 percent of those that get invited make contact before or at least after their interview. This happens so seldom that those who do always make a positive impression. Why is it that people applying for an IT job at a huge internet company don't seem to be able to do a Google search for the names of those they got invited to meet in person?</p>
<p>That's why I'd like to give all job seekers a hint: People that were rejected only had applied; those who got the job, communicated.</p>
<p>So, do communicate -- before, while and after your job interview.</p>
<p>h2. Before the interview</p>
<p>When you prepare for your interview, not only get to know the company you'd like to work at, but especially collect information about those people that are going to do the interview and may probably be your future boss.</p>
<p>Many people warn not to publish compromising material about yourself because it may be found by a future employer. But that works the other way around, too. And it's not only the employer that's entitled to look for references either.</p>
<h1>Ask about who will conduct your interview and make use of the mighty Google. Are they technically savvy? You'd better be prepared accordingly. Do they blog about their hobbies? Maybe you share some of them; mentioning that could build some common ground to talk about.</h1>
<h1>There are other people you could contact, too: your potential future colleagues. For example, Guy Kawasaki sees a tight connection between <a href="http://blog.guykawasaki.com/2007/04/linkedin_and_th.html.">LinkedIn and the Art of Avoiding an Asshole Boss</a><br />
</h1>
<p>h2. During the interview</p>
<p>During the interview, always keep in mind that its purpose is to get as much valuable information as possible -- for both sides! They want to see if you have the necessary skills to do the job and the right attitude to fit in the team. So concentrate on the points that will convince them that it's you they're looking for.</p>
<p>But don't forget that there are also things you need to know. While your interviewers aim to confirm the positive impression they got from your papers, you should put to the test the impression of the company that made you apply. Additional to job details, work environment and perks, there are other things to ask. Alexander Kjerulf has the following <a href="http://positivesharing.com/2007/03/some-killer-questions-to-ask-in-your-next-job-interview/">suggestions for questions</a> that may lead to interesting insights:</p>
<ul>
<li>Whats been your best experience working at this company?</li>
<li>When do you have the most fun at work ?</li>
<li>Who do you enjoy working with the most here? What do you like about them?</li>
<li>Which manager do you admire the most in this company? What do you admire about that person?</li>
<li>Whats the greatest thing your manager has done for his/her people?</li>
</ul>
<p>h2. After the interview</p>
<p>Even if you get rejected, don't stop communicating until you've asked about the reasons of your dismissal. You may not get a useful answer everytime, but I know at least one person that will tell you honestly why you weren't chosen. Don't let that opportunity of improving your job or interview skills slip away unused.</p>
<p>h2. Conclusion</p>
<p>Apart from Dr. Frankenstein, almost all employers expect their staff to successfully communicate.  So, put in the extra effort to get into the position of doing so to your best.</p>
<p>And, by the way, if you're an enthusiastic Linux system administrator that's looking for a new job at a workplace in southern Germany that's as much fun as it's challenging, please do apply. And don't forget to confirm that someone actually reads my blog. ;-)</p>
