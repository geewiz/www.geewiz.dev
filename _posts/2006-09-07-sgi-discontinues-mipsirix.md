---
layout: post
title: "SGI discontinues MIPS/IRIX line"
microblog: false
guid: http://geewiz.micro.blog/2006/09/07/sgi-discontinues-mipsirix.html
date: 2006-09-07T17:18:35+0200
type: post
url: /2006/09/07/sgi-discontinues-mipsirix.html
---

<p>It's been foreseeable and now it's official: <a href="http://www.heise.de/english/newsticker/news/77801">SGI abandons IRIX</a> as well as their MIPS-based workstation and server business at the end of this year.</p>
<p>First presented in 1988, IRIX-based systems like the Indigo, the Onyx, the Origin 2000 series or later day workstations like the O2 and the Octane made Silicon Graphics Inc. famous. If you talked about high-end computer graphics, especially in the movie special effects business, you talked about Silicon Graphics.</p>
<p>When I worked at the Karlsruhe Research Centre for my industrial placement and later for my diploma thesis, I did software development on SGI workstations and always dreamed about owning one myself. I eventually bought an Octane2 off eBay, but since I won't have the space (and time) to use it in our new flat in Freiburg, I reluctantly sold it a few days ago.</p>
<p>In the recent years, SGI had a lot of management and financial trouble. They entered the Linux market by building their Itanium-based Altix server series, unfortunately with only mild success, and stopped the development of new systems based on MIPS RISC CPUs.</p>
<p>Although SGI will continue support for the next years, this era is now finally coming to an end.</p>
