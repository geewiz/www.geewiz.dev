---
layout: post
title: "DHH tries Windows, goes back to Mac begrudgingly"
microblog: false
guid: http://geewiz.micro.blog/2019/11/05/i-can-relate.html
date: 2019-11-05T18:14:00+0200
type: post
url: /2019/11/05/i-can-relate.html
---
I can relate to [DHH trying to drop the Stockholm syndrome](https://m.signalvnoise.com/back-to-windows-after-twenty-years/). Apple machines are a very mixed bag. But they're still the best desktop experience I can find. I'm now pairing my iMac and iPad with a Linux dev server and get both best worlds (i.e. desktop pleasure and open source power).
