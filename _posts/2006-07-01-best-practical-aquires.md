---
layout: post
title: "Best Practical aquires SVK"
microblog: false
guid: http://geewiz.micro.blog/2006/07/01/best-practical-aquires.html
date: 2006-07-01T22:00:21+0200
type: post
url: /2006/07/01/best-practical-aquires.html
---

<p>Best Practical, the company behind the widely used trouble ticket system "Request Tracker", "RT" for short, added another productivity enhancer to their portfolio: Chia-liang Kao, developer of SVK, joined Best Practical as a partner. </p>
<p>SVK is a versioning system based on Subversion that allows distributed repositories. For example, you can pull a copy of the main repository onto your laptop, use that local repository while you're moving all over the world, and wherever you have internet access, you can synchronize your local repository with the main one.</p>
<p>Since SVK, like RT, is written in Perl, Best Practical enforces with that move its position as a small but successful business specialising in Perl-based productivity tools.</p>
