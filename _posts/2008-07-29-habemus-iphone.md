---
layout: post
title: "Habemus iPhone"
microblog: false
guid: http://geewiz.micro.blog/2008/07/29/habemus-iphone.html
date: 2008-07-29T04:17:03+0200
type: post
url: /2008/07/29/habemus-iphone.html
---

<p>It had to happen. And you didn't expect me to withstand the temptation, did you? Thought so. So I'm an iPhone owner now.</p>
<p>The only thing so far I'm not really happy about so far is the battery life. In my backpack, I always carry the charger and USB cable.</p>
<p>But everything else is at least interesting if not totally amazing. The touch UI is great, and with time, I also manage typing text without missing every second virtual key.</p>
<p>The applications I installed first were Instapaper, NetNewsWire, Evernote and, of course, Super Monkey Ball.</p>
<p>To distract people like my precious from the reasons "gadget mania" and "Apple fanboyism", I'm trying out Omni Outliner, both on the Mac and on the iPhone. And to keep productivity from reaching unhealthy limits, I also installed Twinkle. As an interesting fact, both applications use the GPS function to be more effective.</p>
<p>I'm looking forward to more interesting applications, but for me the iPhone already is a better tool than my Nokia E61 ever has been.</p>
<p>PS: Because of the idiots at T-Mobile, I had to get a new cell phone number. If you still have my old Vodafone number (or even the long dead O2 number), please email me!</p>
