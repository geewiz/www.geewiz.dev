---
layout: post
title: "Asteroid nach Douglas Adams benannt"
microblog: false
guid: http://geewiz.micro.blog/2005/01/27/asteroid-nach-douglas.html
date: 2005-01-27T02:39:00+0200
type: post
url: /2005/01/27/asteroid-nach-douglas.html
---

<p>Schon vor einiger Zeit hatte der deutsche Astronom Felix Hormuth einen von ihm entdeckten Asteroid "ArthurDent" getauft, nach der Hauptperson der f&uuml;nfteiligen Trilogie "Per Anhalter durch die Galaxis".</p>
<p>Zu Ehren ihres Autors Douglas Adams wurde jetzt ein weiterer Asteroid benannt: in der Bezeichnung "2001 DA42" stecken neben seinem Todesjahr und seinen Initialen nat&uuml;rlich auch die Antwort auf die Frage nach dem Leben, dem Universum und dem ganzen Rest.</p>
<p>(via <a href="http://www.msnbc.msn.com/id/6867061/">MSNBC</a>)</p>
