---
layout: post
title: "Skype for Symbian is official"
microblog: false
guid: http://geewiz.micro.blog/2006/10/25/skype-for-symbian.html
date: 2006-10-25T20:34:52+0200
type: post
url: /2006/10/25/skype-for-symbian.html
---

<p>Finally, according to the german news site <a href="http://www.golem.de/0610/48535.html">Golem</a>, Skype officially announced a software version of their VOIP client for Symbian-based cell phones until end of the year.</p>
<p>Skype hasn't disclosed yet what Symbian platform exactly the client is being developed for, but since it is meant to be available on some Nokia phones first, we can assume that it'll be the S60 3rd edition that the recent N-series and E-series models run on.</p>
<p>Needless to say that Mobile operators are reluctant to support Skype on their networks, but I guess I can at least stop looking for a wireless home phone now.</p>
