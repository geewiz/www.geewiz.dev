---
layout: post
title: "Subway-Journalismus"
microblog: false
guid: http://geewiz.micro.blog/2008/09/16/subwayjournalismus.html
date: 2008-09-16T08:23:09+0200
type: post
url: /2008/09/16/subwayjournalismus.html
---

<p>Wenn ich bei Stefan Niggemeier Artikel wie <a href="http://www.stefan-niggemeier.de/blog/der-subway-journalismus-von-prosieben/">diesen</a> und <a href="http://www.stefan-niggemeier.de/blog/subway-journalismus-fuer-alle/">diesen</a> lese, dann ist das viel besser als das d&auml;mliche Fernsehen mit seinen Galileo-Sendungen, die offenbar eine kr&auml;ftige Mischung aus Wissensmagazin und Dauerwerbesendung sind.</p>
<p>(The two articles linked report how a PR agency successfully put Subway brand placement into several popular knowledge programmes in German TV, how they uncovered everything by bragging about it and how they now are trying to get the genie back into the bottle).</p>
