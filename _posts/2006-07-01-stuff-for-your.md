---
layout: post
title: "Stuff for your iPod"
microblog: false
guid: http://geewiz.micro.blog/2006/07/01/stuff-for-your.html
date: 2006-07-01T22:14:39+0200
type: post
url: /2006/07/01/stuff-for-your.html
---

<p>(If you haven't noticed yet, I'm cleaning up my "blog this" todo list.)</p>
<p>After <a href="http://blog.jochen-lillich.de/archives/402-Der-iPod-rockboxt.html">purchasing the iPod nano</a>, I had to get a few other accessories: the Ipodome protective overlays, the Sennheiser OMX 70, and the XtremeMac SportWrap. All are just great, so I thought of letting you know.<br />
If the iPod Nano has one weakness, then it's that it's so prone to scratches. It's almost like even watching the thing gets it scratched! I researched all the options for protection and decided to get a <a href="http://shop.ipodome.de/catalog/product_info.php?cPath=179">Ipodome</a>&amp;products_id=495 foil. After all, what's the use of buying an Apple product and then hiding it in some silicone or leather case? ;-) The Ipodome foil wraps all around the Nano, so not only the display, but every surface (except the small ones with the hold switch and the connectors) is protected. I actually managed to apply the overlay correctly, so it's almost invisible and I can now show off my player in all its shine without worrying about the latter getting lost.</p>
<p>The original Apple earbuds may help in signalling <a href="http://www.amazon.de/exec/obidos/ASIN/B000FI10JM/geewizweblog-21">Hey, I can afford an Apple player!", but if you care about wearing comfort and sound, there are better alternatives. Because I wanted to use the iPod while running, I chose the "Sennheiser OMX 70 sport headphones</a> recommended by <a href="http://www.ilounge.com/.">iLounge</a> Since you wear them over your ears instead of inside your ears, they feel very comfortable even after having them on for hours. The cable is long enough to put the iPod wherever you want without risking of yanking it out by moving your head or torso. And the sound is just as you can expect from a Sennheiser product.</p>
<p>Since pants pockets are not the best place to put the iPod while running, I also bought the <a href="http://www.amazon.de/exec/obidos/ASIN/B000E453YK/geewizweblog-21.">XtremeMac SportWrap</a> It comes with two straps of different length, so you can strap your player either onto your biceps or onto your wrist. Since it's made of neoprene, skin irritation and sweat shouldn't be a problem.</p>
<p>Having written this, I realize that being part of the big iPod community really has its price, granted, but on the other hand also the big advantage of having a huge supply of experiences and recommendations helping you spend your money well. I hope my article adds to that.</p>
