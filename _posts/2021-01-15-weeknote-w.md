---
layout: post
title: "Weeknote W2 2021"
microblog: false
guid: http://geewiz.micro.blog/2021/01/15/weeknote-w.html
date: 2021-01-15T19:59:00+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2021/01/15/weeknote-w.html
---
## Re-launching my live stream
This week, I started [live coding on Twitch](https://www.twitch.tv/fullstacklive) again. I had planned to get going on Tuesday but didn't have the energy; more on this later. On Thursday, I was ready to launch. It took only a few minutes of being online until many familiar names started popping up in chat. It felt so good. This year, I'm trying a different strategy. I'm going to focus on an area in which I have lots of experience: IT training, starting with a Ruby course. In simple terms, it'll work like a book club where the participants work through a chapter of the training material, and we'll talk it through on a weekly basis. I'm excited to see how it goes!

## VS Code dev containers
Until now, I didn't need a dedicated website for my live coding stream. I simply referred people to my [personal blog](https://www.geewiz.dev). But with the Ruby course coming up, I need to have a permanent space where people find information. As always, I'm going to use [Jekyll](https://jekyllrb.com/) to build the site for FullStackLive. 

I've been using Docker to run test instances of my Ruby projects for a long time because it makes separating their resources (the app, its database, other services...) so much easier. However, I always used scripts to interact with the containers. This time, I wanted to try a closer integration with my coding tools and set up a [VS Code dev container](https://code.visualstudio.com/docs/remote/containers). Thanks to Microsoft providing a big library of pre-built language images, setting up a dev container for Ruby development was painless. However, I had to invest a bit more time to get [my dotfiles](https://gitlab.com/geewiz/dotfiles) installed automatically after I realised that I didn't have any of my familiar commands and shell aliases available inside the dev container. I'm happy with the result and am looking forward to build a more complex setup for Rails development.

## Over-hyper-scheduling
In [weeknote W1](/2021/01/08/weeknote.html), I explained how I'm using the Sorted app for time-boxing my daily work. It's helping me a lot to keep my focus on what I want to get done. Its auto-scheduling feature also is a great reality check that prevents me from expecting too much from the time I have available in between calendar events.

Where I took a wrong turn was when I added time tracking to mix. I had discovered the Session app that comes with my Setapp subscription and thought that it would increase my focus even more, and on the other hand make sure that I took frequent breaks as well. What happened instead was that I started to feel burnt out. That's why I didn't start streaming on Tuesday. I think that there were multiple reasons for this energy drain:

- The Pomodoro periods in Session didn't align well with my task slots in Sorted that are usually multiples of 30 minutes.
- Getting interrupted every 25 minutes kept me from getting into a flow state.
- I often ended up ignoring the timer completely, leaving me with frustratingly few tasks actually tracked.

This is not the first time that a new tool created more frustration instead of more productivity. Sorted stays, Session goes. I'm a slow learner sometimes.
