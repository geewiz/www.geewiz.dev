---
layout: post
title: "WLAN in 5 Minuten"
microblog: false
guid: http://geewiz.micro.blog/2005/12/07/wlan-in-minuten.html
date: 2005-12-07T09:06:45+0200
type: post
url: /2005/12/07/wlan-in-minuten.html
---

<p>Um mit dem Firmenlaptop m&ouml;glichst flexibel zu sein, habe ich mir wieder ein WLAN eingerichtet. Nat&uuml;rlich wird auch hier Linux-f&auml;hige Hardware bevorzugt. Deshalb habe ich mir einen <a href="http://www1.linksys.com/international/product.asp?coid=8">Linksys WRT-54GS</a>&amp;ipid=837 zugelegt. Den Firmware-Wechsel habe ich zwar bislang noch nicht gemacht, aber schon allein die einfache Inbetriebnahme hat mich beeindruckt.Router ans LAN angeschlossen, auf dem PC ein virtuelles Interface f&uuml;r 192.168.1.1 angelegt, und schon konnte ich im Browser innerhalb von 5 Minuten alle n&ouml;tigen Einstellungen vornehmen. Noch schnell das WLAN-Interface auf dem Notebook konfiguriert -- das Surfen konnte beginnen! Das erinnert mich dann doch schwer an das gute Bobbele.</p>
