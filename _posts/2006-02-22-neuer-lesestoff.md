---
layout: post
title: "Neuer Lesestoff"
microblog: false
guid: http://geewiz.micro.blog/2006/02/22/neuer-lesestoff.html
date: 2006-02-22T05:30:51+0200
type: post
url: /2006/02/22/neuer-lesestoff.html
---

<p>Auf Geburtstag habe ich mir das Buch "J2EE und JBoss" von Torsten Langner geschenkt. Heute hab ich es per Post bekommen, somit bin ich in den n&auml;chsten Tagen mit Lesestoff versorgt. In K&uuml;rze werde ich dann mehr zu dem Buch und seinem Thema schreiben.</p>
