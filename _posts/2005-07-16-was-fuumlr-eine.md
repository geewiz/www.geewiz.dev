---
layout: post
title: "Was f&uuml;r eine Woche..."
microblog: false
guid: http://geewiz.micro.blog/2005/07/16/was-fuumlr-eine.html
date: 2005-07-16T06:04:10+0200
type: post
images:
- /uploads/love_your_job.gif
photos:
url: /2005/07/16/was-fuumlr-eine.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/love_your_job.gif" />
</div>
<p> Gott sei Dank, es ist Freitag Abend. Diese Woche hatte deutlich zu viele unerwartete "Herausforderungen" und klar zu wenig Schlaf. Das Wochenende sollte also besser entspannend werden, sonst besorg ich mir f&uuml;r n&auml;chste Woche n&auml;mlich ein anderes!</p>
