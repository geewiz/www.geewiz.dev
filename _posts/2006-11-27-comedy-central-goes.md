---
layout: post
title: "Comedy Central goes german"
microblog: false
guid: http://geewiz.micro.blog/2006/11/27/comedy-central-goes.html
date: 2006-11-28T00:16:22+0200
type: post
url: /2006/11/27/comedy-central-goes.html
---

<p>Other than Spiegel Online International (why doesn't the german version report it as well?), I don't care if a great part of the german population will get american or british humour. I know I do.</p>
<p>And I also know that most of the dubbing staff doesn't.</p>
<p>Because I don't want to have to translate a sentence back into English word by word to recover its punch line, the great news of <a href="http://www.spiegel.de/international/0">Comedy Central making a german channel</a>,1518,449206,00.html quickly became a disappointment. Well, at least there's the Daily Show Global Edition on CNN. And, of course, YouTube.</p>
