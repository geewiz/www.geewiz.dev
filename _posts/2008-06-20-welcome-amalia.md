---
layout: post
title: "Welcome, Amalia!"
microblog: false
guid: http://geewiz.micro.blog/2008/06/20/welcome-amalia.html
date: 2008-06-20T19:23:17+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm4static_ouxkf-scaled500.jpg?w=225
photos:
- /assets/wp-content/2011/05/media_httpfarm4static_ouxkf-scaled500.jpg?w=225
url: /2008/06/20/welcome-amalia.html
---

<p><em>(Deutscher Text unten)</em></p>
<p>When Carolin went into labour at half past two in the morning, we knew that June 12th would be the birthday of our baby. What we didn't know was what a strain it would be.</p>
<p>We spent the rest of the night at home, but only I could still get some rest while Carolin couldn't get to sleep any more. In the morning, the time came to go to the hospital. After arrival and examination, we went for a walk, but didn't come far because of the contractions getting stronger.</p>
<p>And then, a long birth process started. Eventually, the contractions came almost without pause, so Carolin got medication to slow them down to prevent complete fatigue. When the actual birth was imminent, however, Carolin couldn't come up with enough strength to press the baby out. So, they had to give her another medication with the opposite effect than the one before.</p>
<p>Finally, after 10 hours, at 09:08pm, a totally exhausted Carolin and an also quite weary Jochen held their baby daughter in their arms: <strong>Amalia Elin Gall</strong>. Seh's healthy and obviously inherited my serenity.</p>
<div style="text-align:center;">
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm4static_ouxkf" height="500" src="/assets/wp-content/2011/05/media_httpfarm4static_ouxkf-scaled500.jpg?w=225" width="375" />
</div>
</div>
<p>Both girls will recover in hospital over the next days. Afterwards, we'll together enjoy the time I took off work at home.</p>
<p>More <a href="http://www.flickr.com/photos/geewiz/sets/72157605597464131/">pictures of Amalia</a> will be posted on Flickr. If you like, you can leave some greetings below in the comments which I'll gladly give to Carolin and Amalia!</p>
<p>And now, I'll open this bottle of Midleton Very Rare.</p>
<h2>Deutsch:</h2>
<p>Als gegen halb drei morgens Carolins Wehen einsetzten, wussten wir, dass der 12. Juni der Geburtstag unseres Babys sein w&uuml;rde. Was wir nicht wussten, war, wie anstrengend der Weg dorthin sein w&uuml;rde.</p>
<p>Die Nacht verbrachten wir noch zuhause, allerdings bekam nur ich ein paar Stunden Schlaf, w&auml;hrend Carolin nicht mehr einschlafen konnte. Am Vormittag wurde es dann Zeit, in die Klinik zu fahren. Nach Ankunft und Untersuchung gingen wir noch etwas spazieren, aber aufgrund der st&auml;rker werdenden Wehen kamen wir nicht allzu weit.</p>
<p>Und dann begann eine lange Geburt. Irgendwann lie&szlig;en die Wehen Carolin kaum noch Pausen. Um ihre Ersch&ouml;pfung zu lindern, erhielt sie ein Wehen hemmendes Medikament. Am Ende fehlte ihr dann jedoch die Kraft, das Kind hinaus zu pressen, sodass ein Medikament mit umgekehrter Wirkung n&ouml;tig wurde. </p>
<p>Schlie&szlig;lich jedoch hielten zehn Stunden sp&auml;ter um 21:08 Uhr eine v&ouml;llig ersch&ouml;pfte Carolin und ein auch recht m&uuml;der Jochen ihre Tochter in den Armen: <strong>Amalia Elin Gall</strong>. Sie ist gesund und hat offenbar meine Ruhe geerbt.</p>
<p>Die beiden Damen werden sich in den n&auml;chsten Tagen weiter im Krankenhaus erholen. Zusammen werden wir dann zuhause die Auszeit genie&szlig;en, die ich mir in der Firma genommen habe.</p>
<p>Weitere <a href="http://www.flickr.com/photos/geewiz/sets/72157605597464131/">Bilder von Amalia</a> sind auf Flickr zu finden. Gr&uuml;&szlig;e an Carolin und Amalia nehme ich gern in den Kommentaren unten entgegen -- ich werde jeden einzelnen ausrichten!</p>
<p>Und jetzt mache ich meine Flasche Midleton Very Rare auf. </p>
