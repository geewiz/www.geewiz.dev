---
layout: post
title: "Social Bookmarking"
microblog: false
guid: http://geewiz.micro.blog/2005/12/21/social-bookmarking.html
date: 2005-12-21T03:16:36+0200
type: post
url: /2005/12/21/social-bookmarking.html
---

<p>Weil ich mich momentan mit popul&auml;ren Themen wie J2EE besch&auml;ftige, probiere ich f&uuml;r meine Bookmarks den Dienst <a href="http://del.icio.us">Del.icio.us</a> aus. Ich speichere damit meine Links (verbunden mit einer Beschreibung und beliebig vielen Stichworten) nicht nur an einer zentralen Stelle im Web, wie ich es mit der <a href="http://sitebar.lillich.info">Sitebar</a> schon lange tue. Zus&auml;tzlich profitiere ich von den Eintr&auml;gen anderer, weil ich abfragen kann, welche Links es zu einem Stichwort (engl. "Tag") sonst noch gibt.</p>
<p>Die Organisation &uuml;ber Stichworte anstatt &uuml;ber eine starre Themenhierarchie gef&auml;llt mir bisher ganz gut. Ich behalte mir allerdings erst noch eine Bew&auml;hrungszeit vor, bevor ich alle <a href="http://del.icio.us/Geewiz">meine Bookmarks bei Del.icio.us</a> eintrage.</p>
