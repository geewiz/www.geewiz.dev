---
layout: post
title: "Kaffee-Dealer"
microblog: false
guid: http://geewiz.micro.blog/2005/04/25/kaffeedealer.html
date: 2005-04-25T20:52:53+0200
type: post
url: /2005/04/25/kaffeedealer.html
---

<p>Ich hielt schon bisher nichts von den neumodischen Kaffeemaschinen, bei denen spezielle Kaffeep&auml;ckchen zum Einsatz kommen. Dass damit nur eine Abh&auml;ngigkeit vom Hersteller entsteht, von der nur eine Seite profitiert, best&auml;tigt mir jetzt der <a href="http://www.shopblogger.de/blog/archives/902-Senseo.html.">Shopblogger</a></p>
<p>Meine Alternative ist fair gehandelter Kaffee. Von dem profitieren die Erzeuger vor Ort und er ist schon in vielen Superm&auml;rkten (z.,B. GLOBUS) zu haben.</p>
