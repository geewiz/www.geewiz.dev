---
layout: post
title: "No, listen, it's not a cow!"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/no-listen-its.html
date: 2007-09-24T22:08:48+0200
type: post
url: /2007/09/24/no-listen-its.html
---

<p>I had taken friday and monday off, so I came to work not before tuesday. And my desk looked somewhat different than when I left it thursday last week...</p>
<p>My sysadmin team thought that since I play a Tauren druid on World of Warcraft, I should have some mates around me during the day. That's why they covered my desk with green felt and populated it with nice little paper cows.</p>
<p>Well, I don't need a mouse pad any more, that's for sure. I'm uncertain if I need a new team, though.</p>
<p>PS 1: Additional photos: <a href="http://flickr.com/photos/geewiz/1367121552/">closeup 1</a>, <a href="http://flickr.com/photos/geewiz/1366226469">closeup 2</a></p>
<p>PS 2: This is just in time for "<a href="http://karrierebibel.de/buero-buero-wo-das-internet-gebaut-wird/">B&uuml;ro, B&uuml;ro</a>", a desk photo carnival.</p>
<p>PS 3: I laughed my ass off when I came into the office, even more than <a href="http://blog.jochen-lillich.de/archives/632-Really,-its-not-a-cow!.html">last time</a>. I love it.</p>
