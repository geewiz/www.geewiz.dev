---
layout: post
title: "On the track"
microblog: false
guid: http://geewiz.micro.blog/2006/12/14/on-the-track.html
date: 2006-12-14T04:45:57+0200
type: post
url: /2006/12/14/on-the-track.html
---

<p>"Auf dem Gleis" -- that's the title of an article about the Catalyst web framework for Perl in the german IT magazine "iX" . That title is a little side blow meaning that you don't necessarily need Ruby on Rails to efficiently get a web application with only a few lines of code.</p>
<p>The article from Jens M. N&ouml;dler and Sebastian Willert gives the reader a quick insight into how Catalyst works. It begins with installation instructions, shows how to code database access and user interaction by means of a small example application and ends with adding the Prototype JavaScript framework to the mix to integrate AJAX functionality.</p>
<p>So, if you're a german speaking Perl developer that's not yet using Catalyst for his web applications, have a look at iX issue January 2007.</p>
