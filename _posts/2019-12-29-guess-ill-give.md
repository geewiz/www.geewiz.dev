---
layout: post
title: "Guess I'll give Dark a watch"
microblog: false
guid: http://geewiz.micro.blog/2019/12/29/guess-ill-give.html
date: 2019-12-29T17:39:06+0200
type: post
url: /2019/12/29/guess-ill-give.html
---

> In addition, German children merrily walk home from school on their own through the forest. Everybody knows that it is extremely unhealthy for Irish children to walk anywhere, especially to or from school, never mind through forests.

The sarcasm is strong in this one. Didn't expect to grin reading an [article by Breda O’Brien](https://www.irishtimes.com/opinion/breda-o-brien-binge-watched-insights-into-teutonic-tv-land-1.4125434). 
