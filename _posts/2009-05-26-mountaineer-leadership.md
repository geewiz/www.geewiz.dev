---
layout: post
title: "Mountaineer Leadership"
microblog: false
guid: http://geewiz.micro.blog/2009/05/26/mountaineer-leadership.html
date: 2009-05-26T04:55:50+0200
type: post
url: /2009/05/26/mountaineer-leadership.html
---

<p>In his German article <a href="http://www.hafawo.at/selbstmanagement-motivation/was-ich-als-fuhrungskraft-durch-bergsteigen-gelernt-habe/">"Was ich als F&uuml;hrungskraft durch Bergsteigen gelernt habe"</a>, Rainer from the HaFAWo blog ("have fun at work and life"), describes how the lessons he learned as an alpinist can also be applied to his work as a manager:</p>
<ol>
<li>Know your goals and their nature, for your job as well as your private life. (= Define your goals.)</li>
<li>Have a map and learn to read it right. (= Have a vision.)</li>
<li>Have a compass. (= Have reliable orientation points.)</li>
<li>Make sure to start at the right time to avoid time-dependend dangers. (= Have good timing.)</li>
<li>Expect the worst and be ready to handle it.</li>
<li>Be prepared regarding your shape, food, clothing and equipment. (= Have everything ready you may need.)</li>
<li>Know your skills and with how much of the impossible you can cope. (= Know your limits.)</li>
<li>If there are problems ahead, you maybe have to resort to teamwork. (= Have a supporting team.)</li>
<li>When problems arise, you depend on your equipment. (= Have reliable tools and know how to use them.)</li>
<li>You should know when to turn your back on the mountain and postpone  summit victory. (Admit defeat in time, try again later.)</li>
</ol>
<p>Thanks for the great analogy, Rainer!</p>
