---
layout: post
title: "OTRS goes ITIL"
microblog: false
guid: http://geewiz.micro.blog/2006/09/14/otrs-goes-itil.html
date: 2006-09-14T17:44:16+0200
type: post
url: /2006/09/14/otrs-goes-itil.html
---

<p>As I found in this <a href="https://www.openbc.com/cgi-bin/forum.fpl?op=showarticles">forum entry</a>&amp;id=2314977&amp;articleid=2314977#2314977 on OpenBC, the two german companies Enterprise Consulting GmbH and <a href="http://www.otrs.de">OTRS GmbH</a> started working on a joint venture in February that aims at extending the free ticket system OTRS to an extensive ITIL tool.<br />
For 2006, they plan on doing first steps towards ITIL certification. Among other features, OTRS is getting role-based access control, transactions and interfaces to 3rd party applications.</p>
<p>Early next year, OTRS GmbH is going to implement the ITIL processes "change management" and "configuration management" based on a central integrated data model, the "configuration management database" (CMDB). Implementation of release management and service delivery processes are following goals.</p>
<p>It's always good to see free software targetting the professional IT management tools market. OTRS had a successful start as a trouble ticket system and has every chance to be a good contender in IT process and service level management.</p>
