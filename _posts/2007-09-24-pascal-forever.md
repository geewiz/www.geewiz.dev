---
layout: post
title: "Pascal forever"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/pascal-forever.html
date: 2007-09-24T22:09:08+0200
type: post
url: /2007/09/24/pascal-forever.html
---

<p>My suggestion to go back to using Pascal for our software development didn't quite catch on. The idea came to me when I read about the <a href="http://svn.freepascal.org/svn/fpcbuild/tags/release_2_2_0/install/doc/whatsnew.txt">recent update to FreePascal</a>.</p>
<p>Ah, the memories... Turbo Pascal 3 on CP/M... TP 4 on PC... Learning Object Oriented Programming with TP 6... The times when people didn't think you're a junkie when you said you used "Turbo Vision"...</p>
<p>It was my entry into professional software development. My first client was a nuclear power plant.</p>
<p>What was the best program you developed in Pascal?</p>
