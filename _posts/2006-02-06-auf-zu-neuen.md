---
layout: post
title: "Auf zu neuen Ufern"
microblog: false
guid: http://geewiz.micro.blog/2006/02/06/auf-zu-neuen.html
date: 2006-02-06T05:29:36+0200
type: post
url: /2006/02/06/auf-zu-neuen.html
---

<p>Es wird Zeit, dass ich mir f&uuml;r morgen die m&ouml;glichen Bahnverbindungen zur neuen Wirkungsst&auml;tte heraussuche. W&auml;hrend ich n&auml;mlich schon seit November <em>f&uuml;r</em> Schlund+Partner arbeite, tue ich das ab morgen nachmittag auch <em>bei</em> Schlund+Partner.Am Freitag hab ich alles, was auf und um meinen Schreibtisch in Durlach herumlag, in Kartons verpackt und hoffe, diese morgen wohlbehalten in der Brauerstra&szlig;e vorzufinden. Arbeit wird auf jeden Fall schon auf mich warten.</p>
<p>Wer noch ein paar Bilder von unseren letzten Tagen im Pfaff-Geb&auml;ude sehen m&ouml;chte, dem sei Andys Blogeintrag <a href="http://blog.bumsdiekuh.net/archives/5-Ciao-Amalienbadstrasse">Ciao Amalienbadstrasse!</a>!.html empfohlen.</p>
