---
layout: post
title: "Using UMTS from Ubuntu Linux"
microblog: false
guid: http://geewiz.micro.blog/2007/01/15/using-umts-from.html
date: 2007-01-15T23:56:00+0200
type: post
url: /2007/01/15/using-umts-from.html
---

<p>Everytime I go on my daily commute, I see people with blinking UMTS/3G cards in their laptops. Because I enjoy using my commute time to reduce my to-do lists, but on the other hand many of my tasks require internet access, I've been rather envious on their connectivity. When I had SUSE 10.0 on my laptop, I couldn't get it to work with my Nokia E61, so I started phantasizing about how a MacBook would improve my mobile productivity.</p>
<p>Having installed Kubuntu 6.06 now, I thought I'd give it another try last week. Et voila: after about half an hour of configuration, the 3G arrows on my smartphone started blinking! (Now guess where I wrote today's first blog entries.)</p>
<p>I'm happy. (Even if there's no real reason to get a MacBook any more.) And to make others happy, too, I updated the most-visited page on my website, my  <a href="http://www.jochen-lillich.de/article/bluetooth-unter-linux.">BlueTooth on Linux HOWTO</a> I kept it in german because there's not as much documentation in my native language than there is in English.</p>
