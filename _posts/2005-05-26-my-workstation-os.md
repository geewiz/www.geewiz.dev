---
layout: post
title: "My Workstation OS: Irix"
microblog: false
guid: http://geewiz.micro.blog/2005/05/26/my-workstation-os.html
date: 2005-05-26T05:07:22+0200
type: post
url: /2005/05/26/my-workstation-os.html
---

<p>Unter diesem Titel schreibt Robert Mertling-Blake, warum er IRIX als Betriebssystem so interessant findet. Der Fazit des kurzen <a href="http://os.newsforge.com/os/05/05/13/131257.shtml?tid=10">Artikels</a> auf Newsforge fasst es zusammen: "However, if care to learn 3D graphics, or want a lightweight desktop that's different and have the time and money to invest in a SGI machine, Irix is certainly worth a try." Dem kann ich nur zustimmen. :)</p>
