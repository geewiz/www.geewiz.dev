---
layout: post
title: "Entschleunigung"
microblog: false
guid: http://geewiz.micro.blog/2005/12/02/entschleunigung.html
date: 2005-12-02T06:19:28+0200
type: post
url: /2005/12/02/entschleunigung.html
---

<p>Am Dienstag fand mich ein kleiner Cartoon von Werner K&uuml;stenmacher: Eine vierspurige Autobahn, per Schilderbr&uuml;cke &uuml;berschrieben mit "Rummel" und ganz links aussen ein Autofahrer mit leicht verzweifeltem Blick in Richtung der Ausf&auml;delspur rechts. An deren Rand 4 Schilder, die nicht eine absteigende Zahl von Schr&auml;gbalken, sondern eine, zwei, drei und vier Kerzen zeigen. Laut Schilderbr&uuml;cke f&uuml;hrt die Ausfahrt nach "Weihnachten".</p>
<p>Ich mag nicht alle Tiki-Cartoons, aber der hat mich echt bewegt: Wenn ich die Spur in Richtung Weihnachten erwischen will, dann muss ich rechtzeitig ein, zwei G&auml;nge runterschalten. Und das letzte Wochenende betrachtend muss ich feststellen, dass ich am Ersten Advent schon mal vorbeigeschossen bin. Es wird Zeit f&uuml;r mich, die Kiste mal ein wenig ausrollen zu lassen und von der Autobahn runterzukommen. Sonst kann es sein, dass ich das Weihnachtsleuchten nur als kurzen Blitz im Seitenfenster erlebe.</p>
