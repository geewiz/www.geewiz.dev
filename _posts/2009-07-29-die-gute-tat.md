---
layout: post
title: "Die gute Tat der Piraten"
microblog: false
guid: http://geewiz.micro.blog/2009/07/29/die-gute-tat.html
date: 2009-07-29T11:36:18+0200
type: post
url: /2009/07/29/die-gute-tat.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p>Kulturpiraten, argumentiert der Musikjournalist Matt Mason in seinem gratis im Netz ver&ouml;ffentlichten Buch &bdquo;The Pirate&rsquo;s Dilemma&ldquo;, sind nicht der Feind. Sie erfinden neue Stile, Technologien und Gesch&auml;ftsmodelle. Ohne ihre Innovationen w&auml;re die heutige Kulturindustrie nicht denkbar.
<p />
<p>Umgekehrt hie&szlig;e das: Mit ihren Ideen wird die morgige denkbar.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.tagesspiegel.de/kultur/Musikpiraten-Musikindustrie-Internet;art772,2857452">tagesspiegel.de</a></div></p>
</div>
