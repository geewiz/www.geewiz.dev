---
layout: post
title: "Unzumutbar"
microblog: false
guid: http://geewiz.micro.blog/2004/11/30/unzumutbar.html
date: 2004-11-30T20:28:00+0200
type: post
url: /2004/11/30/unzumutbar.html
---

<p>Wenn ich heute abend meine dbox2 durch Installation von <a href="http://wiki.tuxbox.org/Neutrino">Neutrino</a> zu neuen Horizonten f&uuml;hre, w&uuml;nsche ich mir noch ein Plugin, das es f&uuml;r den Linux-VDR schon gibt: <a href="http://www.magoa.net/linux/index.php?view=taste">vdr-taste</a> verhindert die versehentliche Anzeige von Sendungen, die man zuvor in eine Sperrliste eingetragen hat. Zappt man auf einen Kanal, auf dem gerade Dschungelcamp oder Big Brother l&auml;uft, erscheint nur die Meldung "Programm unzumutbar" und es wird zum n&auml;chsten brauchbaren Kanal weitergeleitet. Das kann sowohl Nerven als auch Daumen enorm schonen.</p>
<p>Ich f&uuml;rchte nur, dass es Zeiten gibt, in denen das Plugin in eine Endlosschleife ger&auml;t...</p>
