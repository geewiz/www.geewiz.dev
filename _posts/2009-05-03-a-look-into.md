---
layout: post
title: "A look into a Google datacenter"
microblog: false
guid: http://geewiz.micro.blog/2009/05/03/a-look-into.html
date: 2009-05-03T05:26:27+0200
type: post
url: /2009/05/03/a-look-into.html
---

<p>Following up to the recent entry about <a href="http://www.jochen-lillich.de/blog/2009/04/29/googles-homebrew-server-chassis/">Google's custom server chassis</a>, in this video we get a glimpse into one of Google's datacenters where those servers are in operation.</p>
<p>[youtube=http://www.youtube.com/watch?v=bs3Et540-_s&amp;hl=de&amp;fs=1]</p>
<p>Again, Google does it their own way by using separate containers with the necessary infrastructure built in instead of having different rooms for all the racks. The data center building itself delivers power and cooling. I guess it's because of the more modular structure that allows replacement of a complete server "room" why Google chose that approach.</p>
