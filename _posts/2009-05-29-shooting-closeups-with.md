---
layout: post
title: "Shooting closeups with the iPhone"
microblog: false
guid: http://geewiz.micro.blog/2009/05/29/shooting-closeups-with.html
date: 2009-05-30T01:06:24+0200
type: post
url: /2009/05/29/shooting-closeups-with.html
---

<p>If you've tried to do short distance photos with the iPhone, you know that it can't focus any more if the item is only a few centimetres away. I noticed this shortcoming when I had the idea to photograph all my collected business cards and make them searchable in <a href="http://www.evernote.com">Evernote</a>. Their OCR just couldn't make sense of those blurry letters.</p>
<p>But I've found a solution. The <a href="http://p.jochen-lillich.de/amazon/B001GK3S7A">Griffin Clarify</a> case not only protects my iPhone from hits and scratches, but also adds a lens that I slide over the camera window when I'm going to do closeup shots.</p>
<p>With the Clarify, I've got rid of all business cards. I now can search for a name on Evernote and get a sharp photo of the card back. </p>
<p>Goodbye Rolodex. Hello iPhone.</p>
