---
layout: post
title: "Nur B&uuml;rger bedrohen B&uuml;rgerrechte"
microblog: false
guid: http://geewiz.micro.blog/2006/01/16/nur-buumlrger-bedrohen.html
date: 2006-01-17T00:34:44+0200
type: post
url: /2006/01/16/nur-buumlrger-bedrohen.html
---

<p>Viel habe ich Isotopps Blogeintrag <a href="http://blog.koehntopp.de/archives/1128-Noch-mehr-lauschen-Einfach-mal-mithoeren.html">Noch mehr lauschen: Einfach mal mith&ouml;ren</a> nicht hinzuzuf&uuml;gen, ich will ihm nur mehr Gewicht verleihen. Es geht darin um die seltsam einfache Weltsicht des Innenministers von Schleswig-Holstein.</p>
<p>Bedenken gegen&uuml;ber dem Wunsch seines Ministeriums, Abh&ouml;rma&szlig;nahmen auch ohne konkreten Anfangsverdacht durchf&uuml;hren zu d&uuml;rfen, h&auml;lt Ralf Stegner offensichtlich f&uuml;r paranoide Anwandlungen einzelner Datenschutzfanatiker.In einer <a href="http://landesregierung.schleswig-holstein.de/coremedia/generator/Aktueller_20Bestand/IM/Pressemitteilung/060112__im__polizeirecht.html">Pressemitteilung</a> weist er die <a href="http://www.datenschutzzentrum.de/polizei/stellungnahme-lvwg.htm">Kritik des Datenschutzzentrums</a> am entsprechenden Gesetzentwurf des Landes zur&uuml;ck und stellt statt dessen die Gleichung "Polizei = Sicherheit" auf. Auf dieser Grundlage versucht er, die Grundrechte auf Leben und Unversehrtheit gegen die des Schutzes der Privatsph&auml;re und vor &Uuml;berwachung auszuspielen. Seine Argumentation "Was ist das denn f&uuml;r eine Vorstellung, dass Erika Mustermann im Extremfall ihr Leben und ihre Gesundheit bereit sein muss zu opfern, weil dem Straft&auml;ter ein f&uuml;r die Polizei tabuisierter Schutzraum f&uuml;r sein kriminelles Tun zugestanden wird, obwohl Schutz- und Gefahrenabwehr m&ouml;glich w&auml;ren" kann ich nur als FUD(Fear, Uncertainty and Doubt)-Taktik bezeichnen.</p>
<p>Gerade aktuelle Vorkommnisse mit deutschen und amerikanischen Geheimdiensten beweisen, dass eine solche Sicht der Gefahrenlage naiv, ja geradezu dumm ist.</p>
<p>Fragt sich so jemand eigentlich, welcher Idiot diese unsere Sicherheit gef&auml;hrdenden Artikel &uuml;berhaupt in unsere Verfassung aufgenommen hat?</p>
