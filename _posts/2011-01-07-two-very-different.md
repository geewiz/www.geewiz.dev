---
layout: post
title: "Two very different acquisition notices I received today"
microblog: false
guid: http://geewiz.micro.blog/2011/01/07/two-very-different.html
date: 2011-01-07T14:16:00+0200
type: post
images:
- /assets/wp-content/2011/05/socialite-scaled500.jpg?w=300
photos:
- /assets/wp-content/2011/05/socialite-scaled500.jpg?w=300
url: /2011/01/07/two-very-different.html
---

<p>Dimdim:</p>
<blockquote><p><span style="font-family:arial, sans-serif;">Dimdim has been acquired by&nbsp;salesforce.com. Your free Dimdim account will remain active until March 15, 2011. After that date, you will no longer be able to access your free Dimdim account.
<p />Please see the&nbsp;Frequently Asked Questions (FAQ)&nbsp;for additional information.
<p />We appreciate your understanding, and we thank you!</span></p>
<p><span style="font-family:arial, sans-serif;">Pursuant to the Dimdim Terms of Use (the &ldquo;Agreement&rdquo;) governing the use of Dimdim Inc.&rsquo;s (&ldquo;Dimdim&rdquo;) Site and Services (as defined under the Agreement) by you (&ldquo;You&rdquo;), Dimdim is hereby exercising its right to terminate Your Dimdim Account and the Agreement in its entirety. Dimdim will continue to provide Services to you until March 15, 2011. Following March 15, 2011, neither You nor Dimdim shall have any further rights or obligations of any kind under the Agreement, including the right to access the Site, or receive or use any Services. Dimdim thanks you for your business, and wishes you success in the future.</span></p>
</blockquote>
<p><span style="font-family:arial, sans-serif;"><span><span style="font-family:Arial, Helvetica, sans-serif;"><span>Socialite:</span></span></span></span></p>
<p><span style="font-family:arial, sans-serif;"><span><span style="font-family:Arial, Helvetica, sans-serif;"><span>
<div class='p_embed p_image_embed'>
<img alt="Socialite" height="166" src="/assets/wp-content/2011/05/socialite-scaled500.jpg?w=300" width="450" />
</div>
<p>That notice above makes me glad I never used Dimdim for serious work. The other one makes me reconsider giving Socialite another try.</span></span></span></span></p>
