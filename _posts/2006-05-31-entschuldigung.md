---
layout: post
title: "Entschuldigung"
microblog: false
guid: http://geewiz.micro.blog/2006/05/31/entschuldigung.html
date: 2006-05-31T20:56:55+0200
type: post
url: /2006/05/31/entschuldigung.html
---

<p>Die <a href="http://www.rbb-online.de/_/nachrichten/politik/beitrag_jsp/key=news4288272.html">Meldung des rbb</a>, dass der Amokl&auml;ufer von Berlin sich bei seinen Opfern entschuldige, gibt mir Anlass zu einer sprachlichen Betrachtung: kann man sich f&uuml;r so eine Tat entschuldigen? Kann man sich denn &uuml;berhaupt entschuldigen? Nein, kann man gar nicht.<br />
"Ent-schuldigen", also von Schuld entledigen, kann sich der Schuldner nur durch Begleichung seiner Schuld. Ist das nicht m&ouml;glich, liegt die M&ouml;glichkeit zur Entschuldigung einzig und allein bei dem, dem man etwas schuldet. Ein "'tschuldigung" sagt also nicht "Das hier ist meine Entschuldigung", sondern kann nur heissen "Ich bitte dich um Entschuldigung". Und mit der Bitte ist die ganze Angelegenheit noch nicht mal halb durchgestanden. Dazu muss erst noch die Verzeihung folgen. Interessanterweise hat sich das Wort "Verzeihung" nicht zum Reflexiv gewandelt: Anders als "ich entschuldige mich" klingt "ich verzeihe mir" f&uuml;r jeden seltsam.</p>
<p>Dass das Bitten, auch das Bitten um Entschuldigung, heutzutage anscheinend schwerer f&auml;llt denn je, zeigt sich, wenn der Anwalt des Amokl&auml;ufers erkl&auml;rt: "Auch f&uuml;r die schwere Zeit, die die Opfer mit der Aids-Gefahr durchleben m&uuml;ssen, entschuldige er sich." Dass sein Mandant sich da mal nicht t&auml;uscht: ob er daf&uuml;r jemals entschuldigt wird, h&auml;ngt von der F&auml;higkeit und Bereitschaft seiner Opfer, ihm zu verzeihen, ab. Die wird mindestens voraussetzen, dass er seine Tat ehrlich bereut; so manche wird aber auch eine Wiedergutmachung fordern. Hunderprozentig wird diese jedoch gar nicht m&ouml;glich sein -- vor allem nicht, wenn er Opfer mit HIV infiziert hat. Sp&auml;testens dann ist Verzeihung ein Ausdruck extremer G&uuml;te.</p>
<p>BTW: Deshalb ist auch das Entscheidende an der Beichte keineswegs ein lapidar dahingesagtes "'Tschuldigung, lieber Gott.", sondern das liebende -- aber ebenfalls ernsthafte Reue und den Versuch, die eigene Schuld zu begleichen, erwartende -- "Ich verzeihe dir." Gottes.</p>
