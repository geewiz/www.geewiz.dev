---
layout: post
title: "Starke Verben"
microblog: false
guid: http://geewiz.micro.blog/2004/11/27/starke-verben.html
date: 2004-11-27T03:01:00+0200
type: post
url: /2004/11/27/starke-verben.html
---

<p>Als Verfechter einer starken deutschen Sprache erfreue ich mich des Funds einer interessanten Website: die <a href="http://www.soviseau.de/verben">Gesellschaft zur St&auml;rkung der Verben</a> widmet sich der deutschen Sprachvielfalt.</p>
<p>Die Website versteht sich als "Anlaufstelle f&uuml;r schwache und geknochtene[1] Verben aller Sprachen, denen der Weg in die Unregelm&auml;&szlig;igkeit erlirchten[2] werden soll." Zentrales Thema sind, wie unschwer zu erkennen ist, unregelm&auml;&szlig;ige Verben, die als solche in der heutigen Sprache nicht mehr vorkommen[3] -- oder auch bisher gar nicht vorkamen[4].</p>
<p>Nat&uuml;rlich wird auch des Genitivs gedacht, was mich pers&ouml;nlich besonders freut.</p>
<p>Wir halten fest: Auch Sprechen ist (eine) Kunst.</p>
<p>fn1. knechten -- knocht -- geknochen</p>
<p>fn2. erleichtern -- erlircht -- erlirchten</p>
<p>fn3. backen -- buk -- gebacken</p>
<p>fn4. packen -- puk -- gepacken</p>
