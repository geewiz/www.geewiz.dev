---
layout: post
title: "Neue IRIX-Software"
microblog: false
guid: http://geewiz.micro.blog/2005/03/24/neue-irixsoftware.html
date: 2005-03-24T02:55:32+0200
type: post
url: /2005/03/24/neue-irixsoftware.html
---

<p>Die <a href="http://www.nekochan.net/downloads/index.php">Nekoware-Sammlung</a> wird zusehends gr&ouml;&szlig;er und aktueller:</p>
<ul>
<li>neko_aspell-0.60.2.tardist - Rechtschreibpr&uuml;fung</li>
<li>neko_slang-1.4.9.tardist - Programmiersprache</li>
<li>neko_slrn-0.9.8.1.tardist - Newsreader</li>
<li>neko_tetex-3.0.tardist - TeX Textsatz-Umgebung</li>
<li>neko_sylpheed_claws-1.0.3.tardist - E-Mail-Software</li>
<li>neko_sylpheed_claws_gtk2-1.9.6.tardist - GTK-Zusatzpaket f&uuml;r Sylpheed</li>
</ul>
