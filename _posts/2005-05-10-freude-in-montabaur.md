---
layout: post
title: "Freude in Montabaur"
microblog: false
guid: http://geewiz.micro.blog/2005/05/10/freude-in-montabaur.html
date: 2005-05-10T22:25:53+0200
type: post
url: /2005/05/10/freude-in-montabaur.html
---

<p>Leider kam die <a href="http://www.heise.de/newsticker/meldung/59416">Meldung</a> &uuml;ber den Gesch&auml;ftserfolg von United Internet nicht mehr rechtzeitig zum vergangenen Sonntag. Schlie&szlig;lich ist es doch beruhigend, dass es dem k&uuml;nftigen Mutterkonzern gut geht. Obwohl -- Pralinen oder Blumen w&auml;ren da auch nicht passend gewesen.</p>
