---
layout: post
title: "Exorcise your mobile with a microwave"
microblog: false
guid: http://geewiz.micro.blog/2008/05/26/exorcise-your-mobile.html
date: 2008-05-26T18:33:11+0200
type: post
url: /2008/05/26/exorcise-your-mobile.html
---

<p>If sharing the bus with some teenagers hasn't already, this video will finally convince you that <a href="http://www.break.com/index/cell-phones-are-evil.html">cell phones are evil</a>.</p>
<p><a href="http://www.break.com/index/cell-phones-are-evil.html">Cell Phones Are Evil</a> - Watch more <a href="http://www.break.com/">free videos</a></p>
<p>(Via <a href="http://www.nerdcore.de/wp/2008/05/25/ein-handy-in-der-mikrowelle-pure-evil/">Nerdcore</a>)</p>
