---
layout: post
title: "Fr&uuml;hsport"
microblog: false
guid: http://geewiz.micro.blog/2006/01/16/fruumlhsport.html
date: 2006-01-16T21:24:22+0200
type: post
url: /2006/01/16/fruumlhsport.html
---

<p>Minus 11 Grad! Alter Schlappen, momentan ist es ja richtig kalt. Dass sich bei diesen Temperaturen auf einem Auto, das eine Woche lang nicht genutzt wird, eine richtig hartn&auml;ckige Eisschicht bildet, durfte ich heute morgen feststellen -- und beheben. Weil ich heute nach der Arbeit zu einer KAB-Sitzung in Rastatt muss, brauche ich heute n&auml;mlich das Auto. Ich hab in etwa so lange Scheiben und Scheinwerfer gekratzt wie ich normalerweise zum Bahnhof laufe.Wenn ich sehe, dass morgen mit Regen und somit Glatteis gerechnet werden muss, bin ich froh, dass ich inzwischen meine zweite KVV-Monatskarte gekauft habe. &Uuml;berhaupt bin ich mit der Entscheidung, f&uuml;r die Fahrt in die Firma den &Ouml;PNV zu nutzen, sehr zufrieden. Fr&uuml;her hatte ich eine Aversion gegen die Bahn -- wohl, weil ich immer mal auf kalten Bahnh&ouml;fen auf den n&auml;chsten Zug warten musste. Inzwischen hat sich das relativiert, denn Unf&auml;lle oder Glatteis auf der Autobahn vermiesen einem den Individualverkehr genauso. Bei der Bahn bleibt mir zumindest der &Auml;rger &uuml;ber d&auml;mliche Mittelspurfahrer erspart. Und ich kann morgens nach einem Spaziergang an der frischen Luft noch ein wenig d&ouml;sen, lesen oder mich mit Mitfahrern unterhalten. Die Kostenersparnis ist nat&uuml;rlich auch ein Faktor, der mich &uuml;ber manche Inperfektion im Nahverkehr hinwegsehen l&auml;sst. Ich werde mich also mal nach einer Jahreskarte erkundigen.</p>
