---
layout: post
title: "Pronun... Pronouncat... Aussprache-Gedichte"
microblog: false
guid: http://geewiz.micro.blog/2006/04/04/pronun-pronouncat-aussprachegedichte.html
date: 2006-04-04T19:42:31+0200
type: post
url: /2006/04/04/pronun-pronouncat-aussprachegedichte.html
---

<p>Deutsch ist nicht die einzige Sprache, in der es von Ausnahmen und Regelabweichungen wimmelt. F&uuml;r die englische Sprache wird das sehr sch&ouml;n durch die <a href="http://www.spellingsociety.org/news/media/poems.php">Spelling Poems</a> demonstriert. Die auf Anhieb fehlerfrei vorzulesen erfordert wirklich einiges an Erfahrung oder aber blitzschnelles Umdenken, denn hier wird offensichtlich, dass es zwischen Aussprache und Schreibung im Englischen keinerlei feste Zusammenh&auml;nge gibt.</p>
