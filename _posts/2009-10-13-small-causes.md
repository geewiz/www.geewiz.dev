---
layout: post
title: "Small causes"
microblog: false
guid: http://geewiz.micro.blog/2009/10/13/small-causes.html
date: 2009-10-13T22:47:24+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpuploadwikim_bisdf-scaled500.jpg?w=200
- /assets/wp-content/2009/10/media_httpimgzemantac_gbgfu-scaled500.png?w=1
photos:
- /assets/wp-content/2011/05/media_httpuploadwikim_bisdf-scaled500.jpg?w=200
url: /2009/10/13/small-causes.html
---

<div class="zemanta-img" style="display:block;margin:1em;">
<div>
<dl class="wp-caption alignright">
<dt class="wp-caption-dt">
<div class='p_embed p_image_embed'>
<img alt="Media_httpuploadwikim_bisdf" height="450" src="/assets/wp-content/2011/05/media_httpuploadwikim_bisdf-scaled500.jpg?w=200" width="300" />
</div>
</dt>
<dd class="wp-caption-dd zemanta-img-attribution">Image via <a href="http://commons.wikipedia.org/wiki/Image:Uncia_uncia.jpg">Wikipedia</a>
</dd>
</dl>
</div>
</div>
<p>Like every self-respecting Mac user, I upgraded to Snow Leopard as soon as Amazon could deliver the package. Since then, I'd been having the worst WiFi experience of my life. </p>
<p>Every few minutes, the Macbook got disconnected from my home network and would reconnect after about 30-60 seconds. Of course that put an end to successful Timemachine backups to the NAS and to fulfilling evenings in the World of Warcraft. But most annoying were the little problems: </p>
<ul>
<li>iChat dis- and reconnecting regularly.</li>
<li>Music played over Airfoil to the Airport Express in the bedroom breaking up every few seconds.</li>
<li>Marco Polo telling me that there's no evidence of a home network and therefore switching to mobile operation; just to switch back a bit later.</li>
</ul>
<p>Marco Polo is an utility that checks some predefined rules to determine my working environment and changes system settings accordingly. For example, if a certain monitor is attached or if a certain WiFi SSID is visible, Marco Polo would change the default printer and deactivate the screen saver password.</p>
<p>Everything pointed to Snow Leopard as the cause of my problems. After all, the problems started with the upgrade and the computers running Leopard still had fine WiFi reception. On Google, I found a few other people having similar problems, but there was no indication of a serious bug.</p>
<p>I played with the WiFi settings of my router. No success.</p>
<p>I adjusted the router's antennas and removed devices that could jam the signal. No success.</p>
<p>I purchased an Apple Airport Extreme base station, replacing the WiFi part of my router. No success.</p>
<p>Finally, last sunday, I was working on the Macbook when I noticed that WiFi was working okay for quite a while for a change! But there were some applications that hadn't been started at login time. And as soon as I manually started Marco Polo, the WiFi dropouts started again.</p>
<p>WHAT? THE? FUCK?</p>
<p>Yes, not running Marco Polo any more solved my WiFi problems! Connection to the base station: great. Music transmission to the bedroom: back to normal. WoW performance: fine. </p>
<p>My explanation, purely speculative: When Snow Leopard came out, people found out that Marco Polo's WiFi network detection didn't work any more. But soon, one user put out a patched version that closed that gap. And it looks like that patch somehow disturbs the WiFi driver, causing the dropouts.</p>
<p>Weeks of anger and despair could have been avoided just by quitting a simple application. Into this blog post I put my hope to save another desperate Snow Leopard user's money and mental health.</p>
<div class="zemanta-pixie" style="margin-top:10px;height:15px;">
<div class='p_embed p_image_embed'>
<img alt="Media_httpimgzemantac_gbgfu" height="1" src="/assets/wp-content/2009/10/media_httpimgzemantac_gbgfu-scaled500.png?w=1" width="1" />
</div>
<p><span class="zem-script more-related pretty-attribution"></span>
</div>
