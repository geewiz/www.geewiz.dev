---
layout: post
title: "ORACLE ASM ist nicht immer gut"
microblog: false
guid: http://geewiz.micro.blog/2005/09/01/oracle-asm-ist.html
date: 2005-09-01T07:03:53+0200
type: post
url: /2005/09/01/oracle-asm-ist.html
---

<p>Im Artikel <a href="http://www.linuxjournal.com/article/8539">Optimizing Oracle 10g on Linux: Non-RAC ASM vs. LVM</a> beschreibt Bert Scalzo im Linux Journal, dass es sinnvoll sein kann, ext3 und LVM zu verwenden, wenn man ORACLE nicht im Cluster betreibt. Gegen&uuml;ber dem ORACLE-eigenen ASM (Automatic Storage Management) haben die Linux-Bordmittel offenbar noch einen Leistungs- und somit Skalierungsvorteil: "Therefore, for people not doing RAC who care more about performance than administrative ease, for now you should stick with the Linux filesystems and an LVM."</p>
