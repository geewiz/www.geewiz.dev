---
layout: post
title: "A short intro to Web 2.0"
microblog: false
guid: http://geewiz.micro.blog/2006/07/03/a-short-intro.html
date: 2006-07-04T00:25:53+0200
type: post
url: /2006/07/03/a-short-intro.html
---

<p>Last week, I gave a talk about examples and principles of Web 2.0 technology. If someone is interested, the slides (in German) are <a href="http://www.jochen-lillich.de/static/vortrag/web20/.">online</a></p>
