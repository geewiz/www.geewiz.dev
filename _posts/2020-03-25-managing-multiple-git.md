---
layout: post
title: "Managing multiple Git identities"
microblog: false
guid: http://geewiz.micro.blog/2020/03/25/managing-multiple-git.html
date: 2020-03-25T23:49:00+0200
type: post
categories:
- "DevOps"
- "DEV"
tags: [devops]
url: /2020/03/25/managing-multiple-git.html
---
I've always been struggling to use the right name and email address, separate between work and personal projects, for each of my Git repositories. Micah Henning solved that problem nicely by removing the global settings but making a repo-specific configuration mandatory. And a handy alias is the icing on the [Git identity selection](https://www.micah.soy/posts/setting-up-git-identities/) cake.
