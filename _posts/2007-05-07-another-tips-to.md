---
layout: post
title: "Another 2 tips to get Twitter on your cellphone for free"
microblog: false
guid: http://geewiz.micro.blog/2007/05/07/another-tips-to.html
date: 2007-05-07T04:34:03+0200
type: post
url: /2007/05/07/another-tips-to.html
---

<p>Om Malik just published his three tips over on <a href="http://webworkerdaily.com/2007/05/06/3-tips-to-get-twitter-on-your-cellphone-for-free/.">Web Worker Daily</a> But while I found the new <a href="http://m.twitter.com">Twitter Mobile website</a> a nice compromise for small screens, I don't think RSS feeds are the best alternative. Having to subscribe to every single one of all my Twitter friends' feeds is just too much effort for me. I think you should rather look for solutions that work with your original friend list.</p>
<p>Here are my suggestions:</p>
<p>First, there's always the IM channel. So, if you own a smartphone that has some Instant Messaging Software available (AFAIK that's at least true for S60, Windows Mobile and Palm), you can receive and send tweets this way.</p>
<p>And if you really want to get fancy, get a specialised Twitter software for mobile phones. If your phone is J2ME capable, you could try out <a href="http://www.madpilot.com.au/twitteresce">Twitteresce</a>, for example.</p>
<p>At the moment, I use Twitteresce and the Twitter Mobile website in turns to find out which suits me better. What's your favourite way of accessing Twitter when you're out under the big, yellow, evil ball?</p>
