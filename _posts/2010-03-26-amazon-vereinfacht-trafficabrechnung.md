---
layout: post
title: "Amazon vereinfacht Traffic-Abrechnung"
microblog: false
guid: http://geewiz.micro.blog/2010/03/26/amazon-vereinfacht-trafficabrechnung.html
date: 2010-03-26T10:20:45+0200
type: post
url: /2010/03/26/amazon-vereinfacht-trafficabrechnung.html
---
<p>
    Amazon Web Services hat die Traffic-Abrechnung ihrer Cloud vereinfacht. Heute morgen kam die Mitteilung per E-Mail, dass der Traffic aller Dienste (S3, EC2 usw.)&nbsp;jetzt&nbsp;nicht mehr einzeln, sondern in Summe abgerechnet wird.
<p />
<div>Das vereinfacht nicht nur die Abrechnung f&uuml;r Amazon, sondern hat auch Vorteile f&uuml;r die Kunden:
<p />
<blockquote class="webkit-indent-blockquote" style="border:none;margin:0 0 0 40px;padding:0;">"Because AWS is now aggregating your total Data Transfer Out usage across multiple services, you can reach higher usage tiers and lower pricing more quickly. In addition, you&#039;ll benefit from a complimentary tier which provides your first GB of outbound transfer in each Region each month at no charge."</p></blockquote>
<p>Allerdings sei erw&auml;hnt, dass die "higher usage tiers" im mehrstelligen TB-Bereich liegen.
<p />Wenn es Amazon jetzt noch schafft, eine EU-konforme Rechnung zu schreiben, bin ich zufrieden.</div>
