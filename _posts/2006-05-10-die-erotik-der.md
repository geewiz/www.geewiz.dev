---
layout: post
title: "Die Erotik der Rhetorik"
microblog: false
guid: http://geewiz.micro.blog/2006/05/10/die-erotik-der.html
date: 2006-05-10T20:33:25+0200
type: post
url: /2006/05/10/die-erotik-der.html
---

<p>Das "MAGAZIN der Hochschule Karlsruhe" vom Sommersemester berichtet in einem Artikel &uuml;ber die Gastprofessur Prof. Dr. Michael Thieles an der Universit&auml;t Krakau zum Thema "Rhetorik/Homiletik". Eine interessante Note gab er der Kunst guten Vortragens respektive Predigens durch eine ethymologische Erkenntnis:</p>
<blockquote class="posterous_medium_quote">
<p>Sichtlich geschockt oder zumindest zu aufgeregten Gespr&auml;chen angeregt habe ich die Kollegen durch den Hinweis, dass ihre traditionsreiche Wissenschaft, die Homiletik, deren Name sich vom griechischen Verb "homil&eacute;o" ableitet, stark erotische Komponenten hat; das weist das Verb durch seine Nebenbedeutungen aus: "homil&eacute;o" hei&szlig;t nicht nur "Umgang haben mit jemandem", sondern auch "Geschlechtsverkehr haben mit jemandem". [...] Rhetorik bedeutet dann tats&auml;chlich in ihrer sch&ouml;nsten Form: Liebe machen mit dem Publikum. Der Funke muss &uuml;berspringen.</p>
</blockquote>
<p>Ich f&auml;nde es sch&ouml;n, wenn sich mehr Vortragende dieser Sichtweise bewusst w&uuml;rden. Bei vielen Ansprachen und Predigten steigt der Redner ohne Vorgepl&auml;nkel sofort voll in die Sache ein, um dann entweder mit seinen Inhalten auf mich einzuh&auml;mmern oder aber so planlos hin- und herzugaloppieren, dass man sich fragt, wo er eigentlich hin will.</p>
<p>Gibt es auch ein griechisches Wort f&uuml;r "rhetorische Vergewaltigung"? ;-)</p>
