---
layout: post
title: "Wilkinson goes viral"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/wilkinson-goes-viral.html
date: 2007-09-24T22:10:23+0200
type: post
url: /2007/09/24/wilkinson-goes-viral.html
---

<p>This is how you do internet marketing: take a common saying, create a story from it, make it a cute and funny video referring people to a game on your corporate website and set it free.</p>
<p>People like me (who got it from <a href="http://www.theofel.de/archives/2007/09/werbung-wie-sie-sein-sollte.html">Jan Theofel</a>) do the rest.</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=LuSBCIV1zuQ])</p>
