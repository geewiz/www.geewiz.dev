---
layout: post
title: "It's expensive to live in Dublin"
microblog: false
guid: http://geewiz.micro.blog/2006/06/29/its-expensive-to.html
date: 2006-06-29T08:06:39+0200
type: post
url: /2006/06/29/its-expensive-to.html
---

<p>Maybe I should rethink my plans of moving to Ireland. At least Dublin is among the 20 most expensive cities in the world to live in. According to the <a href="http://www.smh.com.au/news/world/top-50-cities/2006/06/26/1151174117013.html">Sydney Morning Herald</a>, Dublin holds place no. 18, following Paris and Singapore.</p>
<p>Curiously, not one German city is among the top 50!?</p>
