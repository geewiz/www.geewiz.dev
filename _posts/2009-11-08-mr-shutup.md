---
layout: post
title: "Mr. Shut-up"
microblog: false
guid: http://geewiz.micro.blog/2009/11/08/mr-shutup.html
date: 2009-11-08T23:41:49+0200
type: post
images:
- http://media.tumblr.com/tumblr_kslwv0reLt1qzgz2l.jpg
photos:
- http://media.tumblr.com/tumblr_kslwv0reLt1qzgz2l.jpg
url: /2009/11/08/mr-shutup.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p>Pro parenting tip, and this is one that was all my friend&rsquo;s idea but I thought it was brilliant &ndash; Meet <a href="http://www.ikea.com/us/en/catalog/products/90064441">Mr. Shut-up</a>:</p>
<p><img src="http://media.tumblr.com/tumblr_kslwv0reLt1qzgz2l.jpg" /></p>
<p>If you ever work out of the home, and you have kids, then you know that sometimes you may need to take a call and not have it sound as if you are working out of your home with kids. That is where Mr. Shut-up comes in. Let your critters know that, when Mr. Shut-up is on, it is time to be quiet or face the wrath of Mr. or Ms. Puts-a-roof-over-your-head.</p>
</blockquote>
<div class="posterous_quote_citation">via <a href="http://minimalmac.com/post/233272057/hotel-at-home-minimizing-the-workspace">minimalmac.com</a></div></p>
</div>
