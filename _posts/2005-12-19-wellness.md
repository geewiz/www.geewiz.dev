---
layout: post
title: "80% Wellness"
microblog: false
guid: http://geewiz.micro.blog/2005/12/19/wellness.html
date: 2005-12-19T19:27:07+0200
type: post
url: /2005/12/19/wellness.html
---

<p>Den f&uuml;r Samstag geplanten Besuch der Caracalla-Therme mussten wir ja auf Sonntag verschieben. Dieser somit f&uuml;r Erholung reservierte Tag fing aber erst mal stressig an, weil der Wecker um 8 Uhr <em>irischer Zeit</em> klingelte. Also eine Stunde vor Caros Shiatsu-Gesichtsmassage um 10 Uhr. Naja, wir schlugen dennoch rechtzeitig in Baden-Baden auf. Der Rest des Tages war relaxt, aber mit Abstrichen.Der selbe Therapeut, bei dem Caro ihr Shiatsu erhielt, gab mir sp&auml;ter noch eine R&uuml;ckenmassage. Beides hatte eher Krankengymnastik-Charme, besonders verw&ouml;hnt f&uuml;hlten wir uns nicht. Hier gibt es offenbar Unterschiede im Personal, denn ihre Fu&szlig;reflexzonenmassage konnte Carolin mehr genie&szlig;en.</p>
<p>Die Caracalla-Therme hat zwar eine ausgedehnte Saunalandschaft, aber selbst die reicht leider nicht aus, um den vielen Wochenend-Besuchern gro&szlig;z&uuml;gig Platz zu bieten. Und in der Menge macht sich dann auch der Prozentsatz an denjenigen bemerkbar, die glauben, dass Saunaregeln allgemein &uuml;berbewertet werden. Ich war wenig begeistert, in der Ruhesauna russischer Konversation zu lauschen, und bei manchen franz&ouml;sischen G&auml;sten hat sich der Unterschied zwischen G&auml;ste- und Saunahandtuch offensichtlich auch noch nicht herumgesprochen.</p>
<p>Trotz alldem war der Tag insgesamt erholsam und dank Geschenkgutschein nicht besonders kostspielig. Dass Armin mich jedoch nicht nur vor obigen Wermutstropfen gewarnt hat, sondern auch am gleichen Wochenende deutlich mehr <a href="http://blog.helaron.de/archives/247-WellnessTour-geht-weiter....html">Saunaspa&szlig;</a> gehabt hat, hinterl&auml;sst dann doch etwas Bitterkeit.</p>
