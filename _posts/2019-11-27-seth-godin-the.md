---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2019/11/27/seth-godin-the.html
date: 2019-11-27T15:17:43+0200
type: post
url: /2019/11/27/seth-godin-the.html
---
[Seth Godin](https://seths.blog/2019/11/the-google-tax/): "The existence of DuckDuckGo doesn’t significantly change Google’s position as a monopoly able to dictate how most people experience everything on the web."

...yet. 😃
