---
layout: post
title: "Never leave the playground"
microblog: false
guid: http://geewiz.micro.blog/2013/06/26/never-leave-the.html
date: 2013-06-26T12:10:58+0200
type: post
url: /2013/06/26/never-leave-the.html
---
<p>On <a href="http://www.presentationzen.com/presentationzen/2013/04/the-key-to-life-is-to-never-leave-the-playground.html">Presentation Zen</a>, a blog I've been following for years to improve my conference sessions, I found a post about Stephen Jepson. He's 72 years old, a retired college professor, and he claims that to keep being playful and moving "is the single most important thing to do to be physically healthier and smarter, regardless of age."</p>
<p>His story is very inspiring, so go on and read the <a href="http://www.presentationzen.com/presentationzen/2013/04/the-key-to-life-is-to-never-leave-the-playground.html">article</a> and watch the video clips!</p>
<p>My children have brought me back onto the playground and in teaching me how to have fun playing, they make me a very happy man.</p>
