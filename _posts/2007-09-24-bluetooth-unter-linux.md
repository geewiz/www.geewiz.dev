---
layout: post
title: "BlueTooth unter Linux"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/bluetooth-unter-linux.html
date: 2007-09-24T22:02:31+0200
type: post
url: /2007/09/24/bluetooth-unter-linux.html
---

<p>(This is a German article from my old homepage.)</p>
<h2>Einf&uuml;hrung</h2>
<p>BlueTooth ist ein Standard zur Daten&uuml;bertragung per Funk, speziell ausgelegt f&uuml;r geringe Reichweiten. Diese Technologie erm&ouml;glicht direkte drahtlose Kommunikation zwischen Handys und Headsets, aber auch zwischen PDAs bzw.  Notebooks und Handys. Letztere k&ouml;nnen dann als Modem zur Internet-Anbindung im Mobilfunknetz verwendet werden.</p>
<p>F&uuml;r den LinuxKernel gibt es f&uuml;r BlueTooth ein Treiberpaket namens<br />
<a href="http://bluez.sourceforge.net/">Bluez</a>. Bluez ist inzwischen Bestandteil g&auml;ngiger Linux-Distributionen.</p>
<p>Um einen PC oder Notebook BT-f&auml;hig zu machen, eignen sich USB-Dongles am besten. Dies sind kleine Stecker f&uuml;r den USB-Port, die die ganze n&ouml;tige Hardware enthalten. Es sind aber auch PCMCIA-Karten erh&auml;ltlich und viele Laptops wie der Lenovo R52 haben BT auch bereits eingebaut.</p>
<p>Welche BT-Ger&auml;te unter Linux unterst&uuml;tzt werden, k&ouml;nnen Sie bei <a href="http://www.holtmann.org/linux/bluetooth/devices.html">Marcel Holtmann</a> nachlesen.</p>
<h2>Installation</h2>
<p>Bei allen Distributionen ist BlueTooth-Unterst&uuml;tzung inzwischen bereits im mitgelieferten Standard-Kernel integriert.</p>
<p>Neben den Kernelmodulen sind ggf. noch die folgenden Pakete zu installieren:</p>
<ul>
<li>bluez-libs</li>
<li>bluez-utils</li>
<li>bluez-sdp</li>
<li>bluez-pan</li>
</ul>
<p>Sind in der jeweiligen Linux-Distribution die aktuellen Pakete nicht enthalten, ist der <a href="http://bluez.sourceforge.net/download/download.html">Download von der Bluez-Website</a> sinnvoll.</p>
<h2>Inbetriebnahme</h2>
<h3>Konfiguration und Starten des HCI-Daemons</h3>
<p>Der "Bluetooth Host Controller Interface"-Daemon (hcid) ist der zentrale BT-Dienst. </p>
<p>Seine Konfiguration ist in der Datei <code>/etc/bluetooth/hcid.conf abgelegt.</code></p>
<p>Der hcid wird von root mit einem der Kommandos</p>
<div class="CodeRay">
<div class="code">
<pre>$ /etc/init.d/hcid start
$ /etc/init.d/bluez-utils start</pre>
</div>
</div>
<p>aktiviert. Nach &Auml;nderungen an der Konfiguration muss er mit dem Argument "restart" statt "start" neu initialisiert werden.</p>
<h3>Erster Test</h3>
<p>Mit dem Kommando <code>hcitool scan</code> sucht der Rechner nach erreichbaren BT-Ger&auml;ten. Ein BT-f&auml;higes Handy (zB Nokia 6310, Ericson T68i) sollte hierzu mit aktiviertem BT-Empfang bereitliegen.  Andere BT-Ger&auml;te wie zB ein Headset sollten vom Handy abgekoppelt sein, um St&ouml;rungen zu vermeiden.  Als Ausgabe liefert <code>hcitool scan</code> eine Liste der gefundenen Ger&auml;te mit einer Ger&auml;teadresse und ihrem Namen:</p>
<div class="CodeRay">
<div class="code">
<pre>Scanning ...
00:60:48:6D:5A:D3    Nokia 6310i</pre>
</div>
</div>
<h2>BT-Handy als Modem verwenden</h2>
<h3>Konfiguration des hcid</h3>
<p>F&uuml;r die Nutzung eines BT-Ger&auml;ts als Modem (z.B. ein UMTS-Handy) ist es wichtig, im device-Block die richtige Ger&auml;teklasse zu setzen:</p>
<div class="CodeRay">
<div class="code">
<pre>class 0x100100;</pre>
</div>
</div>
<h3>Kopplung</h3>
<p>Das Kommando</p>
<div class="CodeRay">
<div class="code">
<pre>$ rfcomm bind 0 00:60:48:6D:5A:D3</pre>
</div>
</div>
<p>&ouml;ffnet eine Verbindung vom Rechner zum Handy ("Pairing", s.u.) anhand dessen Ger&auml;teadresse.</p>
<p>Eine feste Zuordnung der Ger&auml;te erfolgt in <code>/etc/bluetooth/rfcomm.conf</code>. Damit wird direkt beim Zugriff auf die Ger&auml;tedatei (i.d.R. <code>/dev/rfcomm0</code>) die Kopplung ausgel&ouml;st.</p>
<div class="CodeRay">
<div class="code">
<pre>rfcomm0 {
  bind yes;
  # Bluetooth address of the phone
  device 00:12:D1:7D:33:A9;
  # RFCOMM channel for the connection
  channel 2;
  # Description of the connection
  comment "UMTS dial-up";
}</pre>
</div>
</div>
<h3>Pairing</h3>
<p>Beim "Pairing" nehmen den beiden Ger&auml;te Kontakt miteinander auf. Beim Erstkontakt muss sich der Rechner mit einer mindestens vierstelligen PIN-Nummer authentifizieren.</p>
<p>Die PIN-Nummer muss in der Datei <code>/etc/bluetooth/pin</code> abgelegt werden.</p>
<p>In KDE eignet sich zur Abfrage der PIN am besten das Programm kbluepin, das in <code>/etc/bluetooth/hcid.conf</code> wie folgt eingetragen werden muss:</p>
<div class="CodeRay">
<div class="code">
<pre>pin_helper /usr/lib/kdebluetooth/kbluepin</pre>
</div>
</div>
<p>Den Erstkontakt l&ouml;st man einfach durch ein kurzes Modemkommando aus:</p>
<div class="CodeRay">
<div class="code">
<pre>$ echo "ATZ" > /dev/rfcomm0</pre>
</div>
</div>
<p>Nun ist auf dem Handy die PIN einzugeben. F&uuml;r die Zukunft erspart man sich die Eingabe, indem man die erfolgreiche Paarung im Handy speichern l&auml;sst. Dies geschieht bei manchen Handies (Nokia 6310) automatisch, bei anderen (Ericsson T68i) muss es manuell im Men&uuml; ausgew&auml;hlt werden.</p>
<h3>Internet-Anbindung</h3>
<p>Ist das Pairing erst einmal vollzogen, kann <code>/dev/rfcomm0</code> wie eine gew&ouml;hnliche Modem-Schnittstelle genutzt werden. Mit den &uuml;blichen Mitteln kann also dar&uuml;ber eine PPP-Verbindung ins Internet aufgebaut werden.</p>
<p>In Ubuntu Linux habe ich f&uuml;r den UMTS-Zugang in <code>/etc/ppp/peers/umts</code> folgende Daten abgelegt:</p>
<div class="CodeRay">
<div class="code">
<pre>hide-password
noauth
connect "/usr/sbin/chat -v -f /etc/chatscripts/umts"
debug
/dev/rfcomm0
460800
defaultroute
noipdefault
user "vodafone"
remotename nokia-e61
ipparam nokia-e61
usepeerdns</pre>
</div>
</div>
<p>Das passende Chatscript liegt in <code>/etc/chatscripts/umts</code>:</p>
<div class="CodeRay">
<div class="code">
<pre># ispauth PAP
 # abortstring
 ABORT BUSY ABORT 'NO CARRIER' ABORT VOICE ABORT 
 'NO DIALTONE' ABORT 'NO DIAL TONE' ABORT 'NO ANSWER' ABORT DELAYED
 # modeminit
 '' "ATZ"
 OK "AT+CGDCONT=1,42IP42,42web.vodafone.de42"
 # ispnumber
 OK-AT-OK "ATDT*99***1#"
 # ispconnect
 CONNECT dc
 # prelogin

 # ispname
 # isppassword
 # postlogin</pre>
</div>
</div>
<p>"web.vodafone.de" ist dabei der "Access Point Name", der von Provider zu Provider unterschiedlich ist.</p>
<p>Da ein Login nicht notwendig ist, habe ich in <code>/etc/ppp/pap-secrets</code> f&uuml;r Benutzername und Passwort Dummywerte eingetragen:</p>
<div class="CodeRay">
<div class="code">
<pre>"vodafone" nokia-e61 "password"</pre>
</div>
</div>
<p>Eine UMTS-Verbindung kann nun ganz einfach mit dem Kommando <code>pon umts</code> auf- und mit <code>poff</code> wieder abgebaut werden.</p>
<h2>Vernetzung</h2>
<p>Mittels der "Private Area Network"-Funktion (PAN) k&ouml;nnen Rechner per Funk vernetzt werden. Dazu wird mit dem Kommando</p>
<div class="CodeRay">
<div class="code">
<pre>$ pand -s -r NAP -M</pre>
</div>
</div>
<p>ein Rechner zum "Network Access Point" und "Master" ernannt, der zun&auml;chst auf den Verbindungsaufbau durch die Gegenstation wartet. Diese baut die Verbindung zum Master anhand dessen Ger&auml;teadresse wie folgt auf:</p>
<div class="CodeRay">
<div class="code">
<pre>$ pand --service NAP -b aa:bb:cc:dd:ee:ff</pre>
</div>
</div>
<p>Man kann auch statt zu einer definierten Ger&auml;teadresse die Verbindung einfach zum erstbesten BT-Ger&auml;t aufbauen:</p>
<div class="CodeRay">
<div class="code">
<pre>$ pand --service NAP -Q</pre>
</div>
</div>
<p>Nach erfolgreichem Verbindungsaufbau existiert auf beiden Stationen ein neues Netzwerk-Interface namens <code>bnep0</code>, das wie &uuml;blich mit <code>ifconfig</code> konfiguriert werden kann:</p>
<div class="CodeRay">
<div class="code">
<pre>$ ifconfig bnep0 192.168.0.1 netmask 255.255.255.0 broadcast 192.168.0.255 up</pre>
</div>
</div>
<h2>Spezielle BlueTooth-Ger&auml;te</h2>
<h3>AVM Blue Fritz! USB</h3>
<p>Zur Anbindung ihrer BlueTooth-Produkte unter Linux hat AVM eine eigene <a href="http://www.avm.de/de/Service/AVM_Service_Portale/Linux/FAQs_Support/bluefritz_linux_neu.html">FAQ-Sammlung</a> online, bei SuSE findet sich eine zus&auml;tzliche <a href="http://portal.suse.com/sdb/de/2003/10/bluetooth_avm.html">Anleitung</a>.</p>
