---
layout: post
title: "Digg The Code"
microblog: false
guid: http://geewiz.micro.blog/2007/05/03/digg-the-code.html
date: 2007-05-04T00:42:10+0200
type: post
images:
- /uploads/codecollage.jpg
photos:
- /uploads/codecollage.jpg
url: /2007/05/03/digg-the-code.html
---

<p>I don't give much about the whole hoopla around the cracked HD-DVD code. Kevin's reaction actually was the only right thing to do: By taking sides with the Digg mob he will either be the Knight in Shining Armor or the Martyr for the Cause -- a hero in both cases.</p>
<p>But what's really interesting is the many creative ways people spread The Code. Take, for example, the <a href="http://www.wired.com/entertainment/hollywood/multimedia/2007/05/crackkdown_protest:">Wired Code Photo Gallery</a></p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/codecollage.jpg" />
</div></p>
<p>I also like Geoff Smith's song (and not only because of Cali's bouncing around):</p>
<p></p>
<p>Force by sheer mass doesn't impress me much. But creativity always does.</p>
