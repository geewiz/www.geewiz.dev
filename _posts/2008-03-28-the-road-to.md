---
layout: post
title: "The road to mediocrity"
microblog: false
guid: http://geewiz.micro.blog/2008/03/28/the-road-to.html
date: 2008-03-28T04:13:00+0200
type: post
url: /2008/03/28/the-road-to.html
---

<p>If you wonder how that noob that just had your instance group wipe once again could get to 70 at all, it may be because you and others ran them through many prior instances. In their "Officers' Quarters" column, WOW Insider posted the article <a href="http://www.wowinsider.com/2008/03/24/officers-quarters-the-road-to-mediocrity/">The road to mediocrity</a> which I agree with very much.</p>
<p>I always resisted the temptation of getting a boost from a high-level, even during the time I was supposed to get my character quickly to my group's common level. Not only did I find it boring running through an instance after an level 70 clearing the way, I also knew that I'd later need that experience from all those various encounters. Why should I pay my monthly fee while missing all that gameplay on purpose?</p>
<p>So, I wholeheartedly second the suggestion the WoW Insider article gives to guilds: don't take away the opportunities of learning from your low levels by running them through instances.</p>
