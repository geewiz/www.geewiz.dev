---
layout: post
title: "Why I chose Obsidian over Notion for my PKM"
microblog: false
guid: http://geewiz.micro.blog/2021/01/04/why-i-chose.html
date: 2021-01-04T19:11:05+0200
type: post
url: /2021/01/04/why-i-chose.html
---
After posting my recent [weeknote](/2020/12/14/weeknote.html) mentioning that I use Obsidian for my personal knowledge management, my former teammate Andy asked why I didn't use Notion like we do for our company wiki. I had to think for a minute and decided that I'd answer his question in another post.

Let me start by saying that these decisions are usually not clear-cut and a matter of preference. I could have used Notion for my PKM with ease and success. The reason I didn't comes down to the different weights I subjectively give each product's pros and cons.

Notion's greatest strength is that it not only has a nested page structure like many other wiki-like systems. The feature it calls "databases" lets you build collections of pages with added custom metadata. You can use this metadata to filter and sort the collection, and you can save multiple filter and sorting combinations as "views". Metadata can be aggregated to calculate counts, sums and averages. It even allows referencing other databases in a "foreign key" manner, which opens the way to complex multi-database data sets, with the single page still as its atomic building block.

Obsidian is much simpler. It's a note editor for Markdown files. Its central feature is that it extends the Markdown standard to support the simple linking of files by surrounding the file title in double brackets. In file A, you can reference `[[file B]]`, and when you view file B, Obsidian will display in a side pane that there's an incoming link from file A. In other words, Obsidian automates bi-directional linking of Markdown documents. These "backlinks" are one of the core attributes of modern knowledge management. Notion recently added a feature that works similarly. (By the way, I don't understand why it took us so long since the invention of Hypertext to realise the value backlinks have for PKM.)

You don't need to know more about Obsidian to use it effectively. And that's one of the reasons I prefer it for my managing my personal notes. I was able to import my existing Markdown notes with ease and didn't have any learning curve to master.

Another important plus of Obsidian is that it's an application running on my desktop machine. It's fast. I don't have to open a browser, click a bookmark and wait for the website to open, as is the case with Notion. I switch to Obsidian and start working.

The Notion team had an easier time to provide mobile apps for their web application. While it took them a while to make them work decently, the Obsidian team doesn't even have any mobile application on offer yet. (It's one of the major goals on their roadmap, though.) That doesn't mean that I can't access my notes from my iPhone or iPad. All I need is a cloud sync service (I'm using iCloud Drive) and a Markdown editor application. At the moment 1Writer seems to be the preferred iOS app because of its support for double-bracket links.

Since Obsidian data is just a bunch of Markdown files, I can use alternative tools even on my desktop machine. I could use another Markdown editor; I don't because Obsidian is fine. I can use git to have versioned backups of my files. I can even use Ruby scripts to do maintenance or analysis tasks.

While Notion can _also_ be used for taking, managing and interlinking notes, this is Obsidian's core purpose. That's why Obsidian offers features that probably didn't have priority for the Notion developers. For example, on top of the backlinks feature, Obsidian offers a graph view that visualises the relationships between notes and allows for easy navigation across links.

What seals the deal for me is that Obsidian has a great user community where many people share their PKM practices and Obsidian experience. They also share CSS themes and plugins that extend Obsidian's core functionality and personalise its look-and-feel.

TL;DR: I use Obsidian because it's like an IDE for notes, a tool that allows me to do my daily knowledge management efficiently, without technology getting in the way.
