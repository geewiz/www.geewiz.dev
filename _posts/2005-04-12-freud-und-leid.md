---
layout: post
title: "Freud und Leid"
microblog: false
guid: http://geewiz.micro.blog/2005/04/12/freud-und-leid.html
date: 2005-04-12T02:29:06+0200
type: post
url: /2005/04/12/freud-und-leid.html
---

<p>Dass Mediacontrol dem Produzenten der S&auml;ngerin Gracia einen Schuss vor den Bug gibt, ist zwar ein guter Zug, w&auml;re mir aber f&uuml;r sich allein keine Erw&auml;hnung wert. Wenn ich im darauf bezogenen <a href="http://www.spiegel.de/kultur/gesellschaft/0">Spiegel-Artikel</a>,1518,350752,00.html aber Gracia mit den Worten "Im Enddefekt hat mich Deutschland gew&auml;hlt" zitiert finde, dann frage ich mich schon, ob es sich dabei nicht um einen freudschen Verschreiber handelt und die K&uuml;nstlerin in Wirklichkeit weit weniger kritisch vom "Endeffekt" sprach. :)</p>
