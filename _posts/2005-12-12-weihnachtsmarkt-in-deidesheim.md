---
layout: post
title: "Weihnachtsmarkt in Deidesheim"
microblog: false
guid: http://geewiz.micro.blog/2005/12/12/weihnachtsmarkt-in-deidesheim.html
date: 2005-12-12T22:52:01+0200
type: post
url: /2005/12/12/weihnachtsmarkt-in-deidesheim.html
---

<p>Gestern haben mich Kai, Heike, Steffi und Schlumpf zum Weihnachtsmarkt in Deidesheim mitgenommen. Dass der Markt sich durch eine h&uuml;bsche gepflasterte Gasse zieht und das Angebot sch&ouml;n gemischt ist, macht ihn weitaus weihnachtlicher als die M&auml;rkte, f&uuml;r die man vorwiegend Pizza- und D&ouml;nerbuden auf einem Marktplatz zusammenpfercht und die Weihnachts-CD auf Autorepeat stellt. Vor allem das Kunsthandwerk pr&auml;sentiert dort wirklich sch&ouml;ne Dinge, die allerdings auch preislich h&ouml;her angesiedelt sind als blinkende Weihnachtsmannm&uuml;tzen. In Deidesheim darf man ausserdem davon ausgehen, dass der Gl&uuml;hwein nicht aus dem Tetrapack kommt, und es gibt ihn auch in 0,5l-Gl&auml;sern. Mit gutem Gewissen kann ich sagen, dass ich nicht mehr als 2 Gl&auml;ser getrunken habe. :)</p>
<p>Wir hatten einen sehr sch&ouml;nen Nachmittag, allerdings dr&auml;ngten uns die <a href="http://portale.web.de/Boulevard/Comics/Perscheid/msg/6023259/6023258/1/">Umst&auml;nde</a> schlie&szlig;lich doch intensiv zum Auto.</p>
