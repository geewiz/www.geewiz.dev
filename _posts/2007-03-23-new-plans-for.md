---
layout: post
title: "New plans for Highrise"
microblog: false
guid: http://geewiz.micro.blog/2007/03/23/new-plans-for.html
date: 2007-03-23T02:04:00+0200
type: post
url: /2007/03/23/new-plans-for.html
---

<p>The guys at <a href="http://www.37signals.com">37signals</a> not only have an eye for details, but also an ear for their customers. Only 36 hours after launching their web-based CRM tool <a href="http://www.highrisehq.com/">Highrise</a>, they <a href="http://www.37signals.com/svn/posts/332-highrise-early-stats-cases-for-all-the-new-solo-plan-and-more-disk-space">announced changing their usage plans</a> in response to customer feedback.</p>
<p>For example, there's now a "Solo" plan for lone warriors that need lots of the features of the "Plus" plan, but no additional user accounts. And every plan had its storage space increased.</p>
<p>It's so easy to conquer your market. Just switch from "Know your enemy" to "Listen to your customer".</p>
