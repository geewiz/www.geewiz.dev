---
layout: post
title: "When Death goes corporate"
microblog: false
guid: http://geewiz.micro.blog/2006/08/11/when-death-goes.html
date: 2006-08-11T22:12:21+0200
type: post
url: /2006/08/11/when-death-goes.html
---

<p><a href="http://www.cedric-der-film.de/index.html">Cedric</a> is a funny and beautifully animated short film made by students at german University of Applied Sciences Lippe-H&ouml;xter. The plot:</p>
<blockquote class="posterous_medium_quote">
<p>Der Tod als globaler Dienstleister? Undenkbar! Das dachte bisher auch der Tod. Doch dann kam sein Neffe Cedric auf die wahnwitzige Idee, w&auml;hrend der Abwesenheit seines Oheims das Unternehmen AFTER LIFE zu gr&uuml;nden. Diese Firma &uuml;bernimmt mit modernster Technik das Handwerk des Todes und vermarktet es weltweit.</p>
</blockquote>
<p>Hilarity ensues. You have to watch it!</p>
<p>And then get back to work, please.</p>
<p>(via <a href="http://blog.handelsblatt.de/indiskretion/eintrag.php?id=823">Indiskretion Ehrensache</a>)</p>
