---
layout: post
title: "Emacs and The Second Coming of TextMate"
microblog: false
guid: http://geewiz.micro.blog/2011/05/15/emacs-and-the.html
date: 2011-05-15T18:38:01+0200
type: post
url: /2011/05/15/emacs-and-the.html
---
<p>A text editor is one of the most important tools of a sysadmin, software developer, documentation and blog writer. So, after switching from Linux to Mac a few years ago, I immediately starting looking for a good editor software. On Linux, I had been using Emacs for many years, but its Mac versions available at that time didn't convince me. They rather reminded me of the reasons for which I replaced my desktop OS after all. It didn't take me long to find TextMate and it became one of the first in the long line of applications I purchased in my Mac life. And I've been using it daily ever since.</p>
<p>TextMate is a very capable editor and its add-on "bundle" concept makes it easily extendable. There are bundles for every common programming language, for using version control systems and even a bundle for blogging that lets you not only write and preview your writing but also publish your finished post.</p>
<p>But there is also one concern that's been bugging TextMate users for a long time now: the author is working on version 2 of the software. At least that's what he uses to claim on his blog every few months. Recently, Watts Martin must have lost his patience and in "<a href="http://chipotle.tumblr.com/post/5207966724/text-editor-intervention">Text Editor Intervention</a>", he makes a compelling case that there are proven alternatives to eternally waiting for the Second Coming of TextMate:</p>
<blockquote>
<p>But in the meantime, you gotta get work done. Either pony up   money for BBEdit, pony up&nbsp;time for MacVim (or Emacs), or   stick with TextMate.</p>
</blockquote>
<p>Shorty after reading his thought-provoking post, I came upon Joshua   Timberman's blog post   "<a href="http://jtimberman.posterous.com/switching-to-gnu-emacs">Switching   to GNU Emacs</a>". I did a short search and it almost looks like   there is an Emacs renaissance going on.</p>
<p>As you may already have guessed, I decided to give it a   try and join the movement. Why?</p>
<ol>
<li>Back in the days, I've been   using Emacs for almost everything that had to do with plain text. I   know I'll be able to accomplish all the tasks for which I've been using TextMate. </li>
<li>GNU Emacs has been ported to Cocoa in the   meantime, so its UI runs natively on Mac OS X.</li>
<li>After   installing Emacs, I realized that all of the important Emacs keyboard   shortcuts are still stored in my muscle memory.</li>
<li>Getting Emacs fit for a variety of tasks is easy with pre-configured packages like the <a href="https://github.com/technomancy/emacs-starter-kit">Emacs Starter Kit</a>.</li>
<li>The effort of customizing and extending probably is more effective if put into   Emacs. As Watts puts it:</li>
</ol>
<blockquote>
<p>Why do I recommend three stodgy old warhorses? Well, any editor   that has a still-growing community after two decades is probably   doing something right.</p>
</blockquote>
<p>And finally, as GNU Emacs is the embodiment of Free Software,   I certainly won't have to pay another license fee for the next major   version.</p>
<p>Repentantly, I return into the arms of the <a href="http://www.dina.dk/~abraham/religion/">Church of Emacs</a>.</p>
