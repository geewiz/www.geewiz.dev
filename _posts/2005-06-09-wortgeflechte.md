---
layout: post
title: "Wortgef(l)echte"
microblog: false
guid: http://geewiz.micro.blog/2005/06/09/wortgeflechte.html
date: 2005-06-09T20:49:29+0200
type: post
url: /2005/06/09/wortgeflechte.html
---

<p><a href="http://blog.koehntopp.de/archives/852-Bewaffnung-fuer-Vokabelkrieger.html">Kollege K&ouml;hntopp</a> identifiziert interessante linguistische Links. Danke daf&uuml;r!</p>
<p>Bei den <a href="http://wortschatz.uni-leipzig.de/wort-des-tages">W&ouml;rtern des Tages</a> werden Newsdienste und Tageszeitungen daraufhin untersucht, welche Worte in der Vergangenheit  und insbesondere zum heutigen Tag herausragend oft auftauchen. Die Verflechtung solch aktueller W&ouml;rter untereinander stellen Assoziationsdiagramme grafisch dar.</p>
<p>Wer also aktuelle Munition f&uuml;r den Vokabelkrieg braucht (Kris denkt da offensichtlich erst mal an mich), kann sich t&auml;glich auf der Wortschatz-Website oder per RSS r&uuml;sten.</p>
