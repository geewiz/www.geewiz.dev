---
layout: post
title: "Die etwas andere Weihnachtsmusik"
microblog: false
guid: http://geewiz.micro.blog/2005/11/29/die-etwas-andere.html
date: 2005-11-29T08:12:23+0200
type: post
url: /2005/11/29/die-etwas-andere.html
---

<p>F&uuml;r alle, denen das ewige <a href="http://www.somafm.com/">Last christmas" und "Leise rieselt der Schnee" in der Vorweihnachtszeit auf den Wecker geht, bietet "Soma.FM</a> eine Alternative: der auf Groove und Beats spezialisierte Webradiosender spielt auf seinem Sonderkanal "Xmas in Frisko" bekannte Weihnachtslieder im Reggae- oder Latin-Rhythmus, aber auch eher unbekannte St&uuml;cke wie "Holy shit, it's christmas" und "All I want for christmas is liposuction". </p>
<p>Wo ist meine Nikolaus-M&uuml;tze und mein Hawaii-Hemd?</p>
