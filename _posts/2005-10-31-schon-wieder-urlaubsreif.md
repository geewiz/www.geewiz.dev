---
layout: post
title: "Schon wieder urlaubsreif?"
microblog: false
guid: http://geewiz.micro.blog/2005/10/31/schon-wieder-urlaubsreif.html
date: 2005-10-31T22:53:43+0200
type: post
url: /2005/10/31/schon-wieder-urlaubsreif.html
---

<p>Wenn man von Kollegen morgens gefragt wird, ob man mit zum Fr&uuml;hst&uuml;ck gehen wolle, denn "Du siehst aus, als h&auml;ttest du einen Kaffee dringend n&ouml;tig.", dann macht man sich so seine Gedanken...</p>
<p>Da ist es wohl gutes Timing, dass ich die kommenden Br&uuml;ckentage f&uuml;r ein verl&auml;ngertes Wochenende nutze. :)</p>
