---
layout: post
title: "Jagd auf Hobbychemiker"
microblog: false
guid: http://geewiz.micro.blog/2006/06/01/jagd-auf-hobbychemiker.html
date: 2006-06-01T07:26:17+0200
type: post
url: /2006/06/01/jagd-auf-hobbychemiker.html
---

<p>Mit 10 Jahren bekam ich einen "Kosmos Chemiebaukasten" zu Weihnachten. Ich war fasziniert -- ich konnte selbst wunderbare Dinge mit Ger&uuml;chen, Farben und Feuer vollbringen, ganz wie der Junge in der Serie "Merlin". Als ich alle Experimente ausprobiert hatte, begann ich, das Chemieregal in der Leihbibliothek durchzuackern. Apotheker und Drogisten riefen meine Eltern an, ob sie mir meine Bestellungen wirklich aush&auml;ndigen d&uuml;rften. Die Firma Merck erhielt betr&auml;chtliche Teile meines Taschengelds. Ich machte es mir zum Hobby, Summenformeln auswendig zu lernen, und lernte, was Atome, Molek&uuml;le, Salze und Konglomerate sind. Ich dekantierte, filtrierte und destillierte. Bald  durfte ich mit meinem Labor in einen Verschlag im Garten umziehen, um nicht mehr den Teppich zu versauen und die Wohnung mit Gestank zu verpesten. Meine Eltern vertrauten mir sogar noch, als auf dem Vorplatz meines Labors eine brennende Acetonflasche platzte. Der Spa&szlig; am Experimentieren weckte mein Interesse an Naturwissenschaften und bescherte mir ein lockeres erstes Jahr Chemie in der Schule.</p>
<p>Heutzutage wird es schwieriger sein, Kindern diese Erfahrung zu vermitteln und so ihren Forscherdrang zu stimulieren. Ich hoffe, dass die Jagd auf Drogenfabriken und Terroristennester, die in den USA zur <a href="http://wired.com/wired/archive/14.06/chemistry.html">St&uuml;rmung von Heimlabors</a> f&uuml;hrt, nicht auch zu uns her&uuml;berschwappt.</p>
