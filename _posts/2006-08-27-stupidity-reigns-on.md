---
layout: post
title: "Stupidity reigns on our way to total surveillance"
microblog: false
guid: http://geewiz.micro.blog/2006/08/27/stupidity-reigns-on.html
date: 2006-08-27T19:57:32+0200
type: post
url: /2006/08/27/stupidity-reigns-on.html
---

<p>I was howling in pain when I read what Uwe D&ouml;ring, minister of justice in Germany's federal state Schleswig-Holstein, <a href="http://www.spiegel.de/netzwelt/politik/0">had to say against encryption</a>,1518,433126,00.html: "There are methods so complex that they can't be cracked".</p>
<p>Hell yes, and we're glad there are! Those methods you want to limit or even ban, Mr. D&ouml;ring, ensure that people don't have to fear for their privacy and companies are secure from espionage!<br />
D&ouml;ring continued by declaring that <a href="http://www.anon-online.de/">AN.ON</a> should be banned, too. AN.ON is a anonymous-surfing service provided by the data privacy center Kiel and the universities of Dresden, Berlin and Regensburg. D&ouml;ring finds it unacceptable to spend tax money on a service that <a href="http://www.datenschutz.de/news/detail/?nid=1911">makes it possible for terrorists and criminals of every description to commit crimes undiscoveredly". What the minister didn't consider is that AN.ON has been "actively cooperating</a> with the authorities for years and already does have the means to observe certain suspects when a court order is issued.</p>
<p>From a perspective like that, providing tools for secure and anonymous communication like GnuPG or Tor means <a href="http://rabe.supersized.org/archives/796-Der-Kampf-gegen-Anonymitaet-und-Verschluesselung-im-Krieg-gegen-den-Terror.html.">supporting criminals</a> Better watch out, Werner Koch, you terrorist backer!</p>
<p>But obviously, there really are people stupid enough to think that prohibiting encryption would stop criminals from using it. And some of those people actually sit in our governments. All those recent actions and bills against terrorism seem to have the only goal as to lull the public into a false sense of security without actually providing it.</p>
<p>I don't believe that blurry surveillance camera pictures can prevent terrorist attacks. But I do believe, as does<br />
<a href="http://lumma.de/eintrag.php?id=2896">Nico Lumma</a>, that observing <a href="http://rabe.supersized.org/archives/802-Wenn-sie-Tacheles-reden.html">every point of public communication, train stations, airports, big streets and places</a> puts every citizen under general suspect and takes his and her personal rights and freedom.</p>
<p>I'm appaled by the level of simple-mindedness that shows in conclusions like those of minister D&ouml;ring. "Simple solutions for simple people" seems to be the motto. Unfortunately, there are people that are not that simple. Me, for example. And the terrorists.</p>
