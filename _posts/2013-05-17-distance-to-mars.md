---
layout: post
title: "Distance to Mars"
microblog: false
guid: http://geewiz.micro.blog/2013/05/17/distance-to-mars.html
date: 2013-05-17T13:37:22+0200
type: post
url: /2013/05/17/distance-to-mars.html
---
<p>Recently, I've had a discussion with my precious about the song "<a href="http://en.wikipedia.org/wiki/Clouds_Across_the_Moon">Clouds across the moon</a>" and how the laws of physics make a phone call in the common sense between Earth and Mars impossible.</p>
<p>Had I known that there is a website with an awesome visualisation of the <a href="http://www.distancetomars.com">distance to Mars</a>, this would have been far easier!</p>
<p>(via <a href="http://www.swiss-miss.com/2013/04/distance-to-mars.html">swissmiss</a>)</p>
