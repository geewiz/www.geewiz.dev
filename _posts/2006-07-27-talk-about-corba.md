---
layout: post
title: "Talk about CORBA"
microblog: false
guid: http://geewiz.micro.blog/2006/07/27/talk-about-corba.html
date: 2006-07-27T23:33:36+0200
type: post
url: /2006/07/27/talk-about-corba.html
---

<p>As a part of the -boot camp- introduction programme our new sysadmins go through, I also give a talk about the concepts of the CORBA middleware architecture. I explain what CORBA is, what its goals are, as well as its advantages and its shortcomings.</p>
<p>I put the <a href="http://www.jochen-lillich.de/static/vortrag/corba">slides</a> of my talk (in german language) online just now.</p>
