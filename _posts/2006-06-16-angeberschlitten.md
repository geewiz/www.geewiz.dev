---
layout: post
title: "Angeberschlitten"
microblog: false
guid: http://geewiz.micro.blog/2006/06/16/angeberschlitten.html
date: 2006-06-16T17:47:19+0200
type: post
url: /2006/06/16/angeberschlitten.html
---

<p>Nach Guy Kawasakis Blog zu urteilen, scheinen deutsche Autos ihren Ruf in Amerika noch nicht ganz verloren zu haben:</p>
<blockquote class="posterous_short_quote">
<p>If you buy your father a German car, he really cant even drive it to the office without appearing to show off.</p>
</blockquote>
<p>Deshalb empfiehlt er in <a href="http://feeds.feedburner.com/guykawasaki/Gypm?m=149">Guy's Top Ten Father's Day Gift List</a> einen Ford Shelby Cobra GT 500.</p>
