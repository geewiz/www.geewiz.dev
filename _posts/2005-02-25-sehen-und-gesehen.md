---
layout: post
title: "Sehen und gesehen werden"
microblog: false
guid: http://geewiz.micro.blog/2005/02/25/sehen-und-gesehen.html
date: 2005-02-25T08:06:00+0200
type: post
url: /2005/02/25/sehen-und-gesehen.html
---

<p>So langsam kommt im neuen Heim alles dorthin, wo es hingeh&ouml;rt. Zum Beispiel ich an den Computer. Und ab jetzt kann man das auch sehen: ich habe meine alte Webcam wieder angeschlossen. Der Link ist im Kasten rechts zu finden.</p>
<p>Keine Angst -- die Kamera wird nicht immer aktiv sein. Aber wenn sie es ist, dann sicher auch mein ICQ-Programm. Ihr k&ouml;nnt mir also gern Bescheid geben, wenn ihr meint, dass ich mich mal wieder rasieren sollte. :)</p>
