---
layout: post
title: "Homeverzeichnis in Subversion"
microblog: false
guid: http://geewiz.micro.blog/2005/01/10/homeverzeichnis-in-subversion.html
date: 2005-01-10T06:37:00+0200
type: post
url: /2005/01/10/homeverzeichnis-in-subversion.html
---

<p>Jeder, der sich intensiver mit Software-Entwicklung besch&auml;ftigt hat, kennt die Vorteile einer Versionskontroll-Software. Gerade bei Open Source Software kommt man ohne CVS und Co. nicht aus.</p>
<p>Ich hatte vor langer Zeit auch mal die Idee, dieses Prinzip auch auf Konfigurationsdateien des Betriebssystems anzuwenden.  Konsequent durchgezogen habe ich sie aber nie.</p>
<p>Bei O'Reilly fand ich jetzt einen <a href="http://www.onlamp.com/pub/a/onlamp/2005/01/06/svn_homedir.html">Artikel</a> dar&uuml;ber, dass man das Ganze auch so weit treiben kann, sein gesamtes Homeverzeichnis inklusive E-Mails und sonstigen Dateien unter Versionskontrolle zu stellen.</p>
<p>Der Gedanke erscheint mir sehr interessant, insbesondere, weil ich die Aussage des Autors Joey Hess "Seit 1999 habe ich keine Datei verloren" leider nicht teilen kann.</p>
<p>Ich werde heute abend mal dr&uuml;ber nachdenken, wie ich das bei meinen Rechnern umsetzen k&ouml;nnte.</p>
