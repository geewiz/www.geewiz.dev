---
layout: post
title: "Die Ideen der anderen"
microblog: false
guid: http://geewiz.micro.blog/2009/07/01/die-ideen-der.html
date: 2009-07-01T22:45:18+0200
type: post
url: /2009/07/01/die-ideen-der.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_short_quote"><p>Die Frage ist also nicht, ob das Netz ein rechtsfreier Raum sein darf, wie derzeit diskutiert wird, sondern ob Millionen von Internetnutzern es hinnehmen, dass die Ungerechtigkeiten der analogen Welt im Netz fortbestehen.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.tagesspiegel.de/kultur/Urheberrecht;art772,2835794">tagesspiegel.de</a></div>
<p>Das Problem mit dem Urheberrecht kritisch beleuchtet.</p>
</div>
