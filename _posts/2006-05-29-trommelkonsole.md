---
layout: post
title: "Trommel-Konsole"
microblog: false
guid: http://geewiz.micro.blog/2006/05/29/trommelkonsole.html
date: 2006-05-29T19:11:46+0200
type: post
url: /2006/05/29/trommelkonsole.html
---

<p>Bislang hat mich die Nintendo Wii-Konsole weitaus weniger interessiert als die Entwicklungen rund um die PS3. Aber wenn ich sehe, dass dank der eingebauten Bewegungssensoren sogar <a href="http://www.techeblog.com/index.php/tech-gadget/wiis-banging-drum-demo">Trommeln mit den Wii-Controllern</a> m&ouml;glich ist, scheint mir hier doch eine neue Generation von Spielkonsole entwickelt worden zu sein.</p>
