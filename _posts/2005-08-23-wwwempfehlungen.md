---
layout: post
title: "WWW-Empfehlungen"
microblog: false
guid: http://geewiz.micro.blog/2005/08/23/wwwempfehlungen.html
date: 2005-08-23T21:10:58+0200
type: post
url: /2005/08/23/wwwempfehlungen.html
---

<p>Mit Kollege Marko sprach ich k&uuml;rzlich &uuml;ber Webstandards wie XHTML und CSS. Weil ich mich mit diesem Thema schon lange auseinandersetze, wollte ich ihm zu dem Thema auch ein paar gute Links heraussuchen und zukommen lassen.</p>
<p>Dazu fiel mir gerade ein, dass das dank <a href="http://sitebar.org/">Sitebar</a> ja ganz einfach ist: einfach in meinen <a href="http://sitebar.lillich.info/">Bookmarks</a> unter "Know-How", "Webdesign" nachschauen!</p>
