---
layout: post
title: "Virtual lynch mobs"
microblog: false
guid: http://geewiz.micro.blog/2006/09/11/virtual-lynch-mobs.html
date: 2006-09-11T21:18:00+0200
type: post
url: /2006/09/11/virtual-lynch-mobs.html
---

<p>I'm a strong proponent of online privacy. I want to feel secure living my online life, and I want the means to do so to be kept legal. (That's why <a href="http://rabe.supersized.org/archives/810-Wie-man-die-Anonymsierung-kriminalisiert.html">Kai Raven</a> always freaks the hell out of me.)</p>
<p>The recent case of a guy <a href="http://blog.wired.com/27BStroke6/index.blog?entry_id=1553329">faking a craigslist ad</a>, posing as a woman looking for a dominant guy and publishing more than 100 responses including names, photos, phone numbers and email addresses, raises another question: what about your right of privacy if you aren't clever or savvy enough to protect it sufficiently?Sure, it's quite stupid to respond to such a classified ad by giving up your name, phone number, work email address or even marital status along with compromising pictures. And it may be immoral to some people to cheat on ones wife or pursue BDSM practices. But that, let's be clear here, doesn't at all give Jason Fortuny, the guy who made up the ad, the right to publish their private information, put them on a virtual pillory and destroy their lives. What time do we have -- the dark ages? I sincerely hope he gets his ass sued off.</p>
<p>I'm with <a href="http://blog.wired.com/27BStroke6/index.blog?entry_id=1553813">Ryan Singel</a> in concluding that this prank was an act of pure arrogance and feeling of superiority. But what's even more reprehensible to me is the number of people approving his action because "those people deserved it", because they're "immoral scum" and "were dumb enough to put their information out there". That's making me sick.</p>
<p>The purpose of laws and basic rights is to secure the lives of all people, also and especially of those who don't know how to protect themselves sufficiently  -- regardless of person, religion, sexual orientation, IQ or race. That's why phishing and the infamous "Nigerian scams" aren't legal. That's why misusing personal information isn't legal either, and publishing such information without the owner's consent is misuse.</p>
<p>We're used to the fact that in some smaller villages, you'll be quickly rejected, defamed or even mobbed into moving away if you don't fit in, or worse, get known for doing things that contradict the common norm.</p>
<p>The "global village" doesn't seem much different now, does it?</p>
