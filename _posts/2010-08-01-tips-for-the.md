---
layout: post
title: "Tips for the S-Office"
microblog: false
guid: http://geewiz.micro.blog/2010/08/01/tips-for-the.html
date: 2010-08-01T15:58:28+0200
type: post
url: /2010/08/01/tips-for-the.html
---

<p>Matt Shapiro suggests that Starbucks should change its name to &ldquo;Startbucks&rdquo;, because so many startup founders not only get their caffeine kick there but also stay for quite a while during the day to do work and have business meetings. Certainly, I&rsquo;m a member of that group. Over the course of a week, I spend at least 15 hours in my favourite Starbucks outlet in Freiburg working through my todo lists and  answering customer requests.</p>
<p>With power outlets at many tables and free WiFi, Starbucks actively attracts road warriors with their laptops or iPads. But since they still call themselves a &ldquo;Coffee House&rdquo; instead of &ldquo;Coworking Space&rdquo;, I&rsquo;d like to emphasise <a href="http://gigaom.com/2010/07/01/using-starbucks-as-your-office-here-are-some-tips/">GigaOm&rsquo;s recommendations</a> for making oneself comfortable working at Starbucks without making others uncomfortable:</p>
<ul>
<li>Almost always favor a single location. It makes it easier for your contacts to drop in for meetings.</li>
<li>Learn the names of most of the baristas and also take time to have a conversation with them. It helps build a human connection.</li>
<li>Make the baristas involved in your venture &ndash; share your news and make them feel part of your struggle.</li>
<li>Make sure you buy coffee or something at least three times a day.</li>
<li>Tip generously &ndash; up to $10 a day will ensure that folks at the store don&rsquo;t view you as a freeloader and a pest.</li>
<li>Don&rsquo;t spread out your stuff and take up too much space at the store.</li>
<li>Invest in great noise-cancelling headphones (to counter the loud background music).</li>
<li>Keep your mobile phones on vibrate and leave the store for conversations.</li>
<li>Make sure that the number of people attending a meeting is fewer than four so that you can all circle around a single table.</li>
</ul>
<p>As much as I&rsquo;ll second the headphone item, I&rsquo;ll also add a quasi-opposite experience: You&rsquo;ll soon find out that you&rsquo;re not the only regular. Drop your introverted geek defences a bit and introduce yourself to your fellow S-Office workers. More often than not, a new acquaintance becomes a business opportunity.</p>
