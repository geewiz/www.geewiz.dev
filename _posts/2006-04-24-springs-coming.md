---
layout: post
title: "Spring's coming"
microblog: false
guid: http://geewiz.micro.blog/2006/04/24/springs-coming.html
date: 2006-04-24T22:05:50+0200
type: post
url: /2006/04/24/springs-coming.html
---

<p>Wer sich eine Weile mit J2EE(Java 2 Enterprise Edition) besch&auml;ftigt, wird feststellen, dass der Sun-Standard der EJB(Enterprise Java Beans) nicht unumstritten ist. Als Alternative wird oft das <a href="http://www.springframework.org">Spring Framework</a> genannt, das deutlich schlanker als ein EJB-Applikationsserver die Erstellung von J2EE-Anwendungen erm&ouml;glichen soll.</p>
<p>F&uuml;r eine schnelle Einarbeitung in das Thema habe ich mir <a href="http://www.amazon.de/exec/obidos/ASIN/0596009100/geewizweblog-21"><em>Spring; A Developer's Notebook</em></a> gekauft und in den letzten Tagen einverleibt. Jetzt weiss ich Spring einigerma&szlig;en einzuordnen und werde es bei n&auml;chster Gelegenheit mal in der Praxis erproben.</p>
<p>Meine <a href="http://www.jochen-lillich.de/buchtipps/spring-a-developers-notebook">Rezension von <em>Spring; A Developer's Notebook</em></a> habe ich gerade auf meiner Homepage ver&ouml;ffentlicht.</p>
