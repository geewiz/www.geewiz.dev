---
layout: post
title: "Thank god for SessionSaver"
microblog: false
guid: http://geewiz.micro.blog/2006/10/04/thank-god-for.html
date: 2006-10-04T22:06:26+0200
type: post
url: /2006/10/04/thank-god-for.html
---

<p>I'm so glad that <a href="https://addons.mozilla.org/firefox/436/.">rue" made the "SessionSaver Firefox extension</a></p>
<blockquote class="posterous_medium_quote">
<p>SessionSaver restores your browser <em>exactly</em> as you left it, every startup, every time. Not even a crash will phase it. Windows, tabs, even things you were typing -- they're all saved. Use the menu to add + remove sessions; right, shift, or middle-clicking will delete. "Simple mode" for peace of mind, or "Expert mode" for advanced flexibility. Just Click. Install. Rad.</p>
</blockquote>
<p>Rad indeed! The plugin not only allows me to start the day with exactly the same browser tabs I left off with the day before, but it particularly saves my nerves when Firefox crashes every second time Javascript closes a popup window.</p>
