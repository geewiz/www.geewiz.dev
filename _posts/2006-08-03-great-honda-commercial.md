---
layout: post
title: "Great Honda commercial"
microblog: false
guid: http://geewiz.micro.blog/2006/08/03/great-honda-commercial.html
date: 2006-08-03T06:50:55+0200
type: post
url: /2006/08/03/great-honda-commercial.html
---

<p>Honda produced a great <a href="http://video.google.com/videoplay?docid=-4341945450931442754">commercial for the Accord</a> showing a Rube Goldberg machine consisting of car parts.</p>
<p>This must have taken a zillion tries but finally was a single shot.</p>
