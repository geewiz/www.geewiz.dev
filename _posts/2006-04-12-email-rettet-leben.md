---
layout: post
title: "E-Mail rettet Leben"
microblog: false
guid: http://geewiz.micro.blog/2006/04/12/email-rettet-leben.html
date: 2006-04-12T06:54:31+0200
type: post
url: /2006/04/12/email-rettet-leben.html
---

<p>Manchmal ist E-Mail kein gutes Kommunikationsmittel. Zum Streiten zum Beispiel ist E-Mail &uuml;berhaupt nicht geeignet. Manchmal aber kann das Medium E-Mail Leben retten.</p>
<p>Zum Beispiel, wenn einem sein Liebling ihre traurige Feststellung schreibt, dass an den Tagen, an denen man sie in Dublin besuchen wird, "Brokeback Mountain" nicht mehr laufen wird. Wenn man seine spontane Reaktion ohne m&uuml;hsame K&ouml;rperbeherrschung hinter sich bringen und sich eine -- naja, sagen wir mal: -- geschicktere schriftliche Reaktion &uuml;berlegen kann.</p>
<p>Okay, ich hab nicht &uuml;berlegt, bevor ich die Antwort schrieb. Ich fliege morgen trotzdem r&uuml;ber. Ich bin gut krankenversichert.</p>
