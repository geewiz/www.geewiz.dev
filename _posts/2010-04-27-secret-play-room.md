---
layout: post
title: "Secret play room"
microblog: false
guid: http://geewiz.micro.blog/2010/04/27/secret-play-room.html
date: 2010-04-27T10:06:28+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwboingboi_cpdhv-scaled500.jpg?w=226
photos:
- /assets/wp-content/2011/05/media_httpwwwboingboi_cpdhv-scaled500.jpg?w=226
url: /2010/04/27/secret-play-room.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwboingboi_cpdhv" height="599" src="/assets/wp-content/2011/05/media_httpwwwboingboi_cpdhv-scaled500.jpg?w=226" width="453" />
</div>
<div class="posterous_quote_citation">via <a href="http://www.boingboing.net/2010/04/22/secret-play-room-use.html?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+boingboing%2FiBag+%28Boing+Boing%29">boingboing.net</a></div>
<p>Some parents really are awesome. Until this, I didn't see the use of owning a family home.</p>
</div>
