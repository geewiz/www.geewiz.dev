---
layout: post
title: "Sometimes, you don't need expensive instruments"
microblog: false
guid: http://geewiz.micro.blog/2007/04/24/sometimes-you-dont.html
date: 2007-04-24T20:20:00+0200
type: post
url: /2007/04/24/sometimes-you-dont.html
---

<p>All you need is <a href="http://www.zappinternet.com/index.php?video=JuRtTiqVib">6 drummers and an apartment</a> to really get grooving.</p>
<p>And, maybe, exact timing.</p>
