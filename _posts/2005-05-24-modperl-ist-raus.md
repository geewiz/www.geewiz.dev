---
layout: post
title: "mod_perl 2.0 ist raus"
microblog: false
guid: http://geewiz.micro.blog/2005/05/24/modperl-ist-raus.html
date: 2005-05-24T21:21:29+0200
type: post
url: /2005/05/24/modperl-ist-raus.html
---

<p>Nach langer Entwicklungszeit wurde jetzt Version 2.0 des Apache-Moduls <a href="http://search.cpan.org/">mod_perl</a>~gozer/mod_perl-2.0.0/ ver&ouml;ffentlicht. mod_perl ist ein Apache-Modul, das den Perl-Interpreter in den Webserver integriert. So erm&ouml;glicht es einerseits die schnelle Verarbeitung von Perl-Programmen und andererseits die Entwicklung eigener Apache-Module in Perl. Die neue Version erf&uuml;llt nun endlich den Wunsch, die Vorteile von Apache 2.x mit denen von Perl 5.8 zu verkn&uuml;pfen.</p>
<p>Zu den gr&ouml;&szlig;ten Neuerungen geh&ouml;ren die Unterst&uuml;tzung von Threads und eine Perl-Schnittstelle zur Apache Portable Runtime. Was sich sonst gegen&uuml;ber der 1er Version ge&auml;ndert hat, steht im <a href="http://search.cpan.org/">&Uuml;berblick</a>~gozer/mod_perl-2.0.0/docs/user/intro/overview.pod.</p>
