---
layout: post
title: "Illuminate your thoughts!"
microblog: false
guid: http://geewiz.micro.blog/2010/08/11/illuminate-your-thoughts.html
date: 2010-08-11T20:20:49+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwfunktion_llfmu-scaled500.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpwwwfunktion_llfmu-scaled500.jpg?w=300
url: /2010/08/11/illuminate-your-thoughts.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwfunktion_llfmu" height="349" src="/assets/wp-content/2011/05/media_httpwwwfunktion_llfmu-scaled500.jpg?w=300" width="349" />
</div>
<div class="posterous_quote_citation">via <a href="http://www.funktionalley.com/m4n?cat1id=5&amp;oid=etailer-product&amp;catgr=0&amp;cat2id=0&amp;_locale=1&amp;viewMode=1&amp;prodid=620">funktionalley.com</a></div>
<p>"Snakkes", a speech bubble lamp that can be written on with whiteboard markers.</p>
</div>
