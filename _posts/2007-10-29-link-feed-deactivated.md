---
layout: post
title: "Link feed deactivated"
microblog: false
guid: http://geewiz.micro.blog/2007/10/29/link-feed-deactivated.html
date: 2007-10-29T17:58:14+0200
type: post
url: /2007/10/29/link-feed-deactivated.html
---

<p>I just decided to switch off the insertion of my del.icio.us links into this blog's RSS feed. I'll rather concentrate on keeping the feed active with blog posts alone.</p>
<p>If you find my link collection interesting, feel free to subscribe to my <a href="http://del.icio.us/rss/Geewiz">del.icio.us link feed</a> directly.</p>
<p>Are you satisfied now, Kai? ;-)</p>
