---
layout: post
title: "Different approaches"
microblog: false
guid: http://geewiz.micro.blog/2006/12/01/different-approaches.html
date: 2006-12-01T03:34:01+0200
type: post
url: /2006/12/01/different-approaches.html
---

<p>I'm an uncurable optimist, believing in good and expecting to make a difference. People can debate endlessly if it's better to be a pessimist and not be disappointed or to be an optimist and have a happy perspective on things. For me, I'd say that optimism is the better approach -- it can even be fueled by pessimism, as I learned today.</p>
<p>At work, many of us write short daily reports of our accomplishments and decisions. Two of my coworkers obviously dealt with the same problem, but in different ways:</p>
<p>First, R. sent his daily:</p>
<blockquote class="posterous_short_quote">
<p>Regarding the memory leak in GNU awk: Since I as an "old naysayer" don't suppose that it can be fixed anyway, I configured some resource capping.</p>
</blockquote>
<p>S. later wrote in his report:</p>
<blockquote class="posterous_short_quote">
<p>I'm gonna show that "old naysayer" R. I fixed the memory leak in gawk and sent it to the GNU developers. I'd have done that anyway, but now more than ever. :P</p>
</blockquote>
<p>Getting things done also is an attitude.</p>
