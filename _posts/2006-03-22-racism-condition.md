---
layout: post
title: "Racism condition"
microblog: false
guid: http://geewiz.micro.blog/2006/03/22/racism-condition.html
date: 2006-03-22T02:49:09+0200
type: post
url: /2006/03/22/racism-condition.html
---

<p>Ein <a href="http://www.bloekschaf.de/archives/14-Heute-vor-44-Jahren.html">Massaker in S&uuml;dafrika</a> vor 44 Jahren f&uuml;hrte dazu, dass heute "Internationaler Tag zur Beseitigung des Rassismus" ist. Fakt ist: Benachteiligung aufgrund von Herkunft und Abstammung findet statt -- im &ouml;ffentlichen wie im privaten Leben. Manchmal erschreckend unbewusst...</p>
