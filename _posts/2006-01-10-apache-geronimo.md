---
layout: post
title: "Apache Geronimo 1.0"
microblog: false
guid: http://geewiz.micro.blog/2006/01/10/apache-geronimo.html
date: 2006-01-10T04:59:49+0200
type: post
url: /2006/01/10/apache-geronimo.html
---

<p>Mit <a href="http://geronimo.apache.org/">Apache Geronimo 1.0</a> steht nach <a href="http://www.jboss.org/products/jbossas">JBoss</a> und <a href="http://jonas.objectweb.org/">Jonas</a> jetzt schon der dritte von Sun zertifizierte J2EE-Anwendungsserver zur Verf&uuml;gung, der als Open-Source-Software freigegeben ist. J2EE-Entwickler, die L&ouml;sungen abseits der propriet&auml;ren Angebote suchen, haben also inzwischen die Qual der Wahl. Das ist erfreulich, denn Konkurrenz spornt schlie&szlig;lich zur Verbesserung an. Auch Konkurrenz im eigenen Lager.</p>
