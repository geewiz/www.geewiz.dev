---
layout: post
title: "Featured Perl module: Module::Starter"
microblog: false
guid: http://geewiz.micro.blog/2006/08/31/featured-perl-module.html
date: 2006-08-31T20:11:34+0200
type: post
url: /2006/08/31/featured-perl-module.html
---

<p>Sometimes, preparing the ground for a new to-be-developed Perl module requires more effort than doing the actual programming: creating a directory structure, building an initial Makefile, writing a MANIFEST file, etc. takes some time, especially if you have to look up which informations are necessary.</p>
<p>Module::Starter to the rescue. This Perl module knows exactly what a new module distribution should look like and creates a proper environment. You don't have to use the module itself, since there's an utility included named <a href="http://search.cpan.org/">module-starter</a>~petdance/Module-Starter-1.42/bin/module-starter that gets all necessary information from the command line and does all the work.</p>
<p>It's always important that basic work takes as less effort as possible, for otherwise developers often get reluctant to do it at all. Module::Starter ensures that creating new Perl modules is as efficient as it can be.</p>
