---
layout: post
title: "Opscode Chef jetzt als gehostete L&ouml;sung erh&auml;ltlich"
microblog: false
guid: http://geewiz.micro.blog/2010/06/25/opscode-chef-jetzt.html
date: 2010-06-25T15:47:00+0200
type: post
url: /2010/06/25/opscode-chef-jetzt.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p>Opscode, Inc., a cloud infrastructure automation company, today announced the limited beta release of the Opscode Platform, the world&rsquo;s first hosted <a href="http://www.opscode.com">configuration management</a> service.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.opscode.com/blog/2010/06/21/opscode-unveils-commercial-platform/">opscode.com</a>
</div>
<p>Was ich noch nicht verstehe: Wieso sollte jemand, dessen Infrastruktur CM-Software wie Chef erfordert, ausgerechnet ihr Herzst&uuml;ck extern hosten lassen?</p>
</div>
