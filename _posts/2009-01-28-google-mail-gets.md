---
layout: post
title: "Google Mail gets offline mode"
microblog: false
guid: http://geewiz.micro.blog/2009/01/28/google-mail-gets.html
date: 2009-01-28T15:17:04+0200
type: post
url: /2009/01/28/google-mail-gets.html
---

<p>I tried so many email applications, but none could actually beat the productivity I operate my GMail account with the original web interface. My only gripe has been so far that I wasn't able to access my email when there was no internet connection. On the train to and from work, for example.</p>
<p>It's not that Google didn't have the tools to solve that problem. Google Gears is available quite some time and I use it extensively with <a href="http://www.mindmeister.com">Mindmeister</a>.</p>
<p>Today I found on the official GMail blog that the Google engineers really are working on an <a href="http://gmailblog.blogspot.com/2009/01/new-in-labs-offline-gmail.html">offline mode for GMail</a>. It's still work in progress and only available over the labs menu. This feature enables you to use GMail offline, and it also offers a "flaky connection mode" where it uses the local cache but tries to synchronize in the background.</p>
<p>Who needs a desktop application with internet connection when you can get the internet on your desktop?</p>
