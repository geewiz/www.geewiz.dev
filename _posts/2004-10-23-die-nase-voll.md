---
layout: post
title: "Die Nase voll"
microblog: false
guid: http://geewiz.micro.blog/2004/10/23/die-nase-voll.html
date: 2004-10-23T02:37:00+0200
type: post
url: /2004/10/23/die-nase-voll.html
---

<p>Es gibt nichts, was es nicht gibt.</p>
<p><a href="http://www.elv.de/Main.asp?Menue=Shop">rhinoBeam forte</a>&amp;Artikel=547-80&amp;Gruppe=HT-GH-MT kann durch "bio-stimulative Photonen (Speziallicht) und ein bipolares Permanentmagnetfeld" meinen zellul&auml;ren Energienotstand beheben. Und der ist ja bekanntlich der eigentliche Grund f&uuml;r Nasenrotz.</p>
<p>Bio-stimulative Photonen sind nat&uuml;rlich eine tolle Sache. Man k&ouml;nnte sich nat&uuml;rlich auch eine Maglite in die Nase stecken, aber das ist selbst bei der Mini-Version auf die Dauer nicht so angenehm. Und f&uuml;r das bipolare Magnetfeld m&uuml;sste man den Dauermagneten auch noch dazwischenquetschen.</p>
<p>Da ist das angebotene Ger&auml;t nat&uuml;rlich geschickter, besonders f&uuml;r abwehrgeschw&auml;chte, &auml;ltere und geistig minderbemittelte Menschen. In die Nase stecken und gesund werden -- einfach und genial.</p>
<p>So. Dann mach ich jetzt mal Feierabend und erfinde ein Ger&auml;t gegen Durchfall. Irgendwo habe ich noch ne Maglite rumliegen.</p>
