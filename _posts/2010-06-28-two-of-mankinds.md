---
layout: post
title: "Two of mankind's greatest inventions: iPad + Velcro"
microblog: false
guid: http://geewiz.micro.blog/2010/06/28/two-of-mankinds.html
date: 2010-06-28T19:00:12+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpimagesapple_ncftw-scaled1000.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpimagesapple_ncftw-scaled1000.jpg?w=300
url: /2010/06/28/two-of-mankinds.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<a href="/assets/wp-content/2011/05/media_httpimagesapple_ncftw-scaled1000.jpg"><img alt="Media_httpimagesapple_ncftw" height="283" src="/assets/wp-content/2011/05/media_httpimagesapple_ncftw-scaled1000.jpg?w=300" width="500" /></a>
</div>
<div class="posterous_quote_citation">via <a href="http://www.apple.com/ipad/velcro/">apple.com</a></div>
<p>Magical meets versatile.</p>
</div>
