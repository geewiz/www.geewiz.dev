---
layout: post
title: "Five things you didn't know about me"
microblog: false
guid: http://geewiz.micro.blog/2007/02/20/five-things-you.html
date: 2007-02-20T06:25:00+0200
type: post
url: /2007/02/20/five-things-you.html
---

<p>Finally, the meme has reached me by <a href="http://blog.helaron.de/archives/374-Stoeckchen-aus-weiter-Ferne-gefangen....html">Helaron</a>, and here they are:</p>
<h1>My first and only crime was the theft of a Sinclair ZX81 magazine that was too expensive for my allowance in 1984. I didn't have a computer yet at that time.</h1>
<h1>In my first semesters of computer science, I had been thinking about taking on catholic theology and become a priest instead.</h1>
<h1>When I'm excited, my eyes start to water.</h1>
<h1>I'm afraid of heights, but I can overcome that fear by will.</h1>
<h1>There's actually a way to make me really angry. Unfortunately, there's no place left to tell it to you. :-D</h1>
<p>The stick goes on to my lovely <a href="http://www.bloekschaf.de/archives/87-Big-Brother-im-Hosentaschenformat.html">Carolin</a>, to <a href="http://blog.bumsdiekuh.net/archives/43-AErger-um-den-kleinen-Hobbit.html">Andy</a> and to <a href="http://blog.kai-uwe-beyer.de/index.php?/archives/584-Kalkulation-von-Ebayhaendlern.html.">Kai</a></p>
