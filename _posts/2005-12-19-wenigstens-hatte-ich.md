---
layout: post
title: "Wenigstens hatte ich keine Kopfschmerzen"
microblog: false
guid: http://geewiz.micro.blog/2005/12/19/wenigstens-hatte-ich.html
date: 2005-12-19T20:00:28+0200
type: post
url: /2005/12/19/wenigstens-hatte-ich.html
---

<p>Dem morgendlichen Episodenaustausch und Gel&auml;chter der Kollegen nach muss die 1&amp;1-Weihnachtsfeier ein voller Erfolg gewesen sein. Schade, offensichtlich habe ich ein (be-) rauschendes Fest verpasst, nach dem nicht jede/r am gewohnten Schlafplatz aufgewacht ist. ;)</p>
