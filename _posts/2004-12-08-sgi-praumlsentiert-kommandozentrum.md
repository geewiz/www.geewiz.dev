---
layout: post
title: "SGI pr&auml;sentiert Kommandozentrum"
microblog: false
guid: http://geewiz.micro.blog/2004/12/08/sgi-praumlsentiert-kommandozentrum.html
date: 2004-12-08T23:05:00+0200
type: post
url: /2004/12/08/sgi-praumlsentiert-kommandozentrum.html
---

<p>Auf der derzeit laufenden <a href="http://www.iitsec.org/">Interservice/Industry Training, Simulation and Education Conference" ("I/ITSEC</a>) pr&auml;sentiert SGI leistungsf&auml;hige Datenverarbeitungs-, Speicher- und Visualisierungstechnologie f&uuml;r die Bereiche Geheimdienst und Milit&auml;r. Gerade in diesen Bereichen steigen die zu verarbeitenden Informationsmengen st&auml;ndig an, auf deren Basis h&auml;ufig schnelle Entscheidungen gef&auml;llt werden m&uuml;ssen.[1]</p>
<p>Highlight der Pr&auml;sentation ist ein "Command Center" mit einem Gro&szlig;display zur Visualisierung gro&szlig;er Datenmengen in Mono- oder Stereo-3D. LCD-Panels an den Seiten stellen erg&auml;nzende Informationen dar, per "Visual Area Networking" kann auf die Informationen auch von entfernten Orten aus nahezu in Echtzeit zugegriffen werden.</p>
<p>(<a href="http://www.ccnmag.com/?nav=headlines">CCN News</a>&amp;id=3528, via <a href="http://www.siliconbunny.com/news/archives/000113.shtml">SiliconBunny</a>)</p>
<p>fn1. Ich muss unbedingt mal wieder "War Games" anschauen...</p>
