---
layout: post
title: "Neue IRIX-Software"
microblog: false
guid: http://geewiz.micro.blog/2005/03/15/neue-irixsoftware.html
date: 2005-03-15T21:56:00+0200
type: post
url: /2005/03/15/neue-irixsoftware.html
---

<p>Neue Pakete auf <a href="http://www.nekochan.net/downloads/index.php?path=Nekoware">Nekochan</a>%2Fbeta/:</p>
<ul>
<li>neko_sim-0.9.3-2.tardist - Einfacher Instant Messenger (MSN, AIM, Jabber, Oscar..)</li>
<li>neko_viruskiller-0.9-1.tardist - Ein kleines Spiel gegen Viren</li>
<li>neko_zziplib-0.10.82.tardist - Eine Bibliothek f&uuml;r Dateiarchive</li>
<li>neko_alephone-cvs.tardist - Eine Game Engine</li>
<li>neko_squid-2.5.STABLE9.tardist - Der Squid Web Proxy</li>
</ul>
<p><em>Update:</em></p>
<ul>
<li>neko_cyrus_imapd-2.2.12.tardist - IMAP-Server</li>
</ul>
