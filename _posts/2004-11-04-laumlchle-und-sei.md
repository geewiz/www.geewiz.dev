---
layout: post
title: "L&auml;chle und sei froh"
microblog: false
guid: http://geewiz.micro.blog/2004/11/04/laumlchle-und-sei.html
date: 2004-11-04T04:19:00+0200
type: post
url: /2004/11/04/laumlchle-und-sei.html
---

<p>Irgendwie ist diese Woche nicht mein Tag.</p>
<p>Am Sonntag sorgte ich bei der CAJ-DV erst mal f&uuml;r einen kleinen Eklat. Danach war ich so neben der Spur, dass ich meine Tasche mit Klamotten sowie Wasch- und Rasierzeug im Tagungshaus vergessen habe.</p>
<p>Als ich gestern nochmal hin gefahren bin, war das Haus geschlossen.</p>
<p>Heute morgen werde ich mit dem Gedanken wach "Seit wann kommt die M&uuml;llabfuhr schon vor 7 Uhr?" Ah ja. Wenn man einen Wecker auf 7 stellt, sollte man ihn auch einschalten. 2 Besprechungen verschlafen.</p>
<p>Ich glaube, das wird eine spannende Woche.</p>
