---
layout: post
title: "Sch&ouml;nes Wetter in Dublin"
microblog: false
guid: http://geewiz.micro.blog/2006/04/16/schoumlnes-wetter-in.html
date: 2006-04-16T21:45:11+0200
type: post
url: /2006/04/16/schoumlnes-wetter-in.html
---

<p>Dass RyanAir nun auch von Baden-Baden aus fliegt, macht meine Besuche bei Carolin noch einfacher. Am Mittwoch hab ich einfach ein wenig fr&uuml;her Schluss gemacht und bin von Karlsruhe aus direkt zum Flughafen gefahren.<br />
Aufgrund der Flugpreise an Ostern und weil ich Dienstag wieder im B&uuml;ro sein muss, hatte ich mich entschieden, schon am Samstag wieder zur&uuml;ckzufliegen. Deshalb war es ein Vorteil, dass der Flug nach Dublin von Baden-Baden aus deutlich fr&uuml;her geht als in Hahn. So hatten Caro und ich noch den ganzen Abend f&uuml;r uns.</p>
<p>&Uuml;berhaupt hatten wir mal richtig Zeit f&uuml;reinander, weil sie nicht so viele Verpflichtungen hatte wie zu meinen fr&uuml;heren Besuchen. (Und die, die sie hatte, verpassten wir meist wegen des langsamen Busverkehrs. Noch mehr Zeit f&uuml;reinander.)</p>
<p>Dieses Mal habe ich nat&uuml;rlich sichergestellt, dass ich nicht wieder fliege, ohne ein Guinness getrunken zu haben. Auch am Gr&uuml;ndonnerstag[1] kann man noch Sitzpl&auml;tze in Pubs finden, wenn man ein wenig sucht.</p>
<p>Jetzt kenne ich auch das "Queen of Tarts", ein kleines Caf&eacute;, in dem Carolin etwas Geld verdient. Es ist total schnucklig, die Leute sind supernett und der Kuchen s&uuml;ndhaft lecker -- wer in Dublin einen Kaffee und s&uuml;&szlig;es Geb&auml;ck genie&szlig;en will, <em>muss</em> das Queen of Tarts ausprobieren!</p>
<p>Leider gab es in der kurzen Zeit keine Gelegenheit, Thomas und Silke zu treffen, die unabh&auml;ngig von mir das Osterwochenende ebenfalls f&uuml;r eine Dublin-Reise auserkoren hatten.</p>
<p>Inzwischen bin ich wieder zuhause, gew&ouml;hne mich an das Regenwetter und verdaue das leckere Ostermittagessen. Und beginne langsam wieder, Dublin und Caro zu vermissen...</p>
<p>fn1. Am Karfreitag haben die meisten L&auml;den und Pubs geschlossen, also gilt es am Vortag, noch einen auf Vorrat zu trinken...</p>
