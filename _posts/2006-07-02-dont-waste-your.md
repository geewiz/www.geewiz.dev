---
layout: post
title: "Don't waste your time in meetings"
microblog: false
guid: http://geewiz.micro.blog/2006/07/02/dont-waste-your.html
date: 2006-07-03T00:17:10+0200
type: post
url: /2006/07/02/dont-waste-your.html
---

<p>In <a href="http://37signals.com/svn/archives2/theres_no_such_thing_as_the_onehour_meeting.php">Theres no such thing as the one-hour meeting</a>, Jason Fried points out that a meeting of one hour with 5 people is a five hour meeting. I've had my share of frustrating  time-wasting meetings, but there also are colleagues I'll meet with gladly because I know that we'll be productive.</p>
<p>In the <a href="http://www.mlp.de">MLP</a> magazine FORUM, I found some basic hints how to make meetings successful. I wish everyone inviting me to meetings would have read them.</p>
<h1>
<em>Preparation:</em> Make clear the goal of the meeting. Define date and participants. Invite timely in writing. Prepare the room and visualization tools (e.g., flipchart).</h1>
<h1>
<em>Goal:</em> Express goal and agenda clearly. Prioritize agenda items.</h1>
<h1>
<em>Time frame:</em> Start punctual presenting the agenda. If the participants don't know each other, include an introduction round.</h1>
<h1>
<em>Participants:</em> Define who has to participate necessarily and for whom a copy of the minutes suffices. Inform the participants in writing.</h1>
<h1>
<em>Moderation:</em> The meeting leader takes on the moderation: he/she guides verbal contributions, attends to the time and structures the meeting.</h1>
<h1>
<em>Visualization:</em> Use a flipchart to visualize the most important issues. That way, the course of the meeting can be followed.</h1>
<h1>
<em>Pauses:</em> Long meetings need periodic pauses (at the latest after 90 or 120 minutes).</h1>
<h1>
<em>Disturbances:</em> Switch off mobile phones. The meeting leader has to firmly moderate distinguishing behaviour. Trouble makers should be addressed directly without marginalizing them.</h1>
<h1>
<em>Summary:</em> The meeting leader should summarize the results regularly, thus preventing misunderstandings.</h1>
<h1>
<em>Minutes:</em> As soon as possible, participants should get copies of the meeting minutes including an exact action plan: Who is supposed to do what until when?</h1>
