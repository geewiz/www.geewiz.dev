---
layout: post
title: "Summer has gone"
microblog: false
guid: http://geewiz.micro.blog/2007/10/01/summer-has-gone.html
date: 2007-10-01T23:03:04+0200
type: post
url: /2007/10/01/summer-has-gone.html
---

<p>The weather has already switched to autumn mode, and yesterday, <a href="http://www.bloekschaf.de/archives/127-Das-letzte-Eis-und-die-unfreundliche-Strassenbahnfahrerin.html">Carolin</a> and I did the switch in our heads, too. We visited "Eisland", our local ice cream parlor, for the last time this year. And not only was it the last day the shop opened -- we also managed to be their last customers for this year. They were just going to lock the doors when we rushed in.</p>
<p>Since Carolin had a part time job there, we know the owners and they gave us this year's last cups of their delicious ice cream for free. We enjoyed it with great devotion.</p>
<p>I'm looking forward to next spring when they'll be back from Italy to open "Eisland" again.</p>
