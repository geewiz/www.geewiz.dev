---
layout: post
title: "Konfigurationen verwalten mit SCPM"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/konfigurationen-verwalten-mit.html
date: 2007-09-24T22:01:55+0200
type: post
url: /2007/09/24/konfigurationen-verwalten-mit.html
---

<p>(This is a German article from my old homepage.)</p>
<p>Wer mit dem selben Rechner wechselweise in verschiedenen Netzwerken arbeitet, wird des Aufwands, immer wieder die Netzwerk-Konfiguration &auml;ndern zu m&uuml;ssen, schnell &uuml;berdr&uuml;ssig werden. Das betrifft vor allem Notebook-Anwender, die zum Beispiel sowohl in der Firma als auch zu Hause ein Netzwerk nutzen. Immer wieder m&uuml;ssen IP-Adresse, Netzwerkmaske, Nameserver-Adressen, Mail-Konfiguration usw. ge&auml;ndert werden, was manuell eine nervige Aufgabe ist. Abhilfe schafft SCPM.</p>
<p>SCPM ("System Configuration Profile Manager") ist ein Hilfsprogramm, das seit Version 8.1 zur SuSE Linux Distribution geh&ouml;rt. Es verwaltet unterschiedliche Netzwerk-Konfigurationen und erm&ouml;glicht es der Anwenderin, mit einem kurzen Kommando zwischen diesen Konfigurationen, "Profile" genannt, zu wechseln.</p>
<h2>Verwendung von SCPM</h2>
<p>In YaST2 findet sich SCPM unter dem Punkt "Profilmanager" im "System"-Men&uuml;. Im Terminal wird SCPM wie folgt verwendet.</p>
<p>Mit dem Kommando</p>
<p><code>scpm enable</code></p>
<p>muss SCPM vor der eigentlichen Nutzung einmalig aktiviert werden. An dieser Stelle wird das "Repository" angelegt, in dem SCPM die verschiedenen Konfigurations-Profile ablegt. Im Dateisystem findet sich das Repository unter /var/lib/scpm.</p>
<p>Ein zweites Mal l&auml;sst sich <code>scpm enable</code> nur mit der Zusatzoption <code>-f</code> ("force" = erzwingen) ausf&uuml;hren. Dann wird das Repository mit allen Profilen gel&ouml;scht und ein neues angelegt.</p>
<p><code>scpm add &laquo;profil&raquo;</code></p>
<p>f&uuml;gt die aktuelle Netzwerk-Konfiguration dem Repository unter dem Namen <code>&laquo;profil&raquo;</code> hinzu. Dies muss f&uuml;r jede Konfiguration nur ein Mal erfolgen. Eine Aktualisierung des Profils geh&ouml;rt zum Funktionsumfang des <code>switch</code> Kommandos.</p>
<div class="CodeRay">
<div class="code">
<pre>scpm switch &laquo;profil&raquo;</pre>
</div>
</div>
<p>&uuml;berschreibt die aktuelle Systemkonfiguration mit den Daten des anschlie&szlig;end aktiven Profils <code>&laquo;profil&raquo;</code>. Bei Bedarf<br />
wird dann das Netzwerk neu gestartet, damit die Konfigurations&auml;nderung zum Tragen kommt. </p>
<p>Sollte beim Aufruf von <code>scpm switch</code> die aktuelle Systemkonfiguration nicht den f&uuml;r das noch aktive Profil abgelegten  Daten entsprechen, fragt <code>scpm</code> f&uuml;r jede betroffene Datei ab, ob der neue Zustand im Repository abgelegt werden soll. Man kann dies nutzen, um das momentan aktive Profil auf den aktuellen Stand zu bringen, indem man mit <code>scpm switch</code> zum bereits aktiven Profil "wechselt".</p>
<p>Soll ein anderes Profil aktiviert werden, ohne die aktuelle Konfiguration zu speichern, ist die Option <code>-f</code> zu verwenden:</p>
<div class="CodeRay">
<div class="code">
<pre>scpm -f switch &laquo;profil&raquo;</pre>
</div>
</div>
<h2>Profil beim Booten ausw&auml;hlen</h2>
<p>Mit der Kerneloption <code>PROFILE</code> kann bereits beim Booten ein bestimmtes Profil ausgew&auml;hlt werden. Diese Kerneloption kann nach Bedarf im Men&uuml; des Bootloaders eingegeben oder in seiner Konfigurationsdatei (<code>/etc/lilo.conf</code> bzw. <code>/boot/grub/menu.lst</code>) fest eingestellt werden.</p>
<p>Bei GRUB k&ouml;nnte zum Beispiel ein entsprechender Eintrag so aussehen:</p>
<p><code></code></p>
<div class="CodeRay">
<div class="code">
<pre>title Office
  kernel (hd0,5) /boot/vmlinuz
  root=/dev/hda6 PROFILE=office
  initrd (hd0,5)/boot/initrd</pre>
</div>
</div>
<h2>Eigene Ressourcen definieren</h2>
<p>Welche Ressourcen SCPM verwaltet, wird in sogenannten "resource sets" (RS) definiert. Dies sind Textdateien, in denen die Pfade der Dateien aufgelistet sind, die SCPM in seiner Datenbank ablegen soll. Abgelegt sind die RS im Verzeichnis <code>/lib/scpm/resource_sets</code>. Hier sind bereits in der<br />
Grundinstallation mehrere verschiedene Dateien vorhanden, von denen gem&auml;&szlig; dem Eintrag <code>RESOURCE_SET="typical"</code> in der SCPM-Konfigurationsdatei <code>/etc/scpm.conf</code> die Variante "typical" verwendet wird.</p>
<p>Ein eigenes RS kann somit einfach dadurch definiert werden, dass man eine vorhandene Datei mit neuem Namen kopiert, sie modifiziert und in <code>/etc/scpm.conf</code> eintr&auml;gt.</p>
<p>Zum Beispiel enth&auml;lt das RS "typical" keinen Eintrag, der den Host- und Dateinamen des Rechners im Profil speichert. Somit ist es nicht m&ouml;glich, diese Namen zwischen Profilen zu variieren. Dies kann einfach behoben werden. Mit</p>
<p><code>cp typical myscpm</code></p>
<p>wird zun&auml;chst ein neues RS erzeugt, das dann auch in die Konfigurationsdatei /etc/scpm.conf einzutragen ist:</p>
<p><code>RESOURCE_SET="myscpm"</code></p>
<p>F&uuml;r die Verwaltung des Host- und Domainnamens muss eine Datei-Ressource <code>/etc/HOSTNAME</code> hinzugef&uuml;gt werden. Mit einem Texteditor ist also die folgende Zeile in <code>myscpm</code> einzutragen:</p>
<div class="CodeRay">
<div class="code">
<pre>file /etc/HOSTNAME</pre>
</div>
</div>
<p>Ab sofort verwaltet SCPM auch die Namensinformationen. </p>
