---
layout: post
title: "Herbstinfarkt"
microblog: false
guid: http://geewiz.micro.blog/2004/09/25/herbstinfarkt.html
date: 2004-09-25T03:25:00+0200
type: post
url: /2004/09/25/herbstinfarkt.html
---

<p>Sieht so aus, als w&auml;re es das gewesen mit dem Sommer 2004. Es wird Zeit, die Pullover aufzustocken und sich an fr&uuml;he Abendd&auml;mmerung und h&auml;ufigen Regen zu gew&ouml;hnen. Meine gute Laune ist zwar zu einem nicht zu vernachl&auml;ssigenden Teil solarbetrieben, aber depressiv werde ich jetzt auch nicht. Ich mach mirs halt zuhause gem&uuml;tlich und komme wohl wieder etwas mehr zum Lesen. Und dann sind da noch die zwei ersten Oktoberwochen, in denen ich meine Solarakkus auf Teneriffa aufladen werde :-)</p>
