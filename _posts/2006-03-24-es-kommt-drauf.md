---
layout: post
title: "Es kommt drauf an, was man draus macht"
microblog: false
guid: http://geewiz.micro.blog/2006/03/24/es-kommt-drauf.html
date: 2006-03-24T22:33:15+0200
type: post
url: /2006/03/24/es-kommt-drauf.html
---

<p>Was entsteht, wenn man normalen Baustoff mit Glasfasern kreuzt? <a href="http://www.techeblog.com/index.php/tech-gadget/light-transmitting-concrete">Lichtdurchl&auml;ssiger Beton</a>! Ich find's megacool, dass sowas wirklich m&ouml;glich ist. Da entstehen ganz neue M&ouml;glichkeiten der Architektur und Raumgestaltung.</p>
<p>N&auml;chster Schritt: lichtdurchl&auml;ssiges Aluminium!</p>
