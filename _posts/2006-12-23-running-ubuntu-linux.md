---
layout: post
title: "Running Ubuntu Linux on Mac OS X with Parallels"
microblog: false
guid: http://geewiz.micro.blog/2006/12/23/running-ubuntu-linux.html
date: 2006-12-23T23:15:08+0200
type: post
images:
- /uploads/parallels-ubuntu.png
photos:
url: /2006/12/23/running-ubuntu-linux.html
---

<p>Not only because I always have to have some Linux on my computer, but also because I often need a Linux system available for training and testing purposes, I installed <a href="http://www.parallels.com">Parallels Desktop for Mac</a> on my Mac Mini yesterday. Parallels provides a virtual environment for installing other operating systems like Windows or Linux[1]. Its virtual harddisk files, its installation wizard as well as the start, stop and restart buttons on the guest OS window very much remind of VMware, but with a license fee of 79 Euro, there's quite a price difference, isn't it?</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/parallels-ubuntu.png" />
</div>
<p>It took me 3 minutes to install Parallels -- you have to love the OS X installation process -- and then I tossed in the Ubuntu 6.06 server CD to install my first guest OS. After choosing <a href="http://forum.parallels.com">Debian Linux" in the installation wizard, Parallels suggested appropriate environment settings and I only reduced the virtual harddisk size to 3 GB[2]. Ubuntu installed flawlessly, but after restarting the virtual machine, the boot process stopped at "loading the kernel". A quick search on the "Parallels user forum</a> unearthed that you have to install the Ubuntu 686 kernel to fix that problem. So, after booting into the Ubuntu rescue shell and issuing <a href="http://forum.parallels.com/showthread.php?t=5136">sudo apt-get install linux-686" (as described in this "forum entry</a>), Ubuntu started into the login prompt as desired.</p>
<p>I'm quite impressed how easy it is to install and maintain virtual machines on Mac OS X using Parallels. I have no doubt that I'll gladly shell over the 79 Euro after the 2-week trial period is over.</p>
<p>fn1. Solaris, BSD and even OS/2 are supported, too.</p>
<p>fn2. I really need an additional harddrive.</p>
