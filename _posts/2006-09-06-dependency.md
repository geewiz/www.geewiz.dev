---
layout: post
title: "Dependency 2.0"
microblog: false
guid: http://geewiz.micro.blog/2006/09/06/dependency.html
date: 2006-09-06T19:59:15+0200
type: post
url: /2006/09/06/dependency.html
---

<p>One of the advantages of using more and more web applications to communicate, organize and get information is that you only need a web browser to do your daily tasks. The browser is the new desktop, what have been desktop icons became bookmarks. Things get easier. But there's a flip side.</p>
<p>What also changes with that movement to working and living online is the dependency on others.<br />
At this time of writing, I can't get to my task list because (once again...) all <a href="http://www.zenlist.com/">Zenlist</a> displays is "500 - Internal Server Error". That may buy me some time to write this blog entry, but when I'll have finally finished all the to-dos I can remember, I'll be stuck.</p>
<p>We're well used to depend on computers and the internet, but we tend to oversee the fact that these dependencies are growing. Think about what happened last time when your broadband connection broke down -- or worse, your computer. If you're already using VOIP services, next time something breaks you'll be cut off not only from email, but also from phone communication. Enjoy the silence.</p>
<p>Depending on hardware and infrastructure is one thing. We try to compensate by choosing high-quality products, known brands and service providers with a good reputation. There's choice, and that's good. But let's not forget that choice comes at a price. If you want to make an informed choice, you have to look at the alternatives, weigh advantages against weaknesses, look for experience reports and reviews to finally base your decision upon.</p>
<p>When we add the dependency on Web 2.0 application providers to the mix, we have to spend a lot more time on that decision process. Look at the overwhelming diversity of application providers: does anyone keep track of how many calendars and social bookmarking services are out there? I don't.</p>
<p>And when you've finally found the application that does exactly what you need it to, don't forget that (like in every relationship) reliability is an important aspect. Even if it's not a marriage for lifetime, every divorce will cost you dearly.</p>
