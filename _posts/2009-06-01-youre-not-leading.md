---
layout: post
title: "You're not leading when..."
microblog: false
guid: http://geewiz.micro.blog/2009/06/01/youre-not-leading.html
date: 2009-06-01T03:10:27+0200
type: post
url: /2009/06/01/youre-not-leading.html
---

<p>Reading some of the many books on leadership that are out there, you get a picture of how it should look like when you're leading. But are you also aware of how it looks when you're not in the lead? You better are, because your subordinates certainly do.</p>
<p>Jon Ferguson made an insightful list in his blog article</p>
<p><a href="http://jonferguson.typepad.com/lead/2008/01/you-know-youre.html">You know you are not leading when...</a>:</p>
<ul>
<li>You wait for someone to tell you what to do rather than taking the initiative yourself</li>
<li>You spend too much time talking about how things should be different</li>
<li>You blame the context, surroundings, or other people for your current situation</li>
<li>You choose not to speak the truth in love</li>
<li>You are more concerned about being cool or accepted than doing the right thing</li>
<li>You seek consensus, rather than casting vision for a preferable future</li>
<li>You aren't taking any significant risks</li>
<li>You accept status quo as the way it's always been and always will be</li>
<li>You start protecting your reputation instead of opening yourself up to opposition</li>
<li>You sleep a little too sound</li>
<li>You procrastinate to avoid making a tough call</li>
<li>You talk to others about the problem rather than taking it to the person responsible</li>
<li>You don't feel like your butt is on the line for anything significant</li>
<li>You think what you say doesn't matter</li>
<li>You ask for way too many opinions before taking action</li>
</ul>
<p>In short: You're appointed the leader, so <strong>act</strong> like one. You'll not be judged on your preparations but on your <strong>results</strong>.</p>
