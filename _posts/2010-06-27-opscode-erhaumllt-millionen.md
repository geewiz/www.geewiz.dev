---
layout: post
title: "Opscode erh&auml;lt 11 Millionen Dollar Venture-Kapital"
microblog: false
guid: http://geewiz.micro.blog/2010/06/27/opscode-erhaumllt-millionen.html
date: 2010-06-27T15:51:00+0200
type: post
url: /2010/06/27/opscode-erhaumllt-millionen.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_medium_quote"><p>Opscode, Inc., a cloud infrastructure automation company, announced today that it has closed an $11 million Series B round of funding. The round was led by Battery Ventures and includes a follow-on investment from Draper Fisher Jurvetson, who led Opscodeʼs Series A round of funding of $2.5 million, bringing the total amount raised for the company to $13.5 million. As part of this investment, Sunil Dhaliwal, a general partner at Battery Ventures will join Opscodeʼs Board of Directors. Proceeds from the new funds will be used to expand the companyʼs engineering staff, fuel research and development initiatives, and drive sales and marketing efforts.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.opscode.com/blog/2010/06/21/opscode-closes-11-million-series-b-round/">opscode.com</a>
</div>
<p>Nach Puppet steht jetzt auch Chef auf finanziell gesicherten Beinen.</p>
</div>
