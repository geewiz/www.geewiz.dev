---
layout: post
title: "Zu viel Hygiene schadet nur"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/zu-viel-hygiene.html
date: 2006-06-18T07:32:56+0200
type: post
url: /2006/06/18/zu-viel-hygiene.html
---

<p>Allergien sind eine Zivilisationserscheinung. Je h&ouml;her das Niveau einer Zivilisation ist, desto st&auml;rker treten Allergien und Autoimmunreaktionen[1] auf. Die Hypothese, dass dabei auch der gestiegene Grad an Hygiene eine Rolle spielt, scheint nun von einer neuen <a href="http://www.eurekalert.org/pub_releases/2006-06/dumc-wvl061406.php">Studie mit Wild- und Laborratten</a> gest&uuml;tzt zu werden.</p>
<p>Gerade die Immunstoffe, die bei den Laborratten zu Allergien und Autoimmunreaktionen f&uuml;hrten, waren bei den Wildratten in noch viel h&ouml;herem Ma&szlig;e zu finden. Trotzdem traten bei diesen keine solchen Fehlreaktionen auf.</p>
<p>Offenbar spielt die Umgebung und ihre Belastungen eine wichtige Rolle dabei, wie das Immunsystem reagiert. Salopp gesagt: Wenn das Immunsystem mit Erregern und Parasiten besch&auml;ftigt ist, muss es sich nicht durch Reaktionen auf Allerweltsstoffe wie Pollen, Lebensmittel oder Tierhaare austoben.</p>
<p>Aber auch genetische Einfl&uuml;sse spielen dabei eine Rolle; diese sollen nun n&auml;her untersucht werden.</p>
<p>[1] Reaktionen des Immunsystems, die gegen den eigenen K&ouml;rper statt gegen externe Erreger gerichtet sind.</p>
