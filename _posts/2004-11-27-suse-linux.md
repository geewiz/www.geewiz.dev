---
layout: post
title: "SuSE Linux 9.2"
microblog: false
guid: http://geewiz.micro.blog/2004/11/27/suse-linux.html
date: 2004-11-27T06:12:00+0200
type: post
url: /2004/11/27/suse-linux.html
---

<p>Nachdem in meinem Inspiron 8200 die Festplatte aufgegeben und Dell mir eine neue geliefert hatte, war eine Neuinstallation f&auml;llig. Ich habe die Gelegenheit genutzt, gleich auf das neue SuSE Linux 9.2 umzusteigen.</p>
<p>Auf den ersten Blick kommt mir die Distribution sehr gelungen vor. Die Installation war reibungslos, meine wichtigsten Software-Pakete sind bereits wieder in Betrieb.</p>
<p>Auch das neue KDE 3.3 scheint deutlich an Funktionen und Komfort zugelegt zu haben. Mal sehen, ob es mir auch auf Dauer  besser gef&auml;llt als der schlanke XFce4-Windowmanager.</p>
<p>Bisher scheint sich das Upgrade von 9.0 auf 9.2 also gelohnt zu haben. Mal sehen, wie ich in einigen Wochen dar&uuml;ber denke.</p>
