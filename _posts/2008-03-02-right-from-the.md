---
layout: post
title: "Right from the book"
microblog: false
guid: http://geewiz.micro.blog/2008/03/02/right-from-the.html
date: 2008-03-03T01:16:50+0200
type: post
url: /2008/03/02/right-from-the.html
---

<p><a href="http://www.blog.cbgreenwood.de/2008/02/18/123-buchstoeckchen.html">cbgreenwood</a> passed the meme on to me by asking me to cite the book I'm reading at the moment, exactly from page 123, three sentences from the fifth one on.</p>
<p>Let's see... Well, what a coincidence! I'm on page 123 right now! The book is "Mindset -- The new psychology of success" from Carol S. Dweck. She's a psychologist and writes about the difference between the "fixed mindset" that sees success as god-given and the "growth mindset" for which success comes from continuous effort. Here's the desired part:</p>
<blockquote class="posterous_medium_quote">
<p>Iacocca played painful games with his executives to keep them off balance. Jerry Levin of Time Warner was likened by his colleagues to the brutal roman emperor Caligula. Sklling was known for his harsh ridicule of those less intelligent than he.</p>
</blockquote>
<p>I guess they'll not be in my list of executive role models. :-)</p>
<p>Interesting what this meme brings forward! I'll pass it on to <a href="http://www.bloekschaf.de/archives/138-Eisland-Freiburg-Die-Eiszeit-hat-endlich-begonnen!.html">my precious</a>, <a href="http://blog.kai-uwe-beyer.de/index.php?/archives/653-Neuer-Arbeitgeber.html">Kai</a> and <a href="http://schimana.net/2008/02/produktivitaet/pagepacker-ausdrucke-im-pocket-format/">Tom</a>. What are you reading, folks?</p>
