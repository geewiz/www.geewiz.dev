---
layout: post
title: "Where the Magic Happens"
microblog: false
guid: http://geewiz.micro.blog/2020/01/13/where-the-magic.html
date: 2020-01-13T15:40:34+0200
type: post
url: /2020/01/13/where-the-magic.html
---

> Effortful study means constantly tackling problems at the very edge of your ability. Stuff you may have a high probability of failing at. Unless you're failing some of the time, you're probably not growing professionally. You have to seek out those challenges and push yourself beyond your comfort limit.
—Jeff Atwood, "How to Stop Sucking and Be Awesome Instead"
