---
layout: post
title: "Der Datenschutz br&ouml;ckelt"
microblog: false
guid: http://geewiz.micro.blog/2005/12/10/der-datenschutz-broumlckelt.html
date: 2005-12-10T02:17:31+0200
type: post
url: /2005/12/10/der-datenschutz-broumlckelt.html
---

<p>Peter Zimmermann, Landesbeauftragter f&uuml;r den Datenschutz in Baden-W&uuml;rttemberg, bef&uuml;rchtet eine zunehmende <a href="http://www.heise.de/newsticker/meldung/67192.">Erosion des Datenschutzes</a> Die Bef&uuml;rchtung teile ich, denn immer und immer wieder soll er zugunsten der "inneren Sicherheit" reduziert werden. Ich hab ja selten die Gelegenheit dazu, aber in dieser Frage zitiere ich mit voller Zustimmung einen amerikanischen Pr&auml;sidenten:</p>
<blockquote class="posterous_short_quote">
<p>"Wer Freiheit um der Sicherheit willen aufgibt, verliert am Ende beides." -- Benjamin Franklin</p>
</blockquote>
