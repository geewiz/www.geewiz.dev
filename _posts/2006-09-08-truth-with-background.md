---
layout: post
title: "Truth with background music"
microblog: false
guid: http://geewiz.micro.blog/2006/09/08/truth-with-background.html
date: 2006-09-09T00:57:44+0200
type: post
url: /2006/09/08/truth-with-background.html
---

<p>On yesterday's episode of the <a href="http://www.podshow.com/shows/?mode=detail">Daily Sourcecode</a>&amp;episode_id=25113, Adam Curry played a song that I liked immediately: "Underwear goes inside the pants" by LazyBoy. Like "Facts of Life", it's again speech -- I'd say a rant, to be exact -- combined with some backing beats and melody, and it's really resonating with me.</p>
<p>Download the song from the <a href="http://profile.myspace.com/index.cfm?fuseaction=user.viewProfile">LazyBoy MySpace page</a>&amp;friendID=74874292 and tell me if you like it!</p>
