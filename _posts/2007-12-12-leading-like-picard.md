---
layout: post
title: "Leading like Picard"
microblog: false
guid: http://geewiz.micro.blog/2007/12/12/leading-like-picard.html
date: 2007-12-12T09:41:02+0200
type: post
url: /2007/12/12/leading-like-picard.html
---

<p>With his blog entry <a href="http://schimana.net/2007/11/teams/der-fuehrungsstil-von-jean-luc-picard/">Der F&uuml;hrungsstil von Jean-Luc Picard</a> ("The leadership style of Jean-Luc Picard"), Thomas Schimana gave me some food for thought. As a fan of "Star Trek -- The Next Generation" owning the complete DVD collection, this immediately peaked my interest.</p>
<p>Thomas perceived the following: Most of the times when Captain Picard had to make a decision, he used to first ask his leading crew for their opinions. Everyone got their chance to suggest a course of action. Based on that, Picard reached his own decision that wasn't to be discussed any more. By all means, he was the one carrying the final responsibility. The command "make it so" was his signature finishing line under the decision process. </p>
<p>I've already realized years ago that Star Trek had instilled me with a deep interest in science and computers. But reading Thomas' article, it became clear to me that I've also adopted a leadership style similar to Picard's.</p>
<p>I'm deeply convinced that each of my directs is a specialist that can contribute to a solution and whose opinion has to be taken seriously. But at the end of the day, I'm the one responsible and also the one that's in the front line when the stuff hits the fan. Therefore, I claim the right to decide the course of action. Even if that course isn't backed enthusiasticly by my team, I expect them to pursue the plan as well as they can nonetheless. On the other hand, they can expect me to do everything in my might to support them in making it so.</p>
<p>So far, I've had amazing success with that approach. I've got a loyal and highly motivated team that I depend upon every day. Like Picard's crew, they're different from each other, but every one has his own features, know-how and experience to contribute. The important things don't happen in my secluded ready room -- we rock when we're moving things together, each at his place of the bridge.</p>
<p>And, other than Picard, I still haven't lost all of my hair over the job. :-)</p>
