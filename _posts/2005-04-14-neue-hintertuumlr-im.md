---
layout: post
title: "Neue Hintert&uuml;r im Internet Explorer"
microblog: false
guid: http://geewiz.micro.blog/2005/04/14/neue-hintertuumlr-im.html
date: 2005-04-14T20:45:16+0200
type: post
url: /2005/04/14/neue-hintertuumlr-im.html
---

<p>Mit dem dringenden Hinweis auf eine neue <a href="http://www.heise.de/newsticker/meldung/58544">Sicherheitsl&uuml;cke im IE</a>, die Angreifern Zugriff auf den eigenen PC gew&auml;hrt, nutze ich die Gelegenheit, mal wieder Werbung f&uuml;r <a href="http://www.mozilla.org">Firefox</a> zu machen.</p>
