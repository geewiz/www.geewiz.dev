---
layout: post
title: "Rackspace leases new data center"
microblog: false
guid: http://geewiz.micro.blog/2009/02/14/rackspace-leases-new.html
date: 2009-02-14T21:47:08+0200
type: post
url: /2009/02/14/rackspace-leases-new.html
---

<p>Since <a href="http://www.rackspace.com/">Rackspace Hosting</a> is running out of floor space in their primary data center, they had to decide between building a new location and to buy or lease existing data center space. </p>
<p>As <a href="http://www.datacenterknowledge.com/archives/2009/02/11/rackspace-expands-with-ashburn-data-center/">Data Center Knowledge</a> reports, they chose leasing:</p>
<blockquote class="posterous_medium_quote">
<p>The company considered building additional data center space in its new headquarters facility in a former shopping center in San Antonio, but later indicated that it believed it could save money by buying or leasing instead.</p>
</blockquote>
<p>Finally, the company decided on expanding by leasing a data center in Auburn, Virginia, because this "will enable it to serve customer demand more quickly and cost effectively than if Rackspace built its own facility".</p>
<p>Like after the first dot com bubble, there's unused data center space at many places because companies decided (and had to money) to build big. In the current economic situation, though, it seems wise to buy or lease that existing space instead of incurring huge new building costs.</p>
