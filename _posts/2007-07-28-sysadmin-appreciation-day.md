---
layout: post
title: "Sysadmin Appreciation Day"
microblog: false
guid: http://geewiz.micro.blog/2007/07/28/sysadmin-appreciation-day.html
date: 2007-07-28T03:25:40+0200
type: post
url: /2007/07/28/sysadmin-appreciation-day.html
---

<p>Today is <a href="http://www.sysadminday.com/">Sysadmin Appreciation Day</a>, the day where you're especially invited to tell your IT crowd where they're doing a good job. I'm embarassed that I forgot it myself, so I'm not in the position to criticize all the colleagues at my workplace that forgot it, too. Of course, I made an entry in my calendar, and dear customer department (you know who you are), you better do, too. Also, if you need some inspiration on how to show your appreciation of our hard work, find some <a href="http://www.sysadminday.com/gifts.html">useful hints</a> here.</p>
<p>For the german users that have a catholic education: Find the melody to sing the headline of the <a href="http://www.heise.de/newsticker/meldung/93328">article on Heise Online</a> in "Gotteslob", no. 258.</p>
