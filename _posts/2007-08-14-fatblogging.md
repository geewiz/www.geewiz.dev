---
layout: post
title: "Fatblogging: 93"
microblog: false
guid: http://geewiz.micro.blog/2007/08/14/fatblogging.html
date: 2007-08-14T07:46:03+0200
type: post
url: /2007/08/14/fatblogging.html
---

<p>You don't need to do the math: I've lost 7 kg so far. The secret of my recent weight loss: mountain hiking burns lots, lots, lots of calories! Just make sure you refill at least as much is needed to keep upright. :-)</p>
<p>Carolin and I loved the hiking so much that we decided to do some more over the summer. We're living right in Black Forest after all, so it really isn't hard to find a slopy way to use our new hiking boots for some hours. We won't climb summits, but we'll enjoy nature, spend time together and burn away unnecessary weight.</p>
<p>For the first time after the mountain tour, I went running this morning. I had to take a short break after 15 minutes, but I'm sure to be in shape to keep running for half an hour soon, even in the hillside. If my knees don't act up. <em>Update:</em> I just found RunningMap and immediately documented <a href="http://www.runningmap.com/?id=21096.">my route</a></p>
<p>So, everything is fine on the health front. Bye bye, belly fat!</p>
