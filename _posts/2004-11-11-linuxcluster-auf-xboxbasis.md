---
layout: post
title: "Linux-Cluster auf Xbox-Basis"
microblog: false
guid: http://geewiz.micro.blog/2004/11/11/linuxcluster-auf-xboxbasis.html
date: 2004-11-12T00:22:00+0200
type: post
url: /2004/11/11/linuxcluster-auf-xboxbasis.html
---

<p>Es ist ja seit l&auml;ngerem bekannt, dass man Linux auch auf der Xbox betreiben kann. Anandtech hat nun ein <a href="http://www.anandtech.com/linux/showdoc.aspx?i=2271">Cluster-Projekt</a> auf die Beine gestellt, bei dem sie 8 Konsolen zu einem leistungsf&auml;higen Rechensystem koppeln.</p>
<p>Bei einem Preis von $150 pro Knoten klingt das zun&auml;chst interessant, aber im Test zeigte sich, dass die sp&auml;rliche Hardware-Ausstattung (700MHz CPU, 64MB RAM) schnell zum Klotz am Bein wird.</p>
<p>Man darf jedoch gespannt sein, was das gleiche Projekt mit der kommenden Xbox 2 ergeben wird.</p>
