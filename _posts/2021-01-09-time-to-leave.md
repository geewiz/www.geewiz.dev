---
layout: post
title: "Time to leave WhatsApp"
microblog: false
guid: http://geewiz.micro.blog/2021/01/09/time-to-leave.html
date: 2021-01-09T17:07:00+0200
type: post
url: /2021/01/09/time-to-leave.html
---
<p>If you're using WhatsApp, you've probably received a notification that the messaging service is updating its terms of service, notably their privacy policy, effective February 8th and with no option to opt out. It has prompted me, like thousands of other WhatsApp users, to switch to a more privacy-oriented service.</p>

<p>Since WhatsApp was acquired by Facebook in 2014, it was to be expected that eventually, the internet behemoth known for its gluttony of personal information would extend its tendrils to WhatsApp user data. This point is now. I don't want Facebook to know more about me, my preferences and behaviours than I do myself. I will not support its practices of influencing its user base and deciding who gets to see what information. That's why I deleted my Facebook account months ago. And now I'm joining thousands of WhatsApp users in moving my messaging over to Signal.</p>

<p>Signal has a flawless reputation for data privacy and transparency. There are other alternatives, for example Apple Messages. <a href="https://twitter.com/mikebutcher/status/1346959937679798274">Mike Butcher's thread</a> on Twitter is a good read. He also posted a comparison of the amount of data these services track of their users; the difference is stark.

<p>If you're concerned about which information about you corporate giants track and use to their advantage (and I think you should be concerned), then use this opportunity to make a difference and help make the internet a safer space for human communication! <a href="https://freedom.press/news/signal-beginners/">Here's how.</a> Say hello to me over on Signal. 😉</p>

_Update 2021-01-11:_ I removed the mention of Telegram as a secure alternative because its app leaks unencrypted chat data to Telegram servers.
