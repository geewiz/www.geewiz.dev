---
layout: post
title: "Knappogue Castle 1993"
microblog: false
guid: http://geewiz.micro.blog/2005/04/19/knappogue-castle.html
date: 2005-04-19T07:51:19+0200
type: post
images:
- /uploads/Whiskey/knappoguecastle.jpg
photos:
- /uploads/Whiskey/knappoguecastle.jpg
url: /2005/04/19/knappogue-castle.html
---

<p>Heute habe ich den Knappogue Castle 1993 Very Special Reserve Single Malt Whiskey bekommen. Und gleich mit dem neuen Nosing Glass probiert. :)
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/Whiskey/knappoguecastle.jpg" />
</div></p>
<p>Die helle F&auml;rbung deutet schon einen leichten Whiskey an, und so kommt der Knappogue Castle auch in Geruch und Geschmack r&uuml;ber. In der Nase wirkt er frisch, fruchtig, mit leichter Gras- und Holznote. Sein Geschmack ist blumig und malzig, &uuml;brig bleibt davon ein trockener, w&uuml;rziger Nachgeschmack. </p>
<p>F&uuml;r ca. 32 Euro bekommt man mit dem Knappogue Castle Single Malt einen Whiskey, den ich als "elegant" bezeichnen m&ouml;chte. </p>
<p>Au&szlig;erdem erinnert er mich daran, wie Carolin vor dem Schloss im County Clare auf dem Rasen sitzt und ihre F&uuml;&szlig;e fotografiert. :)</p>
