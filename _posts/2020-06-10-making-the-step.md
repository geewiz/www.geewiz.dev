---
layout: post
title: "Making the step from being non-racist to being anti-racist"
microblog: false
guid: http://geewiz.micro.blog/2020/06/10/making-the-step.html
date: 2020-06-10T14:29:35+0200
type: post
url: /2020/06/10/making-the-step.html
---

For decades, I've lived a sheltered life as a person whom MLK describes as

> "the white moderate who is more devoted to “order” than to justice; who prefers a negative peace which is the absence of tension to a positive peace which is the presence of justice"

All this time, I stayed in my comfort zone and considered justice someone else's problem, probably something the state needs to take care of via foreign aid or policy.

Just like it took meeting my wife for me to learn that protecting our environment starts with me as an individual changing my consumption behaviour, it took the BLM movement and the recent riots for me to understand that I as an individual can be an ally, can contribute to racial justice, can speak up against discrimination. That being non-racist isn't enough, that I need to be anti-racist.

I have much to learn, and I am grateful for the many online communities that raise awareness, practice solidarity, and foster understanding in "non-racist" people like me.
