---
layout: post
title: "A year without a car"
microblog: false
guid: http://geewiz.micro.blog/2007/09/13/a-year-without.html
date: 2007-09-13T17:44:46+0200
type: post
url: /2007/09/13/a-year-without.html
---

<p>A year ago, when it had become clear that once we had moved to Freiburg, I wouldn't do the 150 km commute by car, I canceled my leasing contract and switched to public transportation.</p>
<p>I've got myself a "Bahncard 100" that enables me to take (almost) every Deutsche Bahn (German Railways) train I want to for a flat monthly fee. So, if I'd like to visit a friend in Hannover or to go to the next Barcamp Cologne, I just hop on the train. The monthly rate for the Bahncard 100 is about the same as my leasing fee was, and there are no extra costs for fuel, insurance, repair, parking etc. Even with the occasional taxi, I spend less money for the train than I used to for the car.</p>
<p>Although my commute now is five times longer than it was when I lived in Philippsburg, it is much more environmentally friendly. I now share the same vehicle with hundreds of other passengers.</p>
<p>It takes me about an hour to get to work, and over this time I don't sit behind a wheel cursing at other drivers but at my laptop, doing some work, typing blog entries like this one, reading my Twitter stream, or just relaxing, listening to a podcast or audio book. Thanks to my in-ear earbuds, I don't get disturbed by crying babies, loudly talking business buffoons or other crazy people.</p>
<p>Of course, not having a car has its downsides. If I want to go on an IKEA rampage or buy five crates of beer, I need to rent a car or ask a friend to help me out. In urgent cases, I have to get a taxi. And there's always the train schedule that I have to adhere to. Sometimes, the train doesn't.</p>
<p>But over all, it's simply comfortable to sit in a soft chair and wait until the conductor has taken me there. I don't miss the car.</p>
