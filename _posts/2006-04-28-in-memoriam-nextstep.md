---
layout: post
title: "In memoriam: NeXTstep"
microblog: false
guid: http://geewiz.micro.blog/2006/04/28/in-memoriam-nextstep.html
date: 2006-04-29T00:38:19+0200
type: post
url: /2006/04/28/in-memoriam-nextstep.html
---

<p>Auf YouTube habe ich heute die <a href="http://www.youtube.com/watch?v=j02b8Fuz73A">Demonstration einer NeXT-Workstation</a>&amp;search=steve%20jobs durch ihren Erfinder (und heutigen Apple-Vorstand) Steve Jobs gefunden.</p>
<p>Ich bin zutiefst beeindruckt, welche Ideen und Technologien es schon Anfang der 90er gab. Dass das NeXTstep-Betriebssystem sich nicht durchgesetzt hat, ist eine Schande.</p>
<p>Die <a href="http://www.macprime.ch/applehistory/stories/next/">Geschichte der Firma NeXT</a> von der Gr&uuml;ndung &uuml;ber ihr Scheitern bis hin zu ihrem Aufgehen in Apple mit deren Mac OS X ist auf MacPrime zu finden.</p>
