---
layout: post
title: "More admins wanted!"
microblog: false
guid: http://geewiz.micro.blog/2006/08/11/more-admins-wanted.html
date: 2006-08-11T18:38:02+0200
type: post
url: /2006/08/11/more-admins-wanted.html
---

<p>Recently, I posted about my <a href="http://blog.jochen-lillich.de/archives/432-Linux-admins-wanted">search for Linux system administrators</a>!.html, and it's continuing! The first Linux admin position is filled, but there still are free ones! </p>
<p>Additionally -- and this may be shocking to those who know me better[1] -- I'm looking for Windows admins that know their ways about IIS and .NET.</p>
<p>All positions are full time, located at 1&amp;1 headquarters in Karlsruhe, with high expectations and responsibility, but with a fun and learning factor that I would rate at least as high. So keep the CVs coming and tell your friends!</p>
<p>BTW: I can only second Matt Asays article about <a href="http://asay.blogspot.com/2006/08/hiring-well.html.">hiring decisions</a> Hiring unproven talents that are shapeable over performers that may be hard to bear is a strategy that worked out well for me every time.</p>
<p>fn1. It surely was shocking to me.</p>
