---
layout: post
title: "The placebo pill called CCTV"
microblog: false
guid: http://geewiz.micro.blog/2008/05/26/the-placebo-pill.html
date: 2008-05-26T02:19:55+0200
type: post
url: /2008/05/26/the-placebo-pill.html
---

<p>Recently, I noticed that the tram I was taking from work to the train station, had more than 10 ceiling cams installed. That's one camera every 2 meters. Oh, did my feeling of security grow instantly.</p>
<p>Meanwhile, the UK recognizes that installing "Closed Circuit TV" cameras is just a waste of money. As the Guardian titles, "<a href="http://www.guardian.co.uk/uk/2008/may/06/ukcrime1">CCTV boom has failed to slash crime, say police</a>".</p>
<p>Meanwhile, my commuting costs will increase because german transportation companies think playing "1984" was a better use for my money than improving my traveling experience.</p>
