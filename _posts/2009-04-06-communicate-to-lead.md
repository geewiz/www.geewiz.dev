---
layout: post
title: "Communicate to lead"
microblog: false
guid: http://geewiz.micro.blog/2009/04/06/communicate-to-lead.html
date: 2009-04-06T05:28:54+0200
type: post
url: /2009/04/06/communicate-to-lead.html
---

<p>Communication is (finally) one of my most important topics at work. <a href="http://www.allthingsworkplace.com/2009/04/nothing-happens-until-people-talk.html">Steve Roesler</a> recommends to "add these four thoughts to your leadership communication kit", and I can't emphasize his points enough:</p>
<ul>
<li>Never assume that anyone knows anything.</li>
<li>The larger the group, the more attention needs to be given to communicating.</li>
<li>When left in the dark, people will fantasize their own reality. Do you want their fantasy to trump your reality?</li>
<li>Effective leaders are obsessed with accurate, frequent communication.</li>
</ul>
