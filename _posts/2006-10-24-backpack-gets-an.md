---
layout: post
title: "Backpack gets an overhaul"
microblog: false
guid: http://geewiz.micro.blog/2006/10/24/backpack-gets-an.html
date: 2006-10-24T07:27:26+0200
type: post
url: /2006/10/24/backpack-gets-an.html
---

<p>37signals announced new goodies in <a href="http://backpackit.com/?referrer=BPBMZZ9">Backpack</a>, their online organization tool for to-do and idea lists, notes, pictures, texts and appointments. There are no big new features, but the existing parts get quite an improvement:</p>
<p>Up until now, on every Backpack page, you had one block of lists, one block of notes, and so on. Lists could only be reordered inside their block, likewise with notes etc. Now, you can mix all different information types at your liking: a list, followed by some notes, then another list, some photos -- you get the picture. In summary: page layout in Backpack gets a lot more flexible.</p>
<p>For a video demonstration, see the <a href="http://www.37signals.com/svn/posts/65-a-preview-of-the-new-backpack">preview</a> of the new reorganization possibilities.</p>
<p>I've been using a paid Backpack account for over half a year now, and seeing how 37signals gradually improve their products with sensible additions, I'll gladly prolong that subscription.</p>
