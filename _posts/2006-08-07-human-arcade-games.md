---
layout: post
title: "Human arcade games"
microblog: false
guid: http://geewiz.micro.blog/2006/08/07/human-arcade-games.html
date: 2006-08-07T02:14:04+0200
type: post
url: /2006/08/07/human-arcade-games.html
---

<p>I'm cleaning out my to-blog-list, so you may already have heard of some of my forthcoming topics. But I'd like to mention them nevertheless.</p>
<p>I'm a huge fan of classic arcade games, and I had a lot of fun at <a href="http://www.retrogames.info.">Retrogames</a> It's certainly more fun to play "Galaxian" or "Street Fighter" on the original console than running MAME.</p>
<p>But you don't need either of these: Some folks at last years Swiss Belluard festival made the effort of using stop-motion technique to create sequences of the games <a href="http://www.notsonoisy.com/pong/index.html">Pong</a> and <a href="http://www.notsonoisy.com/spaceinvaders/index.html.">Space Invaders</a> Nice work!</p>
<p>I'll be extra careful next time I go to the cinema. Don't want to get in the way of a gigantic Pac-Man.</p>
