---
layout: post
title: "Unerreichter Bewerber"
microblog: false
guid: http://geewiz.micro.blog/2006/05/31/unerreichter-bewerber.html
date: 2006-05-31T20:26:17+0200
type: post
url: /2006/05/31/unerreichter-bewerber.html
---

<p>Nur mal so als Tipp: wenn man sich als Betreiber eines Webdienstes f&uuml;r Wegwerf-Mailadressen auf eine Stelle als Sysadmin bewirbt, hat man gar keine so schlechten Chancen. Es sei denn, dank einer solchen Adresse erreicht einen die Einladung zum Gespr&auml;ch nicht und man ist auch auf der einzig angegebenen Handynummer nicht erreichbar.</p>
<p>Schade, echt.</p>
