---
layout: post
title: "Working on tasks in the right context"
microblog: false
guid: http://geewiz.micro.blog/2006/07/19/working-on-tasks.html
date: 2006-07-19T23:03:54+0200
type: post
url: /2006/07/19/working-on-tasks.html
---

<p>Lars Trieloff ponders upon a <a href="http://weblogs.goshaky.com/weblogs/page/lars?entry=a_context_aware_to_do.">context-aware to-do-list system</a> Actually, dividing tasks into contexts so that each task can be dealt with when you're at the best time and place for it is a basic principle in David Allens <a href="http://www.amazon.de/gp/product/0142000280/geewizweblog-21.">Getting Things Done</a></p>
<p>I've been practising the GTD approach for some time now and it helps me in a great way to keep focus on what has to be done, when it has to be done and where I can do it.</p>
<p>I use <a href="http://blog.jochen-lillich.de/archives/290-Backpack-it">Backpack</a>!.html to manage my task lists. Every context (at home, at work, places i can use a phone, etc.) has its own page with a to-do-list there. Every task finished gets ticked off, and the next day, I delete (with great pleasure) all these old tasks.</p>
<p>With <a href="http://www.rousette.org.uk/projects/">Tracks</a>, there's even a special software to practise GTD online. I haven't tried it yet, but it looks promising.</p>
