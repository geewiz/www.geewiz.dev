---
layout: post
title: "Kritischer F&uuml;hrungsnachwuchs"
microblog: false
guid: http://geewiz.micro.blog/2005/08/01/kritischer-fuumlhrungsnachwuchs.html
date: 2005-08-01T22:10:57+0200
type: post
url: /2005/08/01/kritischer-fuumlhrungsnachwuchs.html
---

<p>Viele junge F&uuml;hrungskr&auml;fte sind hochmotiviert, erhalten aber oft nicht die Unterst&uuml;tzung, die sie sich w&uuml;nschen. Diese Aussage eines <a href="http://www.spiegel.de/unispiegel/jobundberuf/0">Spiegel-Artikels</a>,1518,367424,00.html kann ich best&auml;tigen. Vieles, was mir hilft, meine Aufgabe gut zu erf&uuml;llen, habe ich nicht im Beruf, sondern in der kirchlichen Jugendarbeit gelernt.</p>
