---
layout: post
title: "Meetingless Standups"
microblog: false
guid: http://geewiz.micro.blog/2013/05/13/meetingless-standups.html
date: 2013-05-13T12:40:44+0200
type: post
url: /2013/05/13/meetingless-standups.html
---
<p>Keeping the whole team in the loop about what its members are currently busy with is essential for effective collaboration. Especially for distributed teams. For them, the most common method, the daily Stand-up Meeting, doesn't work as well as it does with co-located teams. At freistil IT, we've replaced them with <a href="http://www.freistil.it/2013/04/this-week-at-freistil-it-week-160/">daily status check emails</a>.</p>
<p>In his blog post "<a href="http://weblog.alexgodin.com/kill-your-standup">Kill your standup</a>, Alex Godin describes a variant of the email approach practised at Dispatch.io they call "Show and Tell". Obviously, email as a communication tool is far from dead.</p>
