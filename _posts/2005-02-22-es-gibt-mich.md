---
layout: post
title: "Es gibt mich noch"
microblog: false
guid: http://geewiz.micro.blog/2005/02/22/es-gibt-mich.html
date: 2005-02-22T20:08:00+0200
type: post
url: /2005/02/22/es-gibt-mich.html
---

<p>Aber durch den Umzug bin ich momentan ziemlich besch&auml;ftigt. Am Samstag wollten wir urspr&uuml;nglich die sperrigen M&ouml;bel transportieren, wegen Thomas' Grippe mussten wir das aber auf n&auml;chstes Wochenende verschieben. Bis dahin werde ich halt noch einiges Kleinzeug im Auto transportieren.</p>
<p>Am Sonntag verschob ich auf einem Webserver aus Versehen ein Verzeichnis, woraus ein Hilfsprogramm die falschen Schl&uuml;sse zog und kurzerhand alle Mailkonten l&ouml;schte. Es ist mal wieder verbl&uuml;ffend, wie gut der <a href="http://www.unitedmedia.com/comics/dilbert/archive/dilbert-20050213.html">Dilbert</a> vom gleichen Tag dazu passt.</p>
<p>Auf der positiven Seite kann ich die Bewertung nach dem ersten Jahr als Teamleiter verbuchen.</p>
<p>Fazit: Ich hab genug zu tun, bin aber gut gelaunt. Also alles beim Alten. Nur, dass ich inzwischen 35 bin. :)</p>
