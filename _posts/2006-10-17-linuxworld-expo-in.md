---
layout: post
title: "LinuxWorld Expo in Cologne"
microblog: false
guid: http://geewiz.micro.blog/2006/10/17/linuxworld-expo-in.html
date: 2006-10-18T00:18:03+0200
type: post
url: /2006/10/17/linuxworld-expo-in.html
---

<p>Since I'll be attending the <a href="http://www.linuxworldexpo.de">LinuxWorld Conference &amp; Expo</a> on 14th and 15th of November, I added an entry on <a href="http://upcoming.org/event/118196/.">upcoming.org</a></p>
<p>So, if you're going to be there, too, please add yourself to the event entry. It would be great to meet in Cologne!</p>
