---
layout: post
title: "Eire, a r&uacute;n mo chro&iacute;"
microblog: false
guid: http://geewiz.micro.blog/2005/08/01/eire-a-ruacuten.html
date: 2005-08-01T20:04:11+0200
type: post
url: /2005/08/01/eire-a-ruacuten.html
---

<p>Wenn ich den <a href="http://www.spiegel.de/reise/kurztrip/0">Artikel</a>,1518,366156,00.html auf Spiegel Online lese, dann packt mich mal wieder die Sehnsucht nach der gr&uuml;nen Insel. Ich kann die Beschreibung voll best&auml;tigen: bei meinem letzten Besuch habe ich den Charme der kleinen Inseln im Westen selbst erlebt.</p>
<p>Tom wird mit Silke ja demn&auml;chst einen Kurztrip nach Irland machen -- mein Neid ist ihnen sicher. Aber nachdem Carolin ja zum Wintersemester ins Trinity College nach Dublin wechselt, werde ich sicherlich bald Gelegenheit finden, die Insel wiederzusehen und meine Vorr&auml;te an Lebenswasser aufzustocken.</p>
