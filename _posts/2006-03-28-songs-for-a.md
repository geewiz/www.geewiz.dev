---
layout: post
title: "Songs for a new IT generation"
microblog: false
guid: http://geewiz.micro.blog/2006/03/28/songs-for-a.html
date: 2006-03-28T20:06:13+0200
type: post
url: /2006/03/28/songs-for-a.html
---

<p>Auf unserer B&uuml;roetage scheint es Probleme mit dem Abfluss zu geben. Deshalb wurde uns bereits mehrfach angedroht, die Toiletten zu verschlie&szlig;en, wenn nicht ordentlich gesp&uuml;lt werde.</p>
<p>Mein Vorschlag: Musikeinspielung gegen Vergesslichkeit. Im Normalzustand der Toiletten empfehle ich "Push the button" von den Sugababes.</p>
<p>&Uuml;ber einen Wassersensor k&ouml;nnte man dann im Notfall auf "The tide is high" oder "Hier kommt die Flut" umschalten.</p>
