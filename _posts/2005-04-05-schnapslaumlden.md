---
layout: post
title: "Schnapsl&auml;den"
microblog: false
guid: http://geewiz.micro.blog/2005/04/05/schnapslaumlden.html
date: 2005-04-05T20:28:56+0200
type: post
url: /2005/04/05/schnapslaumlden.html
---

<p>Ich habe zwei weitere L&auml;den zum Erwerb leckerer Spirituosen im Web entdeckt.</p>
<p><a href="http://www.celticwhiskeyshop.com">F&aacute;ilte agus go n-&eacute;ir&iacute; an b&oacute;thar leat" -- so begr&uuml;&szlig;t der "Celtic Whiskey Shop</a> seine Kunden. Hier gibt es eine gro&szlig;e Auswahl an Irish Whiskeys. Kein Wunder, denn die Firma hat ihren Sitz in Irland. Auch wer mal etwas mehr Geld in Whiskey anlegen will, findet hier <a href="http://www.celticwhiskeyshop.com/asp/object_presentation.asp?ObjectID=676">passende Exemplare</a>&amp;Mode=0&amp;RecordID=660.</p>
<p><a href="http://www.whiskywizard.de">WhiskyWizard.de</a> ist offensichtlich eine deutsche Website. Auch hier gibt es eine gro&szlig;e Anzahl Whiskys, sowohl der irischen als auch der schottischen Art. Hier habe ich gerade den "Knappogue Castle 1992 Special Reserve" entdeckt und bestellt.</p>
