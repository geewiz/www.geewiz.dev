---
layout: post
title: "The IT Crowd"
microblog: false
guid: http://geewiz.micro.blog/2006/03/22/the-it-crowd.html
date: 2006-03-22T20:17:17+0200
type: post
url: /2006/03/22/the-it-crowd.html
---

<p>Als die britische Sitcom <a href="http://blog.jochen-lillich.de/archives/235-Britische-Sitcom-ueber-die-Geeks-im-Keller.html">The IT Crowd" angek&uuml;ndigt wurde, war ich "sehr gespannt</a>, wie die Serie ausfallen w&uuml;rde. Inzwischen hab ich die ersten 5 Episoden per Bittorrent heruntergeladen und angeschaut.<br />
Kennern britischer Sitcoms w&uuml;rde ich "The IT Crowd" als Mischung aus "Father Ted" und "Black Books", versetzt in die IT-Welt, beschreiben. Die Akteure werden unglaublich &uuml;berzeichnet dargestellt; so besteht die IT-Truppe aus einem Gamer, dessen T-Shirt noch Reste der vorletzten Pizza enth&auml;lt, einem Obernerd, der noch von Mama angezogen wird, und einer netten Chefin, die aber keine Ahnung von Computern hat. Ihr Vorgesetzter wiederum hat keine Ahnung, was die IT eigentlich tut und beschr&auml;nkt sich auf selbstherrliche Gesten. W&auml;hrend er in einer lichtdurchfluteten Etage arbeitet, wurde die IT in ein Kellerkabuff verbannt. (Ein weiterer zum Keller passender Verbannter st&ouml;&szlig;t nach zwei oder drei Episoden noch dazu.)</p>
<p>Als ITler habe ich viele lustige Details entdeckt, die sich oft im Hintergrund verstecken. Nat&uuml;rlich gefallen mir die Running Gags, z.B. die st&auml;ndig wiederkehrende Telefonmeldung "Hello, IT. Have you tried turning it off and on again?". Und wenn bei der Projektabschlussparty der Chef t&ouml;nt "And of course we couldn't have done it without the people that support us day and night: the... cleaning staff!" und die ITler dabei stehen wie bestellt und nicht abgeholt, dann jubelt meine gequ&auml;lte Admin-Seele.</p>
<p>Ich kann auch nicht verhehlen, dass sich die Gags teilweise schnell abnutzen. Aber hey, so &auml;hnlich wie Father Dougal oder Manny (aus den oben genannten Serien) immer wieder zum Br&uuml;llen sind, ist auch "The IT Crowd" einfach nette Unterhaltung -- und vor allem die Best&auml;tigung, dass es einem in der Firma noch viel, viel schlechter gehen k&ouml;nnte. :-)</p>
