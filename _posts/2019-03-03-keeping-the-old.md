---
layout: post
title: "Keeping the old gears grinding"
microblog: false
guid: http://geewiz.micro.blog/2019/03/03/keeping-the-old.html
date: 2019-03-03T17:11:57+0200
type: post
url: /2019/03/03/keeping-the-old.html
---
> Video games good for building focus create environments that are fast-paced, interactive, adaptive and have complex reward and gaming structures. Like a brain playground.

So at my age it's basically mandatory to keep playing WoW!
