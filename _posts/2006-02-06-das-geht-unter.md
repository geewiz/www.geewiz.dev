---
layout: post
title: "Das geht unter die Haut"
microblog: false
guid: http://geewiz.micro.blog/2006/02/06/das-geht-unter.html
date: 2006-02-06T08:49:33+0200
type: post
url: /2006/02/06/das-geht-unter.html
---

<p>Wechselbare Skins f&uuml;r Softwareapplikationen sind ja nichts besonderes mehr. Jetzt gibt es auch Skins f&uuml;r Laptops. Und ich meine <a href="http://www.skinbag.net/skinbag-gb/fiches/F-computer.html">Skin" im w&ouml;rtlichen Sinne. Hallo, liebe Hellraiser-Fans da draussen: es gibt jetzt "Laptoptaschen aus synthetischer Menschenhaut</a>!</p>
