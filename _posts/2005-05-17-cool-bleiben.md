---
layout: post
title: "Cool bleiben"
microblog: false
guid: http://geewiz.micro.blog/2005/05/17/cool-bleiben.html
date: 2005-05-17T07:56:28+0200
type: post
images:
- /uploads/webdepizza.serendipityThumb.jpg
photos:
- /uploads/webdepizza.serendipityThumb.jpg
url: /2005/05/17/cool-bleiben.html
---

<p>Kaum war ich am Freitag beim CAJ-Pfingsttreff bei Lahr angekommen, klingelte das Telefon. Aufgrund des Datums hatte Martin zun&auml;chst etwas Schwierigkeiten, mit seiner Meldung, WEB.DE sei komplett ausgefallen, ernst genommen zu werden. Aber 10 Minuten sp&auml;ter sa&szlig; ich wieder im Auto...Am Abend war die RZ-Temperatur durch den Ausfall des Klimasystems so weit angestiegen, dass der gr&ouml;&szlig;te Teil der Server heruntergefahren werden musste. Bis ich in Karlsruhe angekommen war, waren schon etwa 10 Kollegen vor Ort und die Arbeiten am Klimasystem im Gange. Parallel dazu lief die Planung des Wiederanlaufs -- mehrere hundert von einander abh&auml;ngige Server schaltet man ja nicht nur einfach wieder ein. Als die K&uuml;hlung wieder gew&auml;hrleistet war, begann der konzertierte Start der einzelnen Systeme, der sich &uuml;ber einige Stunden hinweg erstreckte. Gegen vier Uhr am Morgen war Freemail wieder erreichbar und wenig sp&auml;ter ganz WEB.DE.</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/webdepizza.serendipityThumb.jpg" />
</div>
<p>Ohne Mampf bekanntlich kein Kampf, und wir waren bestens versorgt. :) Doch nicht nur deshalb hat mir die ganze Aktion gefallen. Im Spa&szlig; gebe ich zwar manchmal den Seufzer "Einmal mit Profis arbeiten..." von mir, aber am Freitag abend w&auml;re mir der nicht in den Sinn gekommen. Teamwork in Reinkultur, ich bin echt beeindruckt.</p>
<p>(Weitere Impressionen bei <a href="http://blog.koehntopp.de/archives/794-Danach....html.">Isotopp</a>)</p>
