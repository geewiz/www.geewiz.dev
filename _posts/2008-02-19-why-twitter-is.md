---
layout: post
title: "Why Twitter is worth the risk"
microblog: false
guid: http://geewiz.micro.blog/2008/02/19/why-twitter-is.html
date: 2008-02-19T04:43:17+0200
type: post
url: /2008/02/19/why-twitter-is.html
---

<p>I've been using <a href="http://www.twitter.com/Geewiz">Twitter</a> for many months now and it became a standard communication tool for me. Not everyone likes the service, though. Many people think of it as a continuous source of worthless distractions. And indeed, it can seriously disrupt your work flow.</p>
<p>Since fellow geek <a href="http://schimana.net/">Tom Schimana</a> discovered Twitter himself recently and is <a href="http://schimana.net/2008/02/kommunikation/twitter/">asking for help</a>, I'll write a bit about why I use Twitter despite this risk.</p>
<p>Twitter asks its users: "What are you doing?" and puts a 140 character limit to your answers. Those "tweets" get sent to everyone who subscribed to your Twitter account, either by the Twitter website, via email or over SMS sent to their mobile phone.</p>
<p>You can also mark tweets that are meant for a certain person by starting them with "@username" (an at sign followed by their Twitter name). If you'd like to send them a non-public message, start it with "d username".</p>
<p>In a way, Twitter is the virtual equivalent to your pub at the corner. You periodically spend some time there and, over time, start conversations and learn to know the other regulars.  Sometimes, you even engage in a deeper discussion, but most of the time it's just smalltalk. Nonetheless, you learn about what people are doing, what happens in the community or in the lives of your friends.</p>
<p>Twitter does the same for you, but you are able to choose the people whose messages you get. It's your decision who you'd like to "follow", as Twitter puts it. By posting what you're doing or thinking, you let people participate in a little bit of your life. Over time, your followers will recognize things they share with you, be it that you are a Mac user or going to be a parent. You can direct people to interesting websites, maybe your own. Pose a question and often you'll get responses from your followers in a matter of minutes. </p>
<p>On Twitter and its complementary services like <a href="http://www.twittermap.com">TwitterMap</a>, I found a blogger meetup in my home town and gained contact to the local Barcamp scene. I even found new business partners by answering quickly to their requests for support and then switching to IM or email.</p>
<p>Twitter isn't an Instant Messaging service, though. A publicly held dialogue will soon annoy your other followers and make them think about "unfollowing" you. It's considered as quite rude as is talking loudly to someone in a pub so that anyone can't help but overhear you.</p>
<p>So, just give Twitter a try and your network some time to develop. Find interesting people to follow and share interesting or entertaining bits with your followers. As long as you contribute information more valuable than "Going to the loo.", you'll build new connections that can in turn be interesting and useful to you.</p>
<p>And don't forget to click "Follow" on my <a href="http://www.twitter.com/Geewiz">Twitter page</a>! ;-)</p>
