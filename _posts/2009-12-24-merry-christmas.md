---
layout: post
title: "Merry Christmas!"
microblog: false
guid: http://geewiz.micro.blog/2009/12/24/merry-christmas.html
date: 2009-12-24T21:56:57+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm1static_ocjjy-scaled500.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpfarm1static_ocjjy-scaled500.jpg?w=300
url: /2009/12/24/merry-christmas.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm1static_ocjjy" height="331" src="/assets/wp-content/2011/05/media_httpfarm1static_ocjjy-scaled500.jpg?w=300" width="500" />
</div></p>
<p style="font-size:.8em;">(Image courtesy of <a href="http://www.flickr.com/photos/tommyhj/">Dr. Hemmert</a>)</p>
<p>Another year comes to an end, and it's been a good one. So I say Thank You to all my friends and readers and to everyone who I met and had a good time during this year. </p>
<p>May the blessing of Christmas be upon you! Enjoy the holidays and have a good start into a new year called 2010.</p>
<div style="text-align:center;">
<p>Have yourself a merry little Christmas,<br />
Let your heart be light<br />
From now on,<br />
our troubles will be out of sight</p>
<p>Have yourself a merry little Christmas,<br />
Make the Yule-tide gay,<br />
From now on, <br />
our troubles will be miles away.</p>
<p>Here we are as in olden days,<br />
Happy golden days of yore.<br />
Faithful friends who are dear to us<br />
Gather near to us once more.</p>
<p>Through the years <br />
We all will be together,<br />
If the Fates allow<br />
Hang a shining star upon the highest bough.<br />
And have yourself A merry little Christmas now.</p>
</div>
