---
layout: post
title: "Cloud-Plattform von IBM: \"Smart Business Systems\""
microblog: false
guid: http://geewiz.micro.blog/2010/03/23/cloudplattform-von-ibm.html
date: 2010-03-23T11:32:00+0200
type: post
url: /2010/03/23/cloudplattform-von-ibm.html
---

<div class="posterous_bookmarklet_entry">
<blockquote>
<div>
<p>Mit den Smart Business Systems stellt IBM integrierte Plattformen f&uuml;r die Servicebereitstellung inkl. Managementfunktionen f&uuml;r Hardware, Speicher, Netze, Virtualisierung und Services bereit, mit denen Lastoptimierte Systeme aufgesetzt werden k&ouml;nnen.</p>
<p>Zu den aktuellen Angeboten geh&ouml;ren die IBM CloudBurst&trade; Family (IBM CloudBurst 1.1) und die WebSphere CloudBurst Appliance.</p>
</div>
</blockquote>
<div class="posterous_quote_citation">via <a href="http://clouduser.org/2010/03/22/ibm-smart-business-systems/">clouduser.org</a></div>
<p>&nbsp;</p>
</div>
