---
layout: post
title: "Die Letzten werden die Ersten sein"
microblog: false
guid: http://geewiz.micro.blog/2006/02/18/die-letzten-werden.html
date: 2006-02-18T06:02:19+0200
type: post
url: /2006/02/18/die-letzten-werden.html
---

<p>Heute erinnerte mich eine Diskussionen im Gesch&auml;ft an eine bekannte Bibelstelle im Evangelium nach Matth&auml;us (Kapitel 20).</p>
<blockquote>
<p>Denn das Himmelreich gleicht einem Hausherrn, der fr&uuml;h am Morgen ausging, um Arbeiter f&uuml;r seinen Weinberg einzustellen. Und als er mit den Arbeitern einig wurde &uuml;ber einen Silbergroschen als Tagelohn, sandte er sie in seinen Weinberg.</p>
<p>Und er ging aus um die dritte Stunde und sah andere m&uuml;&szlig;ig auf dem Markt stehen und sprach zu ihnen: Geht ihr auch hin in den Weinberg; ich will euch geben, was recht ist. Und sie gingen hin.</p>
<p>Abermals ging er aus um die sechste und um die neunte Stunde und tat dasselbe. Um die elfte Stunde aber ging er aus und fand andere und sprach zu ihnen: Was steht ihr den ganzen Tag m&uuml;&szlig;ig da? Sie sprachen zu ihm: Es hat uns niemand eingestellt. Er sprach zu ihnen: Geht ihr auch hin in den Weinberg. </p>
<p>Als es nun Abend wurde, sprach der Herr des Weinbergs zu seinem Verwalter: Ruf die Arbeiter und gib ihnen den Lohn und fang an bei den letzten bis zu den ersten. Da kamen, die um die elfte Stunde eingestellt waren, und jeder empfing seinen Silbergroschen. </p>
<p>Als aber die ersten kamen, meinten sie, sie w&uuml;rden mehr empfangen; und auch sie empfingen ein jeder seinen Silbergroschen. Und als sie den empfingen, murrten sie gegen den Hausherrn und sprachen: Diese letzten haben nur eine Stunde gearbeitet, doch du hast sie uns gleichgestellt, die wir des Tages Last und Hitze getragen haben. Er antwortete aber und sagte zu einem von ihnen: Mein Freund, ich tu dir nicht Unrecht. Bist du nicht mit mir einig geworden &uuml;ber einen Silbergroschen? Nimm, was dein ist, und geh! Ich will aber diesem letzten dasselbe geben wie dir. Oder habe ich nicht Macht zu tun, was ich will, mit dem, was mein ist? Siehst du scheel drein, weil ich so g&uuml;tig bin? So werden die Letzten die Ersten und die Ersten die Letzten sein.</p>
</blockquote>
<p>Ich lasse den Text unkommentiert stehen. Er ist aber ein paar Minuten Nachdenken wert.</p>
