---
layout: post
title: "Probleme mit Cisco VPN-Client"
microblog: false
guid: http://geewiz.micro.blog/2005/12/11/probleme-mit-cisco.html
date: 2005-12-11T08:15:55+0200
type: post
url: /2005/12/11/probleme-mit-cisco.html
---

<p>Unter SUSE Linux 10 lie&szlig; sich der VPN-Client von Cisco, den ich f&uuml;r die Fernadministration brauche, nicht mehr compilieren. Etwas Recherche im Web brachte wie immer die L&ouml;sung. Was man bei der Installation des Clients beachten muss, habe ich jetzt mal (vor allem als Ged&auml;chtnisst&uuml;tze f&uuml;r mich) im <a href="http://wiki.jochen-lillich.de/?CiscoVPNClient">Wiki</a> dokumentiert.</p>
