---
layout: post
title: "Does Nokia make me more mobile than Apple?"
microblog: false
guid: http://geewiz.micro.blog/2007/10/22/does-nokia-make.html
date: 2007-10-22T17:30:39+0200
type: post
url: /2007/10/22/does-nokia-make.html
---

<p>Of course, I pondered buying an iPhone when it enters the German market in November. But I decided that it won't improve my mobile productivity in a way that justifies the money spent. I agree that the iPhone has the most advanced user interface of all smartphones. But the lack of third party software is a killer. Even if it is true that Apple will release a software development kit in 2008, I'm convinced that they will control which applications will be made available over the iTunes store. I doubt that there will be an IM application while AT&amp;T or T-Mobile want to sell SMS.</p>
<p>I've been using the Nokia E61 for many months now and I like how it's equipped with 3G, WiFi, a decent browser and a QWERTZ keyboard. That Nokia doesn't prevent me from installing new software enabled me to add applications for IM and RSS feed reading. Because I can do small things online without lugging around my MacBook Pro, I'm very happy with the phone.</p>
<p>Based on this experience, if I wanted to spend money on a new gadget (which curiously isn't the case, seems like I'm saturated ATM), I'd rather consider the Nokia N810 than the iPhone. It's a small tablet with a touch screen like the N770 and the N800. The screen of the N810 has been improved, though, and the device now also sports a slide-out keyboard for easier text input.</p>
<p>For more information, take a look over at <a href="http://www.internettablettalk.com/2007/10/17/the-nokia-n810-internet-tablet/">Internet Table Talk</a>.</p>
