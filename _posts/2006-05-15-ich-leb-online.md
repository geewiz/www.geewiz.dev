---
layout: post
title: "Ich leb online"
microblog: false
guid: http://geewiz.micro.blog/2006/05/15/ich-leb-online.html
date: 2006-05-15T21:32:13+0200
type: post
url: /2006/05/15/ich-leb-online.html
---

<p>Viele Dinge, f&uuml;r die ich einen Computer benutze, erledige ich nicht mehr mit Hilfe von Anwendungen auf meinem Rechner, sondern &uuml;ber das WWW. In letzter Zeit ist mir erst so richtig klar geworden, dass ich immer mehr auf Webanwendungen und Webdienste zur&uuml;ckgreife. Das "Web 2.0" mit seinen neuen M&ouml;glichkeiten, komfortable Dienste &uuml;ber das Internet anzubieten, tr&auml;gt dazu erheblich bei. Meine Erfahrungen mit diesen Angeboten werde ich ab sofort unter dem Label "Ich leb online", kurz "ILO", bloggen.<br />
Nachtr&auml;glich stelle ich schon einmal folgende Artikel unter die &Uuml;berschrift "Ich leb online":</p>
<ul>
<li>
<a href="http://blog.jochen-lillich.de/archives/290-Backpack-it">Backpack it!</a>!.html</li>
<li><a href="http://blog.jochen-lillich.de/archives/358-Projektmanagement-mal-einfach-Basecamp.html">Projektmanagement mal einfach:  Basecamp</a></li>
</ul>
