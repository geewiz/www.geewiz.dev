---
layout: post
title: "A great tutorial for creating Chef cookbooks"
microblog: false
guid: http://geewiz.micro.blog/2011/09/11/a-great-tutorial.html
date: 2011-09-11T15:48:25+0200
type: post
url: /2011/09/11/a-great-tutorial.html
---
<p>System administrators who are looking for a tool that helps them automating their maintenance tasks and have no or only little experience with <a href="http://wiki.opscode.com/display/chef/Home">Chef</a> should really take a look at Joshua Timberman's great tutorial "<a href="http://jtimberman.posterous.com/guide-to-writing-chef-cookbooks">Guide to Writing Chef Cookbooks</a>".</p>
<p>In his article, Joshua describes all steps he takes to create a new Chef cookbook that installs and maintains smartmontools (a set of tools to monitor hard disk health). It's a great example how straightforward it is to automate systems operations tasks with Chef.</p>
<p>Even with two years experience in using Chef, I learned one or two bits from this tutorial. And it just so happened this week that I needed a smartmontools cookbook. So, thanks twice for writing this up, Joshua!</p>
