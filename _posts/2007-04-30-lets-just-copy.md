---
layout: post
title: "Let's just copy the whole thing"
microblog: false
guid: http://geewiz.micro.blog/2007/04/30/lets-just-copy.html
date: 2007-04-30T19:18:35+0200
type: post
url: /2007/04/30/lets-just-copy.html
---

<p>Imitation is the sincerest form of flattery. Noone knows that better than successful internet services. That's why there are clones of Slashdot (I developed <a href="http://web.archive.org/web/19990421094855/http://www.linuxbbs.org/">one</a> myself in 1999 -- my first weblog), clones of Digg, and now clones of Twitter, too.</p>
<p>But there's a line between imitation and outright plagiarism, and this line certainly has been crossed in my opinion when German Twitter clone Wamadu not only copied Twitter's functions and looks, but also the complete <a href="http://groups.google.com/group/twitter-development-talk/web/api-documentation.">Twitter API Documentation</a> Even if you don't know German, the headlines alone give away that <a href="http://www.wamadu.de/info/site/api">Wamadu's equivalent</a> is a blatant copy.</p>
<p>Where Twitter's documentation ends with</p>
<blockquote class="posterous_short_quote">
<p>This document is authored and maintained by Alex Payne, an engineer working on Twitter at Obvious.</p>
</blockquote>
<p>the Wamadu texts finishes</p>
<blockquote class="posterous_short_quote">
<p>Dieses Dokument wurde von thorsten, einem der wamadu-Programmierer erstellt und wird auch von ihm aktualisiert.</p>
</blockquote>
<p>Someone should explain to "thorsten" that there's a thing called copyright. And maybe, other things like ethics and self-respect.</p>
