---
layout: post
title: "Unscheduled Downtime"
microblog: false
guid: http://geewiz.micro.blog/2006/02/24/unscheduled-downtime.html
date: 2006-02-24T23:16:30+0200
type: post
url: /2006/02/24/unscheduled-downtime.html
---

<p>Meine <a href="http://blog.jochen-lillich.de/archives/274-IBM.html">M&uuml;digkeit</a> zu Beginn dieser Woche hing wohl mit einem Anflug von Erk&auml;ltung zusammen. Gestern begann mein Tag mit Halskratzen, am Vormittag kam Schnupfen dazu und beim Mittagessen fing mein Kopf an zu schmerzen. Als ich wenig sp&auml;ter feststellen musste, dass ich mich nicht mehr konzentrieren konnte, brach ich kurzerhand die Verarbeitung ab, fuhr nach Hause und legte mich ins Bett.</p>
<p>Was dann kam, war eine Folge[1] "Robbi, Tobbi und das Fliewat&uuml;t" und viel, viel Schlaf. Heute bin ich wieder fit und beschwerdefrei.</p>
<p>fn1. DVD; Geburtstagsgeschenk meines Bruders.</p>
