---
layout: post
title: "Wasser des Lebens"
microblog: false
guid: http://geewiz.micro.blog/2005/01/19/wasser-des-lebens.html
date: 2005-01-19T22:54:00+0200
type: post
url: /2005/01/19/wasser-des-lebens.html
---

<p>Kollege <a href="http://blog.koehntopp.de/archives/641-Statt-Blumen.html">Isotopp</a> brachte k&uuml;rzlich das Thema Whisky aufs Tapet. Da fiel mir auf, dass ich noch gar nichts &uuml;ber eines meiner liebsten Hobbies geschrieben habe: irische Whiskeys.</p>
<p>Was den Irish Whiskey ausser dem zus&auml;tzlichen <a href="http://www.irlandseiten.de/irland/Whiskey/whiskey.html">e" noch von anderen Whiskys unterscheidet, ist auf den "Irlandseiten</a> beschrieben.</p>
<p>Ich selbst bin durch meinen Bruder und vor allem durch meine Aufenthalte in Irland zum Irish Whiskey Fan geworden. Inzwischen habe ich st&auml;ndig eine Auswahl davon im Schrank stehen.</p>
<p>Mein aktueller Bestand:</p>
<ul>
<li>Jameson 12yrs.</li>
<li>Jameson Destillery Reserve</li>
<li>Midleton Very Rare</li>
<li>Tullamore Dew</li>
<li>Locke's</li>
<li>Dalwhinnie (okay, ein Au&szlig;enseiter)</li>
</ul>
<p>Nicht mehr vorhandene Sorten:</p>
<ul>
<li>John Power &amp; Sons</li>
</ul>
<p>Irish Whiskey ist nicht nur was f&uuml;rs Alleine-Trinken. Wer also gern mal mit mir ein "uisce beatha" genie&szlig;en will, ist herzlich willkommen.</p>
