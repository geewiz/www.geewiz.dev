---
layout: post
title: "Wohnung in Freiburg gesucht"
microblog: false
guid: http://geewiz.micro.blog/2006/08/07/wohnung-in-freiburg.html
date: 2006-08-07T02:53:49+0200
type: post
url: /2006/08/07/wohnung-in-freiburg.html
---

<p>Wenn Carolin im Oktober aus Irland zur&uuml;ckkommt, wollen wir endlich eine gemeinsame Wohnung beziehen. Nach 8 Jahren ist das gar kein so unsinniger Schritt. :-)</p>
<p>Freiburg ist nicht nur eine interessante Stadt in wundersch&ouml;ner Umgebung, sondern auch der Ort, wo Carolin ihre letzten Semester absolviert. Deswegen haben wir jetzt angefangen, dort nach einer Wohnung zu suchen. Daf&uuml;r will ich neben den &uuml;blichen Websites auch mein Blog nutzen. Also:</p>
<p>Wenn du von einer netten 2- oder 2,5-Zimmer-Wohnung in Freiburg weisst, die zum Oktober frei wird, dann schreib mir bitte eine <a href="http://www.jochen-lillich.de/contact.">E-Mail</a> Sie sollte WG-tauglich geschnitten sein, weil wir eigene Zimmer bevorzugen. Und wenn sie dann noch f&uuml;r das Halten einer Katze geeignet ist, ist unser Zuschlag so gut wie sicher!</p>
