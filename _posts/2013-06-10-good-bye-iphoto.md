---
layout: post
title: "Good bye, iPhoto."
microblog: false
guid: http://geewiz.micro.blog/2013/06/10/good-bye-iphoto.html
date: 2013-06-10T11:46:00+0200
type: post
url: /2013/06/10/good-bye-iphoto.html
---
<p>As a long-time disciple of Jobs, I use Mac OS applications for almost every task. And if there's a solution from Apple, it's normally the first I try out and probably use. Photo archiving is no exception -- well, <em>was</em> no exception. I've been using iPhoto for quite some time and over many a version jump.</p>
<p>But recently, I've noticed that iPhoto doesn't quite fit my requirements as much as I'd like it to. Especially, I missed having access to my photo library from every device I use. I'd like to be able to process new photos on the fast Mac Mini, sort them into albums during work breaks on my Macbook and show off the latest cute picture of my adorable baby son on my iPad over coffee.</p>
<p>I felt that a change was necessary and it was finally triggered by Sven Fechner's blog post "<a href="http://simplicitybliss.com/2012/09/exporting-your-iphoto-library-to-dropbox/">Exporting your iPhoto library to Dropbox</a>". Using the tools described in the article, I was able to export all my photos together with their metadata into <code>Dropbox/Pictures/Library</code>. To be exact, they're sorted into subfolders for every month. And all pictures that Dropbox downloads from my iPhone or that I put into <code>Dropbox/Camera uploads</code> manually get automatically sorted into the right month folders by this magic fairy named <a href="http://www.noodlesoft.com/hazel.php">Hazel</a>.</p>
<p>Other people are obviously doing the same. Just while Phoshare was exporting my iPhoto library, a blog post by Panayotis Vryonis appeared in my feeds. And the subtitle after <a href="http://blog.vrypan.net/2013/5/20/leaving-iphoto-for-dropbox/">Leaving iPhoto for Dropbox</a> puts it very succinctly: "from feature rich to future proof". Yes, iPhoto has many useful features and Dropbox won't apply face recognition to automatically tag photos with names. On the other hand, I'm now independent and can choose whatever tool -- and device! -- I like to manage my photos. That's what made it worthwhile for me to spend 20 minutes on exporting all my photos to Dropbox.</p>
