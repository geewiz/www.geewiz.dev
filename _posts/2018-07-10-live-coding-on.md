---
layout: post
title: "Live coding on Twitch"
microblog: false
guid: http://geewiz.micro.blog/2018/07/10/live-coding-on.html
date: 2018-07-10T02:00:00+0200
type: post
url: /2018/07/10/live-coding-on.html
---

After migrating my blog from WordPress to Jekyll (which was long overdue), I'm going to pick up writing again. This first new post is easy, because I've already written it for my company blog. It's about my live coding stream on Twitch that I've been doing for a while now. It's called ["Full Stack Live"](https://www.twitch.tv/fullstacklive), a fitting name (I think) for a stream that covers all kinds of DevOps topics from Rails coding to operating Kubernetes (coming soon!).

If you're interested in why I'm taking my daily work to the digital stage, you can read all about it in my article ["Turning 'Working Out Loud' to 11: Live coding on Twitch"](https://blog.freistil.it/turning-working-out-loud-to-11-live-coding-on-twitch-7dface39203e).

So, if you're interested in DevOps topics and/or would like to watch me struggle and (sometimes) succeed, hop on over on Twitch and [follow my channel](https://www.twitch.tv/fullstacklive)! When I'm back from my holidays in late July, I'm going to be live coding again every Tuesday and Thursday afternoon.
