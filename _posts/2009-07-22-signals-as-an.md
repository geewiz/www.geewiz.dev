---
layout: post
title: "37signals as an example for working in a virtual team"
microblog: false
guid: http://geewiz.micro.blog/2009/07/22/signals-as-an.html
date: 2009-07-22T13:33:30+0200
type: post
url: /2009/07/22/signals-as-an.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_short_quote"><p>At 37signals I really feel more connected and current with what is going on than in any physical workplace I&rsquo;ve been a part of. It is effortless to keep up with what my co-workers are doing and how what I&rsquo;m doing contributes to the whole. I&rsquo;m free to keep up with projects and learn new skills as they fit my interests. We collaborate how and when it makes sense, and stay away from each other when that&rsquo;s the best way to work. That makes for a really effective working environment.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.37signals.com/svn/posts/1785-working-at-37signals">37signals.com</a></div>
<p>I'm impressed how the team at 37signals uses web-based tools (and eats their own dog food in the process) to connect and collaborate.</p>
<p>I know exactly what he means by "Campfire never clicked for me despite a couple of attempts to bring it into a team workflow." I also tried to establish this chat tool at work, but experienced massive pushback twice.</p>
</div>
