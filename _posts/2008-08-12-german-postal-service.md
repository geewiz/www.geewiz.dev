---
layout: post
title: "German Postal Service answers important questions"
microblog: false
guid: http://geewiz.micro.blog/2008/08/12/german-postal-service.html
date: 2008-08-12T06:58:47+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm4static_xeaqq-scaled500.png?w=200
photos:
url: /2008/08/12/german-postal-service.html
---

<p>If you enter the search keyword "Antwort" (answer) on the <a href="http://www.deutschepost.de/">German Postal Service's website</a>, you get more answers than you expected:</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm4static_xeaqq" height="480" src="/assets/wp-content/2011/05/media_httpfarm4static_xeaqq-scaled500.png?w=200" width="320" />
</div></p>
<p>The third search result proves that some web designers certainly are hoopy froods that know where their towels are.</p>
