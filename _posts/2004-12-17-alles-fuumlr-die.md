---
layout: post
title: "Alles f&uuml;r die Sicherheit"
microblog: false
guid: http://geewiz.micro.blog/2004/12/17/alles-fuumlr-die.html
date: 2004-12-17T07:48:00+0200
type: post
url: /2004/12/17/alles-fuumlr-die.html
---

<p>Mit Sabine Leutheusser-Schnarrenberger ist es jetzt sogar eine ehemalige Justizministerin, die in der &Ouml;ffentlichkeit die Infragestellung grundlegender Verfassungswerte durch die Bundesregierung kritisiert.</p>
<p>Ich verfolge schon seit langem mit Sorge, wie immer mehr Grundrechte eingeschr&auml;nkt werden -- angeblich zu Gunsten der Terrorismusbek&auml;mpfung und damit der Sicherheit des Volkes.</p>
<p>"Wer besch&uuml;tzt uns vor denen, die uns besch&uuml;tzen?" frage ich mich aber, wenn ich sehe, wie &uuml;berall Kameras auftauchen, wie man Provider zur Installation von Ger&auml;ten zur E-Mail-Einsicht verpflichtet und wie &uuml;berhaupt der Datenschutz immer mehr ausgeh&ouml;hlt wird.</p>
<p>Wenn Frau Leutheusser-Schnarrenberger sagt, dass "immer intensivere Eingriffe in die Freiheit aller ganz selbstverst&auml;ndlich in Kauf genommen" werden, obwohl die Bedrohung in Deutschland kaum konkret zu fassen ist, kann ich ihr nur zustimmen.</p>
<p>Und das Traurigste ist, dass diese v&ouml;llig berechtigte Kritik einer FDP-Politikerin an eine rot-gr&uuml;ne Regierung gerichtet werden muss.</p>
<p>(via <a href="http://www.heise.de/newsticker/meldung/54355">Heise</a>)</p>
