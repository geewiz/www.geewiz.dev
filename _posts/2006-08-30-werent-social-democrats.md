---
layout: post
title: "Weren't social democrats supposed to be left-wing?"
microblog: false
guid: http://geewiz.micro.blog/2006/08/30/werent-social-democrats.html
date: 2006-08-30T19:07:52+0200
type: post
url: /2006/08/30/werent-social-democrats.html
---

<p>Kai Raven draws the following conclusion from an <a href="http://rabe.supersized.org/archives/803-Politiker-wie-Wiefelspuetz.html">interview with Dieter Wiefelsp&uuml;tz</a>, speaker on domestic policies of the Social Democratic Party (SPD), about public surveillance against terrorist actions:</p>
<blockquote class="posterous_medium_quote">
<p>Terms like "proportion", "purpose", "efficiency" and "control" were heard not a single time, them being only barricades that keep the state -- that in the eyes of that kind of politicians can only act as a prevention and surveillance state -- from fulfilling his duties.</p>
</blockquote>
<p>It makes me sick that politicians are actually prepared to propose actions that could be taken right out of "1984", but it downright worries me when those people don't even come from right-wing parties, but from the "social democratic" one.</p>
