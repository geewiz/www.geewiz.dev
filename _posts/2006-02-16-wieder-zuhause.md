---
layout: post
title: "Wieder zuhause"
microblog: false
guid: http://geewiz.micro.blog/2006/02/16/wieder-zuhause.html
date: 2006-02-16T21:29:27+0200
type: post
url: /2006/02/16/wieder-zuhause.html
---

<p>Gerade habe ich erfolgreich meine O2-Homezone von Durlach in die Brauerstra&szlig;e 48 verlegt. Erwartungsgem&auml;&szlig; hat sich meine Karlsruher Telefonnummer dabei nicht ge&auml;ndert, ich bin also ab sofort wieder unter der gewohnten Nummer zu erreichen.</p>
