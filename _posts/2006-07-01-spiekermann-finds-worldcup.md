---
layout: post
title: "Spiekermann finds Worldcup 2006 design embarrassing"
microblog: false
guid: http://geewiz.micro.blog/2006/07/01/spiekermann-finds-worldcup.html
date: 2006-07-01T23:19:31+0200
type: post
url: /2006/07/01/spiekermann-finds-worldcup.html
---

<p>The Germans obviously are good in organizing and participating in the football Worldcup. But they suck in designing for it. At least, that's the opinion of Erik Spiekermann, one of Germany's most respected designers.</p>
<p>In an <a href="http://www.dw-world.de/dw/article/0">interview with Deutsche Welle</a>,2144,2049898,00.html, he stated that both the mascot and the logo are a poor result typical for "too many cooks in the kitchen".</p>
<blockquote class="posterous_medium_quote">
<p>It's a shame because when people come in from the outside world they think this is how German designers are and for me, it's personally embarrassing. I want to go away and hide and pretend I'm a brain surgeon or something.</p>
</blockquote>
