---
layout: post
title: "Echtzeit-Planetarium mit Onyx"
microblog: false
guid: http://geewiz.micro.blog/2005/01/21/echtzeitplanetarium-mit-onyx.html
date: 2005-01-21T21:34:00+0200
type: post
url: /2005/01/21/echtzeitplanetarium-mit-onyx.html
---

<p>Um neue Welten zu erkunden, m&uuml;ssen Besucher der Olympischen Sommerspiele 2008 nur ins neue <a href="http://www.newscom.com/cgi-bin/prnh/20050120/SFTH021">Planetarium</a> in Peking gehen. Es ist das erste digitale Kuppel-Planetarium mit Echtzeit-Laserprojektion hochaufl&ouml;sender Farbbilder, darunter 37000 Sterne und 30000 Galaxien.</p>
<p>Gesteuert wird das Zeiss-Laserprojektionssystem von einer SGI Onyx mit 24 Prozessoren, 12GB RAM und 6 InfiniteReality4 Grafik-Pipelines, jede mit 1GB Texturspeicher. Zus&auml;tzlich steht eine 3-Pipeline Onyx mit InfiniteReality3 Grafik f&uuml;r die Entwicklung und Vorschau von Shows zur Verf&uuml;gung.</p>
<p>(via <a href="http://www.siliconbunny.com/news/archives/000131.shtml">SiliconBunny</a>)</p>
