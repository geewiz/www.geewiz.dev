---
layout: post
title: "Leaving on a jet plane"
microblog: false
guid: http://geewiz.micro.blog/2005/03/15/leaving-on-a.html
date: 2005-03-16T01:18:10+0200
type: post
url: /2005/03/15/leaving-on-a.html
---

<p>Carolin fliegt heute nach Namibia. Es ist ihr tats&auml;chlich noch knapp vor dem neuen Semester gelungen, ein Praktikum in ihrem afrikanischen Lieblingsland zu bekommen. Ich freu mich f&uuml;r sie, bei mir schleicht sich aber schon auch mal der Gedanke ein: "Warum musste es ausgerechnet eine angehende Ethnologin sein?" </p>
<p>Wieder mal hunderte von Kilometern Entfernung, f&uuml;r 3 Monate. Da kann man nur aufs Internet hoffen.</p>
<p>So kiss me and smile for me...</p>
