---
layout: post
title: "Auf zu Schlund"
microblog: false
guid: http://geewiz.micro.blog/2005/10/30/auf-zu-schlund.html
date: 2005-10-30T22:02:45+0200
type: post
images:
- /uploads/jochenistlieb.jpg
photos:
- /uploads/jochenistlieb.jpg
url: /2005/10/30/auf-zu-schlund.html
---

<p>Vor 6 oder 7 Jahren kam mein erster Webserver bei Schlund + Partner unter (damals noch in ein Regal des klimatisierten Server-Hinterzimmers), jetzt folgt ihm sein Besitzer.Wie <a href="http://felix.pfefferkorn.de/blog/archives/100-Ich-werde-Schlundi.html">Felix</a> und meine anderen Kollegen aus der IT werde ich ab Dienstag Angesteller der Schlund + Partner AG sein. Dass wir mit dem WEB.DE Portal unter die Fittiche der United Internet AG wechseln w&uuml;rden, war ja schon lange klar, und in einer Betriebsversammlung am letzten Donnerstag wurde uns erkl&auml;rt, wie die einzelnen betroffenen Abteilungen auf die Firmen der Unternehmensgruppe verteilt werden. Da Schlund f&uuml;r deren IT-Infrastruktur zust&auml;ndig ist, ist dort der richtige Platz f&uuml;r uns.</p>
<p>Von Langeweile im Job kann also nach wie vor nicht die Rede sein. Ich freue mich auf die neue Umgebung und die neuen Kollegen. Und auf den Umzug unseres Rechenzentrums bin ich echt gespannt, das wird eine Herausforderung in ganz neuer Gr&ouml;&szlig;enordnung.</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/jochenistlieb.jpg" />
</div>
<p>Ich finde es &uuml;brigens sch&ouml;n, dass mein bisheriger Arbeitgeber weiss, dass ich lieb bin. Jetzt weiss es auch mein Brieftr&auml;ger. :)</p>
