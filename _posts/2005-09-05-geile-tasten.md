---
layout: post
title: "Geile Tasten"
microblog: false
guid: http://geewiz.micro.blog/2005/09/05/geile-tasten.html
date: 2005-09-05T18:45:20+0200
type: post
url: /2005/09/05/geile-tasten.html
---

<p>Seit gestern sind die ersten Demos des neuen Yamaha-Flaggschiffs <a href="http://www.yamaha-tyros.de/">Tyros2</a> online. Auch wenn ich davon absehe, dass man den Sprecher auf jeden Fall f&uuml;r die Pr&auml;sentation des ersten bemannten Interstellarflugs buchen sollte, sind die Demos recht eindrucksvoll. Gitarrensound mit zuf&auml;llig eingestreuten Griffger&auml;uschen ist echt cool.</p>
<p>Den Traum meiner schlaflosen N&auml;chte fand ich in der neuen <a href="http://www.okey-online.de">OKEY!</a> vorgestellt: die Lionstracs <a href="http://www.lionstracs.com">Mediastation</a> ist ein 76-Tasten-Keyboard mit allem Schnickschnack -- und sie l&auml;uft unter Linux!</p>
<p>Leider liegt bei beiden Instrumenten auch der Preis absolut in der Oberliga...</p>
