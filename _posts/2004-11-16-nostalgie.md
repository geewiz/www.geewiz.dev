---
layout: post
title: "Nostalgie"
microblog: false
guid: http://geewiz.micro.blog/2004/11/16/nostalgie.html
date: 2004-11-17T01:54:00+0200
type: post
url: /2004/11/16/nostalgie.html
---

<p>Es gibt Krankheiten, die tauchen immer mal wieder auf, oft ausgel&ouml;st durch eine Kleinigkeit. Computer-Nostalgie ist sowas.Phase 1, Infektion: K&uuml;rzlich bin ich beim Rausgehen an einer SGI Indy vorbeigelaufen, die nur noch zu Demozwecken bei uns in der Abteilung steht. Und prompt kamen Erinnerungen an die guten alten Zeiten vor 10 Jahren hoch, als ich das Gl&uuml;ck hatte, an Silicon Graphics Workstations -- damals der Inbegriff grafischer Potenz -- zu arbeiten. Neben der beeindruckenden Leistung im Vergleich zum heimischen 150MHz-PC war es das Design der Geh&auml;use, das einem sofort auffiel. Selbst heute sehen die Kisten gegen&uuml;ber dem 0815-PC noch einfach nur gut aus.</p>
<p>Phase 2, Inkubation: Ich schaute mal bei Ebay nach, was gebrauchte SGI-Workstations denn heutzutage kosten. Auch hier kennt der Preisverfall keine Gnade: Ger&auml;te, f&uuml;r die einmal f&uuml;nfstellige DM-Betr&auml;ge investiert wurden, gehen heute f&uuml;r wenige hundert Euro &uuml;ber die Theke. Interessant...</p>
<p>Phase 3, klare Symptome: Ich w&uuml;nsche mir eine Octane2 auf Weihnachten, zusammen mit dem Kalender <a href="http://www.digicraft.com.au/calendars/index.php?language=deutsch">Classic Computers</a>  Ein Besuch im <a href="http://www.homecomputermuseum.de/cgi-bin/forum/start.pl">virtuellen Homecomputermuseum</a> lindert die Beschwerden nicht ausreichend.</p>
<p>Die Symptome lassen sich durch Darreichung einer Workstation eventuell behandeln. Alternativ k&ouml;nnte ich als Rosskur auch meinen Kontoauszug anschauen.</p>
<p>Aber die Krankheit selbst erscheint mir unheilbar.</p>
