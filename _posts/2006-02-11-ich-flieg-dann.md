---
layout: post
title: "Ich flieg dann mal schnell..."
microblog: false
guid: http://geewiz.micro.blog/2006/02/11/ich-flieg-dann.html
date: 2006-02-11T04:22:38+0200
type: post
url: /2006/02/11/ich-flieg-dann.html
---

<p>...&uuml;bers Wochenende nach Irland. Ist schon interessant, welche neuen Reisem&ouml;glichkeiten die Billigflieger er&ouml;ffnen. Da lohnt es sich auch mal, wie ich jetzt f&uuml;r 4 Tage nach Dublin zu fliegen. Am Mittwoch werde ich dann wieder zuhause sein.</p>
<p>Soll ich f&uuml;r jemanden ein Guinness mittrinken? ;)</p>
