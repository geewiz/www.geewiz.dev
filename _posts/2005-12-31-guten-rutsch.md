---
layout: post
title: "Guten Rutsch!"
microblog: false
guid: http://geewiz.micro.blog/2005/12/31/guten-rutsch.html
date: 2005-12-31T23:26:53+0200
type: post
url: /2005/12/31/guten-rutsch.html
---

<p>Jetzt noch letzte Silvestereink&auml;ufe und ich bin ger&uuml;stet. Ich freue mich auf 2006, denn es bringt viel Neues f&uuml;r mich, zum Beispiel einen neuen Arbeitsplatz und eine neue Frisur. An neuen IT-Kenntnissen arbeite ich ja bereits intensiv und neue Erfahrungen in allen Lebensbereichen lassen auch nie lange auf sich warten.</p>
<p>In diesem Sinne w&uuml;nsche ich allen meinen Freunden und den Lesern meines Weblogs einen guten Start ins neue Jahr!</p>
