---
layout: post
title: "Understanding Multi-tasking on the iPad: What is it really?"
microblog: false
guid: http://geewiz.micro.blog/2010/02/02/understanding-multitasking-on.html
date: 2010-02-02T22:49:21+0200
type: post
url: /2010/02/02/understanding-multitasking-on.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_medium_quote"><p>Instead of holding on to your old notions of how computers should work, take a look at what the new offers. The iPad is a half inch thick device, with multi-touch, forever connected to the internet, simplified, focussed, affordable, and most importantly, can be superbly productive. Sure it won&rsquo;t be just as efficient and productive as your desktop or laptop, and that&rsquo;s why they will continue to remain production machines, but given the iPad&rsquo;s size and mobility, I think the lack of traditional multi-tasking is anything but bad design.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://smokingapples.com/opinion/multi-tasking-iphone-ipad/">smokingapples.com</a></div></p>
</div>
