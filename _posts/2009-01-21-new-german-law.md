---
layout: post
title: "New german law criticized as start into surveillance state"
microblog: false
guid: http://geewiz.micro.blog/2009/01/21/new-german-law.html
date: 2009-01-21T16:22:20+0200
type: post
url: /2009/01/21/new-german-law.html
---

<p>In an <a href="http://www.gi-ev.de/no_cache/aktuelles/meldungsdetails/meldung/gi-kritisiert-bsi-gesetzentwurf-tiefgreifende-schwachstellen-mssen-beseitigt-werden-204/">official statement</a>, the german "Gesellschaft f&uuml;r Informatik" (GI), Germany's biggest association of IT experts, states that the new "BSI law" probably is a progress from the draft of 2008, but still has deeply rooted flaws. (BSI is short for "Bundesamt f&uuml;r Sicherheit in der Informationtechnik", the federal office for information technology security.)</p>
<p>What troubles the IT experts most is the fact that each and every communication with federal authorities will be completely monitored, which they regard as the first step to a surveillance state. "GI demands free and uncontrolled communication of all citizens with federal authorities as warranted by the constitution", the paper announces. Personal information won't be sufficiently secured by the proposed law, so effective restrictions must be put in place, GI concludes.</p>
<p>By criticising the "BSI law", GI joins other voices that fear an increase in  stately surveillance and in the risk of unauthorized access to personal data, for example Peter Schaar, the german federal data privacy commissioner.</p>
<p>I regard it as highly necessary that all parts of german society raise their voices against those attempts at collecting more personal data with neither valid reason nor the technical and legal means of protecting them. Join the protest!</p>
<p>(via <a href="http://www.heise.de/newsticker/Informatiker-kritisieren-BSI-Gesetz--/meldung/122059">Heise Newsticker</a>)</p>
