---
layout: post
title: "Blognation Ireland goes online"
microblog: false
guid: http://geewiz.micro.blog/2007/08/15/blognation-ireland-goes.html
date: 2007-08-15T04:08:05+0200
type: post
url: /2007/08/15/blognation-ireland-goes.html
---

<p>Blognation is growing further. After hiring Nicole Simon and Marcus Spath as their <a href="http://detech.blognation.com/">German startup</a> correspondents, the site now also covers the <a href="http://ietech.blognation.com/.">Irish Web 2.0 entrepreneurs</a> In his <a href="http://ietech.blognation.com/2007/08/13/blognation-ireland-launches/">inaugural post</a>, Damien Mulley lists some of the most interesting Web 2.0 companies in Ireland with their products, such as</p>
<ul>
<li><a href="http://polldaddy.com">PollDaddy</a></li>
<li><a href="http://touristr.com/">TouristR</a></li>
<li><a href="http://pixenate.com/">Pixenate</a></li>
<li><a href="http://putplace.com/">PutPlace</a></li>
<li><a href="http://www.segala.com/">Segala</a></li>
</ul>
<p>Seems like Ireland is, in terms of startup ideas, a rich country. I'll certainly keep an eye on <a href="http://ietech.blognation.com/">Blognation Ireland</a> as a preparation for the time I may have to look for a job over there.</p>
