---
layout: post
title: "Give me back my Google"
microblog: false
guid: http://geewiz.micro.blog/2006/10/18/give-me-back.html
date: 2006-10-18T19:59:41+0200
type: post
url: /2006/10/18/give-me-back.html
---

<p><a href="http://www.givemebackmygoogle.com/">Give me back my Google</a> is one of those genius ideas: small, effective, and it makes you think "I should have come up with that myself earlier".</p>
<p>GmbmG offers you a Google search box and automatically adds excludes for all those annoying affiliate websites that normally clutter your search results.</p>
<p>(via <a href="http://www.thinkvitamin.com/">ThinkVitamin</a>)</p>
