---
layout: post
title: "Making business easy"
microblog: false
guid: http://geewiz.micro.blog/2006/08/01/making-business-easy.html
date: 2006-08-01T21:48:11+0200
type: post
url: /2006/08/01/making-business-easy.html
---

<p>A country's economy depends highly on how it acts as a medium for business. The World Bank just issued its report <a href="http://www.doingbusiness.org/documents/DoingBusines2006_fullreport.pdf">Doing Business in 2006: Creating Jobs</a> where countries are evaluated on how they facilitate business and job market growth.</p>
<p>In terms of ease of doing business, New Zealand, Singapore and the USA are top. While Ireland ranks at 11, Germany is far behind on rank 19 -- despite the claims of our politicians about how their measures are greatly fostering business.</p>
<p>(via <a href="http://blog.guykawasaki.com/2006/07/doing_business_.html">Guy Kawasaki</a>)</p>
