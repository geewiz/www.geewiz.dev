---
layout: post
title: "Bill Gates thinks DRM is rubbish"
microblog: false
guid: http://geewiz.micro.blog/2006/12/16/bill-gates-thinks.html
date: 2006-12-16T02:49:45+0200
type: post
url: /2006/12/16/bill-gates-thinks.html
---

<p>I'm totally flabbergasted: Bill Gates not only has common sense, he even shows it in public! TechCrunch reports from <a href="http://www.techcrunch.com/2006/12/14/bill-gates-on-the-future-of-drm/">Gates' meeting with bloggers</a> where he expressed his dissatisfaction with DRM technology:</p>
<blockquote class="posterous_medium_quote">
<p>There are "huge problems" with DRM, he says, and "we need more flexible models, such as the ability to buy an artist out for life" (not sure what he means). He also criticized DRM schemes that try to install intelligence in each copy so that it is device specific. His short term advice: "People should just buy a cd and rip it. You are legal then."</p>
</blockquote>
<p>Amazing. Just amazing.</p>
