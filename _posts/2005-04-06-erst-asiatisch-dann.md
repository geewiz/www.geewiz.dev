---
layout: post
title: "Erst asiatisch, dann arm"
microblog: false
guid: http://geewiz.micro.blog/2005/04/06/erst-asiatisch-dann.html
date: 2005-04-06T23:38:45+0200
type: post
url: /2005/04/06/erst-asiatisch-dann.html
---

<p>Nicht nur <a href="http://blog.ksile.de/index.php?/archives/253-Manchmal-sind-mir-Manager-auch-sympathisch.html">ksi</a> findet zustimmende Worte zu Herrn Wiedekings j&uuml;ngster Rede. Auch ich bin erstaunt, dass ein Topmanager mal ausnahmsweise nicht in die Kerbe <a href="http://www.spiegel.de/wirtschaft/0">Die Lohnkosten in Deutschland sind zu hoch" schl&auml;gt, sondern "den Politikern den Kopf dorthin zurecht r&uuml;ckt</a>,1518,349844,00.html, wo der Schuh wirklich dr&uuml;ckt.Gerade den Missstand, dass unser Staat die Abwanderung von Unternehmen ins Ausland auch noch mit Steuergeldern subventioniert, habe ich am letzten Wochenende mit Freunden diskutiert. Das ist ein wahrer Skandal.</p>
