---
layout: post
title: "Give us all your money"
microblog: false
guid: http://geewiz.micro.blog/2004/11/29/give-us-all.html
date: 2004-11-29T22:14:00+0200
type: post
images:
- /uploads/scohax0red.serendipityThumb.png
photos:
url: /2004/11/29/give-us-all.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/scohax0red.serendipityThumb.png" />
</div>
<p>Entweder hat man das SCO-Management unter Wahrheitsdrogen gesetzt oder die <a href="http://www.sco.com">Website</a> des Unternehmens wurde "fremdmodifiziert". :)</p>
<p>Mein Screenshot von 12:05 Uhr zeigt jedenfalls das Selbstverst&auml;ndnis der vermeintlichen Eigent&uuml;mer des Linux-Copyrights &uuml;berraschend offen dargestellt.</p>
<p>(via <a href="http://blog.koehntopp.de/archives/598-Ooops.html">Isotopp</a>)</p>
