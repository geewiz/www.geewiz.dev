---
layout: post
title: "Imagine Leadership"
microblog: false
guid: http://geewiz.micro.blog/2009/07/15/imagine-leadership.html
date: 2009-07-15T10:37:45+0200
type: post
url: /2009/07/15/imagine-leadership.html
---

<div class="posterous_bookmarklet_entry">
<div class="posterous_quote_citation">via <a href="http://www.youtube.com/watch?v=TuuTlQ0FzEU">youtube.com</a></div>
<p>XPLANE, a global information design consultancy, in collaboration with Nitin Nohria, Richard P. Chapman Professor of Business Administration, and Co-Chair of the Leadership Initiative at Harvard Business School, has created &ldquo;Imagine Leadership,&rdquo; an inspiring and thought-provoking video on the theme of global leadership.</p>
</div>
