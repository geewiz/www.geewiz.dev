---
layout: post
title: "Using ZYB.com to sync your phone with Outlook"
microblog: false
guid: http://geewiz.micro.blog/2006/07/27/using-zybcom-to.html
date: 2006-07-27T22:21:58+0200
type: post
url: /2006/07/27/using-zybcom-to.html
---

<p>In a comment on my blog entry <a href="http://blog.jochen-lillich.de/archives/439-Mobile-phone-backup-with-ZYB.html">Mobile phone backup with ZYB</a>#c707, John pointed to a tutorial on how to use <a href="http://www.zyb.com">ZYB.com</a> as a kind of middleware for syncing Outlook and your mobile phone.</p>
<p>The idea behind TechTag's <a href="http://www.tech-tag.com/2006/06/28/how-to-easily-sync-outlook-calendar-with-an-ordinary-cell-phone/:">How to easily sync Outlook calendar with an ordinary cell phone</a> if you can use the ZYB service not only as a backup facility but also as a "shared storage" to keep two phones in sync, why shouldn't it be possible to sync your phone with your Outlook calendar and contacts via ZYB?</p>
<p>The only problem is how to connect Outlook with ZYB. TechTag tried two different SyncML plugins for Outlook, having more success with the non-free one.</p>
