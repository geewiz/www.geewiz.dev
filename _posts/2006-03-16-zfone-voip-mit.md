---
layout: post
title: "Zfone: VOIP mit Verschl&uuml;sselung"
microblog: false
guid: http://geewiz.micro.blog/2006/03/16/zfone-voip-mit.html
date: 2006-03-16T05:29:54+0200
type: post
url: /2006/03/16/zfone-voip-mit.html
---

<p>Vor einem Monat beklagte sich Kris noch &uuml;ber die <a href="http://blog.koehntopp.de/archives/1167-Skype-vs.-SPIT.html.">fehlende Verschl&uuml;sselung bei SIP-Anwendungen</a> Gestern ver&ouml;ffentlichte Cryptoguru Phil Zimmermann nun sein <a href="http://www.philzimmermann.com/EN/zfone/index.html">Zfone</a>, eine VOIP-Software mit Verschl&uuml;sselung.</p>
<p>Zfone kommt dabei ohne PKI und andere komplizierte Voraussetzungen aus und spricht auch mit normalen SIP-Gegenstellen (dann nat&uuml;rlich ohne Verschl&uuml;sselung).</p>
<p>Die aktuelle Beta-Version ist f&uuml;r Linux und MacOS X verf&uuml;gbar, eine Windows-Version folgt im April.</p>
