---
layout: post
title: "They just don't get it"
microblog: false
guid: http://geewiz.micro.blog/2006/09/04/they-just-dont.html
date: 2006-09-04T21:57:53+0200
type: post
url: /2006/09/04/they-just-dont.html
---

<p>It seems like some members of the IT press aren't convinced that <a href="http://www.combots.com">COMBOTS</a> will be successful even after aquiring the rights for using characters from New Line Cinema ("Lord of the Rings"), Paws ("Garfield") and United Media ("Peanuts").</p>
<p>At least some editors of the german <a href="http://blog.computerwoche.de/2006/09/04/combots-must-die/">ComputerWoche</a> don't have quite the best wishes for the new project of the inventors of WEB.DE...</p>
