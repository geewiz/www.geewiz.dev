---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2019/11/12/you-have-headphones.html
date: 2019-11-12T12:20:00+0200
type: post
categories:
- "Links"
url: /2019/11/12/you-have-headphones.html
---
You have headphones with noise cancellation. But what about the noise within your head? Here’s a great bunch of tips: [28 Ways to Find the Stillness You Need to Thrive](https://tim.blog/2019/09/26/stillness-is-the-key-ryan-holiday/)
