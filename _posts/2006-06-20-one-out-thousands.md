---
layout: post
title: "One out, thousands in"
microblog: false
guid: http://geewiz.micro.blog/2006/06/20/one-out-thousands.html
date: 2006-06-21T00:53:23+0200
type: post
url: /2006/06/20/one-out-thousands.html
---

<p>James Blunt actually whined a 5-year-old girl out of a coma it fell into after falling from a balcony. She awoke after 10 days of unconsciousness when the clinic radio was playing "You're beautiful".</p>
<p>On the other hand, for people like me, the singer lists among the top reasons for jumping from a balcony in the first place.</p>
