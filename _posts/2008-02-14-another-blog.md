---
layout: post
title: "Another blog"
microblog: false
guid: http://geewiz.micro.blog/2008/02/14/another-blog.html
date: 2008-02-14T02:26:03+0200
type: post
url: /2008/02/14/another-blog.html
---

<p>The <a href="http://www.perl-workshop.de">German Perl Workshop</a> that I'm attending at the moment inspired me to collect all Perl content I've created so far and use it to launch a new website: <a href="http://www.perl-programmieren.de">Perl-Programmieren.de</a> will from now on be the place where I publish my articles about Perl programming as well as the episodes of my audio podcast <a href="http://www.radioperl.de">"Radio Perl"</a>.</p>
<p>You might wonder where I'll be taking the time to maintain yet another blog. Well, I don't expect miracles. So far, only the URL changed, not the posting frequency. But at least there's now one single place where you'll find the things I do all around Perl.</p>
