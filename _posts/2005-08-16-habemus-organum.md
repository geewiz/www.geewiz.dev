---
layout: post
title: "Habemus organum"
microblog: false
guid: http://geewiz.micro.blog/2005/08/16/habemus-organum.html
date: 2005-08-16T08:30:23+0200
type: post
images:
- /uploads/md1030.serendipityThumb.jpg
photos:
- /uploads/md1030.serendipityThumb.jpg
url: /2005/08/16/habemus-organum.html
---

<p>Klar, es konnte nicht lange auf sich warten lassen, dass ich zu einer Orgel komme. Und &uuml;ber einen zuf&auml;lligen Kontakt konnte ich zu einem akzeptablen Preis ein &auml;lteres, aber optisch und klanglich recht ansprechendes Ger&auml;t erwerben, eine B&ouml;hm Musica Digital 1030XL.
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/md1030.serendipityThumb.jpg" />
</div>
<p>Die Optik ist durch das Mahagoni und Chrom extrem edel. Nat&uuml;rlich entsprechen die Kl&auml;nge nicht mehr heutigen Standards, hier herrscht noch die FM-Synthese. Und mit der ist nun mal zum Beispiel kein wiedererkennbarer Pianosound zu erzeugen. Auch die Begleitautomatik ist eher antik. Aber die weichen Kl&auml;nge von Bl&auml;sern und Streichern kommen gut r&uuml;ber, auch der &uuml;ber Zugriegel steuerbare Sinus ist fein. </p>
<p>Und mir geht es ja eben darum, mit H&auml;nden und F&uuml;&szlig;en selbst zu spielen, anstatt den Computer machen zu lassen. Entscheidungskriterien waren f&uuml;r mich deshalb die 2 Manuale (mit je 61 gut spielbaren Tasten) und vor allem das Vollpedal mit seinen 30 Tasten. Damit kann ich auch Sakralst&uuml;cke &uuml;ben, damit ich mal richtig fit f&uuml;r die Kirchenorgel werde.</p>
<p>F&uuml;r die Investitionssicherheit gibt es &uuml;brigens noch immer Erweiterungsplatinen (vom Hersteller selbst zwar recht teuer, aber da ist ja noch eBay), um wieder n&auml;her an den heutigen Stand der Tontechnik zu kommen.</p>
<p>Jetzt muss ich aber erst mal &uuml;ben, um das auszusch&ouml;pfen, was in dem Schmuckst&uuml;ck schon drinsteckt. Und das macht echt Spa&szlig;!</p>
