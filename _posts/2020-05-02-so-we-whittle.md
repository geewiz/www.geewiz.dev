---
layout: post
title: "Cover letter > Resume"
microblog: false
guid: http://geewiz.micro.blog/2020/05/02/so-we-whittle.html
date: 2020-05-02T21:20:00+0200
type: post
categories:
- "Links"
url: /2020/05/02/so-we-whittle.html
---
> So we whittle the group of candidates down aggressively first. This means judging their cover letter and, to a far lesser extent, their resume.

Nice to get confirmation that great teams like Basecamp take the same approach as I.

-> [m.signalvnoise.com/hiring-pr...](https://m.signalvnoise.com/hiring-programmers-with-a-take-home-test/)
