---
layout: post
title: "An easy and secure way to launch helper scripts in a project"
microblog: false
guid: http://geewiz.micro.blog/2020/02/06/an-easy-and.html
date: 2020-02-06T13:59:00+0200
type: post
categories:
- "DevOps"
tags: [devops]
url: /2020/02/06/an-easy-and.html
---
On one of my recent live coding streams, a viewer asked what my `abe` script does. I showed that it simply launched a Ruby command in my project's application container. Since I'm using Docker Compose to spin up most of my development environments, I have to run all development tasks within the application container. Typing `abe rake test` is much faster than typing `docker-compose exec app bundle exec rake test`, so I added this script to my project's `bin` directory:

```bash
#!/bin/bash
docker-compose exec app bundle exec $@
```

It's a nifty time-saver, but the smart part of this isn't the script itself but how I make helper scripts in the `bin` directory of my projects easy to launch without having to prepend every command with `./bin/`.

If you're familiar with how a Unix shell finds the right program to execute, you 'll probably suggest just adding `./bin` to the environment variable `PATH`. But that's a risky move because you don't want to accidentally launch a malicious script after checking out a repository that happens to have an executable `ls` command in its `bin` directory.

Once again, it was the talented devs over at [ThoughtBot](https://thoughtbot.com/blog/git-safe) who found a better solution. Instead of adding `./bin` to `PATH`, they recommend adding `.git/safe/../../bin`. With this entry, the shell descends into `.git`, further down into `safe`, all the way back to the repository root and only then into `bin`. What makes this seemingly roundabout way to find your helper scripts secure is that it only works if you've first manually created the subdirectory `safe` within `.git`. The latter is, after all, git's data directory which normally doesn't contain a directory named `safe`.

There you have it — easy access to your project's helper scripts is simple to achieve. And without any additional effort, it's safe as well!
