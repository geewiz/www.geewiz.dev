---
layout: post
title: "Twitter's got me"
microblog: false
guid: http://geewiz.micro.blog/2007/02/28/twitters-got-me.html
date: 2007-03-01T00:49:00+0200
type: post
url: /2007/02/28/twitters-got-me.html
---

<p>I finally gave in.</p>
<p>After discovering <a href="http://www.twitter.com">Twitter</a>, I immediately came to the conclusion that this was the ultimate weapon of mass productivity destruction. Now, there's not only the phone, e-mail and IM that's disturbing focus and concentration, we also get a kind of micro-blog that informs us in realtime about the thoughts and situations of people all around the world. "What a waste of attention", I thought.</p>
<p>But after seeing how <a href="http://conoroneill.com/2007/02/26/i-still-think-it-is-stoopid-but/">other people cave in</a> because there actually are ways of using Twitter quite creatively and productively, I decided to give it a try, too. There's nothing like too much Web 2.0, is there? ;-)</p>
<p>So, if you're curious what I think but not deem worth the effort to blog about it, see the litte paragraph on the right of my blog's main page or directly subscribe to <a href="http://twitter.com/Geewiz.">my Twitter profile</a></p>
<p>In the meanwhile, I will think of some creative ways of Twittering.</p>
