---
layout: post
title: "Michael Dell runs Linux"
microblog: false
guid: http://geewiz.micro.blog/2007/04/20/michael-dell-runs.html
date: 2007-04-20T17:02:00+0200
type: post
url: /2007/04/20/michael-dell-runs.html
---

<p>If you've given up hope that Dell will ever ship Linux with its systems, take a look at the <a href="http://www.dell.com/content/topics/global.aspx/corp/biographies/en/msd_computers?c=us">list of computers used by Dell's Chairman of the Board</a>&amp;l=en&amp;s=corp. At the top, you'll find that at home, he's using a Dell Precision M90 with the following software on:</p>
<ul>
<li>Ubuntu 7.04 Feisty Fawn</li>
<li>VMWare Workstation 6 Beta</li>
<li>OpenOffice.org 2.2</li>
<li>Automatix2</li>
<li>Firefox 2.0.0.3</li>
<li>Evolution Groupware 2.10</li>
</ul>
<p>I'm impressed, his software is even more up-to-date than mine!</p>
