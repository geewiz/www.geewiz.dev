---
layout: post
title: "Does a cease and desist letter fit the olympic notion? For the DOSB, it does."
microblog: false
guid: http://geewiz.micro.blog/2006/12/15/does-a-cease.html
date: 2006-12-16T01:47:21+0200
type: post
url: /2006/12/15/does-a-cease.html
---

<p>The german company Walther produces juice beverages and publishes <a href="http://www.walthers.de/">Walthers Saftblog</a> ("Walther's Juice Blog"), one of the most successful business blogs in Germany. Well, it seems more appropriate to say "published", since they decided today to shut their blog down after receiving a c&amp;d letter from the German Olympic Sports Union ("Deutscher Olympischer Sportbund", DOSB).<br />
In the 17-pages document, Walther is accused of, among others, exploitation of reputation, copyright infringement, deception and trademark infringement for publishing two harmless blog entries about the Olympic Games (<a href="http://www.walthers.de/blogs/index.php?title=die_olympiade_von_turin_beginnt_am_monta">entry 1</a>&amp;more=1&amp;c=1&amp;tb=1&amp;pb=1 explaining the difference between the <a href="http://en.wikipedia.org/wiki/Olympiad">Olympiad</a> and the Olympic Games, <a href="http://www.walthers.de/blogs/index.php?title=nacholympische_gedanken_oder_im_sport_si">entry 2</a>&amp;more=1&amp;c=1&amp;tb=1&amp;pb=1 about Germany's olympic successes).</p>
<p>German attorney and law blogger <a href="http://www.lawblog.de/index.php/archives/2006/12/14/olympische-abmahnung-saftblog-gibt-auf/">Udo Vetter</a> can't find any offences, though. (At the most a small one, namely using the olympic rings logo without attribution.)</p>
<p>Of course, the german blogosphere reacts in the same way it did for example in the <a href="http://being-reasonable.com/index.php/weblog/permalink/heidi_klums_dad_vs_werbeblogger_pt_2/:">Heidi Klum incident</a> it turns up the heat.</p>
<ul>
<li>Basic Thinking: <a href="http://www.basicthinking.de/blog/2006/12/15/a-loch-alarm/">A-hole alert</a>
</li>
<li>Indiskretion Ehrensache: <a href="http://blog.handelsblatt.de/indiskretion/eintrag.php?id=1033">Meta Tag Olympia</a> ("Outside of your corruption-infested umbrella organization, there's a social order named democracy")</li>
<li>Law blog: <a href="http://www.lawblog.de/index.php/archives/2006/12/14/olympische-abmahnung-saftblog-gibt-auf/">Olympic cease and desist</a> ("This is so weird, there could be doping in the game")</li>
</ul>
<p>Once again, the blogosphere deals out wrath to one side and love to the other: For the DOSB, this could become a PR nightmare, with many bloggers writing articles on their websites as well as emails and letters to the DOSB. The <a href="http://www.marketing-blog.biz/blog/archives/886-So-reagieren..html">Marketing Blog</a> even plans a survey among the organization's sponsors. </p>
<p>On the other hand, company director Kirstin Walther expressed her amazement of all that support by strangers in an addendum to her <a href="http://www.walthers.de/blogs/index.php?p=592">blog entry</a>&amp;more=1&amp;c=1&amp;tb=1&amp;pb=1#more592 where she announced the shutdown of their blog. Let's hope they give that step a second thought. We all want justice to win, not the lawyers.</p>
