---
layout: post
title: "2 Jahre bei WEB.DE"
microblog: false
guid: http://geewiz.micro.blog/2006/02/02/jahre-bei-webde.html
date: 2006-02-02T05:48:00+0200
type: post
url: /2006/02/02/jahre-bei-webde.html
---

<p>Am 1. Februar 2004 begann ich meine Arbeit als Teamleiter in der IT-Abteilung von WEB.DE. Z&auml;hlt man meine Arbeit als externer Trainer noch dazu, bin ich sogar schon schon 2 Jahre und 4 Monate bei der Freemail-Firma.In dieser Zeit habe ich einiges erlebt: nach 2 Monaten &uuml;bernahm ich die Administration des Usermanagements und des Billingsystems. Gleichzeitig begann ich, ein eigenes Admin-Team aufzubauen. Die Arbeit in einem Rechenzentrum mit mehr als 1000 Servern ist hochinteressant. Man lernt st&auml;ndig Neues &uuml;ber die Technik -- und &uuml;ber Menschen. Die H&ouml;hepunkte meiner bisherigen Arbeit waren die Einf&uuml;hrung des neuen Abrechnungssystems und der Umzug unseres Rechenzentrums in die heiligen Hallen unserer neuen Konzernmutter 1&amp;1, den wir gestern erfolgreich und termingerecht zu Ende gebracht haben.</p>
<p>Ich bin dankbar, dass ich einen Job habe, der mir viel Spa&szlig; macht und mich erf&uuml;llt. Dazu tragen auch meine Kollegen bei. Ich finde, "Erfolg ist eine gemeinsame Sache" ist bei uns sp&uuml;rbar mehr als eine Floskel. </p>
<p>&Uuml;bermorgen ist mein letzter Arbeitstag in Durlach, ab Montag geht es morgens in den Karlsruher -Osten- Westen. Ich bin gespannt, wie es unter dem neuen Dach weitergehen wird. Wir m&uuml;ssen mit vielen Ver&auml;nderungen rechnen, und wahrscheinlich werden nicht nur angenehme darunter sein. Eine Sache wird mir wohl aber auch in der neuen Firma nicht begegnen: Langeweile. ;)</p>
