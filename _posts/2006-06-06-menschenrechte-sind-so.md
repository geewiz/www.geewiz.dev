---
layout: post
title: "Menschenrechte sind so &uuml;berbewertet"
microblog: false
guid: http://geewiz.micro.blog/2006/06/06/menschenrechte-sind-so.html
date: 2006-06-06T19:24:04+0200
type: post
url: /2006/06/06/menschenrechte-sind-so.html
---

<p>Ich traue der Regierung der USA ja wirklich einiges zu, aber manche Dinge sind einfach zu absurd f&uuml;r meine Phantasie. So erweitert die Los Angeles Times meinen Horizont, indem sie dar&uuml;ber berichtet, dass der Kreis um Pr&auml;sident Bush dar&uuml;ber nachdenkt, <a href="http://www.latimes.com/news/nationworld/nation/la-na-torture5jun05">Artikel 3 der Genfer Konvention zu widerrufen</a>,0,7975161.story?coll=la-home-headlines. Nach diesen Pl&auml;nen w&uuml;rden Gefangene im "Krieg gegen den Terror" vom Schutz vor Folter und Entw&uuml;rdigung ausgenommen werden.</p>
<p>Ein Blogeintrag bei <a href="http://blogs.usatoday.com/ondeadline/2006/06/report_pentagon.html">USA Today</a> geht auf diese &Uuml;berlegungen ein. Die darauf bezogenen Kommentare (unten auf der Seite) spiegeln die Einstellung amerikanischer B&uuml;rger dazu wider.</p>
<p>Menschenrechte. Bah. Bleib mir doch mit diesem Hippie-Gew&auml;sch vom Leib. Es ist Krieg!</p>
<p>(via <a href="http://irish.typepad.com/irisheyes/2006/06/human_rights_go.html">IrishEyes</a>)</p>
