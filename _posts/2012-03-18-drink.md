---
layout: post
title: "Drink!"
microblog: false
guid: http://geewiz.micro.blog/2012/03/18/drink.html
date: 2012-03-18T22:34:43+0200
type: post
images:
- http://www.jochen-lillich.de/wp-content/uploads/2012/03/jack.jpg.jpeg
photos:
- http://www.jochen-lillich.de/wp-content/uploads/2012/03/jack.jpg.jpeg
url: /2012/03/18/drink.html
---
<p><img src="http://www.jochen-lillich.de/wp-content/uploads/2012/03/jack.jpg.jpeg" alt="Father Jack Hacket" title="jack.jpg.jpeg" border="0" width="400" height="266" /></p>
<p>It looks as if the <a href="/iron-blogger-freiburg/">Iron Blogger Freiburg</a> initiative really was able to breathe some life into the blogs of our small group of spare-time writers!</p>
<p>Money is quite an incentive, I&#8217;ve come to learn. The impending &#8220;fine&#8221; of 4&euro; for missing the entry deadline on Monday morning makes sure that on Sunday evening at the latest, I force myself to launch my writing software. It&#8217;s been only two times now that I didn&#8217;t deliver; one time I spent the weekend in Rome and last weekend, I&#8217;ve been far to knackered to still write coherent sentences.</p>
<p>With Carolin and Heather, who both turned themselves in unsolicitedly for missing the deadline, we&#8217;ve now an open account of 16&euro; for the tab on our first meetup. </p>
<p>Speaking of meetup: Heather is going to visit Freiburg in May or June. Will we have accumulated a tidy sum until then? Anyone else who wants to clear their concience? ;-)</p>
