---
layout: post
title: "Mit der CSU in die Medienzukunft"
microblog: false
guid: http://geewiz.micro.blog/2005/09/13/mit-der-csu.html
date: 2005-09-13T21:44:30+0200
type: post
url: /2005/09/13/mit-der-csu.html
---

<p>Das Handelsblatt-Blog <a href="http://blog.handelsblatt.de/indiskretion/eintrag.php?id=345">Indiskretion Ehrensache</a> sorgt bei mir immer wieder f&uuml;r Kopfsch&uuml;tteln. Aber dass eine Partei nicht mal genug Wissen &uuml;ber elektronische Medien mitbringt, um selbst darauf zu kommen, dass man mit Spam keine Freunde gewinnt, verursacht mir k&ouml;rperliche Schmerzen.</p>
<p>Im konkreten Fall entbl&ouml;det sich die CSU nicht, stolz zu erkl&auml;ren: "Die CSU setzt in der Schlussphase neben Plakatierungen und Kundgebungen vor allem auf moderne Wahl-Werbung. So sollen bis zu 300.000 E-Mails und mehrere tausend Voice-Mails, bei denen sich per Tonbandansage Edmund Stoiber auf Handys zu Wort meldet gerade Jungw&auml;hler ansprechen und zum W&auml;hlen motivieren."</p>
<p>Es gibt eh schon zu wenige Politiker, die Ahnung von Internet und elektronischer Kommuniktion haben. Und leider versuchen gerade die Vorzeigeexemplare wie Zypries und Tauss, der Schublade "Internet-Politiker" zu entkommen, indem sie dem Thema ausweichen.</p>
<p>Dazu noch -Homeland Security- das Innenministerium mit seinen Versuchen, Datenschutz und Kommunikationsfreiheit auszuh&ouml;hlen, und die Zukunft unserer Medienkultur ist -komplett im Arsch- klar.</p>
