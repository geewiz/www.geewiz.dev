---
layout: post
title: "How to survive and succeed at conferences as an introvert"
microblog: false
guid: http://geewiz.micro.blog/2012/02/12/how-to-survive.html
date: 2012-02-12T21:17:05+0200
type: post
url: /2012/02/12/how-to-survive.html
---
<p>Hello, my name is Jochen and I&#8217;m an introvert. This isn&#8217;t easily apparent because when I meet people I can draw from enough self-confidence and eloquence to appear open and communicative. But deep down, I often feel more or less uncomfortable with people I don&#8217;t know well.</p>
<p>As a business owner, being an introvert can be quite a challenge. Especially at conferences and business meetings, you are supposed to engage in smalltalk, connect at lunch or dinner, meet new business prospects as well as existing clients, and generally present yourself and your company in the best possible way. You meet many different people, many of them for the first time, and each has their own personality. This is the arena of the extroverts, the High-I&#8217;s of the DISC typology, who enjoy the limelight. But for an introvert, it&#8217;s tempting to stick with who you already know and then leave sooner rather than later.</p>
<p>I was at the airport, waiting for my flight to the <a href="http://process.drupaldays.org">Drupal Process Meetup</a> in Amsterdam, when I read an article in Harvard Business Review by Lisa Petrilli titled &#8220;<a href="http://blogs.hbr.org/cs/2012/01/the_introverts_guide_to_networ.html">An Introvert&#8217;s Guide to Networking</a>&#8221;. In this article, Lisa talks about her natural aversion to networking, her realization that she&#8217;d need to overcome it to get on with her career, and the strategies that helped her to do so. It immediately struck a chord with me because I had been anxious for days in advance how I would be able to blend in at this conference where I didn&#8217;t know most of the people yet. </p>
<p>It took me a few minutes to read the article and then a few more seconds to buy and download her ebook &#8220;<a href="http://www.lisapetrilli.com/the-introverts-guide/">The Introvert&#8217;s Guide to Success in Business and Leadership</a>&#8221; to my Kindle. I read it on the flight and with every page, I learned more on how to make my trip to Amsterdam a worthwhile business expense.</p>
<blockquote>
<p>&#8220;Introversion is simply a preference for the inner world of ideas because this is where we get our energy.&#8221;</p>
</blockquote>
<p>Her explanation why introverts feel uncomfortable in crowds made immediately sense to me: We draw energy from the creative process, from developing and refining ideas. On the other hand, opening up to others drains our energy. That&#8217;s why we often feel especially spent after an intense conference day.</p>
<p>Lisa has got a great tip how this can be mitigated: When you&#8217;re in a networking situation, <strong>engage in one-to-one conversations</strong> with a single person. With this tactic in mind, I was able to take many opportunities during the meetup to get in touch with other participants and always had great conversations. </p>
<p>She also urges her fellow introverts to <strong>overcome the fear of introducing themselves to others</strong>, especially to people to which they feel somehow inferior. </p>
<blockquote>
<p>&#8220;I learned over time that when I extended my hand with a smile and an introduction my effort would be reciprocated, even when I approached executives above my rank.&#8221;</p>
</blockquote>
<p>This encouraged me tremendously to approach even the executives of million-dollar companies that were at the meetup. It was one of those CEOs that was the first to connect to me via LinkedIn shortly after we talked.</p>
<p>Thanks to Lisa&#8217;s explanation how introverts gain and lose energy, I felt comfortable to excuse myself from the pub tour after dinner and to instead recharge for the next conference day all by myself in a comfy chair in the hotel lounge. </p>
<p>After a great conference experience without feeling uncomfortable once, I now happily look forward to the <a href="http://products.drupaldays.org">next one</a> in two weeks! The trip already paid for itself &#8211; and so did Lisa Petrilli&#8217;s <a href="http://www.lisapetrilli.com/the-introverts-guide/">ebook</a>.</p>
