---
layout: post
title: "Erwachsene im Internet"
microblog: false
guid: http://geewiz.micro.blog/2005/01/11/erwachsene-im-internet.html
date: 2005-01-11T03:31:00+0200
type: post
url: /2005/01/11/erwachsene-im-internet.html
---

<p>Wie Heise <a href="http://www.heise.de/newsticker/meldung/54950">berichtet</a>, sind 62 Prozent der erwachsenen Deutschen online.</p>
<p>Tja, wieder mal ein typischer Fall, wo Studien am Ziel vorbei schie&szlig;en. Mich w&uuml;rde viel mehr interessieren, wie viele der Anwender im Internet sich wie Erwachsene verhalten...</p>
