---
layout: post
title: "German men get homely"
microblog: false
guid: http://geewiz.micro.blog/2006/07/24/german-men-get.html
date: 2006-07-24T19:00:56+0200
type: post
url: /2006/07/24/german-men-get.html
---

<p>According to studies conducted by the "Berlin Institute for application-oriented innovation and future research", male Germans spend half an hour more on household and family time than 10 years ago. Women on the other hand, reduced their weekly household time by 5 hours. (You do the math.)</p>
<p>And while women also spend one hour less with their children, men have increased quality time spent with their offspring by one hour. Interestingly, this hour isn't taken from working time. "The new man actually does exist", the scientists concluded.</p>
<p>(via <a href="http://www.spiegel.de/panorama/0">SPON</a>,1518,428099,00.html)</p>
