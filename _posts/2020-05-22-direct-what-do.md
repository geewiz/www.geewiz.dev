---
layout: post
title: "DIRECT - The seven core topics of remote team communication"
microblog: false
guid: http://geewiz.micro.blog/2020/05/22/direct-what-do.html
date: 2020-05-22T13:59:00+0200
type: post
categories:
- "DEV"
url: /2020/05/22/direct-what-do.html
---
Pick any guide on remote work (oh, there are so many...) and it'll tell you that communication is essential when you work in a distributed team. The problem is that it's not obvious where exactly the centre of balance is between sharing too little and sharing too much. That's why I've created a framework that uses a simple acronym to remind you of good opportunities to keep your distributed team in the loop.

You can watch it here or on YouTube. I'm already working on my next videos, so don't forget to subscribe to my YouTube channel!

<iframe width="560" height="315" src="https://www.youtube.com/embed/BC6vzRiOy-Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
