---
layout: post
title: "Life signs"
microblog: false
guid: http://geewiz.micro.blog/2008/05/23/life-signs.html
date: 2008-05-23T06:58:36+0200
type: post
url: /2008/05/23/life-signs.html
---

<p>Good grace, I've been really busy those past weeks. I apologize for the silence to all my friends and blog readers wondering what happened to me.</p>
<p>I really miss blogging on my diverse websites, so this silence really has to end. I'll try to put away some time for blogging between leading an IT department and caring for my precious.</p>
<p>Work's interesting at the moment, to say the least. My department, "IT Core Services", consists of three teams. The one named "Infrastructure Systems" is responsible for providing services like backup for thousands of servers, facilitate central authentication as well as OS and application software deployment. Then, there's the "Databases and Storage" team whose area of expertise is obvious. Finally, there's "Core Applications" where they run business-critical services like our billing and payment services. My staff is great, but they're much too few and recruiting is only going on slowly. What hits me hardest is that two of the teams have been formed from scratch and both still lack a team lead. Having to lead more than ten people directly is nothing you can do well over long time; and I feel how it's slowly exhausting me. Some of the recent applicants made quite a good impression, though, so I keep my hope up.</p>
<p>My precious is well, gaining more circumference by the day. Since she's due for the 7th of June, the baby could arrive any day now. The excitement is slowly building up, but we're as prepared as you can be for your first child: First clothes, diapers and musical clock neatly piled up in the closet, parents' magazines piled up not quite as neatly on the side table.</p>
<p>To sum it up: All's well, challenges waiting for me at work, at home waiting for another challenge. Boredom? Isn't that a city in Sweden? ;-)</p>
