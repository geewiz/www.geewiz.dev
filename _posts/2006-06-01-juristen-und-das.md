---
layout: post
title: "Juristen und das Internet"
microblog: false
guid: http://geewiz.micro.blog/2006/06/01/juristen-und-das.html
date: 2006-06-01T06:13:04+0200
type: post
url: /2006/06/01/juristen-und-das.html
---

<p>Baeumer, Ulrich, Domain Namen und Markenrecht, CR 1998, 174:</p>
<blockquote class="posterous_medium_quote">
<p>Die IP-Nummern haben den Vorteil [gegen&uuml;ber Hostnamen], dass sie flexibler sind, da sie einem Dezimalzahlensystem unterliegen und die M&ouml;glichkeiten im Rahmen zw&ouml;lfstelliger Zahlen gro&szlig; sind.</p>
</blockquote>
<p>Nee, ja, is klar. Wer sich schon immer gefragt hat, welche Leute die vielen sinnvollen Gesetze, Abmahnungen und Urteile rund um den <a href="http://daufaq.de/index.php4?aktuellerubrik=Leitseite">Datenhighway" verzapfen, sollte einfach mal "Juristen erkl&auml;ren das Internet</a> lesen.</p>
<p>(Via <a href="http://sushee.schreibsturm.org/2006/05/31/mich-wundert-nichts-mehr">Su-Shee</a>)</p>
