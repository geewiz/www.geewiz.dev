---
layout: post
title: "Social brainstorming"
microblog: false
guid: http://geewiz.micro.blog/2007/03/02/social-brainstorming.html
date: 2007-03-02T05:14:50+0200
type: post
url: /2007/03/02/social-brainstorming.html
---

<p>Brainstorming is an old and proven method of getting thought processes running. And now, <a href="http://www.brainr.de">BrainR</a>, a project by the german information service <a href="http://www.ideentower.de">Ideentower</a>, brings this method to the Web 2.0. Just post a question and see how people make suggestions and tell their thoughts about it. </p>
<p>I just gave it a try by posting a question about <a href="http://www.brainr.de/brainstorming/show/219.">virtual training seminars</a> So please, go over and post your ideas! I'm very curious how that will work.</p>
