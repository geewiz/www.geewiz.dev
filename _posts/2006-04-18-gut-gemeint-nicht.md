---
layout: post
title: "Gut gemeint, nicht gut gemacht"
microblog: false
guid: http://geewiz.micro.blog/2006/04/18/gut-gemeint-nicht.html
date: 2006-04-18T06:51:45+0200
type: post
url: /2006/04/18/gut-gemeint-nicht.html
---

<p>Rob Pegoraro von der Washington Post ist nicht begeistert von Nokias Internet Tablet 770: in <a href="http://www.washingtonpost.com/wp-dyn/content/article/2006/04/15/AR2006041500125.html">It Does Little, and Not Very Well</a> l&auml;sst er kaum ein gutes Haar an dem kleinen Surfbrett.<br />
Die Eingabem&ouml;glichkeiten k&ouml;nnen nach Meinung des Autors die fehlende Tastatur nicht ausgleichen, Verbindungen per Bluetooth seien schwer zu konfigurieren und die WLAN-Anbindung stellte sich bei seinen Tests als sehr instabil heraus. Die Verarbeitungsm&ouml;glichkeiten von Multimedia- und Officedateien hinken laut Pegoraro weit hinter dem aktuellen Stand der Technik hinterher. Auch die Akkulaufzeit l&auml;sst f&uuml;r ihn schwer zu w&uuml;nschen &uuml;brig.</p>
<p>Der Artikel erinnert mich ein wenig an den ersten Eindruck, den seinerzeit der Palm Pilot 5000 bei mir hinterlie&szlig;: "So viel Geld f&uuml;r so wenig Funktion?" In beiden F&auml;llen wurde &uuml;bersehen, dass eine Vielfalt von Software nachinstalliert werden kann. Und dass bei jedem solchen Ger&auml;t noch viele M&ouml;glichkeiten zur Korrektur von Problemen und zur Weiterentwicklung seiner Funktionen besteht, zum Beispiel durch Firmware-Updates.</p>
<p>Obwohl ich deshalb davon ausgehe, dass bei der Bewertung einige vers&ouml;hnende Pluspunkte &uuml;bersehen wurden, teile ich die Einsch&auml;tzung, dass das 770 wohl kaum ein Markterfolg werden wird.</p>
