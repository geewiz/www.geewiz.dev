---
layout: post
title: "Server, Server, du musst wandern"
microblog: false
guid: http://geewiz.micro.blog/2005/12/02/server-server-du.html
date: 2005-12-02T06:01:21+0200
type: post
url: /2005/12/02/server-server-du.html
---

<p>Das WEB.DE Portal zieht unter das Dach seiner neuen Eigent&uuml;mer, n&auml;mlich in das 1&amp;1 Gesch&auml;ftsgeb&auml;ude in der Karlsruher Brauerstra&szlig;e. Und das betrifft nicht nur ein paar hundert Mitarbeiter, sondern auch die etwa 1500 Rechner, auf die das Portal seine Dienste verteilt. Der Rechenzentrumsumzug hat diese Woche begonnen und wird in den kommenden Wochen &uuml;ber die B&uuml;hne gehen. Das wird ein hochgradig spannendes Projekt. Sp&auml;testens ab Februar werde ich dann im neuen B&uuml;ro, nur wenige Meter neben dem ZKM, meine Vollkornbr&ouml;tchen verdienen.</p>
