---
layout: post
title: "V&ouml;llig verorgelt"
microblog: false
guid: http://geewiz.micro.blog/2005/07/28/voumlllig-verorgelt.html
date: 2005-07-28T05:08:32+0200
type: post
url: /2005/07/28/voumlllig-verorgelt.html
---

<p>Nach 20 Jahren und Wechseln auf Synthesizer, E-Piano und Keyboard bin ich wieder bei der E-Orgel angekommen. Jedenfalls hat mich vor kurzem die Lust gepackt, mal wieder richtig mit beiden H&auml;nden und F&uuml;&szlig;en klassische und aktuelle Musik zu spielen.Allerdings ist es gar nicht so einfach, ein passendes Instrument zu finden. Der Handel ist nahezu komplett auf Keyboards umgeschwenkt, und die verbliebenen "Orgelstudios" verkaufen ihre Ger&auml;te zu illusorischen Preisen: 27.000 Euro f&uuml;r eine aktuelle WERSI-Orgel? Hallo?</p>
<p>Obwohl eine WERSI mich schon reizen w&uuml;rde, denn deren H&ouml;hepunkt war wohl just in den 80ern, in denen ich meinen Musikunterricht hatte. Aber selbst die &auml;lteren Modelle werden zu erstaunlichen Preisen angeboten -- allerdings meist vergeblich, wie man an den sich wiederholenden Angeboten im Internet sieht. Andere Gebrauchtinstrumente wiederum sind zwar g&uuml;nstig, aber eher als Elektroschrott einzuordnen.</p>
<p>Naja, ich halte erst mal weiter Ausschau nach einer guten M&ouml;glichkeit, Musik und Sport zu verbinden. ;)</p>
