---
layout: post
title: "E-Mails to Notme"
microblog: false
guid: http://geewiz.micro.blog/2006/08/11/emails-to-notme.html
date: 2006-08-11T07:59:00+0200
type: post
url: /2006/08/11/emails-to-notme.html
---

<p>"Who the hell made that update and forgot to change the configuration?" "Notme." </p>
<p>Notme is a coworker that has been here from the start, years before I came aboard. Curiously, I get his email. A lot of email. That's why I created a mail folder especially for Notme.</p>
<p>Seriously. In a company like ours, email is probably the most essential communication medium. It goes as far as that when someone has visitors waiting but can't be reached on the phone, a tracing email goes out to all employees. And that's hundreds of employees. There actually are a lot of such emails every day that catch our attention and waste our precious time. But on the other hand, we're supposed to respond quickly to every important email...</p>
<p>I don't have many email filters (only a few for mailing lists), but there's one that really helps me focusing on important emails, and that's the Notme filter: every email that is not directly addressed to me (such as mailing lists or cc's) is moved to the "Notme" folder. When I look at my inbox, all I see is email that is especially meant for me which I process the Getting Things Done way: those actions that require two minutes or less are done immediately, and all other emails are moved to appropriate action folders. Then I start over with an empty inbox.</p>
<p>Notme's mail, though, is only dealt with when I have the time to read FYI's and mass mailings.</p>
