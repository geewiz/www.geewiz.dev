---
layout: post
title: "New read: Leading Change"
microblog: false
guid: http://geewiz.micro.blog/2009/05/05/new-read-leading.html
date: 2009-05-05T18:36:16+0200
type: post
url: /2009/05/05/new-read-leading.html
---

<p>After listening to the audiobook version of John P. Kotter's "The Heart of Change", I decided to also read his book "Leading Change". Kotter is an expert on change management, i.e. the management and leadership skills to lead an organization and make the necessary changes to adapt it to new situations and implement new strategies.</p>
<p>I've read chapter 1, "Transforming organizations: Why firms fail", and I'm hooked. Kotter explains what errors are common causes of failing change efforts:</p>
<ol>
<li>Allowing too much complacency</li>
<li>Failing to create a sufficiently powerful guiding coalition</li>
<li>Underestimating the power of vision</li>
<li>Undercommunicating the vision by a factor of 10 (or 100 or even 1,000)</li>
<li>Permitting obstacles to block the new vision</li>
<li>Failing to create short-term wins</li>
<li>Declaring victory too soon</li>
<li>Neglecting to anchor changes firmly in the corporate culture</li>
</ol>
<p>Looking at some of the organizational changes I was in some way part of, I can see instances where most or even all eight of those errors were made -- with the appropriate results. I want the changes I'm about to undertake to be a better success, so I'm curious what Kotter has in store in the following chapters.</p>
