---
layout: post
title: "Ich war jung und hatte das Geld"
microblog: false
guid: http://geewiz.micro.blog/2005/05/11/ich-war-jung.html
date: 2005-05-11T05:11:49+0200
type: post
url: /2005/05/11/ich-war-jung.html
---

<p>Immer wieder sind es gerade die gut gemeinten Aktionen, die einen in die Bredoullie bringen.</p>
<p>Das sich &ouml;kologisches Denken r&auml;cht, merkt man zum Beispiel, wenn man alte CDs in die gro&szlig;e Recycling-Box in der Firma wirft, ein Kollege aus diesen dann aber schlie&szlig;en kann, dass man mal Mitglied im MSDN(Microsoft Developers Network) war...</p>
