---
layout: post
title: "The Happiness Advantage"
microblog: false
guid: http://geewiz.micro.blog/2013/06/24/the-happiness-advantage.html
date: 2013-06-24T12:00:00+0200
type: post
url: /2013/06/24/the-happiness-advantage.html
---
<p>This highly entertaining and densely packed TEDx talk explains why it's so important to have fun at work (which we certainly have at <a href="http://www.freistil.it">freistil IT</a>):</p>
<p><iframe src="http://embed.ted.com/talks/shawn_achor_the_happy_secret_to_better_work.html" width="400" height="225" frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></p>
