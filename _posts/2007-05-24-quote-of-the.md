---
layout: post
title: "Quote of the day"
microblog: false
guid: http://geewiz.micro.blog/2007/05/24/quote-of-the.html
date: 2007-05-24T21:24:36+0200
type: post
url: /2007/05/24/quote-of-the.html
---
<br />
<blockquote class="posterous_short_quote">
<p>A freely elected totalitarian government is something new, after all.</p>
</blockquote>
<p>(<a href="http://sushee.schreibsturm.org/article/1646/mein-dank-geht-an-die-waehler">Su-Shee 2.0</a>, translated from German)</p>
