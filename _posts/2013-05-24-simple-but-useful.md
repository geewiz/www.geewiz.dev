---
layout: post
title: "Simple but useful sysadmin tools"
microblog: false
guid: http://geewiz.micro.blog/2013/05/24/simple-but-useful.html
date: 2013-05-24T12:23:16+0200
type: post
url: /2013/05/24/simple-but-useful.html
---
<p>Sysadmins offen happen upon simple tasks for which there's no actual shell command. For example, you may want to run a command after a random delay of up to 3 minutes. Well, there's <code>sleep</code> and there's <code>$RAND</code>, so you'll probably quickly solve that problem. If you need to do this more often, you'll likely build some kind of script to make this task as easy as possible.</p>
<p>Being an awesome sysadmin, Steve Kemp  not only wrote a bunch of small <a href="http://blog.steve.org.uk/sysadmin_tools.html">sysadmin tools</a> for frequent needs but also published them on <a href="https://github.com/skx/sysadmin-util">Github</a>:</p>
<ul>
<li><code>ago</code>: Show how long ago a file/directory was modified in a human-readable fashion.</li>
<li><code>dupes</code>: Report on duplicate files, via a SHA1 hash of the contents, recursively.</li>
<li><code>empty-dir</code>: Indicate, via return code, whether a given directory is empty or not.</li>
<li><code>maybe</code>: In a similar vain to true and false the maybe command exits with a status code of zero or one, depending on a random number.</li>
<li><code>multi-ping</code>: Ping a host, regardless of whether it is an IPv6 or IPv4 host.</li>
<li><code>mysql-slave-check</code>: If the current host is a MySQL slave this script will test that the slave replication is still working.</li>
<li><code>randpass</code>: Generate a single random password via /dev/urandom.</li>
<li><code>since</code>: Show the new output since previously reading a file. This is useful for keeping track of logfile updates.</li>
<li><code>splay</code>: Sleep for a random amount of time, limited by the given max value. (Default is 5 minutes).</li>
<li><code>ssl-expiry-date</code>: Report the date, and number of days, until the given SSL certificate expires.</li>
<li><code>timeout</code>: Timeout allows you to run a command which will be killed after the given number of seconds.</li>
<li><code>until-success</code>: Repeat the specific command until it succeeds - run at least once always.</li>
<li><code>which-shell</code>: Identify the shell we're running under.</li>
<li><code>with-lock</code>: Run a command, unless an existing copy of that command is already running, via the creation of a temporary lockfile.</li>
</ul>
<p>Thanks, Steve!</p>
