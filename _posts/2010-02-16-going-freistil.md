---
layout: post
title: "Going Freistil"
microblog: false
guid: http://geewiz.micro.blog/2010/02/16/going-freistil.html
date: 2010-02-17T00:26:35+0200
type: post
url: /2010/02/16/going-freistil.html
---

<p>I've been very busy working over the last weeks and months. Busy working on a new (ad)venture: My own business. I decided to pull my old freelance business from the back burner and go full-time. Because I thought that the experiences I'm making along the way could be interesting to some people, this will be the first of (hopefully) many articles about my starting up.</p>
<h2>What happened</h2>
<p>How did it come that, amidst a recession of all things, I decided to leave my corporate shelter to start my own business?</p>
<p>One could &nbsp;assume I got tired of commuting every day for more than two hours. But the opposite is true: My one-hour train ride to and from the office often was more productive than double the time in the office. I have noise-blocking earphones, so on the train I got interrupted at most once an hour. Try this sharing an office with two other people. Salary reasons then? Well, there certainly are people getting paid a lot more for doing less work and having less responsibility. But no, I got enough to get along fine, and money isn't my top motivation for sure.</p>
<p>The real reason is that I felt I wasn't growing any more, speaking in a professional sense. I realized that certain conditions to further develop my skills and talents had vanished over time. (Maybe I'll describe those conditions a bit more at another time.) I felt a growing incompatibility with my work environment and when I had the opportunity to sign a dissolution contract in October, I decided to take it. </p>
<p>Since then, I've been doing a bit of freelance work from home (or my "office desk" at Starbucks). More importantly, I caught up on the paternity leave I didn't take when Amalia was born. It feels so great to have quality time with my family and at the same time enjoy the freedom to work on the things I have a passion for!</p>
<p>I've thought hard about my next steps. Shouldn't I look for another job providing me and my family security? Oh shoot, it's 2010 and job security a thing of the past. Going into another employment has virtually the same risk as starting your own business nowadays.</p>
<p>But working hard in IT management would at least grant me a decent salary over the next few years, wouldn't it? Yes, it probably would. But why work hard for other people's wallet when I could do the same for my own -- and towards my own goals and to my own rules?</p>
<p>After reading an informative book on how to properly start a business in Germany, I read "<a href="http://crushitbook.com/" title="Crush It!">Crush It!</a>", "<a href="http://www.escapefromcubiclenation.com/" title="Escape from Cubicle Nation">Escape from Cubicle Nation</a>" and "<a href="http://www.meconomy.me" title="Meconomy">Meconomy</a>". And then I decided to go on a new journey of personal and professional growth by starting a full-time business.</p>
<h2>Going freestyle</h2>
<p>Now, what kind of business? I've been thinking hard about that question and also talked about it with some friends. I still haven't finished my business concept yet, but it'll certainly involve the things I'm passionate about: high performance information technology, open source software, consulting and training services.</p>
<p>It'll also be about finding new ways of doing things. That's why I chose "Freistil Consulting" as the company name.</p>
<h2>What lies ahead</h2>
<p>As I said, I'm working on the business concept. I've done a rough business model, but the detailed business plan sketching out all the strategic and financial points will still require some effort: exploring my strengths and weaknesses, checking on chances and threats, talking with my tax consultant, my financial advisor, and, most importantly, with potential clients.</p>
<p>The number one condition is already met, though: I have the official support of my family, for which I'm very grateful.</p>
<p>At the same time, I'm working on the technical side of things, writing concepts as well as building a  basic IT infrastructure. I finally understand the general enthusiasm for Amazon EC2.</p>
<p>From now on, I'm going to write regular posts about my experiences growing (with) my business. Having just discovered <a href="http://www.weeknotes.com" title="Weeknotes">Weeknotes</a>, it'll probably be in a weekly format. Is there something you're especially interested in? And please tell me your thoughts in the comments, I'll highly appreciate it!</p>
