---
layout: post
title: "I just started my first tumblelog"
microblog: false
guid: http://geewiz.micro.blog/2009/04/06/i-just-started.html
date: 2009-04-06T05:36:16+0200
type: post
url: /2009/04/06/i-just-started.html
---

<p>From time to time, there are some random things I'd like to point my friends to. So far, I've either posted them on <a href="http://www.twitter.com/geewiz">Twitter</a> or in a short entry on my blog here.</p>
<p>A few days ago, I took a look at <a href="http://www.tumblr.com">Tumblr</a> and decided to start my own seperate tumblelog. That way, I can focus my blog here on the more elaborate entries.</p>
<p>So, if you're so inclined, take a look and subscribe to the feed of <a href="http://assocdisarray.tumblr.com">Associative Disarray</a>. It should be fun!</p>
