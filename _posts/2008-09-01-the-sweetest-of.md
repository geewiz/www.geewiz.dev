---
layout: post
title: "The sweetest of them all"
microblog: false
guid: http://geewiz.micro.blog/2008/09/01/the-sweetest-of.html
date: 2008-09-01T21:59:15+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm4static_wdinw-scaled500.jpg?w=225
photos:
- /assets/wp-content/2011/05/media_httpfarm4static_wdinw-scaled500.jpg?w=225
url: /2008/09/01/the-sweetest-of.html
---

<p>I really don't want to start the next daddy blog, but since Amalia currently does -- and for the foreseeable future will -- take a central place in my life, there'll be one or the other post about her.</p>
<p>And, be honest, could you hold back on your pride if you had such a gorgeous litte daughter?</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm4static_wdinw" height="500" src="/assets/wp-content/2011/05/media_httpfarm4static_wdinw-scaled500.jpg?w=225" width="375" />
</div></p>
<p>Don't even get me started about how fascinating it is how she's developing her own body and sound language. And how totally amazed I am watching her successfully try and grab the cord of the jumping jack at her changing table. Seriously, don't ask me. My monologue would be more intense than any Linux pep talk I ever gave. And that means something.</p>
