---
layout: post
title: "Nachtschicht"
microblog: false
guid: http://geewiz.micro.blog/2004/11/24/nachtschicht.html
date: 2004-11-24T12:43:00+0200
type: post
url: /2004/11/24/nachtschicht.html
---
<br />
<blockquote>
<p>Wer konfiguriert noch so sp&auml;t bei Nacht und Wind?<br />
  Es ist der Admin -- ich glaub, der spinnt!<br />
  Er hat die .conf in den vi geladen<br />
  die Backupdatei bewahrt sie vor Schaden.</p>
<p>"Mein Server, von welchem Problem dein Logfile spricht?"<br />
  "Siehst, Admin, du den URLk&ouml;nig nicht?<br />
  Die IP-Adresse mit Port und mit Host?"<br />
  "Mein Server, das ist nur ein Heise-Post."</p>
<p>"Du lieber Server, komm geh' mit mir!<br />
  Gar sch&ouml;ne Spiele spiel ich mit dir,<br />
  Mit Software ganz ohne BO und Race,<br />
  Mein Kumpel hat manches modische Case."</p>
<p>"Mein Admin, mein Admin, protokollierst du nicht,<br />
  Was URLk&ouml;nig mir leise verspricht?"<br />
  "Bleib k&uuml;hl, o bleibe doch k&uuml;hl, mein Ger&auml;t,<br />
  Damit es dir nicht noch die CPU br&auml;t."</p>
<p>"Willst, feiner Rackmount, du mit mir gehn,<br />
  Meine Admins sollen dich warten sch&ouml;n,<br />
  Meine Admins schuften in n&auml;chtlichem Reihn<br />
  Und binden dich in ihr WLAN ein."</p>
<p>"Mein Admin, mein Admin, siehst du nicht im Dump<br />
  URLk&ouml;nigs Admins ver&auml;ndern den JMP!"<br />
  "Mein Server, mein Server, ich seh es genau,<br />
  Das Wallpaper mit einer nackigen Frau."</p>
<p>"Ich lieb dich, mich reizet dein Platteninhalt,<br />
  Und bist du nicht willig, so brauch ich Gewalt!"<br />
  "Mein Admin, mein Admin, jetzt loggt er sich ein,<br />
  URLk&ouml;nig will mein neuer root-User sein."</p>
<p>Dem Admin grausets, er hackt wie ein Wilder,<br />
  Sein X11R6 zeigt blo&szlig; seltsame Bilder,<br />
  Dr&uuml;ckt die Enter-Taste mit M&uuml;he und Not,<br />
  Am Netzwerkkabel der Server war tot.</p>
</blockquote>
<p>p. (W&auml;hrend einer Loadbalancer-Installation nachts um halb drei. Entschuldigung an J.W. von Goethe.)</p>
