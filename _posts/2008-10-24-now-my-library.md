---
layout: post
title: "Now my library is delicious, too"
microblog: false
guid: http://geewiz.micro.blog/2008/10/24/now-my-library.html
date: 2008-10-24T21:46:20+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm4static_xkudq-scaled500.jpg?w=240
photos:
- /assets/wp-content/2011/05/media_httpfarm4static_xkudq-scaled500.jpg?w=240
url: /2008/10/24/now-my-library.html
---

<p>I've been trying the free version of <a href="http://www.delicious-monster.com/">Delicious Library 2</a> for some days now. I hadn't been sure if I'd shell out the money to buy the full license -- it's just a collection management application after all!</p>
<p>But when I put another book in front of my MacBook's built-in webcam to scan in its barcode and the application told me that it won't accept more than 25 items, I couldn't stop myself from ripping out my credit card.</p>
<p><a href="http://www.flickr.com/photos/44494983@N00/2912282675" title="View 'DeliciousLibrary' on Flickr.com">
<div style="text-align:center;">
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm4static_xkudq" height="176" src="/assets/wp-content/2011/05/media_httpfarm4static_xkudq-scaled500.jpg?w=240" width="240" />
</div>
</div>
<p></a></p>
<p>This application is amazing. It doesn't do much more than managing your book, movie and music library. But it does it in a way that's totally appealing. Delicious Library displays your stuff neatly arranged on a virtual shelf. The tedious task of entering new items is made easy as pie by using the eyeSight camera as a barcode scanner. Every title recognized will be read out loudly by the OS X speech synthesizer. If you lend out a book, you just drag it onto the friend's contact (imported from the OS X Address Book) and enter the return date. Until checked in again, the item will be displayed as a ghostly image.</p>
<p>I'm still surprised how knee-jerk this one software purchase happened. Seems like I'm getting more and more spontaneous the longer I've been using a Mac. I notice the same disturbing development when visiting the iTunes Music and App Store. I'll have to keep an eye on that behaviour.</p>
<p>Anyway, you can see what's on my shelves at <a href="http://library.geewiz.de">library.geewiz.de</a>.</p>
