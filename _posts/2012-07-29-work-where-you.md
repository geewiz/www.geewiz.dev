---
layout: post
title: "Work where you work best"
microblog: false
guid: http://geewiz.micro.blog/2012/07/29/work-where-you.html
date: 2012-07-29T22:04:17+0200
type: post
images:
- http://www.jochen-lillich.de/wp-content/uploads/2012/07/setup-2012-1.jpg
- http://www.jochen-lillich.de/wp-content/uploads/2012/07/setup-2012-2.jpg
photos:
- http://www.jochen-lillich.de/wp-content/uploads/2012/07/setup-2012-1.jpg
- http://www.jochen-lillich.de/wp-content/uploads/2012/07/setup-2012-2.jpg
url: /2012/07/29/work-where-you.html
---
<p>I love working on my own terms. For many years now, I&#8217;m deeply convinced that the freedom of choosing your own work style is an important basis for good work results.</p>
<p>Working from home can be a really great way of achieving a balance between work and the other things that are equally important. I&#8217;m not going to call this &#8220;work/life balance&#8221; because to me, work is not an opposite to life but an integral part of it. The flexibility of working from home was one of the reasons I decided to make freistil IT a virtual company. My daugher was only about a year old and I intended not to miss most of her early development sitting in an office away from home. </p>
<p>Like walking a tightrope, achieving work/other balance needs constant adjustment and there are many things that threaten to throw you off. A screaming baby will quickly steal your attention and a stack of unwashed dishes is a great excuse to procrastinate. In my case, another kind of distraction that recently happened is going to lead to new screaming next January. ;-)</p>
<p>With a bit of reason and discipline, though, working from home has great advantages. For example, doing productive work while others waste their time commuting. If you&#8217;re interested in or already practicing working from home, I recommend you subscribe to the &#8220;Home Work&#8221; podcast. Aaron and Dave have great insight into home office reality.</p>
<p>I found that my home office isn&#8217;t the holy grail of work spaces, though. Unfortunately, I won&#8217;t have a separate room until we move house next year. The main problem of this situation is that I&#8217;m physically at home while being mentally at work. During the time my daughter is in day care, it&#8217;s rather quiet, so I only have to fight for focus on the weekends. But sharing the place with my partner all the time without sharing her current tasks and concerns can also be a source of conflict.</p>
<p>That&#8217;s why I&#8217;ve tried the offer of a local real estate company that rents office space by the hour. (Yes, this model can actually also be used for rooms with a desk.) Every time I needed a change of scenery or had to make sure I&#8217;m protected from disturbances, I booked a room for a few hours and went to town (literally). This worked quite well and proved to be a good complement to my desk at home.</p>
<p>Coworking spaces are another &#8220;lean&#8221; alternative and I have first-hand experience since we have our two-day strategy meetings at the coworking space my business partner Markus helps to run. They offer inexpensive desk space as well as other important infrastructure like a printer and a fridge stocked with Club Mate. They also provide something I didn&#8217;t realize I was missing at first: social interaction. Joining folks for lunch or just having a chat while waiting for the coffee to brew can be a healthy break from self-imposed isolation and an opportunity to exchange thoughts.</p>
<p>Unfortunately, there is no real coworking space in my home town. But there&#8217;s a company that does office sharing on a daily or monthly basis, and I&#8217;ve decided to give them a go. After a test period of a few days, I got myself a good office chair and a big monitor and moved in permanently. So, this is how my work place looks now:</p>
<p><img style="display:block; margin-left:auto; margin-right:auto;" src="http://www.jochen-lillich.de/wp-content/uploads/2012/07/setup-2012-1.jpg" alt="My new office" title="My new office" border="0" width="400" height="300" /></p>
<p>I use the screen in the middle for my current task, the laptop screen on the right displays chat windows connecting me with colleagues and customers, and the one on the left shows a dashboard view of our IT infrastructure.</p>
<p><img style="display:block; margin-left:auto; margin-right:auto;" src="http://www.jochen-lillich.de/wp-content/uploads/2012/07/setup-2012-2.jpg" alt="My triple-screen setup" title="My triple-screen setup" border="0" width="400" height="300" /></p>
<p>So far, I&#8217;m really happy working at this office community. The building is in the city centre, about 50m from my favourite Starbucks &#8211; in other words, perfectly located. My office mates are nice and their different occupations make for interesting chats. </p>
<p>I can work there whenever I want, too &#8211; I wrote this blog entry at the office on a Sunday afternoon. I was in need of some solitary time, so I first had a Chai Tea Latte at Starbucks, where I read the second half of Steven Pressfield&#8217;s &#8220;Turning Pro&#8221;. His writing in turn motivated me to get something done myself, so I went the few steps to my desk and started typing. Now I feel balanced again.</p>
