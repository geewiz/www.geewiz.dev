---
layout: post
title: "Amazon bietet eigenes Linux-AMI f&uuml;r EC2"
microblog: false
guid: http://geewiz.micro.blog/2010/09/15/amazon-bietet-eigenes.html
date: 2010-09-15T22:46:19+0200
type: post
url: /2010/09/15/amazon-bietet-eigenes.html
---

<div><a href="http://aws.typepad.com/aws/2010/09/introducing-amazon-linux-ami.html"></a><a href="http://aws.typepad.com/aws/2010/09/introducing-amazon-linux-ami.html">http://aws.typepad.com/aws/2010/09/introducing-amazon-linux-ami.html</a></div>
<p /><span>"Many of our customers have asked us for a simple starting point for launching their Linux applications inside of Amazon EC2 that is easy to use, regularly maintained, and optimized for the Amazon EC2 environment. Starting today, customers can use Amazon Linux AMI to meet these needs.&nbsp; This just adds to the great selection of AMI options in Amazon EC2 that range from free to paid, giving you access to the operating systems and environments you need."</span></p>
