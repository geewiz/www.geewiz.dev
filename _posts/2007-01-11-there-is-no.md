---
layout: post
title: "There is no bad publicity"
microblog: false
guid: http://geewiz.micro.blog/2007/01/11/there-is-no.html
date: 2007-01-11T22:16:05+0200
type: post
url: /2007/01/11/there-is-no.html
---

<p>Wouldn't it be great if getting sued for patent infringement was covered by your company's marketing budget, too? :-)</p>
