---
layout: post
title: "Kommentare, die die Welt nicht braucht"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/kommentare-die-die.html
date: 2006-06-18T06:51:01+0200
type: post
url: /2006/06/18/kommentare-die-die.html
---

<p>Es gibt verschiedene Stile, Fu&szlig;ballspiele zu kommentieren. "Sagenhaft unverst&auml;ndlich", "ungeheuer dynamisch" und "v&ouml;llig nichtssagend" sind die drei davon, die sich die deutschen Kommentatoren Steffen Simon, Reinhold Beckmann und B&eacute;la Rethy angeeignet haben.</p>
<p>Jedenfalls nach Meinung der FAZ in ihrem Artikel <a href="http://www.faz.net/s/Rub47986C2FBFBD461B8A2C1EC681AD639D/Doc">Drei Arten, sinnfrei zu berichten</a>~E308AD8789D6B4135A7926ECC351FAE34~ATpl~Ecommon~Scontent.html.</p>
<p>Was meint ihr? Schaltet ihr bei WM-&Uuml;bertragungen auch lieber den Ton aus?</p>
