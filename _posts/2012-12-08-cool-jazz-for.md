---
layout: post
title: "Cool jazz for cold weather"
microblog: false
guid: http://geewiz.micro.blog/2012/12/08/cool-jazz-for.html
date: 2012-12-08T18:12:29+0200
type: post
url: /2012/12/08/cool-jazz-for.html
---
<p>I decided to spend some quiet time at the office today. After a few stressful days which made me take the tram to save time, it was time to do my 5000 steps to get into the city again.</p>
<p>Inspired by David Sparks' blog post "<a href="http://macsparky.com/blog/2012/12/farewell-dave-brubeck">Farewell Dave Brubeck</a>" which I read this morning, I chose the "Time Out" album for my walking entertainment instead of my usual podcasts or audiobooks.</p>
<p>I enjoyed listening to the music of the recently deceased Jazz giant very much while walking through the cold winter air. Thanks to my Stupidity Shield&trade; earbuds, I heard almost nothing from my surroundings and I felt like in the opening scene of "When Harry met Sally".</p>
<p>The only problem with listening to Brubeck during a walk is that synchronizing your steps with the music will be difficult (and certainly awkward) with many of his songs. As David puts it:</p>
<blockquote>
<p>"Dave will be missed but people will be unsuccessfully trying to tap their feet to his music long after anyone remembers any of us. Farewell Dave."</p>
</blockquote>
