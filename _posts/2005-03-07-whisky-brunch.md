---
layout: post
title: "Whisky Brunch"
microblog: false
guid: http://geewiz.micro.blog/2005/03/07/whisky-brunch.html
date: 2005-03-07T21:15:35+0200
type: post
url: /2005/03/07/whisky-brunch.html
---

<p>Am Sonntag war Whiskybrunch bei <a href="http://blog.koehntopp.de">Isotopp</a> angesagt.Wenn man vorhat, einige Whiskysorten zu testen, sollte man zun&auml;chst f&uuml;r eine gute Grundlage sorgen, und das war bestens gew&auml;hrleistet: W&uuml;rstchen, Schinken, K&auml;se, R&uuml;hrei, Mozarella und -- unter grober Missachtung der Atkins-Regeln -- Br&ouml;tchen und Brezeln vertrieben erfolgreich den morgendlichen Hunger. An die Zazikiso&szlig;e wagte ich mich erst sp&auml;ter. Sie war superlecker, aber Carolin hasste mich noch heute morgen daf&uuml;r.</p>
<p>Wir entschieden uns, Isos neue Serie von <a href="http://www.isleofjura.com/">Isle of Jura</a> Whiskys zu verkosten, die aus einem 10-, einem 16- und einem 21-j&auml;hrigen bestand und durch einen Blend mit dem Namen "Superstition" erg&auml;nzt wurde. W&auml;hrend dem 10-j&auml;hrigen am Aroma von Rauch und Blumenwiese noch die Flegeljahre anzumerken waren, verhielt sich der 16-j&auml;hrige schon deutlich gem&auml;&szlig;igter. Hier traten auch Andeutungen von Honig zu Tage. Der 21-j&auml;hrige &uuml;berraschte uns dadurch, dass er diese Entwicklung nicht einfach fortsetzte, sondern einen ganz eigenst&auml;ndigen Geschmack zu Tage f&ouml;rderte, der u.a. von Gew&uuml;rzen gepr&auml;gt war. Der "Superstition" verbl&uuml;ffte uns durch ein dominantes Torfaroma, das wir in dieser St&auml;rke bei keinem der vorigen Whiskys festgestellt hatten.</p>
<p>Dann wechselten wir die Insel: Ofu und Steffi hatten nochmal ihre beiden gleich alten Bowmores mitgebracht, was mir die Erkenntnis erm&ouml;glichte, dass ein Single Cask keineswegs etwas Besonderes gegen&uuml;ber einem Blended Whisky sein muss. Der Bowmore Single Cask &uuml;berzeugte n&auml;mlich weder durch Farbe noch durch Aroma -- ganz im Gegensatz zur normalen Bowmore-Flasche. Hier zeigte sich deutlich, warum der betreffende Handwerkstitel "Master Blender" heisst.</p>
<p>Den Abschluss machten wir mit den <a href="http://www.islaywhiskysociety.com/map.htm">Islay-Whiskys</a> Ardbeg und Lagavulin. Merke: Islay ist nichts f&uuml;r Warmduscher -- weder die Insel noch der Scotch.</p>
<p>Gegen halb sechs sa&szlig; ich schlie&szlig;lich wieder in der Stra&szlig;enbahn, dank Knoblauch und Alkohol mit einem sch&ouml;nen warmen Gef&uuml;hl im Bauch. Mein erstes ausgiebiges Whisky Tasting hat mir viel Spass gemacht -- ich hab mich blendend ;) unterhalten und einiges &uuml;ber schottische Single Malts gelernt.</p>
<p>Zur Revanche steht nun ein Irish Whiskey Brunch an, Termin wird noch festgelegt.</p>
