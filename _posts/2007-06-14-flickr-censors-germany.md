---
layout: post
title: "Flickr censors Germany"
microblog: false
guid: http://geewiz.micro.blog/2007/06/14/flickr-censors-germany.html
date: 2007-06-14T04:18:05+0200
type: post
images:
- /uploads/flickrcensorship.jpg
photos:
- /uploads/flickrcensorship.jpg
url: /2007/06/14/flickr-censors-germany.html
---

<p>Flickr seems to be concerned with the morality of us Germans:</p>
<blockquote class="posterous_medium_quote">
<p>Note: If your Yahoo! ID is based in Singapore, Germany, Hong Kong or Korea you will only be able to view safe content based on your local Terms of Service so wont be able to turn SafeSearch off.</p>
</blockquote>
<p>Well, we're glad censorship is more or less history in our country, thank you very much!</p>
<p><a href="http://www.flickr.com/photos/assbach/543849824/" class="serendipity_image_link">
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/flickrcensorship.jpg" />
</div>
<p></a></p>
<p>My Flickr Pro subscription ends in July. If this censorship won't be removed, my subscription is certainly not going to be continued.</p>
