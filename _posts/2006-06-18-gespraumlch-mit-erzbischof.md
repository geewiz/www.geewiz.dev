---
layout: post
title: "Gespr&auml;ch mit Erzbischof Zollitsch"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/gespraumlch-mit-erzbischof.html
date: 2006-06-18T09:02:34+0200
type: post
url: /2006/06/18/gespraumlch-mit-erzbischof.html
---

<p>In einem Brief hatte der Bund der Deutschen Katholischen Jugend (BDKJ) den Freiburger Erzbischof Robert Zollitsch um ein Gespr&auml;ch gebeten, das am 8. Juni nun auch stattfand. Das Anliegen des Gespr&auml;chs war es, zu verdeutlichen, welche Auswirkungen die vielen di&ouml;zesanen Prozesse und Veranstaltungen auf die Arbeit der Ehrenamtlichen haben.</p>
<p>Ich war als einer von 6 Vertretern des BDKJ an dem Gespr&auml;ch im Erzbisch&ouml;flichen Ordinariat beteiligt und fand es &auml;u&szlig;erst produktiv. Wir hatten uns darauf vorbereitet, dem Erzbischof plausibel machen zu m&uuml;ssen, dass die Jugendverb&auml;nde einerseits gern am di&ouml;zesanen Leben teilnehmen m&ouml;chten, andererseits aber mit ihrer Arbeit und der Ausrichtung auf neue kirchliche und gesellschaftliche Gegebenheiten schon weitgehend ausgelastet sind. Statt dessen durften wir feststellen, dass Erzbischof Zollitsch eine sehr pragmatische und verst&auml;ndnisvolle Sicht hat: "Ich schaue prim&auml;r auf das, was die Jugendverb&auml;nde leisten und auf das, wo sie sich engagieren, und nicht auf das, wo sie nicht dabei sind."</p>
<p>In den vergangenen 2 Jahren war ich aufgrund der K&uuml;rzungen kirchlicher Mittel, speziell bei der CAJ, oft sehr frustriert. Das positive Gespr&auml;ch mit Erzbischof Zollitsch macht mir aber Hoffnung, dass unsere Jugendverb&auml;nde in der Erzdi&ouml;zese gut aufgehoben sind.</p>
<p>Die offizielle Pressemeldung des BDKJ &uuml;ber das Gespr&auml;ch findet sich auf der <a href="http://www.kja-freiburg.de/efj/dcms/sites/kja/politik/index.html?f_action=show">KJA-Homepage</a>&amp;f_newsitem_id=1282.</p>
