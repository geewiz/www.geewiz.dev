---
layout: post
title: "GMail is good, but not perfect"
microblog: false
guid: http://geewiz.micro.blog/2006/12/16/gmail-is-good.html
date: 2006-12-16T04:05:19+0200
type: post
url: /2006/12/16/gmail-is-good.html
---

<p>In Mike Arrington's opinion, the new POP fetching mechanism was Google Mail's missing piece to <a href="http://www.techcrunch.com/2006/12/09/uh-oh-gmail-just-got-perfect/.">perfection</a> Even though I like GMail very much, I'm not quite that excited. Neither is Om from <a href="http://webworkerdaily.com/2006/12/09/gmail-still-isnt-perfect-even-with-mail-fetcher/.">Web Worker Daily</a></p>
<p>If I want to collect emails in a central Google mailbox, I can simply forward emails from other accounts to GMail. But with the central mailbox, there comes the problem of multiple email addresses: Regardless if mails came in via forwarding or via polling another account, users usually want to answer them with the email address the mails were originally directed to. GMail actually does allow sending responses with another verified sender address, but the mandatory "Sender" header Om describes ("on behalf of [@gmail.com](http://gmail.com)") confuses some email clients like MS Outlook and gets displayed to the recipient instead of the sender address chosen. So, your usage of GMail as your central mail hub is not really as transparent to the public as you'd probably like.</p>
<p>Having access to my email everywhere I have web access (and with the Nokia E61, that's pretty much everywhere I have a cell phone connection) is really nifty. And I like Google Mail's spam filter and tagging features. But on the other hand, the service still lacks import and export functions that allow users to transfer all their archived mail to Google as well as to back up their important communication pieces. Oh, and POP doesn't get replaced more and more by IMAP for no reason -- what about offering this much more powerful mail access protocol? Integration with Google Calender could be improved, too. So, the service still has some steps to go on its road to perfection. </p>
<p>Well, with Christmas coming and all, what's on your wish list for a perfect email solution?</p>
