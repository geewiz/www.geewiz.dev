---
layout: post
title: "These podcast thingies seem to be&nbsp;all the rage"
microblog: false
guid: http://geewiz.micro.blog/2014/12/03/these-podcast-thingies.html
date: 2014-12-03T20:17:36+0200
type: post
url: /2014/12/03/these-podcast-thingies.html
---
<p>In recent weeks, I've seen articles about how podcasting is the&nbsp;new radio popping up everywhere. What has happened? It&rsquo;s been years since I&rsquo;ve unsubscribed from Adam Curry&rsquo;s Daily Source Code after years of listening to it. Over time, I&rsquo;ve spent my children's college fund on buying every new podcatching app I could get my hands on. Has&nbsp;podcasting really been that obscure that mainstream publications now regard it as a new sensation?&nbsp;</p>
