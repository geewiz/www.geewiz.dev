---
layout: post
title: "Wiki Reloaded"
microblog: false
guid: http://geewiz.micro.blog/2005/01/07/wiki-reloaded.html
date: 2005-01-07T07:11:00+0200
type: post
url: /2005/01/07/wiki-reloaded.html
---

<p>Vor einiger Zeit hatte ich schon mal eins, und weil ich das Wiki in der Firma so unheimlich n&uuml;tzlich finde, habe ich mir erneut ein pers&ouml;nliches eingerichtet.</p>
<p>Unter <a href="http://wiki.jochen-lillich.de">http://wiki.jochen-lillich.de</a> werde ich ab sofort Artikel zu technischen Themen ver&ouml;ffentlichen. Zuerst will ich mal meine ganzen bestehenden Artikel einpflegen.</p>
<p>Ein Wiki macht erst dann wirklich Sinn, wenn es die Zusammenarbeit f&ouml;rdert. Daher lade ich euch heftigst dazu ein, meine Machwerke zu erg&auml;nzen und zu korrigieren.</p>
