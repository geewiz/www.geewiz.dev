---
layout: post
title: "Well, I understand that it's a big deal..."
microblog: false
guid: http://geewiz.micro.blog/2010/11/17/well-i-understand.html
date: 2010-11-17T12:35:49+0200
type: post
images:
- /assets/wp-content/2011/05/beatles_by_intern.jpg?w=267
photos:
- /assets/wp-content/2011/05/beatles_by_intern.jpg?w=267
url: /2010/11/17/well-i-understand.html
---

<div class='p_embed p_image_embed'>
<img alt="Beatles_by_intern" height="505" src="/assets/wp-content/2011/05/beatles_by_intern.jpg?w=267" width="450" />
</div>
<p>The music industry finally lost their last stronghold. The iTunes <br />Store has become <strong>the</strong> music distribution channel of <br />the digital age.
<p /> But Steve, when this thing so important to you that you even give it <br />the apple.com homepage, then <em>why let an intern do the <br />photoshopping</em>?</p>
