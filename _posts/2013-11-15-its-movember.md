---
layout: post
title: "It's Movember!"
microblog: false
guid: http://geewiz.micro.blog/2013/11/15/its-movember.html
date: 2013-11-16T01:32:00+0200
type: post
images:
- http://jochen.lillich.co/wp-content/uploads/2013/11/Photo-on-15-11-2013-at-10.07-300x300.jpg
photos:
- http://jochen.lillich.co/wp-content/uploads/2013/11/Photo-on-15-11-2013-at-10.07-300x300.jpg
url: /2013/11/15/its-movember.html
---
<p>Up until this year, I&#8217;ve never paid much attention to the <a href="http://www.movember.com/" title="Movember Worldwide - Home">Movember</a> movement. People growing facial hair in a way that should have been banned since the 80s, what&#8217;s that all about? But this year, it dawned on me that I really should look into the issue the Movember movement tries to shed more light on: Men are getting cancer. And with cancer victims in both our families, that topic is far from irrelevant to me.</p>
<p>When I saw a friend&#8217;s Facebook post about him taking part in Movember, I spontaneously decided that I&#8217;ll <a href="http://mobro.co/geewiz" title="Movember Ireland - Jochen Lillich">join the fun</a>, grow a mustache and finally learn about the health issues I may need to face as I grow older.</p>
<p>Speaking of face, here&#8217;s my progress so far:</p>
<p><a href="http://jochen.lillich.co/wp-content/uploads/2013/11/Photo-on-15-11-2013-at-10.07.jpg"><img src="http://jochen.lillich.co/wp-content/uploads/2013/11/Photo-on-15-11-2013-at-10.07-300x300.jpg" alt="Photo on 15-11-2013 at 10.07" width="300" height="300" class="aligncenter size-medium wp-image-2249" /></a></p>
<p>What do you think? Leave me a comment below!</p>
<p>It looks like fall puts me in a spontaneous mood because, a week ago, I also decided on a whim to participate in the <a href="http://dublin.mo-running.com/" title="MoRunning - Dublin">Dublin Mo Run</a> this Saturday. In that context, it&#8217;s important to know that I stopped running in November last year due to tendonitis in both of my legs and that I haven&#8217;t done a round since then. I registered myself for the Mo Run regardless and did two test runs this week to see how I do. Surprisingly, I managed to run the 5k without major problems. It must be the two children that keep me on my feet day and night. ;-) And that I&#8217;ve lost 10kg since June probably also helped a bit.</p>
<p>So, the geewiz family will head out to Dublin early morning tomorrow to see a lot of people carrying furry animals on their upper lip across Phoenix Park. My start number will be 26.</p>
<p>May I ask you, my dear reader, for a favor? <strong>Support Movember</strong> by making a donation. In Ireland, prostate cancer is the second most common cancer in men; each year 1 in 8 men is diagnosed with prostate cancer. Movember is a fun way to lower these numbers. Simply go to my <a href="http://mobro.co/geewiz" title="Movember Ireland - Jochen Lillich">Mo Bro page</a> and help fight a terrible disease!</p>
