---
layout: post
title: "Sat gesehen"
microblog: false
guid: http://geewiz.micro.blog/2006/02/27/sat-gesehen.html
date: 2006-02-27T03:53:55+0200
type: post
url: /2006/02/27/sat-gesehen.html
---

<p>Nachdem der Verteiler f&uuml;r unser heimisches Satellitenfernsehen schon l&auml;nger Zicken machte, wechselten ihn Tom und ich gestern gegen einen handels&uuml;blichen Multischalter aus. Seitdem hab ich kein Fernsehen mehr. :-(Es stellte sich n&auml;mlich heraus, dass bei der Aufr&uuml;stung der Anlage auf Digitalfernsehen vor vielen Jahren der Twin-LNB an der Sch&uuml;ssel nicht gegen einen Quad-LNB ausgetauscht wurde, sondern die zwei Eingangsleitungen einfach mittels einer Bereichsweiche auf die 4 ben&ouml;tigten ZF-Leitungen verteilt wurden. Diese Weiche funktioniert aber offensichtlich nur mit dem alten Verteiler. Wir konnten zun&auml;chst also nur die beiden Eingangsleitungen auf die f&uuml;r Analog-TV genutzten LowBand-Eing&auml;nge des Multischalters legen. Meine dbox bekommt daher derzeit kein Signal.</p>
<p>So bleibt mir nichts anderes &uuml;brig als m&ouml;glichst bald einen Quad-LNB zu installieren. Bis dahin ist fernsehfreie Zeit f&uuml;r mich. Vielleicht ist's ja zu was gut...</p>
