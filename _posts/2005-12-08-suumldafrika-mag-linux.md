---
layout: post
title: "S&uuml;dafrika mag Linux"
microblog: false
guid: http://geewiz.micro.blog/2005/12/08/suumldafrika-mag-linux.html
date: 2005-12-09T00:56:49+0200
type: post
url: /2005/12/08/suumldafrika-mag-linux.html
---

<p>S&uuml;dafrika ist erstaunlich fruchtbarer Boden f&uuml;r Open Source Software, wie man an augenf&auml;lligen Beispielen sehen kann. Schon seit einiger Zeit entwickelt sich Ubuntu Linux unter den Fittichen des dort lebenden Million&auml;rs Marc Shuttleworth zu einer der erfolgreichsten Linux-Distributionen.</p>
<p>Die Verwaltung von Kapstadt schlie&szlig;t sich jetzt der Entwicklung an, wie eine Stellungnahme der st&auml;dtischen IT-Abteilung erkl&auml;rt: <a href="http://www.pro-linux.de/news/2005/9003.html">Kapstadt will f&uuml;hrender Anwender freier Software werden</a>, insbesondere im Desktopbereich.</p>
<p>Eine interessante Entwicklung. Sollte eine gewisse Ethnologin mal einen Job in S&uuml;dafrika finden, d&uuml;rfte das f&uuml;r einen gewissen Informatiker ebenfalls nicht so schwer werden. :)</p>
