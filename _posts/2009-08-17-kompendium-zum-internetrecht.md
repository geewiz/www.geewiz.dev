---
layout: post
title: "Kompendium zum Internetrecht aktualisiert"
microblog: false
guid: http://geewiz.micro.blog/2009/08/17/kompendium-zum-internetrecht.html
date: 2009-08-17T15:44:37+0200
type: post
url: /2009/08/17/kompendium-zum-internetrecht.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p>Der M&uuml;nsteraner Jura-Professor <a href="http://www.uni-muenster.de/Jura.itm/hoeren/" rel="external" target="_blank">Thomas Hoeren</a> hat erneut eine aktualisierte Fassung des Skripts Internetrecht vorgelegt. Integriert wurden die zahlreichen Gesetzesvorhaben der laufenden Legislaturperiode, zum Beispiel die Novellierungen zum <a href="http://www.heise.de/newsticker/Bundestag-verabschiedet-Datenschutzreform--/meldung/141551" title="Bundestag verabschiedet Datenschutzreform">Bundesdatenschutzgesetz</a> und das <a href="http://www.heise.de/newsticker/Gesetz-zu-Web-Sperren-passiert-den-Bundesrat--/meldung/141849" title="Gesetz zu Web-Sperren passiert den Bundesrat">Zugangserschwerungsgesetz</a>. Die neue Ausgabe liegt auf der Website des Instituts f&uuml;r Informations-, Telekommunikations- und Medienrecht der Universit&auml;t M&uuml;nster als PDF-Datei zum <a href="http://www.uni-muenster.de/Jura.itm/hoeren/materialien/Skript/Skript_September2009.pdf" rel="external" target="_blank">Download</a> bereit. </p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.heise.de/newsticker/Kompendium-zum-Internetrecht-aktualisiert--/meldung/143595">heise.de</a></div></p>
</div>
