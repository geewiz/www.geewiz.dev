---
layout: post
title: "Make temporary files non-executable"
microblog: false
guid: http://geewiz.micro.blog/2012/01/02/make-temporary-files.html
date: 2012-01-02T17:06:09+0200
type: post
url: /2012/01/02/make-temporary-files.html
---
<p>At <a href="http://www.drupalconcept.com">DrupalCONCEPT</a> operations, our intrusion detection system recently notified us that it found a rootkit in the directory <code>/dev/shm</code> on one of our servers. This directory is writeable by the Apache webserver, so attackers that find a vulnerability in the installed software are able put hostile content (aka rootkits) there.</p>
<p>Of course, the vulnerability shouldn&#8217;t be there in the first place. We&#8217;re doing security updates all the time, but only on the OS and hosting infrastructure level. Since the actual web applications running on our infrastructure (in our case, Drupal) are maintained by our customers, we don&#8217;t have the same kind of tight control here as we have on the OS level.</p>
<p>Okay, we may not be able to prevent attackers from deploying their scripts. But we can prevent those scripts from doing any harm. This is where the <code>noexec</code> filesystem option comes in handy. Files on filesystems that have this option enabled can&#8217;t be executed even if they have their execution permissions (&#8220;x&#8221; ) set.</p>
<p>We use a Chef recipe to modify <code>/etc/fstab</code> accordingly. The first <code>execute</code> resource does a remount of the <code>/dev/shm</code> filesystem, but only if triggered by another resource. Namely, the following <code>bash</code> resource that modifies <code>/etc/fstab</code> if it&#8217;s not already hardened:</p>
<p>[gist id=1550976]</p>
<p>Since we include this recipe in our <code>base</code> Chef role, it&#8217;s applied to every server we set up.</p>
