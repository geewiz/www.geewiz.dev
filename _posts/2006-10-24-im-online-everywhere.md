---
layout: post
title: "I'm online everywhere but at home"
microblog: false
guid: http://geewiz.micro.blog/2006/10/24/im-online-everywhere.html
date: 2006-10-24T06:50:40+0200
type: post
url: /2006/10/24/im-online-everywhere.html
---

<p>I still got no DSL at home. Deutsche Telekom really sucks bricks. </p>
<p>So, I'm using the opportunity of at least having WLAN access in my room at the <a href="http://beta.plazes.com/plaze/98f5044f60416df65a541ef555d2b149/">Hotel Global Inn</a> in Wolfsburg where I'm staying until Wednesday teaching Perl. Material for blog entries queueing up, so much stuff to read...</p>
<p>How am I supposed to live my online life while having no broadband for weeks? Man, I'll so celebrate when the first bytes come down my DSL line in Freiburg.</p>
