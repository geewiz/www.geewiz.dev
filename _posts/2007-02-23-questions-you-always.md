---
layout: post
title: "Questions you always wanted to ask a blogger"
microblog: false
guid: http://geewiz.micro.blog/2007/02/23/questions-you-always.html
date: 2007-02-24T00:41:00+0200
type: post
url: /2007/02/23/questions-you-always.html
---

<p>Robert Basic seems to have been quite annoyed by the ever-recurring questions and statements people pose about his blogging habit, so he finally wrote a blog entry (in german), covering the following:</p>
<h1>But you mustn't go blogging this!</h1>
<h1>But what about your social contacts?</h1>
<h1>Get a Real Life.</h1>
<h1>That must consume heaps of time.</h1>
<h1>That's too complicated to me.</h1>
<h1>Who is supposed to read all this?</h1>
<h1>All bloggers do is whine and argue.</h1>
<h1>That's all too commercial, bloggers just sell themselves.</h1>
<h1>All bloggers are geeks and nerds.</h1>
<h1>What do we need that for anyway? There's always email, forums and chats.</h1>
<h1>You can't earn money that way.</h1>
<h1>That just sucks.</h1>
<h1>Why do you blog?</h1>
<p>I sympathize with his answers, but I guess that at least the last question didn't get answered fully...</p>
