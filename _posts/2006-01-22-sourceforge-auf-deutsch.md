---
layout: post
title: "SourceForge auf Deutsch"
microblog: false
guid: http://geewiz.micro.blog/2006/01/22/sourceforge-auf-deutsch.html
date: 2006-01-22T08:51:00+0200
type: post
url: /2006/01/22/sourceforge-auf-deutsch.html
---

<p>Nach meinem neuen Hobby <a href="http://developer.berlios.de/projects/comatose/">comatose</a> habe ich jetzt auch <a href="http://developer.berlios.de/projects/perl-c-s-auth/">CGI::Session::Auth</a> auf BerliOS &uuml;bertragen. Zun&auml;chst hatte ich vor, die Dienste von <a href="http://www.sourceforge.net">SourceForge</a> f&uuml;r die Projekte zu nutzen. Zum einen bietet SF aber nur CVS zur Versionsverwaltung an und zum anderen war der Name <a href="http://www.berlios.de.">comatose" bereits belegt. Beim Suchen stie&szlig; ich auf "BerliOS</a> Als die deutsche OpenSource-Plattform vor einigen Jahren gegr&uuml;ndet wurde, interessierte sie mich kaum. Inzwischen bietet sie aber einen echten Mehrwert gegen&uuml;ber SF und die Vorteile, die sie einem Entwickler (-Team) bietet, waren f&uuml;r mich sofort sp&uuml;rbar. Das hat mich dann auch motiviert, endlich mal wieder ein Release von CGI::Session::Auth zu ver&ouml;ffentlichen.</p>
<p><em>Update:</em> Nichts ist &auml;lter als die Nachrichten von gestern. Prompt gibt es bald auch ein <a href="http://blog.bauer-online.org/archives/82-SourceForge-mit-Subversion.html.">SVN bei SourceForge</a></p>
