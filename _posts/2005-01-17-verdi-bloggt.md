---
layout: post
title: "Ver.di bloggt"
microblog: false
guid: http://geewiz.micro.blog/2005/01/17/verdi-bloggt.html
date: 2005-01-17T22:32:00+0200
type: post
url: /2005/01/17/verdi-bloggt.html
---

<p>Im Forum der <a href="http://www.caj-freiburg.de">CAJ Freiburg</a> habe ich bereits auf das neue <a href="http://www.verdi-blog.de/arbeiterinnen/">ArbeiterInnen-Blog</a> von Ver.di hingewiesen.</p>
<p>Ich finde es interessant, wie sich die Gewerkschaft das Internet und insbesondere das Blog als neue Art der Kommunikation zu Nutze zu machen versucht.</p>
<p>Die Inhalte scheinen sich jedoch in der Hauptsache noch auf Brecht-Zitate zu beschr&auml;nken. Einer der wenigen neuen inhaltlichen Beitr&auml;ge enth&auml;lt leider mehr Rechtschreibfehler als sachliche Fakten.</p>
<p>Ob das wirklich die richtige Art ist, ein Blog einzuf&uuml;hren, wage ich zu bezweifeln. Vom Weblog einer Gewerkschaft erwarte ich -- wie von jedem anderen Blog -- regelm&auml;&szlig;ige Beitr&auml;ge, die es sich zu lesen lohnt, nicht nur Aufrufe zu Solidarit&auml;t und gewerkschaftlicher Organisation.</p>
