---
layout: post
title: "Weblog komplett"
microblog: false
guid: http://geewiz.micro.blog/2004/11/05/weblog-komplett.html
date: 2004-11-05T22:09:00+0200
type: post
url: /2004/11/05/weblog-komplett.html
---
<p>Wie unschwer zu erkennen ist, habe ich meine gesamte Website zu einem Weblog umfunktioniert. Die Entscheidung dazu folgte der Erkenntnis, dass das Weblog der Teil der alten Website war, den ich am intensivsten gepflegt habe.</p>
<p>Jetzt hat das Blog auch optisch und technisch den Stellenwert, der ihm zusteht.</p>
<p>Ich werde in den kommenden Tagen alle noch sinnvollen Inhalte, die auf der alten Website verf&uuml;gbar waren, in die neue Struktur &uuml;bertragen. Bei den Bildergalerien habe ich das gestern schon erledigt, meine Artikel und Vortr&auml;ge werden folgen.</p>
<p>Der Kommentar-Link soll signalisieren, dass ich f&uuml;r Feedback zu diesem Schritt -- wie auch zu allen neuen Weblog-Eintr&auml;gen -- jederzeit offen und dankbar bin.</p>
