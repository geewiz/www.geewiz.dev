---
layout: post
title: "Klare Worte"
microblog: false
guid: http://geewiz.micro.blog/2006/01/23/klare-worte.html
date: 2006-01-24T00:15:15+0200
type: post
url: /2006/01/23/klare-worte.html
---

<p>Es ist ein gutes Gef&uuml;hl, dass es Kollegen gibt, mit denen man Klartext reden kann. Egal, ob man mit ihnen ein Problem hat oder umgekehrt. Und bei denen man weiss, dass man selbst nach unangenehmen Gespr&auml;chen das Thema mit einer Vereinbarung abhaken darf, anstatt dass einem das Thema noch ewig nachschleicht.</p>
