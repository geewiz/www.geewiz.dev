---
layout: post
title: "Internet, my much better radio"
microblog: false
guid: http://geewiz.micro.blog/2012/02/05/internet-my-much.html
date: 2012-02-05T22:38:27+0200
type: post
url: /2012/02/05/internet-my-much.html
---
<p>I&#8217;m not very sophisticated in regards to music. In my early years, I relied solely on my favourite radio station to provide me with enjoyable songs, so my taste developed quite mainstreamish.</p>
<p>Thanks to the Internet, it&#8217;s getting better. Spotify is very useful to quickly check recommendations of interesting artists and bands I find in magazines or online.</p>
<p>What I find very interesting is how YouTube plays an increasingly important role in my musical education. Recently, I&#8217;ve came across very creative works that I never would have heard on the radio.</p>
<p>A good example is two very different cover versions of &#8220;Somebody That I Used to Know&#8221;. I&#8217;ve actually never heard the original by Gotye, but I suspect that these two cover versions are both more creative and more entertaining.</p>
<p>The first version is by the awesome Walk off the Earth and features many people playing one instrument:</p>
<p><iframe width="560" height="315" src="http://www.youtube.com/embed/d9NF2edxy-M" frameborder="0" allowfullscreen></iframe></p>
<p>The second version has one person (Ingrid Michaelson) playing many instruments:</p>
<p><iframe width="560" height="315" src="http://www.youtube.com/embed/dUxLK1misbw" frameborder="0" allowfullscreen></iframe></p>
<p>But it doesn&#8217;t have to be cover versions. Here&#8217;s &#8220;In Your Arms&#8221; by Kina Grannis:</p>
<p><iframe width="560" height="315" src="http://www.youtube.com/embed/IOu0DuxFAT0" frameborder="0" allowfullscreen></iframe></p>
<p>The song doesn&#8217;t impress me so much by its musical qualities as by the effort that went into producing the video:</p>
<p><iframe width="560" height="315" src="http://www.youtube.com/embed/cIH4MJAC2Tg" frameborder="0" allowfullscreen></iframe></p>
<p>So, thank you, Internet, and keep the great music suggestions coming!</p>
