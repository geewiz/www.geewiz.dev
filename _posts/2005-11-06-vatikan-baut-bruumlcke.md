---
layout: post
title: "Vatikan baut Br&uuml;cke zur Wissenschaft"
microblog: false
guid: http://geewiz.micro.blog/2005/11/06/vatikan-baut-bruumlcke.html
date: 2005-11-06T04:59:40+0200
type: post
url: /2005/11/06/vatikan-baut-bruumlcke.html
---

<p>Nicht immer hatte die katholische Kirche einen guten Draht zur Naturwissenschaft -- der Fall Galilei ist ein augenf&auml;lliges Beispiel daf&uuml;r. Aber sie hat offensichtlich daraus gelernt: Kardinal Paul Poupard, Vorsitzender des P&auml;pstlichen Rates f&uuml;r Kultur, rief jetzt die Gl&auml;ubigen seiner Kirche dazu auf, die Stimme der Naturwissenschaft nicht zu ignorieren."Die dauerhafte Lektion des Falles Galileo", so Poupard, "dr&auml;ngt uns dazu, den Dialog der verschiedenen Disziplinen, insbesondere zwischen der Theologie und den Naturwissenschaften, am Leben zu erhalten, wenn wir verhindern wollen, dass sich solche Episoden in Zukunft wiederholen." Wenn Religion ihre Verbindung mit dem Menschenverstand kappe, falle sie dem Fundamentalismus zum Opfer.</p>
<p>Mit dem Blick auf ethische Fragen vergisst Kardinal Poupard umgekehrt nicht, die Naturwissenschaftler aufzurufen, ihrerseits auf die Religionen zu h&ouml;ren. Als mahnende Beispiele nennt er die Atombombe oder das Klonen von Menschen.</p>
<p>Als katholischer Informatiker bin ich &uuml;berzeugt, dass ein solcher Dialog beiden Seiten nur n&uuml;tzen kann.</p>
<p>(Quelle: <a href="http://www.wired.com/news/culture/0">Wired News</a>,1284,69485,00.html)</p>
