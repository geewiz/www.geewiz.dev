---
layout: post
title: "Benebelt und beleuchtet"
microblog: false
guid: http://geewiz.micro.blog/2004/11/16/benebelt-und-beleuchtet.html
date: 2004-11-16T08:01:00+0200
type: post
url: /2004/11/16/benebelt-und-beleuchtet.html
---

<p>Eine kurze Durchsage in eigener Sache:</p>
<p>Die Nebelschlussleuchte ist nur bei Nebel unter 50m Sicht einzuschalten, sonst nicht! 50m ist in der Regel der Abstand zwischen 2 Leitpfosten neben der Fahrbahn.</p>
<p>Weitere Hinweise dazu auf <a href="http://www.fahrtipps.de/frage/nebelschlussleuchte.php.">Fahrtipps.de</a></p>
