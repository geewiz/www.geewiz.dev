---
layout: post
title: "Themed Christmas Presents"
microblog: false
guid: http://geewiz.micro.blog/2010/12/25/themed-christmas-presents.html
date: 2010-12-25T17:10:35+0200
type: post
images:
- /assets/wp-content/2011/05/startup_pack-scaled-1000.jpg?w=225
- /assets/wp-content/2011/05/nerd_pack-scaled-1000.jpg?w=225
photos:
- /assets/wp-content/2011/05/startup_pack-scaled-1000.jpg?w=225
- /assets/wp-content/2011/05/nerd_pack-scaled-1000.jpg?w=225
url: /2010/12/25/themed-christmas-presents.html
---
<p>
    This year, my family chose theme packages as my Christmas presents:
<p /> My father invests into my startup business by feeding my thoughts.
<p /> And my brother seems to have noticed a certain trait of my personality...
<p /> I love my family. Thanks, Dad! Thanks, Tom!
<div class='p_embed p_image_embed'>
<a href="/assets/wp-content/2011/05/startup_pack-scaled-1000.jpg"><img alt="Startup_pack" height="667" src="/assets/wp-content/2011/05/startup_pack-scaled-1000.jpg?w=225" width="500" /></a><br />
<a href="/assets/wp-content/2011/05/nerd_pack-scaled-1000.jpg"><img alt="Nerd_pack" height="667" src="/assets/wp-content/2011/05/nerd_pack-scaled-1000.jpg?w=225" width="500" /></a></p>
<div class='p_see_full_gallery'><a href="http://www.jochen-lillich.de/themed-christmas-presents">See the full gallery on Posterous</a></div>
</div>
