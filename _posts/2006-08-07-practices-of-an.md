---
layout: post
title: "Practices of an Agile Developer"
microblog: false
guid: http://geewiz.micro.blog/2006/08/07/practices-of-an.html
date: 2006-08-07T05:08:48+0200
type: post
url: /2006/08/07/practices-of-an.html
---

<p><a href="http://perlcast.com">Perlcast</a> is a podcast that is obviously interesting mainly to Perl developers. But the newest episode, <a href="http://perlcast.com/2006/07/12/practices-of-an-agile-developer/">Practices of an Agile Developer</a>, doesn't actually focus on Perl. It's an interview with Andy Hunt of the <a href="http://pragmaticprogrammer.com/.">The Pragmatic Programmers</a></p>
<p>I recommend it to all developers regardless of the language they're using, because Andy has some interesting insight into software development in general.</p>
