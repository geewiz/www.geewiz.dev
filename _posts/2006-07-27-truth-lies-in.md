---
layout: post
title: "Truth lies in the eye of the beholder"
microblog: false
guid: http://geewiz.micro.blog/2006/07/27/truth-lies-in.html
date: 2006-07-27T23:18:23+0200
type: post
url: /2006/07/27/truth-lies-in.html
---

<p>In a <a href="http://washingtontimes.com/national/20060724-110410-8309r.htm">recent poll</a>, 50% of 1,020 Americans declared that they believe Iraq had weapons of mass destruction when the United States invaded the country in 2003. Last year, it was only 36% that answered this question positively.</p>
<blockquote class="posterous_short_quote">
<p>Pollsters deemed the increase both "substantial" and "surprising" in light of persistent press reports to the contrary in recent years.</p>
</blockquote>
<p>It seems that George W. Bushs spin doctors are worth their money. The other possibility is that the average IQ of U.S. citizens is dropping rapidly.</p>
