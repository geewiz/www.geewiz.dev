---
layout: post
title: "Linux auf dem ThinkPad R52"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/linux-auf-dem.html
date: 2007-09-24T22:05:11+0200
type: post
url: /2007/09/24/linux-auf-dem.html
---

<p>(This is a German article from my old homepage.)</p>
<p>SUSE Linux 10 (OpenSUSE) lie&szlig; sich problemlos auf dem IBM/Lenovo-Notebook ThinkPad R52 installieren.</p>
<p>Die Hardware wird vom Betriebssystem offensichtlich gut unterst&uuml;tzt. Mein Ger&auml;t enth&auml;lt im einzelnen:</p>
<ul>
<li>CPU: Intel Pentium M 1.7 GHz</li>
<li>RAM: 1,5GB</li>
<li>Display: 15&rdquo; XGA</li>
<li>Grafik: Intel Mobile 915GM/GMS/910GML</li>
<li>Gigabit Ethernet: Broadcom BCM5751M</li>
<li>WLAN: Intel PRO/Wireless 2200BG</li>
<li>BlueTooth</li>
<li>Intel ICH6 Sound</li>
<li>USB 2.0, Firewire (IEEE 1394)</li>
</ul>
<p>Die Grafikkarte wird von SUSE Linux 10.0 einwandfrei unterst&uuml;tzt. Unterwegs arbeite ich im Einschirmbetrieb mit XGA-Aufl&ouml;sung (1024x768), am Schreibtisch schlie&szlig;e ich ein externes TFT mit SXGA-Aufl&ouml;sung (1280x1024) zus&auml;tzlich an und arbeite im Zweischirmbetrieb (siehe unten).</p>
<p>Beide Netzwerkanbindungen arbeiten problemlos, sowohl f&uuml;r Ethernet als auch f&uuml;r WLAN mit 802.11g wurden sofort bei der Installation Treiber gefunden und konfiguriert.</p>
<p>BlueTooth funktioniert ebenfalls, man muss es nur zun&auml;chst mit der Tastenkombination Fn+F5 aktivieren.</p>
<p>Erwartungsgem&auml;&szlig; verursacht auch die Audiokonfiguration keinerlei Schmerzen.</p>
<h2>X.org im Dual-Head-Modus</h2>
<p>Mit SAX2 allein gelang mir die Konfiguration des Zweischirmbetriebs von internem Display und externem Monitor nicht. Aber durch etwas Handarbeit erhielt ich eine funktionierende Konfiguration. Ich habe sie als Textdatei an diesen Artikel angef&uuml;gt.</p>
