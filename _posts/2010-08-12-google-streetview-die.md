---
layout: post
title: "Google Streetview: Die Stunde der Hypokriten"
microblog: false
guid: http://geewiz.micro.blog/2010/08/12/google-streetview-die.html
date: 2010-08-12T19:01:12+0200
type: post
url: /2010/08/12/google-streetview-die.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p>Wenn ich als B&uuml;rger schon nicht verhindern kann&hellip;</p>
<ul>
<li> dass in meinem <a href="http://de.wikipedia.org/wiki/Biometrischer_Pass#Elektronischer_Reisepass_mit_biometrischen_Daten_.28ePass.29" title="Elektronischer Reisepass">neuen Reisepass</a> Fingerabdr&uuml;cke und per Funk lesbare RFID-Chips installiert werden</li>
<li>dass meine <a href="http://de.wikipedia.org/wiki/SWIFT-Abkommen" title="SWIFT-Abkommen">Kontobewegungen</a> &uuml;berwacht werden</li>
<li>dass mein <a href="http://nachrichten.t-online.de/innenminister-huber-fuer-rueckkehr-zur-vorratsdatenspeicherung/id_42394006/index" title="Innenminister Huber f&uuml;r R&uuml;ckkehr zur Vorratsdatenspeicherung" target="_blank">Kommunikationsverhalten registriert werden soll</a></li>
<li>dass ich in vielen St&auml;dten auf Schritt und Tritt von unz&auml;hligen &Uuml;berwachungskameras gefilmt werde</li>
<li>dass ich auf Flugh&auml;fen in Zukunft durch einen <a href="http://www.tagesspiegel.de/wirtschaft/hamburg-testet-koerperscanner-/1900490.html" title="Tagesspiegel: Hamburg testet K&ouml;rperscanner">Nacktscanner</a> spazieren muss</li>
<li> dass Mautbr&uuml;cken automatisch alle <a href="http://de.wikipedia.org/wiki/Automatische_Nummernschilderkennung#Entscheidung_des_Bundesverfassungsgerichts_am_11._M.C3.A4rz_2008" title="Automatische Nummernschilderkennung" target="_blank">Autokennzeichen erfassen</a></li>
</ul>
<p>&hellip;. dann will ich wenigstens einen riesen Aufriss machen, wenn jemand es wagt, meine Hausfassade zu fotografieren.</p>
</blockquote>
<div class="posterous_quote_citation">Erfrischend klare Worte von <a href="http://www.dennis-knake.de/2010/08/11/google-streetview-die-stunde-der-hypokriten/">dennis-knake.de</a></div></p>
</div>
