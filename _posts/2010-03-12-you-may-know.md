---
layout: post
title: "You may know it as \"Pantha Rei\""
microblog: false
guid: http://geewiz.micro.blog/2010/03/12/you-may-know.html
date: 2010-03-12T13:30:49+0200
type: post
url: /2010/03/12/you-may-know.html
---

<div class="posterous_bookmarklet_entry">
<div class="posterous_quote_citation">via <a href="http://www.kickstarter.com/projects/melissapierce/life-in-perpetual-beta">kickstarter.com</a></div>
<p>Technology and passion. Story of my life.</p>
</div>
