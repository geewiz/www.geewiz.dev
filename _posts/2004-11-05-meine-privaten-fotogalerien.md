---
layout: post
title: "Meine privaten Foto-Galerien"
microblog: false
guid: http://geewiz.micro.blog/2004/11/05/meine-privaten-fotogalerien.html
date: 2004-11-05T09:34:00+0200
type: post
url: /2004/11/05/meine-privaten-fotogalerien.html
---

<p>&Auml;ltere Galerien:</p>
<ul>
<li>
<a href="http://www.jochen-lillich.de/pics/irland-2002/index_0.html">Irland 2002</a> -- Fotos vom Irland-Urlaub, den Caro, mein Bruder Tom und ich im Sommer 2002 machten.</li>
<li>
<a href="http://www.jochen-lillich.de/pics/eminis-2002/index_0.html">Ehemaligentreffen</a> der Ministranten Philippsburg, August 2002</li>
<li>
<a href="http://www.jochen-lillich.de/pics/schneeschwein/index.html">Carolins Schneeschwein</a> -- geschaffen von meiner Freundin im Dezember 2001</li>
</ul>
