---
layout: post
title: "The ultimate mobile office"
microblog: false
guid: http://geewiz.micro.blog/2006/07/12/the-ultimate-mobile.html
date: 2006-07-12T07:53:51+0200
type: post
url: /2006/07/12/the-ultimate-mobile.html
---

<p>Mobility is something that's just expected from workers in this information age. Well, I'll volunteer if I can have this <a href="http://www.beckerautodesign.com/jetvan/">mobile office in a van</a>!</p>
