---
layout: post
title: "The Ireland of the land and the people"
microblog: false
guid: http://geewiz.micro.blog/2014/08/01/the-ireland-of.html
date: 2014-08-01T12:58:07+0200
type: post
url: /2014/08/01/the-ireland-of.html
---
<blockquote>
<p>"Having seen quite a bit of the country in the past three years, we agree with those who say you don&rsquo;t have to leave this island to know Ireland boasts one of the world&rsquo;s most stunning ensembles of landscapes. And all of it decorated in an eye-soothing colour that is the Irish green.&rdquo;</p>
<p>&rdquo;But what really makes you feel you will never leave Ireland is the Irish people. In the course of our three years here we had numerous visitors who took trips throughout the country. All returned with very fond memories of the Irish they had met. And they had met many!"</p>
</blockquote>
<p>It&#39;s nice to see two of the main reasons why I moved to Ireland shared by Eckhard L&uuml;bkemeier, departing German ambassador to Ireland. Read his full <a href="http://www.irishtimes.com/news/social-affairs/auf-wiedersehen-ireland-that-we-have-come-to-love-1.1879905">farewell message</a> in the Irish Times.</p>
