---
layout: post
title: "Meiner neuer Standard-Editor: jEdit"
microblog: false
guid: http://geewiz.micro.blog/2006/05/11/meiner-neuer-standardeditor.html
date: 2006-05-11T20:20:48+0200
type: post
url: /2006/05/11/meiner-neuer-standardeditor.html
---

<p>Gut 10 Jahre lang war <a href="http://www.gnu.org/software/emacs/">Emacs</a> mein Editor f&uuml;r alle F&auml;lle. Egal, ob ich an Textdateien, HTML-Seiten, XML-Dateien oder Perlscripts arbeitete, ich tat es mit Emacs. Weil Emacs so viele M&ouml;glichkeiten der Erweiterung bietet, benutzte ich ihn eine Zeit lang sogar f&uuml;r E-Mail und News. Nach einigen Wochen der Erprobung hat ihm <a href="http://www.jedit.org/">jEdit</a> jetzt den Rang abgelaufen.<br />
Ich hatte mehrere Beweggr&uuml;nde, jEdit auszuprobieren: zum einen wollte ich einfach mal sehen, wie gut ein in Java programmierter Editor funktioniert. Und ich wollte herausfinden, was die Gr&uuml;nde f&uuml;r seinen guten Ruf sind.</p>
<p>Ich fand schnell heraus, dass jEdit sehr gut funktioniert. Er ist schnell, &uuml;bersichtlich und komfortabel zu bedienen. Daraus leitet sich auch sein guter Ruf ab. Er enth&auml;lt alle Funktionen, die ein Editor haben muss, schon von Haus aus. Und was dabei noch fehlt, kann in Form von Plugins nachger&uuml;stet werden.</p>
<p>Wie Emacs ist auch jEdit deshalb sehr vielseitig. Allerdings erfordert seine Konfiguration und Erweiterung keine &Auml;nderungen an unverst&auml;ndlichen LISP-Dateien; alles funktioniert per Mausklick. Plugins k&ouml;nnen direkt aus dem Programm &uuml;ber die "Plugin Manager"-Funktion heruntergeladen und installiert werden und sind sofort einsatzbereit. Die Auswahl an Plugins ist gro&szlig;, sie deckt weite Bereiche von Java und Perl &uuml;ber LaTeX und PHP bis hin zu XML und Co. ab. Auch fortgeschrittene Funktionen wie das Parsen und Anwenden der DTDs von XML-Dateien sind in jEdit m&ouml;glich; das kannte ich bisher nur von Emacs' PSGML-Modul.</p>
<p>Alles in allem gef&auml;llt mir jEdit so gut, dass ich jetzt aus "alias e=emacs" ein "alias e=jedit" gemacht habe.</p>
