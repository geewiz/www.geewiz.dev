---
layout: post
title: "SCO gets what it deserves"
microblog: false
guid: http://geewiz.micro.blog/2006/06/29/sco-gets-what.html
date: 2006-06-30T01:39:06+0200
type: post
url: /2006/06/29/sco-gets-what.html
---

<p>Years ago, I made the effort of regularly blogging news about the allegations brought forward by SCO of Linux containing thousands of lines of stolen code. But after some time, I got tired of all those continuously repeated but at any time unsupported claims and their all the more plausible refutation by the open source community. It got just plain boring and ridiculous, and I stopped wasting my time writing about it.</p>
<p>Now, judge Brooke Wells issued an order confirming IBM's reproach about the lack of specificity in SCO's claims. GrokLaw has the <a href="http://www.groklaw.net/article.php?story=20060628203537917.">full text</a> It contains a quite evident analogy for SCO's way of reasoning:</p>
<blockquote class="posterous_medium_quote">
<p>Certainly if an individual was stopped and accused of shoplifting after walking out of Neiman Marcus they would expect to be eventually told what they allegedly stole. It would be absurd for an officer to tell the accused that "you know what you stole Im not telling." Or, to simply hand the accused individual a catalog of Neiman Marcus entire inventory and say "its in there somewhere, you figure it out."</p>
</blockquote>
<p>As an open source aficionado, I gleefully enjoy to see SCO's butt kicked in court, but I cringe thinking about the amounts of money and time this useless court actions are burning.</p>
