---
layout: post
title: "Google Notebook"
microblog: false
guid: http://geewiz.micro.blog/2006/05/16/google-notebook.html
date: 2006-05-17T00:18:00+0200
type: post
url: /2006/05/16/google-notebook.html
---

<p>Auf dem Hype um das <a href="http://www.orkut.com">Web 2.0" schwimmt Google momentan an vorderster Front mit. Es vergeht ja kaum eine Woche, in der das ehemalige Suchmaschinen-Unternehmen nicht einen neuen Dienst bereitstellt, ob nun selbst entwickelt wie "Orkut</a> oder einfach aufgekauft wie <a href="http://www.writely.com.">Writely</a></p>
<p>Inzwischen d&uuml;rfte ja durchgedrungen sein, dass ich ein Fan des neuen WWW bin, und ich nutze auch ein paar der Google-Dienste. Aber was Google heute mit <a href="http://www.google.com/notebook">Google Notebook</a> abliefert, haut mich nicht vom Hocker: eine Sammlung von Bookmarks, denen man jeweils eine Kategorie und eine Notiz beif&uuml;gen kann, habe ich schon lange als <a href="http://sitebar.lillich.info/sitebar.php">Sitebar</a> auf meinem eigenen Webserver. Und selbst das habe ich inzwischen weitgehend durch <a href="http://del.icio.us/Geewiz">del.icio.us</a> abgel&ouml;st, weil ich dort meine Links mit beliebig vielen Tags klassifizieren kann. Und genau das kann Google Notebook seltsamerweise nicht. Ausserdem ist del.icio.us l&auml;ngst etabliert und entspricht in seiner Schlichtheit sogar voll dem Prinzip, das Googles Suchseite seinerzeit so erfolgreich gemacht hat.</p>
<p>Bei Google mag man sich aus diesem Grund vielleicht sogar &auml;rgern, dass man Yahoo den Kauf von del.icio.us &uuml;berlie&szlig; -- ich dagegen bin ganz froh, dass ich meine Webanwendungen &uuml;ber verschiedene Hersteller diversifizieren kann. Denn Google Notebook scheint mir doch eher den Hunger des Unternehmens nach mehr Informationen &uuml;ber die Nutzer zu befriedigen als deren Organisationsbedarf.</p>
