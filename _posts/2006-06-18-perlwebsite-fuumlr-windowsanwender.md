---
layout: post
title: "Perl-Website f&uuml;r Windows-Anwender"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/perlwebsite-fuumlr-windowsanwender.html
date: 2006-06-18T08:16:35+0200
type: post
url: /2006/06/18/perlwebsite-fuumlr-windowsanwender.html
---

<p>Die Programmiersprache Perl hat ihre Wurzeln klar im Bereich der Unix-Betriebssysteme, aber heutzutage findet sie auch unter Windows sinnvolle Anwendung.</p>
<p>Mit <a href="http://win32.perl.org/">win32.perl.org</a> gibt es nun eine Anlaufstelle f&uuml;r alle Windows-Anwender, die auf die M&ouml;glichkeiten dieser m&auml;chtigen Scriptsprache zur&uuml;ckgreifen m&ouml;chten. Momentan ist die Website auf Basis eines Wiki aufgebaut, sie soll aber mit wachsender Informationsmenge um weitere Funktionen erg&auml;nzt werden.</p>
