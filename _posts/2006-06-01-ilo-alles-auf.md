---
layout: post
title: "[ILO] Alles auf einen Blick"
microblog: false
guid: http://geewiz.micro.blog/2006/06/01/ilo-alles-auf.html
date: 2006-06-01T18:39:53+0200
type: post
images:
- /uploads/netvibes.serendipityThumb.png
photos:
url: /2006/06/01/ilo-alles-auf.html
---

<p>Information bestimmt mein Leben, deshalb leb ich online. Mein Tag beginnt immer mit dem Sichten meiner E-Mail und vieler weiterer Informationsquellen. Erleichtert wird das durch <a href="http://www.netvibes.com">Netvibes</a>, eine Portalseite, deren Inhalte jeder individuell festlegen kann.</p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/netvibes.serendipityThumb.png" />
</div></p>
<p>Portalseiten dieser Art schie&szlig;en derzeit wie Pilze aus dem Boden, neben Netvibes ist auch <a href="http://www.pageflakes.com">Pageflakes</a> ein Vertreter dieser Art. Sie zeichnen sich dadurch aus, dass sie individuell gestaltet werden k&ouml;nnen und dank AJAX sehr komfortabel zu bedienen sind.</p>
<p>Mein Netvibes-Portal, das ich auch im Browser als Startseite eingetragen habe, ist auf mehrere thematische Seiten verteilt, die &uuml;ber Karteireiter ausgew&auml;hlt werden. Auf <a href="http://www.flickr.com">Technologie" und "Nachrichten" werden verschiedene Newsticker aufgelistet, die Netvibes per RSS-Feed bezieht, zum Beispiel Heise, Pro-Linux oder Tagesschau.de. "Unterhaltung" enth&auml;lt ebenfalls Feeds, dieses Mal mit unterhaltsamen Inhalten von "Flickr</a> und <a href="http://del.icio.us">Del.icio.us</a>, dazu Freizeit-Infos, z.B. aktuelle <a href="http://www.filmstarts.de.">Filmstarts</a></p>
<p>Am unterschiedlichsten sind die Inhalte der Hauptseite: sie zeigt die Liste meiner ungelesenen E-Mails, Blogeintr&auml;ge meiner Freundin, Flickr-Fotos mit dem Tag "Irland", die Wettervorhersage f&uuml;r Karlsruhe, aktuelle Kalendereintr&auml;ge und eBay-Auktionen, die mich interessieren.</p>
<p>Netvibes bietet noch weitaus mehr Inhalte an, die man als Kasten auf eine Seite drapieren kann, darunter eine To-Do-Liste, pers&ouml;nliche Dokumente auf <a href="http://www.writely.com">Writely</a> oder Einkaufstipps von Kelkoo.</p>
<p>Mit meinem pers&ouml;nlichen Web-Portal kann ich mit einem einzigen Seitenaufruf vielf&auml;ltigste Informationen aus dem gro&szlig;en weiten Web auf meinen Bildschirm versammeln. So habe ich alles Wichtige auf einen Blick und spare Zeit -- f&uuml;r das Leben offline.</p>
<p>Weitere Artikel zum Thema "Ich leb online":</p>
<ul>
<li><a href="http://blog.jochen-lillich.de/archives/371-Ich-leb-online.html">Ich leb online</a></li>
<li>
<a href="http://blog.jochen-lillich.de/archives/290-Backpack-it">Backpack it!</a>!.html</li>
<li><a href="http://blog.jochen-lillich.de/archives/358-Projektmanagement-mal-einfach-Basecamp.html">Projektmanagement mal einfach: Basecamp</a></li>
<li><a href="http://blog.jochen-lillich.de/archives/386-ILO-Geeks-gehen-mit-Backpack-einkaufen.html">Geeks gehen mit Backpack einkaufen</a></li>
</ul>
