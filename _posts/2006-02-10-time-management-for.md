---
layout: post
title: "Time Management for System Administrators"
microblog: false
guid: http://geewiz.micro.blog/2006/02/10/time-management-for.html
date: 2006-02-10T03:35:48+0200
type: post
url: /2006/02/10/time-management-for.html
---

<p>O'Reilly-B&uuml;cher gibt es nicht nur zu reinen IT-Themen: Zum Beispiel bietet der Verlag seit einigen Wochen das Buch <a href="http://www.amazon.de/exec/obidos/ASIN/0596007833/geewizweblog-21">Time Management for System Administrators</a> an. Es besch&auml;ftigt sich mit Methoden, wie Sysadmins ihren Job effektiver und effizienter erledigen k&ouml;nnen. Es geht dabei nicht nur auf die &uuml;blichen Zeitmanagement-Weisheiten ein, sondern ber&uuml;cksichtigt auch die allt&auml;glichen Situationen in dieser Berufsgruppe.</p>
<p>Perlcast.com hat ein <a href="http://perlcast.com/2006/02/09/interview-with-tom-limoncelli/">Interview mit Tom Limoncelli</a>, dem Autor des Buchs, als Podcast ver&ouml;ffentlicht. Beim Anh&ouml;ren war ich sehr &uuml;berrascht, wie sehr Toms Erkenntnisse und Verhaltensweisen den meinen gleichen.</p>
