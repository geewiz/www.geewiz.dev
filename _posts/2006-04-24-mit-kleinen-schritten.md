---
layout: post
title: "Mit kleinen Schritten"
microblog: false
guid: http://geewiz.micro.blog/2006/04/24/mit-kleinen-schritten.html
date: 2006-04-24T18:47:26+0200
type: post
url: /2006/04/24/mit-kleinen-schritten.html
---

<p>Langsam kehrt die Kondition zur&uuml;ck. Wie schon <a href="http://blog.jochen-lillich.de/archives/335-Running-Man.html">erw&auml;hnt</a> baue ich sie nach dem Plan von <a href="http://www.myjogging.de">MyJogging.de</a> wieder auf. Am Samstag war ich mit meiner Leistung unzufrieden und entschied mich, die Einheit zu wiederholen. Das habe ich heute getan und ich bin superzufrieden, denn ich konnte den Lauf ohne Hecheln durchziehen. Es macht wirklich Spa&szlig;, auf einmal sogar eine gewisse Entspanntheit beim Laufen festzustellen! Auf zur n&auml;chsten Stufe...</p>
