---
layout: post
title: "Fatblogging: 98"
microblog: false
guid: http://geewiz.micro.blog/2007/05/28/fatblogging.html
date: 2007-05-28T04:29:00+0200
type: post
url: /2007/05/28/fatblogging.html
---

<p>Hey, this fatblogging really works! I've lost 2 kilos, just by writing the first entry! This is great.</p>
<p>I surely don't want to compromise that success by eating the wrong stuff, so I started to collect information about nutrition. And I experienced a revelation: on my way home, I come past the supermarket we always shop at. When I saw its sign today, some well-known cravings for sweets set in. I hesitated a little, but then gave in and bent off in direction of the shop. Just when I reached its doors, though, I realized that it was my weaker self that was driving me there. And, thanks to a great little book I just read, I now know the name of my weaker self: <a href="http://www.guenter-antwortet.de/.">G&uuml;nter</a> So I gave G&uuml;nter the mental finger and turned around to get home for some fat-reduced yogurt with fresh strawberries.</p>
<p>My morning run wasn't a full success, though. I had to admit that I'm not in the shape yet to run uphill for minutes. And Freiburg just isn't quite as flat as Philippsburg. Must be tectonics or something. Or maybe just the Black Forest. I'll look for a less challenging route.</p>
