---
layout: post
title: "Helpdesk -- suffering from idiots for centuries"
microblog: false
guid: http://geewiz.micro.blog/2007/02/13/helpdesk-suffering-from.html
date: 2007-02-13T05:45:49+0200
type: post
url: /2007/02/13/helpdesk-suffering-from.html
---

<p>It's hilarious to read what helpdesk staff has to cope with on a day to day basis; the web is full of those stories.</p>
<p>But it's even more hilarious to imagine how this may have looked like in the dark ages. No, not when DOS 5.0 came out -- I mean in the original dark ages:</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=eRjVeRbhtRU])</p>
<p>(via <a href="http://www.basicthinking.de/blog/2007/02/12/helpdesk-das-aelteste-gewerbe-der-welt/">Basic Thinking</a>)</p>
