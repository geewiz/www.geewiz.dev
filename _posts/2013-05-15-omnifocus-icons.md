---
layout: post
title: "OmniFocus Icons"
microblog: false
guid: http://geewiz.micro.blog/2013/05/15/omnifocus-icons.html
date: 2013-05-15T11:53:17+0200
type: post
url: /2013/05/15/omnifocus-icons.html
---
<p>It's common knowledge that productivity means getting shit done, not fine-tuning your task management tools. Improving the usage experience from time to time may be necessary, though.</p>
<p>I've been using <a href="http://www.omnigroup.com/omnifocus">OmniFocus</a> for managing my tasks for years. In the company, we're now using <a href="http://www.asana.com">Asana</a> with great success, but for my personal stuff, I've kept using OmniFocus so far.</p>
<p>I've got a bunch of customised perspectives that show me certain aspects of my workload. The most important ones sit in the tool bar and I'm happy to have happened upon a collection of icon sets that give the OmniFocus tool bar a consistent design. You can find them over at <a href="http://simplicitybliss.com/2013/04/the-best-omnifocus-perspective-icons/">SimplicityBliss</a>.</p>
