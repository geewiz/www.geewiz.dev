---
layout: post
title: "43% of Germans don't enjoy holidays in Ireland?"
microblog: false
guid: http://geewiz.micro.blog/2006/06/27/of-germans-dont.html
date: 2006-06-27T05:01:17+0200
type: post
url: /2006/06/27/of-germans-dont.html
---

<p>In no. 8 of Bernie Goldbach's <a href="http://irish.typepad.com/irisheyes/2006/06/10_questions_on.html">10 Questions on Sunday</a>, he states that according to the Irish Tourist Industry Confederation almost half of the Germans visiting Ireland during their vacations are dissatisfied.</p>
<p>Unfortunately, I couldn't find a detailed substantiation why Germans are so dissatisfied with Ireland as a holiday country. I'd be very interested in details, because I don't understand that dissatisfaction at all.</p>
<p>I like both the rural and city Ireland -- I even think about moving there. The people are much more relaxed than here, the cities are bursting of culture and the landscapes are marvellous. Granted, the prices aren't low, but you can find both accomodation and food to decent prices if you care to shop a little.</p>
<p>BTW, being both enthusiastic about Ireland and interested in education technology is the reason why I subscribed to Bernie's blog.</p>
