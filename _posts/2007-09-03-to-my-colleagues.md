---
layout: post
title: "To my colleagues"
microblog: false
guid: http://geewiz.micro.blog/2007/09/03/to-my-colleagues.html
date: 2007-09-03T17:04:00+0200
type: post
url: /2007/09/03/to-my-colleagues.html
---

<p>Dear coworkers,</p>
<p>I'll need to go to the restroom soon, and certainly, we all want this visit to be a pleasant one. To make sure business will go as, let's say, smoothly as possible, there are some rules that I'd like to make you aware of: </p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=IzO1mCAVyMw])</p>
<p>So, please take those rules into consideration, so that we all can continue our work day relieved and relaxed.</p>
<p>Thank you.</p>
