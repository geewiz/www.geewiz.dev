---
layout: post
title: "Tubthumping"
microblog: false
guid: http://geewiz.micro.blog/2006/07/18/tubthumping.html
date: 2006-07-18T19:04:50+0200
type: post
images:
- /uploads/x4500.jpg
photos:
- /uploads/x4500.jpg
url: /2006/07/18/tubthumping.html
---

<p>These days, everyone and his dog are getting their own Web 2.0 service online. And finally, there's hardware to base your Poopr or Sneezr or whatchamacallit on: "Thumper", the harddisk-munching and O'Reilly-approved Web 2.0 server from Sun!</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/x4500.jpg" />
</div></p>
<p>Seriously, folks, what's so "Web 2.0" about this X4500, as its official name is? Sure, it's impressive, with its high performance storage capacity of up to 24TB in a single case. But "Web 2.0 server", that's cheesy unless it does social networking with other servers, tags my network cables automatically and offers an AJAX management web interface. :-)</p>
<p>But, since it has some interesting features, I'm trying to get my hands on one for some tests. And I'm sure that <a href="http://retro-park.de/blog/index.php?/categories/2-Bastard-Operators-from-Hell">Ralf</a> will publish a review afterwards.</p>
