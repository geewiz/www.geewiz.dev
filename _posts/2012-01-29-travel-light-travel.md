---
layout: post
title: "Travel light, travel relaxed"
microblog: false
guid: http://geewiz.micro.blog/2012/01/29/travel-light-travel.html
date: 2012-01-29T20:39:37+0200
type: post
url: /2012/01/29/travel-light-travel.html
---
<p>I spent this weekend in Amsterdam at the <a href="http://process.drupaldays.org">Drupal Process Meetup</a>, and it&rsquo;s been a great experience! I had many interesting talks with other Drupal business owners, learned a lot in the Openspace-format discussion rounds and enjoyed having dinner together in downtown Amsterdam. As I&rsquo;m writing this article, I&rsquo;m reclining in an Eames Lounge Chair and listening to relaxing music at the amazing <a href="http://www.citizenmamsterdamairport.com">CitizenM hotel</a> near Schiphol Airport.</p>
<p>Since I intend to do much more business trips this year, I put some thought into making my travel as stress-free as possible. I flew with EasyJet who let you print out your boarding pass in advance. When I arrived at the airport, I could go directly through security without having to check in (and pay for) baggage because I was able to fit everything I needed into my backpack. I had it on me all the time, so I didn&rsquo;t have to worry about my stuff getting stolen. After landing in Amsterdam, instead of having to wait at the baggage claim, I went directly to the hotel and arrived there completely relaxed.</p>
<p>Packing light means to consciously limit yourself to the things you&rsquo;ll actually need. That you need <em>fresh</em> clothes for every day of your journey doesn&rsquo;t mean you need <em>different</em> clothes for every day. I didn&rsquo;t have that realization until recently; before, when I prepared for a 5 day vacation, I packed 5 T-shirts. This time, I simply packed a tube of travel detergent. By washing my stuff, I can get by, for example, with three pairs of socks: One to wear today, one for tomorrow and one that&rsquo;s currently drying after doing the laundry. Especially outdoor and travel clothes will easily dry over night.</p>
<p>Choosing equipment that makes traveling light easy does of course also apply to tech stuff. With my iPhone, Macbook Air and Kindle, I carried around both my complete office and my book shelf, all adding up to less than 2kg.</p>
<p>These are two of many articles you can find on the Web about packing light:</p>
<ul>
<li>&ldquo;<a href="http://www.onebag.com/">Learning to Lighten your Load</a>&rdquo;</li>
<li>&ldquo;<a href="http://patrickrhone.com/2008/12/17/my-manifesto-travel-light/">My Manifesto: Travel Light</a>&rdquo;</li>
</ul>
<p>Try it out, you&rsquo;ll be amazed how stress-free traveling becomes when you minimize your baggage!</p>
