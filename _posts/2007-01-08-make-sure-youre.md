---
layout: post
title: "Make sure you're subscribed to the new feed"
microblog: false
guid: http://geewiz.micro.blog/2007/01/08/make-sure-youre.html
date: 2007-01-08T07:52:30+0200
type: post
url: /2007/01/08/make-sure-youre.html
---

<p>It's been months since I changed the RSS link on my blog to a FeedBurner feed because it has a lot of advantages both for me as an author and you, the readers of this blog. For example:</p>
<ul>
<li>I get better transparency what topics are the most interesting to you.</li>
<li>Regardless of what the source URL of my RSS will be, the FeedBurner URI will never change.</li>
<li>The FeedBurner feed offers some helpful links under each entry.</li>
<li>You also get additional information like my newest del.icio.us links.</li>
</ul>
<p>But there's still quite a number of people that get my entries via the old S9Y feed. If you're one of them:</p>
<h1>You must be a long-time reader. Thank you so much for sticking with me!</h1>
<h1>Please  re-subscribe via the <a href="http://feeds.feedburner.com/BlogOfGeewiz">FeedBurner feed</a>!</h1>
