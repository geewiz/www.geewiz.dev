---
layout: post
title: "A Simple Place to Live"
microblog: false
guid: http://geewiz.micro.blog/2014/11/25/a-simple-place.html
date: 2014-11-25T16:51:32+0200
type: post
url: /2014/11/25/a-simple-place.html
---
<blockquote>
<p>&#8220;I can be at home anywhere because feeling content and safe and loved has nothing to do with the stuff that surrounds me.&#8221;</p>
</blockquote>
<p>What Courtney Carver describes as &#8220;<a href="http://bemorewithless.com/heart/">A Simple Place to Live</a>&#8221; resonates quite a bit with me.</p>
