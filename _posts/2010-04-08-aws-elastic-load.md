---
layout: post
title: "AWS Elastic Load Balancing bietet jetzt auch \"sticky sessions\""
microblog: false
guid: http://geewiz.micro.blog/2010/04/08/aws-elastic-load.html
date: 2010-04-08T10:41:43+0200
type: post
images:
- http://d1nqddva888cns.cloudfront.net/elb_sticky.png
photos:
url: /2010/04/08/aws-elastic-load.html
---

<div class="posterous_bookmarklet_entry">
<blockquote><div>
<p>With the new sticky session feature, it is possible to instruct the load balancer to route repeated requests to the same EC2 instance whenever possible.</p>
<p style="text-align:center;"><img src="http://d1nqddva888cns.cloudfront.net/elb_sticky.png" style="border:0 none;" /></p>
<p>In this case, the instances can cache user data locally for better performance. A series of requests from the user will be routed to the same EC2 instance if possible. If the instance has been terminated or has failed a recent health check, the load balancer will route the request to another instance.</p>
</div>
</blockquote>
<div class="posterous_quote_citation">via <a href="http://aws.typepad.com/aws/2010/04/new-elastic-load-balancing-feature-sticky-sessions.html">aws.typepad.com</a></div>
<p>Die noch recht einfach gestrickte Loadbalancing-Funktion der Amazon Web Services bietet jetzt auch "sticky sessions", wodurch Anfragen des gleichen Benutzers auch immer auf die gleiche Instanz (sofern verf&uuml;gbar) geleitet werden.</p>
</div>
