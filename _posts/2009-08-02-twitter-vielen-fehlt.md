---
layout: post
title: "Twitter: \"Vielen fehlt die Medienkompetenz\""
microblog: false
guid: http://geewiz.micro.blog/2009/08/02/twitter-vielen-fehlt.html
date: 2009-08-02T15:25:49+0200
type: post
url: /2009/08/02/twitter-vielen-fehlt.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_short_quote"><p>Dennoch halten sich die deutschen Unternehmen zur&uuml;ck. "Vielen fehlt die Medienkompetenz f&uuml;r diesen neuen Kanal, deswegen ist eine gewisse Unsicherheit zu sp&uuml;ren", erkl&auml;rt Ulrike R&ouml;ttger, Professorin f&uuml;r Public Relations an der Universit&auml;t M&uuml;nster.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.manager-magazin.de/it/artikel/0,2828,639790,00.html">manager-magazin.de</a></div></p>
</div>
