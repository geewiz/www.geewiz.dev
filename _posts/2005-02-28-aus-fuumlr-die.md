---
layout: post
title: "Aus f&uuml;r die Smarties-Rolle"
microblog: false
guid: http://geewiz.micro.blog/2005/02/28/aus-fuumlr-die.html
date: 2005-03-01T01:33:00+0200
type: post
url: /2005/02/28/aus-fuumlr-die.html
---

<p>An der Entwicklung an der S&uuml;&szlig;waren-Front kann man das eigene Alter ablesen: Dass "Twix" fr&uuml;her "Raider" hie&szlig;, ist ja hinl&auml;nglich bekannt. Aber wer weiss noch, welche Farbe die "Snickers"-Verpackung fr&uuml;her hatte? Wer erinnert sich an den Geschmack von "Banjo"?</p>
<p>Jetzt kommt erneut eine einschneidende Ver&auml;nderung: wie der <a href="http://www.shopblogger.de/blog/archives/387-Smarties-werden-sechseckig.html">Shopblogger</a> mitteilt, hat sich Nestl&eacute; entschieden, die Smarties-Rolle sechseckig zu machen.</p>
<p>Es ist mir ein komplettes R&auml;tsel, warum man den &uuml;ber Jahrzehnte gepr&auml;gten Wiedererkennungswert der Packung aufgibt. </p>
