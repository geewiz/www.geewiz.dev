---
layout: post
title: "Quick git actions with tig"
microblog: false
guid: http://geewiz.micro.blog/2013/05/10/quick-git-actions.html
date: 2013-05-11T00:52:33+0200
type: post
url: /2013/05/10/quick-git-actions.html
---
<p>For the most common git actions like commits and branches, nothing beats the speed of the command line.  But when it gets more complex, a user interface could help make things more clear and easy to manage. There are many GUIs available for git but unfortunately, they tend to get convoluted at times, which defeats their purpose.</p>
<p>Now, there's a console UI that claims to be "the mutt to your Outlook, the Vim to your Emacs, the w3m to your Firefox". Its name fits its philosopy: "tig". Quick, simple, easy to remember.</p>
<p>Read more about it on the <a href="http://blogs.atlassian.com/2013/05/git-tig/">Atlassian blog</a>.</p>
