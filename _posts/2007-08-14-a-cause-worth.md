---
layout: post
title: "A cause worth fighting for"
microblog: false
guid: http://geewiz.micro.blog/2007/08/14/a-cause-worth.html
date: 2007-08-15T00:58:02+0200
type: post
url: /2007/08/14/a-cause-worth.html
---

<p>I've been in love with computer animation since my VIC-20 in 1984. "A Gentlemen's Duel" is a bit more sophisticated than the animations of that time, and a lot funnier:</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=yIPGXc7EjF4])</p>
