---
layout: post
title: "Montag ist Messetag"
microblog: false
guid: http://geewiz.micro.blog/2006/03/11/montag-ist-messetag.html
date: 2006-03-11T18:10:00+0200
type: post
url: /2006/03/11/montag-ist-messetag.html
---

<p>Am Montag werde ich mit Kollege Lehr die CeBit unsicher machen. W&auml;hrend ich fr&uuml;her jedes Jahr dort war, hatte ich irgendwann keine Lust mehr, mir das Gew&uuml;hle anzutun, nur um festzustellen, dass wirklich alles in der IT gr&ouml;&szlig;er, schneller, bunter und lauter ist als im letzten Jahr. Jetzt wirds mal wieder Zeit f&uuml;r ein neues Messeerlebnis.</p>
<p>Wer am Montag auch auf der Messe unterwegs ist, kann mir gern ne <a href="mailto:jochen@lillich.info">Mail schreiben</a>! Vielleicht l&auml;sst sich ja ein Treffen arrangieren.</p>
<p><em>Update:</em> Irgendwelche Tipps, was man unbedingt sehen muss?</p>
