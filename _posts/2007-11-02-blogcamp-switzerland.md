---
layout: post
title: "BlogCamp Switzerland"
microblog: false
guid: http://geewiz.micro.blog/2007/11/02/blogcamp-switzerland.html
date: 2007-11-02T08:01:54+0200
type: post
url: /2007/11/02/blogcamp-switzerland.html
---

<p>It's now been almost a week since I attended BlogCamp Switzerland at the ETH Z&uuml;rich. I just didn't have the time to write about it yet.</p>
<p>We -- that is <a href="http://www.blog.cbgreenwood.de/">Sebastian</a>, <a href="http://blog.himmelskratzer.de/">Diana</a> and <a href="http://www.pixeljunkie.de/">Sven</a> -- met early in the morning to have Sebastian drive us across the Swiss border to Z&uuml;rich. We managed to arrive just in time, so we registered and immediately joined about a hundred other participants in one of the lecture halls. After a warm welcome by Peter Hogenkamp, we had a short introduction round where everyone stated their name and three personal tags. I was one of the speakers that then explained their topic, each followed by a show of hands to determine the audience's interest.</p>
<p>But first, I went to Adrian Heydecker's talk about "<a href="http://www.slideshare.net/Hey/blog-usability-bloggen-fr-leser-ausserhalb-der-blogosphre/">Blog Usability</a>". He not only gave some good advice about blog layouts that help building an audience, he also coped quite well with two projector outages (one caused by me by accidentally unplugging the power cord when I returned from the restroom, the other one from a lamp failure).</p>
<p>Several people had supported my topic "Getting Blogs Done" and I got one of the slots at 11:15. In about half an hour, I gave a short introduction into David Allen's "Getting Things Done" concept and then explained how I used it to organize my blogging more productively. We had an interesting Q&amp;A session afterwards, and there are comment threads in <a href="http://document-dot-write.blogspot.com/2007/10/blogcampswitzerland-getting-blogs-done.html">Markurs Tressl's blog</a> and my <a href="http://www.selbstadministration.de/2007/10/20/getting-blogs-done/">Selbstadministration blog</a>.</p>
<p>With "<a href="http://www.krusenstern.ch/blogbuch/10-tipps-fuer-bessere-weblog-texte.html">10 Tipps f&uuml;r bessere Blogtexte</a>" ("10 tips for a better blog copy"), J&uuml;rg Vollmer shared interesting insights about what to look out for when writing blog posts.</p>
<p>The last talk I attended was "Gro&szlig;e Blogprojekte" ("Big blog projects") by <a href="http://www.theofel.de/archives/2007/10/blogcampswitzerland-20-rueckblick-und-ausblick.html">Jan Theofel</a>. He demonstrated some of his successful blog projects that generated a impressive number of hits per day, and explained what he took care of to get and keep his audience.</p>
<p>All in all, it was a day well spent. We were amazed how well organized everything was and enjoyed the conversations with other swiss and german bloggers. Next time in 2008, BlogCamp Switzerland will be an event during the "Computer Science Days". I'll be there if my time allows.</p>
