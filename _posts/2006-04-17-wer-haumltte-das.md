---
layout: post
title: "Wer h&auml;tte das gedacht?"
microblog: false
guid: http://geewiz.micro.blog/2006/04/17/wer-haumltte-das.html
date: 2006-04-17T03:55:41+0200
type: post
url: /2006/04/17/wer-haumltte-das.html
---

<p><a href="http://www.heise.de/newsticker/meldung/72039"><em>IT-Arbeit ist ungesund</em></a>, stellt eine Studie des Instituts f&uuml;r Arbeit und Technik Gelsenkirchen fest. Die untersuchten IT-Mitarbeiter litten viermal mehr unter psychosomatischen Beschwerden als der Durchschnitt.</p>
<p>Dass Dauerstress zum Burnout-Syndrom f&uuml;hrt, kann doch nicht ernsthaft jemanden &uuml;berrascht haben!</p>
<p>Als konkrete Handlungsempfehlung nehme ich aber den Hinweis auf die Hauptursache dieses Stresses mit: "widerspr&uuml;chliche Arbeitsanforderungen, &uuml;berlange Arbeitszeiten und Leistungsdruck" sind Belastungen, die ich meinen Mitarbeitern weitgehend ersparen kann.</p>
