---
layout: post
title: "Blender 2.37 f&uuml;r IRIX"
microblog: false
guid: http://geewiz.micro.blog/2005/06/01/blender-fuumlr-irix.html
date: 2005-06-01T20:50:45+0200
type: post
url: /2005/06/01/blender-fuumlr-irix.html
---

<p>Mit Bugfixes und zus&auml;tzlichen Funktionen wie Soft Bodies, Subdivision Surfaces und neuen Transformationswerkzeugen steht jetzt <a href="http://www.blender3d.org/cms/Blender_2_37.496.0.html">Version 2.37</a> der 3D-Animations-Software Blender zur Verf&uuml;gung.</p>
<p>(via. <a href="http://www.nekochan.net/weblog/archives/000248.html">Nekochan.net</a>)</p>
