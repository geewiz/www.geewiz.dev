---
layout: post
title: "SW-Patent des Monats: Kontextmen&uuml;s"
microblog: false
guid: http://geewiz.micro.blog/2006/04/03/swpatent-des-monats.html
date: 2006-04-03T20:23:27+0200
type: post
url: /2006/04/03/swpatent-des-monats.html
---

<p>Dass Philips einen Schutzanspruch auf die Darstellung eines Kontextmen&uuml;s erhalten konnte, brachte dem Unternehmen das <a href="http://www.nosoftwarepatents-award.de/">Softwarepatent des Monats" ein und nominiert es f&uuml;r den "No Software Patents Award</a>, den 1&amp;1 zusammen mit GMX und Partnern ins Leben riefen und der im Herbst zum ersten Mal verliehen werden soll.</p>
<p>Ziel der Preisverleihung ist es, auf die absurden Folgen und Gefahren der Softwarepatent-Praxis hinzuweisen.</p>
