---
layout: post
title: "You're a senior engineer. Now what?"
microblog: false
guid: http://geewiz.micro.blog/2013/06/20/youre-a-senior.html
date: 2013-06-20T12:07:38+0200
type: post
url: /2013/06/20/youre-a-senior.html
---
<p>It's a widespread practice to give sysadmins and developers that have accrued a few years of experience a new prefix to their job title: "senior". So they suddenly become Senior System Administrator, Senior Ruby Developer and so on.</p>
<p>So, senior something. What does that even mean?</p>
<p>The very senior John Allspaw shared his thoughts on this topic a few months ago on his blog under the title "<a href="http://www.kitchensoap.com/2012/10/25/on-being-a-senior-engineer/">On Being A Senior Engineer</a>".</p>
<p>John cites Theo Schlossnagle who asked what might come next:</p>
<blockquote>
<p>After five more years will you not have accrued more invaluable experience? What then? &ldquo;Super engineer&rdquo;? Five more years? &ldquo;Super-duper engineer.&rdquo;</p>
</blockquote>
<p>When you get promoted to a "senior", you haven't actually become someone else over night. It's not an event of metamorphosis, neither of enlightenment. John chose to find another adjective that has more meaning:</p>
<blockquote>
<p>I expect a &ldquo;senior&rdquo; engineer to be a mature engineer.</p>
</blockquote>
<p>And he lists these main characteristics of a mature engineer:</p>
<ul>
<li>Mature engineers seek out constructive criticism of their designs.</li>
<li>Mature engineers understand the non-technical areas of how they are perceived.</li>
<li>Mature engineers do not shy away from making estimates, and are always trying to get better at it.</li>
<li>Mature engineers have an innate sense of anticipation, even if they don&rsquo;t know they do.</li>
<li>Mature engineers understand that not all of their projects are filled with rockstar-on-stage work.</li>
<li>Mature engineers lift the skills and expertise of those around them.</li>
<li>Mature engineers make their trade-offs explicit when making judgements and decisions.</li>
<li>Mature engineers don&rsquo;t practice CYAE (&ldquo;Cover Your Ass Engineering&rdquo;)</li>
<li>Mature engineers are empathetic.</li>
<li>They don&rsquo;t make empty complaints.</li>
<li>Mature engineers are aware of cognitive biases.</li>
</ul>
<p>He explains each characteristic in depth, so don't miss reading <a href="http://www.kitchensoap.com/2012/10/25/on-being-a-senior-engineer/">his post</a>!</p>
<p>So, can you tick all the boxes of the mature engineer for the seniors in your team, for yourself? Yes? Then there's one last aspect.</p>
<p>After listing "The Ten Commandments of Egoless Programming", John adds another essential requirement that I was missing in quite a few senior sysadmins I have been working with over time:</p>
<blockquote>
<p>Dirty secret: mature engineers know the importance of (sometimes irrational) feelings people have. (gasp!)</p>
</blockquote>
<p>When you'd like to become a senior, or better, a mature engineer, then first and foremost become a mature person.</p>
