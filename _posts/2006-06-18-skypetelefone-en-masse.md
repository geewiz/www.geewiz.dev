---
layout: post
title: "Skype-Telefone en masse"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/skypetelefone-en-masse.html
date: 2006-06-18T08:09:45+0200
type: post
url: /2006/06/18/skypetelefone-en-masse.html
---

<p>Man soll den Markt nicht schon bei den Ank&uuml;ndigungen loben, aber es ist schon auff&auml;llig, wie viele Skype-kompatible VOIP-Telefone derzeit vorgestellt werden. Zum Reigen sto&szlig;en jetzt dazu:</p>
<ul>
<li><a href="http://gizmodo.com/gadgets/wireless/bcm-wlan800-wifi-skype-phone-180151.php">BCM WLAN800</a></li>
<li><a href="http://www.engadget.com/2006/06/16/yet-another-skype-phone-airwises-skyair/">Airwise SkyAir</a></li>
</ul>
<p>Ich glaube, wenn letztlich nur die H&auml;lfte der angek&uuml;ndigten Ger&auml;te hierzulande zu vern&uuml;nftigem Preis erh&auml;ltlich und von brauchbarer Qualit&auml;t sein wird, bleibt immer noch genug Auswahl.</p>
