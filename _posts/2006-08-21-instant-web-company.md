---
layout: post
title: "Instant Web 2.0 Company"
microblog: false
guid: http://geewiz.micro.blog/2006/08/21/instant-web-company.html
date: 2006-08-21T21:10:24+0200
type: post
url: /2006/08/21/instant-web-company.html
---

<p>Although it's still in beta, the <a href="http://emptybottle.org/bullshit/">Web 2.0 bullshit generator</a> works quite well already. All I have to do now before founding my next company is deciding whether it'll "harness citizen-media wikis" or "reinvent long-tail life-hacks". It will certainly "incentivize semantic networking", of course.</p>
<p>Next steps: <a href="http://andrewwooldridge.com/myapps/webtwopointoh.html">Generate the company name</a> and <a href="http://msig.info/web2test.php.">make a nice logo</a> Et voil&agrave;, another business to sell to Yahoo!</p>
