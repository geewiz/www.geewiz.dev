---
layout: post
title: "my $child1 = fork()"
microblog: false
guid: http://geewiz.micro.blog/2007/12/04/my-child-fork.html
date: 2007-12-04T18:01:18+0200
type: post
url: /2007/12/04/my-child-fork.html
---

<p>I've got as little spare time these days as I've always had over the last years. If you know me, you know that there always are many things I'm doing and thinking about.</p>
<p>A few weeks ago, something new appeared that's going around in my mind and hopefully will make sure I won't run out of occupation in the years to come.</p>
<p>Carolin and I are very happy that our tries finally were successful and that everything is going well. We're looking forward to a time of joy and desperation! ;-)</p>
