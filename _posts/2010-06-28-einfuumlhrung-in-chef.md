---
layout: post
title: "Einf&uuml;hrung in Chef"
microblog: false
guid: http://geewiz.micro.blog/2010/06/28/einfuumlhrung-in-chef.html
date: 2010-06-28T08:41:00+0200
type: post
url: /2010/06/28/einfuumlhrung-in-chef.html
---

<p><a href="http://www.opscode.com/chef">Opscode Chef</a> ist eine Open Source Software, die &auml;hnlich wie Puppet dazu dient, die Einrichtung und Pflege von Servern zu automatisieren. Statt in 15 Shell-Fenstern steuert ein Systemadministrator seine Systeme von einer zentralen Chef-Instanz aus. Das folgende Video gibt einen ersten Einblick in Sinn und Zweck dieser Systemintegrations-Software:</p>
<p>[blip.tv [blip.tv/play/hMAg...](http://blip.tv/play/hMAggebAawI)]</p>
