---
layout: post
title: "New website on Drupal"
microblog: false
guid: http://geewiz.micro.blog/2007/10/22/new-website-on.html
date: 2007-10-22T19:03:29+0200
type: post
url: /2007/10/22/new-website-on.html
---

<p>If you can read this, I managed to get my new homepage online. Instead of the combination of Textpattern and Serendipity, all runs on Drupal now.</p>
<p>Drupal ist great to build bigger websites that not only have a blog part, but also things like photo galleries etc. I already built some customer websites on Drupal as well as the <a href="http://freistil-consulting.de">Freistil-Consulting</a> website. Now, my personal page follows.</p>
<p>I didn't want to make the effort of importing and converting the <a href="http://blog.jochen-lillich.de">old blog</a>'s entries, so it'll stay online on its own subdomain.</p>
<p>I hope you enjoy the new and improved <a href="http://www.jochen-lillich.de">www.jochen-lillich.de</a> as much as I do!</p>
