---
layout: post
title: "Ad astra, Clark."
microblog: false
guid: http://geewiz.micro.blog/2005/01/18/ad-astra-clark.html
date: 2005-01-18T09:48:00+0200
type: post
url: /2005/01/18/ad-astra-clark.html
---

<p>Im Alter von 85 Jahren verstarb am 15.01.2005 Walter Ernsting, einer der Gr&uuml;ndungsv&auml;ter der deutschen Science Fiction-Literatur.</p>
<p>Der Autor, der unter seinem Pseudonym "Clark Dalton" bekannt wurde, tr&auml;umte schon in der sowjetischen Kriegsgefangenschaft von der Reise zu den Sternen.</p>
<p>1961 begr&uuml;ndete er als einer von 2 Chefautoren die Romanserie "Perry Rhodan", die noch heute Woche f&uuml;r Woche weiter geschrieben wird. Seine Figur "Gucky", deren Aussehen an eine &uuml;bergro&szlig;e Kreuzung aus Maus und Biber erinnert, ist einer der beliebtesten Ausserirdischen der SF-Literatur.</p>
<p>Wir Leser verdanken Clark viele Stunden spannender und unterhaltsamer Lekt&uuml;re von Geschichten &uuml;ber ein Leben in Gemeinschaft -- &uuml;ber die Grenzen des eigenen Planeten hinaus.</p>
<p>Link: <a href="http://www.perry-rhodan.net/aktuell/news/2005011701.html">PerryRhodan.net</a></p>
