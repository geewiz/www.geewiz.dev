---
layout: post
title: "The Myth of Transparent Clustering"
microblog: false
guid: http://geewiz.micro.blog/2006/07/06/the-myth-of.html
date: 2006-07-06T23:49:17+0200
type: post
url: /2006/07/06/the-myth-of.html
---

<p>Manik Surtani, Lead of the JBoss Cache team, advises against just wrapping a cluster environment around an application. According to his reasoning in the <a href="http://labs.jboss.com/portal/index.html?ctrl:cmd=render">JBoss Labs Blog</a>&amp;ctrl:window=default.blog.PrjBlogPortletWindowDefaultBlog&amp;project=all&amp;from=1&amp;link=The_Myth_of_Transparent_Clustering#The_Myth_of_Transparent_Clustering, performing in a cluster is something that has to be considered already when building the application.</p>
<blockquote class="posterous_medium_quote">
<p>As much as people would like to think that with modern techniques like AOP, bytecode injection, annotations, along with a healthy dose of ignorance of reality, wishing upon a star and belief in the tooth fairy, clustering can be a truly decoupled aspect that can be applied to anything, they are wrong.</p>
</blockquote>
