---
layout: post
title: "Novell switches to Geronimo"
microblog: false
guid: http://geewiz.micro.blog/2006/07/19/novell-switches-to.html
date: 2006-07-19T22:04:52+0200
type: post
url: /2006/07/19/novell-switches-to.html
---

<p>Starting with SUSE Linux Enterprise Server 10, Novell delivers <a href="http://geronimo.apache.org">Apache Geronimo</a> as J2EE Application Server. <a href="http://www.jboss.com">JBoss</a> got dropped from the distribution, but Novell committed themselves to continue support for JBoss.</p>
<p>Allegedly, license issues caused Novell to switch application servers, but I don't get that argument since JBoss as well as its companion products are LGPL licensed. I can't imagine a problem arising from that.</p>
<p>I think it's more of a competition thing with Red Hat, but that's okay. Competition is good for customers.</p>
<p><em>Update, 2006-07-20:</em> According to <a href="http://www.businessreviewonline.com/os/archives/2006/07/novell_explains.html">Computer Business Review</a>, it's not about the application server's licensing but about that of third party components. Novell's director of marketing for Linux and open platform solutions, Justin Steinman, explained:</p>
<blockquote class="posterous_medium_quote">
<p>As part of the SUSE Linux Enterprise development process, we evaluate the licensing for each package that we ship in our distribution. While packages like JBoss are distributed under the LGPL license, there are many components which are proprietary technologies from third parties," he stated.<br />
  "We found several components in JBoss4 that fit this profile. We checked with JBoss and the two areas in question, the SRP security extensions and the application client, ship under licenses that have questionable redistribution rights for Novell," he added.</p>
</blockquote>
