---
layout: post
title: "Umstellung auf neue Blogsoftware"
microblog: false
guid: http://geewiz.micro.blog/2005/03/02/umstellung-auf-neue.html
date: 2005-03-02T20:26:00+0200
type: post
url: /2005/03/02/umstellung-auf-neue.html
---

<p>So langsam kommt mein Weblog hier wirklich ins Rollen, und eure Kommentare zeigen, dass ich nicht nur mit der Raufasertapete rede. Das ist super.</p>
<p>Auf der anderen Seite merke ich, dass mir meine jetzige Blogsoftware zu wenig Funktionen und Komfort bietet. Daher werde ich in den kommenden Tagen die Software wechseln. Leider gibt es kein vergleichbares Perl-basiertes Blog, also werde ich Serendipity 0.8 einsetzen.</p>
<p>Die bestehenden Eintr&auml;ge werde ich manuell &uuml;bertragen, leider wird mir das f&uuml;r eure bisherigen Kommentare wahrscheinlich nicht m&ouml;glich sein.</p>
<p>Durch die Umstellung werden sich bestimmt auch die URLs der Eintr&auml;ge und RSS-Feeds &auml;ndern. Wundert euch also nicht, wenn der RSS-Reader  erst mal nichts mehr anzeigt.</p>
