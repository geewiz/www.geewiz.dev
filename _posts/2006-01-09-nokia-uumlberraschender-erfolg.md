---
layout: post
title: "Nokia 770: &uuml;berraschender Erfolg"
microblog: false
guid: http://geewiz.micro.blog/2006/01/09/nokia-uumlberraschender-erfolg.html
date: 2006-01-09T08:07:00+0200
type: post
url: /2006/01/09/nokia-uumlberraschender-erfolg.html
---

<p><a href="http://www.theregister.co.uk/2006/01/05/electric_nokia770/">The Register</a> berichtet, dass sich das Internet-Tablett Nokia 770 gro&szlig;en Erfolges erfreut. Dass ausgerechnet ein Ger&auml;t ohne Telefonfunktion[1] derzeit so gefragt ist, hat auch den finnischen Hersteller &uuml;berrascht, sodass sich K&auml;ufer auf leere Lager und mehrw&ouml;chige Wartezeiten einstellen m&uuml;ssen. Da mein Geburtstag schon im Februar ist, bitte ich das zu ber&uuml;cksichtigen. ;)</p>
<p>fn1. Daf&uuml;r verf&uuml;gt es &uuml;ber Linux-Basissoftware, ein hochaufl&ouml;sendes Display und mobile Daten&uuml;bertragung per WLAN und BlueTooth.</p>
