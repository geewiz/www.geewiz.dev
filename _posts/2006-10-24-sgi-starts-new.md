---
layout: post
title: "SGI starts new, into mainstream"
microblog: false
guid: http://geewiz.micro.blog/2006/10/24/sgi-starts-new.html
date: 2006-10-24T06:37:03+0200
type: post
url: /2006/10/24/sgi-starts-new.html
---

<p>After filing for bankruptcy earlier this year, SGI reorganized and laid off 250 people. Now they're out of Chapter 11 and start with a new management and new money.</p>
<p>The times of big honkin' graphics workstations with MIPS CPUs and IRIX operating system are definitely over, though. Their new computer systems use Intel Xeon and Itanium CPUs and Linux as operating system.</p>
<p>In other words: Nothing to see here, please move along.</p>
