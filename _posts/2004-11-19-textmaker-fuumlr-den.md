---
layout: post
title: "Textmaker f&uuml;r den Zaurus"
microblog: false
guid: http://geewiz.micro.blog/2004/11/19/textmaker-fuumlr-den.html
date: 2004-11-19T02:20:00+0200
type: post
url: /2004/11/19/textmaker-fuumlr-den.html
---

<p>Mit <a href="http://www.softmaker.de/tm.htm">TextMaker</a> gibt es zum Preis von knapp 50 Euro eine neue Textverarbeitungssoftware f&uuml;r die Linux-basierten Zaurus-PDAs von Sharp.</p>
<p>Das Programm kann Word-Dokumente verarbeiten und verf&uuml;gt unter anderem &uuml;ber Funktionen wie mehrsprachige Rechtschreibpr&uuml;fung, Grafikeinbindung, Fu&szlig;noten, Endnoten, Kopfzeilen, Tabellen und Formularbearbeitung.</p>
<p>Ich hab meinen Zaurus 5000 zwar seinerzeit gegen einen Planer aus Papier eingetauscht, aber ich freue mich, dass die Linux-PDAs noch immer Unterst&uuml;tzung finden.</p>
<p>(via <a href="http://www.heise.de/newsticker/meldung/53388">Heise</a>)</p>
