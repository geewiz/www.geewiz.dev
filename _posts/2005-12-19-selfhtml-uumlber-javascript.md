---
layout: post
title: "SELFHTML &uuml;ber Javascript"
microblog: false
guid: http://geewiz.micro.blog/2005/12/19/selfhtml-uumlber-javascript.html
date: 2005-12-19T19:04:01+0200
type: post
url: /2005/12/19/selfhtml-uumlber-javascript.html
---

<p>Das SELFHTML-Blog hat einen sehr interessanten Artikel &uuml;ber JavaScript ver&ouml;ffentlicht. <a href="http://aktuell.de.selfhtml.org/weblog/javascript-einsatz">Der sinnvolle Einsatz von JavaScript</a> wirft einen kritischen Blick auf die Scriptsprache, die insbesondere mit der AJAX(Asynchronous JavaScript and Xml)-Technologie immer mehr ins Rampenlicht r&uuml;ckt.</p>
<p>Hervorhebend zitieren m&ouml;chte ich den folgenden Grundsatz:</p>
<blockquote class="posterous_short_quote">
<p>"Wer JavaScript einsetzt, sollte () die M&ouml;glichkeiten der Sprache so einsetzen, dass der Anwender einen Mehrwert davon hat, und nicht so, dass ihm etwas genommen wird."</p>
</blockquote>
