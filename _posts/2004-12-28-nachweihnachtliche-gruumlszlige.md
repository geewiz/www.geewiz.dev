---
layout: post
title: "Nachweihnachtliche Gr&uuml;&szlig;e"
microblog: false
guid: http://geewiz.micro.blog/2004/12/28/nachweihnachtliche-gruumlszlige.html
date: 2004-12-28T09:34:00+0200
type: post
url: /2004/12/28/nachweihnachtliche-gruumlszlige.html
---

<p>Die Weihnachtstage sind vor&uuml;ber -- die Ruhe h&auml;lt an. Ich hoffe, ihr habt die Feiertage so weitgehend entspannt verbringen k&ouml;nnen wie ich!</p>
<p>Jetzt bin ich voll dabei, meine Geschenke zu genie&szlig;en. Es ist super -- dieses Jahr sind nur Volltreffer dabei. (Tipp: Ein gepflegter Wunschzettel lohnt sich.)</p>
<p>Den "Ghost in the Shell -- Manmachine Interface" habe ich schon durch -- von hinten nach vorn und von rechts nach links. Im Web schrieb jemand "Wer diesen Manga beim ersten Mal versteht, ist ein Genie". Ich bin erleichtert.</p>
<p>Sowohl aus der "Raumpatroille Orion Alphabox" als auch aus "Star Trek TNG 3. Season" lagen schon erste Disks im DVD-Player. Heja, die n&auml;chsten sturmfreien Abende sind gesichert!</p>
<p>Der Dilbert-Kalender wird eine Zierde f&uuml;r meinen Schreibtisch in der Firma sein, und f&uuml;r die Foto-Collage werde ich in meinem neuen Zuhause einen Ehrenplatz suchen.</p>
<p>Eins steht fest: Ich bin sehr dankbar f&uuml;r jedes kleine oder gro&szlig;e Geschenk -- dazu z&auml;hle ich an erster Stelle Liebe und Freundschaft.</p>
