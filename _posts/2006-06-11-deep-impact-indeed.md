---
layout: post
title: "Deep Impact indeed"
microblog: false
guid: http://geewiz.micro.blog/2006/06/11/deep-impact-indeed.html
date: 2006-06-11T21:48:43+0200
type: post
url: /2006/06/11/deep-impact-indeed.html
---

<p>Wer wissen m&ouml;chte, was von unserer blauen Kugel &uuml;brig bleibt, wenn sie einem gr&ouml;&szlig;eren Gesteinsbrocken begegnet, der sollte sich diese beeindruckende <a href="http://www.techeblog.com/index.php/tech-gadget/cgi-meteorite-collision-simulation">Computeranimation eines Meteoriteneinschlags</a> anschauen.</p>
<p>(Ich hab die Definition nicht zur Hand, aber ich glaube, eigentlich ist "Meteor" f&uuml;r ein Teil dieser Gr&ouml;&szlig;e die richtige Bezeichnung.)</p>
