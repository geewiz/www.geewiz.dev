---
layout: post
title: "Recommendation: Netgear WG311T WiFi card"
microblog: false
guid: http://geewiz.micro.blog/2006/09/12/recommendation-netgear-wgt.html
date: 2006-09-12T04:26:11+0200
type: post
url: /2006/09/12/recommendation-netgear-wgt.html
---

<p>After my unnerving <a href="http://blog.jochen-lillich.de/archives/526-I-hate-fiddling-with-hardware.html">WiFi problems</a>, Kai was so kind to get me a Netgear WG311T PCI card.</p>
<p>I just installed it, configured WPA and it works like a charm with Ubuntu 6.06.1!</p>
