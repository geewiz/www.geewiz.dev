---
layout: post
title: "How to find the right CMS"
microblog: false
guid: http://geewiz.micro.blog/2006/12/08/how-to-find.html
date: 2006-12-08T21:48:17+0200
type: post
url: /2006/12/08/how-to-find.html
---

<p>Choosing a content management system is getting more and more difficult, because the amount of alternatives keeps growing almost by the day. First, there are the open source solutions like Joomla, Typo3, Drupal or WebGUI. Or should it be a more "professional" (aka proprietary) software like CoreMedia or Contens? Which features offered by a certain CMS are the ones you need? And what crucial features does it lack?</p>
<p>Already in 1998, when I had to choose a CMS for the company I was working at at that time, defining the necessary functions and comparing CMS solutions was so time-consuming that I made that task a diploma thesis for a student working at the IT department. Up to today, the expense of comparing CMS software has risen by orders of magnitude.</p>
<p>But <a href="http://www.cmsmatrix.org/">The CMS Matrix</a> comes to the rescue. This website lists several hundred CMS solutions from "Ariadne Content Manager" to "Zumu Software" and facilitates the comparison of up to 10 of them at a time. The task of defining your needs is still yours, but finding the one CMS that suits them best has gotten a lot easier.</p>
