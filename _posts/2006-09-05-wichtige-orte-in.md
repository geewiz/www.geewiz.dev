---
layout: post
title: "Wichtige Orte in Freiburg"
microblog: false
guid: http://geewiz.micro.blog/2006/09/05/wichtige-orte-in.html
date: 2006-09-05T21:39:41+0200
type: post
url: /2006/09/05/wichtige-orte-in.html
---

<p>Inzwischen liegt der unterschriebene Mietvertrag auf dem Schreibtisch -- ab Oktober werde ich mit Carolin in Freiburg wohnen!</p>
<p>Neben den ganzen Umzugsmodalit&auml;ten gilt es jetzt vor allem, folgende wichtige Dinge zu kl&auml;ren:</p>
<h1>Wo in Freiburg gibt es einen empfehlenswerten Irish Pub?</h1>
<h1>Wo in Freiburg bekommt man Fr&uuml;hst&uuml;ck/Mittagessen/Kaffee inklusive WLAN?</h1>
<p>Irgendwelche Tipps?</p>
