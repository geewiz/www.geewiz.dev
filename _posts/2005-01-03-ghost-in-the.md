---
layout: post
title: "Ghost in the Shell -- Manmachine Interface"
microblog: false
guid: http://geewiz.micro.blog/2005/01/03/ghost-in-the.html
date: 2005-01-03T09:16:00+0200
type: post
url: /2005/01/03/ghost-in-the.html
---

<p>Mein Interesse f&uuml;r japanische Kultur erstreckt sich auch auf den Bereich der Mangas. F&uuml;r einen ersten Einblick habe ich mir "Ghost in the Shell -- Manmachine Interface" von Masamune Shirow  ausgesucht.<br />
Shirow ist einer der meistgelesenen Manga-Autoren und "Ghost in the Shell" ist sein bekanntestes Werk, das ich schon vor einiger Zeit auf VHS-Video gesehen habe. "Manmachine Interface" ist die Fortsetzung dazu und handelt von Major Motoko Aramaki, die f&uuml;r ihres Konzerns zum einen in der realen Welt, vor allem aber im Cyberspace f&uuml;r Ordnung sorgt.Der Manga besteht zu mehr als der H&auml;lfte aus farbigen Seiten in beeindruckender Qualit&auml;t. Oft fallen die vielen Details erst auf den zweiten Blick auf. Shirow arbeitet offenbar mit Computer-Unterst&uuml;tzung, denn z.B. Aramakis kleine Hilfsroboter sehen deutlich 3D-gerendert aus.</p>
<p>Dass die Protagonistin mit verschiedenen Cyberk&ouml;rpern arbeitet, beschert dem Leser viele sch&ouml;ne Bilder unglaublich proportionierter Frauen. Die in Mangas weit verbreiteten "Panty-Shots" sind auch hier zahlreich vorhanden.</p>
<p>Die Handlung dreht sich um einen Fall cyber-manipulierter Schweine, denen menschliche Gehirne eingesetzt wurden. Sie endet aber in einer Diskussion fern&ouml;stlicher Sch&ouml;pfungslehre, f&uuml;r es mir schlichtweg an Shintoismus-Kenntnissen mangelte.</p>
<p>Nach dem ersten Lesen surfte ich erst mal nach einschl&auml;gigen Websites und fand dort den Satz "Wer diesen Manga beim ersten Lesen versteht, ist ein Genie". Das beruhigt mich. Ich werde ihn demn&auml;chst nochmal lesen und hoffe, dass er dann f&uuml;r mich zu mehr als nur einem sch&ouml;nen Bilderbuch wird.</p>
