---
layout: post
title: "Making my own Nerd Font"
microblog: false
guid: http://geewiz.micro.blog/2021/10/05/making-my-own.html
date: 2021-10-05T20:59:10+0200
type: post
categories:
- "DevOps"
tags: [devops]
url: /2021/10/05/making-my-own.html
---
Well-structured status lines in vim and shell prompts with version control symbols are a nice quality-of-life improvement. Unfortunately, not all monospace fonts come with the necessary PowerLine glyphs. For example, my favourite font is [Operator Mono](https://www.typography.com/blog/introducing-operator), and it too doesn't have PowerLine symbols built in.

Thanks to the [NerdFonts font patcher](https://github.com/ryanoasis/nerd-fonts#option-8-patch-your-own-font), I was able to generate a font variant that has the necessary symbols. I simply ran the tool as a Docker container in my fonts directory:

```shell
$ mkdir HCo_OperatorMonoSSmNF
$ docker run --rm -v $PWD/HCo_OperatorMonoSSm/OpenType:/in \
  -v $PWD/HCo_OperatorMonoSSmNF:/out \
  nerdfonts/patcher --windows --powerline --powerlineextra
```

Now my [spaceship](https://github.com/spaceship-prompt/spaceship-prompt) shell 
prompt sparks even more joy.
