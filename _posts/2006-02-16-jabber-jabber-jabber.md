---
layout: post
title: "Jabber Jabber Jabber"
microblog: false
guid: http://geewiz.micro.blog/2006/02/16/jabber-jabber-jabber.html
date: 2006-02-17T01:03:58+0200
type: post
url: /2006/02/16/jabber-jabber-jabber.html
---

<p>Gestern hab ich mich mal bei <a href="http://www.jabber.org/about/overview.shtml">Jabber</a> angemeldet. Nicht, dass ich mit ICQ, MSN und Skype nicht schon mit Instant-Messaging-Diensten versorgt w&auml;re. Aber das offene Protokoll, auf dem Jabber basiert, hat einige Vorteile gegen&uuml;ber den genannten Insell&ouml;sungen der Internet-Konzerne.* Es gibt vielf&auml;ltige Software f&uuml;r alle g&auml;ngigen Betriebssysteme.<br />
* Jeder kann einen Jabber-Server betreiben, ihn ins weltweite Jabber-Netz einf&uuml;gen oder davon getrennt nutzen.<br />
* Bibliotheken, um eigene Jabber-Software zu entwickeln, sind frei verf&uuml;gbar.<br />
* &Uuml;ber Gateways (sog. "Transports") kann man auch ICQ und MSN erreichen. GoogleTalk nutzt sogar das Jabber-Protokoll selbst.<br />
* Der Dienst wird nicht von kommerziellen Interessen bestimmt.</p>
<p>Aus diesen Gr&uuml;nden habe ich Jabber zu meiner zentralen IM-L&ouml;sung erkoren.</p>
<p>Auf meiner <a href="http://www.jochen-lillich.de/kontakt">Kontaktseite</a> findet ihr meine ID und in meinem Wiki pflege ich eine kleine <a href="http://wiki.jochen-lillich.de/?JabberHilfe.">Anleitung zu Jabber</a></p>
