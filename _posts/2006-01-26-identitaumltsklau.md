---
layout: post
title: "Identit&auml;tsklau"
microblog: false
guid: http://geewiz.micro.blog/2006/01/26/identitaumltsklau.html
date: 2006-01-26T05:37:11+0200
type: post
url: /2006/01/26/identitaumltsklau.html
---

<p>Heute war Post vom Amtsgericht im Kasten:</p>
<blockquote class="posterous_medium_quote">
<p><em>Beschluss in der Strafsache Jochen Lillich [...] wegen Computerbetruges</em><br />
  Der Antrag der Staatsanwaltschaft vom 19.10.2005 auf Erlass eines Strafbefehls wird abgelehnt.<br />
  Der Hintergrund:</p>
</blockquote>
<p>Ende 2004 erhielt ich ein Schreiben eines Anwaltsb&uuml;ros, das im Auftrag von eBay die Provision f&uuml;r die Auktionen eintreiben sollte, die ich als Benutzer "super_hasi446" durchgef&uuml;hrt h&auml;tte. Nach einem Antwortschreiben, in dem ich sinngem&auml;&szlig; erkl&auml;rte, dass ich unter diesem d&auml;mlichen Pseudonym niemals bei eBay aktiv gewesen sei, war erst mal wieder Ruhe. Doch im Februar 2005 flatterte mir &uuml;berraschend eine Vorladung der Polizei ins Haus. Es sei n&auml;mlich gegen mich Strafanzeige wegen Computerbetrugs erstattet worden.</p>
<p>Das war dann doch ein Grund, meinen Anwalt einzuschalten. Die Akteneinsicht ergab, das jemand unter meinem Namen und meiner Adresse ein eBay-Benutzerkonto angelegt und damit irgendwelche Porno-Dienstleistungen ersteigert hatte. Das Problem: auch die angegebene Bankverbindung war geklaut und der Dienstleister zog seinen Preis per Lastschrift ein. Die Gesch&auml;digte lie&szlig; die Buchung stornieren und erstattete Anzeige. Unsere Freunde und Helfer fanden nur eine Spur: den registrierten Namen nebst Adresse.</p>
<p>Trotz meiner schriftlichen Stellungnahme, in der ich vor allem darauf hinwies, dass <em>jeder</em>, der meinen Namen und mein Geburtsdatum kennt, damit ein eBay-Konto anlegen kann, beantragte die Staatsanwaltschaft Karlsruhe den Erlass eines Strafbefehls gegen mich.</p>
<p>Wenigstens das Amtsgericht Philippsburg aber kam jetzt zu dem Schluss, dass meine Erkl&auml;rung nicht zu widerlegen ist und "gegenw&auml;rtig keine &uuml;berwiegende Wahrscheinlichkeit f&uuml;r eine Verurteilung des Angeschuldigten besteht."</p>
<p>Endlich ist das Thema vom Tisch. Es kostete mich einiges an Nerven und Geld. Zumindest letzteres kann ich jetzt von Vater Staat zur&uuml;ckfordern.</p>
