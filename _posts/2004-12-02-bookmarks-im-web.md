---
layout: post
title: "Bookmarks im Web"
microblog: false
guid: http://geewiz.micro.blog/2004/12/02/bookmarks-im-web.html
date: 2004-12-02T02:37:00+0200
type: post
url: /2004/12/02/bookmarks-im-web.html
---

<p>Wenn man an wechselnden Orten surft (daheim, im B&uuml;ro, im Internet-Cafe), entstehen schnell unterschiedliche Bookmark-Sammlungen. Und oft ist das Bookmark, das man gerade braucht, auf dem anderen Rechner.</p>
<p>Durch die Installation von <a href="http://sitebar.lillich.info/">Sitebar</a> auf meinem Rootserver kann ich jetzt seit einigen Wochen meine Bookmarks von &uuml;berall aus ablegen und abrufen.</p>
<p>Browser wie Mozilla oder Firefox erlauben die Integration in eine seitliche Leiste neben dem Browserfenster (daher auch die Anspielung auf <a href="http://de.wikipedia.org/wiki/Bookmarklet">Sidebar"), sodass man jederzeit bequemen Zugriff auf seine Bookmarks hat. &Uuml;ber ein "Bookmarklet</a> kann ich den URL der Seite, die ich gerade besuche, per Mausklick in Sitebar &uuml;bernehmen.</p>
<p>Einen Teil meiner Bookmarks mache ich <a href="http://sitebar.lillich.info/sitebar.php">&ouml;ffentlich</a> zug&auml;nglich, damit auch andere Nutzen daraus ziehen k&ouml;nnen. Andere Links behalte ich wiederum f&uuml;r mich. Das Rechtekonzept von Sitebar erlaubt beides auf einfache Weise.</p>
<p>Wer jetzt ebenfalls seine Bookmarks im Web pflegen m&ouml;chte, kann mir gern eine E-Mail schicken, dann richte ich ihr auf meinem Server ein eigenes Benutzerkonto ein.</p>
