---
layout: post
title: "Der Datenschutz geht unter"
microblog: false
guid: http://geewiz.micro.blog/2006/04/12/der-datenschutz-geht.html
date: 2006-04-12T22:16:28+0200
type: post
url: /2006/04/12/der-datenschutz-geht.html
---

<p>Das Szenario vom "gl&auml;sernen Mensch" sei l&auml;ngst Realit&auml;t, erkl&auml;rte der Hamburger Datenschutzbeauftragte Hartmut Lubomierski bei der Vorlage seines T&auml;tigkeitsberichtes der letzten Jahre. Das Recht, sich im &ouml;ffentlichen Raum unbeobachtet und unerfasst zu bewegen, werde auf der einen Seite immer mehr ausgeh&ouml;hlt und auf der anderen Seite von den B&uuml;rgern kaum eingefordert.</p>
<p>Die derzeit diskutierte Video&uuml;berwachung in Schulen m&uuml;sse auf Objektsicherung beschr&auml;nkt werden, "sonst brechen die letzten D&auml;mme". (Die Ideen wuchern bei diesem Thema bis zur Kontrolle des laufenden Unterrichts.)</p>
<p>Weitere Besorgnis erregende Entwicklungen nennt der Artikel <a href="http://www.heise.de/newsticker/meldung/71905"><em>Datensch&uuml;tzer: Gl&auml;serner Mensch l&auml;ngst Realit&auml;t</em></a> auf Heise Online.</p>
