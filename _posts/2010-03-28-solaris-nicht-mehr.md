---
layout: post
title: "Solaris nicht mehr kostenlos"
microblog: false
guid: http://geewiz.micro.blog/2010/03/28/solaris-nicht-mehr.html
date: 2010-03-28T16:43:55+0200
type: post
url: /2010/03/28/solaris-nicht-mehr.html
---

<div class="posterous_bookmarklet_entry">
<p>Sun &auml;ndert die Lizenzbedingungen f&uuml;r Solaris und stellt klar:</p>
<blockquote><div>
<p>Please remember, your right to use Solaris acquired as a download is limited to a trial of 90 days, unless you acquire a service contract for the downloaded Software.</p>
</div>
</blockquote>
<div class="posterous_quote_citation">via <a href="http://www.cuddletech.com/blog/pivot/entry.php?id=1120">cuddletech.com</a></div>
<p>Auch wenn Solaris in Zukunft nur noch mit kostenpflichtiger Lizenz betrieben werden darf, gibt es ja noch OpenSolaris und Dritt-Distributionen wie Nexenta, Schillix und Belenix.</p>
</div>
