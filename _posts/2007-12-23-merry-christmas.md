---
layout: post
title: "Merry christmas!"
microblog: false
guid: http://geewiz.micro.blog/2007/12/23/merry-christmas.html
date: 2007-12-24T01:59:36+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm1static_eorvj-scaled500.jpg?w=240
photos:
- /assets/wp-content/2011/05/media_httpfarm1static_eorvj-scaled500.jpg?w=240
url: /2007/12/23/merry-christmas.html
---

<p>To everyone out there I wish happy holidays. Enjoy the festive season, spread some love and happiness!</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm1static_eorvj" height="180" src="/assets/wp-content/2011/05/media_httpfarm1static_eorvj-scaled500.jpg?w=240" width="240" />
</div></p>
<p>And remember: there's a reason <strong>christ</strong>mas has been a special time for joy and love for so many centuries.</p>
