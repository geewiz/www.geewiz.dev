---
layout: post
title: "Armut in mehrfachem Sinn"
microblog: false
guid: http://geewiz.micro.blog/2004/10/19/armut-in-mehrfachem.html
date: 2004-10-19T20:03:00+0200
type: post
url: /2004/10/19/armut-in-mehrfachem.html
---

<p>Bei <a href="http://blog.koehntopp.de/archives/531-Cholera.html">Isotopp</a> fand ich den Link auf den Telepolis-Artikel <a href="http://blog.koehntopp.de/exit.php?url_id=2044">Sie f&uuml;rchten weder Pest noch Colera. Aber das Markenrecht.</a>&amp;entry_id=531. Nach der Lekt&uuml;re habe ich mich entschieden, in Zukunft auf Coca-Cola zu verzichten.</p>
<p>Nicht nur, dass ein staatlicher Plakatwettbewerb zur globalen Armut sich aus Angst vor juristischen Folgen von vornherein scheut, das Siegermotiv abzubilden, nein, auch die im Artikel verlinkten Fakten zu den Machenschaften der Getr&auml;nkefirma sind erschreckend. Mich jedenfalls erschrecken sie. Und daher werde ich mich jetzt nach einer Alternative umschauen, um meinen K&ouml;rper mit Fl&uuml;ssigzucker zu versorgen.</p>
