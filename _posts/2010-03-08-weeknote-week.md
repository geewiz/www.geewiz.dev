---
layout: post
title: "Weeknote #-5 (week 9, 2010)"
microblog: false
guid: http://geewiz.micro.blog/2010/03/08/weeknote-week.html
date: 2010-03-08T21:56:59+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2010/03/08/weeknote-week.html
---
<br />
<h3>Perl online training, open source development</h3>
<p>At the beginning of the week, I decided to offer our "<a href="http://www.freistil-consulting.de/onlinetraining/perl-meisterkurs">Perl Meisterkurs</a>" online seminar from now on in regular intervals. I planned three courses for April, July and October, created their registration pages, and sent informational posts to several blogs and social networks. Training is the most intensive and time-consuming work I do currently, but it helps getting in some cash that will be needed when we have to expand our IT infrastructure.</p>
<p>Side note: Maybe it was organizing a Perl online training that triggered some<br />
sleeping Perl development enzymes in me. On Tuesday, I picked up my old<br />
project<br />
<a href="http://search.cpan.org/perldoc?CGI%3A%3ASession%3A%3AAuth">CGI::Session::Auth</a>,<br />
a Perl Module I published as free software many years ago. I fixed a few bugs<br />
and finally implemented suggestions I got back in 2008 for improving<br />
the documentation. Because I now use Bazaar as my version control<br />
software, I moved the project from BerliOS to<br />
<a href="http://www.launchpad.net">Launchpad</a> where I'll hopefully maintain it a<br />
bit better now.</p>
<h3>Website building</h3>
<p>On Saturday, we got the finished design for our new Drupal hosting website. We're now working on the site content to get it online ASAP.</p>
<h3>Communication infrastructure</h3>
<p>Now that first business contacts are forming, telephone communication becomes very important. I tested and chose <a href="http://www.sipgate.de/team">Sipgate Team</a> as our virtual PBX system. We got a set of phone numbers that we're able to distribute among our VOIP accounts by single user or by team. Voicemail is integrated and delivers incoming messages via email; new message notifications are sent as SMS. The system is easy to configure and has a good cost structure.</p>
<h3>Family</h3>
<p>Carolin and Amalia will return home next Wednesday, so the days of my all-day quiet home office are coming to an end. It's a real challenge to balance work and family life if both happen at the same place, but to me it's a challenge worth taking on.</p>
