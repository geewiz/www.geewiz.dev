---
layout: post
title: "Live blogging from the German Perl Workshop"
microblog: false
guid: http://geewiz.micro.blog/2007/02/21/live-blogging-from.html
date: 2007-02-21T21:11:57+0200
type: post
url: /2007/02/21/live-blogging-from.html
---

<p>Yesterday, I arrived in Munich, land of the walking moustaches, for the German Perl Workshop that I will be attending from Wednesday to Friday.</p>
<p>After discovering the open WLAN at FH M&uuml;nchen where the workshop takes place, I'm able to blog live from the talks. So, head over to <a href="http://www.it-dojo.de">IT-Dojo</a> if you're interested in my findings.</p>
