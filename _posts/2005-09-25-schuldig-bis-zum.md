---
layout: post
title: "Schuldig bis zum Beweis des Gegenteils"
microblog: false
guid: http://geewiz.micro.blog/2005/09/25/schuldig-bis-zum.html
date: 2005-09-25T07:28:59+0200
type: post
url: /2005/09/25/schuldig-bis-zum.html
---

<p>Beim Konflikt "Datenschutz und Pers&ouml;nlichkeitsrechte" gegen "Innere Sicherheit" kommt schnell das Argument auf den Tisch, wer nichts zu verbergen habe, m&uuml;sse sich auch wegen des Abbaus bei Datenschutz und Kommunikationsgeheimnis keine Sorgen machen.</p>
<p>Wie es jemandem gehen kann, der v&ouml;llig schuldlos ist, aber trotzdem die Aufmerksamkeit der Sicherheitsw&auml;chter erregt, beschreibt ein Artikel im Guardian, <a href="http://www.guardian.co.uk/comment/story/0">Suspicious behaviour on the tube</a>,,1575411,00.html. David Mery beschreibt darin, wie er beim Warten auf die U-Bahn verhaftet wurde und anschlie&szlig;end seine Wohnung durchsucht sowie zahlreiche verd&auml;chtige Besitzt&uuml;mer beschlagnahmt wurden.Die Gr&uuml;nde daf&uuml;r:</p>
<ul>
<li>Er habe sich im Augenschein und auf den &Uuml;berwachungsvideos verd&auml;chtig benommen.</li>
<li>Beim Betreten des Bahnhofs habe er die Polizisten nicht angeschaut.</li>
<li>Zwei andere M&auml;nner h&auml;tten den Bahnhof gleichzeitig mit ihm betreten.</li>
<li>Seine Jacke sei zu warm f&uuml;r die Jahreszeit gewesen.</li>
<li>Er hatte einen Rucksack dabei gehabt und diesen immer bei sich getragen.</li>
<li>Er habe Leute, die den Bahnsteig betraten, betrachtet.</li>
<li>Er habe mit seinem Handy gespielt und dann ein Papier aus der Jacke gezogen.</li>
</ul>
<p>Wie gut, dass es klare Erkennungsmerkmale f&uuml;r Terroristen gibt. Also, liebe Leser, wenn ihr in London die U-Bahn benutzt:</p>
<ul>
<li>Sofort anfangen, die Polizisten anzustarren!</li>
<li>Wenig Kleidung tragen, am besten nackt gehen!</li>
<li>Auf dem Bahnsteig keine Passanten ansehen, sondern nur auf den Boden blicken!</li>
<li>Den Rucksack auf dem Bahnsteig abstellen und sich sofort davon entfernen!</li>
</ul>
<p>Sonst seid ihr selbst schuld, dass ihr verhaftet werdet. Und merkt euch diese Regeln auch gleich f&uuml;r deutsche Bahnh&ouml;fe, denn auch unsere Sicherheit wird durch Schily, Beckstein und Co. zusehends verbessert.</p>
