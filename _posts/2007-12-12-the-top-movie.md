---
layout: post
title: "The Top 20 movie hackers"
microblog: false
guid: http://geewiz.micro.blog/2007/12/12/the-top-movie.html
date: 2007-12-12T09:41:33+0200
type: post
url: /2007/12/12/the-top-movie.html
---

<p>In <a href="http://www.drivl.com/posts/view/461">Top 20 Hackers in Film History</a>, Scott Willoughby collected the "20 coolest, funniest, dare I say sexiest hackers and computer geeks" that appeared in movies.</p>
<p>While I'd have picked Wyatt Donnelly, John Draper, Michael Bolton &amp; Samir Nagheenanajar, The Puppet Master, Martin Bishop and Neo myself, I'd have put them on the top ranking places. A boy creating Kelly Le Brock with his computer, a real life hacker pioneer, two prototypes of the corporate IT guy, the worthy opponent of a gorgeous cyber police girl, a hacker become smart and good-looking, and the god of cyberspace obviously have to be on the leading ranks.</p>
<p>But come on, David Lightman only on rank 4? He's clearly my number one! "War Games" is my most-watched DVD and it'll always remind me of the times when I started hacking myself. The movie also explains why I'm still amazed by speech synthesizers. (And additionally, the same actor, Matthew Broderick, also starred in my eternal #2 movie of all times, "Ferris Bueller's Day Off".)</p>
<p>These are the guys that make me proud being called a geek.</p>
