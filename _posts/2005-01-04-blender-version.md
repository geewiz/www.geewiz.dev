---
layout: post
title: "Blender Version 2.36"
microblog: false
guid: http://geewiz.micro.blog/2005/01/04/blender-version.html
date: 2005-01-04T02:27:00+0200
type: post
url: /2005/01/04/blender-version.html
---

<p>Die freie 3D-Grafik- und Animationssoftware Blender ist seit kurzem in Version 2.36 verf&uuml;gbar.</p>
<p>Die <a href="http://www.blender3d.org/cms/Blender_2_36.384.0.html">Neuerungen</a>  sind auf der Projekt-Website ver&ouml;ffentlicht. </p>
<p>Ich kannte Blender bisher nur dem Namen nach. Die neue Version habe ich jetzt mal installiert und allein die Demos auszuprobieren macht schon viel Spa&szlig;. Ohne Anleitung kommt man allerdings nicht weit. Grund genug, ein passendes Buch auf meinen <a href="/persoenlich/wunschzettel.html">Wunschzettel</a> zu setzen.</p>
