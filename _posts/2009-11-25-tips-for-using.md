---
layout: post
title: "Tips for using Google Wave in client projects"
microblog: false
guid: http://geewiz.micro.blog/2009/11/25/tips-for-using.html
date: 2009-11-25T19:20:40+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwjochenli_fdyde-scaled500.jpg?w=180
photos:
- /assets/wp-content/2011/05/media_httpwwwjochenli_fdyde-scaled500.jpg?w=180
url: /2009/11/25/tips-for-using.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwjochenli_fdyde" height="153" src="/assets/wp-content/2011/05/media_httpwwwjochenli_fdyde-scaled500.jpg?w=180" width="180" />
</div>
<p>The media hype around <a href="http://wave.google.com">Google Wave</a> seems to fade a bit, and since invitations to the service spread, people start looking for practical applications. </p>
<p>What's getting clear is that Wave isn't a replacement for email as the Google I/O premiere presentation suggested. Because Wave's more of a mix between web chat and collaborative document editing, it's more likely to replace my team chat on <a href="http://www.campfirenow.com">Campfire</a> some day than it's going to replace Google's own GMail.</p>
<p>Will Kelly tried Wave for a bit of client collaboration and posted his findings on <a href="http://webworkerdaily.com/2009/11/09/using-google-wave-on-your-first-project-6-tips/">6 Tips For Using Google Wave On Your First Project</a>. To the people that want to try Google Wave with a client, he suggests:</p>
<ol>
<li>Set suitable expectations.</li>
<li>Do a dry run with a Wave.</li>
<li>Take control of your Waves.</li>
<li>Use folders and tags.</li>
<li>Consider whether to use live editing or attachments.</li>
<li>Have a Plan B.</li>
</ol>
<p>Since Google Wave still is a preview version, I'd be careful using it for serious project work. The service still has got many rough edges and misses some extensions and gadgets to add useful functionality. But if your team or some of your clients are open for some early adopting, give it a try! It's fun!</p>
<p>(BTW, if you've been sitting under a rock over the last days and/or noone shared an invitation with you so far, I've got some left. Just leave a comment with your email address if you'd like one.)</p>
