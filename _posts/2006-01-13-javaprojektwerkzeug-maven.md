---
layout: post
title: "Java-Projektwerkzeug: Maven"
microblog: false
guid: http://geewiz.micro.blog/2006/01/13/javaprojektwerkzeug-maven.html
date: 2006-01-13T07:53:15+0200
type: post
url: /2006/01/13/javaprojektwerkzeug-maven.html
---

<p><a href="http://ant.apache.org">Jeden Tag ein neues Buzzword." Das ist momentan meine Devise, wie es scheint. Es gibt rund um Java inzwischen so viele Technologien, Werkzeuge, Architekturen und Frameworks, dass man erst mal den Wald vor lauter B&auml;umen nicht sieht. Genau deshalb habe ich so lange gez&ouml;gert, einen Fu&szlig; auf Java-Terrain zu setzen. Aber langsam blicke ich immer mehr durch. Meine neueste Bekanntschaft heisst "Maven".Vor allem Apache Software Foundation ist ein unersch&ouml;pflicher Quell f&uuml;r Frameworks und Werkzeuge. Schon deren "ant</a> hat mir gefallen, weil es besser als Make in die von XML beherrschte Landschaft passt und z.B. schon einen eingebauten XSLT-Prozessor mitbringt[1]. Mit <a href="http://maven.apache.org">Maven</a> aus der gleichen Schmiede kann man ebenfalls Build-Prozesse modellieren, aber die sind nur die Grundlage, auf der die Software Plugins f&uuml;r die Projektverwaltung und -Dokumentation aufbaut. Zum Beispiel erzeugt Maven nach der Durchf&uuml;hrung der Unit-Tests gleich ausf&uuml;hrliche HTML-Berichte aus den Testergebnissen. Auswertungen &uuml;ber die Einhaltung von Stilstandards, ebenfalls als HTML-Seiten, liefert das Checkstyle-Plugin. Andere Plugins werten SVN-Repositories aus oder laden erzeugte Dateien gleich auf einen Webserver hoch.</p>
<p>Ich bin ein Fan von Dokumentation. Und ich bin ein Fan von Werkzeugen, die Dokumentation unterst&uuml;tzen und automatisieren. Und ich glaube, ich bin ein Fan von Maven.</p>
<p>fn1. Wer mit DocBook arbeitet, weiss das auf Anhieb zu sch&auml;tzen.</p>
