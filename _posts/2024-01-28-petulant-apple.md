---
layout: post
title: "Petty and petulant"
date: 2024-01-28 17:19 +0000
description: How Apple is losing its appeal for me.
feature-img:
category: commentary
tags:
  - tech
  - apple
published: true
sitemap: true
---

Apple seems to have lost its way. It once built computers that would empower their users. Now they're doing everything to cage them in.

<!--more-->

In his article "[Apple as a Petulant Child](https://curtismchale.ca/2024/01/27/apple-as-a-petulant-child/)", Curtis McHale shares his perspective on how Apple is behaving in the wake of the EU ruling with regard to the Digital Markets Act, and in general lately.

> "I want to like Apple devices. The software is good. The hardware is even good, unless we talk about repairability. [...] The only viable tablet I've found is the iPad family of devices from Apple. Yet watching the moves that Apple continues to make that are customer hostile I get more and more hesitant to purchase future devices from them."

I share his discomfort. I once was an Apple fan. While I switched my desktop machine and my laptops from Mac to Linux a few years ago already, I'm still using an iPhone and two iPads both for work and leisure. I used to replace my iPhones and iPads with newer models occasionally, but my reluctance to keep doing so is growing.

I thought about going for an iPhone Pro Max next time because of its bigger screen. I do a lot of reading on my phone, and font size has become a concern that younger me didn't know. But at the eye-watering price of a big iPhone, I'd rather support Framework's efforts of making tech more sustainable by buying their 13" laptop. My business is paying for my phone, but I just can't see myself spending laptop money on a phone.

I'm also losing interest in the iPad. Even the iPad Pro in its latest Apple Silicon incarnations is still way behind the initial promise of a workstation in a tablet form factor. After all these years, we're still getting just a larger iPhone with only a few concessions made to make better use of the screen real estate.

Curtis speaks to my heart when he concludes that "the decisions from Apple in the last number of years have turned me from an enthusiastic Apple customer into a reluctant Apple customer. If we keep up this trajectory I'll be a never again Apple customer."

That's why I first switched to Windows and finally to Linux as my desktop OS. And now I've started to look at greener phone pastures as well, currently over on the Google Pixel side of the fence. I'm hearing good things about [GrapheneOS](https://grapheneos.org/).

On a side note, the only critique I have on Curtis' article is that the Apple II was considered a *microcomputer*, not a minicomputer. In the 1970s, a minicomputer was just one step down in size from what was initially the only type of computer, the room-filling mainframe. A minicomputer, on the other hand, now "only" had the size of a living room cabinet. One of the most popular minicomputer models was the DEC PDP-11. Dennis Ritchie and Ken Thompson used one to develop UNIX at Bell Labs. (The story how they navigated corporate bureaucracy to get one is pretty funny, actually.) Here's a famous photo of them working at the teletype console of the PDP-11 in the background. The teletype would fit on a desk, but the computer not so much.

![](/assets/img/thompson_ritchie_pdp11.jpg)
