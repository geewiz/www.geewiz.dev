---
layout: post
title: "I find Dilbert spooky"
microblog: false
guid: http://geewiz.micro.blog/2007/10/29/i-find-dilbert.html
date: 2007-10-29T20:58:14+0200
type: post
url: /2007/10/29/i-find-dilbert.html
---

<p>In a timeframe of about a week, I'll always find a Dilbert cartoon that fits what happens around me at work.</p>
<p>Okay, <a href="http://www.dilbert.com/comics/dilbert/archive/dilbert-20071026.html">this one</a> from Friday is timeless and generic. :-)</p>
