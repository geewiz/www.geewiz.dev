---
layout: post
title: "Skype-Telefone f&uuml;rs WLAN"
microblog: false
guid: http://geewiz.micro.blog/2006/05/08/skypetelefone-fuumlrs-wlan.html
date: 2006-05-08T21:29:02+0200
type: post
url: /2006/05/08/skypetelefone-fuumlrs-wlan.html
---

<p>Im n&auml;chsten Monat sollen gleich zwei Telefone auf den Markt kommen, mit denen man auch ohne PC &uuml;ber <a href="http://www.skype.com">Skype</a> telefonieren kann:</p>
<ul>
<li>das <a href="http://www.netgear.de/Privat/voip/SPH101/index.html">SPH101</a> von Netgear</li>
<li>das <a href="http://www.smc.com/index.cfm?event=press.pressRelease">SMCWSKP100</a>&amp;localeCode=DE_DEU&amp;pid=4655 von SMC<br />
Der Listenpreis f&uuml;r das Netgear-Modell liegt bei 299,- , es sind aber bei zahlreichen H&auml;ndlern bereits Vorbestellungen zu Preisen unter 220,-  m&ouml;glich. Das Skype-Handy von SMC liegt mit einem Listenpreis von 199,-  noch ein St&uuml;ck darunter.</li>
</ul>
<p>Die beiden Alternativen haben einen &auml;hnlichen Funktionsumfang. Sie verbinden sich per WLAN mit Skype, ohne dabei auf einen PC angewiesen zu sein, stellen die Kontaktliste mit Statusinformationen auf dem Display dar und erm&ouml;glichen die Abfrage des Skype-Anrufbeantworters. Nat&uuml;rlich ist &uuml;ber die Dienste SkypeIn und SkypeOut auch die Anbindung an das Telefon-Festnetz m&ouml;glich.</p>
<p>Als intensiver Skype-Anwender bin ich gespannt, was erste Tests ergeben, wenn die Ger&auml;te tats&auml;chlich erh&auml;ltlich sind!</p>
