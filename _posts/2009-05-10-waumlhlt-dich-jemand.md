---
layout: post
title: "W&auml;hlt dich jemand, den du &ouml;ffentlich verh&ouml;hnst?"
microblog: false
guid: http://geewiz.micro.blog/2009/05/10/waumlhlt-dich-jemand.html
date: 2009-05-10T21:25:30+0200
type: post
url: /2009/05/10/waumlhlt-dich-jemand.html
---

<p>Im Artikel &#8220;<a href="http://www.zeit.de/online/2009/20/netzsperren-kritik-verdrossenheit?page=all">Wie man eine Generation verliert</a>&#8221; erkl&auml;rt &#8220;Die Zeit&#8221;, wie Politiker die Verdrossenheit junger W&auml;hler weiter steigern, indem sie ihre Anliegen einfach nicht ernst nehmen:</p>
<blockquote style="border:none;margin:0 0 0 40px;padding:0;"><p>Im Internet gibt es viele Proteste gegen die geplanten Kinderpornosperren. Politiker haben f&uuml;r die Kritik nur Verachtung &uuml;brig. So verprellen sie ihre k&uuml;nftigen W&auml;hler.
</p></blockquote>
