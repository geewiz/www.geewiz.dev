---
layout: post
title: "Linux auf Dell Inspiron 8200"
microblog: false
guid: http://geewiz.micro.blog/2007/10/22/linux-auf-dell.html
date: 2007-10-22T19:04:32+0200
type: post
url: /2007/10/22/linux-auf-dell.html
---

<p>Im Februar 2003 legte ich mir ein Dell Inspiron 8200 Notebook zu. Nat&uuml;rlich installierte ich Linux, genauer gesagt SuSE Linux 8.1.</p>
<h2>Technische Daten</h2>
<p>Mein Ger&auml;t hat folgende Daten:</p>
<ul>
<li>Intel Pentium IV Mobile 2GHz</li>
<li>15 Zoll UXGA Ultra Sharp Display</li>
<li>Grafikkarte ATI Radeon 9000 Mobile</li>
<li>max. Aufl&ouml;sung: 1600x1200 Pixel</li>
<li>Festplatte: 40GB</li>
<li>Arbeitsspeicher: 512 MB</li>
<li>Kombi CDRW/DVD Laufwerk Samsung SN-324B</li>
<li>Touchpad, Trackpoint</li>
<li>Sound: AC97 i810</li>
</ul>
<h2>Kernel</h2>
<p>Der SuSE-Kernel kann meist unver&auml;ndert genutzt werden.</p>
<p>Um VMware betreiben zu k&ouml;nnen, muss jedoch die APIC-Unterst&uuml;tzung aktiviert werden. Dazu ist in <code>/boot/grub/menu.lst</code> die <code>kernel</code>-Zeile um den Parameter <code>apic</code> zu erweitern:</p>
<div class="CodeRay">
<div class="code">
<pre>title linux
   kernel (hd0,5)/boot/vmlinuz root=/dev/hda6  hdb=ide-scsi vga=normal apic
     apm=on acpi=off
   initrd (hd0,5)/boot/initrd</pre>
</div>
</div>
<p>Mit <code>vga=normal</code> wird zus&auml;tzlich der Framebuffer-Betrieb<br />
des ersten virtuellen Terminals abgeschaltet, damit er keine Probleme<br />
mit XFree86 verursacht.</p>
<h3>Power Management</h3>
<p>ACPI habe ich zu Gunsten von APM abgeschaltet. SuSE hat zwar die<br />
ACPI-Funktionen aus den 2.5er Versionen zur&uuml;ckportiert, mit dem<br />
Inspiron kommen sie aber scheinbar noch nicht richtig zu Recht. Zum<br />
Beispiel erhalte ich keine Batterie-Informationen mehr, seit ich einen<br />
zweiten Akku einbaute. Mit APM hingegen zeigt mir <code>wmbattery</code> eine Laufzeit von knapp 6 Stunden an.</p>
<h3>BlueTooth</h3>
<p>BlueTooth konnte ich mit dem Kernel von SuSE Linux 8.1 nicht betreiben. Hier fehlen bei 2.4.19-SuSE noch wichtige Module wie zB "rfcomm".</p>
<p>Um auch den USB-Dongle von Acer nutzen zu k&ouml;nnen, installierte ich einen Vanilla Kernel von <a href="http://www.kernel.org">http://www.kernel.org</a> in der Version 2.4.20 mit den BlueTooth-Patches von Marcel Holtmann (<a href="http://www.holtmann.org/linux/kernel/">http://www.holtmann.org/linux/kernel/</a>).</p>
<h3>Sound</h3>
<p>Die ALSA-Treiber spielten auch nach einem Update auf die aktuelle<br />
Version Sounds nicht fehlerfrei ab, es passieren vermehrt Knackser und<br />
Ruckler. Der im Kernel integrierte OSS-Treiber funktioniert hingegen<br />
problemlos. Daher habe ich ALSA deaktiviert (<code>insserv -r alsasound</code>).</p>
<p>In <code>/etc/modules.conf</code> sind die ALSA-Eintr&auml;ge zu entfernen und folgende Zeilen zu &auml;ndern:</p>
<div class="CodeRay">
<div class="code">
<pre>alias char-major-14 i810_audio
alias sound i810_audio</pre>
</div>
</div>
<h2>Display</h2>
<p>XFree86 unterst&uuml;tzt die Radeon-Grafikkarten von ATI angeblich erst mit Version 4.2.3. Daher habe ich die aktuellen ATI-Treiber installiert.<br />
Heruntergeladen k&ouml;nnen sie von der <a href="http://www.ati.com/support/drivers/linux/radeon-linux.html">ATI-Website</a> werden. Die Installation erfolgt einfach per <code>rpm</code>:</p>
<div class="CodeRay">
<div class="code">
<pre>rpm -ihv fglrx-glc22-x.y.rpm</pre>
</div>
</div>
<p>Es wird anschlie&szlig;end automatisch ein Script (<code>fglrxconfig</code>) ausgef&uuml;hrt, das die X11-Konfiguration erzeugt. F&uuml;r das UltraSharp<br />
Display habe ich als Eckdaten f&uuml;r die horizontale Frequenz 30-200 und f&uuml;r die vertikale 50-150 eingegeben.</p>
<p>Die Grafik funktionierte allerdings erst dann richtig, als ich manuell noch eine <code>ModeLine</code> f&uuml;r das Display eingetragen hatte.</p>
<p><i>Update 03.08.2003:</i> Auch nach einem Update auf XFree86 4.3 bot mir Sax2 keine passenden Treiber an. Mit dem ATI-Treiber f&uuml;r 4.3 l&auml;uft der Notebook jedoch bislang hervorragend.</p>
<h2>Maus</h2>
<p>Die beiden eingebauten Mausger&auml;te, Touchpad und Trackpoint, funktionierten auf Anhieb. Damit auch eine angeschlosene USB-Maus<br />
funktioniert, muss die X11-Konfiguration um einen Abschnitt f&uuml;r ein<br />
weiteres Eingabeger&auml;t ("Mouse2" in unten stehender Konfiguration)<br />
erweitert werden.</p>
<h2>Modem</h2>
<p>Das Modem habe ich nie in Betrieb zu nehmen versucht.</p>
