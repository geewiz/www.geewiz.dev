---
layout: post
title: "Weniger ist mehr"
microblog: false
guid: http://geewiz.micro.blog/2006/03/28/weniger-ist-mehr.html
date: 2006-03-28T20:11:42+0200
type: post
url: /2006/03/28/weniger-ist-mehr.html
---

<p>Ich benutze seit einigen Wochen GMail als Postfach und bin damit sehr zufrieden. Ich komme &uuml;ber eine einfache und schnelle, aber dennoch komfortable Oberfl&auml;che an meine Mail -- und das von &uuml;berall. (Ich gebe zu, dass diese Erkenntnis sehr sp&auml;t kam.)</p>
<p>In <a href="http://dip.lodoc.us/articles/2006/03/13/yahoo-mail-vs-gmail-contrasting-ideas-about-web-application-design">Yahoo Mail vs. Gmail: Contrasting Ideas About Web Application Design</a> wird nun der AJAX-Pionier mit seiner neuen Konkurrenz verglichen. Interessant finde ich dabei vor allem die Feststellung, dass GMail eher den Charakter einer "Webanwendung" habe, w&auml;hrend YahooMail den Eindruck einer "Desktopanwendung" hinterlasse.</p>
<p>Ich bin sehr skeptisch, ob das Emulieren von Desktopanwendungen &uuml;berhaupt der richtige Weg ist. Die Vertreter des "Web 2.0", die ich begeistert einsetze, zeichnen sich gerade dadurch aus, dass sie weniger mit Funktionen &uuml;berladen und deshalb deutlich einfacher zu bedienen sind.</p>
