---
layout: post
title: "Science Fiction"
microblog: false
guid: http://geewiz.micro.blog/2006/04/26/science-fiction.html
date: 2006-04-26T20:00:42+0200
type: post
url: /2006/04/26/science-fiction.html
---

<p><em>Eigentlich</em> m&uuml;sste ich es cool finden, dass die ganzen Motive aus meinen geliebten Science-Fiction-Streifen der 80er und 90er Realit&auml;t werden: &Uuml;berwachung <a href="http://www.heise.de/newsticker/meldung/72377">Situationsaufmerksamkeit</a> durch Sensoren aller Art, herumfliegende W&auml;chterdrohnen, Speicherung und Verarbeitung umfassender Bewegungsdaten usw.</p>
<p>Eigentlich.</p>
