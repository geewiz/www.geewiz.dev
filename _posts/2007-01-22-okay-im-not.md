---
layout: post
title: "Okay, I'm not a fanboy after all"
microblog: false
guid: http://geewiz.micro.blog/2007/01/22/okay-im-not.html
date: 2007-01-22T21:29:47+0200
type: post
url: /2007/01/22/okay-im-not.html
---

<p>First, I don't think the iPhone is the best thing since sliced bread. Especially when it'll be closed for third party developers. That would be a really stupid move for Apple. At the moment, I'd rather add the Linux-based Nokia N800 internet tablet to my E61 if I wanted a multimedia and internet tablet with phone and internet connectivity. (And of course I do.)</p>
<p>And second, I'm still far from writing love songs to a computer.</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=zsqi2QHXaFI])</p>
