---
layout: post
title: "Sie lebt noch"
microblog: false
guid: http://geewiz.micro.blog/2005/03/19/sie-lebt-noch.html
date: 2005-03-19T07:30:44+0200
type: post
url: /2005/03/19/sie-lebt-noch.html
---

<p>Ich hatte mir schon Sorgen gemacht, aber heute hat sie sich gemeldet: Caro ist gut in Namibia angekommen. Mich hat sie zwar nicht erreicht (immerhin hat sich mein Telefon die Nummer gemerkt), aber ihren Eltern hat sie erz&auml;hlt, dass das Wetter gut ist und sie schon viele s&uuml;&szlig;e Kinder getroffen hat.</p>
<p>Mit dem Internet scheint dort leider nicht viel anzufangen zu sein, und telefonisch ist sie wohl auch nur schwer zu erreichen. Ich werde jeweils darauf warten m&uuml;ssen, dass sie mich erwischt.</p>
<p>Ich bin jedenfalls erst mal erleichtert, dass es ihr gut geht. Und 3 Tage von 3 Monaten sind schon vorbei. :)</p>
