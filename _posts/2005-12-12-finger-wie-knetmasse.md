---
layout: post
title: "Finger wie Knetmasse"
microblog: false
guid: http://geewiz.micro.blog/2005/12/12/finger-wie-knetmasse.html
date: 2005-12-12T22:19:20+0200
type: post
url: /2005/12/12/finger-wie-knetmasse.html
---

<p>Ein Studie f&uuml;r das amerikanische Department for Homeland Security ergab, dass 90% der getesteten Biometrie-Systeme mit ganz einfachen Tricks get&auml;uscht werden k&ouml;nnen. So scheint Knetmasse zum Beispiel ein <a href="http://www.yubanet.com/artman/publish/article_28878.shtml">ausreichend guter Ersatz</a> f&uuml;r den passenden Finger zu sein.</p>
<p>Hallo Ruckzuck, ich hoffe, eure Fingerabdruckscanner sind da etwas intelligenter!</p>
<p>(via <a href="http://it.slashdot.org/article.pl?sid=05/12/12/0557249">Slashdot</a>&amp;from=rss)</p>
