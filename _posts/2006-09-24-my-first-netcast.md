---
layout: post
title: "My first netcast: Radio Perl"
microblog: false
guid: http://geewiz.micro.blog/2006/09/24/my-first-netcast.html
date: 2006-09-24T06:28:01+0200
type: post
url: /2006/09/24/my-first-netcast.html
---

<p>Since I started listening to netcasts[1], they've been giving me a lot of fun as well as things to learn and think about. As always, it didn't take long until I wanted to give netcasting a try myself. Recording music and other audio content was one of the bigger reasons I bought the Mac, after all.</p>
<p>So, after a week of fiddling with GarageBand, I now proudly present to you:</p>
<p><a href="http://www.it-dojo.de/content/view/14/31/">Radio Perl, episode 1</a></p>
<p>It's a netcast in german language and directed towards Perl developers. I plan to pick one central topic that I'd like to talk about in every episode. Alternatively, I could invite someone else to do the talking here. So, if you'd like to be interviewed, contact me! Other fixed segments will be "Perl news" and "Interesting new Perl modules".</p>
<p>I'd like to make Radio Perl a weekly netcast, but only time will tell if I really can go at that pace.</p>
<p>At the same time, this is the premiere of <a href="http://www.it-dojo.de">IT-Dojo</a>, my new website for IT know-how and training.</p>
<p>Please tell me what you think about Radio Perl! You'll find the netcast's email address as well as a telephone number for audio comments on the page linked above.</p>
<p>fn1. Since "podcast" seems to suggest that you need an iPod to listen to it and since Apple is starting to make suspicious legal moves regarding the word, I prefer to call them "netcasts".</p>
