---
layout: post
title: "You like to be where you can laugh"
microblog: false
guid: http://geewiz.micro.blog/2006/11/07/you-like-to.html
date: 2006-11-07T19:10:01+0200
type: post
images:
- /uploads/laughing_girl.jpg
photos:
- /uploads/laughing_girl.jpg
url: /2006/11/07/you-like-to.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/laughing_girl.jpg" />
</div>
<p>This morning, the conductor not just took a short glance over people's tickets, but took the time to chat a bit with kids and intensively comparing the photo on my commuter card with the real thing: "Well... Hm... Okay. Hair two millimetres longer on the picture, but... Okay." He then complimented on the photo of the woman vis-a-vis and left us smiling. Having some fun on my early morning commute isn't that common, and didn't happen at all when I had been driving to work alone in my car. So this will unconsciously, but certainly, affirm my decision to take the train.</p>
<p>There are a lot situations that you're used to endure, but not to enjoy. It's where you think, "I may have to be here, but I'd rather be somewhere else." The daily commute is one such situation, and being at work is one of those, too. So if you want people to draw a connection between work and fun, put some fun in for them!</p>
<p>For example, I often do unexpected things that make people laugh. As a boss, I'm not expected to make public semi-rude comments about my team, so I do. Of course, I make it obvious that I'm joking. There are so many  ways of turning wrinkled brows into smiles. (Oh, here's the conductor again consulting the girl over there on keeping her teddy bear warm.) Noone likes brown-nosed people, so in management meetings (that most people outside our department probably consider boring) I blatantly confirm that I share my boss's opinion (and try not to get beaten up by my fellow leads). What happens is that there's more energy around the table for some time.</p>
<p>Put a smile on people's face. Brighten up their day. Help them feel they're at exactly the right place to be.</p>
