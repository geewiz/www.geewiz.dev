---
layout: post
title: "dotenv and direnv for better developer QoL"
microblog: false
guid: http://geewiz.micro.blog/2021/06/23/dotenv-and-direnv.html
date: 2021-06-23T13:46:12+0200
type: post
categories:
- "DevOps"
- "DEV"
tags: [devops]
url: /2021/06/23/dotenv-and-direnv.html
---
The latest [RubyTapas](https://www.rubytapas.com/) tutorial taught me the difference between the [`dotenv`](https://github.com/bkeepers/dotenv) and [`direnv`](https://direnv.net/) tools. While they have significant overlap, they complement each other quite nicely. `dotenv` simplifies both development and production ops by importing environments variables either from a `.env` file or the application hosting platform. `direnv` on the other hand augments the dev environment even further; not only can it add dev-only variables but also modify shell settings like extending `$PATH` on a per-project basis.
