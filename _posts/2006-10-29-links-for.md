---
layout: post
title: "links for 2006-10-28"
microblog: false
guid: http://geewiz.micro.blog/2006/10/29/links-for.html
date: 2006-10-29T08:18:34+0200
type: post
url: /2006/10/29/links-for.html
---

<ul class="delicious">
<li>
<div class="delicious-link"><a href="http://praegnanz.de/essays/freie-schriften-im-mini-portrait">Freie Schriften im Mini-Portrait</a></div>
<div class="delicious-extended">Showcase of some free fonts (in german)</div>
<div class="delicious-tags">(tags: <a href="http://del.icio.us/Geewiz/free">free</a> <a href="http://del.icio.us/Geewiz/font">font</a> <a href="http://del.icio.us/Geewiz/typography">typography</a> <a href="http://del.icio.us/Geewiz/german">german</a> <a href="http://del.icio.us/Geewiz/design">design</a>)</div>
</li>
<li>
<div class="delicious-link"><a href="http://www.idav-freiburg.de/index.htm">IDAV</a></div>
<div class="delicious-extended">Interkultureller Deutsch-Afrikanischer Verein Freiburg</div>
<div class="delicious-tags">(tags: <a href="http://del.icio.us/Geewiz/africa">africa</a> <a href="http://del.icio.us/Geewiz/culture">culture</a> <a href="http://del.icio.us/Geewiz/freiburg">freiburg</a>)</div>
</li>
</ul>
