---
layout: post
title: "Tetris f&uuml;r Anf&auml;nger"
microblog: false
guid: http://geewiz.micro.blog/2004/11/05/tetris-fuumlr-anfaumlnger.html
date: 2004-11-05T05:04:00+0200
type: post
url: /2004/11/05/tetris-fuumlr-anfaumlnger.html
---

<p>Tetris wird f&uuml;r immer die Liste der Kultspiele beherrschen, und die Chefs der eckigen Bl&ouml;cke spielen sogar die 3D-Variante.</p>
<p>Nun gibt es auch f&uuml;r die weniger Kombinationsbegabten eine Version als Onlinespiel: <a href="http://www.tetris1d.org/tetris.php">Tetris1D</a></p>
<p>(Danke an <a href="http://blog.koehntopp.de/">Isotopp</a>)</p>
