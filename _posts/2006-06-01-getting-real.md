---
layout: post
title: "Getting Real"
microblog: false
guid: http://geewiz.micro.blog/2006/06/01/getting-real.html
date: 2006-06-01T05:55:13+0200
type: post
url: /2006/06/01/getting-real.html
---

<p>Als ich k&uuml;rzlich in einer E-Mail eines Kollegen las "Wir m&uuml;ssen uns sehr bald anstrengen und den Mut entwickeln, Features wegzulassen, um schlank zu werden.", da klingelte es in meinem Kopf: "There's someone Getting Real!"</p>
<p>Wenn man wie ich ein Fan des kleinen Erfolgsunternehmens <a href="http://www.37signals.com">37signals</a> ist und Webanwendungen entwickelt, dann kommt man kaum umhin, das Buch <a href="http://gettingreal.37signals.com">Getting Real</a> zu kaufen und zu lesen. <a href="http://backpackit.com/?referrer=BPBMZZ9">Getting Real" gibt die Philosophie und Erfahrungen von 37signals wieder, die sich mit Webanwendungen wie "Backpack</a> und <a href="http://www.basecamphq.com/?referrer=geewiz">Basecamp</a> erfolgreich als ASP etabliert haben und im Fr&uuml;hjahr 2006 sogar auf dem Titelblatt des Forbes-Magazins erschienen.<br />
Die Philosophie des Unternehmens kann man auf den Nenner "Weniger ist mehr" bringen, und 37signals setzt sie in allen Phasen der (Weiter-) Entwicklung und des Betriebs ihrer Applikationen konsequent um.</p>
<p>In "Getting Real" geht es darum, wie man Webanwendungen sinnvoll konzipiert, agil entwickelt, erfolgreich auf den Markt bringt und dort h&auml;lt -- und das alles ohne unn&ouml;tigen Aufwand. Auch auf Fragestellungen zu Personal, Finanzierung und Marketing wird ausf&uuml;hrlich eingegangen.</p>
<p>"Getting Real" ist nicht im Buchhandel erh&auml;ltlich: 37signals entschied sich daf&uuml;r, es ausschlie&szlig;lich als E-Book im PDF-Format zu einem Preis von $19 zu vertreiben. Als Kopierschutz wird jedes Exemplar auf den K&auml;ufer personalisiert.</p>
<p>Das Buch gliedert sich in folgende Kapitel, die jeweils wiederum aus f&uuml;nf bis zehn Abschnitten bestehen:</p>
<ul>
<li>Introduction</li>
<li>The Starting Line</li>
<li>Stay Lean</li>
<li>Priorities</li>
<li>Feature Selection</li>
<li>Process</li>
<li>The Organization</li>
<li>Staffing</li>
<li>Interface Design</li>
<li>Code</li>
<li>Words</li>
<li>Pricing and Signup</li>
<li>Promotion</li>
<li>Support</li>
<li>Post-Launch</li>
<li>Conclusion</li>
</ul>
<p>Einige dieser Kapitel k&ouml;nnen auch kostenlos von der <a href="http://gettingreal.37signals.com">Website des Buchs</a> heruntergeladen werden. Hier findet man auch die Zitate vieler weiterer begeisterter Leser.</p>
<p>Die Gliederung in kurze Abschnitte und der lockere Schreibstil, erg&auml;nzt durch passende Zitate, machen das Buch einfach zu lesen, zu verstehen und zu verinnerlichen.</p>
<p>Jeder, der an der Konzeption und Entwicklung von Webanwendungen arbeitet, kann aus "Getting Real" wertvolle Denkanst&ouml;&szlig;e beziehen. Und selbst ausserhalb des WWW-Kontexts kann man die Grunds&auml;tze des Buchs anwenden, denn die Konzentration auf das Wesentliche und der Verzicht auf unn&ouml;tigen Ballast sind auch im sonstigen Leben (sofern vorhanden :)) sinnvolle Ans&auml;tze.</p>
