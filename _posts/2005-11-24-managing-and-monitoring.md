---
layout: post
title: "Managing and Monitoring JBoss"
microblog: false
guid: http://geewiz.micro.blog/2005/11/24/managing-and-monitoring.html
date: 2005-11-24T09:18:57+0200
type: post
url: /2005/11/24/managing-and-monitoring.html
---

<p>Ich sag ja immer: <a href="http://www.onjava.com/pub/a/onjava/excerpt/jboss_dev_notebook_chap8/index.html?CMP=OTC-FP2116136014">Timing ist alles". Gerade in den letzten Wochen besch&auml;ftige ich mich mit Administrationsfragen zu J2EE-Diensten und just ver&ouml;ffentlicht O'Reilly ein passendes "Beispielkapitel</a>&amp;ATT=Managing+and+Monitoring+JBoss+Part+1 aus dem neuen Buch "JBoss: A Developer's Notebook".</p>
<p>&Uuml;ber Amazon bin ich dann noch auf Heiko W. Rupps Buch <a href="http://www.amazon.de/exec/obidos/ASIN/3898643182/geewizweblog-21">JBoss</a> gesto&szlig;en und hab's mir gleich bestellt.</p>
