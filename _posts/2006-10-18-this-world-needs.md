---
layout: post
title: "This world needs more hugs"
microblog: false
guid: http://geewiz.micro.blog/2006/10/18/this-world-needs.html
date: 2006-10-18T22:42:31+0200
type: post
url: /2006/10/18/this-world-needs.html
---

<p>YouTube is more than a billion dollar acquisition, and it certainly is more than a tool of illegally distributing copyrighted movies. For example, it makes available many inspirational movies that I wouldn't have seen otherwise. Movies like <a href="http://www.youtube.com/watch?v=dn0AVpGNhOs.">Free Hugs</a></p>
<p>It's a video of Australian Juan Mann offering free hugs in a Sydney street mall. It's interesting and touching to see how different the people around him react to his simple offer.<br />
[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=dn0AVpGNhOs])</p>
<p>news.com.au says it well in its column <a href="http://www.news.com.au/story/0">Free hugs priceless in a culture of violence</a>,23599,20522769-5007146,00.html:</p>
<blockquote class="posterous_medium_quote">
<p>We live in a world where violence and violent acts are becoming normalised. The producers of the new James Bond film see fit to cut the superspy smoking a cigar but not images of him killing people with a smoking gun. We are shocked and appalled at sexual imagery and intimacy (Margaret Whitlam's comments about Janette and John Howard holding hands, for example), but take death, pain and the destruction of human life in our stride.<br />
  [...] But just as you begin to despair, along comes a young man with a big heart and YouTube, technology that spreads goodwill like a virus and gives us what we need in these dark times  a hug and a smile.</p>
</blockquote>
<p>I guess I'd hesitate myself when confronted with such an offer. What have we become? We're talking such much about love and goodwill to all humankind (especially in church), but hesitate to return a simple hug?</p>
