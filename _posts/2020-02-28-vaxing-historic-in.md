---
layout: post
title: "VAX on, VAX off"
microblog: false
guid: http://geewiz.micro.blog/2020/02/28/vaxing-historic-in.html
date: 2020-02-28T17:02:00+0200
type: post
url: /2020/02/28/vaxing-historic-in.html
---
In “geewiz explores computer history” news, I’m now an official member of DECUS, the Digital Equipment Corporation User Society! The reason is that HP (who bought Compaq who bought DEC) issue a Hobbyist License to DECUS members who’d like to install OpenVMS for funsies. And I have to admit, working with an old-school operating system that isn’t unixoid has been a great experience so far. I’ll do a live installation of OpenVMS on my [Twitch stream](https://www.twitch.tv/fullstacklive) tomorrow.
