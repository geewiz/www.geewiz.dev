---
layout: post
title: "Disoriented sysadmin obliterates 700GB of customer data"
microblog: false
guid: http://geewiz.micro.blog/2006/07/18/disoriented-sysadmin-obliterates.html
date: 2006-07-19T01:15:48+0200
type: post
url: /2006/07/18/disoriented-sysadmin-obliterates.html
---

<p>Everytime I discover that I just made quite a blunder, there comes that hot feeling rushing fast from my stomach up to my head. But I guess that's only a fraction of how that poor bloke at Plusnet must have felt when he realised that the storage system he just reconfigured (sending all data to nirvana in the process) wasn't the backup system but actually <a href="http://www.theregister.co.uk/2006/07/11/plusnet_email_fiasco/.">the productive one</a></p>
<p>That's a situation where I would consider taking that katana sword off the wall...</p>
