---
layout: post
title: "Forum der Tastenfreunde"
microblog: false
guid: http://geewiz.micro.blog/2005/08/17/forum-der-tastenfreunde.html
date: 2005-08-18T00:50:15+0200
type: post
url: /2005/08/17/forum-der-tastenfreunde.html
---

<p>Weil es offenbar nicht allzuviele Anlaufstellen f&uuml;r Organisten und Pianisten im Web gibt, habe ich mir 10 Minuten Zeit genommen, um <a href="http://www.tastenforum.info">Tastenforum.info</a> online zu stellen. Die Tastenk&uuml;nstler und Keyboarderinnen unter euch sind herzlich eingeladen, sich zu beteiligen!</p>
