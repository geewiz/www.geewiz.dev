---
layout: post
title: "Germany isn't a very happy place"
microblog: false
guid: http://geewiz.micro.blog/2007/02/19/germany-isnt-a.html
date: 2007-02-19T02:06:46+0200
type: post
url: /2007/02/19/germany-isnt-a.html
---

<p>ScienceDaily reports that a pychologist from the University of Leicester published the first <a href="http://www.sciencedaily.com/releases/2006/11/061113093726.htm.">world map of happiness</a></p>
<blockquote class="posterous_medium_quote">
<p>The meta-analysis is based on the findings of over 100 different studies around the world, which questioned 80,000 people worldwide. For this study data has also been analysed in relation to health, wealth and access to education.</p>
</blockquote>
<p>Among the top 10 of the happiest countries are many scandinavian countries. Other than the fact that the end of the list consists of african countries, this suprises me because countries like Finland are said to have a lot of depressed or even suicidal people due to the lack of sunlight over great parts of the year.</p>
<p>With Austria coming in on third place, there's also a german-speaking country with many happy people. But Germany itself only ranks no. 35, miles behind Ireland and many of the other european countries. So, if living in Germany makes you unhappy, there are 34 choices where you could be better off...</p>
<p>(via <a href="http://blog.guykawasaki.com/2007/02/the_world_map_o.html">How to change the world</a>)</p>
