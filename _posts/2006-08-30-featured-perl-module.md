---
layout: post
title: "Featured Perl module: Net::FeedBurner"
microblog: false
guid: http://geewiz.micro.blog/2006/08/30/featured-perl-module.html
date: 2006-08-30T20:14:08+0200
type: post
url: /2006/08/30/featured-perl-module.html
---

<p>One reason for the success of Web 2.0 services is that they provide APIs that enable developers to use them from other applications. You just need an interface module, library or package[1] and yet you can easily call the service's functions from your own program.</p>
<p><a href="http://search.cpan.org/">Net::FeedBurner</a>~sock/Net-FeedBurner-0.10/lib/Net/FeedBurner.pm, for example, makes it possible to access and modify the RSS feeds you registered with <a href="http://www.feedburner.com:">Feedburner</a></p>
<div class="CodeRay">
<div class="code">
<pre>use Net::FeedBurner;
my $fb = Net::FeedBurner->new('user' => $user, 'password' => $password);
my $feeds = $fb->find_feeds();
my $feed_id = (keys %{$feeds})[0];
my $feedinfo = $fb->get_feed($feed_id);</pre>
</div>
</div>
<p>fn1. In most cases, you also need a registered user account.</p>
