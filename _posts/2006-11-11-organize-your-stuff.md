---
layout: post
title: "Organize your stuff with playlists"
microblog: false
guid: http://geewiz.micro.blog/2006/11/11/organize-your-stuff.html
date: 2006-11-11T07:40:00+0200
type: post
url: /2006/11/11/organize-your-stuff.html
---

<p>Today, I discovered two completely different areas where playlists can bring both order and diversity into your stuff:</p>
<p>First, there's music collections. I guess that's what you thought of when you read <a href="http://www.43folders.com/2006/11/09/music-only-playlists/.">playlists". With playlists, you can divide your music into different categories, genres, moods and so on. What I really got to appreciate are iTunes' "Intelligent Playlists". Those playlists are not simple static folders you drag and drop your songs into, but dynamic collections of music meeting certain user-defined criteria. If you want to see some examples how you can get the maximum out of those music organization tools, read about "Merlin Mann's playlists</a></p>
<p>There's another kind of collection that can also be organized with playlists: From now on, you can make <a href="http://www.bloglines.com/about/news">playlists of your Bloglines RSS feeds</a>#128. But I'll write about that soon in another article.</p>
