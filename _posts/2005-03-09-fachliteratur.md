---
layout: post
title: "Fachliteratur"
microblog: false
guid: http://geewiz.micro.blog/2005/03/09/fachliteratur.html
date: 2005-03-09T23:51:51+0200
type: post
images:
- /uploads/chili_con_carne.serendipityThumb.png
photos:
url: /2005/03/09/fachliteratur.html
---

<p>Ich wurde heute noch mal auf die Knoblauchso&szlig;e vom Whiskybrunch am Sonntag angesprochen. Ich antwortete: "Scharf? Iso hat das Buch &uuml;ber Scharf geschrieben!" In dem Moment kam mir der Gedanke "Hm. Wie w&uuml;rde dieses Buch denn eigentlich aussehen?"Kurz nachgedacht, GIMP gestartet, et voil&aacute;: das neue Buch zum Thema "Verbrennungen zweiten Grades in der Speiser&ouml;hre".</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/chili_con_carne.serendipityThumb.png" />
</div></p>
