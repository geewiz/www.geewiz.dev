---
layout: post
title: "Recommendation: Logitech Mini Boombox"
microblog: false
guid: http://geewiz.micro.blog/2012/06/17/recommendation-logitech-mini.html
date: 2012-06-17T22:42:14+0200
type: post
url: /2012/06/17/recommendation-logitech-mini.html
---
<p>Most of the time, I get my iPhone&#8217;s audio delivered to my ears by a pair of Etymotic hf3 headphones. There are a few situations, though, when I need a speaker. For example, when I take a power nap &#8211; since I can&#8217;t sleep lying on my back, the earbuds would start to hurt quickly. Or when I&#8217;m under the shower where the need for cordless operation is obvious.</p>
<p>Until recently, I&#8217;ve put up with the iPhone&#8217;s built-in speaker, but that&#8217;s a revelation neither in terms of volume nor of sound. Then I read about the <a href="http://p.jochen-lillich.de/amazon/B005TPEXTK">Logitech Mini Boombox</a> that got an ever better review than its bigger sibling, the &#8220;Logitech Boombox&#8221;. </p>
<p>So I invested about 60&euro; and have been very happy with it ever since. The speaker is easy to pair with all kinds of audio sources via Bluetooth, has practical touch sensors to change volume, start/stop the audio source and skip tracks. For its size, it has enough punch and still is small enough to fit into my hand or even my travel luggage. Since it&#8217;s powered by a battery that can be charged via a standard USB charger, the Mini Boombox is highly mobile.</p>
<p>Conclusion: If you need a small, simple Bluetooth speaker, try the <a href="http://p.jochen-lillich.de/amazon/B005TPEXTK">Logitech Mini Boombox</a>!</p>
