---
layout: post
title: "A remote access alternative to ngrok"
date: 2022-04-24 18:25 +0100
description:
image:
category:
tags: [devops]
published: true
sitemap: true
---

Following up on my recent [post on ngrok][1], I found an interesting alternative 
via [@shakalandy's newsletter][2]. [Bore][3] presents itself as "a modern, 
simple TCP tunnel that exposes local ports to a remote server, bypassing 
standard NAT connection firewalls."

Apart from its simplicity, what makes `bore` interesting is that its connection 
server can be self-hosted. That way, you're in full control of who can create or 
access tunnels on your private `bore` instance. You can even require 
authentication using a pre-shared key.

And of course, just like seemingly all nifty command line tools these days, 
`bore` is written in Rust.

[1]: /2022/04/12/remote-administration-ngrok.html
[2]: https://allesnurgecloud.com/newsletter/allesnurgecloud-60-opensource-als-geschaeftsmodell-9-5-verschwindet-bash-bug-bei-cloudflare-und-mehr/#bore_-_localhost_tunnels_leicht_gemacht
[3]: https://github.com/ekzhang/bore
