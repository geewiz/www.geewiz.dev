---
layout: post
title: "Farewell, floppy disk!"
microblog: false
guid: http://geewiz.micro.blog/2007/02/08/farewell-floppy-disk.html
date: 2007-02-08T08:26:07+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm1static_eglga-scaled500.jpg?w=240
photos:
- /assets/wp-content/2011/05/media_httpfarm1static_eglga-scaled500.jpg?w=240
url: /2007/02/08/farewell-floppy-disk.html
---

<p>German news service Tagesschau <a href="http://www.tagesschau.de/aktuell/meldungen/0">sings the swan song</a>,,OID6385352_REF1,00.html about a storage medium I grew up with: the floppy disk. The article describes how the floppy gradually got replaced by ZIP disks, CD ROMs, DVDs and USB sticks. Today, computer manufacturers don't bother with installing a floppy drive any more.</p>
<p>But the author not only knows about the reasons of the floppy vanishing but also about its emotional consequences:</p>
<blockquote class="posterous_medium_quote">
<p>It's time to finally say good bye to the floppy disk. But whoever saw the moist eyes of PC nostalgics when talking about the 5 1/4 inch disk, the bigger, somewhat floppier predecessor of the smaller quadratic disks, knows: that won't be easy for some of the hardcore hackers.</p>
</blockquote>
<p>Oh yes, it's hard. Here's a picture of my first 5,25" floppy disk:</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm1static_eglga" height="180" src="/assets/wp-content/2011/05/media_httpfarm1static_eglga-scaled500.jpg?w=240" width="240" />
</div></p>
<p>It came empty with my Commodore C128, so it's 20 years old now. The label says "128: The First" and was written with a typewriter.</p>
<p>Today, it's hanging on the wall at my desk at home. Above a Mac Mini that will never know what a floppy disk is. Or was.</p>
