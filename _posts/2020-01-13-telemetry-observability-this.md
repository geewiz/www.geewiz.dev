---
layout: post
title: "Telemetry != Observability"
microblog: false
guid: http://geewiz.micro.blog/2020/01/13/telemetry-observability-this.html
date: 2020-01-13T10:42:00+0200
type: post
categories:
- "DevOps"
tags: [devops]
url: /2020/01/13/telemetry-observability-this.html
---
This post helps to understand the difference: [Understanding Observability](https://sdarchitect.blog/2020/01/08/understanding-observability/).
