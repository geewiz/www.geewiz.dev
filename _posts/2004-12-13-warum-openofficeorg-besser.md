---
layout: post
title: "Warum OpenOffice.org besser ist"
microblog: false
guid: http://geewiz.micro.blog/2004/12/13/warum-openofficeorg-besser.html
date: 2004-12-14T01:16:00+0200
type: post
url: /2004/12/13/warum-openofficeorg-besser.html
---

<p>In einem <a href="http://www.pc-tools.net/comment/openoffice/">Kommentar</a> auf PC-Tools.net nimmt Jem Berkes einen Vergleich zwischen der freien Office-Suite und dem propriet&auml;ren "Standard" vor.</p>
<p>Berkes kommt zu dem Schluss, dass OpenOffice.org nicht nur ausreicht, um die t&auml;gliche Arbeit zu erledigen, sondern sogar einige Vorteile gegen&uuml;ber MS Office bringt:</p>
<ul>
<li>
<p>OOo l&auml;uft auf verschiedenen Plattformen (er nennt Windows, Linux, Solaris, FreeBSD, Mac OS X; IRIX geh&ouml;rt meines Wissens auch dazu). So kann auf verschiedenen Rechnern mit den gleichen Dokumenten gearbeitet werden.</p>
</li>
<li>
<p>OOo ist stabil und l&auml;uft sauber. Berkes stellt dem einige Schrecken durch das wackelige Verhalten von MS Office gegen&uuml;ber.</p>
</li>
<li>
<p>Berkes hat Vertrauen zu OpenOffice.org und dessen Entwicklern. Best&auml;rkt wird er noch durch die M&ouml;glichkeit, selbst zum Projekt beitragen zu k&ouml;nnen. Bei Open Source Software ist kein &Auml;rger durch verfallene Lizenzen, verlorene Freischaltungen oder erzwungene Updates zu erwarten.</p>
</li>
<li>
<p>Das Thema "Langlebige Daten" ist wichtig, wird aber oft &uuml;bersehen, weil es sich erst in der Zukunft bemerkbar macht Das offene, XML-basierte Dateiformat von OpenOffice.org macht den Inhalt der Dokumente sowohl f&uuml;r den Anwender als auch f&uuml;r die Entwickler von Sekund&auml;rsoftware leicht zug&auml;nglich.</p>
</li>
<li>
<p>In die gleiche Kerbe schl&auml;gt der Punkt <a href="http://www.oasis-open.org/.">Datenaustausch". Wo Microsoft sich str&auml;ubt, den Datenfluss von und zu anderer Software zu unterst&uuml;tzen, f&ouml;rdert ihn OOo durch sein Design nach der "OASIS-Spezifikation</a></p>
</li>
</ul>
<p>Berkes' Fazit: "Software-Unternehmen sterben -- Daten nicht. Wenn ein Unternehmen das Geheimnis zum Zugriff auf Ihre Daten mit ins Grab nimmt, wo bleiben Sie dann?"</p>
