---
layout: post
title: "dbox2 assimiliert"
microblog: false
guid: http://geewiz.micro.blog/2004/12/06/dbox-assimiliert.html
date: 2004-12-06T23:08:00+0200
type: post
url: /2004/12/06/dbox-assimiliert.html
---

<p>Am vergangenen Montag haben wir ja abends im B&uuml;ro zwei Premiere dbox2 mit Neutrino geflasht -- eine davon war meine.</p>
<p>Am Wochenende gelang es Tom und mir, sie so zum Laufen zu bringen, dass sie sowohl die freien Fersehprogramme als auch PREMIERE "Start" und "Sport" auf den Schirm zauberte.</p>
<p>Dass ich "Sport" sofort k&uuml;ndigen werde, war ja von vornherein klar. Selbst wenn da t&auml;glich Kampfsport &uuml;bertragen w&uuml;rde, w&auml;re es mir die immensen Geb&uuml;hren nicht wert. Aber auch "Start" ist nur ein Mischmasch der anderen PREMIERE-Programme und nur wenig davon spricht mich an. So habe ich auch gleich ein Fax an den Kundenservice geschickt, in dem ich beides gek&uuml;ndigt habe.</p>
<p>Was bleibt, reicht mir immer noch v&ouml;llig, n&auml;mlich eine Settop-Box, mit der ich hochkomfortabel &uuml;bers Netzwerk Sendungen mitschneiden bzw. (genauso wie DVDs oder MP3s) wieder abspielen kann.</p>
