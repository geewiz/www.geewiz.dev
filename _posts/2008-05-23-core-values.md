---
layout: post
title: "Core values"
microblog: false
guid: http://geewiz.micro.blog/2008/05/23/core-values.html
date: 2008-05-23T22:36:50+0200
type: post
url: /2008/05/23/core-values.html
---

<p>Recruiters are used to briefings where the head of HR hands them a carefully crafted job profile, explains the benfits and pension plans and finishes by pointing out the salary perspective.</p>
<p>But sometimes, the briefing gets a different angle when you exchange the head of HR with leaders that know what's really necessary to get the job done.</p>
<p>This obviously happens very rarely, judging from df5jt's <a href="http://df5jt.over-blog.com/article-19805738.html">excited blog entry</a>  (article in german).</p>
<p>Let's see if my teams will benefit from that very briefing.</p>
