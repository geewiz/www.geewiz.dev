---
layout: post
title: "Automating my desktop with Hammerspoon"
microblog: false
guid: http://geewiz.micro.blog/2021/02/05/automating-my-desktop.html
date: 2021-02-05T18:51:07+0200
type: post
url: /2021/02/05/automating-my-desktop.html
---
After my unsuccessful attempt at adopting a tiling window manager, I was looking for a more flexible way to arrange my windows efficiently on macOS.

In the past, I had tried dedicated apps like [Mosaic](https://www.lightpillar.com/mosaic.html) or more generic ones like [Keyboard Maestro](http://www.keyboardmaestro.com/main/) and [BetterTouchTool](https://folivora.ai/) to assign window movements to hotkeys. The problem with this approach is that you're going to run out of key combinations quickly even if you set up a special Hyper key.

A good way to avoid this issue is a modal approach where a single key combo switches you to window management mode. In this context, you then have the full keyboard available for all kinds of stuff. I had forgotten that [Moom](https://manytricks.com/moom/) actually works this way, so I decided to finally give [Hammerspoon](https://www.hammerspoon.org/) a try. Up until that point, I was afraid that coding your own desktop automation in LUA would be too deep of a rabbit hole. But it's actually not too bad. You can find the results of only a few hours of procrastination in my [dotfiles repository](https://gitlab.com/geewiz/dotfiles/-/tree/main/hammerspoon).

Another fear of mine was that an open-source tool that interacts with the macOS GUI as closely as Hammerspoon would be prone to be broken by Apple every other week. I'm positively surprised that this doesn't seem to be the case. Its authors seem to handle even OS upgrades very well.

This makes window management pretty much a solved problem for me. Fortunately, Hammerspoon offers many more possibilities to spend my time not getting things done...
