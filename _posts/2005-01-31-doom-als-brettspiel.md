---
layout: post
title: "DOOM als Brettspiel"
microblog: false
guid: http://geewiz.micro.blog/2005/01/31/doom-als-brettspiel.html
date: 2005-01-31T22:03:00+0200
type: post
url: /2005/01/31/doom-als-brettspiel.html
---

<p>Der Klassiker unter den Ballerspielen ist nicht tot zu kriegen: Fantasy Flight Games hat DOOM jetzt als <a href="http://www.fantasyflightgames.com/doomproducts.html">Brettspiel</a> ver&ouml;ffentlicht.</p>
<p>Es ist f&uuml;r 2 bis 4 Spieler ausgelegt, die Spieldauer liegt bei etwa 1 bis 2 Stunden. Als Spielfiguren tauchen viele alte Bekannte wieder auf, vom Imp &uuml;ber den Hell Knight bis hin zum Cyberdemon.</p>
<p>Ich war schon immer ein DOOM-Fan, und wenn das Brettspiel auch hier verf&uuml;gbar werden sollte, muss ich nicht mal ne neue Grafikkarte kaufen. :)</p>
