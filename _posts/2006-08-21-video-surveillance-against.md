---
layout: post
title: "Video surveillance against terror"
microblog: false
guid: http://geewiz.micro.blog/2006/08/21/video-surveillance-against.html
date: 2006-08-21T21:40:49+0200
type: post
url: /2006/08/21/video-surveillance-against.html
---

<p>I found a more serious argument about the dubious value of video surveillance in the prevention of terrorist actions  on <a href="http://rabe.supersized.org/archives/784-Inszenierungen-und-Ungereimtheiten-aus-Kiel.html:">rabenhorst</a></p>
<blockquote class="posterous_medium_quote">
<p>Weder das BKA, noch die Generalbundesanw&auml;ltin oder das Bundesinnenministerium haben bis heute schl&uuml;ssig dargelegt, wie die Verbindung zwischen den Aufnahmen vom 31. Juli &uuml;ber die Ver&ouml;ffentlichung am 18. August und der fast augenblicklichen Ergreifung in Kiel zustandekam, insbesondere, ob das Wiedererkennen einer Videoaufnahme der entscheidende Hinweis war und wenn, von wem dieser Hinweis kam.</p>
</blockquote>
