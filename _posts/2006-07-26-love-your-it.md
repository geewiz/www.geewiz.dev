---
layout: post
title: "Love your IT crowd"
microblog: false
guid: http://geewiz.micro.blog/2006/07/26/love-your-it.html
date: 2006-07-26T18:58:00+0200
type: post
url: /2006/07/26/love-your-it.html
---

<p>Did you already do some work on your PC today? Checked your electronic mailbox? Surfed the Web? And everything worked? Maybe you just got some reports that your corporate website is running smoothly and page views are growing and growing?</p>
<p>Well, then maybe it's time that you remember who makes all this possible -- the people working in the background running network cables, installing software, doing upgrades to your hardware and explaining in a soft voice that your printer will function just fine if you actually switch it on.</p>
<p>Yes, I mean those scruffy people in the basement. The sysadmins.</p>
<p>Friday July 28th is <a href="http://www.sysadminday.com/.">System Administrator Appreciation Day</a> Think about something nice and give them some love for them sacrificing their spare time and mental sanity for you.</p>
