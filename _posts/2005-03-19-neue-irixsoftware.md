---
layout: post
title: "Neue IRIX-Software"
microblog: false
guid: http://geewiz.micro.blog/2005/03/19/neue-irixsoftware.html
date: 2005-03-19T08:44:41+0200
type: post
url: /2005/03/19/neue-irixsoftware.html
---

<p>Neue Software auf Nekochan: zwei MP3-Player und ein Webserver.</p>
<ul>
<li><a href="http://www.nekochan.net/downloads/Nekoware/beta/neko_mpg123-pre0.59s.tardist">neko_mpg123-pre0.59s.tardist</a></li>
<li><a href="http://www.nekochan.net/downloads/Nekoware/beta/neko_gqmpeg-0.20.0.tardist">neko_gqmpeg-0.20.0.tardist</a></li>
<li><a href="http://www.nekochan.net/downloads/Nekoware/beta/neko_lighttpd-1.3.13.tardist">neko_lighttpd-1.3.13.tardist</a></li>
</ul>
