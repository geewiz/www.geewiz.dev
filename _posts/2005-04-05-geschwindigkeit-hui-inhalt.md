---
layout: post
title: "Geschwindigkeit hui, Inhalt pfui"
microblog: false
guid: http://geewiz.micro.blog/2005/04/05/geschwindigkeit-hui-inhalt.html
date: 2005-04-05T05:06:37+0200
type: post
url: /2005/04/05/geschwindigkeit-hui-inhalt.html
---

<p>Der Spiegel Online-Artikel <a href="http://www.spiegel.de/unispiegel/jobundberuf/0">Sie haben Post - mit Fehlern</a>,1518,348729,00.html beschreibt, dass Bewerber f&uuml;r den Erstkontakt zu Unternehmen zwar zunehmend das Internet nutzen, dabei aber h&auml;ufig die grundlegenden Regeln vergessen: "Jede f&uuml;nfte Bewerbung weist formale oder Rechtschreibfehler auf, jede zweite ist unvollst&auml;ndig."Ich kann das best&auml;tigen. Derzeit bekomme ich jede Woche ein, zwei Bewerbungen zur Durchsicht. In den meisten F&auml;llen handelt es sich um E-Mails, und das ist bei einem Internet-Unternehmen auch v&ouml;llig okay. Manchmal bin ich aber &uuml;ber das Niveau der Texte regelrecht best&uuml;rzt. Welcher Bewerber glaubt denn ernsthaft, dass man ihm einen HighTech-Beruf zutraut, wenn er nicht einmal eine korrekte Anrede hinbekommt?</p>
<p>Auch das im Artikel erw&auml;hnte Ph&auml;nomen "Massenbewerbungen" ist mir bekannt. Da k&uuml;ndigte mir k&uuml;rzlich ein Bewerber an, dass er Anfang M&auml;rz eine erg&auml;nzende Schulung besuchen wird. Meint er etwa M&auml;rz 2006?</p>
<p>Auf Spiegel Online sind auch die Ergebnisse einer Studie zu lesen, der zufolge mehr als die H&auml;lfte der Unternehmen maximal 4 Minuten f&uuml;r die erste Durchsicht einer Bewerbung aufwenden. In diesen 4 Minuten entscheidet sich, wie mit der Bewerbung weiter verfahren wird. Hier gilt also definitiv: der erste Eindruck ist ein entscheidender.</p>
<p>Online-Bewerbungen sind schnell, unkompliziert und preisg&uuml;nstig. Und gibt man sich bei ihrer Erstellung keine M&uuml;he, sind sie auch vergeblich.</p>
