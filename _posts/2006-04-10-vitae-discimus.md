---
layout: post
title: "Vitae discimus"
microblog: false
guid: http://geewiz.micro.blog/2006/04/10/vitae-discimus.html
date: 2006-04-10T22:19:46+0200
type: post
url: /2006/04/10/vitae-discimus.html
---

<p>Dass es auch anders geht als in der seit 2 Wochen diskutierten <a href="http://blog.jochen-lillich.de/archives/320-Class-of-1984.html">Horror-Hauptschule</a>, zeigt eine <a href="http://www.spiegel.de/unispiegel/schule/0">Hauptschule in Duisburg</a>,1518,410105,00.html, die sogar zur sozialsten Schule Deutschlands gek&uuml;rt wurde. Wenngleich das soziale Milieu nicht mit dem in Berlin gleichzusetzen ist, kann man gut erkennen, was P&auml;dagogen meines Erachtens zus&auml;tzlich zum Schulwissen vermitteln m&uuml;ssen: Respekt voreinander, klare Regeln sowie Grenzen im Miteinander und Konsequenzen bei deren &Uuml;bertretung.</p>
