---
layout: post
title: "Blogstille"
microblog: false
guid: http://geewiz.micro.blog/2005/05/09/blogstille.html
date: 2005-05-09T05:17:28+0200
type: post
url: /2005/05/09/blogstille.html
---

<p>Es ist wirklich still geworden hier im Blog, und wer von euch mich schon un&uuml;blich lange nicht mehr gesehen hat oder noch auf eine Antwort auf seine E-Mail wartet, hat es vielleicht schon erraten: ich war in den letzten Wochen komplett &uuml;berlastet. Im Gesch&auml;ft muss ich derzeit ein Gro&szlig;projekt organisieren, in der CAJ k&auml;mpfen wir mit der Hauptamtlichenkrise und den letzten Rest Freizeit muss ich wirklich zur Erholung nutzen.</p>
<p>Hinzu kam noch -- und ich sehe da durchaus Zusammenh&auml;nge -- eine Erk&auml;ltung, wegen der ich dieses ganze Wochenende zuhause verbracht habe. Inzwischen gehts mir wieder einigerma&szlig;en gut, und erholt habe ich mich auf jeden Fall. Jetzt mache ich mich dran, eure Mails zu beantworten und hoffe, dass die kommende Woche wieder normaler aussieht.</p>
