---
layout: post
title: "Tactics against burnout"
microblog: false
guid: http://geewiz.micro.blog/2013/05/29/tactics-against-burnout.html
date: 2013-05-29T10:52:38+0200
type: post
url: /2013/05/29/tactics-against-burnout.html
---
<p>Being an entrepreneur in the tech space means working 60 to 80 hours a week and hustling from one opportunity to the next. That's what many people think. That's what many entrepreneurs in the tech space think. It's what I thought, too, when I started freistil IT in 2010. The temperature started rising. It felt like a fever. And I mean that in the literal sense.</p>
<p>One busy day, my body started heating up and I started to feel weary and devoid of energy. It felt similar to a flu, but I had no other flu symptoms on top of the 40 degrees. I remembered that I had experienced this before. Back then, I went to a doctor and had blood samples taken. No conclusive results at all. Now that it happened again, I started to recognize a pattern: This was how my body alerted me that I was hitting my limits. So I dropped what ever I was sweating about, went straight to bed and switched to private mode completely. No email, no phone calls, no pondering business issues. Soon, the fever vanished and I slowly got into business again, carefully ramping up my workload. Since then, it never happened again because I've become much more aware of what drains me of energy and motivation, and because I learned how to replenish my mental fuel.</p>
<p>Andrew Dumont describes his experience with this issue in his blog entry "<a href="http://andrewdumont.me/avoiding-burnout">Avoiding Burnout</a>". These are his tactics to stay in good shape:</p>
<ul>
<li>Morning Workouts</li>
<li>An Evening Walk</li>
<li>Fiction Reading</li>
<li>A Day A Week</li>
<li>Intellectual Hobbies</li>
<li>Small Wins</li>
<li>A Healthy Diet</li>
<li>Limiting Decisions</li>
<li>Yearly Unplugs</li>
</ul>
<p>While I'm doing many of these myself already, the yearly unplugging is something of which I still need to make a habit. When work -- even hard work -- is fun and fulfilling, it's sort of addictive. But Andrew is right in that work as an entrepreneur needs to be more than just hard:</p>
<blockquote>
<p>It's taken me years to realize that overnight success is fictional. Overnight success comes after years of hard, sustainable work.</p>
</blockquote>
