---
layout: post
title: "Perl-Kursmaterial online"
microblog: false
guid: http://geewiz.micro.blog/2005/11/27/perlkursmaterial-online.html
date: 2005-11-28T01:53:22+0200
type: post
url: /2005/11/27/perlkursmaterial-online.html
---

<p>Nachdem ich immer wieder gefragt werde, ob man die Unterlagen zu meinen Perl-Kursen auch online abrufen kann, habe ich mich letzte Woche entschieden, daf&uuml;r eine eigene Website zu bauen.</p>
<p>Derzeit &uuml;berarbeite ich alle Foliens&auml;tze und werde wohl auch damit beginnen, komplette Kurshandb&uuml;cher zu schreiben. Diese werde ich dann nach und nach unter einer freien Lizenz auf <a href="http://www.perl-programmieren.de">Perl-Programmieren.de</a> ver&ouml;ffentlichen.</p>
