---
layout: post
title: "Amazon Web Services jetzt mit g&uuml;nstigerem Cloud Storage"
microblog: false
guid: http://geewiz.micro.blog/2010/05/19/amazon-web-services.html
date: 2010-05-19T17:40:00+0200
type: post
url: /2010/05/19/amazon-web-services.html
---

<p>Wem die Vorteile des Amazon Simple Storage Service (S3) bisher zu teuer waren, bietet Amazon ab sofort die Alternative "Reduced Redundancy Storage" an:</p>
<blockquote class="posterous_medium_quote">
<p>We are pleased to introduce a new storage option for Amazon S3 called Reduced Redundancy Storage (RRS) that enables customers to reduce their costs by storing non-critical, reproducible data at lower levels of redundancy than the standard storage of Amazon S3.</p>
</blockquote>
<p>RRS wird ebenfalls nach Datenvolumen abgerechnet, aber zu geringeren GB-Preisen als S3. Dateien, die im Notfall wiederhergestellt werden k&ouml;nnen, k&ouml;nnen mit RRS also mit geringerer Redundanz, aber eben auch zu geringeren Kosten in die Cloud verlagert werden.</p>
