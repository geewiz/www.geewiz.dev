---
layout: post
title: "Mobile phone backup with ZYB"
microblog: false
guid: http://geewiz.micro.blog/2006/07/03/mobile-phone-backup.html
date: 2006-07-03T17:26:38+0200
type: post
url: /2006/07/03/mobile-phone-backup.html
---

<p>Among the things I change regularly, there's also my mobile phone. Last time I got a new one, I spent about an hour transferring all my contacts and other information from the old one by bluetooth, one contact by another. Especially when the two phones aren't from the same manufacturer, this seemed to be the only way. But there's another!</p>
<p>Thanks to the SyncML standard, you can synchronize many mobile phones with a central repository, forth and back. In the current issue of c't, there's a tutorial how to install your own SyncML server.</p>
<p>But it's even more effortless if you use <a href="http://www.zyb.com">ZYB</a>, the mobile phone synchronization service on the web. They provide a central SyncML server for free, letting you backup and transfer your mobile phone data. If you have more than one mobile, you can even keep their data synchronized. Furthermore, you can access all your contact and calendar data over a nice AJAXish user interface.</p>
<p>It took me about a minute to get all my contacts onto ZYB. And when my Nokia E61 gets delivered this week, I'll just download the contacts and it'll be ready to go. The mobile life actually can be easy!</p>
