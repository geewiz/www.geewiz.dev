---
layout: post
title: Using Obsidian as my blog editor
date: 2024-12-23 11:12
description: 
image: 
category: 
tags: 
published: true
sitemap: true
---
[Obsidian](https://obsidian.md/) has been my long-term memory for many years now, but only yesterday I realized how easy it was to also make it my blog editor and thus greatly simplify writing new posts.

I use [Jekyll](https://jekyllrb.com/) to generate the files for this blog; these files then get served from a free [Netlify](https://www.netlify.com/) account. The setup required to integrate Obsidian is minimal: I created a new folder named `posts` that became the root folder of my new Obsidian vault. Then I moved the existing `_posts` folder containing all my blog posts as Markdown files in there. Jekyll doesn't care, as long it can find its `_posts` folder anywhere, it's happy. I added an `images` folder next to `_posts` to make future image files visible in Obsidian as well. Finally, I added the `.obsidian` directory to `.gitignore` to avoid merge conflicts. These are all the changes necessary to use Obsidian; everything else is quality-of-life improvements.

Obsidian is well-known for its plugin ecosystem, and quite a few of them help with writing. I started out with a minimal set of community plugins to keep things simple (and myself from tinkering). All I installed was the [Templater plugin](https://github.com/SilentVoid13/Templater) to start a new blog post with all the YAML front matter already in place and partly filled-out, and the [Quick Add plugin](https://github.com/chhoumann/quickadd) to create a new blog post from my post template with a single hotkey. The post template lives in a folder `templates` next to `_posts`; it needs to be excluded from processing in the Jekyll configuration. Finally, I've added the [Git plugin](https://github.com/Vinzent03/obsidian-git) so I can publish (i.e. `git commit` and `git push`) my new posts right from Obsidian.

Since I have Obsidian open all day anyway, starting a new blog post has now become as easy as it can ever be: Hit `Alt-Shift-q`. All I have to do now is write. That then might turn out not to be quite as easy, but that's a problem Obsidian can't solve.
