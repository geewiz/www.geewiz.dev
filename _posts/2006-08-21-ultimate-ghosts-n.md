---
layout: post
title: "Ultimate Ghosts 'n' Goblins for PSP"
microblog: false
guid: http://geewiz.micro.blog/2006/08/21/ultimate-ghosts-n.html
date: 2006-08-21T23:10:19+0200
type: post
url: /2006/08/21/ultimate-ghosts-n.html
---

<p>I certainly had no problem with finding arguments for buying a Playstation Portable anyway, but seeing that <a href="http://psp.ign.com/objects/769/769179.html">Ultimate Ghosts 'n' Goblins</a> has been made available for the PSP is a huge boost towards putting it on my christmas wish list. I can still hum the GnG game music after more than a decade since I last played it.</p>
