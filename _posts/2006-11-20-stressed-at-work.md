---
layout: post
title: "Stressed at work"
microblog: false
guid: http://geewiz.micro.blog/2006/11/20/stressed-at-work.html
date: 2006-11-20T22:36:10+0200
type: post
images:
- /uploads/stressed.jpg
photos:
- /uploads/stressed.jpg
url: /2006/11/20/stressed-at-work.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/stressed.jpg" />
</div>
<p>Stress at the workplace is a common topic everywhere and increasingly a cause of health problems. </p>
<p>I'm a -passionate- <a href="http://en.wikipedia.org/wiki/Stoicism">Stoic</a>, so I'm very hard to get stressed. Even today, having to do all the work myself because everyone of my team is either ill or coming late for a night shift, I don't feel particularly uncomfortable.</p>
<p>I don't like being stressed, so I do everything to avoid it. That doesn't necessarily mean that I try to avoid work, though. In the contrary -- I dislike boredom as much as I dislike stress. "I'm not stressed, I just have a lot to do" is one of my favourite responses when people are concerned about my workload.</p>
<p>One measure for building a wall between Busyland and Stressland is organization. Approaches like <a href="http://p.jochen-lillich.de/amazon/0142000280">Getting Things Done</a> aim at actively managing one's tasks instead of being managed by them. They also build a feeling of accomplishment. It's like a good workout at the gym: having done a lot of work doesn't necessarily mean that you'll go home exhausted and tired -- it can even give you additional energy and motivation.</p>
<p>It's a myth that stress is equal to workload and can be overcome by more or less work. It's even more a myth that stress is an indicator of how important you are at your company. Stress is only an indicator for how good you feel at work, and it always is a negative one. <a href="http://positivesharing.com/2006/11/top-5-myths-about-workplace-stress/">Chief Happiness Officer" Alexander Kjerulf debunks the "Top 5 myths about workplace stress</a> in his blog.</p>
<p><a href="http://blog.jochen-lillich.de/archives/582-You-like-to-be-where-you-can-laugh.html">To accept stress as a normal condition of work is bad for people and bad for business!" is the central important insight in his article. In my opinion, it's one of managers' top priorities to shield their subordinates and themselves from stress. Even if that means "doing strange things</a> because they help to discharge.</p>
