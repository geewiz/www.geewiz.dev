---
layout: post
title: "Spying while flying"
microblog: false
guid: http://geewiz.micro.blog/2006/09/12/spying-while-flying.html
date: 2006-09-12T07:27:25+0200
type: post
url: /2006/09/12/spying-while-flying.html
---

<p>Most of my friends and colleagues like flying to holiday places, some even are frequent business flyers. Folks, you better watch your tongue and behaviour in the future.</p>
<p>According to the British <a href="http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2006/09/11/nplanes11.xml.">Telegraph", soon "Passengers' chat will be recorded to foil hijackers</a> Currently, researchers are developing computer systems that can monitor the whole plane, of course including the lavatory, with cameras and microphones.</p>
<p>And if you tend to get nervous before lift-off or even have a bit of fear of flying, well, you better don't show it.</p>
<blockquote class="posterous_medium_quote">
<p>"It would pick passengers who are behaving oddly or in an unruly manner," she said. "They may appear nervous, or could be getting up while the plane is taxiing. If someone looks as if they are praying, the microphones would be able to tell if they were by picking up key words."</p>
</blockquote>
<p>Just imagine what will happen when you sit on the toilet, constipated, muttering to yourself "Oh my god, I think I'm gonna explode..."</p>
