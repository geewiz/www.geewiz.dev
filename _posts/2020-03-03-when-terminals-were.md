---
layout: post
title: "When terminals were real tty’s"
microblog: false
guid: http://geewiz.micro.blog/2020/03/03/when-terminals-were.html
date: 2020-03-03T19:17:00+0200
type: post
categories:
- "DEV"
url: /2020/03/03/when-terminals-were.html
---
For my first Retro Saturday [live stream](https://www.twitch.tv/fullstacklive), I chose working with a PDP-11 minicomputer. I don’t own a PDP-11, nor can I afford to buy or run one, but I can simulate one realistically using[simh](http://simh.trailing-edge.com), the simulator for historic computer systems.

After successfully installing Unix V7, the original Unix operating system from 1979, I ran into unexpected issues with entering source code. One of the problems was the lack of a full-screen editor combined with my lack of `ed` skills. Another problem was that the Backspace key didn’t work as expected; an issue I first encountered 27 years ago trying out an early version of Linux. And then there was the strange behaviour that I had to escape the number sign at the beginning of `#include <stdio.h>` with a backslash; otherwise it would disappear.

After the stream, I found out what the problem was: V7 was simply still geared towards being operated from a teletype, basically a typewriter with a serial port. (It would take many years until typewriters actually came with correction ribbons that enabled you to erase and overwrite. But by that time, teletypes had been replaced by serial terminals with CRT screens.)

On an input device that prints each character you type immediately on paper, it’s impossible to do what Backspace does. That’s why the Unix developers assigned special meaning to the `#` and `@` signs. 

The `#` sign tells the V7 shell to ignore the previously typed character. Correcting a typo would look like this: `cat memp#o.txt`.

For larger errors, there’s the `@` sign. It tells the shell to ignore everything before it. For example: `rm mem@mv memo.txt memo.bak`.

Exploring historic operating systems has become a passion for me lately. Not only do I learn a lot about early computing technology. It also makes me appreciate so much more the achievements of pioneers like Thompson and Richie who didn’t have any of the conveniences software developers enjoy today.

2.11BSD seems to be a more user-friendly OS for the PDP-11, so that’s what I’m going to try next!
