---
layout: post
title: "Just watched the new Star Trek"
microblog: false
guid: http://geewiz.micro.blog/2009/05/30/just-watched-the.html
date: 2009-05-31T00:31:08+0200
type: post
url: /2009/05/30/just-watched-the.html
---

<p>Because Carolin participates in the YCW meetup with Amalia, I can spend the weekend on my own. After cleaning the apartment and doing some laundry, I spent the evening at the movies.</p>
<p>I haven&#8217;t been to a cinema for almost two years and Star Trek was the best choice to end that time.</p>
<p>The film brought many smiles to my face. I&#8217;d like to thank J. J. Abrams for an entertaining movie, Leonard Nimoy for gracing the movie with his dignity and Chris Pine for a convincing portayal of a young Kirk, Shatner-style.</p>
<p>And thanks to my love Carolin for the courage to spend the weekend on her own, in a tent with a baby that needs a lot of care, giving me the opportunity to go out without a bad conscience.</p>
