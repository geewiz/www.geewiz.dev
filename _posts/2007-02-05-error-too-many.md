---
layout: post
title: "Error: Too many flies"
microblog: false
guid: http://geewiz.micro.blog/2007/02/05/error-too-many.html
date: 2007-02-06T01:34:32+0200
type: post
url: /2007/02/05/error-too-many.html
---

<p>No, I didn't mean <a href="http://en.wikipedia.org/wiki/Sciaridae">files". It's flies. Tiny, pesky flies (they're called "sciaridae</a>). And our plant pots at home are infested with them.</p>
<p>When the first ones appeared in our flat a while ago, Carolin prohibited me from swatting them; she found them cute and called them <a href="http://www.bloekschaf.de/archives/84-Herrin-der-Fliegen.html">her little friends". Well, when she returned from a weekend at her parents yesterday, she had to sweep away "more than a hundred fly corpses</a>, including the ones she had instantly swatted herself. She had been warned.</p>
<p>Choose your friends wisely, or as Seneca said:</p>
<blockquote class="posterous_short_quote">
<p>Ponder for a long time whether you shall admit a given person to your friendship; but when you have decided to admit him, welcome him with all your heart and soul.</p>
</blockquote>
