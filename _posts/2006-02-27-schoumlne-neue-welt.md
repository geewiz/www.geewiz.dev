---
layout: post
title: "Sch&ouml;ne neue Welt"
microblog: false
guid: http://geewiz.micro.blog/2006/02/27/schoumlne-neue-welt.html
date: 2006-02-27T23:33:08+0200
type: post
url: /2006/02/27/schoumlne-neue-welt.html
---

<p>Bei <a href="http://www.c0t0d0s0.org/archives/1226-Google-Epic-2015.html">Joerg Moellenkamp</a> bin ich auf eine interessante Zukunftsvision gesto&szlig;en: <a href="http://www.c0t0d0s0.org/exit.php?url_id=2674">epic</a>&amp;entry_id=1226. Der als R&uuml;ckblick aus dem Jahr 2015 konzipierte Film ist beeindruckend realistisch. Und damit meine ich nicht die ausgezeichnete deutsche Synchronisierung oder das kreative epic-Logo, sondern die Tatsache, dass wir auf dem besten Weg zu dieser Dystopie sind.</p>
