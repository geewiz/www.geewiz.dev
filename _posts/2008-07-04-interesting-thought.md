---
layout: post
title: "Interesting thought..."
microblog: false
guid: http://geewiz.micro.blog/2008/07/04/interesting-thought.html
date: 2008-07-04T19:24:25+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpimgsxkcdcom_hnzwz-scaled500.png?w=255
photos:
url: /2008/07/04/interesting-thought.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpimgsxkcdcom_hnzwz" height="381" src="/assets/wp-content/2011/05/media_httpimgsxkcdcom_hnzwz-scaled500.png?w=255" width="325" />
</div></p>
<p>Okay, I guess, Carolin will inhibit my curiosity. But, when she's old enough to carry a screwdriver, I will teach Amalia how to get into the heart of things. She certainly already knows how to get into our hearts.</p>
