---
layout: post
title: "I don't like Mondays"
microblog: false
guid: http://geewiz.micro.blog/2005/08/08/i-dont-like.html
date: 2005-08-08T23:17:42+0200
type: post
url: /2005/08/08/i-dont-like.html
---

<p>Zumindest, wenn sie so sind wie der aktuelle. Hier platzt eine Maschine unerwartet aus allen N&auml;hten, da korrumpiert ein Datenbestand, dazu dann noch Anfragen, die unbedingt in zwei Stunden erledigt sein m&uuml;ssen. Klar, sowas geh&ouml;rt zum Job. Aber warum gleich in den ersten Stunde der neuen Woche? Da frage ich mich dann schon (um auch am Ende einen Liedtitel zu haben): "What have I done to deserve this?"</p>
<p><em>Update:</em> Mein Handy ist wohl auch kaputt. Seit einer unabsichtlichen 1G-Beschleunigung mit abrupter Verz&ouml;gerung funktioniert das Mikro nicht mehr. Na supi.</p>
