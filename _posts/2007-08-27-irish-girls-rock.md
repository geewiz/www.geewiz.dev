---
layout: post
title: "Irish girls rock!"
microblog: false
guid: http://geewiz.micro.blog/2007/08/27/irish-girls-rock.html
date: 2007-08-27T02:11:48+0200
type: post
url: /2007/08/27/irish-girls-rock.html
---

<p>Today, I found Sophie Merry, the Groovy Dancing Girl, on YouTube. Not only are her dancing moves really awesome, but they're also filmed in slow motion and played back at higher speed, but still synchronous to the music. Take a look!</p>
<p>"Harder Better Faster Stronger" by Daft Punk:</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=Sr2JneittqQ])</p>
<p>"Phantom" by Justice:</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=zbSKnU5ZwJg])</p>
