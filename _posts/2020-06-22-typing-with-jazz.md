---
layout: post
title: "Typing with Jazz"
microblog: false
guid: http://geewiz.micro.blog/2020/06/22/typing-with-jazz.html
date: 2020-06-22T07:48:56+0200
type: post
url: /2020/06/22/typing-with-jazz.html
---

I do my work using keyboards and I make music using keyboards. [This clever web application](https://jazzkeys.plan8.co) brings it all together.

