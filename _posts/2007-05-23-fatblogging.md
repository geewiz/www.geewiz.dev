---
layout: post
title: "Fatblogging: 100"
microblog: false
guid: http://geewiz.micro.blog/2007/05/23/fatblogging.html
date: 2007-05-23T19:02:00+0200
type: post
url: /2007/05/23/fatblogging.html
---

<p>Many bloggers are overweight. That's no surprise, since sitting in front of the screen all day and living from pizza and coffee has never been regarded as a recipe to lose weight.</p>
<p>Now, a new weight loss plan is gaining ground among bloggers: Losing weight by writing about it. Yes, it may seem like a strange concept, but it seems to work with many, so I'm willing to give it a try. It's time I do something to shrink my belly, and it gets me a new topic to blog about.</p>
<p>My starting weight is 100kg, and I guess that's about 15kg too much. I guess I'd look at lot better with 85 kilos. Let's see how blogging my weight every few days will help me reach that goal.</p>
<p>On another note, I also started running again. It's embarrassing if climbing the stairs to our flat makes me breathe more heavily than a marathon winner. (I won't get into other situations where it's much more enjoyable to be in a good condition.) I'm using the starter plan of <a href="http://www.myjogging.de">MyJogging.de</a> <a href="http://blog.jochen-lillich.de/archives/344-Mit-kleinen-Schritten.html">again</a> to get myself into shape: over the course of six weeks, I'll always run for half an hour. But that half hour gets broken into growing intervals, starting at two minutes of  running followed by one minute of walking.</p>
<p>Trying to run in the evening after coming home from work won't be very successful. It's too hard for me to get going when I'm tired and digesting a lunch. So I decided to get up at 5:30 in the morning and start the day in running shoes, followed by a shower and a nice little breakfast.</p>
<p>I don't want to flood my blog with entries, you know, so I'll simply report about my running progress in the fatblogging entries. Mens sana in corpore sano. Let's work on that second part.</p>
