---
layout: post
title: "Linux admins wanted!"
microblog: false
guid: http://geewiz.micro.blog/2006/07/01/linux-admins-wanted.html
date: 2006-07-01T21:11:16+0200
type: post
url: /2006/07/01/linux-admins-wanted.html
---

<p>It really can get a vicious circle not having enough staff for the challenges rising. You get buried with work, planned and unplanned. So you don't have the time reading applications. So you don't invite applicants. So your team doesn't grow. So you'll keep drowning.</p>
<p>I'm glad that this week, I could break that circle. I got some really interesting applications, and I also met some people that might fit the job.</p>
<p>At WEB.DE IT, we're <a href="http://www1.schlund.de/index.php?">looking for sysadmins</a>&amp;page=jobs&amp;action=displayJobs&amp;cat=1#job297 that have "Linux" written all over them. We're running hundreds of Linux servers in a high availability environment, so there's much to do and much to learn, too. </p>
<p>If you're inclined to and feel fit to do system administration in a real enterprise setting or if you know someone who does, then I'd be happy to get your application! (Contact provided in the job ad linked above.)</p>
