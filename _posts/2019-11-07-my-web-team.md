---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2019/11/07/my-web-team.html
date: 2019-11-07T15:11:18+0200
type: post
url: /2019/11/07/my-web-team.html
---
> "My web team told me about your [Drupalcon talk](https://www.freistil.it/drupalcon-amsterdam-2019/) and I watched the YouTube recording yesterday. I just wanted to say that I really appreciated your talk, your honesty, humour, concrete advice, thank you."

This made my day.
