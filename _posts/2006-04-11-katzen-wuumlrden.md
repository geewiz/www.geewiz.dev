---
layout: post
title: "Katzen w&uuml;rden..."
microblog: false
guid: http://geewiz.micro.blog/2006/04/11/katzen-wuumlrden.html
date: 2006-04-11T19:34:34+0200
type: post
url: /2006/04/11/katzen-wuumlrden.html
---

<p>...sich an den Kopf fassen, wenn sie die neuesten Ans&auml;tze verstehen k&ouml;nnten, ihnen <a href="http://blog.handelsblatt.de/indiskretion/eintrag.php?id=661">Lifestyle</a> angedeihen zu lassen.</p>
<p>Angesichts der Tatsache, dass t&auml;glich noch immer tausende von Menschen an Unterern&auml;hrung sterben, grenzt dieser Trend, Haustierhaltern mit immer neuen Futter-Ideen immer mehr Geld aus der Tasche zu ziehen, f&uuml;r mich an Menschenverachtung.</p>
<p>In einem sehr guten Vortrag bei Google mit dem Titel <a href="http://video.google.com/videoplay?docid=-6909078385965257294">All marketers are liars</a>&amp;q=type%3Agoogle+duration%3Along&amp;pl=true ging Marketing-Berater Seth Godin auch auf das Thema Katzenfutter ein:</p>
<blockquote class="posterous_short_quote">
<p>Cat food is not made for cats. If it was, it would come in mouse flavors. Cat food is made for cat owners.</p>
</blockquote>
<p>Ich liebe Katzen. Aber ich w&uuml;rde ihnen niemals Lifestyle-Futter geben. Denn ich liebe auch Menschen.</p>
