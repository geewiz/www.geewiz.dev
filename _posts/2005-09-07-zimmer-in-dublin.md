---
layout: post
title: "Zimmer in Dublin gesucht"
microblog: false
guid: http://geewiz.micro.blog/2005/09/07/zimmer-in-dublin.html
date: 2005-09-07T06:38:35+0200
type: post
url: /2005/09/07/zimmer-in-dublin.html
---

<p>Mein Schatz Carolin m&ouml;chte ab Ende des Monats ihr Ethnologiestudium am Trinity College in Dublin fortsetzen. Sie ist aber immer noch auf der Suche nach einem Zimmer. Vielleicht findet sich auf diesem Weg ein Kontakt, der ihr dabei helfen kann!> Hallo ihr da drau&szlig;en,</p>
<blockquote>
<p>wie einige von euch wohl wissen werden, fliege ich am 24. diesen Monats nach Irland, um dort in Dublin weiterzustudieren. Zur Zeit kann ich mir von Deutschland aus kaum ein Zimmer suchen, da man das vor Ort auch mal anschauen muss, bevor man zusagt, etc.</p>
<p>Jetzt brauche ich eure Hilfe:</p>
<p>Habt ihr jemanden in eurem Bekannten-, Freundes- oder Familienkreis, der oder die in Dublin wohnt oder zumindest jemanden kennt, der oder die dort wohnt, oder Verbindungen dorthin hat? Es geht darum, dass ich f&uuml;r die ersten paar Tage eine Unterkunft brauche. Dabei ist es mir egal, ob auf dem Fussboden, dem Sofa oder auf einer Isomatte im Keller, Hauptsache es kostet nicht so viel, denn ich habe kaum Kohle. Und Hostels oder Jugendherbergen werden auf die Dauer teuer. Es geht dabei wirklich nur um die ersten Tage bis ca. eine Woche und bis ich etwas anderes gefunden habe.</p>
<p>Bitte helft mir und &uuml;berlegt. Und fragt auch mal weiter bei der Arbeit, auf einer Party oder wo auch sonst immer. Ich w&auml;r euch total dankbar.</p>
<p>Danke schonmal und viele Gr&uuml;&szlig;e an alle, die mir helfen (ja, und auch an die, die mir nicht helfen),</p>
<p>Carolin</p>
</blockquote>
<p>Wer von euch einen hilfreichen Kontakt weiss, kann sich gern direkt an <a href="mailto:carolin.gall">Caro</a>@web.de wenden. Sie w&uuml;rde sich riesig freuen!</p>
