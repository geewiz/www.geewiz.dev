---
layout: post
title: "Linux on the PS3"
microblog: false
guid: http://geewiz.micro.blog/2006/10/24/linux-on-the.html
date: 2006-10-24T06:58:26+0200
type: post
url: /2006/10/24/linux-on-the.html
---

<p>Oh yeah, Sony, you really want me to get your new console, don't you? First, my PS2 stops playing my newer game DVDs, and now there'll be a <a href="http://www.engadget.com/2006/10/17/yellow-dog-linux-5-0-to-run-on-sonys-playstation-3/.">Linux for the Playstation 3</a></p>
<p>Well, I'm not falling for that, you know! No, I'll definitely not buy your huge block of graphical goodness!</p>
<p>At least not before christmas.</p>
