---
layout: post
title: "Playstation 3 kommt im Sp&auml;tjahr"
microblog: false
guid: http://geewiz.micro.blog/2006/03/16/playstation-kommt-im.html
date: 2006-03-16T05:23:25+0200
type: post
url: /2006/03/16/playstation-kommt-im.html
---

<p>Einem <a href="http://www.nytimes.com/2006/03/15/business/15cnd-sony.html">Bericht der New York Times</a> zufolge wird die Playstation 3 im November erscheinen. Dass sie deutlich nach der Xbox 360 auf den Markt kommen wird, war ja hinl&auml;nglich bekannt. Aber was mich wirklich &uuml;berrascht, ist, dass sie Linux als Betriebssystem mitbringen und damit auch als Heimserver dienen soll!</p>
