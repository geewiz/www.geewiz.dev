---
layout: post
title: "Bloglines adds blog playlists"
microblog: false
guid: http://geewiz.micro.blog/2006/11/16/bloglines-adds-blog.html
date: 2006-11-16T03:41:09+0200
type: post
url: /2006/11/16/bloglines-adds-blog.html
---

<p>A few days ago, <a href="http://www.bloglines.com">Bloglines</a> added a playlist feature to their online feed reader: Just like titles in a music collection, you can group your feeds into separate playlists that make reading of new blog entries and news as well as listening to new netcast episodes much easier.<br />
You can group your feeds at your choosing, so for example, you can make a "almost no time" playlist that only contains the most important information sources that you want to tap even when there's only little time.</p>
<p>I'm quite happy with Bloglines adding practical features periodically, and no other RSS aggregating service could so far draw me away from Bloglines.</p>
<p>Of course I had a look at the new <a href="http://www.google.com/reader">Google Reader</a>, and it really is nice. The user interface, e.g. the "mark the entry as read not until I actually read it" feature, is great. But Google Reader falls short in one area that is very important to me: mobile access. While Google also offers a mobile version of their feed reader, you can't choose which feeds you actually want to read when you're on the road. It just displays everything, in chronological order. That's very impractical when you subscribe to high volume feeds like Engadget or the Perl Monastery. I don't want to have to go through hundreds of product announcements on my smartphone before I get to the feeds of my friends.</p>
<p>Bloglines allows me to choose which feeds should be displayed in the mobile version. And with their new playlist feature, they make displaying different feeds in different situations even more flexible and easy.</p>
