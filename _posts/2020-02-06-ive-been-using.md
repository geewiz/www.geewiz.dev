---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2020/02/06/ive-been-using.html
date: 2020-02-06T13:18:49+0200
type: post
url: /2020/02/06/ive-been-using.html
---
I've been using [DuckDuckGo](https://duckduckgo.com) as my default search engine for a while. TIL that DuckDuckGo also has practical shortcuts called [bangs](https://duckduckgo.com/bang?c=Tech&sc=Languages+(Ruby)) for searching on a specific site.
