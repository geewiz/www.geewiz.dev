---
layout: post
title: "DNS-Check bei Pingdom"
microblog: false
guid: http://geewiz.micro.blog/2010/07/22/dnscheck-bei-pingdom.html
date: 2010-07-22T09:19:49+0200
type: post
images:
- /assets/wp-content/2011/05/dnscheck.png?w=200
photos:
url: /2010/07/22/dnscheck-bei-pingdom.html
---
<p>
    Seit kurzem bietet Pingdom einen DNS-Check an. Einfach die zu pr&uuml;fende Domain auf <a href="http://dnscheck.pingdom.com">http://dnscheck.pingdom.com</a> eingeben und man erh&auml;lt einen ausf&uuml;hrlichen Testbericht.
<div class='p_embed p_image_embed'>
<img alt="Dnscheck" height="258" src="/assets/wp-content/2011/05/dnscheck.png?w=200" width="200" />
</div></p>
