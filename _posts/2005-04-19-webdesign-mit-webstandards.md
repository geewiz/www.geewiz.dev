---
layout: post
title: "Webdesign mit Webstandards"
microblog: false
guid: http://geewiz.micro.blog/2005/04/19/webdesign-mit-webstandards.html
date: 2005-04-19T20:50:18+0200
type: post
url: /2005/04/19/webdesign-mit-webstandards.html
---

<p><a href="http://www.andreas-kalt.de/webdesign/tutorials/webdesign-mit-webstandards">Webdesign mit Webstandards</a> ist Andreas Kalts &Uuml;bersetzung des englischsprachigen Artikels "Developing With Web Standards" von Roger Johansson.</p>
<p>Ziel des Artikels ist es, aufzuzeigen, "wie und warum die Ber&uuml;cksichtigung von Webstandards es Ihnen erm&ouml;glicht, als Webdesigner Zeit und Geld zu sparen und Ihren Besuchern bessere Websites zu bieten. Au&szlig;erdem werden Methoden, Richtlinien und sinnvolle Techniken vorgestellt, die ihnen helfen werden, qualitativ hochwertige Websites zu erstellen, die so vielen Besuchern wie m&ouml;glich zug&auml;nglich sind."</p>
<p>Weil ich selbst gro&szlig;e Dinge auf die Webstandards halte, empfehle ich den Artikel allen Web-Bastlern zur Lekt&uuml;re.</p>
<p>(via <a href="http://news.css-technik.de/index?id=P669">CSS-Technik-News</a>)</p>
