---
layout: post
title: "Weeknote #-6 (week 8, 2010)"
microblog: false
guid: http://geewiz.micro.blog/2010/03/03/weeknote-week.html
date: 2010-03-03T04:49:34+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2010/03/03/weeknote-week.html
---

<p>Monday last week, I attended a founders seminar sponsored by the Work Agency. I already knew most of the seminar content, but the exchange among the participants had been interesting. I even could talk a bit about my experiences from my former shots at self-employment. </p>
<p>When my new tax consultant told me on Tuesday what amount of tax return I can expect for 2008 (<em>cough</em>), I could not help but wonder why I didn't get help with my taxes earlier. </p>
<p>Many hours this week went into my talk titled "Drupal in the Cloud". I held it at DrupalCamp which took place in Essen over the weekend. Judging from the questions I got from the audience, I hit an interesting topic. After the talk, I had conversations with several Drupal service providers that I had start thinking about moving their hosting to us. This and the praise I got for my presentation made the journey worthwhile. If only the storm on Sunday wouldn't have disrupted train traffic so thoroughly that I ended up at my brother's place at midnight because I just couldn't make it all the way home.</p>
