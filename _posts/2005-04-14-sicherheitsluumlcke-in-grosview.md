---
layout: post
title: "Sicherheitsl&uuml;cke in gr_osview"
microblog: false
guid: http://geewiz.micro.blog/2005/04/14/sicherheitsluumlcke-in-grosview.html
date: 2005-04-14T20:47:27+0200
type: post
url: /2005/04/14/sicherheitsluumlcke-in-grosview.html
---

<p>Sicherheitswarnungen f&uuml;r IRIX gibts nur noch selten, also gebe ich diese mal weiter:</p>
<p>Der grafische Systemmonitor gr_osview erlaubt lokalen Benutzern, sich fremde Rechte anzueignen. Patches stehen zur Verf&uuml;gung und sind auf <a href="http://secunia.com/advisories/14875/">secunia.com</a> verlinkt.</p>
<p>(via <a href="http://www.siliconbunny.com/news/archives/000174.shtml">SiliconBunny</a>)</p>
