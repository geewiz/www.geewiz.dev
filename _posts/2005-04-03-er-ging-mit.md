---
layout: post
title: "Er ging mit Gott"
microblog: false
guid: http://geewiz.micro.blog/2005/04/03/er-ging-mit.html
date: 2005-04-03T08:40:00+0200
type: post
images:
- /uploads/JohannesPaulII.serendipityThumb.jpg
photos:
- /uploads/JohannesPaulII.serendipityThumb.jpg
url: /2005/04/03/er-ging-mit.html
---

<p>Nach fast 27-j&auml;hriger Amtszeit ist <a href=<a href="http://www.spiegel.de/panorama/0">[de.wikipedia.org/wiki/Joha...](http://de.wikipedia.org/wiki/Johannes_Paul_II.)">Papst Johannes Paul II.</a> heute gestorben -- auch f&uuml;r mich der Anlass, &uuml;ber seine Bedeutung f&uuml;r die Kirche, f&uuml;r die Welt und f&uuml;r mich pers&ouml;nlich nachzudenken.
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/JohannesPaulII.serendipityThumb.jpg" />
</div>
<p> Johannes Paul II. war kein unumstrittener Papst. Seine konservative Einstellung zeigte sich in vielen kirchlichen und sozialen Themen, zum Beispiel zum Z&ouml;libat, zur Stellung der Frau in der Kirche sowie zu Verh&uuml;tung und Homosexualit&auml;t. Mit zahlreichen Lehrentscheidungen verursachte er heftige Kontroversen innerhalb und ausserhalb der Kirche. Wie gerade bei vielen j&uuml;ngeren Katholiken richtet sich deshalb auch meine Glaubenspraxis nicht nach allem, was in den letzten Jahrzehnten aus Rom verk&uuml;ndet wurde. Dennoch habe ich gro&szlig;en Respekt vor der Konsequenz, mit der er seine Theologie und seinen Glauben vertreten hat. In seinem Spiegel-Artikel "Das Verm&auml;chtnis des L&ouml;wen,1518,349424,00.html legt Matthias Matussek ausf&uuml;hrlich dar, warum diese "Sturheit" dankenswert ist.</p>
<p>Eins l&auml;sst sich bei aller Kritik nicht in Abrede stellen: Papst Johannes Paul II. ist ein wichtiger Teil der Kirchen- und Weltgeschichte. Sein Wirken in Kirche und Welt hat Spuren hinterlassen und vieles bewegt. Weil die Kirche f&uuml;r alle Menschen da ist und weil das Reich Gottes nicht erst im Jenseits anf&auml;ngt, hat er sich intensiv politisch engagiert. Augenf&auml;llige Beispiele daf&uuml;r sind der Nahe Osten, Polen und die Sowjetunion, aber auch das seinerzeit geteilte Deutschland. Auf seinen Reisen demonstrierte er die N&auml;he Gottes zu den Menschen aller Welt. Bahnbrechend war sein Einsatz f&uuml;r den interreligi&ouml;sen Dialog, insbesondere zwischen Christentum, Judentum und Islam. Er setzte sich immer f&uuml;r den Frieden ein und warf die Stellung der Kirche zum Krieg &uuml;ber den Haufen: "Es gibt keinen gerechten Krieg." </p>
<p>Ich war 8 Jahre alt, als er zum Oberhaupt der Katholischen Kirche gew&auml;hlt wurde. Damit geh&ouml;re ich zu den vielen Menschen, die sich an keinen anderen Papst erinnern k&ouml;nnen. Ich habe ihn zweimal live erlebt, bei den Romwallfahrten der Ministranten 1985 und 1995. Was mich aber viel mehr beeindruckt hat, war sein auch durch Krankheit und Leiden ungebrochener Wille und die Kraft, mit der er bis zuletzt seinen Gottesdienst versehen hat. F&uuml;r mich ist er zum Vorbild f&uuml;r ein konsequentes und glaubw&uuml;rdiges christliches Leben geworden.</p>
