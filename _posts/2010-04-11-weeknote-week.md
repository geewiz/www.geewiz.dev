---
layout: post
title: "Weeknote #1 (week 14, 2010)"
microblog: false
guid: http://geewiz.micro.blog/2010/04/11/weeknote-week.html
date: 2010-04-12T00:49:05+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2010/04/11/weeknote-week.html
---

<p>Whoa, only a few weeks in and already falling off the wagon? Those things are called "weeknotes" for a purpose, my dear friend!</p>
<p>Well okay, I've been pretty busy lately and the holidays with their parent-visiting didn't help. So, what's going in at the Freistil front?</p>
<p>Passion for starting a new business is one thing, being able to support your family is another. Since I don't expect our revenue to be huge from the start, I had to look for some type of funding that lets me keep paying for rent and food. Fortunately, the German state offers subsidies for people who leave or lose their job and want to build their own business. The downside: it's the state. Think beaurocracy. So, the week before Easter, I went to the Work Agency to submit my papers. Because I try to live the dream of a paperless office, I had all forms scanned in and later printed them out for my CPA or myself to fill them in. Now, at the Agency, the clerk politely explained to me that applications have to be submitted on the original form. And sent me back home. <em>cricket</em> So, instead of closing the deal, I had achieved nothing and that setback destroyed my motivation for the rest of the day. Productivity ground zero. Fortunately, I got another appointment two days later and delivered the papers right before the official went on her Easter holiday. Now it's waiting with my fingers crossed. </p>
<p>After building some clusters on <a href="http://aws.amazon.com">Amazon Webservices</a>, I tried <a href="http://www.rackspacecloud.com">Rackspace</a> for comparison. Rackspace doesn't offer a service landscape as a big as AWS does, they're more of a VPS-by-the-hour shop. But at that, they seem to be quite good. Since our infrastructure is highly automated, we'll be able to use inexpensive Rackspace servers without much hassle where they fit in.</p>
<p>Getting all the necessary infrastructure in place takes a lot of my time. Frist, there are many parts in this puzzle of high performance and availability. And second, I often have to catch up with many software solutions I may have heard about during my management days but hadn't had the opportunity to put my own hands on. Because of that, I spend many days on the command line. Which is actually fun, but keeps me from doing other important tasks like website building.</p>
<p>So, I'm getting used to working long hours, or, like Gary Vaynerchuck puts it, to "crushing it".</p>
<p>And in the same Crushing Mode, I finished the <a href="http://campus.freistil-consulting.de">Freistil Campus</a> website on Friday night at 3am. Campus replaces the Moodle installation I've been using for online trainings. Based on Drupal, Campus will give us more flexibility to build the features we need for a great online training platform.</p>
<p>For the coming week, I hope to get the new hosting website ready to launch and for a positive verdict on my subsidies.</p>
