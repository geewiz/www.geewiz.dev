---
layout: post
title: "Online-Bewerbung: einfach und doch schwer"
microblog: false
guid: http://geewiz.micro.blog/2005/12/30/onlinebewerbung-einfach-und.html
date: 2005-12-30T02:07:30+0200
type: post
url: /2005/12/30/onlinebewerbung-einfach-und.html
---

<p>In <a href="http://www.spiegel.de/unispiegel/jobundberuf/0">K.o. durch Smileys im Anschreiben</a>,1518,392189,00.html geht Spiegel Online auf die T&uuml;cken der Online-Bewerbung ein.</p>
<p>Auch wenn ich mich mit <a href="http://blog.koehntopp.de/archives/1108-Onlinebewerbungen-nerven.html">Kris</a> frage, warum Firmen so sauer werden, wenn ersichtlich ist, dass sie nur ein Empf&auml;nger unter vielen sind (schlie&szlig;lich machen sie auch jedem Bewerber deutlich, dass er nur einer unter vielen ist), enth&auml;lt der Artikel doch einige Hinweise, welche Patzer bei der Online-Bewerbung vermeidbar sind.</p>
