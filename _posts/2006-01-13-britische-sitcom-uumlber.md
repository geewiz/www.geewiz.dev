---
layout: post
title: "Britische Sitcom &uuml;ber \"die Geeks im Keller\""
microblog: false
guid: http://geewiz.micro.blog/2006/01/13/britische-sitcom-uumlber.html
date: 2006-01-13T07:04:58+0200
type: post
url: /2006/01/13/britische-sitcom-uumlber.html
---

<p>Und wieder einmal h&auml;tte ich so gern ausl&auml;ndisches Fernsehen! Am 3. Februar startet auf dem britischen Sender <a href="http://www.channel4.com/entertainment/tv/microsites/I/itcrowd/.">Channel&nbsp;4" die Serie "The IT Crowd</a> Ich w&uuml;rde die Sitcom nicht nur deshalb gern sehen, weil sie meine Zunft aufs Korn nimmt, sondern vor allem, weil sie aus der Feder von Graham Linehan stammt. Ihm verdankt die Welt bereits die Drehb&uuml;cher zu den genialen Serien "Father Ted" und "Black Books".Und darum gehts:</p>
<blockquote class="posterous_medium_quote">
<p>The high-rise towers of Renham Industries are full of go-getters, success stories, and winners... apart from in the basement. While their beautiful colleagues work upstairs in fantastic surroundings, the I.T. department - Jen, Roy and Moss - lurk below ground, scorned by their co-workers as geeky losers. </p>
</blockquote>
<p>Oh ja. Ich hatte selbst mal ein B&uuml;ro im Keller...</p>
<p>Ein kleiner Trost f&uuml;r den noch in Deutschland Festsitzenden: die Folgen kann man auch &uuml;bers Web anschauen. Und mittelfristig wird die Serie wohl per DVD den Weg zu mir finden.</p>
