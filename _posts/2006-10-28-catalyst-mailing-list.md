---
layout: post
title: "Catalyst mailing list in German"
microblog: false
guid: http://geewiz.micro.blog/2006/10/28/catalyst-mailing-list.html
date: 2006-10-28T08:09:17+0200
type: post
url: /2006/10/28/catalyst-mailing-list.html
---

<p>German speakers that are interested in the Catalyst framework for development of web applications in Perl can now subscribe to a <a href="http://lists.scsys.co.uk/mailman/listinfo/catalyst-de">Catalyst mailing list</a> in german language.</p>
<p>As <a href="http://use.perl.org/articles/06/10/24/190228.shtml">use perl;</a> states, Matt S. Trout created the mailing list after some discussion in two of the german Perl mongers mailing lists.</p>
