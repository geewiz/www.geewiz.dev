---
layout: post
title: "Wieder geht ein Jahr"
microblog: false
guid: http://geewiz.micro.blog/2006/02/02/wieder-geht-ein.html
date: 2006-02-03T01:16:50+0200
type: post
images:
- /uploads/gebparty2006.png
photos:
url: /2006/02/02/wieder-geht-ein.html
---

<p>Dieses Jahr hat ein gutes Timing: viele Feiertage fallen auf Werktage, und mein Geburtstag auf einen Samstag. Da bleiben kaum Ausreden, keine Party zu machen...Somit lade ich alle meine Freunde und Bekannten herzlich ein, mit mir zu feiern! &Uuml;ber Geschenke braucht ihr euch den Kopf nicht zu zerbrechen (wenngleich ich eine Amazon-Wunschlist pflege...) -- bringt einfach was zu essen mit. F&uuml;r Getr&auml;nke und Knabberzeug sorge ich. Ein Klick auf das Banner bringt euch zur Party-Seite:</p>
<div class="serendipity_imageComment_center">
<div class="serendipity_imageComment_img">
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/gebparty2006.png" />
</div>
</div>
<div class="serendipity_imageComment_txt">F&uuml;r die Anmeldeseite einfach anklicken.</div></p>
</div>
