---
layout: post
title: "Someone, please get a needle"
microblog: false
guid: http://geewiz.micro.blog/2007/04/24/someone-please-get.html
date: 2007-04-24T18:15:00+0200
type: post
url: /2007/04/24/someone-please-get.html
---

<p>Could anyone please deflate <a href="http://comicstripblog.com/?p=331?">Comic Strip Blogger</a> I'm listening to Adam Curry's interview of CSB on episode 586 of <a href="http://www.dailysourcecode.com">The Daily Sourcecode</a> and I can't get the image of a blowfish out of my head. Remember the one in the fish tank from "Finding Nemo" that blew up everytime he got excited?</p>
<p>When I first noticed how people picked on him about a year ago, I thought "Come on folks, cut the guy some slack!" But over time, CSB's babble has been getting next to unbearable to me too, and now Adam is offering him an even bigger soap box.</p>
<p>This guy is nothing but a walking, talking image neurosis. In the interview, Adam's EU-resident "savant" is frantically babbling to just get his point across without looking right or left. In the part where they talk about Nokia, CSB calls his former employer "bastards that just have good marketing". Adam barely manages to throw in that he is really satisfied with how his E61i works and how using it is generally enjoyable. Oblivious to that short intelligent intermission, CSB continues his argument how Nokia only has good marketing. I really wonder why CSB hasn't yet started his own podcast where he can rant uninterruptedly for hours. Asked about what his problems with Nokia actually are, he staggers shortly and mutters something about "personal reasons". That's what I call a hidden agenda. And that from the guy that questions everone's intentions and doesn't hesitate to call people hypocrites.</p>
<p>Marc Yoshimoto Nemcoff did the right thing when he took apart CSB's overly inflated ego in Daily Source Code episode 585. Unfortunately, seeing how Adam is going to engage his regular callers even more, that beating CSB took (and reacted to with a sickening slimy "Okay, Marc, we're friends") obviously won't reduce his presence in future episodes. </p>
<p>But as Adam closes his pocast, "that's what the fucking fast forward is for". I'm afraid I'll have to use it more often in the future.</p>
