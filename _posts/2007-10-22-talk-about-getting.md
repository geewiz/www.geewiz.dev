---
layout: post
title: "Talk about Getting Things Done"
microblog: false
guid: http://geewiz.micro.blog/2007/10/22/talk-about-getting.html
date: 2007-10-22T17:39:59+0200
type: post
url: /2007/10/22/talk-about-getting.html
---

<p>If you're interested in personal productivity, you surely have already heard of "Getting Things Done", the task management concept by David Allen. Over on my blog "Selbstadministration.de", I put my <a href="http://www.selbstadministration.de/2007/10/13/vortrag-getting-things-done/">introductory talk about GTD</a> online. It's nearly half an hour long and in German.</p>
