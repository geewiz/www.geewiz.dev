---
layout: post
title: "Linux auf dem ThinkPad R52"
microblog: false
guid: http://geewiz.micro.blog/2005/12/07/linux-auf-dem.html
date: 2005-12-07T18:37:55+0200
type: post
url: /2005/12/07/linux-auf-dem.html
---

<p>Neuer Laptop, neue Herausforderungen! Mit dem ThinkPad R52 waren die ersten 24 Stunden allerdings sehr erfreulich, denn bislang funktioniert alles, was ich ben&ouml;tige, wunderbar. Sogar den Dual Head Modus hab ich in den Griff bekommen und in meinem <a href="http://wiki.jochen-lillich.de/?ThinkPadR52">Wiki</a> dokumentiert.</p>
