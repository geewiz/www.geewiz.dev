---
layout: post
title: "Working hours vs. living hours"
microblog: false
guid: http://geewiz.micro.blog/2013/06/03/working-hours-vs.html
date: 2013-06-03T12:52:00+0200
type: post
url: /2013/06/03/working-hours-vs.html
---
<p>Following up on my recent post on <a href="http://www.jochen-lillich.de/2013/05/tactics-against-burnout/">Tactics against burnout</a>, I'd like to shed some light on the aspect of working hours. In the tech space, it seems popular to boast with insane work durations. "Mine is longer than yours", anyone?</p>
<p>Rob Ashton disagrees with this behaviour, as do I. What he describes in his article "<a href="http://codeofrob.com/entries/a-note-on-working-hours-and-working-at-home.html">A note on working hours and working at home</a>" is basically an example of what is called "ROWE", a Results-Only Work Environment. (You can read all about ROWE in the book "<a href="http://www.amazon.com/Why-Work-Sucks-How-ebook/dp/B0010SKUP6">Why Work Sucks and How to Fix It: The Results-Only Revolution</a>".) In short, work should always be about the results you generate, not about the time you are (seemingly) busy. If you can finish your tasks within 5 hours, there's no sense in doing 8 more just to impress someone (whom anyway?).</p>
<p>I can't see any contradiction in his approach to the Github article with which he "passionately disagrees", though. The article is even titled "<a href="http://zachholman.com/posts/how-github-works-hours/">Hours are bullshit</a>" and in it, Zach Holman explains:</p>
<blockquote>
<p>"When you're in the right mindset, your best day of coding can trump weeks of frustrated keyboard-tapping.</p>
</blockquote>
<p>Again, it's not about how long you're busy but how effective you are. Both guys also have similar conclusions:</p>
<blockquote>
<p>Zach: "By allowing for a more flexible work schedule, you create an atmosphere where employees can be excited about their work."</p>
<p>Rob: "When I decide what I'm doing with regards to work (when summer hits), I find it hard to believe I'll be working at any company who is enforcing 9-5, or have some rigidly described "flexi-time" as part of their contract in an effort to seem cool."</p>
</blockquote>
<p>Okay, one sentence in Zach's post might be easily misunderstood:</p>
<blockquote>
<p>"Ultimately it should lead to more hours of work, with those hours being even more productive."</p>
</blockquote>
<p>In my experience, having the freedom to decide when to work often actually leads to more time spent working than in a normal 9-to-5 frame. Nobody is productive all the time, right? When I start to feel tired at 15:30, I don't have to spend the remaining time mindlessly staring at my screen or scrolling through Google Plus for another 90 minutes, just to leave as soon as the clock strikes 5. In the long run, this just makes people hate their job. Instead, I take a break, go for a walk or maybe even spend some time at home with my family. Rob obviously handles this similarly.</p>
<p>Now, if I don't have other important things, I'll continue where I left off later. I might now work for another 90 minutes, but maybe also for 120 or 180. Because I'm working at a time that's right for me and because my efforts have a visible effect. Which lets me keep loving my work.</p>
<p>Yes, this blurs the line between work and the other important things in my life. Because my work is actually one of them. And I think that's what Zach meant with "Working weekends blur into working nights into working weekdays, since none of the work feels like work." Work isn't something evil that keeps me from enjoying life, it's an integral part of it.</p>
<p>Or as Markus Cerenak puts it in "<a href="http://www.hafawo.at/selbstmanagement-motivation/warum-das-konzept-work-life-balance-ein-irrtum-ist/">Warum das Konzept &ldquo;Work-Life Balance&rdquo; ein Irrtum ist.</a>" -- "Why the concept of work/life balance is a falsity":</p>
<blockquote>
<p>"When you follow your passion, you don't have a need for balancing."</p>
</blockquote>
