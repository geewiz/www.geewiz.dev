---
layout: post
title: "Das Kind mit dem Bade ausgesch&uuml;ttet"
microblog: false
guid: http://geewiz.micro.blog/2006/03/25/das-kind-mit.html
date: 2006-03-25T07:10:00+0200
type: post
url: /2006/03/25/das-kind-mit.html
---

<p>Ich bin f&uuml;r sinnvolle Boykotts durchaus zu haben. Nestl&eacute; und Coca-Cola stehen ja schon eine Weile auf meiner schwarzen Liste. Und der Gedanke hinter Su-Shees <a href="http://sushee.geekheim.de/?p=932">DRM-Boykott</a> gef&auml;llt mir auch gut.</p>
<p>Aber wenn <a href="http://www.spiegel.de/wirtschaft/0">Quelle Lonsdale-Kleidung aus dem Programm nimmt</a>,1518,406981,00.html, weil die sich (wohl ob der Buchstabenfolge "NSDA") bei Neonazis gro&szlig;er Beliebtheit erfreut, dann frage ich mich schon, welchen Sinn diese Aktion hat. Wem n&uuml;tzt sie? Vielleicht dem eigenen Image, aber maximal f&uuml;r ein paar Tage. Wem schadet sie? H&ouml;chstens Lonsdale -- vorausgesetzt, die Glatzendeppen kaufen nicht einfach auf anderen Vertriebswegen. Und will man Lonsdale &uuml;berhaupt schaden? Die Firma signalisiert mit Nachdruck, dass sie ideologisch nicht in der rechten Ecke steht[1].</p>
<p>Bei aller politischer Kritikfreude: Boykott, dessen einzige Aussage ist "Wir geh&ouml;ren jedenfalls zu den Guten!", hinterl&auml;sst bei mir einen schalen Nachgeschmack.</p>
<p><em>Update:</em> KarstadtQuelle kam jetzt selbst drauf, dass der <a href="http://www.spiegel.de/wirtschaft/0">Boykott Unsinn</a>,1518,407803,00.html ist.</p>
<p>fn1. Siehe <a href="http://de.wikipedia.org/wiki/Lonsdale">Lonsdale</a> in der Wikipedia.</p>
