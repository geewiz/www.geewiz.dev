---
layout: post
title: "Datensch&uuml;tzer sind nicht blau&auml;ugig"
microblog: false
guid: http://geewiz.micro.blog/2006/05/13/datenschuumltzer-sind-nicht.html
date: 2006-05-13T03:47:05+0200
type: post
url: /2006/05/13/datenschuumltzer-sind-nicht.html
---

<p>"Weder im Grundgesetz noch in den sonstigen relevanten Verfassungen gibt es ein eigenes Grundrecht auf Sicherheit. Sicherheit hat nur eine dienende Funktion: die Freiheit zu sch&uuml;tzen." und "Wir haben das Recht, vom Staat und von der Wirtschaft in Ruhe gelassen zu werden." -- das sind nur zwei Zitate aus einem Artikel in "Technology Review", den Thilo Weichert vom Landeszentrum f&uuml;r Datenschutz Schleswig-Holstein verfasst hat.</p>
<p>Darin wendet er sich gegen die Vorw&uuml;rfe, er k&ouml;nne sich als Datensch&uuml;tzer einseitig auf das Thema Freiheit verlegen, w&auml;hrend die Politiker den Spagat zwischen Freiheit und Sicherheit schaffen m&uuml;ssten.</p>
<p>Dass es sich seine Kritiker damit etwas zu einfach machen, legt er in seiner Entgegnung mit dem Titel <a href="http://www.heise.de/tr/artikel/72937">&Uuml;berwachung ohne Transparenz f&ouml;rdert staatliche Willk&uuml;r</a> plausibel dar.</p>
