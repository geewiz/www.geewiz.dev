---
layout: post
title: "Finished my cyberdeck"
microblog: false
guid: http://geewiz.micro.blog/2020/05/10/finished-my-cyberdeck.html
date: 2020-05-10T14:54:30+0200
type: post
images:
- https://www.geewiz.dev/assets/img/d73cf4a111.jpg
photos:
- https://www.geewiz.dev/assets/img/d73cf4a111.jpg
url: /2020/05/10/finished-my-cyberdeck.html
---
GMK Night Runner, one of a few keycap sets I ordered last year, finally arrived, completing my cyberpunk-themed keyboard. The case is a Keyboardbelle Nouvelle limited edition. I used tactile T1 switches with 67g springs, lubed with Tribosys 3204. They're mounted in a carbon-fibre plate over a hot-swap HS60 PCB.

Because the case is 3D-printed, it sounded a little bit hollow. I was able to mitigate that by putting a thin foam envelope (previously used for packaging a plate or PCB) into the bottom of the case.

I'm very happy with how nicely everything came together, especially how the CF plate lets a lot of the RGBs shine through.

I've created a [gallery](https://imgur.com/gallery/0jKoRYk) with a few more pictures.


<img src="/assets/img/d73cf4a111.jpg" width="600" height="450" alt="" />
