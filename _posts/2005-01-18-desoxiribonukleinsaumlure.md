---
layout: post
title: "Desoxiribonukleins&auml;ure"
microblog: false
guid: http://geewiz.micro.blog/2005/01/18/desoxiribonukleinsaumlure.html
date: 2005-01-18T09:12:00+0200
type: post
url: /2005/01/18/desoxiribonukleinsaumlure.html
---

<p>Als ITler hat man es manchmal schwer, einem Kunden den Umfang einer L&ouml;sung klar zu machen: "Nein, so einfach ist eine hochverf&uuml;gbare Webserver-L&ouml;sung mit Datenbank nicht zu realisieren. Das wird richtig aufw&auml;ndig. Und teuer. Und Sie sollten sich gut &uuml;berlegen, ob der Nutzen wirklich den Kosten angemessen ist..."<br />
Daher kann ich mir gut vorstellen, wie es gerade den Kriminologen und Datensch&uuml;tzern geht. "Nein, lieber BDK, liebe GdP, so einfach ist das mit DNS-Proben aller Straff&auml;lligen nicht. Das wird richtig aufw&auml;ndig. Und teuer. Und ihr solltet euch gut &uuml;berlegen, ob der Nutzen..."</p>
<p>
Wenn sich der nieders&auml;chsische Innenminister Sch&uuml;nemann daf&uuml;r ausspricht, in Zukunft jedem (!) Straff&auml;lligen einen "genetischen Fingerabdruck" zu nehmen, dann hat er wohl noch nicht verstanden, dass dabei kein Stempelkissen im Spiel ist. Als Spitzenpolitiker kann man doch nicht ernsthaft glauben, dass der Aufwand und die Kosten, jedem Schwarzfahrer und Ladendieb eine DNS-Probe zu nehmen, zusammen mit der Einschr&auml;nkung derer Pers&ouml;nlichkeitsrechte, gerechtfertigt sind! Oder geht Herr Sch&uuml;nemann wirklich davon aus, dass in jedem H&uuml;tchenspieler die Anlagen zum Gewaltverbrecher stecken?</p>
<p>Selbst aus Bayern h&ouml;rt man leicht Gem&auml;&szlig;igteres: Innenminister Beckstein sieht den Bedarf einer DNA-Analyse "nur" dort, wo auch der physische Fingerabdruck genommen wird. Und das findet ja beileibe nicht bei jedem Schwarzfahrer statt.</p>
<p>Und dass man auf die Erfolge im Fall Mooshammer keineswegs gleich mit spontanem Hurra-Opportunismus reagieren muss, demonstrieren u.a. Stellungnahmen der Gr&uuml;nen und der FDP. Dort hat man offenbar das Urteil des BVG, das Genanalysen enge Grenzen setzt, besser verstanden.</p>
<p>Genanalysen k&ouml;nnen schon heute nicht nur Verbindungen zwischen Spuren und T&auml;tern herstellen, sie liefern gleich noch eine Vielzahl weiterer Informationen mit, &uuml;ber Krankheiten zum Beispiel. Alle diese Informationen auf unbegr&uuml;ndeten Verdacht hin zu erheben und zu speichern, schr&auml;nkt das Recht auf informationelle Selbstbestimmung in einer Weise ein, die vor allem in den Augen der Datensch&uuml;tzer nicht zu rechtfertigen ist. Dabei geh&ouml;rt diese zu den Freiheits- und B&uuml;rgerrechten, die laut unserer Verfassung auch Straff&auml;lligen grunds&auml;tzlich zustehen. Sie sch&uuml;tzen uns B&uuml;rger vor staatlicher Willk&uuml;r und vor Benachteiligung, sie sichern den Fortbestand unserer Demokratie. Eben diese Rechte werden jedoch derzeit unter dem Deckmantel von Sicherheit und Verbrechensbek&auml;mpfung immer weiter abgebaut. Das ist f&uuml;r mich nicht mehr als billiger Aktionismus und Populismus.</p>
<p>DNS-Analyse ist ganz klar eine starke Waffe im Kampf gegen Gewalt- und Sexualstraft&auml;ter. Sie deshalb gleich im Gie&szlig;kannenprinzip in allen F&auml;llen kleiner und gro&szlig;er Kriminalit&auml;t anwenden zu wollen, zeugt jedoch davon, dass ein hohes Amt in Politik und Beh&ouml;rden nicht mit hoher Kompetenz gleichzusetzen ist.</p>
<p>Links:</p>
<ul>
<li>
<a href="http://www.faz.net/s/Rub812F1B901A514F208C613F6B1B336BCC/Doc">FAZ</a>~EED4FA2C04F3A46D19B3360C9F3725BF2~ATpl~Ecommon~Scontent.html</li>
<li><a href="http://www.heise.de/newsticker/meldung/55209">Heise online</a></li>
</ul>
