---
layout: post
title: "SUSE 10 contra Skype"
microblog: false
guid: http://geewiz.micro.blog/2005/11/14/suse-contra-skype.html
date: 2005-11-14T07:47:00+0200
type: post
url: /2005/11/14/suse-contra-skype.html
---

<p>Wie <a href="http://blog.koehntopp.de/archives/1068-Skype-vs.-Linux-OSS.html">Iso</a> k&auml;mpfe auch ich momentan damit, Skype f&uuml;r Linux mit seinem veralteten OSS-Sound unter SUSE Linux 10.0 zum Funktionieren zu bringen.</p>
<p>Ich verwende f&uuml;r Skype ein Logitech USB Headset, das ich auch erst mit Hilfe des bei Kris beschriebenen <a href="http://juljas.net/linux/skype/">skype_dsp_hijacker</a> einigerma&szlig;en zum Laufen bekam.</p>
<p>Einigerma&szlig;en deshalb, weil bisher nur das Mikrofon funktioniert, w&auml;hrend die Kopfh&ouml;rer stumm bleiben. Momentan behelfe ich mir damit, dass ich meine Desktop-Lautsprecher zur Ausgabe nutze. Das ist aber nicht Sinn eines Headsets. :-( Offenbar haben auch andere dieses Problem, weshalb ich den <a href="https://bugzilla.novell.com/show_bug.cgi?id=132114">SUSE-Bugzilla</a> weiter beobachten werde.</p>
<p><em>Update:</em> In obigem Bugzilla-Eintrag steht inzwischen zu lesen, wie das USB-Problem durch einen Patch des USB-Audio-Moduls behoben werden kann.</p>
