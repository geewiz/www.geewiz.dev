---
layout: post
title: "Divide, relax and conquer"
microblog: false
guid: http://geewiz.micro.blog/2006/07/31/divide-relax-and.html
date: 2006-07-31T07:16:44+0200
type: post
url: /2006/07/31/divide-relax-and.html
---

<p>Seemingly, Kristian K&ouml;hntopp wasn't as weary as I was today. In his (obviously german) article <a href="http://blog.koehntopp.de/archives/1349-Leben-mit-Fehlern-der-Schluessel-zum-Scaleout.html">Leben mit Fehlern - der Schl&uuml;ssel zum Scaleout</a> ("Living with errors - the key to scale out"), he describes how to solve scalability problems by implementing software based on a service oriented architecture (SOA) and by loosening requirements.</p>
<p>He explains how too much restrictions, imposed by concepts like Two-Phase Commit or ACID(Atomicity, Consistency, Isolation, Durability), can make scaling an infrastructure a difficult task. Using WEB.DE, MySQL and Amazon as examples, Kris illustrates how using an architecture that employs small distributed services as building blocks, abandoning some of those tight restrictions, and dealing with a certain level of uncertainty or inconsistency makes distributing load and gaining performance a lot easier.</p>
<p>It's an article well worth reading.</p>
