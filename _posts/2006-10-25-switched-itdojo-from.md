---
layout: post
title: "Switched IT-Dojo from Joomla to Serendipity"
microblog: false
guid: http://geewiz.micro.blog/2006/10/25/switched-itdojo-from.html
date: 2006-10-25T17:23:25+0200
type: post
url: /2006/10/25/switched-itdojo-from.html
---

<p><a href="http://www.it-dojo.de">IT-Dojo</a> is my new website where I publish IT content like training materials and my netcast "Radio Perl".</p>
<p>First, I built the site with Joomla, mainly out of curiousity regarding the CMS's features. But I quickly found out that Joomla follows the "feature richness means complexity" rule, too.</p>
<p>Yesterday, while updating my and Carolins blog to the current <a href="http://www.s9y.org">Serendipity</a> version, I read through the extensive list of S9Y plugins and realized that <a href="http://www.it-dojo.de">IT-Dojo</a> will also mainly be a blog. So I decided to get rid of the complexity of Joomla and built the site new, now based on Serendipity. Installation was as easy as always and it took me only some more minutes to modify the default template with the IT-Dojo banner.</p>
<p>Simplicity rules. As does Serendipity. :-)</p>
