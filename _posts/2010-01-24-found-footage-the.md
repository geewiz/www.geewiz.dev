---
layout: post
title: "Found Footage: The Story of Macintosh"
microblog: false
guid: http://geewiz.micro.blog/2010/01/24/found-footage-the.html
date: 2010-01-24T21:27:04+0200
type: post
url: /2010/01/24/found-footage-the.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_medium_quote"><p>I found this VHS cassette while cleaning my office this week. This "Found Footage" comes from a video tape I received from Apple back in 1984 when the original 128K Mac was introduced. It was part of the authorized dealer training videos given to each store to help them become familiar with the Macintosh. You will see a very young Burrell Smith, Andy Hertzfeld, Phil Gibbons, Mitch Kapor, Bill Gates and Steve Jobs.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.tuaw.com/2010/01/18/found-footage-the-story-of-macintosh/">tuaw.com</a></div></p>
</div>
