---
layout: post
title: "Spendenaufruf"
microblog: false
guid: http://geewiz.micro.blog/2006/04/23/spendenaufruf.html
date: 2006-04-23T02:50:17+0200
type: post
url: /2006/04/23/spendenaufruf.html
---

<p>Die <a href="http://www.stiftung-warentest.de">Stiftung Warentest</a> muss sparen. Mindestens an Bindestrichen. Ich hab grade einen Artikel &uuml;ber Akupunktur im aktuellen Heft gelesen und fand darin schmerzhafte Konstrukte wie "Deutsche Akupunktur Gesellschaft". Und wer bitte ist dieser "Techniker Krankenkasse"?</p>
<p>Ich schlage vor, dass wir alle jeweils einen Divis "-" per E-Mail an die Stiftung schicken, damit sie diese kleinen Wortverbinder im n&auml;chsten Heft wieder verwenden kann.</p>
<p>Gottlob entschied sich der Autor wenigstens dagegen, mit ein paar mehr <a href="http://de.wikipedia.org/wiki/Deppenleerzeichen">Deppenleerzeichen</a> das Monster "Bundes &Auml;rzte Kammer" zu schaffen...</p>
