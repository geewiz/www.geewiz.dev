---
layout: post
title: "Die Waage h&auml;ngt schief"
microblog: false
guid: http://geewiz.micro.blog/2005/03/11/die-waage-haumlngt.html
date: 2005-03-11T08:41:33+0200
type: post
url: /2005/03/11/die-waage-haumlngt.html
---

<p>Es ist mal wieder Donnerstag, und ich hab mal wieder meine 40 Wochenstunden schon viel zu fr&uuml;h voll. Und wieder mal ging der Job vor -- und in diesem Fall die Bandprobe f&uuml;r mich fl&ouml;ten. Ich muss wichtig sein. Oder ein Trottel. Oder beides.</p>
<p>Tatsache ist, dass ich momentan einen sp&uuml;rbaren Anteil meiner Freizeit dem Job opfere. Auf der einen Seite wird dieser Einsatz erwartet, insbesondere in Notf&auml;llen. Allerdings h&auml;ufen die sich in letzter Zeit, und in gleichem Ma&szlig;e steigt der Unmut meiner Liebsten. Carolin sagte heute zu mir: "Loyalit&auml;t ist eines, aber du bist zu gutm&uuml;tig." Zum Teil mag das stimmen, aber es gibt nun mal auch &auml;u&szlig;ere Zw&auml;nge, zum Beispiel das Fehlen zweier Mitarbeiter in meinem Team.</p>
<p>Spass an Herausforderungen, Loyalit&auml;t, Kollegialit&auml;t -- das alles sind Gr&uuml;nde, die mich im Beruf antreiben. Aber da sind auch Liebe, Ruhe, Hobbies, Freundschaft und eine ganze Welt ausserhalb des Firmengeb&auml;udes.</p>
<p>Ich habe schmerzlich gelernt, dass Beruf und Privatleben auf die Dauer nur in Balance koexistieren k&ouml;nnen. Denn w&auml;hrend meiner Selbst&auml;ndigkeit war diese Balance oft gest&ouml;rt. Aber ich stelle fest, dass sie mir auch als leitender Angestellter nicht in den Scho&szlig; f&auml;llt. Offenbar ist es sogar Arbeit, daf&uuml;r zu sorgen, dass man auch mal nicht arbeitet.</p>
