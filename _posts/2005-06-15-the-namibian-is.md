---
layout: post
title: "The Namibian is back"
microblog: false
guid: http://geewiz.micro.blog/2005/06/15/the-namibian-is.html
date: 2005-06-15T06:36:57+0200
type: post
url: /2005/06/15/the-namibian-is.html
---

<p>Gestern morgen ist Carolin endlich wieder aus Namibia zur&uuml;ckgekommen und ich hab sie am Bahnhof in Wagh&auml;usel abgeholt. Weil ich erst um 14 Uhr in der Firma sein musste, konnte ich zumindest ein paar erste Stunden mit ihr verbringen. </p>
<p>Dass ich seither jedoch gar keine Gelegenheit mehr hatte, sie zu sehen, ist schon ziemlich hart. Aber leider wird mein Team bei WEB.DE noch bis Ende des Monats mit einem Gro&szlig;projekt besch&auml;ftigt sein und dabei regelm&auml;&szlig;ig die 40 Wochenstunden &uuml;berschreiten. Auch diesen Eintrag schreibe ich von Durlach aus. :-(</p>
<p>Wer Carolins interessante Berichte aus Namibia nicht per E-Mail bekommen hat, kann sie auch nachtr&auml;glich auf <a href="http://www.carolins-tagebuch.de">http://www.carolins-tagebuch.de</a> nachlesen.</p>
