---
layout: post
title: "Diversifying the cash flow"
microblog: false
guid: http://geewiz.micro.blog/2006/11/02/diversifying-the-cash.html
date: 2006-11-02T05:16:00+0200
type: post
images:
- /uploads/oven.serendipityThumb.jpg
photos:
- /uploads/oven.serendipityThumb.jpg
url: /2006/11/02/diversifying-the-cash.html
---

<p><a href="/uploads/oven.jpg" class="serendipity_image_link">
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/oven.serendipityThumb.jpg" />
</div>
<p></a></p>
<p>Well, look at that! Seems like the CTO of my former employer started a side business! Could that mean that their new product's success chances aren't as high as they predicted at their recent shareholders' meeting?</p>
<p>Let's hope that's wood burning, not stocks...</p>
