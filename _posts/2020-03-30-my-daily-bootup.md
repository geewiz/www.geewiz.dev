---
layout: post
title: "My Daily Bootup checklist"
microblog: false
guid: http://geewiz.micro.blog/2020/03/30/my-daily-bootup.html
date: 2020-03-30T22:03:00+0200
type: post
categories:
- "DEV"
images:
- https://www.geewiz.dev/assets/img/015b3e0b52.png
- https://www.geewiz.dev/assets/img/782e515101.png
photos:
url: /2020/03/30/my-daily-bootup.html
---
With such a huge number of people suddenly thrust into working from home, I thought I'd share a bit of my experience (more than 10 years, actually) with doing Remote Work. I started a new morning livestream called "[The Daily Bootup](https://www.twitch.tv/fullstacklive)". It's named after the checklist I use to start every workday. This checklist helps me keep track of my goals and to-dos, especially my most important tasks (aka "MIT"). This is how it looks like:

* Check my calendar for the day
* Check my weekly goals
* Check my to-do list
* Check other sources of work
* Schedule my MIT
* Write a 5-Minute Journal entry
* Send a team check-in

Let's take a look at each checklist item in detail!

## Check my calendar for the day
Before I can start planning my work for the day, I have to get a feel for how much of the day is already predetermined by appointments and repeating events. That's why I first check my calendar.

## Check my weekly goals
I use weekly goals to guide my focus. On Mondays, I define one or two important results I'd like to achieve by the end of the week. Every other weekday, I reference this definition (which I share with my team on Basecamp) to keep me on track.

## Check my to-do list
Unfortunately, I'm still living in a world in which there are tasks that I have to do no matter what, and deadlines that I haven't set myself. My to-do list helps preventing them from falling by the wayside.

## Check other sources of work
On top of my primary to-do list, there are other systems that tell me things that need to be pushed forward. Sales deals in our CRM and incident follow-ups are only two examples.

## Schedule my MIT
At this point, it's finally time to define today's work by entering tasks into my calendar. For me, a time boxing approach has been working best to make sure that I put in the work to get the important things done in time.  

## Write a 5-Minute Journal entry
Many people recommend picking up journaling as a daily practice for reflection and recording one's thoughts and feelings. Instead of doing free form writing, I have a 5-Minute-Journal template in [Day One](https://dayoneapp.com/) that asks me three things every morning: "What are you grateful for?", "What would make today great?" and "What's your daily affirmation?"

<img src="/assets/img/015b3e0b52.png" width="600" height="482" alt="5-Minute Journal" />

In my experience, a gratitude journal is a great way to keep myself on the positive side of things. Listing the things that are good in my life takes effect while I'm writing them down, and again when I read these entries two weeks or many years later.

## Send a team check-in
As the final step in my morning routine, I submit a form that our team uses to tell everyone what to expect of ourselves today. It does so on multiple levels because it lets us not only enter our main goal for the day but also our mood and additional information that might be helpful for dealing with us. I haven't yet tried to analyse the data. For now, it's just nice to quickly see who's feeling excited and who's already tired at breakfast time.

<img src="/assets/img/782e515101.png" width="316" height="437" alt="Team Check-In" />

So that's my Daily Bootup routine! Let me know on [Twitter](https://www.twitter.com/geewiz) or [micro.blog](https://micro.blog/geewiz) what you think of it. Or how about you drop by on [Full Stack Live](https://www.twitch.tv/fullstacklive)? I do "The Daily Bootup" weekdays from 9:30am to 10am (Ireland/UK time) and would love to have a chat!
