---
layout: post
title: "\"History Flow Visualization\" f&uuml;r Wikis"
microblog: false
guid: http://geewiz.micro.blog/2005/03/28/history-flow-visualization.html
date: 2005-03-28T22:21:01+0200
type: post
url: /2005/03/28/history-flow-visualization.html
---

<p><a href="http://de.wikipedia.org/wiki/Wiki">Wikis</a> sind sp&auml;testens mit der <a href="http://www.wikipedia.de">Wikipedia</a> als einfach zu bedienendes Werkzeug zur Dokumentation im Web bekannt geworden. Indem sie den b&uuml;rokratischen Aufwand auf ein Minimum reduzieren, erm&ouml;glichen sie es der Autorin, sich voll auf den Inhalt ihres Dokuments zu konzentrieren. Nicht nur im &ouml;ffentlichen WWW finden Wikis Verbreitung, auch Unternehmen wie mein Br&ouml;tchengeber setzen Wikis erfolgreich zur internen Dokumentation ein.</p>
<p>Nicht nur aus Archivierungsgr&uuml;nden, sondern auch, um auf Vandalismus reagieren zu k&ouml;nnen, bieten die meisten Wikis ein Versionierungssystem an, &uuml;ber das fr&uuml;here Zust&auml;nde eines Wiki-Dokuments eingesehen oder wiederhergestellt werden k&ouml;nnen.</p>
<p>IBM ver&ouml;ffentlichte jetzt die Java-Software <a href="http://www.alphaworks.ibm.com/tech/historyflow">History Flow Visualization</a>, die anhand der Versionshistorie die Entwicklung eines Wiki-Dokuments grafisch darstellt. Weil die sich &auml;ndernden Teile durch verschiedene Farben dargestellt werden, kann man gut erkennen, wo diese im Laufe der Zeit ver&auml;ndert, verschoben oder gar entfernt wurden. Damit erh&auml;lt man einen interessanten Einblick in die "Evolution" eines Dokuments.</p>
<p>Wikis spielen eine zunehmend wichtige Rolle beim Festhalten von Wissen. (Die <a href="http://www.roell.net/weblog/archiv/2005/03/21/einfachheit_ist_kein_motivationsfaktor.shtml">Gr&uuml;nde</a> daf&uuml;r werden zum Teil kontrovers diskutiert.) Aber erst an diesem Beispiel fiel mir auf, dass ihr Potenzial durch solche Erg&auml;nzungen noch deutlich gesteigert werden kann.</p>
