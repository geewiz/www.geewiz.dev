---
layout: post
title: "Web Application Framework f&uuml;r Perl"
microblog: false
guid: http://geewiz.micro.blog/2006/04/19/web-application-framework.html
date: 2006-04-19T23:58:22+0200
type: post
url: /2006/04/19/web-application-framework.html
---

<p>F&uuml;r ein gr&ouml;&szlig;eres Webprojekt, das ich jetzt angehen will, werde ich Perl als Programmiersprache verwenden. Meinen Kenntnissen der m&ouml;glichen Java-Frameworks traue ich einfach noch nicht in ausreichender Weise.</p>
<p>Aber auch in Perl gibt es Frameworks, die einem das Leben -- und vor allem das Entwickeln -- leichter machen. In letzter Zeit hat sich im Bereich Web-Anwendungen vor allem <a href="http://www.catalystframework.org/">Catalyst</a> einen Namen gemacht. Deshalb habe ich es auch f&uuml;r mein Projekt als Basis gew&auml;hlt.</p>
<p>W&auml;hrend die Installation sich aufgrund der Lawine an Abh&auml;ngigkeiten ganz sch&ouml;n hingezogen hat (CPAN ist aber einfach eine feine Sache), dauerte es etwa 40 Sekunden, bis die erste Seite der neuen Web-Anwendung in meinem Browser erschien. Ich bin beeindruckt!</p>
<p>Dass ein kleiner, auf Entwicklungszwecke spezialisierter Webserver mitgeliefert wird, finde ich eine prima Idee. Wenn auch das Framework selbst so entwicklerfreundlich ist, werde ich bis Juli einiges gebacken bekommen.</p>
