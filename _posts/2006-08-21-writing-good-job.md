---
layout: post
title: "Writing good job applications"
microblog: false
guid: http://geewiz.micro.blog/2006/08/21/writing-good-job.html
date: 2006-08-22T00:21:03+0200
type: post
url: /2006/08/21/writing-good-job.html
---

<p>Sometimes I get job applications that make me wonder if the applicant did even read the job description. Those are nothing more than a waste of paper (or storage space) -- and of my time, which is worse. There is only a small percentage of applications that make me curious about the respective person. Most just make me think "Okay, the cover letter is rubbish, but maybe the resume is better. Let's read on for another minute."</p>
<p>In his blog entry <a href="http://blog.guykawasaki.com/2006/08/dear_libby.html.">Dear Libby", Guy Kawasaki, former Chief Apple Evangelist, gives an example of "how to write an appealing job application</a> In the article, he refers to a previous blog entry in which he interviewed Libby Sartain, HR chief at Yahoo!, and now he shows how his own application would look if he applied to a job at that company.</p>
<p>It may be a bit intimidating to see what qualities a celebrity like Guy can refer to, but it's instructive nonetheless. I wish more job applications were as concise as his, even when they don't contain former jobs as a director or CEO.</p>
