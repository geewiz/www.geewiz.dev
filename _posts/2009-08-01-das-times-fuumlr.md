---
layout: post
title: "Das 1&times;1 f&uuml;r F&uuml;hrungskr&auml;fte und Manager"
microblog: false
guid: http://geewiz.micro.blog/2009/08/01/das-times-fuumlr.html
date: 2009-08-01T21:40:20+0200
type: post
url: /2009/08/01/das-times-fuumlr.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p>Die wahre Kunst des F&uuml;hrens liegt darin seinen Mitarbeitern und Kollegen F&uuml;hrungswissen und &ldquo;<a href="http://www.hafawo.at/category/fun-tools/">Werkzeuge</a>&rdquo; zur Verf&uuml;gung zu stellen, welche ihnen dabei helfen, sich Gedanken dar&uuml;ber zu machen, wie man die Angestellten f&uuml;hren kann, anstatt sie zu treiben.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.hafawo.at/beruf-und-karriere/das-1x1-fuer-fuehrungskraefte-und-manager/">hafawo.at</a></div>
<p>Grundlagenwissen, aber noch lange nicht selbstverst&auml;ndlich...</p>
</div>
