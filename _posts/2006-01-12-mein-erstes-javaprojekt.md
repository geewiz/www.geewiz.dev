---
layout: post
title: "Mein erstes Java-Projekt"
microblog: false
guid: http://geewiz.micro.blog/2006/01/12/mein-erstes-javaprojekt.html
date: 2006-01-12T02:42:12+0200
type: post
url: /2006/01/12/mein-erstes-javaprojekt.html
---

<p>Lernen durch Anwenden hat mich dahin gebracht, wo ich heute bin. Die Vielzahl an Technologien rund um J2EE kann man zwar im Web kennenlernen, aber ihre Anwendung, ihre Vor- und Nachteile muss man in der Praxis erfahren. Deshalb machte ich mich auf die Suche nach einer sinnvollen Anwendung.</p>
<p>Wer seinen <a href="http://www.firstmonday.org/issues/issue3_3/raymond/">ESR</a> gelesen hat, weiss: <a href="http://de.wikipedia.org/wiki/IT_Infrastructure_Library">Every good work of software starts by scratching a developer's personal itch." Und weil mich momentan das Thema "Configuration Management" (im Sinne von "ITIL</a>) juckt, heisst mein neues Projekt "comatose".Dabei soll der Name kein schlechtes Omen sein. Ich habe ihn als Akronym f&uuml;r "Configuration Management ToolSet" gew&auml;hlt, denn die Software soll die Verwaltung von IT-Infrastruktur unterst&uuml;tzen. Oder auf ITIL: die Software soll eine Configuration Management Database inklusive angeschlossener Dienste implementieren.</p>
<p>Innerhalb des Projekts will ich nicht nur Java, sondern auch professionelles Vorgehen &uuml;ben. Deshalb habe ich mich f&uuml;r Berlios als Projektplattform entschieden. &Uuml;ber die dort eingerichtete <a href="https://developer.berlios.de/projects/comatose/">Homepage</a> stehen praktische Projektwerkzeuge wie Bugtracking, Foren und Wiki zur Verf&uuml;gung, und den dortigen Webspace nutze ich zur Ver&ouml;ffentlichung des <a href="http://comatose.berlios.de/">comatose project manual</a>, das die Grundlage der Entwicklungsarbeiten sein wird.</p>
<p>Weil ich dabei nicht allein das Entwicklerteam spielen muss, freut es mich sehr, dass ich mit Kai schon einen Mitstreiter gefunden habe. Sollten da draussen noch mehr Leute sein, die sich mit uns auf den Weg machen wollen, sind sie eingeladen, mir ihren Berlios-Benutzernamen zu mailen.</p>
<p>Ich bin sehr gespannt, wie wir dabei vorankommen werden. Es wird sicher auch den einen oder anderen R&uuml;ckschlag oder Durchh&auml;nger geben. Aber in den letzten Wochen habe ich schon viel gelernt und ich freue mich drauf, weiter in diese Richtung zu gehen.</p>
