---
layout: post
title: "Tab Mix Plus Beta For Firefox 3.5"
microblog: false
guid: http://geewiz.micro.blog/2009/07/20/tab-mix-plus.html
date: 2009-07-20T14:45:03+0200
type: post
url: /2009/07/20/tab-mix-plus.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p><a href="https://addons.mozilla.org/en-US/firefox/addon/1122">Tab Mix Plus</a> is a popular Firefox add-on that stopped working when Firefox 3.5 came out, many users were disappointed and did not upgrade for that. [...] If you have been putting up upgrading Firefox for this reason, you can safely do that now and use the beta version of Tab Mix Plus.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://techie-buzz.com/firefox/tab-mix-plus-firefox-35.html">techie-buzz.com</a></div>
<p>Finally, I gained back control over my browser.</p>
</div>
