---
layout: post
title: "Computer science to be Leaving Cert option"
microblog: false
guid: http://geewiz.micro.blog/2017/02/06/computer-science-to.html
date: 2017-02-06T21:49:02+0200
type: post
url: /2017/02/06/computer-science-to.html
---
<p>The Irish Times reports that computer science <a href="https://www.e-pages.dk/irishtimes/1090/article/533413/7/4/render/?token=a09b00490c9f022efc8db6af44429fcf" title="The Irish Times: Computer science to be Leaving Cert option">gets an upgrade</a> in secondary education. That&#39;s very good news.</p>
