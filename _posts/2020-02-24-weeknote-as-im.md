---
layout: post
title: "Weeknote 8/2020"
microblog: false
guid: http://geewiz.micro.blog/2020/02/24/weeknote-as-im.html
date: 2020-02-22T11:10:00+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2020/02/24/weeknote-as-im.html
---
As I'm pretty much [Epoch](https://en.wikipedia.org/wiki/Unix_time) years old, I celebrated a special birthday this week. It’s interesting in this context that my interest in historic computer systems has never been higher. I want that [PiDP11](https://obsolescence.wixsite.com/obsolescence/pidp-11) so hard. I've just applied for a hobbyist OpenVMS license. And on my [live stream](https://www.twitch.tv/fullstacklive) today, I installed the ancient Unix V7 from a virtual tape.

I mentioned in my last weeknote that I was trying out [Brain.fm](https://www.brain.fm) to put focus-improving background noise into my head. During one of my uses, its selection page for choosing a style of music made me remember another familiar page: the "Music" page that [Calm](https://www.calm.com) added to their mindfulness app last year (I think?). While I've been using Calm for meditation for years, I've been using its music feature only for falling asleep easier. But since there's also a "Focus" section, I won't need to add another music app to my already long list of subscriptions.

That list still did get a new entry recently, though. Despite the fact that I'm fortunate in that I don't have to schedule many meetings and calls, my calendar is one of my most important productivity tools. Most of the entries there are appointments with myself that I schedule every morning. By time-boxing my most important tasks for the day, I make sure that I actually have the time to get them done. That's why I'm among the first subscribers of Fantastical 3 Premium, my calendar app of choice on all my devices.

My usual table at Starbucks where I sit to plan my day in the morning has a raised platform in the middle. Because of its more ergonomic height, it's the ideal place for my iPad. The downside: It makes touching the screen inconvenient because I have to lean in and reach out with my arm. Even though there are people who started using a mouse as soon as iPadOS 13 came out, I dismissed the iPad's mouse support because it was "only an accessibility feature". Now that I finally decided to try connecting a Logitech Master Anywhere mouse, I am wondering how many times my "doing it the proper way" has been getting in the way of actual improvement already. Setting up the mouse (with all its 5 buttons, no driver installation necessary) took 2 minutes and it's working perfectly. I can tap things easily, and with just a single click, I can go back to the home screen, switch to a different app or display the control center. With the iPad, the Happy Hacking Keyboard and the new mouse, I think I've found my mobile work endgame.
