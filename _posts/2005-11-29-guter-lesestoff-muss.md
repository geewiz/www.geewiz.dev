---
layout: post
title: "Guter Lesestoff muss nicht teuer sein"
microblog: false
guid: http://geewiz.micro.blog/2005/11/29/guter-lesestoff-muss.html
date: 2005-11-29T03:49:11+0200
type: post
url: /2005/11/29/guter-lesestoff-muss.html
---

<p>Nur wenige Tage, nachdem ich mich daf&uuml;r entschied, meine <a href="http://www.perl-programmieren.de">Perl-Kursunterlagen</a> freizugeben, macht sich Stefan M&uuml;nz[1] in seinem Weblog Gedanken &uuml;ber die <a href="http://aktuell.de.selfhtml.org/weblog/schenk_kultur_verlage.">Schenk-Kultur im Internet</a> </p>
<p>Seine Feststellung "Dass auf Autorenseite der Wunsch nach technischer Aufkl&auml;rung st&auml;rker sein kann als der nach unmittelbarem Profit, blieb und bleibt immer noch vielen Anwendern unverst&auml;ndlich." kann ich gut nachvollziehen. F&uuml;r diese Form des Idealismus erntet ja auch die OpenSource-Gemeinde immer wieder Unverst&auml;ndnis.</p>
<p>Anlass f&uuml;r Stefans Blogeintrag ist, dass sich nach einigen anderen Verlagen auch Addison-Wesley f&uuml;r das Thema "online kostenlos verf&uuml;gbare B&uuml;cher" interessiert und jetzt mit seinem Buch "Professionelle Websites" einen Versuchsballon startet.</p>
<p>fn1. Stefan M&uuml;nz wurde bekannt f&uuml;r sein Online-Kompendium <a href="http://www.selfhtml.org/">SelfHTML</a>, das inzwischen von einem ganzen Team betreut wird und das es ebenfalls als gedrucktes Buch zu kaufen gibt.</p>
