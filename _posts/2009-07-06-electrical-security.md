---
layout: post
title: "Electrical security"
microblog: false
guid: http://geewiz.micro.blog/2009/07/06/electrical-security.html
date: 2009-07-06T11:16:42+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpgallerywormulonnetpics20090619udaipur120020090619164250jpg_opckwhqcamcqngw-scaled1000.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpgallerywormulonnetpics20090619udaipur120020090619164250jpg_opckwhqcamcqngw-scaled1000.jpg?w=300
url: /2009/07/06/electrical-security.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<a href="/assets/wp-content/2011/05/media_httpgallerywormulonnetpics20090619udaipur120020090619164250jpg_opckwhqcamcqngw-scaled1000.jpg"><img alt="Media_httpgallerywormulonnetpics20090619udaipur120020090619164250jpg_opckwhqcamcqngw" height="333" src="/assets/wp-content/2011/05/media_httpgallerywormulonnetpics20090619udaipur120020090619164250jpg_opckwhqcamcqngw-scaled1000.jpg?w=300" width="500" /></a>
</div>
<div class="posterous_quote_citation">via <a href="http://gallery.wormulon.net/album.php?id=669&amp;num=252&amp;res=1200">gallery.wormulon.net</a></div>
<p>With "security" meaning "It's secured that this fuse will not blow".</p>
</div>
