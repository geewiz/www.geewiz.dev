---
layout: post
title: "Verliehen, verloren"
microblog: false
guid: http://geewiz.micro.blog/2005/01/07/verliehen-verloren.html
date: 2005-01-07T06:35:00+0200
type: post
url: /2005/01/07/verliehen-verloren.html
---

<p>Gerade ich, der manchmal so vergesslich ist, sollte mir alles aufschreiben. Vor allem, was ich verleihe. Heute wollte ich f&uuml;r den kommenden DSL-Anschluss das Netzwerkkabel verlegen, stellte aber fest, dass mein Netzwerk-Testset nicht mehr da ist.</p>
<p>Ich erinnere mich dunkel, es mal verliehen zu haben. Aber alle, die ich bisher angesprochen hatte, verneinten.</p>
<p>Sollte also jemand da draussen ein 2-teiliges Testset f&uuml;r Netzwerkkabel haben, das ihm nicht geh&ouml;rt, so k&ouml;nnte es vielleicht meins sein. Ich w&auml;re dann f&uuml;r eine Meldung sehr dankbar.</p>
