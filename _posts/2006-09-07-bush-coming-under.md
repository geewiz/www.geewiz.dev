---
layout: post
title: "Bush coming under fire"
microblog: false
guid: http://geewiz.micro.blog/2006/09/07/bush-coming-under.html
date: 2006-09-07T21:37:59+0200
type: post
url: /2006/09/07/bush-coming-under.html
---

<p>Replacing freedom with fear is not always a sure recipe for political success. At least not in America -- I guess in Germany it will still take citizens some more time to realize how they're dealt with.</p>
<p>The United States president though has to deal with a growing amount of open criticism. Criticism like that of <a href="http://www.crooksandliars.com/2006/09/05/olbermanns-latest-special-comment-targets-bush-">Keith Olbermann</a>%e2%80%9chave-you-no-sense-of-decency-sir%e2%80%9d/ from MSNBC's Countdown show:</p>
<blockquote class="posterous_medium_quote">
<p>More over, Mr. Bush, you are accomplishing in part what Osama Bin Laden and others seek -- a fearful American populace, easily manipulated, and willing to throw away any measure of restraint, any loyalty to our own ideals and freedoms, for the comforting illusion of safety.</p>
</blockquote>
<p>And regarding Bush's comparison between Al Quaeda and Nazi Germany, Olbermann just poses one question: "Have you no sense of decency, sir?"</p>
