---
layout: post
title: "Seltsam vertraut"
microblog: false
guid: http://geewiz.micro.blog/2006/02/08/seltsam-vertraut.html
date: 2006-02-08T20:35:37+0200
type: post
url: /2006/02/08/seltsam-vertraut.html
---

<p>"H&auml;? Seit wann habe ich Klingon-Charts auf dem MP3-Player?" Das war der Gedanke, der mir heute morgen im Zug durch den Kopf ging. Was ich da h&ouml;rte, klang wirklich nach einem von einem klingonischen Duo dargebrachten Popsong.</p>
<p>Aber auf dem Display stand dann doch nur "Caveman -- Mike Oldfield -- Tubular Bells 2003".</p>
