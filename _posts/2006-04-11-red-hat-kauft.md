---
layout: post
title: "Red Hat kauft JBoss"
microblog: false
guid: http://geewiz.micro.blog/2006/04/11/red-hat-kauft.html
date: 2006-04-11T20:08:00+0200
type: post
url: /2006/04/11/red-hat-kauft.html
---

<p>Die Ger&uuml;chte sind wahr: Linux-Distributor Red Hat kauft JBoss, den Hersteller Java-basierter Middleware. Als Betrag stehen mindestens 350 Millionen Dollar im Raum. Mit dem Kauf will Red Hat das Gewicht von Open Source Software im Bereich SOA(Service Oriented Architecture) verst&auml;rken.</p>
<p>Von Red Hat habe ich in letzter Zeit kaum Aktivit&auml;ten am Markt mitbekommen. Umso erstaunter bin ich von diesem Schachzug. Grund genug, mal wieder nachzuschauen, wo meine wenigen, vor Jahren schmerzhaft abgesackten RHAT-Aktien stehen.</p>
<p><em>Update:</em> Interessant, wie 350*10^6 Dollar <a href="http://www.theregister.co.uk/2006/04/10/fleury_redhat_critic/">Meinungen ver&auml;ndern</a> k&ouml;nnen. :-)</p>
