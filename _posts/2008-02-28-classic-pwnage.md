---
layout: post
title: "Classic pwnage"
microblog: false
guid: http://geewiz.micro.blog/2008/02/28/classic-pwnage.html
date: 2008-02-28T06:52:56+0200
type: post
url: /2008/02/28/classic-pwnage.html
---

<p>It may not be totally hot news any more, but if something's giving me an asthma attack from laughing, it has to be put here.</p>
<p>The setting: Jimmy Kimmel is a US talk show host that's made a routine of making snide remarks on Matt Damon. He even invites him as a guest but then doesn't call him up because, unfortunately, "we're out of time". It must have been a bummer when his girlfriend, comedian Sarah Silverman, sent him this video message:</p></p>
<p>But not much time later, Kimmel took sweet revenge...</p>
<p>[youtube=http://www.youtube.com/watch?v=sIQrBouWRiE&amp;rel=1]</p>
