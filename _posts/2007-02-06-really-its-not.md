---
layout: post
title: "Really, it's not a cow!"
microblog: false
guid: http://geewiz.micro.blog/2007/02/06/really-its-not.html
date: 2007-02-06T23:38:20+0200
type: post
images:
- /uploads/tauren.jpg
photos:
- /uploads/tauren.jpg
url: /2007/02/06/really-its-not.html
---

<p>Although I keep trying to explain the issue to them, my colleagues seem to have a really hard time to understand that a Tauren druid is in so many ways different from a cow.</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/tauren.jpg" />
</div></p>
<p>And judging from the "enhancements" I got as a present last week, it's even more difficult for them to distinguish between me and my World of Warcraft character than it is for me.</p>
