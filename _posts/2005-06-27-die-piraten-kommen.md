---
layout: post
title: "Die Piraten kommen wieder"
microblog: false
guid: http://geewiz.micro.blog/2005/06/27/die-piraten-kommen.html
date: 2005-06-27T20:55:59+0200
type: post
url: /2005/06/27/die-piraten-kommen.html
---

<p>Und als ob Johnny Depp als Captain Jack Sparrow nicht abgedreht genug gewesen w&auml;re -- nein, im <a href="http://www.scifi.com/scifiwire2005/index.php?id=31374">Nachfolgefilm</a> wird Keith Richards Sparrows Vater spielen. Falls er neben der Stones-Tour die Zeit dazu findet.</p>
