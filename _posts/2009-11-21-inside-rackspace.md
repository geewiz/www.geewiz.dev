---
layout: post
title: "Inside Rackspace"
microblog: false
guid: http://geewiz.micro.blog/2009/11/21/inside-rackspace.html
date: 2009-11-21T20:45:34+0200
type: post
url: /2009/11/21/inside-rackspace.html
---

<p>This short clip takes a peek into the <a href="http://www.rackspace.com">Rackspace Hosting</a> headquarters. They've just opened the 120,000 square foot second phase of their headquarters facility within a former shopping mall in San Antonio.</p>
<p>[youtube=http://www.youtube.com/watch?v=5mZcuY0I_yQ&amp;hl=de_DE&amp;fs=1&amp;]</p>
<p>(via <a href="http://www.datacenterknowledge.com/archives/2009/11/16/rackspace-expands-its-headquarters/">DataCenterKnowledge</a>)</p>
