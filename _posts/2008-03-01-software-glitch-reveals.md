---
layout: post
title: "Software glitch reveals new president prematurely"
microblog: false
guid: http://geewiz.micro.blog/2008/03/01/software-glitch-reveals.html
date: 2008-03-01T21:22:08+0200
type: post
url: /2008/03/01/software-glitch-reveals.html
---

<p>Now that the Oscars are over, there'll be another show from the US waiting for us in November. But due to an unfortunate mishap, its entertainment value could just have gone down the drain.</p>
<p>The Onion News Network reveals why those electronic voting machines need to be banned, just like many have been requesting for years:</p>
<p><a href="http://www.theonion.com/content/video/diebold_accidentally_leaks?utm_source=embedded_video">Diebold Accidentally Leaks Results Of 2008 Election Early</a></p>
