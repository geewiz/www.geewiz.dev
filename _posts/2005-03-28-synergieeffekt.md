---
layout: post
title: "Synergieeffekt"
microblog: false
guid: http://geewiz.micro.blog/2005/03/28/synergieeffekt.html
date: 2005-03-29T01:30:25+0200
type: post
url: /2005/03/28/synergieeffekt.html
---

<p>Wer -- wie einige meiner Kollegen -- gleichzeitig mehrere Rechner mit unterschiedlichen Betriebssystemen betreibt, muss entweder pro Rechner den Platz f&uuml;r je einen Monitor, eine Tastatur und eine Maus opfern oder einen KVM-Switch benutzen.</p>
<p>Auf eine weitere Alternative bin ich gerade gesto&szlig;en: hat jeder Rechner seinen eigenen Monitor, gen&uuml;gt beim Einsatz der f&uuml;r Unix, Windows und MacOS verf&uuml;gbaren Software <a href="http://synergy2.sourceforge.net/">Synergy</a> eine Tastatur und eine Maus. Zum Umschalten auf den anderen Rechner reicht es dann, die Maus &uuml;ber den Rand des einen Bildschirms hinweg auf den anderen zu bewegen.</p>
<p>Zus&auml;tzlich werden die Bildschirmschoner aller Rechner synchronisiert. Sind sie durch ein Passwort gesch&uuml;tzt, muss dieses nur auf einem der Rechner eingegeben werden.</p>
<p>Nicht jeder wird bereit sein, pro Rechner einen Monitor aufzustellen. Dennoch finde ich Synergy eine interessante Idee!</p>
