---
layout: post
title: "Es sind die kleinen Dinge"
microblog: false
guid: http://geewiz.micro.blog/2006/04/06/es-sind-die.html
date: 2006-04-06T23:46:59+0200
type: post
url: /2006/04/06/es-sind-die.html
---

<p>Erfreuliche Erkenntnis: statt an E-Mails angeh&auml;ngte Word-Dateien herunterzuladen und darauf zu warten, dass die Textverarbeitung zu ihrer Anzeige geladen ist, kann man in <a href="http://mail.google.com/">Google Mail</a> einfach auf die automatisch erzeugte HTML-Version der Textdatei klicken und kann noch in der selben Sekunde mit dem Lesen anfangen.</p>
<p>Ein Postfach, das statt viel Schnickschnack kleine, aber sinnvolle Funktionen bietet, die mir Zeit sparen -- sowas begeistert mich.</p>
