---
layout: post
title: "Ausweitung der DNS-Analyse gebremst"
microblog: false
guid: http://geewiz.micro.blog/2005/03/19/ausweitung-der-dnsanalyse.html
date: 2005-03-19T03:53:17+0200
type: post
url: /2005/03/19/ausweitung-der-dnsanalyse.html
---

<p>Der Bundestag ist dagegen, dass Staatsanwaltschaft und Polizei zuk&uuml;nftig f&uuml;r alle ihre Ermittlungen den genetischen Fingerabdruck einsetzen. Wie der <a href="http://www.spiegel.de/politik/deutschland/0">Spiegel</a>,1518,347108,00.html berichtet, lehnten Bundesregierung, Koalition und FDP eine entsprechende Forderung der Union ab.</p>
<p>Auch wenn das Thema damit nicht vom Tisch ist, bin ich erleichtert. Immerhin hat es den Anschein, als wolle die Regierung erst einmal sorgf&auml;ltig pr&uuml;fen, wo der Einsatz der neuen Technologie seinen Sinn hat und der Konflikt zu den Grundrechten dem Ziel der Verbrechensbek&auml;mpfung und -pr&auml;vention angemessen ist.</p>
