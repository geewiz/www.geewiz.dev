---
layout: post
title: "Always wear a podcast"
microblog: false
guid: http://geewiz.micro.blog/2006/06/21/always-wear-a.html
date: 2006-06-21T03:37:41+0200
type: post
url: /2006/06/21/always-wear-a.html
---

<p>Today, my walk from the train station home was quite a bit longer than usual. Because it was the train station of another town.</p>
<p>Thats's what happened: Soon after boarding the train in Karlsruhe, due to the temperature inside I got rather weary and I dozed off several times. Unfortunately, one time too many: when the train stopped once again, I realized that I had missed the station where I had had to change trains. I was already in Wiesental, a neighboring town of Philippsburg, and on my way to Mannheim. At least, there was enough time to leave the train here.</p>
<p>Calling my folks to pick me up was not an option -- they were watching Germany winning against Ecuador just now. Well, I had intended to go jogging this evening anyway, so I decided to just walk the few kilometres home. Weather was great, and I had put some new podcasts[1] on my iPod.</p>
<p>That's what I like about podcasts: they give you an opportunity to have fun or even learn something while you can't do anything else (or don't want to). In those 25 minutes it took me to get home, I listened to an interview with Tom Limoncelli, author of <a href="http://www.jochen-lillich.de/buchtipps/time-management-for-system-administrators">Time Management for System Administrators</a>, and learned about <a href="http://svk.elixus.org/.">SVK</a> If there's nothing else you can do, you can always listen and learn. Try it out, it's easy!</p>
<p>On another note, I'm quite pleased that I now can sustain a fast-paced walk over 25 minutes without problems. Not more than a few weeks ago, even rushing to my local train station was giving me cramps!</p>
<p>fn1. For those who don't know: a podcast is an audio file of people talking about stuff, like in a radio show. There are podcasts on all topics you can imagine on the internet.</p>
