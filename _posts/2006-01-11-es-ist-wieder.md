---
layout: post
title: "Es ist wieder soweit"
microblog: false
guid: http://geewiz.micro.blog/2006/01/11/es-ist-wieder.html
date: 2006-01-11T19:37:35+0200
type: post
url: /2006/01/11/es-ist-wieder.html
---

<p>Der Freitag naht. Und er weckt <a href="http://blog.pommesbude.org/archives/178-Paraskavedekatriaphobie.html.">keine guten Erinnerungen</a> Der letzte Freitag dieser Art brachte n&auml;mlich ein arbeitsreiches Pfingstwochenende.</p>
<p>Gut, dass ich nicht abergl&auml;ubisch bin. Sollte ich Urlaub beantragen, ist das reiner Zufall.</p>
