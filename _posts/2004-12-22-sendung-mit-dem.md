---
layout: post
title: "Sendung mit dem Spreeblick erkl&auml;rt Jamba"
microblog: false
guid: http://geewiz.micro.blog/2004/12/22/sendung-mit-dem.html
date: 2004-12-22T03:14:00+0200
type: post
url: /2004/12/22/sendung-mit-dem.html
---

<p>Wer immer schon mal wissen wollte, wie man millionenschwere Fernsehwerbung finanziert, kann bei der "Sendung mit dem Spreeblick" etwas lernen.</p>
<p>Der bissige <a href="http://spreeblick.de/wp/index.php?p=324">Artikel &uuml;ber Jamba</a> -- die Firma, zwischen deren Werbung MTV Musikvideos schaltet -- schl&auml;gt derzeit heftige Wellen in der Blogwelt.</p>
<p>Noch scheint im Gesch&auml;ft mit Klingelt&ouml;nen ein Haufen Geld zu stecken --  wenn man darauf baut, dass das Kleingedruckte nicht gelesen wird.</p>
