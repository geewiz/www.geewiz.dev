---
layout: post
title: "Nicht mehr radioaktiv"
microblog: false
guid: http://geewiz.micro.blog/2005/03/29/nicht-mehr-radioaktiv.html
date: 2005-03-30T00:09:34+0200
type: post
url: /2005/03/29/nicht-mehr-radioaktiv.html
---

<p>Das Internet verdr&auml;ngt bei mir immer mehr die anderen Medien. Nachdem ich schon seit geraumer Zeit mehr per E-Mail und Chat kommuniziere als per Telefon und mich mehr aus RSS-Feeds als aus Zeitung und Nachrichten informiere, hat jetzt auch das Radio keine guten Karten mehr.</p>
<p>Gestern habe ich <a href="http://www.xmms.org">XMMS</a> per Plugin an <a href="http://www.audioscrobbler.com">Audioscrobbler</a> angebunden. Dort wird aus den Liedern, die ich anh&ouml;re, ein Profil erstellt. Dieses dient wiederum <a href="http://www.last.fm">Last.FM</a> dazu, mir einen ma&szlig;geschneiderten MP3-Stream frei Haus zu liefern. Lieder, die mir gefallen, stecke ich in die "Loved" Liste und das Zeug, das ich auf keinen Fall mehr h&ouml;ren will, wird mit einem Bann belegt. Auf diese Weise ist Last.FM immer besser in der Lage, meine Vorlieben mit den Profilen anderer Benutzer zu vergleichen und mir sinnvolle Vorschl&auml;ge zu machen.</p>
<p>Damit k&ouml;nnte endlich Schluss sein mit albernen Moderationen, mit Gelaber und Jingles mitten in meinen Lieblingsliedern, mit d&auml;mlicher Werbung, vor allem mit der ewig gleichen Charts&uuml;lze. Statt dessen bekomme ich Musik auf die Ohren, die mir gef&auml;llt -- und die ich in vielen F&auml;llen bisher nur selten oder nie geh&ouml;rt habe.</p>
<p>Meine <a href="http://www.audioscrobbler.com/user/Geewiz/">pers&ouml;nliche Playlist</a> ist nat&uuml;rlich online einsehbar.</p>
