---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2021/05/13/after-all-these.html
date: 2021-05-13T18:59:01+0200
type: post
url: /2021/05/13/after-all-these.html
---
After all these years, it still doesn't stop being fun when articles like "[How I navigate tmux in 2021](https://dev.to/waylonwalker/how-i-navigate-tmux-in-2021-2ina)" teach me a few tiny bits to improve my setup.
