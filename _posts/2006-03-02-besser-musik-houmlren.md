---
layout: post
title: "Besser Musik h&ouml;ren mit Amarok"
microblog: false
guid: http://geewiz.micro.blog/2006/03/02/besser-musik-houmlren.html
date: 2006-03-02T07:44:42+0200
type: post
url: /2006/03/02/besser-musik-houmlren.html
---

<p>Ich muss sagen, ich bin beeindruckt. Die KDE-Musikbox "Amarok" bietet wirklich weit mehr als nur das Abspielen der eigenen MP3- oder OGG-Dateien.</p>
<p>Dass es m&ouml;glich ist, durch Kopplung mit <a href="http://www.last.fm">last.fm</a> die eigenen H&ouml;rgewohnheiten zu registrieren und mit denen anderer abzugleichen, wusste und nutzte ich schon l&auml;nger.</p>
<p>Inzwischen ist Amarok aber auch in der Lage, aus den so gewonnenen Erkenntnissen selbst&auml;ndig Titellisten zusammenzustellen. Man gibt einfach eine gewisse Menge von Titeln vor, und f&uuml;r jedes abgespielte St&uuml;ck f&uuml;gt Amarok ein neues, passendes aus der Sammlung hinzu. Anders als bei rein zuf&auml;llig zusammengestellten Listen brauche ich so keine Angst zu haben, dass ich erst mit Enya eingelullt und dann pl&ouml;tzlich mit Rob Zombie in den Infarkt geschubst werde. "And once again, the Internet saves the day!"</p>
