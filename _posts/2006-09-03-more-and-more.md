---
layout: post
title: "More and more Skype phones"
microblog: false
guid: http://geewiz.micro.blog/2006/09/03/more-and-more.html
date: 2006-09-03T06:04:18+0200
type: post
url: /2006/09/03/more-and-more.html
---

<p>While not at all the first to present a Skype phone, Philips could very well claim having the best-looking autonomous handset with their <a href="http://crunchgear.com/2006/08/31/skype-and-philips-get-busy-ditch-computer/.">VOIP841</a> (I disagree with the CrunchGear article that says the USRobotics USR9631 was operable without a PC, because it appears to me as using the USB port to connect to Skype.)</p>
<p>With <a href="http://www2.panasonic.com/webapp/wcs/stores/servlet/prModelDetail?storeId=11301">Panasonic</a>&amp;catalogId=13251&amp;itemId=98296&amp;modelNo=Content08312006061130726&amp;surfModel=Content08312006061130726, yet another manufacturer enters the ring. But it seems they haven't got very far into the market, since they don't even have a name for their Skype phone yet.</p>
