---
layout: post
title: "Simon's cat does it again"
microblog: false
guid: http://geewiz.micro.blog/2008/07/22/simons-cat-does.html
date: 2008-07-23T01:02:17+0200
type: post
url: /2008/07/22/simons-cat-does.html
---

<p>It's <a href="http://www.jochen-lillich.de/strangely-familiar-simon039s-cat">that animal</a> again that every cat lover recognizes as his own! This time, Simon answers the question often asked by bewildered visitors: "How come your cat has her own couch while you don't?"</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=s13dLaTIHSg])</p>
<p>(Via <a href="http://fudder.de/index.php?id=163&amp;tx_ttnews%5Btt_news%5D=8761">fudder</a>)</p>
