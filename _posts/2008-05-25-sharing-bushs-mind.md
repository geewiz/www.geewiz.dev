---
layout: post
title: "Sharing Bush's mind"
microblog: false
guid: http://geewiz.micro.blog/2008/05/25/sharing-bushs-mind.html
date: 2008-05-26T01:37:35+0200
type: post
url: /2008/05/25/sharing-bushs-mind.html
---

<p>The article "<a href="http://www.heise.de/tp/r4/artikel/27/27887/1.html">Krieg hei&szlig;t jetzt Friedenserzwingung</a>" on Telepolis facilitates a view into the minds of two of Germany's leading (and ruling) political parties CDU (Christian Democratic Union) and CSU (Christian Social Union), as explained in their paper "Security Strategy for Germany".</p>
<p>Citing Telepolis:</p>
<blockquote class="posterous_medium_quote">
<p>'The state's foremost reponsibility is security', said Andreas Schockenhoff, deputy chairman of the CDU/CSU parliamentary party, in his speech before the security congress. Nobody disagreed. That says a lot about the CDU's understanding of state and democracy. The Grundgesetz [i.e. the German constitution] obviously takes another angle: 'Human dignity is not infringeable. To respect and protect it is every state power's obligation.' Nothing to be seen about 'security', much more about 'freedom', though.</p>
</blockquote>
<p>As neo-imperialistic as the CDU portrays their approach to internal and foreign affairs, as much creativity they show in their choice of words. Their strategic paper doesn't talk of war, oh no.</p>
<p>Instead, it talks of "enforced peace".</p>
