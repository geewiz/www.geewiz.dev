---
layout: post
title: "CAJ Leitungsrat 2004"
microblog: false
guid: http://geewiz.micro.blog/2004/11/08/caj-leitungsrat.html
date: 2004-11-08T06:16:00+0200
type: post
url: /2004/11/08/caj-leitungsrat.html
---

<p>An diesem Wochenende fand in Ludwigshafen der erste Leitungsrat des CAJ Bundesverbands statt. Ich war mit Daniel mit dabei und finde, dass es sich gelohnt hat.Am Freitag Abend waren wir nach einer Odyssee durch das idyllische Ludwigshafen im Heinrich-Pesch-Haus angekommen. Der entscheidende Unterschied zu den fr&uuml;heren Bundeskonferenzen fiel uns dort sofort ins Auge: es sind bedeutend weniger Leute, weil jede Di&ouml;zese nur noch 2 Ehrenamtliche in den Rat entsendet. Da einigen Di&ouml;zesen aus Krankheits- oder Termingr&uuml;nden nicht einmal das m&ouml;glich war, kam es uns erst mal ziemlich leer vor. </p>
<p>Aber dieser Unterschied ist auch ein klarer Vorteil, denn ich konnte mir schnell einen &Uuml;berblick &uuml;ber die Teilnehmer machen und die Arbeit im Plenum funktionierte stets einwandfrei. Die tolle Diskussionskultur wurde am Ende auch ausdr&uuml;cklich gelobt.</p>
<p>Der erste Leitungsrat sorgte mit einem gro&szlig;en Wahlen-Block f&uuml;r eine neue Verbandsleitung und verabschiedete seine neue Gesch&auml;ftsordnung. Ab jetzt leiten Heike Reisert und Arndt Schr&ouml;ders als ehrenamtliche Bundesvorsitzendende, verst&auml;rkt durch Sarah Winkens als Stellvertretung, den Verband. Mit Gregor Giehrlich ist im kommenden Jahr auch die kompetente Nachfolge von Joachim als Bundessekret&auml;r gesichert.</p>
<p>Die gestrige Party nach dem gemeinsamen Gottesdienst musste ich heute erst mal verdauen -- herzliche Gr&uuml;&szlig;e nach Hamburg und ins Allg&auml;u! :)</p>
<p>Wir haben gemerkt, dass es in den Details noch Verbesserungen geben muss, aber die neue Struktur ist ganz klar ein gewaltiger Schritt in die richtige Richtung.</p>
