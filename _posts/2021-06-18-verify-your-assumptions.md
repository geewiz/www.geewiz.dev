---
layout: post
title: "Verify your assumptions"
microblog: false
guid: http://geewiz.micro.blog/2021/06/18/verify-your-assumptions.html
date: 2021-06-18T16:42:24+0200
type: post
categories:
- "DevOps"
- "DEV"
tags: [devops]
url: /2021/06/18/verify-your-assumptions.html
---
That's what I keep preaching to my team but still fail at myself every so often.

This week, I wasted a whole day implementing a fix for a bug that wasn't there. My task was to add one line to a configuration file in a Chef cookbook. When I ran its test suite, it surprisingly failed and it was clear that my simple change couldn't be the cause. I assumed it was due to recent changes in software packages installed by the cookbook. So I started adapting our old code to these changes, and one change led to another, and another, until I finally realised that over the course of multiple hours, my one-line bugfix had turned into a full code overhaul. How did I get here?

I decided to start from scratch. `git reset --hard HEAD`. I quickly fixed my config file and ran the tests. When they failed again, closer inspection revealed that it was actually just the same test failing twice. The cause was a bug that had found its way into the main branch without breaking the CI pipeline (finding the reason for this will be *interesting*). It had been there all along! After another one-line fix, my change was ready for review. Round 2 had taken me less than half an hour. 

The lesson: Don't rush into battle blindly. Look closely. Identify assumptions and verify them. It might just be windmills!
