---
layout: post
title: "This is the archive of my old blog"
microblog: false
guid: http://geewiz.micro.blog/2007/09/08/this-is-the.html
date: 2007-09-08T19:58:00+0200
type: post
url: /2007/09/08/this-is-the.html
---

<p>If you didn't notice already, I moved my blog to <a href="http://www.jochen-lillich.de.">my homepage</a> The RSS feed should have switched transparently.</p>
<p>This website will be preserved for archival purposes.</p>
