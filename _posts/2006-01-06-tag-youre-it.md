---
layout: post
title: "Tag, you're it"
microblog: false
guid: http://geewiz.micro.blog/2006/01/06/tag-youre-it.html
date: 2006-01-06T23:25:04+0200
type: post
url: /2006/01/06/tag-youre-it.html
---

<p>Nachdem mir beim <a href="http://blog.jochen-lillich.de/index.php?/archives/218-Social-Bookmarking.html">Ausprobieren von del.icio.us</a> der Vorteil des Taggens klar wurde, n&auml;mlich eine flache Kategorisierung statt einer komplex hierarchischen, will ich es nun auch auf mein Weblog anwenden. Ab sofort werde ich also allen neuen Eintr&auml;gen mit Hilfe von Garvins FreeTag-Plugin m&ouml;glichst passende Stichworte zuordnen.</p>
