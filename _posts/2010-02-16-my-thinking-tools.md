---
layout: post
title: "My thinking tools"
microblog: false
guid: http://geewiz.micro.blog/2010/02/16/my-thinking-tools.html
date: 2010-02-16T21:46:54+0200
type: post
url: /2010/02/16/my-thinking-tools.html
---

<p>Just like "<a href="http://mark.pilgrim.usesthis.com/">good writing comes from writing and not from good writing tools</a>", good thinking doesn't come from good thinking tools. But tools can help you concentrate on the task at hand instead of the environment you're doing it in. </p>
<p>The creative congregation of the Church of Mac seems especially interested in which tools help in getting things done. Summing up many conversations I had on blogs, IM, Twitter and in real life (gasp!), I put together this mind map of the tools I use to collect ideas, thoughts as well as important documents I need to refer to.</p>
<p>In another article, I'll describe my work and data flow when I use those tools.</p>
<p>Take a look and please tell me in the comments what tools we have in common and what your recommendations are!</p>
