---
layout: post
title: "1&1 und MySQL mobilisieren gegen Softwarepatente"
microblog: false
guid: http://geewiz.micro.blog/2005/03/22/und-mysql-mobilisieren.html
date: 2005-03-22T09:38:34+0200
type: post
url: /2005/03/22/und-mysql-mobilisieren.html
---

<p>Ich bin zwar sicher, dass die &Uuml;berschrift bei <a href="http://www.pro-linux.de/news/2005/7945.html">Pro-Linux</a> auf ein Missverst&auml;ndnis zur&uuml;ckzuf&uuml;hren ist, und sowieso ungl&uuml;cklich &uuml;ber die unsensible Wortwahl. Dennoch freue ich mich &uuml;ber das Engagement von 1&amp;1 und MySQL im Kampf gegen Softwarepatente. </p>
<p>In einem offenen Brief wenden sich 1&amp;1-Vorstandssprecher Andreas Gauger und MySQL-CEO M&aring;rten Mickos klar gegen die Einf&uuml;hrung von Softwarepatenten und rufen die Branche auf, sich auch finanziell im Verfahren zur EU-Softwarepatent-Richtlinie zu engagieren. Schlie&szlig;lich stecke auch die Gegenseite erhebliche Mittel in ihre Lobbyarbeit.</p>
<p>Der offene Brief der Sprecher beider Firmen ist als <a href="http://www.nosoftwarepatents.com/docs/050321">PDF-Datei</a>(de)calltoaction.pdf auf <a href="http://www.nosoftwarepatents.com/phpBB2/viewtopic.php?t=473">NoSoftwarePatents.com</a> online.</p>
