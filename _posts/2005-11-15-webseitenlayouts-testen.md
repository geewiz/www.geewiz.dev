---
layout: post
title: "Webseiten-Layouts testen"
microblog: false
guid: http://geewiz.micro.blog/2005/11/15/webseitenlayouts-testen.html
date: 2005-11-15T19:29:29+0200
type: post
url: /2005/11/15/webseitenlayouts-testen.html
---

<p>Nachdem ich in den letzten Tagen wieder ein bisschen am CSS-Layout rumgespielt hatte, stellte ich fest, dass Firefox meine Homepage einwandfrei darstellt, der IE6 aber Teile nach rechts verschob. Jetzt wollte ich wissen, wie andere Browser auf das neue Layout reagieren.</p>
<p>Da ich auch mit VMware nicht alle m&ouml;glichen Varianten ausprobieren kann und will, hielt ich im Web nach entsprechenden Diensten Ausschau. Mit <a href="http://www.browsercam.com">Browsercam</a> fand ich ein Angebot mit professionellen Leistungen, aber leider auch ebensolchen Preisen. Kostenlos hingegen ist der Dienst <a href="http://www.browsershots.org.">Browsershots</a> Dessen Nachteil ist die lange Bearbeitungszeit -- auf die letzten Schnappsch&uuml;sse warte ich schon seit gestern.</p>
<p>Wenn sich mehrere Leute melden, lohnt sich eventuell auch ein gemeinsames Browsercam-Abo. Interesse?</p>
