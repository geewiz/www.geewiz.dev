---
layout: post
title: "Millennium Falcon Bed"
microblog: false
guid: http://geewiz.micro.blog/2009/12/19/millennium-falcon-bed.html
date: 2009-12-19T21:05:00+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm3staticflickrcom276241966109629d1b7569e2jpg_ddhfweawfwaotgg-scaled500.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpfarm3staticflickrcom276241966109629d1b7569e2jpg_ddhfweawfwaotgg-scaled500.jpg?w=300
url: /2009/12/19/millennium-falcon-bed.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm3staticflickrcom276241966109629d1b7569e2jpg_ddhfweawfwaotgg" height="334" src="/assets/wp-content/2011/05/media_httpfarm3staticflickrcom276241966109629d1b7569e2jpg_ddhfweawfwaotgg-scaled500.jpg?w=300" width="500" />
</div>
<div class="posterous_quote_citation">via <a href="http://starwarsblog.starwars.com/index.php/2009/12/18/inside-the-millennium-falcon-bed/">starwarsblog.starwars.com</a></div>
<p>Looks great, don't you think?</p>
</div>
