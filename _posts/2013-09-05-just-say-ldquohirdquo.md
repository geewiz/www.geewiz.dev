---
layout: post
title: "Just say \"Hi\""
microblog: false
guid: http://geewiz.micro.blog/2013/09/05/just-say-ldquohirdquo.html
date: 2013-09-05T15:43:00+0200
type: post
url: /2013/09/05/just-say-ldquohirdquo.html
---
For many introverts, it’s incredibly difficult to start a conversation with people they don’t already know. Paul Campbell recently wrote about this inner conflict in his blog post “<a href="http://blog.tito.io/2013/09/03/hi-im-obie.html">Hi, I’m Obie</a>”. I can relate very much because I feel the same when I’m at conferences, especially with people who I admire and would really love to get to know better. I remember so many occasions when I tried to join a group conversation, stood there for a while not knowing what to say and finally backed away, slightly embarrassed and disappointed about myself.

Paul then describes a watershed moment:

> He put out his hand to shake my hand, looked me in the eye, and said “Hi, I’m Obie”. “Hi, I’m Paul.” I had never felt so loved.

In this moment, Paul had an epiphany: You don’t need to do extraordinary stuff to get in touch with people. Just say “Hi”.

> Introducing yourself by name at a conference might not seem like a huge deal, but for me, it was just the recipe I needed to break the ice, to avoid the “what do I say now?” question.

I had the same insight start of last year, after reading a great ebook. It made a big change about which I wrote in “<a href="http://www.jochen-lillich.de/2012/02/how-to-survive-and-succeed-at-conferences-as-an-introvert/">How to survive and succeed at conferences as an introvert</a>”.

As IT guys, we’re familiar with the principle of breaking big tasks down into small parts that are easier to handle. The thing is, we can apply the “divide et impera” principle to the conversation problem, too: Instead of trying to tackle a whole group at once, just pick a person, stick out your hand and say “Hi, I’m $NAME.” I can assure you, noone will answer with “Yes? And?”

Except maybe if there’s such a thing as professional asshole conferences. But then again, you might not want to attend these anyway.
