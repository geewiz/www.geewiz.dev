---
layout: post
title: "Keyboard One"
microblog: false
guid: http://geewiz.micro.blog/2015/05/20/keyboard-one.html
date: 2015-05-20T13:24:12+0200
type: post
url: /2015/05/20/keyboard-one.html
---
<p><a href="http://www.marco.org/2015/05/19/mistake-one">Marco Arment</a>:</p>
<blockquote><p>&ldquo;What I hadn&rsquo;t considered was that even though I had common tasks that could fit within the MacBook&rsquo;s limited specs &mdash; email, writing, chat &mdash; all of them required a lot of typing. Oops.&rdquo;</p></blockquote>
<p>That&rsquo;s one of the reasons why I decided to go for a Macbook Pro 13" as replacement of my old MacBook Air 11", despite of the weight. All my work has to do with typing, be it in a browser, a terminal or even in Word.</p>
