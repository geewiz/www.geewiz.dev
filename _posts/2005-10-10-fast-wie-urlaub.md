---
layout: post
title: "Fast wie Urlaub"
microblog: false
guid: http://geewiz.micro.blog/2005/10/10/fast-wie-urlaub.html
date: 2005-10-10T20:22:08+0200
type: post
url: /2005/10/10/fast-wie-urlaub.html
---

<p>Vielen Dank an Armin f&uuml;r den <a href="http://blog.helaron.de/archives/225-Heute-war-Entspannung-angesagt...-Urlaub-Tag-1.html">Hinweis</a> auf das <a href="http://www.felsland-badeparadies.de">Felsland-Badeparadies</a>! Mit Kai habe ich mich gestern f&uuml;r einen Entspannungstag auf den Weg nach Dahn gemacht.Bei dem wundersch&ouml;nen Wetter war selbst die Fahrtzeit von einer knappen Stunde kein Problem. Und gelohnt hat sich die Fahrt voll und ganz: W&auml;hrend der Badeteil dem Standard gr&ouml;&szlig;erer B&auml;der mit mehreren Becken (davon eines mit Aussenteil), Rutschbahn und Whirlpool entspricht, hat die Saunalandschaft wirklich die Bezeichnung verdient.</p>
<p>Innen verf&uuml;gt das Badeparadies &uuml;ber mehrere Saunen und zwei Dampfb&auml;der. Mehrere Ruher&auml;ume und Duschen aller Art, von der Schwallbrause bis zum Eimer, fehlen nat&uuml;rlich auch nicht. Hervorheben m&ouml;chte ich hier die "Schneckendusche", in der man zwischen "Polareisregen" und "Warmer Urwaldregen" umschalten kann, und den Eisb&auml;renkopf, der Crusheis zum Abreiben spuckt. Ein ansprechendes und sauberes Ambiente setze ich eigentlich voraus, aber es sei erw&auml;hnt, dass alles sehr sch&ouml;n gestaltet und gepflegt ist.</p>
<p>Wirklich &uuml;berrascht hat uns aber die Aussenanlage. Wenn man dort aus einer der beiden finnischen Blockhaussaunen kommt, kann man nach dem Duschen einige Bahnen im Kaltwasserpool ziehen und dann auf der gro&szlig;en Liegewiese ausruhen. Das haben wir auch ausgiebig getan. In der goldenen Oktobersonne bin ich prompt eingeschlafen (bis sich meine Nachbarin bei ihrem Mann &uuml;ber irgendwelches Schnarchen beschwerte).</p>
<p>Es war ein wunderbar erholsamer Sonntag, dessen Kosten sich mit 11,50 f&uuml;r die Tageskarte sowie 6 Euro f&uuml;rs Mittagessen und 6,50 f&uuml;r den Pf&auml;lzer Neuen Wein, den wir auf dem R&uuml;ckweg noch eingesackt haben, angenehm im Rahmen hielten.</p>
