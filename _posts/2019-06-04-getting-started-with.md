---
layout: post
title: "Getting started with a big codebase"
microblog: false
guid: http://geewiz.micro.blog/2019/06/04/getting-started-with.html
date: 2019-06-04T02:00:00+0200
type: post
url: /2019/06/04/getting-started-with.html
---

In my recent [talk about monorepo](/talks/monorepo-dcscot19), I mentioned as a downside of the all-in-one approach that the sheer volume of code can be intimidating. This article has some tips for getting started: ["Don't let that huge codebase scare you! Tips and tools to make sense of other people's code"](https://dev.to/mjraadi/don-t-let-that-huge-codebase-scare-you-tips-and-tools-to-make-sense-of-other-people-s-code-3lgb)
