---
layout: post
title: "E-Mail, aber richtig"
microblog: false
guid: http://geewiz.micro.blog/2005/03/25/email-aber-richtig.html
date: 2005-03-25T02:14:39+0200
type: post
url: /2005/03/25/email-aber-richtig.html
---

<p>Der neueste Teil der <a href="http://www.spiegel.de/kultur/zwiebelfisch/0">Zwiebelfisch-Kolumne</a>,1518,347867,00.html bei SPIEGEL ONLINE geht auf das Thema "E-Mail" ein. Der Artikel fiel ungew&ouml;hnlich lang aus, denn er deckt eine Vielzahl von Einzelthemen ab:</p>
<ul>
<li>Betreffzeile</li>
<li>Anrede und Signatur</li>
<li>HTML-Mail</li>
<li>Abk&uuml;rzungen</li>
<li>Orthographie</li>
<li>Re: Aw: Re: Aw:</li>
<li>Vertraulich</li>
</ul>
<p>Weil E-Mail l&auml;ngst mein Hauptmedium zur Kommunikation neben dem pers&ouml;nlichen Gespr&auml;ch ist, kann ich den Link nur begeistert weiterempfehlen.</p>
