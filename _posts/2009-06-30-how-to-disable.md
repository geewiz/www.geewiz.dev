---
layout: post
title: "How to disable journaling on a HFS+ volume"
microblog: false
guid: http://geewiz.micro.blog/2009/06/30/how-to-disable.html
date: 2009-06-30T10:24:33+0200
type: post
url: /2009/06/30/how-to-disable.html
---

<div class="posterous_bookmarklet_entry">
<p>If you want to disable the journaling on a HFS+ volume, for example if you want to mount it under Linux, you can do so via a simple Terminal command:</p>
<blockquote class="posterous_long_quote"><p><tt>diskutil disableJournal /Volumes/TheVolumeName</tt></p></blockquote>
<div class="posterous_quote_citation">via <a href="http://julipedia.blogspot.com/2007/04/how-to-disable-journaling-on-hfs-volume.html">julipedia.blogspot.com</a></div></p>
</div>
