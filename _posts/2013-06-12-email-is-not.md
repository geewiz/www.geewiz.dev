---
layout: post
title: "Email is not dead"
microblog: false
guid: http://geewiz.micro.blog/2013/06/12/email-is-not.html
date: 2013-06-12T17:07:20+0200
type: post
url: /2013/06/12/email-is-not.html
---
<p>After trying collaboration tools like <a href="http://www.yammer.com">Yammer</a> for a while, email had a renaissance at <a href="http://www.freistil.it">freistil IT</a>. Our decision of giving the dreaded mailbox another chance was triggered by a post about how the team at Stripe practices <a href="https://stripe.com/blog/email-transparency">email transparency</a>. We've adopted their system and it works well; we still need to get more used to it, though.</p>
<p>This is how it works: Every team and project by default has three or four mailing lists (like Stripe, we're using Google Groups for Business):</p>
<ul>
<li>A conversation mailing list ("marketing") for the communication within the team. Everyone in the team (and maybe beyond) subscribes to this list.</li>
<li>A low-traffic announcement mailing list ("marketing-announce") that reaches many or even all coworkers.</li>
<li>A "bacn" mailing list ("marketing-bots") that receives automatically generated emails, for example from social networks and external services. Everyone that manages or uses such a service subscribes to this list.</li>
<li>An archive mailing list ("marketing-archive") that is mainly used to preserve emails that don't concern anyone at the moment. Very few people will subscribe to this list, but it makes it easy to share emails instead of hiding them in personal mailboxes.</li>
</ul>
<p>The effect of this approach is not only easy written communication but, just as important, transparency:</p>
<blockquote>
<p>"As we&rsquo;ve grown, the experiment has become about both efficiency and philosophy. We don&rsquo;t just want Stripe to be a successful product and company. We also want to try to optimize the experience of working here. As as we&rsquo;ve grown, we&rsquo;ve come to realize that open email can help."</p>
</blockquote>
<p>By sorting the daily email influx into many mailing lists (Stripe has more than 100) to which only these people subscribe that have the need, the amount of email anyone has to deal with stays on a managable level.</p>
<p>At freistil IT, we still need to get better at adhering to the addressing rules described in Greg's post. Too much email still only reaches personal mailboxes without being shared in a group visible to the team. I think I'll start by copying the rules into our Company Runbook and from there get them into people's heads (mine included).</p>
<p>But apart from that, it's an interesting experience. As I wrote in a <a href="http://www.jochen-lillich.de/2013/06/email-best-practices-for-teams/">previous post</a>, email can be very disruptive to my daily work. At the same time, being able to at any time tap into exactly these email streams that I'm interested in is engaging and efficient.</p>
