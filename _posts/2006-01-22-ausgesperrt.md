---
layout: post
title: "Ausgesperrt"
microblog: false
guid: http://geewiz.micro.blog/2006/01/22/ausgesperrt.html
date: 2006-01-22T09:12:01+0200
type: post
url: /2006/01/22/ausgesperrt.html
---

<p>Lieber Kollege Bleek, man muss sich gar nicht viel M&uuml;he geben, um im Firmen-VPN gesperrt zu werden. Da mir das vorhin genau nach der H&auml;lfte einer unaufschiebbaren Datenbank-Wartung passiert ist, sitze ich jetzt in Durlach. Und habe viel Zeit, um Blogeintr&auml;ge zu machen, J2EE zu &uuml;ben und "Firefly" anzuschauen...</p>
