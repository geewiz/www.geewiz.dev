---
layout: post
title: "Free - The past and future of a radical price"
microblog: false
guid: http://geewiz.micro.blog/2009/07/07/free-the-past.html
date: 2009-07-07T15:48:00+0200
type: post
images:
- /assets/wp-content/2011/05/anderson-chris_free-cover-scaled500.jpg?w=220
photos:
- /assets/wp-content/2011/05/anderson-chris_free-cover-scaled500.jpg?w=220
url: /2009/07/07/free-the-past.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="Anderson-chris_free-cover" height="425" src="/assets/wp-content/2011/05/anderson-chris_free-cover-scaled500.jpg?w=220" width="313" />
</div></p>
<p>I just downloaded the audiobook version of Chris Anderson's new book from <a href="http://www.wired.com/techbiz/it/magazine/17-07/mf_freer">Wired.com</a>.</p>
