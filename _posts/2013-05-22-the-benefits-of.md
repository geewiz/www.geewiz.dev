---
layout: post
title: "The benefits of daily meditation"
microblog: false
guid: http://geewiz.micro.blog/2013/05/22/the-benefits-of.html
date: 2013-05-22T17:50:00+0200
type: post
url: /2013/05/22/the-benefits-of.html
---
<p>Being a leader in a growing business with all the duties and responsibilities is a challenge that requires me to learn new skills all the time. For every task that I get done, two new ones seem to grow back. I actually enjoy that. But I also realise the hidden dangers of losing focus and going into burnout.</p>
<p>That's why I'm making it a habit to start my day with 20 minutes of mindfulness meditation. Concentrating on my breath alone and putting all the thoughts whizzing around in my head back to their waiting line (again and again and again...) helps me keep my peace of mind and trains my mental muscles.</p>
<p>Buffer CEO Joel Gascoigne lists "<a href="http://joel.is/post/41942221925/5-reasons-as-a-ceo-you-should-develop-a-habit-of-daily">5 reasons as a CEO you should develop a habit of daily meditation</a>":</p>
<ol>
<li>You will easily handle the inevitable ups and downs</li>
<li>It will save you time, by reducing procrastination</li>
<li>You will have bursts of creative genius</li>
<li>You will feel alive and healthy and have better sleep</li>
<li>It will make you happy and you&rsquo;ll find meaning</li>
</ol>
<p>And <a href="http://blogs.hbr.org/bregman/2012/10/if-youre-too-busy-to-meditate.html">If You're Too Busy to Meditate, Read This</a>.</p>
