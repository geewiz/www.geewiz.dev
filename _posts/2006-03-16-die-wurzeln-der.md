---
layout: post
title: "Die Wurzeln der amerikanischen Verfassung"
microblog: false
guid: http://geewiz.micro.blog/2006/03/16/die-wurzeln-der.html
date: 2006-03-16T05:38:46+0200
type: post
url: /2006/03/16/die-wurzeln-der.html
---

<p>Dilbert-Autor Scott Adams pflegt eine erfrischende Distanz zu seinem Heimatland. Der neueste <a href="http://dilbertblog.typepad.com/the_dilbert_blog/2006/03/constitutional_.html">Eintrag in seinem Blog</a> besch&auml;ftigt sich mit der Verfassung, bekannterma&szlig;en einem amerikanischen Heiligtum:</p>
<blockquote class="posterous_medium_quote">
<p>I keep hearing the argument that some things are constitutional while other things are not. The idea is that we should be in favor of all the things that were decided over 200 years ago by a bunch of slave-owning cross-dressers who pooped in holes.</p>
</blockquote>
<p>Gut erkannt, nett ausgedr&uuml;ckt. :-)</p>
