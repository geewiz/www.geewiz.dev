---
layout: post
title: "My space, not MySpace"
microblog: false
guid: http://geewiz.micro.blog/2007/05/18/my-space-not.html
date: 2007-05-18T22:26:30+0200
type: post
images:
- /uploads/workplace-home.jpg
photos:
- /uploads/workplace-home.jpg
url: /2007/05/18/my-space-not.html
---

<p>The folks over at <a href="http://webworkerdaily.com/2007/05/16/my-space-not-myspace/">WebWorker Daily</a> asked for a picture of my work space. No problem, here's where I work when I'm at home:</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/workplace-home.jpg" />
</div></p>
<p>I also put a larger, <a href="http://flickr.com/photos/geewiz/503195343/.">annotated picture on Flickr</a></p>
