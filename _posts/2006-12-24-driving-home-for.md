---
layout: post
title: "Driving home for christmas"
microblog: false
guid: http://geewiz.micro.blog/2006/12/24/driving-home-for.html
date: 2006-12-24T20:48:49+0200
type: post
images:
- /uploads/merry_christmas.jpg
photos:
- /uploads/merry_christmas.jpg
url: /2006/12/24/driving-home-for.html
---

<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/merry_christmas.jpg" />
</div>
<p>Although I claim to be living online, I won't be particularly connected to the net for the rest of the year. I've just arrived in Philippsburg to celebrate the holidays with my and Carolin's family and friends. Sadly, before she and I will be flying over to Ireland to spend New Year's Eve in Dublin, we'll have to attend the burial of her grandad who passed away last sunday.</p>
<p>Therefore, I, like many others, won't be as responsive as usual over the last days of 2006. But of course, emails will reach me: I'm typing this at my dad's computer, there are internet cafes, and finally, with my trusty E61, I can get an internet connection almost everywhere.</p>
<p>So, take care, folks, and thanks for all the great feedback over the past year. Have some blessed christmas days and a good start into a happy new year! "Peace" is one of the most important words in the story of Jesus' birth. Whatever that word means to you -- be it not having to work for some days, the company of people you love, or some quiet time without disturbances and sorrow -- may you enjoy it!</p>
<p>Merry christmas, see you in 2007!</p>
