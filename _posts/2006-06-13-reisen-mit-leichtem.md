---
layout: post
title: "Reisen mit leichtem Gep&auml;ck"
microblog: false
guid: http://geewiz.micro.blog/2006/06/13/reisen-mit-leichtem.html
date: 2006-06-13T03:58:43+0200
type: post
url: /2006/06/13/reisen-mit-leichtem.html
---
<br />
<blockquote class="posterous_short_quote">
<p>Phileas Fogg had Passepartout, Bertie Wooster had Jeeves and Sir Edmund Hillary had a retinue of stalwart Sherpas. The rest of us, alas, must schlep our own bags.</p>
</blockquote>
<p>(Ha, wieder ein deutsches Wort im englischen Sprachschatz!) Gep&auml;ck rumzuschleppen hat leider mehr mit Arbeit als mit Urlaub zu tun. F&uuml;r alle, die in der kommenden Reisesaison auf die Malediven fliegen (Neid!) oder sonstwo Urlaub machen, hat der San Francisco Chronicle deshalb <a href="http://sfgate.com/cgi-bin/article.cgi?f=/c/a/2006/06/11/TRGIKJA90J1.DTL.">gute Tipps, wie man zu viel Gep&auml;ck vermeidet</a></p>
<p>Besonders gut gefielen mir:</p>
<ul>
<li>Wieviel man glaubt, einpacken zu m&uuml;ssen, ist proportional zum Gep&auml;ckst&uuml;ck. Also einfach den eine Nummer kleineren Koffer nehmen!</li>
<li>Es gibt keinen Unterschied, ob man f&uuml;r eine Woche oder f&uuml;r einen Monat packt.</li>
<li>Zur&uuml;ck zuhause sollte man alles, was man auspackt, pr&uuml;fen, ob es gebraucht wurde. Wenn nicht, dann fliegt es von der Packliste f&uuml;rs n&auml;chste Mal!</li>
</ul>
<p>"Keep it simple" gilt also auch f&uuml;r die Urlaubsvorbereitungen!</p>
