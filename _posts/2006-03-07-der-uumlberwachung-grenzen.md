---
layout: post
title: "Der &Uuml;berwachung Grenzen setzen"
microblog: false
guid: http://geewiz.micro.blog/2006/03/07/der-uumlberwachung-grenzen.html
date: 2006-03-07T07:58:22+0200
type: post
url: /2006/03/07/der-uumlberwachung-grenzen.html
---

<p>Manchmal wird meine Sorge um den Datenschutz und die Privatsph&auml;re in diesem unserem Land dann doch wieder ein bisschen ged&auml;mpft. So hat das Verwaltungsgericht L&uuml;neburg die von der Polizei im Falle eines CASTOR-Gegners eigenm&auml;chtig durchgef&uuml;hrte <a href="http://www.heise.de/newsticker/meldung/70429.">Kopie von Daten als unverh&auml;ltnism&auml;&szlig;ig erkl&auml;rt</a> Zuvor hatte das BVG bereits die pr&auml;ventive Telefon&uuml;berwachung als verfassungswidrig erkl&auml;rt.</p>
<p>Bei der Gelegenheit f&auml;llt mir allerdings der Artikel in der neuesten c't ein, aus dem sich eine provokative Frage ergibt: warum sollten unsere Daten &uuml;berhaupt gesch&uuml;tzt werden, wo wir sie doch so bereitwillig &uuml;berall im Internet aus der Hand geben?</p>
