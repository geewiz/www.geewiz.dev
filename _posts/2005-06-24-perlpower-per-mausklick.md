---
layout: post
title: "Perl-Power per Mausklick"
microblog: false
guid: http://geewiz.micro.blog/2005/06/24/perlpower-per-mausklick.html
date: 2005-06-24T18:53:28+0200
type: post
url: /2005/06/24/perlpower-per-mausklick.html
---

<p><a href="http://sprog.sourceforge.net/">Sprog</a> ist eine grafische Anwendung, geschrieben in Perl-GTK, die das Konzept "Scripting f&uuml;r Mausschubser" realisiert. Nehmen wir ein Beispiel: man will Mitarbeiterdaten, die im Intranet ver&ouml;ffentlicht sind, via HTTP auslesen und in Form einer LDIF-Datei abspeichern. F&uuml;r Leute mit den h&ouml;heren Weihen der Perl-Programmierung kein gro&szlig;es Problem. F&uuml;r die Klickibunti-J&uuml;nger jedoch gibt es jetzt eine Alternative zum staunenden Zuschauen.Dem gewieften Perl-Hacker fallen sofort einige Module ein, die er nur in einem kurzen Script zusammenziehen muss, um die Aufgabe zu erledigen: Mittels LWP runterladen, durch HTML::TableContentParser jagen und mittels Template-Toolkit elegant rausschreiben. Ganz einfach.</p>
<p>Sprog jedoch macht das "Practical Extracting and Reporting" auch f&uuml;r Anwender ohne Kenntnisse der "Language" m&ouml;glich. In einer grafischen Oberfl&auml;che werden die einzelnen Module f&uuml;r das Einlesen, Parsen, Formatieren und Schreiben einfach wie Legosteine zusammengesetzt und mit geeigneten Parametern versehen. Am Ende wird der Ergebnisprozess per Mausklick gestartet -- fertig.</p>
<p>Wie das genau aussieht, zeigt der <a href="http://www.perl.com/pub/a/2005/06/23/sprog.html">Artikel</a> auf perl.com.</p>
