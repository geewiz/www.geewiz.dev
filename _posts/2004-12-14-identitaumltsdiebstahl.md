---
layout: post
title: "Identit&auml;tsdiebstahl"
microblog: false
guid: http://geewiz.micro.blog/2004/12/14/identitaumltsdiebstahl.html
date: 2004-12-14T10:34:00+0200
type: post
url: /2004/12/14/identitaumltsdiebstahl.html
---

<p>Bei eBay ein Benutzerkonto mit fremden Adressdaten einzurichten scheint wirklich kein Problem zu sein. Ich hab dar&uuml;ber schon einiges in der c't und im Web gelesen, und seit heute bin ich auch direkt davon betroffen.</p>
<p>Denn heute erhielt ich ein Schreiben einer Hamburger Anwaltskanzlei, die f&uuml;r die eBay Schweiz AG Auktionsgeb&uuml;hren bei mir eintreiben will -- nat&uuml;rlich f&uuml;r Auktionen eines Pseudonyms, das ich noch nie in meinem Leben gesehen habe.</p>
<p>Das Schreiben an meinen Anwalt ist bereits raus, mal sehen, was jetzt kommt. Wenn ich mir <a href="http://www.kefk.net/Shopping/Auktionen/Ebay/index.asp">einschl&auml;gige Webseiten</a> anschaue, kann das ja richtig lustig werden...</p>
