---
layout: post
title: "How MMO rules would help evolution"
microblog: false
guid: http://geewiz.micro.blog/2007/04/23/how-mmo-rules.html
date: 2007-04-23T17:50:00+0200
type: post
url: /2007/04/23/how-mmo-rules.html
---

<p>Sometimes, you wish a Massive Multiplayer Online Game was more like the real life.</p>
<p>And sometimes, you wish it was <a href="http://cad-comic.com/comic.php?d=20070205.">the other way around</a></p>
