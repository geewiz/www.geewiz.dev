---
layout: post
title: "Sleep in, be more productive"
microblog: false
guid: http://geewiz.micro.blog/2009/05/03/sleep-in-be.html
date: 2009-05-03T03:05:56+0200
type: post
url: /2009/05/03/sleep-in-be.html
---

<p>Finally, there is <a href="http://www.theglobeandmail.com/servlet/story/RTGAM.20090423.wsleep0423/BNStory/Science/">proof</a> that it&#8217;s better not to wake me too early. A scientific study shows that people who get up later in the day are able to stay productive longer:&nbsp;</p>
<blockquote style="border:none;margin:0 0 0 40px;padding:0;"><p>If you allow them to live on their preferred schedule, then they can outperform the morning types.</p></blockquote>
<p>Please don&#8217;t call before 1pm tomorrow. :-)</p>
