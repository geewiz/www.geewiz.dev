---
layout: post
title: "Advanced Databases for Beginners"
microblog: false
guid: http://geewiz.micro.blog/2006/09/03/advanced-databases-for.html
date: 2006-09-03T05:55:04+0200
type: post
url: /2006/09/03/advanced-databases-for.html
---

<p>On <a href="http://www.yapceurope.org/">YAPC::Europe</a> (taking place this weekend in Birmingham), Dave Cross held an interesting talk about <a href="http://mag-sol.com/talks/yapc/2006/advdb/">Advanced Databases for Beginners</a>, presenting best practices in database design. Every developer should know them before starting with laying out his first database.</p>
<p>(via <a href="http://marcusramberg.livejournal.com/33155.html">Marcus Ramberg</a>)</p>
