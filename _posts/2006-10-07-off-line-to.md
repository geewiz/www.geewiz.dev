---
layout: post
title: "Off (line) to Freiburg"
microblog: false
guid: http://geewiz.micro.blog/2006/10/07/off-line-to.html
date: 2006-10-07T17:33:47+0200
type: post
url: /2006/10/07/off-line-to.html
---

<p>Okay! Now I've only got to disassemble the bed and put the computer into a packing case and I'm ready to go.</p>
<p>Obviously, there'll be no broadband connection waiting for me in Freiburg, so it may get a bit silent around here over the next days. But I promise to give you a life sign as soon as I get my internet connection back.</p>
<p>See you folks soon!</p>
<p>PS: From what film (that just comes to my mind) does that line come from:</p>
<blockquote class="posterous_short_quote">
<p>I gave him the wrong finger! I gave him the wrong finger!</p>
</blockquote>
