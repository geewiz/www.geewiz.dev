---
layout: post
title: "Angerwhale: blog software with digital signatures"
microblog: false
guid: http://geewiz.micro.blog/2007/02/13/angerwhale-blog-software.html
date: 2007-02-13T02:00:15+0200
type: post
url: /2007/02/13/angerwhale-blog-software.html
---

<p><a href="http://blog.jrock.us/articles/Angerwhale">Angerwhale" is the name of a new blog software written in Perl, and its first official version "Angerwhale 0.02 was released</a>%20Released.pod to <a href="http://search.cpan.org/">CPAN</a>~jrockway/Angerwhale-0.02/lib/Angerwhale.pm today.</p>
<blockquote class="posterous_medium_quote">
<p>Features include guaranteed valid XHTML 1.1 output, social tagging, categories, syntax highlighting, RSS and YAML feeds for every article, comment, tag, and category, nested comments, intelligent caching of everything, space-conserving mini-posts, search-engine (and human!) friendly archiving, a flashy default theme, and lots of other cool stuff.</p>
</blockquote>
<p>The software is based on the <a href="http://www.catalystframework.org">Catalyst Web Framework</a> and uses the file system as storage backend instead of a database like MySQL. Another interesting feature is that Angerwhale uses cryptography to map articles and comments to users:</p>
<blockquote class="posterous_medium_quote">
<p>[Angerwhale] determines authorship based on the post's PGP digital signature. These posts can be in a variety of formats (text, wiki, HTML, POD), and new formats can be added dynamically at runtime. Posting comments is also supported, and again, authorship is determined by checking the digital signature.</p>
</blockquote>
<p>Finally, there's a contender for all the (great nonetheless) blog software written in PHP!</p>
