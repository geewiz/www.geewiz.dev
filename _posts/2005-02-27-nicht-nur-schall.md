---
layout: post
title: "Nicht nur Schall und Rauch"
microblog: false
guid: http://geewiz.micro.blog/2005/02/27/nicht-nur-schall.html
date: 2005-02-27T09:44:00+0200
type: post
url: /2005/02/27/nicht-nur-schall.html
---

<p>Bei der Systemadministration ist Fantasie gefragt. Und das nicht nur, wenn es Fehler zu finden oder Probleme zu beheben gilt, sondern schon bei so simplen Aufgaben wie der Vergabe von Hostnamen.</p>
<p>In der Firma ist das bei etwa 1200 Servern im RZ auch wirklich nicht mehr einfach. Nat&uuml;rlich stehen auch bei uns die &uuml;blichen Quellen Pate, zum Beispiel Computerspiele ("quake", "frogger"), ber&uuml;hmte Hacker ("hagbard") oder "Der Herr der Ringe" ("gollum"). F&uuml;r den richtig fetten Fileserver (einige TB), den ich letzte Woche online genommen habe, kam nur ein Name in Frage: "cartman". :) Was mich &uuml;brigens wundert, ist, dass ich bei uns noch keine Namen aus dem Star Trek Universum gesehen habe!</p>
<p>Inspiriert mich mal: wie heissen eure Favoriten f&uuml;r Servernamen?</p>
