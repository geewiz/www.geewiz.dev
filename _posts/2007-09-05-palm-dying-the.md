---
layout: post
title: "Palm dying the slow, painful death"
microblog: false
guid: http://geewiz.micro.blog/2007/09/05/palm-dying-the.html
date: 2007-09-05T18:48:17+0200
type: post
url: /2007/09/05/palm-dying-the.html
---

<p>Once, I was proud to belong to the first Palm PDA users -- back in the time when they were still were called "Palm Pilots". I had a Palm 1000, a Palm III and a Palm Vx. I learned digital organization and started to take my data with me whereever I went.</p>
<p>Then came the Zaurus that enabled me to have Linux in my pocket, and I abandoned Palm. Unfortunately, the Zaurus never gained enough market share and I missed the software I was used to on the Palm PDAs every day.</p>
<p>Today, I'm using a Nokia E61, because it's not as bulky as a Treo and, more importantly, it has WiFi capabilities.</p>
<p>Over all the years, Palm got more and more behind the market demand. With their new Foleo product, they seemed to make the steps necessary to gain relevancy again, but the price tag of almost $600 was way too close to a full-fledged laptop. And now, Palm kills their latest child before it even saw the light of day. In a <a href="http://blog.palm.com/palm/2007/09/a-message-to-pa.html">A Message to Palm Customers, Partners and Developers</a>, Palm CEO Ed Colligan announces that the Foleo will not be brought to market.</p>
<blockquote class="posterous_short_quote">
<p>We're not going to speculate now on timing for a next Foleo, we just know we need to get our core platform and smartphones done first.</p>
</blockquote>
<p>I'm sorry, but the iPhone is already in stores, you know?</p>
