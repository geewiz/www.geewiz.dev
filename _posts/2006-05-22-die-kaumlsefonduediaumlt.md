---
layout: post
title: "Die K&auml;sefondue-Di&auml;t"
microblog: false
guid: http://geewiz.micro.blog/2006/05/22/die-kaumlsefonduediaumlt.html
date: 2006-05-23T00:42:01+0200
type: post
url: /2006/05/22/die-kaumlsefonduediaumlt.html
---

<p>Seit 2 Wochen &auml;rgere ich mich, dass ich trotz mehrfachen Joggings und Karate-Trainings pro Woche so gut wie kein Gewicht verliere. So viele Muskeln baue ich wohl doch nicht auf, oder? Umso &uuml;berraschter war ich, als ich heute morgen auf der Waage stand.</p>
<p>Dazu gibt es eine Vorgeschichte: Am Wochenende war ich mit Bernhard und Bettina (und Klein-b) in <a href="http://www.annecy.fr.">Annecy</a> Der Samstag war leider komplett verregnet, daf&uuml;r war das Abendessen aber ein guter Ausgleich: ein leckeres K&auml;sefondue, abgeschlossen mit einem Genipi (Kr&auml;uterlik&ouml;r). Die Schmidts hatten im Gegensatz zu mir sogar noch Platz f&uuml;r eine Creme Brulee. Sonntags nach dem Fr&uuml;hst&uuml;ck gingen wir auf den gro&szlig;en Markt zum Einkaufen. Das zum Mittagessen geplante Raclette bliesen wir ab, weil wir immer noch recht satt waren. Selbstverst&auml;ndlich fuhren wir schlie&szlig;lich bei blendendem Wetter nach Hause. Insgesamt war es ein entspanntes und spa&szlig;iges Wochenende.</p>
<p>Als ich mich heute morgen nach dem Joggen mutig auf die Waage stellte, war ich auf Schlimmes gefasst. Aber siehe da: sie zeigte 2 Kilo weniger an als am Donnerstag! Ich hab eine neue Di&auml;t gefunden! :-)</p>
