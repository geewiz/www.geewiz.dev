---
layout: post
title: "Don't meet, work!"
microblog: false
guid: http://geewiz.micro.blog/2009/07/17/dont-meet-work.html
date: 2009-07-17T17:08:17+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwjochenli_wcfbt-scaled500.png?w=150
photos:
url: /2009/07/17/dont-meet-work.html
---

<p>On his blog Remarkable Leadership, Kevin Eikenberry cited an interesting study result in his article "<a href="http://blog.kevineikenberry.com/blog/kevin-eikenberry-group/0/0/leadership-and-meetings">Leadership and Meetings</a>": Almost no manager expects productivity to drop if meetings got banned for one day a week. About half of them even think productivity would increase!</p>
<p>In the <a href="http://officeteam.com/">OfficeTeam</a> survey, 150 executives were asked "How would employee productivity be affected if your company banned meetings one day a week?" The results:</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwjochenli_wcfbt" height="144" src="/assets/wp-content/2011/05/media_httpwwwjochenli_wcfbt-scaled500.png?w=150" width="150" />
</div></p>
<ul>
<li>No change: 46% (blue)</li>
<li>More productive: 45% (green)  </li>
<li>Less productive: 7% (yellow)</li>
<li>Don't know: 2% (red)</li>
</ul>
<p>My subsequent question would be: "So, why do you think those meetings don't add value, and what are you going to do about it?"</p>
<p>Meetings have the purpose of fostering efficient communication. But just coming together in a room to talk doesn't cut it. That's the time, money and drive sink we all dread. As always, you have to do things right to reap the benefits.</p>
<p>Brian lists the most important things you should take care of to stop the waste by ineffective meetings:</p>
<ul>
<li>Have clear desired outcomes for every meeting that are communicated before hand.</li>
<li>Use, and follow an agenda (that is focused on those desired outcomes).</li>
<li>Hold people accountable for the action items.</li>
</ul>
<p>So, there are two documents that are crucial for effective meetings: an agenda, sent to everyone in advance, and the meeting minutes (complete with action items and deadlines), sent to everyone after the meeting.</p>
<p>And hey, if you make your meetings really effective, you can have that no-meeting day anyway!</p>
