---
layout: post
title: "Mobile communication hub: Fring"
microblog: false
guid: http://geewiz.micro.blog/2007/05/21/mobile-communication-hub.html
date: 2007-05-21T16:44:00+0200
type: post
url: /2007/05/21/mobile-communication-hub.html
---

<p>If you've been following my blog for a while then you know that I'm very interested in using <a href="http://www.skype.com">Skype</a> on my mobile phone. But so far, they broke all their promises to release a Symbian S60 version of the Skype client.</p>
<p>It seems it needed a third party vendor to step in and deliver: <a href="http://www.fring.com/.">Fring</a> Fring is a free communication software for S60 that you install on your phone. It then connects to several services if you enter your account credentials. Apart from Skype, there are also Google Talk, SIP and as the most recent addition, <a href="http://www.fring.com/blog/?p=38.">Twitter</a> For Skype, Google Talk and Twitter, Fring also offers a chat function to exchange text messages.</p>
<p>The Fring client connects to their servers which in turn make a connection to Skype, Google, Twitter or the SIP contact you want to talk to.</p>
<p>So far, Fring works quite nicely on my Nokia E61. The voice quality over Skype is acceptable and by enabling me to also send instant messages from the same application makes Fring my single mobile communication hub. The only thing I'm afraid of is how running Fring will drain the phone's battery. But I'll see about that.</p>
