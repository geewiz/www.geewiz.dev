---
layout: post
title: "Sch&ouml;ne Aussicht"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/schoumlne-aussicht.html
date: 2006-06-18T22:15:52+0200
type: post
url: /2006/06/18/schoumlne-aussicht.html
---

<p>"Sch&ouml;ne Aussicht, Block 16" heisst die Kurzgeschichte von Malte Landsberger in c't Ausgabe 13/2006. Darin geben sich zwei Herren -- anscheinend in staatlichem Auftrag -- viel M&uuml;he, in eine Wohnung einzudringen und darin Wanzen anzubringen. Alles l&auml;uft gut -- bis sich herausstellt, dass sie die falsche Wohnung erwischt haben!</p>
<p>Zun&auml;chst denken sie daran, die Abh&ouml;rger&auml;te als Verlust zu deklarieren und sich die Kosten zu teilen. Aber als ihnen klar wird, dass das empfindlich teuer werden w&uuml;rde, kommt man auf eine andere Idee: der gute Wohnungsinhaber und Familienvater hatte doch Pornos im Schrank und einen neuen DVD-Brenner. Warum also nicht auf Verdacht gegen ihn wegen sexuellen Missbrauchs ermitteln lassen -- irgendwas findet sich dabei doch immer, und seien es nur Video-Raubkopien. So w&uuml;rde aus dem d&auml;mlichen Irrtum ein "Zufallserfolg einer &Uuml;berwachung im Rahmen der Terrorbek&auml;mpfung".</p>
<p>Und hey, "wenn er wirklich nichts zu verbergen hat, kann ihm doch gar nichts passieren, oder?"</p>
