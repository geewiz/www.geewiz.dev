---
layout: post
title: "KatholITen"
microblog: false
guid: http://geewiz.micro.blog/2005/03/19/katholiten.html
date: 2005-03-19T05:08:44+0200
type: post
url: /2005/03/19/katholiten.html
---

<p>Als ich k&uuml;rzlich auf die Homepage meines Teamleiter-Kollegen Felix stie&szlig;, war ich sehr erstaunt, dort eine Seite &uuml;ber <a href="http://felix.pfefferkorn.de/glaube/">Glaubensfragen</a> zu finden. Wie sich herausstellte, ist Felix nicht nur &uuml;berzeugter Katholik, sondern auch recht bewandert in theologischen Fragen. Und dar&uuml;ber hinaus gibt es sogar noch weitere Kollegen in der IT, mit denen ich die jahrzehntealte Frage <a href="http://blog.helaron.de/">Turnschuhe am Altar" diskutieren kann: auch "Armin</a> ('<a href="http://blog.koehntopp.de/archives/739-Gute-Besserung">Macht kaputt</a>,-Armin!.html, was euch kaputt macht') war Oberministrant. Sieh an, man lernt Leute immer wieder von neuen Seiten her kennen!</p>
