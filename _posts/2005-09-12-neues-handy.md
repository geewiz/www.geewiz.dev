---
layout: post
title: "Neues Handy"
microblog: false
guid: http://geewiz.micro.blog/2005/09/12/neues-handy.html
date: 2005-09-12T05:22:20+0200
type: post
url: /2005/09/12/neues-handy.html
---

<p>K&uuml;rzlich rutschte mir mein Handy aus der Tasche und fiel auf den Fliesenboden. Das n&auml;chste Telefonat gestaltete sich dann recht einseitig: wie sich herausstellte, hatte das Mikrofon den Sturz nicht &uuml;berlebt. Timing ist alles: mein O2-Vertrag war gerade verl&auml;ngerungsreif.Und weil ich mit O2 wirklich zufrieden bin, verl&auml;ngerte ich meinen Vertrag online und suchte mir ein neues Handy aus. Smartphones wie mein 6600 waren mir zu teuer, weil ich deren M&ouml;glichkeiten ohnehin nicht ausnutze. Letzten Endes fiel meine Wahl auf das Motorola RAZR V3, weil es alles n&ouml;tige zu haben schien und auch noch nett aussah.</p>
<p>Und ich muss sagen, f&uuml;r die 29 Euro inklusive BT-Headset hab ich wirklich brauchbare Hardware erworben. Das Handy ist gut zu bedienen (u.a. dank eigener Tasten f&uuml;r die Funktionen WAP und Messaging sowie einer erweiterten Texterkennung beim SiMSen), hat alle wichtigen Funktionen (z.B. das L&ouml;schen vieler System-Monitoring-SMS auf einen Schlag) und einen guten Hinkuckfaktor (zum Star Trek TOS Communicator fehlt nur noch das Zwitschern beim Aufklappen). Ausserdem wird eine praktische G&uuml;rteltasche gleich mitgeliefert.</p>
<p>Jetzt muss ich noch meine Rechnungen pr&uuml;fen, ob sich f&uuml;r mich die Homezone-Flatrate lohnt. So oder so jedoch macht das Telefonieren mit dem RAZR V3 Spa&szlig;.</p>
