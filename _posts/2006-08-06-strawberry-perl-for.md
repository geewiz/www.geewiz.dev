---
layout: post
title: "Strawberry Perl for Win32"
microblog: false
guid: http://geewiz.micro.blog/2006/08/06/strawberry-perl-for.html
date: 2006-08-07T01:54:07+0200
type: post
url: /2006/08/06/strawberry-perl-for.html
---

<p>David Golden released an early version of a binary distribution of Perl for Windows called <a href="http://search.cpan.org/dist/Perl-Dist-Strawberry/.">Strawberry Perl</a> The POD says:</p>
<blockquote class="posterous_medium_quote">
<p>The purpose of the Strawberry Perl series is to provide a practical Win32 Perl environment for experienced Perl developers to experiment with and test the installation of various CPAN modules under Win32 conditions, and to provide a useful platform for doing real work.</p>
</blockquote>
<p>A problem that arises when you install Perl modules from CPAN are XS modules that need to be compiled. Strawberry Perl solves that problem by including a compiler and a set of pre-installed modules.</p>
<p>Although Strawberry Perl is still in Alpha and not recommended for production usage, it is great to have a Perl distribution that aims at delivering the same power Perl has on Unix to our unfortunate colleagues using Windows.</p>
<p>For more information, also visit <a href="http://win32.perl.org/wiki/index.php?title=Strawberry_Perl.">win32.perl.org</a></p>
