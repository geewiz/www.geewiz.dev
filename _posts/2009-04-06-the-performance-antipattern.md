---
layout: post
title: "The Performance Antipattern"
microblog: false
guid: http://geewiz.micro.blog/2009/04/06/the-performance-antipattern.html
date: 2009-04-06T04:43:16+0200
type: post
url: /2009/04/06/the-performance-antipattern.html
---

<p>You're developing a new web application? You want it to run fast? </p>
<p>The Highscalability Blog tells you <a href="http://highscalability.com/performance-anti-pattern">what not to do</a>.</p>
