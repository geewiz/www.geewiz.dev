---
layout: post
title: "Jochen's photos 2.0"
microblog: false
guid: http://geewiz.micro.blog/2006/07/26/jochens-photos.html
date: 2006-07-26T06:01:43+0200
type: post
url: /2006/07/26/jochens-photos.html
---

<p>From now on, I'll publish my pictures on <a href="http://www.flickr.com/photos/geewiz/.">Flickr</a> You can't give <a href="http://blog.jochen-lillich.de/archives/440-A-short-intro-to-Web-2.0.html">talks about the advantages of the Web 2.0</a> and stay credible without using these services, can you? ;-)</p>
<p>At the moment, I'm uploading the pictures I took during the journey through Ireland that Carolin, Tom and I went on in 2002. To be exact, <a href="http://juploadr.sourceforge.net/">jUploadr</a> is doing the upload.</p>
<p>jUploadr is a great tool for annotating, tagging and uploading pictures to Flickr. Since it's written in Java (as you can guess from the initial "j"), it's platform-independent. So it works on Linux and it makes photo sharing on Flickr so much easier: just drag and drop your pictures onto the jUploadr window, type in some headlines, descriptions and tags, and click "Upload!". Easy-peasy.</p>
