---
layout: post
title: "Running Man"
microblog: false
guid: http://geewiz.micro.blog/2006/04/18/running-man.html
date: 2006-04-18T02:08:13+0200
type: post
url: /2006/04/18/running-man.html
---

<p>Ich bin offenbar nicht allein mit dem Ziel, die Kondition endlich mal auf Vordermann zu bringen. Mit Hirnarbeit allein gibt sich weder die Waage noch mein Kreislauf zufrieden. Seit 2 Wochen stehe ich deshalb eine Stunde fr&uuml;her auf, um etwas f&uuml;r meine Fitness zu tun.</p>
<p>Da passt es super, dass Cruiser den <a href="http://cruisersblog.de/archives/448-Dem-Schweinehund-den-Kampf-angesagt.html">Trainingsplan von MyJogging</a> entdeckt hat. Den werde ich jetzt auch anwenden und bin gespannt, wie sich meine Ausdauer entwickeln wird.</p>
