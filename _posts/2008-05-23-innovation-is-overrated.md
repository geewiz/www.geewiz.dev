---
layout: post
title: "Innovation is overrated"
microblog: false
guid: http://geewiz.micro.blog/2008/05/23/innovation-is-overrated.html
date: 2008-05-23T07:04:28+0200
type: post
url: /2008/05/23/innovation-is-overrated.html
---

<p>You'd think that an internet company and ISP has "innovation" written all over the business. But boy, do those two clips remind me of some meetings! :-)</p>
<p>[youtube=http://www.youtube.com/watch?v=OORnMYoWX9c&amp;hl=en]</p>
<p>[youtube=http://www.youtube.com/watch?v=ku4Ugw0lQ4Q&amp;hl=en]</p>
