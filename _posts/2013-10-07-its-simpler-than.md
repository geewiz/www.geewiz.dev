---
layout: post
title: "It's simpler than I thought."
microblog: false
guid: http://geewiz.micro.blog/2013/10/07/its-simpler-than.html
date: 2013-10-07T13:25:58+0200
type: post
url: /2013/10/07/its-simpler-than.html
---
<blockquote>One thing to help you be a great dad.<br />
<strong>Appreciate your child&rsquo;s love for you.</strong><br />
Do this every day, whenever you see your child, and even when they&rsquo;re not around.</p></blockquote>
<p>(via <a href="http://zenhabits.net/dad/">Zen Habits</a>)</p>
