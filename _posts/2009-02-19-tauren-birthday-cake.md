---
layout: post
title: "Tauren Birthday Cake"
microblog: false
guid: http://geewiz.micro.blog/2009/02/19/tauren-birthday-cake.html
date: 2009-02-19T17:29:03+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpfarm4static_btzjr-scaled500.jpg?w=225
photos:
- /assets/wp-content/2011/05/media_httpfarm4static_btzjr-scaled500.jpg?w=225
url: /2009/02/19/tauren-birthday-cake.html
---

<div>
<div class='p_embed p_image_embed'>
<img alt="Media_httpfarm4static_btzjr" height="500" src="/assets/wp-content/2011/05/media_httpfarm4static_btzjr-scaled500.jpg?w=225" width="375" />
</div>
</div>
<p>My birthday cake, made by Carolin with 39 candles and a hand-drawn Tauren.</p>
