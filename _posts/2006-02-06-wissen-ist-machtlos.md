---
layout: post
title: "Wissen ist machtlos"
microblog: false
guid: http://geewiz.micro.blog/2006/02/06/wissen-ist-machtlos.html
date: 2006-02-06T05:36:09+0200
type: post
url: /2006/02/06/wissen-ist-machtlos.html
---

<p>Dilbert-Autor Scott Adams hat einen Beziehungstipp, den ich weiterempfehlen m&ouml;chte: <a href="http://dilbertblog.typepad.com/the_dilbert_blog/2006/02/the_wcm_method_.html.">die WCM-Methode</a> "WCM" steht f&uuml;r "Who cares most" und bedeutet: in Diskussionen mit der besseren H&auml;lfte z&auml;hlt nicht die Sachkenntnis, mit der Argumente vorgetragen werden, sondern die Leidenschaft. Alles andere bringt nichts. Argumente wie "Schatz, ich habe meine Diplomarbeit zu dem Thema geschrieben, und es ist definitiv nicht, wie du sagst." kommen bei der anderen Seite ohnehin nur als "Ich bin ein zickiger Klugscheisser, der seine Nase nicht von einem Stopschild unterscheiden kann" an.</p>
<p>Bei der WCM-Methode liegen alle Entscheidungen zu einem Thema schlichtweg bei der Person, der es am wichtigsten ist. Das d&uuml;rfte zwar in 98% der F&auml;lle eine Frau sein, aber damit m&uuml;ssen wir wohl leben.</p>
