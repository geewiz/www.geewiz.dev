---
layout: post
title: "Weeknote #-7 (week 7, 2010)"
microblog: false
guid: http://geewiz.micro.blog/2010/02/22/weeknote-week.html
date: 2010-02-23T00:54:57+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2010/02/22/weeknote-week.html
---

<p>Regarding my new business, the last week was quite short because I went to visit my girlfriend and my daughter at their health resort at the Baltic Sea on Wednesday.</p>
<p>I've decided to change our web meeting service provider. Up until now, I'd used GoToMeeting to do webinars and online trainings. It worked okay; only an audio problem that made people sound like the Chipmunks when I used an USB headset on the Mac had been annoying me for some weeks. Not until <a href="http://www.acquia.com">Acquia</a> recently switched from GoToWebinar to WebEx, I realized that an important part of our customer base couldn't join our webinars in the first place: There's no Linux client for GoToMeeting. It didn't take me much more time to decide to switch to WebEx, too. </p>
<p>For starters, we won't incorporate. I discussed my plans with my tax consultant and she advised me not to incorporate early on but start lightweight as a sole proprietorship company.</p>
