---
layout: post
title: "Going to DevOps Days Berlin"
microblog: false
guid: http://geewiz.micro.blog/2013/05/21/going-to-devops.html
date: 2013-05-21T11:10:00+0200
type: post
url: /2013/05/21/going-to-devops.html
---
<p>Just a quick announcement that I'll be in Berlin Monday and Tuesday next week for <a href="http://devopsdays.org/events/2013-berlin/">DevOps Days</a>.</p>
<p>If you're going to be there too, feel free to leave me a comment or an <a href="mailto:jochen@lillich.info">email</a> and let's have a chat (and probably some drinks) there!</p>
