---
layout: post
title: "Gute Incident-Kommunikation lindert den Schmerz"
microblog: false
guid: http://geewiz.micro.blog/2010/04/06/gute-incidentkommunikation-lindert.html
date: 2010-04-06T20:42:00+0200
type: post
url: /2010/04/06/gute-incidentkommunikation-lindert.html
---

<p>Am 1. April war den Ops-Kollegen der Amazon Web Services&nbsp;wohl&nbsp;nicht nach Scherzen zumute. Jedenfalls nicht mehr nach 3 Stunden Teilausfall im Amazon-Rechenzentrum an der amerikanischen Ostk&uuml;ste. Ich bin durch einen <a href="http://searchcloudcomputing.techtarget.com/news/article/0,289142,sid201_gci1507837,00.html">Bericht auf SearchCloudComputing</a>&nbsp;auf diesen Fall aufmerksam geworden.</p>
<p>Bemerkenswert finde ich dabei zwei Dinge:</p>
<p>Erstens hatte Amazon die Krisenkommunikation, anders als bei vorhergehenden St&ouml;rungen, offensichtlich sehr gut im Griff. Auf Blog und Statusseite gab Amazon ausf&uuml;hrlich Einblick in den Ausfall und seine Hintergr&uuml;nde. Man gab dabei auch unumwunden zu, dass ein vorher nicht getesteter Rollout zu der St&ouml;rung f&uuml;hrte. Dem Artikel auf SearchCloudComputing ist zu entnehmen, dass diese Transparenz durchaus Lob auch bei den betroffenen Kunden fand.</p>
<p>Zweitens finde ich es interessant, wie sp&auml;t der Ausl&ouml;ser korrekt diagnostiziert wurde. Zun&auml;chst vermutete das Ops-Team von Amazon n&auml;mlich einen Kapazit&auml;tsengpass und versuchte, durch zus&auml;tzliche IT-Ressourcen Abhilfe zu schaffen. Erst als schlie&szlig;lich klar wurde, dass auf jeden Fall genug Leistung zur Verf&uuml;gung steht, verwarf man die Hypothese und suchte erneut nach der wahren Ursache. Diesen zeitraubenden Irrweg will Amazon durch genaue Analyse des Falls und eine geeignete Anpassung des Monitorings in Zukunft vermeiden.</p>
