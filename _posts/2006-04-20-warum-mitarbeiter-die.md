---
layout: post
title: "Warum Mitarbeiter die Motivation verlieren"
microblog: false
guid: http://geewiz.micro.blog/2006/04/20/warum-mitarbeiter-die.html
date: 2006-04-20T22:01:05+0200
type: post
url: /2006/04/20/warum-mitarbeiter-die.html
---

<p>Gerechtigkeit, Erfolg und Gemeinschaftsgeist -- diese drei &Uuml;berbegriffe stehen f&uuml;r das, was die Mehrheit der Angestellten von ihrem Job erwartet. Schon die Unzufriedenheit mit nur einem dieser Gebiete reicht aus, um den Enthusiasmus eines Mitarbeiters auf ein Drittel absacken zu lassen. Zu diesem Ergebnis kommt eine Studie mit 1,2 Millionen Angestellten.</p>
<p>Die Harvard Business School leitet daraus sieben Hinweise f&uuml;r Vorgesetzte ab, die helfen k&ouml;nnen, Mitarbeiter bei der Stange zu halten. &Uuml;ber diesen Hinweisen aus <a href="http://hbswk.hbs.edu/item.jhtml?id=5289">Why Your Employees Are Losing Motivation</a>&amp;t=organizations steht die Maxime:</p>
<blockquote class="posterous_short_quote">
<p>Die meisten Firmen packen es v&ouml;llig falsch an. Sie m&uuml;ssen ihre Angestellten gar nicht motivieren. Sie m&uuml;ssen aufh&ouml;ren, sie zu demotivieren.</p>
</blockquote>
