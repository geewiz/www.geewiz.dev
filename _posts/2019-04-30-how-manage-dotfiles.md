---
layout: post
title: "How I manage my dotfiles across hundreds of machines"
microblog: false
guid: http://geewiz.micro.blog/2019/04/30/how-i-manage.html
date: 2019-04-30T02:00:00+0200
type: post
categories:
- "DevOps"
tags: [devops]
url: /2019/04/30/how-i-manage.html
---

(Updated 2022-05-11)

Almost all of the command line tools I use on my Mac workstation, on my 
development server in the cloud, and on my company's Linux server infrastructure 
store their configuration in so-called "dotfiles" — files (or even whole 
directories) in my user home directory that are hidden from plain sight by 
prefixing their name with a period, for example `.vimrc`. (Like this example, 
actually quite a few of these files do end in `rc`. That's why they're sometimes 
also called "rc files".)

On dev.to, Jonathan Carter asked ["How do you manage and synchronize your 
dotfiles across multiple 
machines?"](https://dev.to/lostintangent/how-do-you-manage-and-synchronize-your-dotfiles-across-multiple-machines-2n24) 
Since "multiple" in my case means "more than 500 servers" (we operate a [managed 
high-performance hosting platform for Drupal and 
WordPress](https://www.freistilbox.com/)), I thought I'd answer his question in 
a short blog post.

### dotfile management

I maintain my dotfiles in a public [dotfiles 
repository](https://gitlab.com/geewiz/dotfiles) which I clone to every machine I 
want to customize. I then create a symlink for each file to where in the 
filesystem it needs to appear. This separation allows me to maintain my original 
dotfiles dot-free, i.e. as normal, visible files.

The key to keeping this symlinking process simple and error-free is the tool 
[rcm](https://github.com/thoughtbot/rcm). It can handle one or more dotfiles 
directories, each with its own precedence.

You can also provide it with tag names that switch between alternative versions 
of a file. For example, the BSD subsystem on the Mac sometimes behaves a bit 
differently from Linux. In this case, I maintain alternative versions stored in 
subfolders starting with `tag-`. My dotfile deployment then installs the correct 
version depending on the host OS (more on that below).

Using a `post-up` hook, you can add steps that need to happen after the dotfiles 
are in place, for example launching vim to update its plugins.

I install rcm right after setting up a new development machine (which usually 
only happens once or twice a year). On our servers, it gets installed 
automatically via our configuration management software Chef.

### dotfile deployment

Here's a short example script that clones and deploys a dotfiles repository. For 
my actual version, check the `setup` script in the repository.

```
#!/bin/bash

if ! which rcup >/dev/null; then
  echo "Fatal: rcm is not installed on this machine."
  exit 1
fi

os_type=$(uname -s)

if [[ -d ~/.dotfiles ]]; then
  cd ~/.dotfiles
  git pull
else
  git clone <repo URL> ~/.dotfiles
fi

rcup -f -t $os_type -d ~/.dotfiles
```

As you can see, it clones the dotfiles repository and calls `rcup` using an 
OS-specific tag ("linux" or "darwin").

As for the initial deployment on a new machine, I simply curlbash my `setup` 
script:

```
curl -L https://gitlab.com/geewiz/dotfiles/-/raw/main/setup | bash
```

`rcm` then takes care of the rest.

That's how I manage and deploy my dotfiles to have a consistent setup across all 
my work machines and our hosting infrastructure.

If you'd like to watch me put these nifty dotfiles to good use, join me on 
Twitch for my next [live coding session](https://www.twitch.tv/fullstacklive)!
