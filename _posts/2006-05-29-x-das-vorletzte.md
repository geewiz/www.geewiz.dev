---
layout: post
title: "X3: Das (vor-)letzte Gefecht"
microblog: false
guid: http://geewiz.micro.blog/2006/05/29/x-das-vorletzte.html
date: 2006-05-29T19:27:19+0200
type: post
url: /2006/05/29/x-das-vorletzte.html
---

<p>Nachdem ich letzte Woche schon haufenweise Folgen der Zeichentrickserie "X-Men Evolution" auf YouTube angeschaut hatte, war ich bestens auf den neuen Kinofilm "X3 -- The Last Stand" eingestimmt, den ich am Samstag mit Kai und Heike angeschaut habe.</p>
<p>Ich sag mal: gute Action-Unterhaltung. Die Handlung spielte ganz klar die Nebenrolle hinter den Spezialeffekten, die von einfachen Verwandlungen verschiedener Mutanten (z.B. Colossus) bis hin zur Versetzung der Golden Gate Bridge durch Magneto rangierten. Nicht immer erscheint die Handlung schl&uuml;ssig; vor allem, wenn man die Comic-Vorlage nicht kennt (Stichwort "Phoenix Force"). Wenn man sie jedoch kennt, wird man sich vielleicht wiederum an diversen Abweichungen des Films st&ouml;ren.</p>
<p>Schade fand ich, dass Alan Cummings die Maske als Nightcrawler zu l&auml;stig war und seine Rolle deshalb nicht mehr vorkam. Auf der anderen Seite lie&szlig; sich Halle Berry gl&uuml;cklicherweise &uuml;berzeugen, entgegen ihrer fr&uuml;heren Entscheidung doch wieder mitzuspielen. Ihre Rolle als Storm wurde daf&uuml;r auch deutlich aufgewertet.</p>
<p>Insgesamt f&uuml;hlte ich mich gut unterhalten. Und nachdem inzwischen die Entscheidung fiel, dass es einen vierten Teil geben wird (wie schon aus den Szenen vor und nach (!) dem Abspann zu erraten war), werde ich auch diesen wieder anschauen.</p>
