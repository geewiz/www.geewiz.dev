---
layout: post
title: "Travel tips for sysadmins"
microblog: false
guid: http://geewiz.micro.blog/2011/05/24/travel-tips-for.html
date: 2011-05-24T19:35:58+0200
type: post
url: /2011/05/24/travel-tips-for.html
---
<p>OpenDNS recently added a datacenter location in Frankfurt, Germany. On their <a href="http://blog.opendns.com/2011/04/27/opendns-in-frankfurt">blog</a>, George Patterson,&nbsp;Director of Operations for OpenDNS, not only posted some pictures of their server rack but also a bunch of tips for sysadmins that have to travel to a remote facility:</p>
<ul>
<li><strong>Have a solid deployment checklist of everything you want at the site.</strong> If you don't bring all necessary tools and equipment with you, getting them will cost you extra time.</li>
<li><strong>Set up all your power at the datacenter and make sure it's working before you leave.</strong> Don't waste time waiting for the datacenter staff to have your power supply connected. And have them install a remote manageable power distribution unit, so you don't have to pay remote-hands charges. </li>
<li><strong>If you can avoid it, don&rsquo;t book a flight until your gear has cleared customs.</strong> Depending on the country, customs handling can take from a few days to several weeks. Don't just hope that your gear will arrive earlier than you.</li>
<li><strong>Always plan for extra days.</strong> You shouldn't have to go into fast-forward mode because something took a bit longer than planned; that will only account for more problems. Plan for some extra days and if you'll finish early, there probably will be more to go and see than only a datacenter.</li>
<li><strong>Take photos along the way, and at the end.</strong> If your site documentation includes images, it's very easy to point a remote tech to the right place.</li>
</ul>
<p>Read George's whole blog post on the <a href="http://blog.opendns.com/2011/04/27/opendns-in-frankfurt">OpenDNS blog</a>!</p>
