---
layout: post
title: "Lotus Notes client for Linux"
microblog: false
guid: http://geewiz.micro.blog/2006/07/23/lotus-notes-client.html
date: 2006-07-23T06:15:50+0200
type: post
url: /2006/07/23/lotus-notes-client.html
---

<p>It's unbelievable: from Monday on, you will be able to use a real Lotus Notes client on Linux!</p>
<p>Think about it: Lotus Notes hasn't been available for more than a few years (Notes 3.0 was released 1993) and the Domino server has just been made available for Linux (in 2003) -- and already, there's a native client for Linux!</p>
<p>IBM certainly is an early adopter. :-)</p>
<p>PS: If you'd like to take a look at the history of a software that has been there forever, see <a href="http://www-128.ibm.com/developerworks/lotus/library/ls-NDHistory/.">The History of Notes and Domino</a></p>
