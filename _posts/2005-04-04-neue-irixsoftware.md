---
layout: post
title: "Neue IRIX-Software"
microblog: false
guid: http://geewiz.micro.blog/2005/04/04/neue-irixsoftware.html
date: 2005-04-04T21:27:39+0200
type: post
url: /2005/04/04/neue-irixsoftware.html
---

<p>Auf <a href="http://www.nekochan.net">nekochan.net</a> wurde der Download-Bereich umstrukturiert. F&uuml;r die Nekoware-Sammlung freier IRIX-Pakete existieren jetzt drei Unterverzeichnisse: In "beta" liegen die Pakete jetzt nur noch tempor&auml;r und wandern nach einer Testphase in "current". Pakete, die nicht mehr gebraucht werden, harren in "obsolete" ihrer Entsorgung.</p>
<p>In <a href="http://www.nekochan.net/downloads/index.php?sort=date">Nekoware/current</a>&amp;path=Nekoware/current/ finden sich also die bisher in "beta" gelagerten und zus&auml;tzlich folgende neue Pakete:</p>
<ul>
<li>neko_imagemagick-6.2.0-8.tardist</li>
<li>neko_libsigcxx-2.0.10.tardist</li>
<li>neko_php5_pspell.tardist</li>
<li>neko_openssl-0.9.7f.tardist</li>
<li>neko_crimson_fields-0.4.7.tardist</li>
<li>neko_openssh-4.0p1.tardist</li>
<li>neko_openssh-3.9p1-hpn.v2.tardist</li>
<li>neko_bind-9.3.1.tardist</li>
<li>neko_synergy-1.2.2.tardist</li>
<li>neko_lame-3.96.1.tardist</li>
<li>neko_gnupg-1.4.1.tardist</li>
<li>neko_gpa-0.7.0.tardist</li>
<li>neko_gpgme1-1.0.2.tardist</li>
<li>neko_proftpd-1.2.10.tardist</li>
</ul>
