---
layout: post
title: "Why You Need to Fail"
microblog: false
guid: http://geewiz.micro.blog/2009/07/18/why-you-need.html
date: 2009-07-18T22:59:48+0200
type: post
url: /2009/07/18/why-you-need.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_short_quote"><p>If you believe your talent grows with persistence and effort, then you seek failure as an opportunity. [...] If you have a growth mindset, then you use your failures to improve.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://blogs.harvardbusiness.org/bregman/2009/07/why-you-need-to-fail.html?cm_re=homepage-061609-_-secondary-2-_-headline">blogs.harvardbusiness.org</a></div></p>
</div>
