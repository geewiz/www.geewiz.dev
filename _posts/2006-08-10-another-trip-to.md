---
layout: post
title: "Another trip to Dublin"
microblog: false
guid: http://geewiz.micro.blog/2006/08/10/another-trip-to.html
date: 2006-08-10T20:26:10+0200
type: post
images:
- /uploads/weather.png
photos:
url: /2006/08/10/another-trip-to.html
---

<p>I'll be seeing Carolin from Friday to Tuesday next week. As I always say, "Timing is everything":</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/weather.png" />
</div></p>
<p>Of course, I'm also looking forward to visiting the Queen Of Tarts and to having some pints of freshly drawn Guinness!</p>
