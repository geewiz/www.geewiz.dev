---
layout: post
title: "Die Musikmafia setzt sich durch"
microblog: false
guid: http://geewiz.micro.blog/2006/03/24/die-musikmafia-setzt.html
date: 2006-03-24T23:41:51+0200
type: post
url: /2006/03/24/die-musikmafia-setzt.html
---

<p>F&uuml;r das private Kopieren von Musik drohen laut der am Mittwoch vom Regierungskabinett beschlossenen Urheberrechts-Novelle jetzt drei Jahre Haft. Bei mit Kopierschutz versehenen CDs wird lediglich noch die analoge Vervielf&auml;ltigung f&uuml;r private Zwecke geduldet, das Kopieren von CD auf CD ist jedoch verboten.</p>
<p>Nachdem mir das schon einige Kollegen nicht glauben wollten, verweise ich auf diesem Weg auf einen Spiegel-Artikel mit <a href="http://www.spiegel.de/netzwelt/politik/0">Details zum neuen Urheberrecht</a>,1518,407467,00.html</p>
<p>Ich hatte mir im Februar vorgenommen, meine Musiksammlung durch einen CD-Kauf pro Monat mal wieder zu aktualisieren. Diese Idee habe ich gestern in den Mentalm&uuml;lleimer geworfen.</p>
