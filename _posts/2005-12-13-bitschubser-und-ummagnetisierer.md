---
layout: post
title: "Bitschubser und Ummagnetisierer"
microblog: false
guid: http://geewiz.micro.blog/2005/12/13/bitschubser-und-ummagnetisierer.html
date: 2005-12-13T06:53:39+0200
type: post
url: /2005/12/13/bitschubser-und-ummagnetisierer.html
---

<p>Meine Kollegen <a href="http://cruisersblog.de/archives/356-Ummagnetisierer-oder-Bitkipper.html">Cruiser</a>, El*Loco und <a href="http://blog.helaron.de/archives/246-Ummagnetisierer.html">Helaron</a> hatten heute offensichtlich trotz aller Hardwarerundfahren und Softwareloopings noch genug Mu&szlig;e, um unser Fachgebiet zu reflektieren. Das Ergebnis: "W&auml;hrend andere Wolkenkratzer bauen oder die Polarkappen urbar machen, bringen wir mit unserer Arbeit nur ein paar Bits zum Kippen." Das kann ich so nicht stehen lassen!Liebe Mitstreiter vor dem Herrn, ihr vergesst eines: so, wie man ein Meisterst&uuml;ck Picassos letztlich auf "ein paar Pinselstriche" reduzieren kann, ohne ihm jedoch gerecht zu werden, so ist das auch mit den Bits und Bytes, aus denen die Ergebnisse unseres Tagewerks bestehen. Das Ganze ist, wie man so sch&ouml;n sagt, mehr als die Bin&auml;rdarstellung seiner Teile! Ein sauber verkabeltes Rack, eine wohlkonfigurierte Software, ein auf maximale Leistung getunter Rechner -- das ist Physik und Kunst, Mathematik und Poesie.</p>
<p>Eines Tages h&auml;nge ich mir bestimmt noch einen von Dominiks Einzeilern &uuml;ber den Kamin.</p>
<p>Und wenn, ja, Armin, dann will ich ein T-Shirt mit der Aufschrift "Diplom-Ummagnetisierer".</p>
