---
layout: post
title: Doing Karate Again
date: 2024-10-05 16:38 +0100
description:
image:
category:
tags:
published: true
sitemap: true
---

One of the best things that happened lately was that I found a Karate club nearby. I have no idea why I didn't earlier, because I wanted to get back into martial arts for years.

I did look for dojos in recent years, but for some reason, I didn't find any for Shotokan Karate, the style I trained in before we had kids. Turns that there are two Shotokan clubs in the next town alone!

I've been training again for four weeks now. Having spent more than a decade aging instead of practicing, I can tell that it'll take me a lot of hard work to get back into shape. But no pain can dampen my joy of wearing the dogi again.
