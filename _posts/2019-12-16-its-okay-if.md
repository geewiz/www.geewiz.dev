---
layout: post
title: "It's okay if you're not running Kubernetes"
microblog: false
guid: http://geewiz.micro.blog/2019/12/16/its-okay-if.html
date: 2019-12-16T15:36:15+0200
type: post
url: /2019/12/16/its-okay-if.html
---
Adopt technology because it's helpful to you, not just because it's hot. Or, as Mattias Geniar says, "get satisfaction in knowing that you’re making a difference for the business & its developers because your servers are running as best they can." [sysadvent.blogspot.com/2019/12/d...](https://sysadvent.blogspot.com/2019/12/day-10-it-ok-if-you-not-running.html)
