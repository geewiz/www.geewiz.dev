---
layout: post
title: "Wilder S&uuml;den"
microblog: false
guid: http://geewiz.micro.blog/2006/05/15/wilder-suumlden.html
date: 2006-05-15T07:03:21+0200
type: post
url: /2006/05/15/wilder-suumlden.html
---

<p>Dieses Jahr wollen Carolin und ich mal wieder auf ein Festival gehen, und nach dem Obstwiesenfest vom letzten Mal darfs dieses Mal ein richtig gro&szlig;es sein. Wir haben uns f&uuml;r das Southside entschieden und f&uuml;r die Tickets jeweils ca. 80 Euro gel&ouml;hnt. Dass es sich lohnt, fr&uuml;hzeitig Karten zu besorgen, habe ich heute festgestellt, als mich Caro auf die <a href="http://search.ebay.de/southside-tickets_W0QQfromZR40">aktuellen Ticketpreise</a> aufmerksam machte. Hm, vielleicht sollten wir unsere Karten wieder verh&ouml;kern und uns vom Gewinn ein Wochenende im Hotel leisten?</p>
