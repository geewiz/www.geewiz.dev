---
layout: post
title: "Klorollenhalter mit Dockingstation"
microblog: false
guid: http://geewiz.micro.blog/2006/01/17/klorollenhalter-mit-dockingstation.html
date: 2006-01-17T08:52:58+0200
type: post
url: /2006/01/17/klorollenhalter-mit-dockingstation.html
---

<p>Gute Nachrichten f&uuml;r die Analfixierten unter euch -- und f&uuml;r die, die einfach viel Zeit auf dem Klo verbringen (du weisst, wenn du gemeint bist): die Toilette mag noch Grund f&uuml;r schlechten Geruch sein, aber keinesfalls mehr f&uuml;r schlechte Musik. <a href="http://www.techeblog.com/index.php/tech-gadget/toilet-paper-dispenser-with-ipod-dock">iLounge</a> sei Dank.</p>
