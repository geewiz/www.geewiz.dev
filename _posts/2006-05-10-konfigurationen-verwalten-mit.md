---
layout: post
title: "Konfigurationen verwalten mit SCPM"
microblog: false
guid: http://geewiz.micro.blog/2006/05/10/konfigurationen-verwalten-mit.html
date: 2006-05-10T20:20:40+0200
type: post
url: /2006/05/10/konfigurationen-verwalten-mit.html
---

<p>Auf meiner alten Homepage hatte ich vor Jahren einen Artikel &uuml;ber die Verwaltung von Konfigurations-Profilen mit SCPM ver&ouml;ffentlicht. Beim CMS-Wechsel habe ich ihn nicht &uuml;bertragen -- aber das Web erinnert sich an ihn! Auf eine Anfrage hin habe ich <a href="http://www.jochen-lillich.de/article/konfigurationen-verwalten-mit-scpm"><em>Konfigurationen verwalten mit SCPM</em></a> heute wieder online gestellt.</p>
