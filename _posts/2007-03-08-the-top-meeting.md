---
layout: post
title: "The top 10 meeting rules"
microblog: false
guid: http://geewiz.micro.blog/2007/03/08/the-top-meeting.html
date: 2007-03-08T22:50:00+0200
type: post
url: /2007/03/08/the-top-meeting.html
---

<p>Those are my 10 most important rules for a successful meeting:</p>
<h1>For you to legitimately attend a meeting, you have to meet the following criteria:</h1>
<p><em>* You contribute to the meeting's success.<br />
<em></em> You profit from attending the meeting.<br />
<em></em> You know goal and issues of the meeting as well as the expectations hold about you.<br />
*</em> You come prepared.</p>
<h1>Give all participants the opportunity to come prepared to your meeting. Inform them of the meeting's goal and agenda and distribute the necessary material at least three days in advance.</h1>
<h1>It's essential that every participant has the opportunity to talk. You can facilitate that by delegating tasks whose results have to be presented in the meeting.</h1>
<h1>Reserve enough time to prepare well for a meeting. Develop a standpoint or, even better, a solution to every issue named on the agenda.</h1>
<h1>Start and end the meeting in time. Define a meeting schedule and a time limit for every topic. If there's not enough time, delegate the topic or schedule a followup.</h1>
<h1>Avoid distractions and interruptions. Ask the participants to switch off their mobile phones and Crackberries.</h1>
<h1>The issues should be ordered by priority and be dealt with in descending order of significance.</h1>
<h1>Avoid dicussions that sidetrack the topic and don't contribute to reaching the session goal.</h1>
<h1>Before finishing the meeting, repeat and summarize your decisions. Every participant has to know what he or she has to do afterwards.</h1>
<h1>A protocol is mandatory. The shorter and simpler, the better. Many times, a handwritten sheet with the most important decisions, responsibilities and dates is sufficient.</h1>
<p>Addendum: <a href="http://www.37signals.com/svn/archives2/meetings_considered_harmful.php.">Many Meetings are productivity killers</a> <em>If a meeting isn't urgent, it can be postponed. If it's not even important, cancel it.</em></p>
