---
layout: post
title: "Das Sch&ouml;nste, was F&uuml;&szlig;e tun k&ouml;nnen, ist..."
microblog: false
guid: http://geewiz.micro.blog/2006/05/13/das-schoumlnste-was.html
date: 2006-05-13T02:16:56+0200
type: post
url: /2006/05/13/das-schoumlnste-was.html
---

<p>Wer die Entwicklung popul&auml;rer T&auml;nze von den Sechzigern bis heute kompakt in wenigen Minuten pr&auml;sentiert bekommen und sich dabei vor Lachen wegschmei&szlig;en m&ouml;chte, ist mit der <a href="http://www.youtube.com/watch?v=dMH0bHeiRNg">Vorstellung von Juddson Laipply</a>&amp;eurl=http%3A%2F%2Fmachomedia%2Eblogspot%2Ecom%2F2006%2F05%2Fevolution%2Dof%2Ddance%2Ehtml bestens bedient!</p>
<p>(via <a href="http://blog.helaron.de/archives/295-Eine-echte-Zeitreise..html">Helaron</a>)</p>
