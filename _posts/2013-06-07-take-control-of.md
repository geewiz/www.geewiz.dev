---
layout: post
title: "Take control of your email"
microblog: false
guid: http://geewiz.micro.blog/2013/06/07/take-control-of.html
date: 2013-06-07T12:22:00+0200
type: post
url: /2013/06/07/take-control-of.html
---
<p>This week, I realized that I&rsquo;ve got a problem with email. I was wondering why I struggled so much with finishing my important tasks and found that I've been living in my inbox. I&rsquo;ve been constantly looking out for new messages from colleagues and customers. While this made for great response times, it prohibited me from concentrating on what I needed to work on. So many times, I have read the advice to not get addicted to my inbox and still, I did.</p>
<p>That&rsquo;s why I&rsquo;ve decided to limit my checking for new email to a few times per day. I&rsquo;ll still be notified of anything important or urgent by <a href="http://www.awayfind.com">AwayFind</a> and by the support request escalation of our Help Center.  I&rsquo;ve also reinstalled the <a href="http://www.getconcentrating.com">Concentrate app</a> on my computer to minimize distractions while I&rsquo;m working on a certain task &mdash; for example writing this blog post.</p>
<p>Since email is obviously a both useful and disruptive medium, I&rsquo;d like to point you to a great article by Kelly Forrister, a Senior Coach with the David Allen Company. In <a href="http://www.gtdtimes.com/2013/03/06/email-best-practices-for-teams/">Email best practices for your team</a>, Kelly gives the following tips:</p>
<ol>
<li>Match the message to the best medium.</li>
<li>Be discerning about your use of &ldquo;To:&rdquo; vs. &ldquo;Cc:&rdquo;.</li>
<li>Use subject lines that clearly describe the topic; add short codes for minimum reading effort.</li>
<li>Resist the urge to simply click &ldquo;reply to all&rdquo;.</li>
<li>Set a standard for response time and use the leeway it gives you.</li>
</ol>
<p>Since I seem to have blind spots regarding the influence of email on my productivity, I&rsquo;ll take a good look at which of Kelly&rsquo;s tips might further improve my work style. Read them on the <a href="http://www.gtdtimes.com/2013/03/06/email-best-practices-for-teams/">GTD blog</a> in full length!</p>
