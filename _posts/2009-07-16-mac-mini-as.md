---
layout: post
title: "Mac Mini as a baby monitor"
microblog: false
guid: http://geewiz.micro.blog/2009/07/16/mac-mini-as.html
date: 2009-07-16T09:50:00+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwblogcdncomwwwtuawcommedia200907henrycam3lthpng_juoczgelagjcinw-scaled500.png?w=246
photos:
url: /2009/07/16/mac-mini-as.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwblogcdncomwwwtuawcommedia200907henrycam3lthpng_juoczgelagjcinw" height="304" src="/assets/wp-content/2011/05/media_httpwwwblogcdncomwwwtuawcommedia200907henrycam3lthpng_juoczgelagjcinw-scaled500.png?w=246" width="250" />
</div>
<div class="posterous_quote_citation">via <a href="http://www.tuaw.com/2009/07/13/the-mighty-mini-take-two-diy-video-baby-monitor/">tuaw.com</a></div>
<p>Lauren Hirsch: What do you get when you combine a new parent on maternity leave with a love of gadgets and Apple products? Why, you get "baby monitor overkill!"</p>
<p>Right.</p>
</div>
