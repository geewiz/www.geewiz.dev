---
layout: post
title: "Steile Lernkurve"
microblog: false
guid: http://geewiz.micro.blog/2006/02/26/steile-lernkurve.html
date: 2006-02-26T22:01:30+0200
type: post
url: /2006/02/26/steile-lernkurve.html
---

<p>In meinem <a href="http://blog.jochen-lillich.de/archives/233-Mein-erstes-Java-Projekt.html">J2EE-Pilotprojekt</a> sind die ersten Planungen erledigt und es entsteht schon ein wenig Quellcode. Dabei merke ich aber immer wieder deutlich, dass ich noch viel &uuml;ber die eingesetzten Werkzeuge lernen muss. Zum Beispiel werde ich heute daran arbeiten m&uuml;ssen, Unterprojekte f&uuml;r <a href="http://blog.jochen-lillich.de/archives/236-Java-Projektwerkzeug-Maven.html">Maven</a> anzulegen.<br />
Mir war bisher nicht bewusst gewesen, dass ein Projekt nach Maven-Philosophie genau ein "Produkt" liefert und nicht mehr. Da COMATOSE aber aus zahlreichen Teilen, darunter Enterprise Java Beans, Hibernate-Datenobjekte und eine Weboberfl&auml;che, bestehen soll, muss ich diese Bausteine sinnvoll in Unterprojekte aufteilen. Erst dann kann Maven sinnvoll eingesetzt werden.</p>
<p>Erst dann kommt auch Mavens Multiproject-Plugin zu voller Geltung, das vom Hauptprojekt aus alle Unterprojekte verarbeiten und zu einem gro&szlig;en Ganzen zusammenf&uuml;gen kann.</p>
<p>Weil solche Erkenntnisse immer wieder zu einer weitreichenden Umgestaltung der Verzeichnisstruktur des Projekts f&uuml;hren, bin ich sehr froh, Subversion als Software-Repository einzusetzen. Anders als CVS verliert es n&auml;mlich nicht die Versionshistorie einer umbenannten oder verschobenen Datei.</p>
