---
layout: post
title: "Belkin Skype phone reviewed"
microblog: false
guid: http://geewiz.micro.blog/2006/08/22/belkin-skype-phone.html
date: 2006-08-22T18:50:02+0200
type: post
url: /2006/08/22/belkin-skype-phone.html
---

<p>Lately, I refrained from writing about newly announced Skype phones. Folks, it's about time to stop announcing and start delivering!</p>
<p>And yesterday, Gizmodo actually published a first <a href="http://gizmodo.com/gadgets/wireless/exclusive-belkin-wifi-skype-phone-review--first-anywhere-195468.php.">review of the Belkin Skype WiFi phone</a></p>
<p>I'm happy that, for a pioneer model of a new type of VOIP phone, the Belkin Skype phone seems to be pretty decent:</p>
<blockquote class="posterous_medium_quote">
<p>All in all, a pretty good Skype WiFi phone that actually looks nice enough to be carried around with you to work or to connect to WiFi hotspots around the city. Everything's good in this phone including call quality, button feel, and wireless reception.</p>
</blockquote>
<p>I think I know what the handset for our new flat will look like...</p>
<p><em>Update:</em> There is one caveat, though: The phone will not be able to authenticate against hotspots that require a web-based login. Unfortunately, that rules out a lot of hotspots in hotels, at train stations or airports.</p>
