---
layout: post
title: "Advantages of a distributed company"
microblog: false
guid: http://geewiz.micro.blog/2010/03/10/advantages-of-a.html
date: 2010-03-10T20:37:38+0200
type: post
url: /2010/03/10/advantages-of-a.html
---

<p>This morning, I worked on the business plan for my new business, doing a SWOT analysis. One aspect in assessing strengths and weaknesses of a new business is its location. But if you're offering your products and services over the Internet, location doesn't matter much to your clients. It may still matter to your employees in terms of moving house or commuting. That is, if you're not building the company in a distributed manner.</p>
<p>So just when I was pondering this aspect, I got a link via <a href="http://twitter.com/jkleske/status/10259557618">@jkleske</a> that fitted perfectly. In his blog, Toni Schneider from Automattic (of WordPress fame) lists "<a href="http://toni.org/2010/03/08/5-reasons-why-your-company-should-be-distributed/">5 reasons why your company should be distributed</a>":</p>
<ol>
<li>Your employees will love it</li>
<li>You can hire great people wherever you find them</li>
<li>You will use better communication tools</li>
<li>You can still be social</li>
<li>Your offices will be more fun</li>
</ol>
<p>Go read the article for his explanations, and read the comments as well.</p>
<p>I'm convinced that building myself a distributed company is the way to go. On the technical level, the Internet and especially Cloud Computing prove that virtual, distributed systems are superior to monolithic ones. It doesn't matter any more where a server is located if it's well connected, so you can choose simply by price and performance. </p>
<p>Since my company will take advantage of those principles, it's only consequent to apply them to the company itself, isn't it?</p>
