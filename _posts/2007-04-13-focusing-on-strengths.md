---
layout: post
title: "Focusing on strengths proves most effective"
microblog: false
guid: http://geewiz.micro.blog/2007/04/13/focusing-on-strengths.html
date: 2007-04-13T06:58:20+0200
type: post
url: /2007/04/13/focusing-on-strengths.html
---

<p>Via <a href="http://www.allthingsworkplace.com/2007/04/strengths_weakn.html">All Things Workplace</a>, I just discovered David Zingers blog about employee engagement, where in his latest entry, he points out the different results of <a href="http://davidzinger.wordpress.com/2007/04/09/an-employee-disengagement-quiz-monday-morning-percolator-8/">focusing on strengths or on weaknesses</a> of employees.</p>
<p>He explains that the Gallup Management Journal had found the following conclusions in their research of employee engagement:</p>
<h1>If your manager primarily ignores you your chances of being actively disengaged are 40%.</h1>
<h1>If your manager focuses on your weaknesses your chances of being actively disengaged are 22%.</h1>
<h1>If you manager focuses on your strengths your chances of being actively disengaged are only 1%.</h1>
<p>Obviously, pointing out weaknesses of employees and working together in resolving them isn't the best choice. For someone that always wants to help people develop, this felt a bit weird to myself first. Shouldn't I help my directs overcome their weak sides?</p>
<p>But after some thinking, it occured to me that focusing on their strengths instead is actually a very reasonable approach. Employees aren't clay sculptures who I'm to shape, rounding all edges to a perfect state. I'm a leader, not a sculptor. And I don't like to be looked at and treated with the perspective of a sculptor, either. (Hm, why am I thinking of my spouse at this point?)</p>
<p>People want to do what they can do best and to be recognized for that. Since it is one of my core beliefs that it's my foremost duty as a leader to make sure that my directs can work their magic as effective as possible, it's actually quite natural to focus on their strengths and to arrange their work in a way that they can employ them most effectively. A good thought to start the next week with.</p>
