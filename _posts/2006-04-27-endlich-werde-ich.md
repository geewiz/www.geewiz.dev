---
layout: post
title: "Endlich werde ich reich"
microblog: false
guid: http://geewiz.micro.blog/2006/04/27/endlich-werde-ich.html
date: 2006-04-28T00:37:08+0200
type: post
url: /2006/04/27/endlich-werde-ich.html
---

<p>Mit dem Weblog Geld verdienen -- das ist es doch, was wir wirklich wollen. Und nachdem ich jetzt bei FeedShow.com gelistet bin, wird das endlich wahr werden.</p>
<p>Es <em>w&uuml;rde</em> wahr werden, wenn der Satz "It is also the first aggregator that shares revenues with publishers." nicht einfach eine dreiste L&uuml;ge des Betreibers w&auml;re, der glaubt, im Internet einfach schmarotzen zu d&uuml;rfen und dessen "About"-Link lediglich ein "This page is coming soon" anzeigt.</p>
