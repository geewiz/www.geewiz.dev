---
layout: post
title: "Buch: Einf&uuml;hrung in Perl"
microblog: false
guid: http://geewiz.micro.blog/2004/11/13/buch-einfuumlhrung-in.html
date: 2004-11-13T06:54:00+0200
type: post
url: /2004/11/13/buch-einfuumlhrung-in.html
---

<p>"Einf&uuml;hrung in Perl" ist die deutsche &Uuml;bersetzung des Buchs "Learning Perl" von Randal L. Schwartz und Tom Christiansen. Was der deutsche Titel verspricht, h&auml;lt das Buch auch in vollem Umfang ein.Man kann schon am Inhaltsverzeichnis erkennen, dass "Einf&uuml;hrung in Perl" alle wichtigen Themen abdeckt, die angehende Perl-Entwickler interessieren:</p>
<h1>Einf&uuml;hrung</h1>
<h1>Skalare Daten</h1>
<h1>Listen und Arrays</h1>
<h1>Subroutinen</h1>
<h1>Hashes</h1>
<h1>IO-Grundlagen</h1>
<h1>Das Konzept der Regul&auml;ren Ausdr&uuml;cke</h1>
<h1>Mehr &uuml;ber Regul&auml;re Ausdr&uuml;cke</h1>
<h1>Regul&auml;re Ausdr&uuml;cke anwenden</h1>
<h1>Weitere Kontrollstrukturen</h1>
<h1>Dateihandles und Dateitests</h1>
<h1>Zugriff auf Verzeichnisse</h1>
<h1>Dateien und Verzeichnisse bearbeiten</h1>
<h1>Prozessverwaltung</h1>
<h1>Strings und Sortierfunktionen</h1>
<h1>Einfache Datenbanken</h1>
<h1>Fortgeschrittene Perl-Techniken</h1>
<p>Dem ganzen Buch merkt man an, dass es aus Kursmaterialien entstanden ist, die Schwartz in seinen Perl-Trainings verwendete. Er orientiert sich an der Programmier-Praxis und erkl&auml;rt die Sachverhalte anschaulich und verst&auml;ndlich. Durch die &Uuml;bungen am Ende jedes Kapitels und die oft witzigen Fu&szlig;noten macht es Spass, sich immer weiter in die Tiefen der Programmiersprache vorzuwagen. </p>
<p>Neben den grundlegenden Kontrollstrukturen und Datentypen kommen auch weiterf&uuml;hrende Themen wie Dateihandling, eval oder DBM-Datenbanken nicht zu kurz. Mit drei Kapiteln werden die Regul&auml;ren Ausdr&uuml;cke besonders gew&uuml;rdigt. Das ist einerseits ihrem Stellenwert f&uuml;r die Bedeutung von Perl, aber auch andererseits ihrer Komplexit&auml;t angemessen.</p>
<p>Fazit: Es ist kein Wunder, dass O'Reilly das Buch nun schon in der 3. Auflage herausgegeben hat. Zusammen mit Larry Walls Referenz "Programmieren in Perl" geh&ouml;rt "Einf&uuml;hrung in Perl" in das B&uuml;cherregal jeder Perl-Entwicklerin.</p>
