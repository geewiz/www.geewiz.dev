---
layout: post
title: "A series of tubes"
microblog: false
guid: http://geewiz.micro.blog/2007/03/20/a-series-of.html
date: 2007-03-20T05:37:18+0200
type: post
url: /2007/03/20/a-series-of.html
---

<p>It's time to be afraid when your minister for economy and technology utters the following, don't you think?</p>
<blockquote class="posterous_short_quote">
<p>Thank god I have people that operate the Internet for me.</p>
</blockquote>
<p>This indeed was said by Michael Glos, german minister for <em>economy and technology</em>, at a <a href="http://www.spiegel.de/videoplayer/0">visit to CeBit</a>,6298,16863,00.html, the world's biggest IT fair.</p>
<p>Well, there goes the neighborhood. I guess I'll be going soon, too.</p>
<p>(via <a href="http://blog.handelsblatt.de/indiskretion/eintrag.php?id=1167">Indiskretion Ehrensache</a>)</p>
