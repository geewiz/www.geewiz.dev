---
layout: post
title: "Google Mond, du k&auml;st so stille"
microblog: false
guid: http://geewiz.micro.blog/2005/07/21/google-mond-du.html
date: 2005-07-21T06:42:28+0200
type: post
url: /2005/07/21/google-mond-du.html
---

<p>In Gedenken an die erste bemannte Mondlandung hat Google nun auch den Erdtrabanten zur <a href="http://moon.google.com">Online-Ansicht</a> verf&uuml;gbar gemacht. Eine bislang von Experten weithin diffamierte Theorie erweist sich best&auml;tigt, wenn man den h&ouml;chsten Vergr&ouml;&szlig;erungsfaktor ausw&auml;hlt.</p>
<p>(F&uuml;r die &Uuml;berschrift bitte ich vor allem Karl Wilhelm Enslin vielmals um Entschuldigung.)</p>
