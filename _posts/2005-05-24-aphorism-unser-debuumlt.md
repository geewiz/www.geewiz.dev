---
layout: post
title: "Aphorism: Unser Deb&uuml;t"
microblog: false
guid: http://geewiz.micro.blog/2005/05/24/aphorism-unser-debuumlt.html
date: 2005-05-24T22:44:54+0200
type: post
url: /2005/05/24/aphorism-unser-debuumlt.html
---

<p>Am vergangenen Samstag spielten wir zum ersten Mal in der breiten &Ouml;ffentlichkeit. Das "Milj&ouml;&ouml;" in Bruchsal stellte uns im Rahmen von "Catch the Bus" seine kleine B&uuml;hne zur Verf&uuml;gung und von halb neun bis nach eins gaben wir uns alle M&uuml;he, W&auml;nde und Knie zum Wackeln zu bringen. Nach dem Applaus zu urteilen, waren wir stellenweise richtig gut, aber zwischendurch hing die Stimmung auch mal ein wenig durch. Insgesamt aber kein schlechtes Ergebnis f&uuml;r eine Premiere.</p>
<p>Ein paar Fotos gibts auf <a href="http://www.madchickenfactory.de/fotos/210505.">Mad Chicken Factory</a></p>
