---
layout: post
title: "T-Mobile throttles IPhone bandwidth for german power users"
microblog: false
guid: http://geewiz.micro.blog/2007/10/29/tmobile-throttles-iphone.html
date: 2007-10-29T22:57:52+0200
type: post
url: /2007/10/29/tmobile-throttles-iphone.html
---

<p>I just found the official rates T-Mobile will charge IPhone users in Germany:</p>
<ul>
<li>"Complete M": 100 minutes and 40 SMS for 49 &euro;</li>
<li>"Complete L": 200 minutes and 150 SMS for 69 &euro;</li>
<li>"Complete XL": 1000 minutes and 300 SMS for 89 &euro;</li>
</ul>
<p>As usual, unused talk minutes and SMS will expire at the end of each month.</p>
<p>But the main bummer is in the fine print: T-Mobile will limit the bandwidth to a maximum of 64 kbit/s (8KB/sec) downstream and 16 kbit/s (2KB/sec) upstream after 200MB for "Complete M", after 1GB for L, or after 5GB for XL.</p>
<p>Thanks, T-Mobile, for making not buying an IPhone this easy.</p>
<p>(via <a href="http://www.fscklog.com/2007/10/die-offiziellen.html">fscklog</a>)</p>
