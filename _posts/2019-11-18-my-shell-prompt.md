---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2019/11/18/my-shell-prompt.html
date: 2019-11-18T15:19:00+0200
type: post
categories:
- "Links"
url: /2019/11/18/my-shell-prompt.html
---
[My shell prompt](https://starship.rs/)  is already written in Rust. Looks like [my next shell](http://www.jonathanturner.org/2019/08/introducing-nushell.html) might be as well. 
