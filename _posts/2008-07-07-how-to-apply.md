---
layout: post
title: "How to apply for a job, the non-boring way"
microblog: false
guid: http://geewiz.micro.blog/2008/07/07/how-to-apply.html
date: 2008-07-07T19:13:49+0200
type: post
url: /2008/07/07/how-to-apply.html
---

<p>Good gracious, how I envy those design types. With the applications I get, I'm already happy when there are no spelling or grammar errors in them.</p>
<p>[youtube=http://www.youtube.com/watch?v=6KFCfZsu5Gc&amp;hl=en&amp;fs=1]</p>
<p>And can you imagine that there actually are people that would like to get a Linux system administration job without mentioning Linux in their application?</p>
<p>(via <a href="http://www.core77.com/blog/business/an_awesome_way_to_interview_for_a_design_job_10151.asp">Core77</a>)</p>
