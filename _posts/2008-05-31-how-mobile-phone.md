---
layout: post
title: "How mobile phone operators become banks in rural Africa"
microblog: false
guid: http://geewiz.micro.blog/2008/05/31/how-mobile-phone.html
date: 2008-05-31T15:03:53+0200
type: post
url: /2008/05/31/how-mobile-phone.html
---

<p>The abstract of Jan Chipchase's TED talk "<a href="http://www.ted.com/index.php/talks/view/id/190">Our cell phones, ourselves</a>" goes as follows:</p>
<blockquote class="posterous_medium_quote">
<p>Nokia researcher Jan Chipchase's investigation into the ways we interact with technology has led him from the villages of Uganda to the insides of our pockets. He's made some unexpected discoveries along the way.</p>
</blockquote>
<p>One of these interesting discoveries is how Africans use mobile phones to transfer money to rural areas where there's no bank let alone an ATM machine:</p>
<ul>
<li>Person A buys a pre-paid card in a bigger city</li>
<li>Person A calls phone kiosk owner B in a small village. B doesn't have to own more for his business than a simple mobile phone that he rents to other villagers.</li>
<li>Over the phone, A tells B the pre-paid card's code number</li>
<li>B collects the amount of pre-paid phone credit</li>
<li>B pays villager C the amount, keeping a discount of 10-20%</li>
</ul>
<p>Imagine: People that aren't creditworthy enough to get a bank account practically become human ATM terminals, using the mobile phone infrastructure as a medium of money transfer. I find it fascinating how those people use the same technology we use -- but for a totally different use case. Is that what we meant with "developing countries"?</p>
