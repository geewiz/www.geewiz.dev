---
layout: post
title: "Neue Nekoware f&uuml;r IRIX"
microblog: false
guid: http://geewiz.micro.blog/2005/04/14/neue-nekoware-fuumlr.html
date: 2005-04-14T08:07:53+0200
type: post
url: /2005/04/14/neue-nekoware-fuumlr.html
---

<p>Bei foetz und Co. gl&uuml;hen die Compiler, und das beschert uns neue Software-Versionen auf <a href="http://www.nekochan.net/downloads/index.php?path=Nekoware">Nekochan</a>%2Fcurrent/:<br />
* neko_tar-1.15.1.tardist<br />
* neko_zlib-1.2.2.tardist<br />
* neko_libpng-1.2.8.tardist<br />
* neko_scribus-1.2.tardist<br />
* neko_fireflies-2.06.tardist<br />
* neko_rss-glx-0.7.6.tardist<br />
* neko_xscreensaver-4.21.tardist<br />
* neko_openssl-0.9.7g.tardist<br />
* neko_postfix-2.2.2.tardist<br />
* neko_whois-4.7.2.tardist<br />
* neko_foobillard-3.0a.tardist<br />
* neko_gltron-0.70.tardist<br />
* neko_libmikmod-3.2.0b2.tardist<br />
* neko_sdl_sound-1.0.1.tardist<br />
* neko_timidity++-2.13.2.tardist</p>
<p>In seinem <a href="http://www.nekochan.net/downloads/index.php?path=contrib">contrib-Verzeichnis</a>%2Ffoetz/ hat Foetz noch einen weiteren Schwung neuer Pakete abgelegt:</p>
<ul>
<li>sylpheed_198.tar.bz2</li>
<li>perltk_804027.tardist</li>
<li>samba_3014.tardist</li>
<li>nedit-5.6-dev.tar.bz2</li>
<li>gimpshop_224.tar.bz2</li>
<li>openssl_097g.tardist</li>
<li>gcc40_20050409.tar.bz2</li>
<li>proftpd_130rc1.tar.bz2</li>
<li>xmms_20050408.tardist</li>
<li>gimp_226.tar.bz2</li>
<li>postgresql_802.tar.bz2</li>
<li>ncftp_319.tardist</li>
<li>mysql_4111.tar.bz2</li>
</ul>
