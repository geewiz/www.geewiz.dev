---
layout: post
title: "Gentoo is for learning, Ubuntu is for using"
microblog: false
guid: http://geewiz.micro.blog/2007/05/19/gentoo-is-for.html
date: 2007-05-19T17:32:00+0200
type: post
url: /2007/05/19/gentoo-is-for.html
---

<p>I never used Gentoo myself, but I'm hearing every <a href="http://www.adminlife.net/persoenliches/umstieg-von-gentoo-zu-kubuntu/">now</a> and <a href="http://schimana.net/2007/05/17/umstieg-von-gentoo-zu-kubuntu/">then</a> that Gentoo users tend to switch to Ubuntu once their learning curve flattens and the compile cycles become increasingly annoying. "I want to do work <em>with</em> my system, not <em>on</em> my system", some switchers say.</p>
<p>I've been using Kubuntu on my ThinkPad R52 laptop for months now and I'm totally satisfied. There are no complicated or time-consuming procedures to install new software. I just do <code>apt-get install</code>, and that's it. And from the "universe", "multiverse" and "medibuntu" repositories, I can get all the applications I need.</p>
<p>The same goes for my web servers. With Ubuntu 6.06 LTS, I have everything I need to run web, mail, chat and other services. Thanks to "Long Time Support", I won't have to do an upgrade to a newer distribution version for years.</p>
<p>I've been a SuSE Linux user for many years, but now I guess I could become a Ubuntu Linux user for at least as long. And since I passed the "Learning Linux" phase long ago, I may never be inclined to try Gentoo...</p>
