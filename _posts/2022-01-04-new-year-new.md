---
layout: post
title: "New year, new Linux workstation"
microblog: false
guid: http://geewiz.micro.blog/2022/01/04/new-year-new.html
date: 2022-01-04T21:47:23+0200
type: post
url: /2022/01/04/new-year-new.html
---
Not counting IoT devices and the Raspberry Pi in my PiDP-11, I haven't had a 
Linux machine at home for more than 10 years.
End of last year, I decided to get a dedicated Linux box under my desk again so 
I can enjoy the full Linux experience when I do software and infrastructure 
engineering.

I got my Linux machine today, a Tuxedo Core One midi tower.
I installed it with Manjaro, my new favourite distribution. The process took 
only a few minutes because I came prepared with a USB SSD that manages a 
selection of ISO install images using 
[Ventoy](https://github.com/ventoy/Ventoy).
With the OS installed, I only had to go through my well-maintained [Linux Setup 
checklist](https://publish.obsidian.md/geewiz-kb/Knowledge+Base/Linux+setup+for+Development).
A few hours of setting up projects later, the machine was pretty much ready for 
prime time, and so far, I'm very happy!
Having everything I need for setting up a Linux machine stored in the cloud, for 
example on Gitlab or in Dropbox, made the whole affair a very streamlined 
experience.
Compared to my Ubuntu installations in the past, Manjaro made things much easier 
as well.
The AUR is an amazing package repository.
As someone used to living in PPA hell, it blows my mind how much software is 
available from the Arch User Repository.

I did have a short panic moment, though, when I realised that I managed to order 
the box without a Wifi card.
Somehow, these "Oh, so you actually have to..." moments keep getting more 
frequent for me.
Maybe I'm spoilt by too many all-in-one Apple machines.
Fortunately, I remembered that I still had a Netgear Wifi client "access point" 
in storage that I hadn't yet put up for sale.
I'm going to replace it with a more modern access point in WPS bridge mode to 
build an office LAN.

Even though I now have to switch monitor, mouse and keyboard between Linux box and Windows laptop, being able to quickly do so is going to make my work (and 
spare time) more efficient and fun.
And two years into the pandemic, having more fun is important.
