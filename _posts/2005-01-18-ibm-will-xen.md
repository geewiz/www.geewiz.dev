---
layout: post
title: "IBM will Xen Linux sicherer machen"
microblog: false
guid: http://geewiz.micro.blog/2005/01/18/ibm-will-xen.html
date: 2005-01-18T22:35:00+0200
type: post
url: /2005/01/18/ibm-will-xen.html
---

<p>Mit <a href="http://user-mode-linux.sourceforge.net/">User Mode Linux</a> besch&auml;ftige ich mich schon eine Weile. Mit dieser Kernel-Erweiterung ist es m&ouml;glich, mehrere Linux-Kernel ("Guests") als Anwendung, also als virtuelles Linux-System, in einem Wirts-Linux ("Host") laufen zu lassen. </p>
<p>Die M&ouml;glichkeit, auf einer physischen Maschine mehrere unabh&auml;ngige virtuelle Linux-Instanzen betreiben zu k&ouml;nnen, beg&uuml;nstigt Ressourcenausnutzung und Kostenersparnis.  In der Firma betreiben wir mit User Mode Linux u.a. ein Prelive-System zum Rollout- und Funktionstest neuer Software-Komponenten.</p>
<p><a href="http://www.cl.cam.ac.uk/Research/SRG/netos/xen/index.html">Xen</a> ist eine Alternative zu User Mode Linux, die mehr Kontrolle der virtuellen Maschinen gestattet. IBM will diesen Vorteil weiter vorantreiben. Dies gab Reiner Sailer, Mitglied des <a href="http://www.research.ibm.com/secure_systems_department/">Secure Systems Department</a> bei IBM, in einem <a href="http://sourceforge.net/mailarchive/forum.php?thread_id=6364737">Posting</a>&amp;forum_id=35600 auf der Xen-Entwickler-Mailingliste bekannt.</p>
<p>Die von Sailers Arbeitsgruppe entwickelte Sicherheitsarchitektur "sHype" basiert auf "mandatory access controls" (MAC). Sie erlaubt Xen, Zugriffsregeln zu definieren, mit denen Ressourcenverteilung und Informationsfluss  zwischen virtuellen Linux-Maschinen gesteuert werden kann. Angelehnt ist die Architektur an die von SELinux ("Security Enhanced Linux").</p>
<p>Solche M&ouml;glichkeiten vermisse ich bei User Mode Linux. Hier kann z.B. eine virtuelle Maschine den anderen komplett die Ressourcen (z.B. CPU-Leistung) wegnehmen. Ich werde mich mit Xen intensiver besch&auml;ftigen m&uuml;ssen.</p>
