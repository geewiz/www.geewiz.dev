---
layout: post
title: "Monastisch"
microblog: false
guid: http://geewiz.micro.blog/2005/06/09/monastisch.html
date: 2005-06-09T22:55:07+0200
type: post
url: /2005/06/09/monastisch.html
---

<p>N&auml;chtliche Entdeckung: "IT-Mitarbeiter" ist ein Anagramm f&uuml;r "Imitierter Abt". Was sagt mir das?</p>
<ul>
<li>"Leitet eine Truppe von M&auml;nnern, die durch die Gegend laufen und unverst&auml;ndliches Zeug murmeln." <em>Check.</em>
</li>
<li>"Lebt oft zur&uuml;ckzogen, h&ouml;chstens umgeben von Gleichgesinnten." <em>Check.</em>
</li>
<li>"Das Leben ist nicht Arbeit, sondern Berufung." <em>Check.</em>
</li>
<li>"Das Lesen heiliger B&uuml;cher geh&ouml;rt zum Tagesalltag." <em>Check.</em>
</li>
<li>"Arbeit und Beten sind untrennbar verbunden." <em>Check.</em>
</li>
<li>"Kontakte zu Frauen beschr&auml;nken sich auf ein Minimum." <em>Check.</em>
</li>
</ul>
<p>Jetzt weiss ich auch, warum ich Kapuzenpullover so mag.</p>
