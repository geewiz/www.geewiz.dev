---
layout: post
title: "Neue IRIX-Software"
microblog: false
guid: http://geewiz.micro.blog/2005/03/07/neue-irixsoftware.html
date: 2005-03-07T23:45:12+0200
type: post
url: /2005/03/07/neue-irixsoftware.html
---

<p>Neu auf <a href="http://www.nekochan.net/downloads/index.php:">Nekochan</a></p>
<ul>
<li>neko_ispell-3.2.06.tardist (Rechtschreibungspr&uuml;fung)</li>
<li>neko_quat-1.20.tardist (Fraktalgenerator)</li>
<li>neko_bluefish-0.7-gtk1.tardist (Grafischer HTML-Editor)</li>
</ul>
