---
layout: post
title: "Leadership tips"
microblog: false
guid: http://geewiz.micro.blog/2009/05/19/leadership-tips.html
date: 2009-05-19T02:55:38+0200
type: post
url: /2009/05/19/leadership-tips.html
---

<p>In "<a href="http://www.lifehack.org/articles/management/ten-top-tips-for-the-innovative-leader.html">Ten Top Tips for the Innovative Leader</a>", Paul Sloane gives valuable recommendations to everyone who wants to be more of a leader than just being some levels above in the org chart.</p>
<p>He recommends:</p>
<ol>
<li>Have a vision for change</li>
<li>Fight the fear of change</li>
<li>Think like a venture capitalist</li>
<li>Have a dynamic suggestion scheme</li>
<li>Break the rules</li>
<li>Give everyone two jobs</li>
<li>Collaborate</li>
<li>Welcome failure</li>
<li>Build prototypes</li>
<li>Be passionate</li>
</ol>
<p>Many of his tips remind me of the concepts explained in "<a href="http://p.jochen-lillich.de/amazon/0684852861">First, Break All the Rules</a>", a great leadership book I read recently (and have to review here ASAP). And looking at the fifth point, this doesn't seem like a coincidence.</p>
<p>Especially the first two points resonate with me at the moment because I'm going to undertake a big change effort myself with my department. </p>
<p>Thanks to Paul for his great summary of important leadership qualities and practices!</p>
