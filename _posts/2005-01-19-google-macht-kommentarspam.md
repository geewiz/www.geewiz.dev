---
layout: post
title: "Google macht Kommentarspam sinnlos"
microblog: false
guid: http://geewiz.micro.blog/2005/01/19/google-macht-kommentarspam.html
date: 2005-01-19T20:04:00+0200
type: post
url: /2005/01/19/google-macht-kommentarspam.html
---

<p>Kommentarspam ist eine Plage f&uuml;r Foren und Weblogs. Gerade wegen des intensiven Spammens meines Blogs in den letzten Tagen habe ich dem Kommentarformular einen IQ-Test hinzugef&uuml;gt.</p>
<p>Im <a href="http://www.google.com/googleblog/2005/01/preventing-comment-spam.html">Google Blog</a> ist jetzt zu lesen, dass Google, zusammen mit anderen Suchmaschinen, den Spammern das Wasser abgraben will. Ab sofort wird Google keine Links mehr im Page Rank ber&uuml;cksichtigen, die das Attribut @rel="nofollow"@ enthalten. F&uuml;gt die Blogsoftware also automatisch dieses Attribut in die Link-Tags der Kommentare ein, macht es &uuml;berhaupt keinen Sinn mehr, Spam einzutragen.</p>
