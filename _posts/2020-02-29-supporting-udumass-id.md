---
layout: post
title: "Supporting UDUMASS"
microblog: false
guid: http://geewiz.micro.blog/2020/02/29/supporting-udumass-id.html
date: 2020-02-29T17:00:00+0200
type: post
url: /2020/02/29/supporting-udumass-id.html
---
I'd like us to start gathering funds to have the uncensored version played every day on every major TV station and streaming service.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0Ab4BRAQElw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
