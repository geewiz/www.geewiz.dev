---
layout: post
title: "Google's homebrew server chassis"
microblog: false
guid: http://geewiz.micro.blog/2009/04/30/googles-homebrew-server.html
date: 2009-04-30T05:40:06+0200
type: post
url: /2009/04/30/googles-homebrew-server.html
---

<p>At this year's Efficient Data Center Summit, held April 1st 2009 at Google's Mountain View, CA campus, Ben Jai, Google Server Platform Architect, displayed one of the servers they deploy in all of their global datacenters:</p>
<p>[youtube=http://www.youtube.com/watch?v=J139Aelaf0g&amp;hl=de&amp;fs=1]</p>
<p>While there probably are many other well-known ISPs that use custom-built servers, I find it interesting that Google goes as far as to add a 12V battery to every server to  replace the traditional large UPS systems normally used in datacenters.</p>
