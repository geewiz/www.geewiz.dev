---
layout: post
title: "Bilder vom letzten AK-Wochenende gerettet"
microblog: false
guid: http://geewiz.micro.blog/2004/12/07/bilder-vom-letzten.html
date: 2004-12-07T20:58:00+0200
type: post
url: /2004/12/07/bilder-vom-letzten.html
---

<p>Ich hatte nach dem Aufbaukurs Teil 3 alle Fotos auf meinen Notebook &uuml;berspielt und wie immer auf der Kamera gel&ouml;scht. Aber erst nachdem die Festplatte wegen eines Defekts ausgetauscht worden war, stellte ich fest, dass das Bilderverzeichnis nicht im Backup ber&uuml;cksichtigt worden war. So musste ich die Bilder als verloren abschreiben. :(</p>
<p>Am Freitag Abend stie&szlig; ich dann aber auf die Software <a href="http://jbj.rapanden.dk/magicrescue/.">Magic Rescue</a>  Sie kann gel&ouml;schte Dateien retten, indem sie die rohen Plattendaten nach bestimmten Mustern untersucht.</p>
<p>Und tats&auml;chlich -- ich konnte damit alle Bilder der seit dem AK nicht mehr benutzten Speicherkarte wiederherstellen! Heute abend erzeuge ich die Bildergalerie und lade sie auf den Server.</p>
