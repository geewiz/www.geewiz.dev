---
layout: post
title: "Mindful breaks"
microblog: false
guid: http://geewiz.micro.blog/2014/11/26/mindful-breaks.html
date: 2014-11-26T15:28:57+0200
type: post
url: /2014/11/26/mindful-breaks.html
---
<p>Supported by the little <a href="http://breaktimeapp.com" title="BreakTime &ndash; You need a break">BreakTime</a> app, I&#8217;ve been breaking up my work into chunks for a long time now. I&#8217;ve started with the common Pomodoro intervals of 25 minutes of work followed by a 5 minute break.</p>
<p>Recently, I&#8217;ve switched to a 50/15 rhythm. This gives me longer time to focus on what I&#8217;m doing and I get more options for what to do during my breaks. For example, I can now move around the house a bit (sitting kills, as we know), make myself some tea and chat with my family.</p>
<p>Today, I&rsquo;ve started to do short mindfulness exercises during my breaks, too. I got the idea while testing the <a href="http://www.calm.com" title="calm.com">Calm.com</a> iPhone app. When I meditate in the morning, I use a wooden meditation bench on the living room floow. But while listening to the &#8220;7 steps of Calm&#8221; intro sessions, I realised that if a chair is okay, why not a desk chair, too?</p>
<p>During my 15 minute breaks, I can now do a short mindfulness exercise to regain my focus and still have time to lift my ass around the house.</p>
