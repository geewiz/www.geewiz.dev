---
layout: post
title: "DevOps &ndash; Zehn Tipps f&uuml;r Entwickler"
microblog: false
guid: http://geewiz.micro.blog/2010/09/18/devops-ndash-zehn.html
date: 2010-09-18T21:28:00+0200
type: post
url: /2010/09/18/devops-ndash-zehn.html
---
<p>
    					 					 <a href="http://evan.bottch.com/2010/09/16/devops-ten-tips-for-developers/">http://evan.bottch.com/2010/09/16/devops-ten-tips-for-developers/</a>
<p><strong>1. Verstehe die Beweggr&uuml;nde</strong></p>
<p><strong> 2. Beziehe Ops fr&uuml;hzeitig ein</strong></p>
<p><strong>3. Ein Team</strong></p>
<p><b>4. Ziehe die pers&ouml;nliche Kommunikation vor</b></p>
<p><strong>5. Ops ist ein Endanwender (und ein Teammitglied)</strong></p>
<p><strong>6. Teile Verantwortung</strong></p>
<p><strong>7. Gib nicht nur Bestellungen auf</strong></p>
<p><strong>8. Halte Versprechen ein</strong></p>
<p><strong>9. Missbrauche die Freundschaft nicht</strong></p>
<p><strong>10. Bilde dich weiter</strong></p>
<p>Und zuletzt noch ein Nachtrag in puncto H&ouml;flichkeit: Es schadet nicht, "bitte" und "danke" zu sagen -- und "sorry", falls du was verbockt hast.</p>
