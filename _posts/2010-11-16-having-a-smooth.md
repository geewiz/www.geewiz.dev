---
layout: post
title: "Having a smooth day"
microblog: false
guid: http://geewiz.micro.blog/2010/11/16/having-a-smooth.html
date: 2010-11-16T10:52:00+0200
type: post
url: /2010/11/16/having-a-smooth.html
---

<p>This morning, I woke up. And I mean, for the first time since I went to bed! Which is unusual because over the last weeks, Amalia either woke me up by walking up to my bedside in her sleeping bag in the middle of the night or simply because she has a bit of a cold and occasionally coughs really heavily. At other times, I woke up by myself with thoughts running around my head, not being able to get back to sleep again.
<p /> So, this was a really good night for a change. We had to hurry to get her to daycare in time, but if that's the price of sleeping through, that's okay.
<p /> On the way to the S-office, as always I tapped the button on my headset. Instead of the podcast playlist I expected to hear, the iPhone started a Smooth Jazz channel in TuneIn Radio. What a relaxing way to get the brain running! That's what I call a smartphone.
<p /> I decided to make this whole day a smooth one. Relaxed, swinging, smiling.
<p /> What about you? Leave a comment!</p>
