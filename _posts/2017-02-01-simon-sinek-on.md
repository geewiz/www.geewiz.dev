---
layout: post
title: "Simon Sinek on the game of empathy"
microblog: false
guid: http://geewiz.micro.blog/2017/02/01/simon-sinek-on.html
date: 2017-02-01T15:08:08+0200
type: post
url: /2017/02/01/simon-sinek-on.html
---
<p>I have a man crush on Simon Sinek. I consider his book "Start with Why" essential reading for every entrepreneur and leader. I also did his Why Course and found out amazing things about what drives me.</p>
<p>He&#39;s even better in person. Go watch his talk "Understanding the Game We&#39;re Playing" at Creative Mornings and get some inspiration to practice empathy:</p>
<p><iframe src="https://creativemornings.com/videos/embed/5548" width="400" height="225" frameborder="0"></iframe><br />
Here&#39;s the follow-up Q&amp;A, too:</p>
<p><iframe src="https://creativemornings.com/videos/embed/5625" width="400" height="225" frameborder="0"></iframe></p>
