---
layout: post
title: "Gutes Karma"
microblog: false
guid: http://geewiz.micro.blog/2004/11/12/gutes-karma.html
date: 2004-11-13T00:23:00+0200
type: post
url: /2004/11/12/gutes-karma.html
---

<p>In der Hoffnung, endlich auch mal mehr Zeit unterwegs im Freien oder gar im Ausland zu verbringen, habe ich mir im August 2004 den Digital Music Player "Rio Karma" zugelegt.Das Teil hat eine sehr handliche quadratische Form, obwohl es 20GB Plattenplatz mitbringt. Ich habe inzwischen fast meine ganze Musiksammlung draufgespielt -- und es sind immer noch einige GB frei...</p>
<p>Als Idealist gefallen mir besonders folgende Eigenschaften: </p>
<ul>
<li>Der Karma spielt auch OGG Dateien, deren Qualit&auml;t schon bei platzsparenden 64kbps v&ouml;llig ausreicht. </li>
<li>Der Player kann &uuml;ber seine Dockingstation ins Netzwerk eingebunden werden und ist dann einfach per Webbrowser erreichbar. Auf diesem Weg kann man sich ein kleines Java-Programm namens "Rio Music Manager Lite" herunterladen. Dies ist eine abgespeckte, aber plattformunabh&auml;ngige Version der mitgelieferten Windows-Software und somit auch unter Linux und Mac OS einsetzbar.</li>
<li>Mit dem RMM k&ouml;nnen Musikdateien zwischen Karma und PC ausgetauscht werden (wohlgemerkt in beide Richtungen). Andere Dateien k&ouml;nnen &uuml;ber die "Rio Taxi" Funktion transferiert werden -- der Karma dient auf diese Weise auch als mobile Festplatte.</li>
</ul>
<p>Die Auswahl zu spielender Musik kann bequem nach K&uuml;nstler, Album, Musikgenre oder auch nach Erscheinungsjahr erfolgen. Die Musikdateien sollten dazu sauber mit ID3-Tags versehen sein. Mit Playlists k&ouml;nnen die gespeicherten Songs nach Belieben gruppiert werden. Die Funktion "Rio DJ" erlaubt die automatische Zusammenstellung einer Playlist nach verschiedenen Kriterien, z.B. nur Titel, die ich schon lange nicht mehr angeh&ouml;rt habe, oder nur Songs aus dem Jahr 1988.</p>
<p>Die mitgelieferten Ohrst&ouml;psel habe ich durch etwas bessere von Philips eingetauscht und ich bin mit dem Klang sehr zufrieden.</p>
<p>Auch die Unterst&uuml;tzung im Web f&uuml;r den Player ist gut: auf <a href="http://www.riovolution.com/">Riovolution.com</a> finden sich ein belebtes Forum wie auch Links zu Software-Downloads und anderen Websites. </p>
<p>Ich kann das Ger&auml;t deshalb uneingeschr&auml;nkt empfehlen.</p>
