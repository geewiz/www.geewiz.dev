---
layout: post
title: "Vitalist upgrades its features"
microblog: false
guid: http://geewiz.micro.blog/2007/04/02/vitalist-upgrades-its.html
date: 2007-04-02T07:53:00+0200
type: post
url: /2007/04/02/vitalist-upgrades-its.html
---

<p>After <a href="http://blog.jochen-lillich.de/archives/653-Why-I-use-Vitalist-to-keep-track-of-my-tasks.html">using Vitalist</a> for some weeks now, I think I really found my task planning solution. It's the complete concept of David Allen's <a href="http://p.jochen-lillich.de/amazon/0142000280">Getting Things Done</a> book (by the way, there's finally also a <a href="http://p.jochen-lillich.de/amazon/3492240607">german translation</a>) implemented as an easy to use Web 2.0 application.</p>
<p>Over the weekend, the folks at Vitalist Solutions have <a href="http://www.vitalist.com/neteffect/archives/new-and-improved/">released new features</a> they had announced on their blog over the recent weeks:</p>
<ul>
<li>Contexts as Tags: the contexts a task can be handled in are displayed more prominently.</li>
<li>Sub-Projects: you can now build a project tree by splitting a big project into several small ones.</li>
<li>Quick Add/Edit: entering new tasks is now even easier.</li>
<li>Priorities: you can give every task one of four priorities, symbolized by a coloured star.</li>
</ul>
<p>Michael Ramm has a detailed <a href="http://www.blackbeltproductivity.net/blog/03-27-2007/vitalist-when-it-is-vital-to-get-things-done/">review of Vitalist</a> over at the Black Belt Productivity blog. Like Michael, I see no need to get the paid version with features like encryption, attachments and collaboration. The free version has everything I need to get the calming feeling that everything gets taken care of in time.</p>
<p>So, if you're looking for a GTD solution that you can access from everywhere you have net connectivity, give Vitalist a test drive. (Robert, it's time to offer an affiliate program!)</p>
