---
layout: post
title: "Live microblogging from re:publica"
microblog: false
guid: http://geewiz.micro.blog/2007/04/11/live-microblogging-from.html
date: 2007-04-11T21:27:48+0200
type: post
url: /2007/04/11/live-microblogging-from.html
---

<p>Since many attendees of the german blogger conference <a href="http://re-publica.de/">re:publica</a> in Berlin are avid Twitter users, I'm receeiving my own little live comment stream to my desk.</p>
<p>I can see what talks get positive or negative notions and how the conference is going as a whole. As a presenter at re:publica, I'd use this Twitter stream as a source of feedback to improve my <a href="http://twitter.com/dominik/statuses/24497591">presentation style</a>, <a href="http://twitter.com/nicole_de/statuses/24520851">attitude</a> and <a href="http://twitter.com/dominik/statuses/24476331.">choice of topic</a></p>
<p>It's also interesting to watch attendees even <a href="http://twitter.com/dotdean/statuses/24483731">make appointments</a> over Twitter.</p>
<p>And I already have one very clear insight: at a blogger conference, there's one thing that's even more important than <a href="http://twitter.com/leralle/statuses/24450771">WiFi</a> and <a href="http://twitter.com/oliverg/statuses/24461731">power outlets</a> for all the MacBooks: <a href="http://twitter.com/dotdean/statuses/24462341.">coffee</a> <a href="http://twitter.com/nicole_de/statuses/24470531">Good</a> <a href="http://twitter.com/dobschat/statuses/24482471.">coffee</a> :-)</p>
<p>PS: For the non-Twitterati, there's also a <a href="http://republica.tumblr.com/">TumbleLog</a> and a <a href="http://onelinr.com/channel/republica">Onelinr backchannel</a> live from re:publica.</p>
