---
layout: post
title: "Family First"
microblog: false
guid: http://geewiz.micro.blog/2012/02/20/family-first.html
date: 2012-02-20T02:44:22+0200
type: post
url: /2012/02/20/family-first.html
---
<p>Founding my own business has proven to be a very fulfilling venture, but it&rsquo;s also very time-intensive. Balancing my professional duties with the responsibilities of a father and partner actually is a challenge every day.</p>
<p>Over the years, I had many opportunities to experience how much the support of my family means to me. That&rsquo;s why I follow Gary V&rsquo;s advice in &ldquo;<a href="http://p.jochen-lillich.de/amazon/0061914177">Crush It</a>&rdquo; (which, by the way, did immensely influence me) and chose &ldquo;Family First&rdquo; as my rule number one.</p>
<p>Now, how do I put this rule into practice? The article &ldquo;<a href="http://freelancefolder.com/manifesto-for-a-freelancer-with-a-family/">Manifesto for a Freelancer with a Family</a>&rdquo; on FreelanceFolder has a great answer with which I agree wholeheartedly! Author Brian McDaniel makes the following declarations, adding to each some concrete guidelines:</p>
<ul>
<li>My Family Will Always Come First</li>
<li>I Will Keep My Marriage Healthy</li>
<li>I Will Pour Myself into My Children</li>
<li>I Will Keep Myself Healthy and Sane</li>
</ul>
<p>I think having these principles really helps in making the right decisions and achieving something like &ldquo;work/family balance&rdquo; (I don&rsquo;t like the term &ldquo;work/life balance&rdquo; since I regard my work an essential part of my life). That&rsquo;s why I copied the Manifesto into my &ldquo;Important Notes&rdquo; folder and reread it from time to time.</p>
<p>I have to admit that there are still some points I&rsquo;m struggling with, for example with &ldquo;Be present. Not just physically, but completely present, even when I&rsquo;m working.&rdquo; because it seems to conflict with my very focused working style.</p>
<p>Since I know from experience that working as an employee can be as taxing on your family life as is working for your own business, I recommend reading Brians article to every professional that has (or intends to found) a family.</p>
