---
layout: post
title: "Happiness Reality Check"
microblog: false
guid: http://geewiz.micro.blog/2007/04/21/happiness-reality-check.html
date: 2007-04-21T17:11:00+0200
type: post
url: /2007/04/21/happiness-reality-check.html
---

<p>In <a href="http://blog.jochen-lillich.de/archives/663-Happiness-boosts-productivity.html">Happiness boosts productivity</a>, I announced that I would interview my team about things that make them unhappy. Alexander reminded me in his comment not to forget to ask about sources of happiness, too.</p>
<p>In two meetings, we collected the following issues and discussed necessary consequences.<br />
h3. "What makes me happy working here?"</p>
<ul>
<li>
<em>Challenging tasks:</em> Having opportunities to demonstrate the know-how and creativity necessary to solve problems that don't arise every day is a great motivation booster.</li>
<li>
<em>Colleagues:</em> Working with like-minded people that not only share expertise but also a sense of humour is, well, fun. A team BBQ is highly indicated.</li>
<li>
<em>Open atmosphere:</em> The relaxed way we treat and communicate with each other creates an environment where work can be fun and where you can learn from mistakes instead of having to cover them up.</li>
<li>
<em>Technical infrastructure:</em> Working in the data center of one of Germany's biggest ISP and hosting companies is a huge opportunity to develop new skills. And learning always leads to a great sense of achievement.</li>
<li>
<em>Online team chat:</em> To have a channel for "stuff" helps communicating small chunks of information without cluttering up mailboxes. It's also nice to joke about colleagues without them noticing.</li>
<li>
<em>Off-topic talk:</em> That it's okay to have some watercooler talk helps to get the mind free for the next task and prevents burn-out.</li>
</ul>
<p>h3. "Things that I really don't like"</p>
<ul>
<li>
<em>Short-term solutions:</em> Time pressure often leads to ugly workarounds that don't actually solve the problems but just cover them up. We're going to educate our customers by making them aware of more thorough solution alternatives.</li>
<li>
<em>Office noise:</em> Loud conversations from people walking by or from adjacent areas are really bad on concentration. We'll try to get walls installed that block some of the noise.</li>
<li>
<em>Repetitive tasks:</em> routine work simply is boring. We'll use our technical abilities to automate them as much as possible.</li>
<li>
<em>Interruptions:</em> Our problem no. 1 is that we get interrupted so often by telephone calls, or worse, people coming to our desks asking "May I disturb you?" We'll define a "disturbed of the day" that we'll forward all interruptors to. Everyone that doesn't want to use our ticket system will have to talk to that person, while all others will be able to stay "in the flow".</li>
</ul>
<p>Our two "happiness at work" meetings were wholeheartedly welcomed by my team. We agreed upon having such meetings in regular intervals.</p>
