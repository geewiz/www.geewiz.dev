---
layout: post
title: "SGI steht vor dem Aus"
microblog: false
guid: http://geewiz.micro.blog/2006/02/10/sgi-steht-vor.html
date: 2006-02-10T23:53:20+0200
type: post
url: /2006/02/10/sgi-steht-vor.html
---

<p>Der Hersteller von Hochleistungsrechnern und Grafikworkstations SGI (ehemals Silicon Graphics) liegt offenbar in den letzten Z&uuml;gen seines Todeskampfs: in einer Mitteilung an die amerikanische B&ouml;rsenaufsicht SEC stellte SGI ein <a href="http://www.pro-linux.de/news/2006/9273.html.">Konkursverfahren in Aussicht</a></p>
<p>Es macht mich traurig, den Hersteller der Traummaschinen meiner Jugend in den Bankrott gehen zu sehen. Ich glaube, ich muss mal wieder meine Octane2 in Aktion setzen.</p>
