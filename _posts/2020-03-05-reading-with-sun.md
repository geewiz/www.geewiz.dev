---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2020/03/05/reading-with-sun.html
date: 2020-03-05T13:28:00+0200
type: post
images:
- https://www.geewiz.dev/assets/img/ba2ffffdb6.jpg
photos:
- https://www.geewiz.dev/assets/img/ba2ffffdb6.jpg
url: /2020/03/05/reading-with-sun.html
---
Reading with the sun in my back. 

<img src="/assets/img/ba2ffffdb6.jpg" width="600" height="600" alt="" />
