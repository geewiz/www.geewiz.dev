---
layout: post
title: "Dinge, die ich niemals tun werde"
microblog: false
guid: http://geewiz.micro.blog/2006/06/06/dinge-die-ich.html
date: 2006-06-06T02:35:54+0200
type: post
url: /2006/06/06/dinge-die-ich.html
---

<p>Erstens: einen <a href="http://www.youtube.com/watch?v=taMOfHaExJg">Bellagio-Springbrunnen mit Cola und Mentos</a> nachbauen. Zweitens: Mit einem Modellflugzeug einen erstaunlichen <a href="http://www.youtube.com/watch?v=U7U4UTnjMVE">Kunstflug in einer Sporthalle</a> vorf&uuml;hren.</p>
<p>Es gibt nichts, was es nicht gibt. Dank YouTube kann man sich das jetzt auch alles anschauen.</p>
<p><em>Update:</em> Link zum Kunstflug ist korrigiert!</p>
