---
layout: post
title: "Live again!"
microblog: false
guid: http://geewiz.micro.blog/2006/10/28/live-again.html
date: 2006-10-28T06:32:31+0200
type: post
url: /2006/10/28/live-again.html
---

<p>Today is a happy day! My <a href="http://www.1und1.de">1&amp;1 3DSL</a> got connected, my router has arrived and I'm online with 16 MBit/s downstream and 1 MBit/s upstream. The weekend is saved!</p>
<p>It was a huge test of my patience, but the timing is perfect since I have to migrate my web server in the next few days. My hosting provider Alturo closes shop and I have to move all my websites and email services to a new server before the end of the month. I was prepared to go back to Philippsburg tomorrow night to use my brother's DSL connection, but now I can do the work from home when it fits best.</p>
<p>Additionally, I have to answer a lot of emails that I had to queue because I didn't find the time to respond at work. And of course there's the second episode of <a href="http://www.radioperl.de">Radio Perl</a> that waits to be recorded.</p>
<p>The weekend will not be boring, I guess.</p>
