---
layout: post
title: "Don't let your enemy know you"
microblog: false
guid: http://geewiz.micro.blog/2006/07/01/dont-let-your.html
date: 2006-07-01T23:04:54+0200
type: post
url: /2006/07/01/dont-let-your.html
---

<p>Every second website wants to know who you are. And we all know why. So what can you do to protect your identity from spreading around the web and your mailbox from getting flooded with even more spam?</p>
<p>Easy: invent fake data.</p>
<p>Even easier: the <a href="http://dev.allredtech.com/fakename/">Fake Name Generator</a> does it for you!</p>
<p>This web service not only invents a new name for you, it also provides you with residential data (US-based) with plausible zip code and phone number, and it creates a public <a href="http://www.mailinator.com/">mailinator mailbox</a> where you can receive confirmation emails.</p>
<p>I just feel like being someone else today...</p>
