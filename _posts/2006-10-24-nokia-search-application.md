---
layout: post
title: "Nokia search application for download"
microblog: false
guid: http://geewiz.micro.blog/2006/10/24/nokia-search-application.html
date: 2006-10-24T07:07:31+0200
type: post
url: /2006/10/24/nokia-search-application.html
---

<p>On one of my recent train rides, I transferred all the contacts from my Time/System paper planner to my Nokia E61. Since it also automatically syncs with my Exchange account via the WLAN at work, I now have all my important up to date calendar and contact information on my smartphone. But with a growing amount of data on my smartphone there came a new problem: finding information. Searching for contacts is no challenge, of course, but what I've been missing, for example, was a search function for calendar events: "When was that certain anniversary in 2007 again?"</p>
<p>Thrilled by a post on the E61Life blog, I just downloaded and installed the <a href="http://e61life.com/?p=101">Nokia search application</a> that obviously already is pre-installed on the new Nokia E50. It enables you to search messages, email, calendar, contacts and other files.</p>
<p>Nokia, keep up the good work! My E61 gets more useful by the day.</p>
