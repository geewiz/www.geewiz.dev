---
layout: post
title: "Ay caramba! Skype for Symbian!"
microblog: false
guid: http://geewiz.micro.blog/2006/07/26/ay-caramba-skype.html
date: 2006-07-26T03:15:11+0200
type: post
url: /2006/07/26/ay-caramba-skype.html
---

<p>Mobile Diva Darla Mack confirms rumours about <a href="http://darlamack.blogs.com/darlamack/2006/07/skype_spain_to_.html">Skype releasing a mobile version of their VOIP client</a> for Symbian-based mobile phones:</p>
<blockquote class="posterous_medium_quote">
<p>According to Alberto Lorente, managing director of Skype in Spain, from September, Skype will also be compatible with telephones equipped with the operating system Symbian, which is used by Nokia.</p>
</blockquote>
<p>That's great news, because the hope of a Skype client coming was one of the reasons I decided to get a Nokia E61.</p>
<p>In other news, there will also be a <a href="http://www.golem.de/0607/46728.html">new version of Skype for Mac OS X</a> (german). That's also great because a MacBook is entry no. 1 on my christmas wish list. :-)</p>
