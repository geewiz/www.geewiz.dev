---
layout: post
title: "My Delicious tags visualized"
microblog: false
guid: http://geewiz.micro.blog/2008/06/21/my-delicious-tags.html
date: 2008-06-22T00:46:14+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpimgskitchco_knbei-scaled500.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpimgskitchco_knbei-scaled500.jpg?w=300
url: /2008/06/21/my-delicious-tags.html
---

<p>Created with <a href="http://wordle.net">Wordle</a>:</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpimgskitchco_knbei" height="238" src="/assets/wp-content/2011/05/media_httpimgskitchco_knbei-scaled500.jpg?w=300" width="400" />
</div></p>
<p>(Click to see in full size)</p>
