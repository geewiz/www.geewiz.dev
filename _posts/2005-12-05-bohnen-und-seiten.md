---
layout: post
title: "Bohnen und Seiten"
microblog: false
guid: http://geewiz.micro.blog/2005/12/05/bohnen-und-seiten.html
date: 2005-12-05T04:34:57+0200
type: post
url: /2005/12/05/bohnen-und-seiten.html
---

<p>Ein Projekt mit einem konkreten Ziel hilft mir immer enorm, um neue Dinge zu erlernen. Das gilt insbesondere f&uuml;rs Programmieren. Mitte der Achtziger lernte ich BASIC auf dem VC20, indem ich eine Adressverwaltung schrieb. PHP lernte ich Ende der Neunziger beim Bau eines der ersten deutschsprachigen Linux-Portale.</p>
<p>In diesem Jahrzehnt will ich jetzt erste praktische Schritte in J2EE machen. Die Idee zum Projekt gibt es, und mit Kai habe ich auch einen ersten Mitstreiter. Java 1.5 und JBoss sind installiert, die erste Literatur ist angekommen. Es kann losgehen!</p>
