---
layout: post
title: "Glaub keiner Statistik..."
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/glaub-keiner-statistik.html
date: 2006-06-19T01:50:34+0200
type: post
url: /2006/06/18/glaub-keiner-statistik.html
---

<p>Die Bundesanstalt f&uuml;r Arbeit gab vor knapp drei Wochen stolz bekannt, dass im Mai 255.000 Menschen weniger arbeitslos waren als noch im April. Jetzt wurde bekannt, dass das etwas ungenau formuliert war: im Mai waren nur 255.000 Menschen weniger als "arbeitslos" registriert, denn seit einer Softwareumstellung werden krankgemeldete Arbeitslose jetzt als "arbeitssuchend" eingestuft!</p>
<p>Wer weiss -- wenn man genau hinschaut, stellt sich vielleicht auch heraus, dass eine gro&szlig;e Menge an N&uuml;rnberger Beh&ouml;rden gar nicht so idiotisch ist, wie sie immer wieder aussieht.</p>
