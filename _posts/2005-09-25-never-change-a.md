---
layout: post
title: "Never change a running system"
microblog: false
guid: http://geewiz.micro.blog/2005/09/25/never-change-a.html
date: 2005-09-26T01:26:47+0200
type: post
url: /2005/09/25/never-change-a.html
---

<p>Das Online-Update meines SuSE Linux 9.3 h&auml;lt heute einen besonderen Leckerbissen bereit: "Update auf OpenOffice.org 2.0 Beta Milestone m125. Dies ist ein Update auf die aktuelle Beta Version <em>und eine Reihe von Problemen in allen Bereichen</em>."</p>
<p>Das muss wohl die Version sein, die wir im B&uuml;ro schon eine ganze Weile einsetzen. :)</p>
