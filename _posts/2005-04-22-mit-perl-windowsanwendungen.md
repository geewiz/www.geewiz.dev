---
layout: post
title: "Mit Perl Windows-Anwendungen automatisieren"
microblog: false
guid: http://geewiz.micro.blog/2005/04/22/mit-perl-windowsanwendungen.html
date: 2005-04-22T20:44:55+0200
type: post
url: /2005/04/22/mit-perl-windowsanwendungen.html
---

<p>In <a href="http://www.perl.com/pub/a/2005/04/21/win32ole.html">Automating Windows Applications with Win32::OLE</a> beschreibt Henry Wasserman, wie Perl unter Windows effektiv eingesetzt werden kann. Mit dem erw&auml;hnten Modul ist es m&ouml;glich, Perl-Scripts zu schreiben, die Windows-Anwendungen per OLE steuern. Mit anderen Worten: Perl macht selbst Windows ertr&auml;glicher. :)</p>
