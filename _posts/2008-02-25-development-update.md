---
layout: post
title: "Development update"
microblog: false
guid: http://geewiz.micro.blog/2008/02/25/development-update.html
date: 2008-02-25T06:14:07+0200
type: post
url: /2008/02/25/development-update.html
---

<p>Everyone is asking me for updates. There's Thomas who wants for me to finally release <a href="https://developer.berlios.de/projects/perl-sioc/">perl-SIOC</a>, there are the participants of my Perl online seminar waiting for the final version of the course manual, and there's Eva that's <a href="http://twitter.com/_coolcat/statuses/739911382">curious</a> about how the baby's doing.</p>
<p>Well, what Eva wants of course Eva gets! The baby's great -- alive and kicking would be the correct term. Kicking like a pony, Carolin would add. Here's the latest ultrasound picture.</p>
<p>Now I'll have to put some work into the other two release duties. I'm sure they'll be finished first. :-)</p>
