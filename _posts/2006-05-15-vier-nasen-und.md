---
layout: post
title: "Vier Nasen und Google"
microblog: false
guid: http://geewiz.micro.blog/2006/05/15/vier-nasen-und.html
date: 2006-05-15T19:11:33+0200
type: post
url: /2006/05/15/vier-nasen-und.html
---

<p>Unter dem Titel "Vier Nasen tanken Super" diskutieren Heiko Hebig, Mario Sixtus, Nico Lumma und Sebastian Keil aktuelle Themen rund ums Internet und ver&ouml;ffentlichen die Mitschnitte als Podcast.</p>
<p>In Folge 2, <a href="http://viernasen.blogg.de/eintrag.php?id=2">Vier Nasen und Google</a>, sprechen sie haupts&auml;chlich &uuml;ber Google und das ungew&ouml;hnlich hohe Vertrauen, das das Unternehmen bei seinen Nutzern genie&szlig;t. Sie stellen kritische Fragen, die mich unsicher machen, wie viel Privatsph&auml;re ich f&uuml;r wie viel Nutzen zu opfern bereit bin.</p>
<p>Interessant war f&uuml;r mich auch das Thema "Deutsche Web-Communities"; nicht nur, weil darin die Branchengr&ouml;&szlig;en wie T-Online, WEB.DE und GMX als "Schnachnasen" tituliert werden, sondern auch, weil ich privat gerade an etwas in diesem Bereich entwickle.</p>
