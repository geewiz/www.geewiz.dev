---
layout: post
title: "Internetsperren"
microblog: false
guid: http://geewiz.micro.blog/2009/07/28/internetsperren.html
date: 2009-07-28T16:50:09+0200
type: post
url: /2009/07/28/internetsperren.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_short_quote"><p>Internetsperren sind der Deckel auf der Bierflasche des Alkoholikers - ein unbedeutendes Hindernis auf dem Weg zum Ziel.</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://lebens-mittel-punkt.de/WordPress/?p=72">lebens-mittel-punkt.de</a></div></p>
</div>
