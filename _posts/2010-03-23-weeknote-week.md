---
layout: post
title: "Weeknote #-3 (week 11, 2010)"
microblog: false
guid: http://geewiz.micro.blog/2010/03/23/weeknote-week.html
date: 2010-03-23T19:15:51+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2010/03/23/weeknote-week.html
---
<br />
<h2>Outsourcing work</h2>
<p>One of the four essential activities of a manager is delegation. But how should a business starter delegate when they've got only a few employees, surely with enough tasks on their hands, or even no employee at all? Tim Ferris' book "<a href="http://p.jochen-lillich.de/amazon/0307465357">The 4 hour workweek</a>" made me aware of another possibility: the Virtual Private Assistant, in short VPA. A VPA works for many clients who outsource ancillary tasks to them. They specialize in jobs that can be done via phone or Internet, like booking flights and hotels, answering emails and researching topics.</p>
<p>This week, I tried this kind of delegation myself. A friend of mine was looking for additional work, and I realized that, starting my own business, I could help her start hers. So I suggested to her to try and work as a VPA &mdash; I'd be her first test client. </p>
<p>I started with delegating the task of extending my social network by researching XING members with "Drupal" in their profiles. While I was busy working on my business concept, she started reaching out to new contacts,  taking breaks when the system throttled her request queue. Early next morning, contact confirmations began pouring in. And with them, a lot of surprisingly positive and encouraging responses like: "Sounds interesting, we could cooperate on a win-win basis!", "Please keep me informed because you meet exactly our clients' hosting needs." and "I'm happy to see you fill that obviously empty niche in the market." Happily, I immediately assigned the next task to my VPA: entering the new business contacts into our CRM system. </p>
<p>In conclusion, by outsourcing tasks to a VPA, I not only saved time that I needed for other important things but also gained publicity and even more motivation to start doing serious business. Thanks, Tim!</p>
<h2>REWORK</h2>
<p>I finished "<a href="http://p.jochen-lillich.de/amazon/0091929784">REWORK</a>", the new business book by 37Signals founders Jason Fried and David Heinemeier Hansson. In REWORK, they pick up where they left off with "Getting Real", transforming their experience of running 37Signals for over 10 years into practical tips for business starters. What they describe is what I'd like to call the "4S" approach to business: "Stay Simple, Stay Sane". In pithy language, Fried and Heinemeier Hansson make the point that you should always keep your focus on the business and especially on its outcome:</p>
<ul>
<li>Do without outside money, be frugal instead and keep complete control over your business.</li>
<li>Work hard, but not like a madman; burnout doesn't benefit neither your business nor your health.</li>
<li>Go forward making tiny steps.</li>
<li>Hire on talents and team fit, not on resum&eacute; and formal education.</li>
<li>And if you're not making profit after an appropriate time, it's not business, it's a hobby.</li>
</ul>
<p>The book contains some things I consider common sense (but that's hopefully from the lessons I've learned so far) and a few contradictions (what now, should I "pick a fight" or "ignore my competition"?). But all in all, the book's a refreshing read and well worth the money.</p>
