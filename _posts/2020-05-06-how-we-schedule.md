---
layout: post
title: "How we schedule our days is how we spend our lives"
microblog: false
guid: http://geewiz.micro.blog/2020/05/06/how-we-schedule.html
date: 2020-05-06T12:34:13+0200
type: post
url: /2020/05/06/how-we-schedule.html
---

> "Scheduling is an invaluable tool for habit formation: it helps to eliminate decision making; it helps us make the most of our limited self-command; it helps us fight procrastination. Most important, perhaps, the Strategy of Scheduling helps us make time for the things that are most important to us. How we schedule our days is how we spend our lives." (Gretchen Rubin, Better Than Before)
