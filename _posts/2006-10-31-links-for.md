---
layout: post
title: "links for 2006-10-30"
microblog: false
guid: http://geewiz.micro.blog/2006/10/31/links-for.html
date: 2006-10-31T08:21:32+0200
type: post
url: /2006/10/31/links-for.html
---

<ul class="delicious">
<li>
<div class="delicious-link"><a href="http://shared.snapgrid.com/index.html">GTDTiddlyWiki - your simple client side wiki</a></div>
<div class="delicious-extended">The purpose of GTD Tiddly Wiki is to give users a single repository for their GTD lists and support materials so they can create/edit lists, and then print directly to 3x5 cards for use with the HipsterPDA.</div>
<div class="delicious-tags">(tags: <a href="http://del.icio.us/Geewiz/gtd">gtd</a> <a href="http://del.icio.us/Geewiz/wiki">wiki</a> <a href="http://del.icio.us/Geewiz/productivity">productivity</a> <a href="http://del.icio.us/Geewiz/tools">tools</a> <a href="http://del.icio.us/Geewiz/organization">organization</a> <a href="http://del.icio.us/Geewiz/lifehacks">lifehacks</a> <a href="http://del.icio.us/Geewiz/work">work</a>)</div>
</li>
<li>
<div class="delicious-link"><a href="http://share.skype.com/sites/devzone/2006/10/skype4java_a_developers_collab.html">Skype4Java</a></div>
<div class="delicious-extended">Skype4Java is a wrapper library of Skype API for Java developers.</div>
<div class="delicious-tags">(tags: <a href="http://del.icio.us/Geewiz/java">java</a> <a href="http://del.icio.us/Geewiz/skype">skype</a> <a href="http://del.icio.us/Geewiz/framework">framework</a>)</div>
</li>
<li>
<div class="delicious-link"><a href="http://www.kalyxo.de/2005/07/seibertmedia/schnell-perl-programmieren-lernen/">Schnell Perl programmieren lernen</a></div>
<div class="delicious-extended">Dieser Beitrag will anhand von KUMON, einer japanischen Lehrmethode f&Atilde;&frac14;r Mathematik, aufzeigen, wie ein Vorgehen zur Erlernung von Perl geschaffen werden kann.</div>
<div class="delicious-tags">(tags: <a href="http://del.icio.us/Geewiz/perl">perl</a> <a href="http://del.icio.us/Geewiz/@toread">@toread</a> <a href="http://del.icio.us/Geewiz/radioperl">radioperl</a>)</div>
</li>
</ul>
