---
layout: post
title: "Do you poken?"
microblog: false
guid: http://geewiz.micro.blog/2009/07/22/do-you-poken.html
date: 2009-07-22T14:39:11+0200
type: post
images:
- /assets/wp-content/2011/05/photo.jpg?w=225
photos:
- /assets/wp-content/2011/05/photo.jpg?w=225
url: /2009/07/22/do-you-poken.html
---
<p>
    When the hype first started, my answer was "I don't think so". But last week, two coworkers sported their new gadgets and as the resident certified social network addict, I couldn't help but join the movement. Now, my Poken profile grows while the wave is rolling through the company.
<div class='p_embed p_image_embed'>
<a href="/assets/wp-content/2011/05/photo.jpg"><img alt="Photo" height="667" src="/assets/wp-content/2011/05/photo.jpg?w=225" width="500" /></a>
</div></p>
