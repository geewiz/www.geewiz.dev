---
layout: post
title: "Finally, Skype supports ALSA"
microblog: false
guid: http://geewiz.micro.blog/2006/06/29/finally-skype-supports.html
date: 2006-06-29T20:01:08+0200
type: post
url: /2006/06/29/finally-skype-supports.html
---

<p>The Skype client for Linux is unfortunately way behind its Windows counterpart. So far, only chat and phone functions are available for the free operating system; if you want video conferencing or "Skypecasts", the Windows client is your only choice. But it's even worse: until this week, Skype for Linux didn't even support the ALSA sound system that has been the common way of dealing with sound hardware on Linux for years.</p>
<p>Finally, Skype now released a <a href="http://www.skype.com/download/skype/linux/13beta.html">beta version of Skype 1.3 for Linux</a> with ALSA support. Additional to this desperately awaited feature, the <a href="http://www.skype.com/download/skype/linux/changelog.html">Changelog</a> contains a long list of bug fixes and functions letting Skype for Linux catch up at least a bit to its Windows sibling.</p>
