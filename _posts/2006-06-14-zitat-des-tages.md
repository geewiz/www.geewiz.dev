---
layout: post
title: "Zitat des Tages"
microblog: false
guid: http://geewiz.micro.blog/2006/06/14/zitat-des-tages.html
date: 2006-06-14T21:14:20+0200
type: post
url: /2006/06/14/zitat-des-tages.html
---
<br />
<blockquote class="posterous_short_quote">
<p>Technology is dominated by two types of people: those who understand what they do not manage, and those who manage what they do not understand. -- Archibald Putt</p>
</blockquote>
