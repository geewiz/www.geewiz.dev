---
layout: post
title: "Book review: Perls of Wisdom"
microblog: false
guid: http://geewiz.micro.blog/2007/08/20/book-review-perls.html
date: 2007-08-20T20:29:33+0200
type: post
url: /2007/08/20/book-review-perls.html
---

<p>On the website of my side business, <a href="http://freistil-consulting.de/">Freistil-Consulting</a>, I just posted my <a href="http://www.freistil-consulting.de/node/15">review</a> (in German language) of Randall Schwartz's book "Perls of Wisdom".</p>
<p>The book is a collection of articles that Merlyn wrote for print and online magazines during the last decade. My verdict: While those articles are easy to read piece by piece and permit focusing on one special aspect of Perl programming at a time, you can find similar writeups on the internet for free. And some of the articles are even outdated, which the author himself hints at in more than one article preface. If those articles that really are likely to close some knowledge gaps (like the ones on Object-oriented Perl) are worth the 30 Euro, the prospective reader should decide carefully.</p>
