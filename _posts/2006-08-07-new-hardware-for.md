---
layout: post
title: "New hardware for Skype"
microblog: false
guid: http://geewiz.micro.blog/2006/08/07/new-hardware-for.html
date: 2006-08-07T03:18:33+0200
type: post
url: /2006/08/07/new-hardware-for.html
---

<p>As I don't get weary to point out, there are Skype handsets coming like the <a href="http://www.belkin.com/skype/howitworks/.">Belkin Wi-Fi Phone for Skype</a> They may be handy, but they're also kind of boring.</p>
<p><img width='200' height='140' style=<a href="http://www.verballs.com/">float: left; border: 0px; padding-left: 5px; padding-right: 5px;" src="/uploads/verballs.jpg" alt="" />Not so the "Verballs</a>! These little Skype monsters not only act as a speaker and wave their hands when you get a call, they also lip-sync to your callers!</p>
<p>That's so cute. And funny. And utterly useless.</p>
