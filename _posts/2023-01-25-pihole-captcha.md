---
layout: post
title: "WTF, using PiHole triggers Captcha?"
date: 2023-01-25 10:21 +0000
description: "Allegedly, using PiHole causes Captcha to trigger more often."
image:
category:
tags:
published: true
sitemap: true
---

Recently, I noticed an increasing frequency in which I get presented with Captcha requests. In part, this might have been caused by me switching browsers or reinstalling my laptop. But still, thanks to their annoyance factor, it felt like I was getting more of them than in the past.

Imagine my surprise when I listened to the [Late Night Linux podcast](https://pca.st/zw3bbviz#t=1536.0) and learned that the flood of images of fire hydrants and traffic lights might actually be caused by my use of [Pi-hole](https://pi-hole.net/)!

If that's the case, may I suggest that Google change the prompt from "Are you a robot?" to _"Are you a valuable ad target?"_
