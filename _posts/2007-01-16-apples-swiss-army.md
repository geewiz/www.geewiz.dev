---
layout: post
title: "Apple's swiss army phone"
microblog: false
guid: http://geewiz.micro.blog/2007/01/16/apples-swiss-army.html
date: 2007-01-16T03:06:03+0200
type: post
url: /2007/01/16/apples-swiss-army.html
---

<p>Well, it seems that I was wrong. The iPhone actually is worth the hype. It's not just a smart phone. It's a genius phone. It's...</p>
<p>...everything you need it to be:</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=1xXNoB3t8vM])</p>
<p>Well, almost.</p>
