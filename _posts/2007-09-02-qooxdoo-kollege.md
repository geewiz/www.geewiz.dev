---
layout: post
title: "qooxdoo, Kollege!"
microblog: false
guid: http://geewiz.micro.blog/2007/09/02/qooxdoo-kollege.html
date: 2007-09-02T18:37:00+0200
type: post
url: /2007/09/02/qooxdoo-kollege.html
---

<p>It's nice to find a video featuring a coworker on a big programming website. In this case, Andreas Ecker talks about the qooxdoo Javascript framework on <a href="http://www.theserverside.com/news/thread.tss?thread_id=46720.">TheServerSide.com</a></p>
<blockquote class="posterous_short_quote">
<p>qooxdoo is a robust, state-of-the-art GUI toolkit and AJAX framework. Its origins can be traced back to 2003, when it was already doing AJAX before the term was invented.</p>
</blockquote>
<div>
<br /><b><a href="http://www.dailymotion.com/video/x2v8bn_qooxdoo-theserverside-tech-brief_news">Qooxdoo:  TheServerSide Tech Brief</a></b><br /><i>Hochgeladen von <a href="http://www.dailymotion.com/ciurana">ciurana</a></i>
</div>
