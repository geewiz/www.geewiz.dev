---
layout: post
title: "The long way to a mobile Skype connection"
microblog: false
guid: http://geewiz.micro.blog/2006/09/29/the-long-way.html
date: 2006-09-30T01:12:44+0200
type: post
url: /2006/09/29/the-long-way.html
---

<p>A Skype client for mobile phones is eagerly awaited, especially since phones able to connect to UMTS (3G) and WiFi  networks like my Nokia E61 are entering the market. But the only mobile version of Skype currently available is one for Windows Mobile, and even that isn't widely adopted. Skype announced a version of Skype for Symbian-based mobile phones a while ago now, but it seems we'll have to keep being patient for another while.</p>
<p>According to <a href="http://news.com.com/For">CNet</a>+most%2C+mobile+Skype+still+a+long+distance+away/2100-7352_3-6120610.html?tag=nefd.top,  Niklas Zennstrom, Skype chief executive and co-founder, admitted unexpected difficulties implementing the mobile Skype client:</p>
<blockquote class="posterous_medium_quote">
<p>When we began developing the mobile-phone version, we didn't realize the number of technical obstacles. It is challenging and is taking much longer than expected. We have no publicly available products yet to offer, and I can't give you a timetable.</p>
</blockquote>
<p>In the United States, where phones are tightly tied to mobile operators, there are additional political issues: no operator likes to see people switching from their cell network to cheap WiFi connections available at a growing number of hotspots. In Europe, where customers can get their mobile phone separately from the operator's SIM card, Skype may find it easier to push a cell/Skype hybrid solution to the market. Could it be that we old-worlders actually have an advantage here as an exception?</p>
