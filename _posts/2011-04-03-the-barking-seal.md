---
layout: post
title: "The Barking Seal: Fun with date"
microblog: false
guid: http://geewiz.micro.blog/2011/04/03/the-barking-seal.html
date: 2011-04-03T16:57:00+0200
type: post
url: /2011/04/03/the-barking-seal.html
---
<p>Over at The Barking Seal, I found a nice demonstration what's possible with the <code>date</code> command: <a href="http://www.barkingseal.com/2011/03/fun-with-date/">Fun with Date</a>.</p>
<p>Especially, the handling of Epoch timestamps and relative dates is very useful.</p>
