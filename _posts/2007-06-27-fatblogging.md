---
layout: post
title: "Fatblogging: 95"
microblog: false
guid: http://geewiz.micro.blog/2007/06/27/fatblogging.html
date: 2007-06-27T19:30:00+0200
type: post
url: /2007/06/27/fatblogging.html
---

<p>This is great! The scale now displays 95 kg. It's in the upper area of 95 kg, but let's not be overexact. I wasn't exactly 100 kg when I started fatblogging, so I can claim to have lost 5 kg of body weight so far. I have to admit that I dreaded weighing myself this time, because I hadn't been that strong lately when it came to sweets. But seeing that I reached 95 nonetheless, I feel extra idiotic because I could have achieved even more weight loss by not caving in to my cravings.</p>
<p>Apart from sabotaging my fatblogging success, I managed quite well to separate the intake of carbohydrates from the one of fat. I really haven't changed my diet drastically, I just try to rule out food that combines fat and sugar/starch. This doesn't affect my breakfast, because I use to have sweet breakfast with bread, honey and jam. I don't like eating sausages or bacon this early anyway. By having my muesli with fat-reduced yogurt, I bend the rules a bit; I think I can afford that. For lunch, I have to be a bit more careful. Our company restaurant usually mixes noodles or potatoes with meat and a sauce made of oil and starch -- totally off-limits. If there's noodles with vegetables, I ask to leave the sauce. Today, at the Chinese restaurant, I chose the buffet lunch, which gave me the opportunity to take some meat with vegetables and hot sauce but no rice. So, in summary, there are two alternatives: first, carbohydrates  from rice, potatoes or noodles with vegetables and, on the other hand, fat from meat, sauce etc. with vegetables. With a bit of looking around, those aren't too hard to find.</p>
<p>My running progress is steady. Today, I ran my route for the last time divided into three parts of 9 minutes running and 2 minutes walking. Next time, there will be only two halves with 14 minutes of running and a short walking break in between. That's actually the last phase before running 30 minutes in one go! Unfortunately, the weather has been really bad in the morning most of the days lately. If there's just slight drizzle like this morning, I don't mind. But I don't like risking my health by running in the pouring rain. I hope that the sun will fulfill its summer duty reliably again soon, as I'm slowly getting into summer shape!</p>
