---
layout: post
title: "I believe in working remotely, too."
microblog: false
guid: http://geewiz.micro.blog/2013/10/13/i-believe-in.html
date: 2013-10-13T22:05:09+0200
type: post
url: /2013/10/13/i-believe-in.html
---
<p>We've now been living at our new house in Bray for almost two months now and we're happily settling in. The move to Ireland has had almost no impact on my work because I've built freistil IT as a virtual company from the get-go. It really doesn't matter where our team members do their work; it only matters that they do a great job.</p>
<p>Being able to freely move to another place is only one of the advantages we have as a distributed team. In his post on the Stack Exchange blog, David Fullerton lists several more reasons &ldquo;<a href="http://blog.stackoverflow.com/2013/02/why-we-still-believe-in-working-remotely">Why We (Still) Believe in Working Remotely</a>&rdquo;:</p>
<ul>
<li><strong>It lets you hire good people who can&rsquo;t move.</strong> Maybe they've just bought a house or they need to take care of a family member. Not being able to hire people who've made a commitment doesn't make any sense to me.</li>
<li><strong>You don&rsquo;t lose people to silly things like their significant other going to medical school.</strong> Or like fulfilling their wish of moving to another country&hellip;</li>
<li><strong>When done right, it makes people extremely productive.</strong> We've built <a title="freistilbox | Next-generation managed hosting" href="http://www.freistilbox.com">freistilbox</a>, our platform for <a href="http://www.freistilbox.com/managed-hosting/drupal">Managed Drupal Hosting</a>&nbsp;and <a href="http://www.freistilbox.com/managed-hosting/wordpress">Managed WordPress Hosting</a> with a team of three doing everything from IT architecture to payment processing and accounting. The flexibility of our work environment helped us not getting burnt out despite many challenges.</li>
<li><strong>It makes you focus on more than butts in chairs.</strong> Big companies like Yahoo! and HP recently called their remote workers back into offices with the explanation that this will make their teams more effective. I think that's nonsense. &ldquo;Going into office&rdquo; doesn't equal good work and &ldquo;counting butts in chairs&rdquo; doesn't equal good management.</li>
</ul>
<p>David goes on with giving some insight into their learnings and how they collaborate. I highly recommend reading the whole post.</p>
<p>Here are a few more reasons why we, too, think that the benefits of working remotely outweigh its disadvantages:</p>
<ul>
<li><strong>It makes expenses more effective.</strong> Instead of paying for more and more office space, we rather put money into the tools that make us more flexible and productive (powerful laptops, mobile internet access, collaboration software, coffee machines).</li>
<li><strong>It makes teamwork multi-threaded.</strong> It's like parallel processes in an operating system: Once you have the &ldquo;inter-process communication&rdquo;, i.e. collaboration tools and processes, in place, each team member can work independently without relying on everyone being readily available at the opposite desk.</li>
<li><strong>It makes choosing the right things easier.</strong> My family is the most important thing in my life. Back when I was working in an office, I could not go home just to babysit for half an hour while my precious had a haircut. Today, a haircut can be an opportunity for both of us to take a break from our usual responsibilities.</li>
</ul>
<p>I'm the first one to admit that running a distributed team has its challenges. And there are ways to master them. If you'd like to know more, please leave your question in the comments!</p>
<p><em>Want to make more awesome from whereever you're the most happy? <a title="Jobs at freistil IT" href="http://www.freistil.it/jobs/">Join our team</a>!</em></p>
