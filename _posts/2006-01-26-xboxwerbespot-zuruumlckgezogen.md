---
layout: post
title: "XBox-Werbespot zur&uuml;ckgezogen"
microblog: false
guid: http://geewiz.micro.blog/2006/01/26/xboxwerbespot-zuruumlckgezogen.html
date: 2006-01-26T20:21:00+0200
type: post
url: /2006/01/26/xboxwerbespot-zuruumlckgezogen.html
---

<p>Beim <a href="http://www.techeblog.com/index.php/tech-gadget/banned-xbox-360-ad">TechEBlog</a> findet sich ein Werbespot f&uuml;r die XBox360, bei dem man sich daf&uuml;r entschied, ihn nicht zu senden. Offenbar will man sich nicht dem Vorwurf aussetzen, nicht das Kind, sondern den Terroristen im Erwachsenen anzusprechen. Ich find ihn lustig, denn er erinnert mich an fr&uuml;here Zeiten auf Baustellen und Spielpl&auml;tzen.</p>
