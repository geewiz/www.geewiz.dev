---
layout: post
title: "Productive virtual meetings"
microblog: false
guid: http://geewiz.micro.blog/2012/06/24/productive-virtual-meetings.html
date: 2012-06-24T22:41:47+0200
type: post
url: /2012/06/24/productive-virtual-meetings.html
---
<p>At <a href="http://freistil.it">freistil IT</a>, we&#8217;re not sharing an office. Instead, our work environment is completely virtual. &#8220;Coming into office&#8221; means logging in to our internal chat server &#8211; from home, from a coworking space or (quite frequently, in my case) from Starbucks. On our IRC server, we have channels for different purposes, for example the &#8220;virtual watercooler&#8221; without a pre-defined topic, or the &#8220;#incident&#8221; channel where we manage an ongoing systems outage.</p>
<p>This means that our meetings are virtual, too. We mainly use Skype (since there&#8217;s actually still no better alternative). While meetings that have all people in a room have their own problems (hands up if you thought at least once &#8220;Oh dear, please, someone shoot me now&#8221;), virtual meetings need even more effort to be effective.</p>
<p>On LifeHack, I recently found a great list of <a href="http://www.lifehack.org/articles/communication/tips-for-having-great-virtual-meetings.html">Tips for Having Great Virtual Meetings</a>. The most important point comes right at the beginning of the article: &#8220;The three most important ingredients of a successful virtual meeting are trust, communication and ready access to information.&#8221; These three ingredients actually depend on each other and create the foundation of productive teamwork.</p>
<p>From the list of tips, these are the three I think are most important:</p>
<ul>
<li>&#8220;Before the meeting, make sure attendees have all the preparation materials they will need and the time to review them.&#8221; That&#8217;s a prerequisite for <em>every</em> kind of meeting, virtual or not. The worst meetings are those where everyone comes unprepared.</li>
<li>&#8220;Solicit participation.&#8221; By keeping everyone engaged in the meeting, you can prevent people &#8220;spending their time more effectively&#8221; by checking email or Google Plus.</li>
<li>&#8220;Assign a meeting monitor.&#8221; Having someone focus on feedback coming in from the different participants helps that everyone feels being &#8220;heard&#8221; and connected.</li>
</ul>
<p>There&#8217;s also a tip on the list that I disagree with: &#8220;Begin with a quick warm-up.&#8221; If all participants already know each other, I don&#8217;t think it&#8217;s necessary to spend (may I say &#8220;waste&#8221;?) time on things that don&#8217;t contribute to the purpose of the meeting. This reminds me of a story where, in a meeting at Apple, somebody started to chat about the weekend and quickly got interrupted by Steve Jobs saying &#8220;Can we raise the tone of conversation here?&#8221; I feel the same. There are other ways of letting colleagues know about each other&#8217;s news outside of meetings, for example in chat rooms or on an internal social network like <a href="http://www.yammer.com">Yammer</a>.</p>
<p>Instead, I&#8217;d add this tip to the list: Keep the meeting&#8217;s agenda and minutes in a shared document that all participants can follow in real-time, for example on Google Docs. This keeps everyone literally &#8220;on the same page&#8221; and even spares the effort of having to write and send a protocol after the meeting concludes.</p>
<p>What&#8217;s your experience with virtual meetings? How do you make sure they&#8217;re not a waste of time and bandwidth?</p>
