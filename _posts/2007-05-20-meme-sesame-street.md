---
layout: post
title: "[Meme] Sesame Street"
microblog: false
guid: http://geewiz.micro.blog/2007/05/20/meme-sesame-street.html
date: 2007-05-20T20:39:09+0200
type: post
url: /2007/05/20/meme-sesame-street.html
---

<p>HulaLena made me go on a trip back to my childhood by <a href="http://dreisamblog.twoday.net/stories/3712789/:">asking some questions about Sesame Street</a></p>
<p>h3. What Sesame Street character did you find the coolest?</p>
<p>Lefty the Salesman ("Schlehmil"). I found it hilarious how Ernie always thwarted his attempts to sneakily sell an O like it was something forbidden. </p>
<p>h3. Whom didn't you like?</p>
<p>The grumpy "Herr B&ouml;defeld" from the german insertions.</p>
<p>h3. Can you recite a song from Sesame Street? Which one?</p>
<p>I know the title song by heart, and both my brother and I still love to recite <a href="http://www.toughpigs.com/anthkermit02.htm.">Hey diddle diddle, the cat has a fiddle, the cow jumps over the moon</a></p>
<p>h3. What tought you Sesame Street?</p>
<p>One of the most important things a teacher needs to know I learned from Grover ("Grobi"): you have to really make an effort if you want to teach people things. "Near... (running to the back) Far! (running to the front) Near... (huff huff huff) Far again!" </p>
<p>h3. When did you watch Sesame Street the last time?</p>
<p>I'm afraid that must be two decades ago. But I still like to watch The Muppets, especially the movies, e.g. "The Muppets Treasure Island" ("Not bad for an amphibian, hm?").</p>
<p>I pass this <a href="http://www.bloekschaf.de/archives/103-Kaesekoenigin">St&ouml;ckchen" on to "Carolin</a>!.html and <a href="http://blog.kai-uwe-beyer.de/index.php?/archives/608-Das-Rotkraut-And-the-winner-is.html.">Kai</a></p>
