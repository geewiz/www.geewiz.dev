---
layout: post
title: "Chiquita wird kaufbar"
microblog: false
guid: http://geewiz.micro.blog/2005/10/24/chiquita-wird-kaufbar.html
date: 2005-10-24T03:34:56+0200
type: post
url: /2005/10/24/chiquita-wird-kaufbar.html
---

<p>Da ich leicht allergisch gegen Steinobst bin, geh&ouml;ren Bananen zu den naheliegenden Obstsorten, wenn ich mir mal ausnahmsweise Vitamine zuf&uuml;hren will. Bananen sind aber gleichzeitig die Obstsorte, bei der sehr h&auml;ufig Natur und Arbeiter schamlos ausgebeutet werden. Und regional angebaut k&ouml;nnen sie aus naheliegenden Gr&uuml;nden auch nicht werden. Daher habe ich bisher nur ganz selten Bananen gekauft.</p>
<p>Ich war also sehr positiv &uuml;berrascht, als ich beim <a href="http://www.shopblogger.de/blog/archives/2147-Chiquita">Shopblogger</a>!-thumbs-up.html las, dass ausgerechnet die bekannteste Marke eine Kooperation mit der <a href="http://www.rainforest-alliance.org/">Rainforest Alliance</a> eingegangen ist. Ich werde in Zukunft ganz einfach und mit gutem Gewissen &ouml;kologisch und sozial einwandfrei gehandelte Fr&uuml;chte einkaufen k&ouml;nnen!</p>
