---
layout: post
title: "How to unsuck meetings"
microblog: false
guid: http://geewiz.micro.blog/2013/07/08/how-to-unsuck.html
date: 2013-07-08T15:34:16+0200
type: post
url: /2013/07/08/how-to-unsuck.html
---
<p>Nowadays, everyone seems to hate meetings. And, very much like a broken relationship, we keep having them all the same. Obviously, we can't get rid of meetings, so I think it's a good thing to see how we can make them worth the effort.</p>
<p><a href="http://katemats.com/meetings-suck/">Kate Matsudeira hates meetings, too.</a> As a leader, she spends a lot of time in meetings, and she wants that time to return as much value as possible.</p>
<blockquote>
<p>"Thankfully wasteful meetings don&rsquo;t have to be the course du jour. No matter what kind of meetings you&rsquo;re involved in, you can do a lot to make that time more productive.&nbsp; In fact as you can make everyone&rsquo;s time more useful by simply being prepared."</p>
</blockquote>
<p>Kate finds that there are two main causes for bad meetings:</p>
<ul>
<li>They lack structure or purpose</li>
<li>Leaders come to them with unrealistic expectations</li>
</ul>
<p>Both causes have their roots in communication. And she offers really good advice that I know to work from my own experience. <a href="http://katemats.com/meetings-suck/">Recommended reading</a>!</p>
