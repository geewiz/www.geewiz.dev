---
layout: post
title: "Heute ist mein Tag"
microblog: false
guid: http://geewiz.micro.blog/2005/11/03/heute-ist-mein.html
date: 2005-11-03T21:35:13+0200
type: post
url: /2005/11/03/heute-ist-mein.html
---

<p>Kai hat mich grade darauf hingewiesen, dass heute der "Welttag des Mannes" ist.</p>
<p>Zu diesem Anlass hat man sich bei der Financial Times mal genauer angesehen, wie es um die M&auml;nner im Vergleich zu den Frauen steht: <a href="http://www.ftd.de/me/tl/28942.html.">Frauen, ihr macht uns krank!</a></p>
<p>Schade nur, dass mein Schatz zu weit weg ist, um den Tag geb&uuml;hrend mit mir zu begehen. Massage&ouml;l w&auml;re genug da.</p>
<p>Dennoch: Liebe Mitm&auml;nner, ich w&uuml;nsche euch einen sch&ouml;nen Tag. :)</p>
