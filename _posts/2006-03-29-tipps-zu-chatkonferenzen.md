---
layout: post
title: "Tipps zu Chat-Konferenzen"
microblog: false
guid: http://geewiz.micro.blog/2006/03/29/tipps-zu-chatkonferenzen.html
date: 2006-03-30T00:04:52+0200
type: post
url: /2006/03/29/tipps-zu-chatkonferenzen.html
---

<p>Nachdem ich gestern in der CAJ zum ersten Mal eine virtuelle Leitungssitzung moderiert habe, habe ich einen <a href="http://www.jochen-lillich.de/article/besprechungen-per-chat-abhalten">Artikel &uuml;ber Beprechungen per Chat</a> begonnen. Hier halte ich Erfahrungen und Hinweise aus der Praxis fest, die helfen k&ouml;nnen, erfolgreich Zeit und Fahrtkosten einzusparen, indem man sich in virtuellen Meetingr&auml;umen trifft.</p>
