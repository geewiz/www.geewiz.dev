---
layout: post
title: "Vista is innovative, dammit!"
microblog: false
guid: http://geewiz.micro.blog/2006/12/15/vista-is-innovative.html
date: 2006-12-15T23:59:31+0200
type: post
url: /2006/12/15/vista-is-innovative.html
---

<p>Finally, The New York Times columnist David Pogue debunks all the badmouthing of Windows Vista being just a cheap rip-off of Mac OS X:</p>
<p>[youtube=[www.youtube.com/watch](http://www.youtube.com/watch?v=MDNuq94Zg_8])</p>
<p>(via <a href="http://www.mac-essentials.de/index.php/mac/article/18538/">Mac Essentials</a>)</p>
