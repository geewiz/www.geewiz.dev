---
layout: post
title: "First E61 crash"
microblog: false
guid: http://geewiz.micro.blog/2006/08/28/first-e-crash.html
date: 2006-08-28T19:32:22+0200
type: post
url: /2006/08/28/first-e-crash.html
---

<p>Something seems to be buggy in the WiFi implentation of my E61. Last week, I got some kind of "white screen of death" everytime my E61 tried to connect to the company WLAN. (And it connects quite often due to Mail for Exchange.) That white screen seems to go away without a reboot after some time, but it's quite annoying nonetheless. And yesterday, when I turned the phone back on after having it shut off on Saturday, I was greeted with "Phone start-up failed. Contact the retailer." According to my web search, that's often related to WiFi problems, too. </p>
<p>Since I didn't want to send my phone on a weeks-long journey, I tried a factory reset by starting it up with the buttons "*", "3" and "green" pressed. It then took several forced 5-minute pauses because I couldn't effing remember my phone lock password, but finally, I had a working phone again. An <em>empty</em> working phone, to be exact.</p>
<p>But I didn't have much software installed anyway (one of the advantages of Web 2.0), and all important data (calendar and contact entries) could be restored by syncing with <a href="http://zyb.com">ZYB.com</a> and our Exchange server.</p>
<p>I guess that's the consequence of that Nokia doesn't manufacture "mobile phones" any more but "multimedia computers". So the same rule applies: don't forget to backup. Jesus saves, and so should you.</p>
