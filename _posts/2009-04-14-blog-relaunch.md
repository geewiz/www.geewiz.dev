---
layout: post
title: "Blog Relaunch"
microblog: false
guid: http://geewiz.micro.blog/2009/04/14/blog-relaunch.html
date: 2009-04-14T06:12:14+0200
type: post
url: /2009/04/14/blog-relaunch.html
---

<p>All those sick days (of the last three weeks, I've spent two in bed) had one advantage: I had time to tackle some things.</p>
<p>One of those things was consolidating my old Serendipity blog and the current one, running on Drupal, into one Wordpress blog. I wanted to switch from Drupal to Wordpress because WP is more focused on blogging and there are some features (e.g. trackbacks, remote blogging via XML-RPC) that Drupal doesn't do as well.</p>
<p>It took me about 12 hours of Perl hacking to write a converter that reads posts and comments from a source database (Drupal or S9Y) and writes them into a destination database (WP). So, all posts since 2004 including their comments should have made their way into the new blog!</p>
<p>Let's see if the new possibilities help me in writing posts in shorter intervals. Of course, that's what I've been telling myself for weeks now. ;-)</p>
