---
layout: post
title: "Teure Freunde"
microblog: false
guid: http://geewiz.micro.blog/2006/01/06/teure-freunde.html
date: 2006-01-06T11:18:36+0200
type: post
url: /2006/01/06/teure-freunde.html
---

<p>Manchmal bin ich mir nicht mehr so sicher, ob es gut ist, wenn einen die eigenen Freunde so gut kennen. Zum Beispiel hat Kai vor kurzem das <a href="http://engadget.com/2006/01/04/netgear-wifi-phone-for-skype-no-pc-required/">Netgear WLAN Phone</a> entdeckt und prompt <a href="http://blog.ksile.de/index.php?/archives/396-Skype-WLAN-Telefon.html.">gebloggt</a> Dabei hatte ich mir doch vorgenommen, dieses Jahr mal weniger Elektronik zu kaufen...</p>
