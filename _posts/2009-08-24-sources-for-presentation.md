---
layout: post
title: "Sources for presentation images"
microblog: false
guid: http://geewiz.micro.blog/2009/08/24/sources-for-presentation.html
date: 2009-08-24T04:49:38+0200
type: post
url: /2009/08/24/sources-for-presentation.html
---

<p>Recently, I gave a talk about "Effective Presentations", giving a short overview how to prepare and hold presentations that offer a concrete gain to the audience.</p>
<p>I mentioned <a href="http://www.amazon.de/dp/3827327083?camp=0&amp;creative=0&amp;linkCode=as1&amp;creativeASIN=3827327083&amp;tag=geewizweblog-21">Presentation Zen</a> and <a href="http://www.amazon.de/dp/0596522347?camp=0&amp;creative=0&amp;linkCode=as1&amp;creativeASIN=0596522347&amp;tag=geewizweblog-21">slide:ology</a>, my favourite books about transferring insight via presentations, and how they advocate replacing bullet-pointed text by images.</p>
<p>Just in time, Smashing Magazine posted an article about <a href="http://www.smashingmagazine.com/2009/08/16/free-and-commercial-stock-photography-sites/">sources for presentation images</a>, so I'll simply spread the link to my participants.</p>
