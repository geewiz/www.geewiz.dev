---
layout: post
title: "Fishmaster"
microblog: false
guid: http://geewiz.micro.blog/2006/08/01/fishmaster.html
date: 2006-08-01T19:38:09+0200
type: post
url: /2006/08/01/fishmaster.html
---

<p>Thanks to <a href="http://www.madchickenfactory.de/home?wid=75">Tom</a>&amp;func=viewSubmission&amp;sid=354 and Kluus, I just had a quite different impression from one of my favourite songs, Nightwish's "Wishmaster".</p>
<p>I just watched this YouTube video from a guy that made his own video clip based on <a href="http://www.youtube.com/watch?v=gg5_mlQOsUQ.">misheard lyrics of the song</a> "Hamster -- A dentist..." I had to constrain myself so much from scaring my colleagues by rolling on the floor laughing.</p>
