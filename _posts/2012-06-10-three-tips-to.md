---
layout: post
title: "Three tips to keep you sane in your business"
microblog: false
guid: http://geewiz.micro.blog/2012/06/10/three-tips-to.html
date: 2012-06-10T19:59:39+0200
type: post
url: /2012/06/10/three-tips-to.html
---
<p>I&rsquo;ve been working in my own business for over 2 years now and I enjoy it very much. Success and fun in business is a great thing but what about the rest? Although &mdash; and since &mdash; there are more things to life, I sometimes struggle to keep the business from eating up all my time and energy.</p>
<p>In &ldquo;<a href="http://under30ceo.com/stay-sane-while-managing-your-business/" title="Stay Sane While Managing Your Business">Stay Sane While Managing Your Business</a>&rdquo;, I&rsquo;ve found three good tips that can help prevent your work life from overwhelming you.</p>
<p>Tip no. 1: <strong>Cut Clutter</strong> </p>
<p>Even if you&rsquo;ve never heard of entropy, you can see it in action everywhere in your life where disorder tends to grow. Clutter means extra work, so it&rsquo;s important to fight it whereever it rears its ugly head.</p>
<p>Tip no. 2: <strong>Take Control</strong></p>
<p>Never let go of the steering wheel of your ship, especially in heavy waters. Set yourself goals (both for your business and your personal life) and keep them in sight.</p>
<p>Tip no. 3: <strong>Have Fun</strong></p>
<p>Fun is no extracurricular activity. &ldquo;Never continue in a job you don&rsquo;t enjoy. If you&rsquo;re happy in what you&rsquo;re doing, you&rsquo;ll like yourself, you&rsquo;ll have inner peace. And if you have that, along with physical health, you will have had more success than you could possibly have imagined.&rdquo; (Johnny Carson)</p>
<p>If you have 5 more minutes, read the <a href="attribution">article</a> for more details on each tip!</p>
