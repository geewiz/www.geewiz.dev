---
layout: post
title: "Shopping in slow motion"
microblog: false
guid: http://geewiz.micro.blog/2006/08/27/shopping-in-slow.html
date: 2006-08-27T04:05:08+0200
type: post
url: /2006/08/27/shopping-in-slow.html
---

<p>I love the folks from Improv Everywhere. After invading a Best Buy shop with a crowd of seem-to-be-employees, they now <a href="http://www.improveverywhere.com/mission_view.php?mission_id=59.">slowed time in a Home Depot shop</a></p>
<blockquote class="posterous_medium_quote">
<p>We would sychronize our watches and then walk over to Home Depot and shop. At exactly 4:15 we would all begin moving in slow motion. We'd do that for five minutes, and then shop normally for five minutes as if nothing had happened. At exactly 4:25 we would all freeze in place for five minutes. When that was over we would go back to normal and eventually leave the store.</p>
</blockquote>
<p>It's hilarious to see the reactions of other shoppers and the shop employees. I'd love to participate in something like that some time.</p>
