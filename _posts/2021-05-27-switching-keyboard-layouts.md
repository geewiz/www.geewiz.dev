---
layout: post
title: "Switching keyboard layouts easily on Windows 10"
microblog: false
guid: http://geewiz.micro.blog/2021/05/27/switching-keyboard-layouts.html
date: 2021-05-27T11:41:25+0200
type: post
url: /2021/05/27/switching-keyboard-layouts.html
---
It's fun to follow in the footsteps of [@isotopp](https://twitter.com/isotopp) getting acquainted with Windows 10. In a recent post, he explained [how to set up multiple keyboard layouts](https://isotopp.github.io/2021/05/15/windows-10-and-umlauts-with-an-english-keyboard.html), a necessary task when you're switching between ANSI for coding and German for writing. 

I'll add a nifty shortcut for reconfiguring your keyboard with your keyboard: You can switch between layouts by holding the Windows key and pressing Space.
