---
layout: post
title: "Getting another blog done"
microblog: false
guid: http://geewiz.micro.blog/2007/04/17/getting-another-blog.html
date: 2007-04-17T21:00:58+0200
type: post
url: /2007/04/17/getting-another-blog.html
---

<p>I just wrote an introduction to Getting Things Done on the latest addition to my own little blog network: on <a href="http://www.selbstadministration.de/">selbstadministration.de</a>, I'll cover the topics of self management, organization and using your time reasonably (e.g., for maintaining another blog). Since I'm an IT guy, I'll also have a look at tools and tricks for a more effective and efficient life.</p>
<p><a href="http://www.43folders.com">Merlin Mann</a> begged me to not make him obsolete, so I decided to write in german language. ;-)</p>
<p><em>I wouldn't mind if some of you supported me in feeding the blog -- it doesn't have to be a one-man-show. So, if you're interested in writing about productivity and life hacks, give me a nudge!</em></p>
