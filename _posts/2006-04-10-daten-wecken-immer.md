---
layout: post
title: "Daten wecken immer Begehrlichkeiten"
microblog: false
guid: http://geewiz.micro.blog/2006/04/10/daten-wecken-immer.html
date: 2006-04-11T00:19:10+0200
type: post
url: /2006/04/10/daten-wecken-immer.html
---

<p>Warum wollen Sicherheitskr&auml;fte an unsere Daten? Weil sie da sind.</p>
<p>Wenn in unserem Land irgendwo Daten gesammelt werden, dann normalerweise aus einem klaren Ziel und unter bestimmten Bedingungen. Nicht selten gibt es sogar gesetzliche Regelungen, die den Missbrauch der Daten verhindern sollen.</p>
<p>Dass staatliche Institutionen dennoch immer wieder die Finger nach Daten ausstrecken, die sie gar nichts angehen, best&auml;tigte mir gestern der Heise-Artikel &uuml;ber das <a href="http://www.heise.de/newsticker/meldung/71847">Google-WLAN</a> in San Francisco. Dieser erkl&auml;rt, dass sich Datensch&uuml;tzer &uuml;ber den Missbrauch der monatelang gespeicherten Nutzungsdaten Sorgen machen. Erg&auml;nzend heisst es:</p>
<blockquote class="posterous_medium_quote">
<p>Dass solche Bef&uuml;rchtungen keineswegs theoretischer Natur sind, zeigt das Beispiel Toll Collect. Toll Collect sammelt nicht nur Autobahngeb&uuml;hren, sondern als Nebenprodukt auch Daten &uuml;ber das Woher und Wohin der erfassten LKWs. Obwohl im Mautgesetz seinerzeit explizit Sicherungen zur Zweckbindung der Daten eingebaut wurden, fordern Polizei, Staatsanwaltschaft oder Politiker bei passender Gelegenheit gerne, die Daten auch f&uuml;r die Aufkl&auml;rung oder Bek&auml;mpfung von Verbrechen einzusetzen.</p>
</blockquote>
