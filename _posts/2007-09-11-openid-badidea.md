---
layout: post
title: "OpenID = BadIDea?"
microblog: false
guid: http://geewiz.micro.blog/2007/09/11/openid-badidea.html
date: 2007-09-11T02:01:08+0200
type: post
url: /2007/09/11/openid-badidea.html
---

<p>When OpenID was made public, it seemed like a really good idea, solving the problem of having to maintain dozens or hundreds of user accounts all over the web.</p>
<p>But Stefan Brands has another point of view and he not only points out one or two flaws of the new identification and authentication protocol, but a whole list at a time:</p>
<ul>
<li>Security problems</li>
<li>Privacy problems</li>
<li>Trust problems</li>
<li>Usability problems</li>
<li>Adoption problems</li>
<li>Availability problems</li>
<li>Patent problems</li>
</ul>
<p>After reading his article "<a href="http://www.idcorner.org/?p=161">The problem(s) with OpenID</a>", I guess I'll go back to using <a href="http://keepassx.sourceforge.net/start/">Keepass</a>.</p>
