---
layout: post
title: "Vorratsdatenhaltung ist dumm"
microblog: false
guid: http://geewiz.micro.blog/2006/04/18/vorratsdatenhaltung-ist-dumm.html
date: 2006-04-19T01:19:43+0200
type: post
url: /2006/04/18/vorratsdatenhaltung-ist-dumm.html
---

<p>Wer sich fragt, warum ich hier eigentlich die ganze Zeit &uuml;ber <a href="http://blog.jochen-lillich.de/plugin/freetag/datenschutz">Datenschutz</a> labere, findet bei Jan Beilicke eine gute Erkl&auml;rung: in seinem Artikel <a href="http://jotbe-fx.de/daily/article/1570/UeberwachungsinstrumentVorratsdatenspeicherung">&Uuml;berwachungsinstrument Vorratsdatenspeicherung</a> begr&uuml;ndet er kurz und pr&auml;gnant, warum die Vorratsdatenspeicherung genau die verfehlt, zu deren Beobachtung sie angeblich n&ouml;tig ist, und demzufolge v&ouml;llig unsinnig die Freiheitsrechte aller B&uuml;rger verletzt.</p>
<p>(via <a href="http://blog.bauer-online.org/archives/102-Vorratsdatenspeicherung.html">Sebastian</a>)</p>
