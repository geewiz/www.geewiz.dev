---
title: "How I do remote administration using ngrok"
tags: [devops]
---

This month, a new intern from Spain started at my company. While we used to use 
Macbooks for our interns, he was the first to get a Linux laptop. Nothing fits 
the DevOps use case better, after all.

I set up the machine with Manjaro Linux in my office, but I had to solve the 
problem of later installing and upgrading software after the laptop was handed 
over. It'll always access the internet from a private network behind a router, 
and therefore will not be visible for me from the outside. And I certainly don't 
want to commute to Dublin, where we book a coworking space for our interns, 
every time maintenance is required.

In the end, I chose a minimalist approach by using `ngrok`. This nifty command 
line tool, paired with the online service at [ngrok.com](https://ngrok.com),
allows developers to create network tunnels with an endpoint on the public 
internet. Its main purpose is for web developers to share access to an 
application running on their personal machine. In other words, it allows people 
with whom you share the connection details to connect their web browser to your 
development laptop, even if it's inside a closed WiFi network. However, `ngrok` 
extends this functionality beyond the HTTP protocol and port 80.

When our intern asked me to install Discord and VS Code earlier today, I asked 
him to run a simple command and tell me the tunnel endpoint address. The command 
was:

```shell
ngrok tcp 22
```

It sets up the network tunnel as well as a local website on which it lists the 
details of each connection. Our intern then gave me the listed address 
`tcp://0.tcp.ngrok.io:14463`, from which I constructed an SSH command:

```shell
ssh -p 14463 geewiz@0.tcp.ngrok.io
```

Within a second, I was logged into his laptop. A few minutes of installing new 
packages and upgrading existing ones later, I was done and asked him to cut the 
connection. It doesn't get much simpler than that, does it?
