---
layout: post
title: "Getting out of the rat race"
microblog: false
guid: http://geewiz.micro.blog/2007/05/18/getting-out-of.html
date: 2007-05-18T22:06:00+0200
type: post
url: /2007/05/18/getting-out-of.html
---

<p>Alex, the Chief Happiness Officer, put a great <a href="http://positivesharing.com/2007/05/happiness-is-just-around-the-corner/">illustration of the rat race</a> we're always tempted to join in his blog. It was a revelation to me that I've been one of these rats during the recent weeks. Steadily getting more work, watching my to-do lists grow, trying to stay organized, failing in some cases, hurrying to correct those situations, losing focus on the important things and getting frustrated from missing the feeling of accomplishment. And all the while losing more and more energy, thus worsening this vicious circle. Now I know why they called that successful racing game "Burnout".</p>
<p>I decided to break the circle today. I wanted to concentrate on some topics that I should have finished long ago, without getting disturbed or distracted. That's why I decided to work from home today. Here I have everything I need -- a computer, a cup of tea and tranquility. The ideal environment to focus on the tasks at hand.</p>
<p>I started work half an hour early because I didn't have to commute and finished many tasks over the course of the morning that I tried to do all week in vain. But instead of feeling pressure and stress like on the days before, I feel more energized with every entry I'm able to cross out on my to-do list. I'm actually looking forward to the things I get to tackle after having some light lunch.</p>
<p>When you don't succeed, trying harder is not always the solution. Often it's trying something different.</p>
