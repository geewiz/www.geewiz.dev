---
layout: post
title: "Mehr Spa&szlig; an der Arbeit"
microblog: false
guid: http://geewiz.micro.blog/2006/02/26/mehr-spaszlig-an.html
date: 2006-02-26T21:48:36+0200
type: post
url: /2006/02/26/mehr-spaszlig-an.html
---

<p>Als junger Computerfreak habe ich davon getr&auml;umt, in einer Firma zu arbeiten, in der ich coole neue Technologie kennenlernen und anwenden kann. Beim Lesen von Ralfs Blogeintrag zum <a href="http://retro-park.de/blog/index.php?/archives/524-SunFire-T2000-Erster-Eindruck.html">Test der SunFire T2000</a> fiel mir auf, dass das inzwischen Realit&auml;t ist.</p>
<p><a href="http://cruisersblog.de/archives/413-Wochenendplanung.html">Martins Wochenendplanung</a> lie&szlig; mich jedoch erkennen, dass es in anderen Firmen andere, aber mindestens genauso spa&szlig;ige Sachen zu testen gibt.</p>
<p>Ich glaube, ich w&uuml;rde mich, h&auml;tte ich die Wahl, f&uuml;r Cruisers Variante entscheiden. Muss das Alter sein...</p>
