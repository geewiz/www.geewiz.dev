---
layout: post
title: "We’ve created democracies out of chaos before"
microblog: false
guid: http://geewiz.micro.blog/2019/12/03/weve-created-democracies.html
date: 2019-12-03T12:02:00+0200
type: post
url: /2019/12/03/weve-created-democracies.html
---
I'm surprised that an article like ["A Better Internet Is Waiting for Us"](https://www.nytimes.com/interactive/2019/11/30/opinion/social-media-future.html) can be as enlightened as it is and still not mention the term IndieWeb or reference an alternative social platform like [micro.blog](https://www.micro.blog).

PS: Is that CSS intro transition gorgeous or what?

PPS: I like to think that "You may not develop artificial or undesired entities for use in Photon Emission Products (PEPs)." is in fact advocating for ethical lasers.

