---
layout: post
title: "Orwell was an optimist"
microblog: false
guid: http://geewiz.micro.blog/2009/08/04/orwell-was-an.html
date: 2009-08-04T11:18:46+0200
type: post
url: /2009/08/04/orwell-was-an.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_long_quote"><p class="storycopy">The Children&rsquo;s Secretary set out &pound;400million plans to put 20,000 problem families under 24-hour CCTV super-vision in their own homes.</p>
<p class="storycopy">They will be monitored to ensure that children attend school, go to bed on time and eat proper meals.</p>
</blockquote>
<div class="posterous_quote_citation">via <a href="http://www.express.co.uk/posts/view/115736/Sin-bins-for-worst-families">express.co.uk</a></div></p>
</div>
