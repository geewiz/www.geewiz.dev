---
layout: post
title: "FLOSS Weekly"
microblog: false
guid: http://geewiz.micro.blog/2006/08/14/floss-weekly.html
date: 2006-08-14T05:21:57+0200
type: post
url: /2006/08/14/floss-weekly.html
---

<p><a href="http://www.twit.tv/FLOSS">We're not talking dentistry here; FLOSS is all about Free Libre Open Source Software." "FLOSS Weekly</a> is a podcast hosted by Leo Laporte and Chris di Bona, where they interview well-known people from the Free Software scene.</p>
<p>Today, I sat in St. Stephen's Green[1], listening to the <a href="http://www.twit.tv/floss12">latest episode of FLOSS Weekly</a> with Rasmus Lerdorf of PHP fame as a guest. They talk about the genesis of PHP (back then named PHP/FI), about security flaws and their impact on PHP's reputation, and about Rasmus' favourite PHP applications, which include <a href="http://www.s9y.org">Serendipity</a>, the software used for this blog.</p>
<p>Another interesting episode I liked was (of course) <a href="http://www.twit.tv/floss/randall_schwartz">FLOSS Weekly episode #9</a> with Perl guru Randall Schwartz. He talks about the early days of Perl, Web Application Frameworks like <a href="http://www.catalystframework.org">Catalyst</a>, his famous conference parties and about big websites made with Perl, including IMDB, Amazon and Ticketmaster. Another hot topic is the upcoming Perl version 6.</p>
<p>Leo and Chris are great hosts with a solid background in Open Source Software and their interviews are as interesting as entertaining. Get FLOSS Weekly into your podcatcher!</p>
<p>fn1. ...forgetting the time and getting late for my date with Carolin. I'm sorry, darling!</p>
