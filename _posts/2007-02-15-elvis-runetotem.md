---
layout: post
title: "Elvis Runetotem"
microblog: false
guid: http://geewiz.micro.blog/2007/02/15/elvis-runetotem.html
date: 2007-02-16T00:59:06+0200
type: post
images:
- /uploads/taurenelvis.jpg
photos:
- /uploads/taurenelvis.jpg
url: /2007/02/15/elvis-runetotem.html
---

<p><a href="http://blog.helaron.de">Armin</a> sent me this email:</p>
<blockquote class="posterous_short_quote">
<p>You're not alone! I could touch him -- Elvis lives... standing in some little Tauren bar singing..."</p>
</blockquote>
<p>
<div class='p_embed p_image_embed'>
<img alt="" src="/uploads/taurenelvis.jpg" />
</div></p>
