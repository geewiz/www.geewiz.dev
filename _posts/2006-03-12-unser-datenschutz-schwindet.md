---
layout: post
title: "Unser Datenschutz schwindet"
microblog: false
guid: http://geewiz.micro.blog/2006/03/12/unser-datenschutz-schwindet.html
date: 2006-03-12T23:39:27+0200
type: post
url: /2006/03/12/unser-datenschutz-schwindet.html
---

<p>Anl&auml;sslich des Datenschutz-Kongresses am 16. und 17. M&auml;rz weisen Datensch&uuml;tzer erneut darauf hin, dass der Schutz unserer Daten vor Missbrauch schleichend immer geringer wird. B&uuml;rgerrechte w&uuml;rden mehr und mehr zu Gunsten der vermeintlichen Sicherheit des Staates abgebaut und beschnitten, erkl&auml;rte der Vorsitzende des Bundesverbands der Datensch&uuml;tzer (BvD), Hannes Federrath. (Mehr dazu im Artikel <a href="http://www.heise.de/newsticker/meldung/70728">Experte: Schleichender Verlust an Datenschutz</a> bei Heise Online.)</p>
<p>Wenn B&uuml;rger und Politiker nicht zunehmend darauf dr&auml;ngen, dass pers&ouml;nliche Daten so sorgf&auml;ltig und vorsichtig zu behandeln sind wie die pers&ouml;nlichen Rechte, mit denen sie zusammenh&auml;ngen, dann werden wir bald gar nicht mehr wissen, wer was von uns wei&szlig;.</p>
<p>Ja, ich will mich sicher f&uuml;hlen vor Terroristen. Aber ich will mich auch sicher f&uuml;hlen vor dem Staat.</p>
