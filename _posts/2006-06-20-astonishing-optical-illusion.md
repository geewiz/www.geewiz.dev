---
layout: post
title: "Astonishing optical illusion"
microblog: false
guid: http://geewiz.micro.blog/2006/06/20/astonishing-optical-illusion.html
date: 2006-06-20T18:15:16+0200
type: post
url: /2006/06/20/astonishing-optical-illusion.html
---

<p>There are a lot of optical illusions published on the net. Everybody knows the "which line is longer?" and the "are those lines parallel?" pictures. Yawn.</p>
<p>But the <a href="http://www.johnsadowski.com/big_spanish_castle.html">Spanish Castle</a> is one I find really amazing. It's a picture of a castle in false colours with a black dot in the middle. Stare at the dot for about 30 seconds, then move your mouse over the picture. You'll see the castle in real colours -- but only as long as you don't move your eyes!</p>
<p>This reminds me of the "Magic Eye" pictures popular a few (gosh, I'm getting old) years ago. When I got the first Magic Eye book, I spent hours staring at the patterns trying to make the 3D shape appear...</p>
