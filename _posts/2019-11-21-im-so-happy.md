---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2019/11/21/im-so-happy.html
date: 2019-11-21T12:08:54+0200
type: post
images:
- https://www.geewiz.dev/assets/img/3f3dc70981.jpg
photos:
- https://www.geewiz.dev/assets/img/3f3dc70981.jpg
url: /2019/11/21/im-so-happy.html
---
I’m so happy to see how open source conferences are getting more and more inclusive. Good job, #RubyConf!
![EBF83775-C45E-4343-9C00-50A0039C6A59.jpg](https://www.geewiz.dev/assets/img/3f3dc70981.jpg)
