---
layout: post
title: "Pegamento 1.0 Beta-5"
microblog: false
guid: http://geewiz.micro.blog/2004/12/14/pegamento-beta.html
date: 2004-12-14T03:47:00+0200
type: post
url: /2004/12/14/pegamento-beta.html
---

<p>Wie <a href="http://www.nekochan.net/weblog/archives/000232.html">Nekochan</a> meldet, wurde eine neue, kostenfreie Beta-Version der Image Compositing Software <a href="http://webs.ono.com/usr040/pegamentobeta/pegamento.html">Pegamento</a> f&uuml;r IRIX ver&ouml;ffentlicht.</p>
<p>Bei Pegamento handelt es sich um eine Software zum Zusammenf&uuml;gen von Bildern (daher auch der spanische Name, der auf Deutsch "Kleber" bedeutet). Dabei nutzt sie intensiv Funktionen der Grafik-Hardware.</p>
