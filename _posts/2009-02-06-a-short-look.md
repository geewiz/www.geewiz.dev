---
layout: post
title: "A short look into LiquidWeb's data center"
microblog: false
guid: http://geewiz.micro.blog/2009/02/06/a-short-look.html
date: 2009-02-06T02:50:39+0200
type: post
url: /2009/02/06/a-short-look.html
---

<p>Hosting provider Liquid Web (Michigan, USA) has been building a new data center to provide 50,000 square feet of data center space and additional office space. Infrastructure director Chris Strandt and marketing director Travis Stoliker give a short look into the facilities:</p>
<p>[youtube=http://www.youtube.com/watch?v=mwHzK6xxQ3I&amp;hl=de&amp;fs=1]</p>
<p>Nothing shocking when you're used to our data center. I'm curious if my employer will let us do a short walk-through when we get a new one somedays.</p>
<p>(via <a href="http://www.datacenterknowledge.com/archives/2009/01/27/liquid-web-data-center-nears-completion/">Data Center Knowledge</a>)</p>
