---
layout: post
title: "Viele &Auml;ngste um Privatsph&auml;re sind begr&uuml;ndet"
microblog: false
guid: http://geewiz.micro.blog/2006/06/12/viele-aumlngste-um.html
date: 2006-06-12T06:21:39+0200
type: post
url: /2006/06/12/viele-aumlngste-um.html
---

<p><a href="http://blog.bauer-online.org/archives/144-Web2.0-Das-Ende-der-Privatsphaere.html">Wieviel Privatsph&auml;re ist eigentlich notwendig? Haben wir etwas zu verbergen?</a>, fragt sich Augusto. Auch bei ihm macht sich Skepsis breit, ob der Preis f&uuml;r die neuen, n&uuml;tzlichen Anwendungen des Web 2.0 nicht der Verlust der Anonymit&auml;t und Privatsp&auml;re im Internet sein wird[1].</p>
<p>Martin R&ouml;ll hingegen ist der Meinung, <a href="http://www.roell.net/weblog/archiv/2006/06/03/die_meisten_privacyaengste_sind_unbegruendet.shtml.">die meisten Privacy-&Auml;ngste sind unbegr&uuml;ndet</a> Schlie&szlig;lich interessiere es wirklich niemanden, ob Lieschen M&uuml;ller gerade aus Italien blogt.</p>
<p>Ich bin mir (zusammen mit vielen Kommentatoren von Martins Artikel) da nicht so sicher. Warum lie&szlig; Lieschen M&uuml;ller vor der Abreise die Wohnzimmerlampe an? Weil es manche Leute doch interessiert, ob sie zuhause ist.</p>
<p>Dass sich f&uuml;r unsere im Web verteilten Daten aber auch Parteien interessieren, die das Recht auf ihrer Seite haben, erkl&auml;rt Bernie Goldbach in seinem Blogeintrag <a href="http://irish.typepad.com/irisheyes/2006/06/your_data_from_.html.">Your data from the cradle to the grave</a> Und sei es nur, weil manche Regierungen gern das Recht nach den Interessen dieser Parteien, zum Beispiel der Musikindustrie, gestalten. Das belegt Bernie mit einem <a href="http://irish.typepad.com/irisheyes/2006/06/serial_leaker_k.html">Zitat</a> aus der New York Times:</p>
<blockquote class="posterous_medium_quote">
<p>At the meeting with privacy experts yesterday, Justice Department officials focused on wanting to retain the records for use in child pornography and terrorism investigations. But they also talked of their value in investigating other crimes like intellectual property theft and fraud, said Marc Rotenberg, executive director of the Electronic Privacy Information Center in Washington, who attended the session.</p>
</blockquote>
<p>Und nat&uuml;rlich werden die Unternehmen, denen ich meine Daten anvertraue, die ersten sein, die sie nutzen. Ich stelle heute schon fest, dass Amazon mich recht geschickt auf "interessante" Angebote st&ouml;&szlig;t. Wie wird das werden, wenn &uuml;berall Hinweise auf meine Vorlieben und Gewohnheiten gespeichert sind -- und diese gar durch Data Mining miteinander verkn&uuml;pft werden? Unternehmen, die mit Werbung Geld verdienen, haben nat&uuml;rlich ein Interesse daran, diese optimal zu pr&auml;sentieren. Ich arbeite selbst t&auml;glich mit Technologie, die genau dieses Ziel verfolgt. </p>
<p>Deshalb werde ich eher vorsichtig sein, denn meiner Meinung nach sind die &Auml;ngste vor dem gl&auml;sernen Surfer keineswegs unbegr&uuml;ndet.</p>
<p>fn1. Wobei mir nicht ganz klar ist, wieso das durch einen Slashdot-Artikel ausgel&ouml;st wurde. Schreib ich Polnisch? ;-)</p>
