---
layout: post
title: "W&auml;r ich doch ne Bank"
microblog: false
guid: http://geewiz.micro.blog/2006/03/27/waumlr-ich-doch.html
date: 2006-03-27T19:59:25+0200
type: post
url: /2006/03/27/waumlr-ich-doch.html
---

<p>Ich muss bei meiner Bank einen V-Scheck einreichen und eine Formalit&auml;t unterschreiben. Als ich am Freitag gegen halb f&uuml;nf Feierabend machte, schaute ich noch schnell bei der Sparkasse vorbei. Dumm gelaufen: 16 Uhr ist dort Feierabend. Okay, dann Montag fr&uuml;h. Kurz nach 8 kam ich heute morgen an -- und stand wieder vor verschlossenen T&uuml;ren. &Ouml;ffnungszeit ist 8:30. Und in meiner Mittagspause macht der Laden nat&uuml;rlich auch zu.</p>
<p>Aber irgendwie beruhigt es mich, dass ich mir ein wenig dieser kindlichen Naivit&auml;t bewahrt habe, anzunehmen, Dienstleistungsunternehmen w&uuml;rden sich nach den Bed&uuml;rfnissen ihrer Kunden richten.</p>
