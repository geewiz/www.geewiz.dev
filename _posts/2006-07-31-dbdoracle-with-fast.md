---
layout: post
title: "DBD::Oracle with fast arrays"
microblog: false
guid: http://geewiz.micro.blog/2006/07/31/dbdoracle-with-fast.html
date: 2006-07-31T07:09:56+0200
type: post
url: /2006/07/31/dbdoracle-with-fast.html
---

<p>The Oracle DBMS has powerful features for handling big arrays of data. With <a href="http://www.nntp.perl.org/group/perl.dbi.dev/4606">release 1.18</a>, DBD::Oracle makes these features accessible from Perl DBI:</p>
<blockquote class="posterous_medium_quote">
<p>With this release DBD::Oracle finally implements Oracle's native Array Interface.  You will see very dramatic increase in speed. For example; the time for a 2 million plus insert query dropped from well over an hour to less than 10 minutes when using execute_array() and the new code.</p>
</blockquote>
<p>For documentation and downloads, go to <a href="http://search.cpan.org/">CPAN</a>~pythian/DBD-Oracle-1.18a/, as usual.</p>
