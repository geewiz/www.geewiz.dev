---
layout: post
title: "Die Todesmelone"
microblog: false
guid: http://geewiz.micro.blog/2005/05/12/die-todesmelone.html
date: 2005-05-12T02:29:02+0200
type: post
url: /2005/05/12/die-todesmelone.html
---

<p>Die Organic Trade Association nutzt die Gunst der Stunde, um f&uuml;r Bio-Produkte zu werben. Und wer rechnet schon damit, dass jemand <a href="http://www.storewars.org/flash/index.html">Krieg der Sterne" mit Gem&uuml;se nachspielt? Lacht selbst &uuml;ber "Grocery Store Wars</a>!</p>
<p>(via <a href="http://www.shopblogger.de/blog/archives/1021-Store-Wars.html">Shopblogger</a>)</p>
