---
layout: post
title: "Chainsaw Maid"
microblog: false
guid: http://geewiz.micro.blog/2008/07/13/chainsaw-maid.html
date: 2008-07-13T23:47:21+0200
type: post
url: /2008/07/13/chainsaw-maid.html
---

<p>This is especially for Kai: A claymation movie about a zombie-splattering maid!</p>
<p>(<strong>Warning:</strong> Not for the faint of heart.)</p>
<p>[youtube=http://www.youtube.com/watch?v=6d-tNXxTRBA&amp;hl=en&amp;fs=1]</p>
<p>(Via <a href="http://www.nerdcore.de/wp/2008/07/01/knetmannchenzombies-chainsaw-maid/">Nerdcore</a>)</p>
