---
layout: post
title: "Twitter starts OAuth beta test"
microblog: false
guid: http://geewiz.micro.blog/2009/02/24/twitter-starts-oauth.html
date: 2009-02-24T23:30:59+0200
type: post
url: /2009/02/24/twitter-starts-oauth.html
---

<p>For a long time now, many websites providing a service to Twitter users had to use those users' credentials to get access to their profile, messages or to post tweets in their name. For example, I've given my credentials to <a href="http://twitpic.com/">TwitPic</a>, <a href="http://www.twitterfeed.com/">Twitterfeed</a>, <a href="http://www.rememberthemilk.com/">Remember the Milk</a> and <a href="http://skitch.com/">Skitch</a>.</p>
<p>Many people are quick to give away their username and password -- and as many learn the hard way why that's a bad idea. When Twply, a service emailing your "@name" replies, first promised "Your password is safe with us. No worries." and then sold on eBay for $1200 after one day, a lot of people that had given up their username and password there were left wondering what the new owner would do with those credentials.</p>
<p>In software development, the underlying structures of best practices are called "patterns". Using one's username and password on a service to get access to another service has many bad implications and is therefore called an "antipattern", a practice that should be discouraged.</p>
<p>Finally now, the Twitter crew has done its homework and is <a href="http://twitter.com/oauth_clients">testing OAuth</a>, a protocol to give one service access to another service in your name without revealing your password. </p>
<p>I'm sure that this move will make even more Twitter support services appear, and now you don't have to do a multi-day due diligence period until you gather the courage to enter your credentials outside of Twitter any more.</p>
