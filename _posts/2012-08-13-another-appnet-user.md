---
layout: post
title: "Another App.net user"
microblog: false
guid: http://geewiz.micro.blog/2012/08/13/another-appnet-user.html
date: 2012-08-13T18:23:44+0200
type: post
url: /2012/08/13/another-appnet-user.html
---
<p>I was late to the game when yesterday, I made the decision to back App.net. So I&#8217;m now, like <a href="http://www.insideview.ie/irisheyes/2012/08/joined-the-appnet-cork-posse-.html">Bernie</a> put it, part of the App.net posse. My temporary user page is at <a href="https://alpha.app.net/geewiz">https://alpha.app.net/geewiz</a>.</p>
<p>I&#8217;ve put my $50 into the tip jar because I&#8217;m curious if it&#8217;s really possible to maintain a stable microcommunication platform in competition to Twitter. Back when identi.ca tried this, it didn&#8217;t gain strong adoption and Diaspora obviously took its name as a self-fulfilling prophecy.</p>
<p>There are people who object that, as James argues in a comment on Bernie&#8217;s post, &#8220;we don&#8217;t need another (centralized) network&#8221;. Well, we had all that already when they launched identi.ca aka StatusNet &#8211; open, decentral, federated. But from my perspective, it&#8217;s not the technology that matters first, it&#8217;s adoption. I&#8217;m optimistic that App.net will add federation later after they transform the healthy adoption that gave them more than $500,000 in start money to a stable user base.</p>
<p>App.net could be the right thing at the right time, as Twitter continues to alienate its user and third-party developer base. And they have something to show, only a few weeks after first announcing the project. The alpha version of the website works well (granted, with minimal functionality) and their <a href="https://github.com/appdotnet/api-spec/wiki/Directory-of-third-party-devs-and-apps">developer ecosystem</a> is bustling. I also like the transparency the App.net team strives for, for example by using Github for maintaining not only their software but also their TOS.</p>
<p>I&#8217;m looking forward to what my fellow App.net users and I will make of the platform. For a start, we&#8217;ve already put our money where our mouth is.</p>
