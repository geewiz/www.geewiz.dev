---
layout: post
title: "Ireland, here we come!"
microblog: false
guid: http://geewiz.micro.blog/2013/08/03/ireland-here-we.html
date: 2013-08-03T12:04:00+0200
type: post
url: /2013/08/03/ireland-here-we.html
---
<p>How exciting! Later today, we'll board our Air Lingus flight to Dublin. With one-way tickets. Besides our children, we're bringing with us just two big backpacks and a bit of carry-on with the most essential things. Everything else waits packed in boxes at our parents for the day when we have our own Irish address.</p>
<p>Until we find a new home, we'll be staying at the Dublin International Hostel. We'll see how long it will take us to find a decent house in Bray. I'm a bit nervous about that particular task, but since all of the Irish people I've talked to were quite optimistic, I'm confident as well. And, honestly, a bit of uncertainty is part of the adventure!</p>
<p>For up-to-date news on our journey, follow me on this blog, on <a href="https://foursquare.com/geewiz">FourSquare</a> and on <a href="https://www.facebook.com/jochen.lillich">Facebook</a>!</p>
