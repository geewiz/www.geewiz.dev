---
layout: post
title: "Guerilla marketing by IKEA"
microblog: false
guid: http://geewiz.micro.blog/2010/01/18/guerilla-marketing-by.html
date: 2010-01-18T10:34:13+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpstaticflick_jhkzb-scaled500.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpstaticflick_jhkzb-scaled500.jpg?w=300
url: /2010/01/18/guerilla-marketing-by.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<img alt="Media_httpstaticflick_jhkzb" height="333" src="/assets/wp-content/2011/05/media_httpstaticflick_jhkzb-scaled500.jpg?w=300" width="500" />
</div>
<div class="posterous_quote_citation">via <a href="http://marketallica.wordpress.com/2006/05/21/outstanding-ads-of-ikea/">marketallica.wordpress.com</a></div></p>
</div>
