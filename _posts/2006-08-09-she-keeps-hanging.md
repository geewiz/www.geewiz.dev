---
layout: post
title: "She keeps hanging on"
microblog: false
guid: http://geewiz.micro.blog/2006/08/09/she-keeps-hanging.html
date: 2006-08-09T07:15:03+0200
type: post
url: /2006/08/09/she-keeps-hanging.html
---

<p>When I was a teen, I had posters of her on my walls as well as all of her LPs in my collection: Kim Wilde, the  blonde singer from Britain I was a fan of for her looks and her Synclavier-backed songs[1]. During the early nineties, her success faded and she began focusing on her private life.</p>
<p>In 2003, she had a comeback with a duet she did with german singer Nena and her song "Anywhere, anyplace, anytime". </p>
<p>And now, sitting in the McDonald's restaurant in Karlsruhe, I hear a new remix of the Kim Wilde hit "Keep me hanging on". I think it's being released as a promo song for the casting show "Popstars".</p>
<p>She may be far from being a Madonna, but I'm happy that she's still in the music business, reminding me of times when "Love blonde" or "The Second Time" were the soundtrack of my life[2].</p>
<p>fn1. Samantha Fox was another british export I liked, but for two other reasons.</p>
<p>fn2. <em>Update:</em> I listened to "The Second Time" song this morning. "I've every reason to believe there's still a man in you -- you done it once so come on go again"? It seems it actually took me 20 years to get what she's singing about...</p>
