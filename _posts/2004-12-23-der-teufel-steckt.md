---
layout: post
title: "Der Teufel steckt im Detail"
microblog: false
guid: http://geewiz.micro.blog/2004/12/23/der-teufel-steckt.html
date: 2004-12-23T08:37:00+0200
type: post
url: /2004/12/23/der-teufel-steckt.html
---

<p>Wo der Philippsburger Stadtanzeiger eine harmlose Meldung zu ver&ouml;ffentlichen glaubte, findet das Rock Hard 12/2004 eine Botschaft zwischen den Zeilen:</p>
<blockquote class="posterous_short_quote">
<p>666 Kinder in 16 Jahren getauft: Pfarrer Anton Killer feierlich in den Ruhestand verabschiedet</p>
</blockquote>
<p>Beim Fachblatt f&uuml;r Stromgitarrenmusik schloss man: "Der Deibel treibt mal wieder seine kleinen Sp&auml;ssken mit dem Fu&szlig;volk seines alten Schachpartners".</p>
