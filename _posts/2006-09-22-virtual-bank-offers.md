---
layout: post
title: "Virtual bank offers virtual safety deposit box"
microblog: false
guid: http://geewiz.micro.blog/2006/09/22/virtual-bank-offers.html
date: 2006-09-22T22:16:58+0200
type: post
url: /2006/09/22/virtual-bank-offers.html
---

<p>According to the article <a href="http://www.techcrunch.com/2006/09/22/exclusive-mysterious-paypal-secure-storage/">Mysterious Paypal Secure Storage</a> on TechCrunch, Paypal is in the process of building a service that allows customers to safely transfer and store files on the payment provider's servers. So far, though, it's unclear to whom the service will be offered -- only to merchants or to all the users.</p>
<p>What's remarkable is that files will be transferred via SFTP(Secure File Transfer Protocol), a protocol offered by the SSH(Secure Shell) software suite, even though Windows has no default SFTP application (of course there are offers from 3rd party vendors). SSH and SFTP are widely used on Unix systems where they are available as command line applications. As open source software, the SSH suite offers mature tools for secure data transfer.</p>
<p>It's an interesting move for a payment provider to offer such a service, but maybe it'll be just the basis for future fulfilment services.</p>
