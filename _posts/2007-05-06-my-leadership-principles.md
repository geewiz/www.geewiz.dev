---
layout: post
title: "My leadership principles"
microblog: false
guid: http://geewiz.micro.blog/2007/05/06/my-leadership-principles.html
date: 2007-05-06T02:00:00+0200
type: post
url: /2007/05/06/my-leadership-principles.html
---

<p>Over the years, I've collected several principles that I find important for my management work. I consider myself a successful leader and my success is based in part on those principles. I tried to write down every recurring theme that you can find in my thinking and acting as a manager. This list is the result.</p>
<p>h2. I lead by example.</p>
<ul>
<li>Working and being happy aren't mutually exclusive.</li>
<li>My decisions are transparent and based on the situation, not only on rules.</li>
<li>I am loyal to my staff and to the company (in that order).</li>
<li>I only commit to what I can deliver.</li>
<li>I adhere to commitments. If I see a problem doing so, I'll tell in time.</li>
<li>I practise and expect honesty, timeliness, diligence and creativity.</li>
</ul>
<p>h2. I depend on my team.</p>
<ul>
<li>I trust that my directs have both the qualification and determination to create the best solutions.</li>
<li>I need my team to help me fulfil my responsibilities, so I'll do everything to help them kick ass.</li>
<li>Every team member can claim some my time immediately.</li>
<li>If there's a way of improving our communication, I'll give it a try.</li>
<li>Everyone makes mistakes. The team will straighten them out. (So try not to repeat them.)</li>
<li>I give and accept feedback on a regular schedule.</li>
</ul>
<p>h2. I use my resources efficiently.</p>
<ul>
<li>I do things consequently or not at all.</li>
<li>I join meetings in time. If other people don't, I'll feel free to leave for more important things.</li>
<li>I won't listen to problem descriptions that don't come with a suggested solution.</li>
</ul>
<p>h2. I set goals for myself and my staff.</p>
<ul>
<li>Everyone in the team knows my goals.</li>
<li>I stipulate clear goals with every team member.</li>
<li>I have a development plan for myself and every team member.</li>
<li>It is up to the employees to reach their development milestones, not mine.</li>
</ul>
<p>I'm certainly not perfect in adhering to all of those principles all the time. But I take them very seriously. If I find something missing from the list, I'll update it.</p>
<p>And I'm interested in your thoughts -- what are the secrets of your or your bosses success?</p>
