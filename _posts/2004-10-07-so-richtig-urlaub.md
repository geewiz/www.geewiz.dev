---
layout: post
title: "So richtig Urlaub"
microblog: false
guid: http://geewiz.micro.blog/2004/10/07/so-richtig-urlaub.html
date: 2004-10-07T08:38:00+0200
type: post
url: /2004/10/07/so-richtig-urlaub.html
---

<p>Jetzt bin ich seit 5 Tagen im <a href="http://www.bahia-principe.es/aleman/tenerife_descripcion.html">Hotel Bahia Principe</a> auf Teneriffa und fuehle mich zunehmend erholt.</p>
<p>Am Samstag kamen mein Bruder und meine Freunde Kai und Heike hier an und geniessen seither das geniale Wetter. Knapp 30 Grad bei leichtem Wind sind problemlos zu ertragen.</p>
<p>Das Hotel ist rundum zu empfehlen. Die Zimmer sind wohnlich (wir erhielten sogar Junior Suites, weil die Doppelzimmer aus sind), die Anlage ist gross, sodass sich die Leute gut verteilen. Das Essen ist wahnsinnig reichhaltig und abwechslungsreich und die Hotelangestellten fast durch die Bank total freundlich. Als All Inclusive Gaeste koennen wir natuerlich aus dem Vollen schoepfen, was Fruehstueck, Mittagessen, Abendessen und Poolbar betrifft. Das Animationsprogramm ist ebenfalls abwechslungsreich, und die Animadores weisen die Gaeste nur freundlich darauf hin, ohne Druck auszuueben. Alles sehr angenehm.</p>
<p>Morgen werden wir uns mal aus der Anlage raus trauen und mit einem Mietwagen den Loro Park, einen grossen Tierpark, besuchen.</p>
<p>Sieht so aus, als haetten wir mit der Auswahl unseres Urlaubsortes voll ins Schwarze getroffen. Ich muss nur darauf achten, dem guten Essen auch ausreichend Bewegung entgegenzusetzen. :)</p>
