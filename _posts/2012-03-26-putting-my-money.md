---
layout: post
title: "Putting my money where my mouth is"
microblog: false
guid: http://geewiz.micro.blog/2012/03/26/putting-my-money.html
date: 2012-03-26T03:08:55+0200
type: post
images:
- http://farm1.staticflickr.com/2/2418695_3600b4cab5_n.jpg
photos:
- http://farm1.staticflickr.com/2/2418695_3600b4cab5_n.jpg
url: /2012/03/26/putting-my-money.html
---
<p><a href="http://www.flickr.com/photos/emdot/2418695/" title="day in the life: lunch money by emdot, on Flickr"><img src="http://farm1.staticflickr.com/2/2418695_3600b4cab5_n.jpg" width="320" height="179" alt="day in the life: lunch money"></a></p>
<p>The <a href="http://www.jochen-lillich.de/iron-blogger-freiburg/">Iron Blogger Freiburg</a> initiative made me realize how much an incentive the threat of losing money can be. Basically, it&rsquo;s a bet against myself that I can write at least one blog post a week. Which I do at this precise moment, once again only a few hours before the deadline.</p>
<p>So, recently I had the idea of betting against myself in another area I&rsquo;ve made much less progress than I&rsquo;d like to admit: losing weight. Of course, I know all the benefits of not being overweight by heart and I appreciate them. So I tried time and time again, but eventually I always lost my discipline and gained back the little I had been able to shed.</p>
<p>This changed a few weeks ago when I decided to literally put my money where my mouth is: Yes, I want to lose at least 10kg, and yes, I&rsquo;m going to pay cash if I fail. It works like this: I set a new weight goal at the beginning of the month and for every kilo I&rsquo;m off at its end, I pay 20&euro; to my partner. Since the difference is always rounded up to the next full kilo, missing my goal by only 100g means handing over 20&euro;.</p>
<p>Lo and behold: Suddenly, I&rsquo;m steadily losing weight (4kg so far)! Between losing money and missing a few sugary pleasures, I choose the latter. And since I&rsquo;m setting realistic goals (2kg per month at the moment), I still have fun eating.</p>
<p>Looks like if the motivational carrot just isn&rsquo;t enough, I need to find the right stick.</p>
