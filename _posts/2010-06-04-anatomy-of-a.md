---
layout: post
title: "Anatomy of a Stick Figure"
microblog: false
guid: http://geewiz.micro.blog/2010/06/04/anatomy-of-a.html
date: 2010-06-04T19:55:53+0200
type: post
url: /2010/06/04/anatomy-of-a.html
---

<div class="posterous_bookmarklet_entry">
      <a href="http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=anatomyofastickfigure-100422123121-phpapp02&#038;stripped_title=anatomy-ofa-stickfigure">http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=anatomyofastickfigure-100422123121-phpapp02&#038;stripped_title=anatomy-ofa-stickfigure</a></p>
<div class="posterous_quote_citation">via <a href="http://www.slideshare.net/betsystreeter/anatomy-ofa-stickfigure">slideshare.net</a></div>
<p>Nice tutorial for drawing stick figures.</p>
</div>
