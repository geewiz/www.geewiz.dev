---
layout: post
title: "The Elder Ones Recommend Drupal"
microblog: false
guid: http://geewiz.micro.blog/2010/03/04/the-elder-ones.html
date: 2010-03-04T18:57:44+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwdrupalor_amjfo-scaled1000.png?w=211
photos:
url: /2010/03/04/the-elder-ones.html
---

<div class="posterous_bookmarklet_entry">
<div class='p_embed p_image_embed'>
<a href="/assets/wp-content/2011/05/media_httpwwwdrupalor_amjfo-scaled1000.png"><img alt="Media_httpwwwdrupalor_amjfo" height="709" src="/assets/wp-content/2011/05/media_httpwwwdrupalor_amjfo-scaled1000.png?w=211" width="500" /></a>
</div>
<div class="posterous_quote_citation">via <a href="http://www.drupal.org.uk/elder-ones-recommend-drupal">drupal.org.uk</a></div>
<p>HT Markus Heurung (<a href="http://byzero.de">http://byzero.de</a>)</p>
</div>
