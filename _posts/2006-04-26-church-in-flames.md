---
layout: post
title: "Church in flames"
microblog: false
guid: http://geewiz.micro.blog/2006/04/26/church-in-flames.html
date: 2006-04-26T20:07:09+0200
type: post
url: /2006/04/26/church-in-flames.html
---

<p>Die MTV-Zeichentrickserie "Popetown" schl&auml;gt schon riesige Wellen, bevor sie &uuml;berhaupt angelaufen ist. Schon ihr Trailer f&uuml;hrte zu heller Emp&ouml;rung in Kirchenleitung, bei frommen Katholiken und Politikern. Von Protesten in Weblogs und Presseverlautbarungen bis hin zu juristischen Schritten zieht man alle Register der medialen Stalinorgel.</p>
<p>Gerade nach den Mohammed-Karikaturen kann ich diese ungez&uuml;gelte Emp&ouml;rung nicht nachvollziehen.<br />
Die Kirche ist immer wieder Ziel humoristischer Angriffe, die von intelligenter Satire bis zu d&uuml;mmlicher Verunglimpfung rangieren. Und das ist auch gut so, denn wenn sie von niemandem mehr kritisiert oder auf die Schippe genommen wird, ist das ein untr&uuml;gliches Zeichen daf&uuml;r, dass sie ihre Bedeutung f&uuml;r die Gesellschaft komplett verloren hat.</p>
<p>Nach vielen Jahren elektronischer Mail weiss ich inzwischen gut, auf welche Anfeindungen ich eingehe und welche ich ignoriere -- auch wenn sie mich erst mal rasend vor Wut machen. Wer sich seiner Sache sicher ist, sollte zu einem gewissen Ma&szlig; &uuml;ber den Dingen stehen.</p>
<p>Und das gilt insbesondere f&uuml;r eine Serie wie "Popetown", die anscheinend mit Kirche und Glauben gerade noch so viel zu tun hat, dass sie christliche Symbole und kirchliche &Auml;mter aufgreift und ins Absurde verzerrt.</p>
<p>Ein weiser Mann sagte einst: "Erst ignorieren sie euch, dann lachen sie &uuml;ber euch, dann bek&auml;mpfen sie euch, und dann siegt ihr." Dann lasst sie doch lachen!</p>
<p>Ich lache sogar oft genug gerne mit. Von Walter Moers' "Die Klerikalen" habe ich seinerzeit einen ganzen Satz gekauft, um sie bei netten Gelegenheiten verschenken zu k&ouml;nnen.</p>
<p>So kann ich mich nur <a href="http://blog.handelsblatt.de/indiskretion/eintrag.php?id=689">Tobias Kn&uuml;wers Ansicht</a> anschlie&szlig;en und wie er auf den <a href="http://www.faz.net/s/Rub117C535CDF414415BB243B181B8B60AE/Doc">Hintergrund-Artikel der FAZ</a>~E087BADF833B74EE4AA371DADA337B80F~ATpl~Ecommon~Scontent.html verweisen.</p>
