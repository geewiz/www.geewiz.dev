---
layout: post
title: "Watch the world tweeting"
microblog: false
guid: http://geewiz.micro.blog/2007/03/19/watch-the-world.html
date: 2007-03-19T03:37:21+0200
type: post
url: /2007/03/19/watch-the-world.html
---

<p>David Troy is a one-man Twitter mashup factory.</p>
<p>First, he built <a href="http://twittermap.com">TwitterMap</a>, a mashup with Google Maps. People that answer Twitter's question <a href="http://www.plazes.com">What are you doing?" can now also answer the question "Where are you doing it?". You just have to insert a "L:" followed by a place description that Google Maps understands into your tweet. With TwitterMap, you can now easily find twitterati in your neighborhood. For me, that makes "Plazes</a> obsolete, because with Twitter, I'm able to not only see who's near me but also to get in contact with them immediately.</p>
<p>A few minutes ago now, Dave published his newest work: <a href="http://twittervision.com/.">TwitterVision</a> It's a mashup with Google Maps, too. But its purpose is to visualize in real-time what's tweeting. On TwitterVision, every time someone uses Twitter, a bubble pops up at his location displaying his message, together with his icon and Twitter name. And it's simply amazing to watch Twitter in realtime.</p>
<p>Now all I need is a 60" screen to build my own Twitter command center!</p>
<p>TwitterVision: Watch the world communicate. Watch your productivity go down the drain. ;-)</p>
