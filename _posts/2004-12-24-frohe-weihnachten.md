---
layout: post
title: "Frohe Weihnachten!"
microblog: false
guid: http://geewiz.micro.blog/2004/12/24/frohe-weihnachten.html
date: 2004-12-24T09:36:00+0200
type: post
url: /2004/12/24/frohe-weihnachten.html
---

<p>Wie immer f&uuml;r alle &uuml;berraschend ist auch dieses Weihnachtsfest auf den 24. Dezember gefallen. Ich hoffe, ihr seid inzwischen vorbereitet. (Was immer das heisst.)</p>
<p>Ich habe inzwischen alle Geschenke vorbereitet, und dank neuer Winterreifen kann die wei&szlig;e Weihnacht jetzt kommen. (Hey, man darf doch tr&auml;umen.)</p>
<p>Den Heiligen Abend werde ich bei Carolin in Freiburg verbringen. Wir werden es uns gem&uuml;tlich machen und das Fest genie&szlig;en. Am Weihnachtsmorgen fahren wir zur&uuml;ck, um mit meiner Familie essen zu gehen. (Essen: die T&auml;tigkeit, an der man Weihnachten erkennt.)</p>
<p>Zuvor jedoch m&ouml;chte ich euch allen da drau&szlig;en ein gesegnetes Weihnachtsfest w&uuml;nschen. An diesem Tag wurde jemand geboren, den Leute wie ich sehr wichtig finden und mit dem Begriff "Liebe" gleichsetzen. (Armin, du erlebst ja vielleicht eine ganz &auml;hnliche Situation -- ich dr&uuml;ck euch die Daumen!)</p>
<p>M&ouml;ge die Zeit am Ende des Jahres uns allen etwas Gelegenheit geben, Luft zu holen, nachzudenken, liebe Menschen zu treffen sowie die Ruhe und den Frieden zu genie&szlig;en. (Leider ist das nicht allen Menschen verg&ouml;nnt.)</p>
<p>In diesem Sinne w&uuml;nsche ich euch allen frohe Weihnachten, buon natale, merry christmas, feliz navidad!</p>
