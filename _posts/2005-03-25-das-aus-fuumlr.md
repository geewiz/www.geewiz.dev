---
layout: post
title: "Das Aus f&uuml;r MPlayer und Co.?"
microblog: false
guid: http://geewiz.micro.blog/2005/03/25/das-aus-fuumlr.html
date: 2005-03-25T08:36:14+0200
type: post
url: /2005/03/25/das-aus-fuumlr.html
---

<p>Software-Patente kommen, wenn die Europ&auml;ische Kommission ihren Kurs fortsetzt, und dann kommen dunkle Stunden f&uuml;r Freie Software. In entsprechenden Farben findet man deshalb momentan die <a href="http://mplayerhq.hu/homepage/index.html">Homepage</a> des freien MPlayer-Projekts vor. Die Startseite beginnt mit der Erkl&auml;rung "Diese Website wurde wegen zahlreicher Patentverletzungen in MPlayer geschlossen. Die anderen freien Multimedia-Player sind die n&auml;chsten."</p>
<p>Im Anschluss ruft das MPlayer-Team dazu auf, mehr zu tun als Online-Petitionen zu unterzeichnen, weil allein dadurch kaum etwas bewegt werden wird. Es gelte vielmehr, im direkten Kontakt die Abgeordneten des Europaparlaments davon zu &uuml;berzeugen, dass Software-Patente gro&szlig;en Schaden anrichten werden und daher vom Parlament abgelehnt werden m&uuml;ssen.</p>
<p>Details zur drohenden EU-Richtlinie f&uuml;r Softwarepatente ver&ouml;ffentlicht <a href="http://nosoftwarepatents.com/.">nosoftwarepatents.com</a></p>
