---
layout: post
title: "Bed&uuml;rfnistheorien"
microblog: false
guid: http://geewiz.micro.blog/2005/03/28/beduumlrfnistheorien.html
date: 2005-03-29T01:05:58+0200
type: post
url: /2005/03/28/beduumlrfnistheorien.html
---

<p>Weil ich t&auml;glich als F&uuml;hrungskraft gefordert bin, besch&auml;ftige ich mich in letzter Zeit auch theoretisch mit den Gebieten Organisation und Management. Im <a href="http://wiki.jochen-lillich.de/">Wiki</a> habe ich begonnen, meine Erkenntnisse zusammenzufassen.</p>
<p>Im neuesten Artikel <a href="http://wiki.jochen-lillich.de/?Bed">Bed&uuml;rfnistheorien</a>%C3%BCrfnisTheorien halte ich fest, welche theoretischen Modelle es zum Thema "Bed&uuml;rfnisse" gibt. Diese Modelle sind wichtig, wenn es um die Motivation von Mitarbeitern geht.</p>
