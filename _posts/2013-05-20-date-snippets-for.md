---
layout: post
title: "Date snippets for TextExpander"
microblog: false
guid: http://geewiz.micro.blog/2013/05/20/date-snippets-for.html
date: 2013-05-20T11:37:38+0200
type: post
url: /2013/05/20/date-snippets-for.html
---
<p>One of the utilities that I immediately install on every new Mac is <a href="http://smilesoftware.com/TextExpander/index.html">TextExpander</a>. It makes typing routine stuff so much easier.</p>
<p>In a recent blog post, David Sparks wrote about his <a href="http://macsparky.com/blog/2013/5/text-expander-snippets-date-and-time">TextExpander snippets for date and time</a>, describing two simple but effective use cases: Shortcuts like "xm8", which expands to "August" (why haven't I thought of this myself?), and date calculation snippets like "d--" that inserts yesterday's date.</p>
<p>Using David's snippets on your Mac requires only three mouse clicks (on the link above, on the download link in his post and finally on the snippet file).</p>
