---
layout: post
title: "Linux gewinnt erneut"
microblog: false
guid: http://geewiz.micro.blog/2004/12/14/linux-gewinnt-erneut.html
date: 2004-12-14T04:05:00+0200
type: post
url: /2004/12/14/linux-gewinnt-erneut.html
---

<p>Auf <a href="http://www.theage.com.au/news/Breaking/TCO-study-Linux-wins-again/2004/12/13/1102786990788.html?oneclick=true">The Age</a> wird auf eine <a href="http://www.cybersource.com.au/about/linux_vs_windows_tco_comparison.pdf">aktualisierte Studie</a> hingewiesen, die die TCO von Windows und Linux vergleicht.</p>
<p>Erneut geht das Ergebnis zu Gunsten von Linux aus: ein 250-Personen-Unternehmen k&ouml;nne mit Linux bis zu 36% an Ausgaben einsparen.</p>
