---
layout: post
title: "Rezension: Time Management for System Administrators"
microblog: false
guid: http://geewiz.micro.blog/2006/03/03/rezension-time-management.html
date: 2006-03-03T09:33:50+0200
type: post
url: /2006/03/03/rezension-time-management.html
---

<p>Nachdem ich das O'Reilly-Buch <a href="http://www.amazon.de/exec/obidos/ASIN/0596007833/geewizweblog-21"><em>Time Management for System Administrators</em></a> schon vor ein paar Wochen in einem <a href="http://blog.jochen-lillich.de/archives/266-Time-Management-for-System-Administrators.html">Blogeintrag</a> erw&auml;hnt hatte, bekam ich jetzt auch Gelegenheit, es zu lesen. Danke f&uuml;r's Ausleihen, Ralf -- ich werde es mir jetzt auch selbst zulegen. Ich finde n&auml;mlich, dass das Buch tats&auml;chlich eine gute Hilfe bei den t&auml;glichen Versuchen unseres Berufsstandes ist, Unmengen an Auftr&auml;gen und Projekten unter einen Hut zu bringen, ohne die Nerven und den Verstand zu verlieren.</p>
<p><a href="http://www.jochen-lillich.de/buchtipps/time-management-for-system-administrators">Meine Rezension</a> findet ihr auf meiner Homepage.</p>
