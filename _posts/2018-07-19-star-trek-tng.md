---
layout: post
title: "Star Trek TNG intro, the missing lyrics"
microblog: false
guid: http://geewiz.micro.blog/2018/07/19/star-trek-tng.html
date: 2018-07-19T02:00:00+0200
type: post
url: /2018/07/19/star-trek-tng.html
---

Wil Wheaton:

> When we worked on Next Generation, Brent Spiner and I would sit at our consoles on the bridge, and make up lyrics to our show's theme song. I vaguely recall coming up with some pretty funny and clever stuff, but nothing that held together as perfectly as this

Expect me to sing along from now on.
