---
layout: post
title: "Using Twitter for good"
microblog: false
guid: http://geewiz.micro.blog/2007/03/11/using-twitter-for.html
date: 2007-03-11T22:17:39+0200
type: post
url: /2007/03/11/using-twitter-for.html
---

<p>Many people don't get or like <a href="http://twitter.com/Geewiz.">Twitter</a> Gee, even I didn't like it until recently! It seems so much like a source of steady interruptions. But there's not only a downside. <a href="http://twitter.com/chrisbrogan">Chris Brogan</a> wrote an article on LifeHacker titled <a href="http://www.lifehack.org/articles/lifehack/5-ways-to-use-twitter-for-good.html">5 ways to use Twitter for good</a> where he lists reasonable uses for this new "web text messaging":</p>
<h1>Quick Human Answers</h1>
<h1>Conference / News Briefings</h1>
<h1>Friendsourcing</h1>
<h1>Micro-Attention-Sharing</h1>
<h1>Direct People to Good Causes</h1>
<p>And as a bonus, as he points out, you get to inform people about the central question that Twitter poses: "Where are you now?"</p>
<p>By the way, I just installed <a href="http://www.adiumxtras.com/index.php?a=xtras">TwitterAdium</a>&amp;xtra_id=3484 so that my IM away message always displays my Twitter status. It's so great when you can mash up software and services!</p>
