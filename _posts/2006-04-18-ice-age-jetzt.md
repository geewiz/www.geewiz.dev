---
layout: post
title: "Ice Age 2 - Jetzt tauts"
microblog: false
guid: http://geewiz.micro.blog/2006/04/18/ice-age-jetzt.html
date: 2006-04-18T07:08:55+0200
type: post
url: /2006/04/18/ice-age-jetzt.html
---

<p>Heute abend haben Kai und ich in Kirrlach "Ice Age 2" angeschaut. Von der ersten Minute, in der der Sound gest&ouml;rt war, mal abgesehen, hatten wir wirklich Spa&szlig;. Die Macher haben sich einige Gags einfallen lassen, um auch mit der zweiten Folge das Publikum zum Lachen zu bringen. Der Urzeitnager Scrat -- wie immer auf der Jagd nach seiner Eichel -- ist davon nur einer.</p>
