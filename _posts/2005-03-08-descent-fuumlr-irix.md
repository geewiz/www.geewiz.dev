---
layout: post
title: "Descent 2 f&uuml;r IRIX"
microblog: false
guid: http://geewiz.micro.blog/2005/03/08/descent-fuumlr-irix.html
date: 2005-03-08T21:13:42+0200
type: post
url: /2005/03/08/descent-fuumlr-irix.html
---

<p>In letzter Zeit schie&szlig;en auf Nekochan vor allem die IRIX-Spiele wie Pilze aus dem Boden. Wie Dexter1 im dortigen Forum <a href="http://forums.nekochan.net/viewtopic.php?t=5266">bekanntgab</a>, hat er ein <a href="http://www.nekochan.net/downloads/Nekoware/beta/neko_descent2x-0.2.5.tardist">Tardist-Paket</a> f&uuml;r das 3D-Spiel Descent 2 gebaut.</p>
<p>Das Paket enth&auml;lt 2 Programmdateien:</p>
<ul>
<li>d2x-sdl (langsam)</li>
<li>d2x-gl (OpenGL mit Texturen, sehr schnell, aber kleine Fehler)</li>
</ul>
<p>Descent 1 soll in K&uuml;rze folgen.</p>
