---
layout: post
title: "GI gegen &uuml;bertriebene Vorratsdatenspeicherung"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/gi-gegen-uumlbertriebene.html
date: 2006-06-18T08:32:58+0200
type: post
url: /2006/06/18/gi-gegen-uumlbertriebene.html
---

<p>Die Gesellschaft f&uuml;r Informatik (GI) spricht sich daf&uuml;r aus, die umstrittene verdachtsunabh&auml;ngige Vorratsspeicherung von Telefon- und Internetdaten auf sechs Monate zu beschr&auml;nken. Diese Frist liegt deutlich unter der, mit der der Bundestag die vom EU-Parlament beschlossene Ausforschung unserer Nutzungsdaten umsetzen will.</p>
<p>Die <a href="http://www.gi-ev.de/fileadmin/redaktion/Download/stellungnahme_datenspeicherung060614.pdf">Stellungnahme der GI</a> gibt es als PDF-Datei. Sie fordert darin klare Definitionen und einen "angemessenen Ausgleich zwischen Strafverfolgungsinteressen und Grundrechtsschutz". Deutschland d&uuml;rfe in der Einschr&auml;nkung der Grundrechte seiner B&uuml;rger nicht &uuml;ber das auf europ&auml;ischer Ebene beschlossene Mindestma&szlig; hinausgehen.</p>
