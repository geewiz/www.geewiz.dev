---
layout: post
title: "A new Freiburg Iron Blogger"
microblog: false
guid: http://geewiz.micro.blog/2012/01/10/a-new-freiburg.html
date: 2012-01-10T19:39:30+0200
type: post
url: /2012/01/10/a-new-freiburg.html
---
<p>I just made a new entry to the member list of <a title="Iron Blogger Freiburg" href="/iron-blogger-freiburg/">Iron Blogger Freiburg</a>. And it's certainly a bit unusual, because <a href="http://hmunro.wordpress.com/2011/12/02/three-days-in-freiburg/">Heather</a> actually doesn't live in Freiburg. She's from&nbsp;Minneapolis, which is a teeeensy bit away from here...</p>
<p>But she's asked so nicely that I didn't hesitate to add her to the list:</p>
<blockquote><p>I'm wondering whether you'd consider adding someone to your group who doesn't currently live in Freiburg &mdash; but who will be there in September to settle any fines she may incur. (Heck, even if I *don't* incur any fines, I'll gladly pick up a round or two of beers.)</p>
<p>If you'd like to check out my blogging qualifications before answering, I'm at <a href="http://www.hmunro.wordpress.com/">www.hmunro.wordpress.com</a>.</p></blockquote>
<p>I'd say you're more than qualified. So, welcome to Iron Blogger Freiburg, Heather! I'm looking forward to meeting you in September!</p>
<p>Man, this whole thing may only be a few days old, but it's already becoming incredibly fun and motivating!</p>
