---
layout: post
title: "Nanotechnologie gegen Krebs"
microblog: false
guid: http://geewiz.micro.blog/2005/11/08/nanotechnologie-gegen-krebs.html
date: 2005-11-08T03:35:32+0200
type: post
url: /2005/11/08/nanotechnologie-gegen-krebs.html
---

<p>In einem <a href="http://www.wired.com/news/medtech/0">Artikel</a>,1286,69206,00.html berichtet Wired von Pl&auml;nen amerikanischer Mediziner, mit Nanomaschinen gegen Krebsgeschw&uuml;re anzugehen.</p>
<p>Die "Maschinchen" sind in diesem Falle spezielle Molek&uuml;le in der Gr&ouml;&szlig;e weniger Nanometer (schon ein rotes Blutk&ouml;rperchen hat 4000nm), die darauf programmiert werden k&ouml;nnen, Krebszellen aufzusp&uuml;ren und sich an sie anzulagern.</p>
<p>Im ersten Schritt k&ouml;nnte man Krebszellen, die sonst noch gar nicht auffielen, finden, indem man die Nanomolek&uuml;le per Magnetresonanz oder unter floureszierendem Licht detektierbar macht.</p>
<p>Der n&auml;chste Schritt k&ouml;nnte so weit gehen, die Nanoteilchen "scharf" zu machen, indem man sie mit Metallatomen versieht. Durch elektrische oder magnetische Felder k&ouml;nnte man dann die Nanomaschinen mitsamt den Krebszellen vernichten.</p>
<p>Auch nach der Lekt&uuml;re des passenden <a href="http://www.amazon.de/exec/obidos/ASIN/0061015725/geewizweblog-21">Crichton-Thrillers</a> finde ich das eine vielversprechende Aussicht. Erste richtungsweisende Erfolge wurden sogar schon erzielt.</p>
