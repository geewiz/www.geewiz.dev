---
layout: post
title: "Writing command line applications with App::Cmd"
microblog: false
guid: http://geewiz.micro.blog/2006/08/27/writing-command-line.html
date: 2006-08-27T07:34:35+0200
type: post
url: /2006/08/27/writing-command-line.html
---

<p>If you write command line applications in Perl, there are certain standards you happen to code everytime, for example the definition and validation of command line options. The new Perl module <a href="http://search.cpan.org/">App::Cmd</a>~rjbs/App-Cmd-0.007/ uses OOP to simplify those tasks and make your code reusable.</p>
<blockquote class="posterous_medium_quote">
<p>App::Cmd is a set of tools designed to make it simple to write sophisticated command line programs. It handles commands with multiple subcommands, generates usage text, validates options, and lets you write your program as easy-to-test classes.</p>
</blockquote>
<p>With App::Cmd, you write a subclass for every task your application carries out. If we take Subversion as an example, there would be a subclass for checking source code out, one for checking changes in, one for generating changelogs and so on.</p>
<p>By using base classes, you can factor out standard tasks like displaying help information when the "-h" switch is used.</p>
<p>It seems to me that App::Cmd makes it easier to write non-trivial command line programs without losing structure and maintainability. I'll give it a try.</p>
