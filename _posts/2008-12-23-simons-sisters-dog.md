---
layout: post
title: "Simon's sister's dog"
microblog: false
guid: http://geewiz.micro.blog/2008/12/23/simons-sisters-dog.html
date: 2008-12-23T09:01:56+0200
type: post
url: /2008/12/23/simons-sisters-dog.html
---

<p>You know Simon's Cat from the first <a href="http://www.jochen-lillich.de/strangely-familiar-simon039s-cat">two</a> <a href="http://www.jochen-lillich.de/simon039s-cat-does-it-again">animations</a> I published on my blog. Now it's time to introduce you to Simon's sister's dog!</p>
<p>Recently, I got an email that, at first, looked like spam, but was clever PR for the RSPCA&rsquo;s campaign to tackle pet obesity:</p>
<blockquote>
<p>I noticed that you have blogged about Simon&rsquo;s Cat in the past, so I thought you would be really interested to hear that Simon Tofield, the maker of Simon&rsquo;s cat, has finally released a new animation. The video, featuring Simon&rsquo;s sister&rsquo;s dog has been made especially for the RSPCA, and can be watched at <a href="http://www.giveanimalsavoice.org.uk">www.giveanimalsavoice.org.uk</a>.</p>
</blockquote>
<p>I'm amazed at this social media approach to get the RSPCA's message spread and more than glad to help the cause. Here's the clip:</p>
