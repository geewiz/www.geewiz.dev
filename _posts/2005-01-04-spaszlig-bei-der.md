---
layout: post
title: "Spa&szlig; bei der Arbeit"
microblog: false
guid: http://geewiz.micro.blog/2005/01/04/spaszlig-bei-der.html
date: 2005-01-04T20:46:00+0200
type: post
url: /2005/01/04/spaszlig-bei-der.html
---

<p>Ja, es gibt sie noch, die Nischen, die nur clevere Leute besetzen und damit Geld verdienen. Eine solche Nische ist zum Beispiel das <a href="http://www.managementcircle.de/v/v.cfm?Nr=5959">Humor-Training</a>&amp;BR=Management%20%26%20Soft%20Skills&amp;cid=bar6 f&uuml;r F&uuml;hrungskr&auml;fte.</p>
<p>Ich finds super, dass es sowas gibt. Da stellt sich mir nur noch eine Frage: wenn ich jetzt einem Vorgesetzten dieses Training empfehle, stellt er mich danach wieder ein?</p>
