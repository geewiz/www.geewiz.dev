---
layout: post
title: "Fatblogging: 96"
microblog: false
guid: http://geewiz.micro.blog/2007/06/14/fatblogging.html
date: 2007-06-14T20:09:43+0200
type: post
url: /2007/06/14/fatblogging.html
---

<p>I still don't fully get that fatblogging concept yet, but at least it really seems to work: I'm now down to 96kg. And that's all I expect, so I wonder if I should waste my time trying to find out if it's fatblogging frequency, entry length or another factor that makes me lose weight. We'll see if the secret will eventually be lifted.</p>
<p>On the jogging front, I'm now at five 5-minute running segments, each followed by a minute of walking. I'm doing quite well -- although I have to run slightly uphill, I often miss the breaks, running 6 or more minutes. Following the advice in my running guide, if I feel like doing more, I run longer than the half hour in the plan, instead of increasing my speed. The weather is great at the moment and it feels really good to do my early morning route under the rising sun, in fresh air and the quietness of just nature and me.</p>
<p>Nutrition-wise, there are rare phases when I just can't resist buying my beloved fruit gums. But many times, I realize it's just my weaker self trying to take control, and I fight it successfully. Separating carbohydrates from fat sometimes is a challenge, since neither our company restaurant nor the restaurants in the neighborhood offer many dishes that don't mix noodles, dough or potatoes with oil or fat. My best bet always are salads (sometimes with turkey breast or other meat, but I always leave the bread untouched) or completely vegetarian dishes. Since those can be delicious, too, I don't mind the diet change.</p>
<p>So, everything is fine on the weight loss and fitness front. And hopefully, the motivation I gain from this success will result in a self-energizing circle!</p>
