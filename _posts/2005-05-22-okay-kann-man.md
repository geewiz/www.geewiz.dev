---
layout: post
title: "Okay, kann man lassen"
microblog: false
guid: http://geewiz.micro.blog/2005/05/22/okay-kann-man.html
date: 2005-05-22T21:45:48+0200
type: post
url: /2005/05/22/okay-kann-man.html
---

<p>Nachdem sich schon <a href="http://blog.koehntopp.de/archives/807-Philologe">Kris</a>,-was.html und <a href="http://retro-park.de/blog/index.php?/archives/307-Philosoph.html">Ralf</a> dem <a href="http://de.tickle.com/test/iq/intro.html">Klassischen Intelligenztest</a> unterzogen hatten, wurde ich doch neugierig, was ich zustande bringe. Das Ergebnis ist dann auch ganz okay. ;)> Jochen Lillich, Ihr Intelligenzquotient ist 135</p>
<blockquote>
<p>Jochen Lillich, Ihr Ergebnis liegt deutlich &uuml;ber dem Durchschnitt! Herzlichen Gl&uuml;ckwunsch! Sie verf&uuml;gen &uuml;ber eine gro&szlig;e Anzahl an Talenten, und diese sind besser ausgepr&auml;gt als bei den meisten Menschen. Sie haben dar&uuml;ber hinaus bewiesen, dass Sie besonders gut sind, Fragen wie die im Test gestellten beantworten k&ouml;nnen. Der klassische Intelligenztest analysiert Ihre pers&ouml;nlichen St&auml;rken und Schw&auml;chen in den Bereichen Mathematik, Sprache, visuell-r&auml;umliches Denken, und Logik. Anhand der Auswertung Ihrer Antworten k&ouml;nnen wir ableiten, in welchem dieser Bereiche Sie am besten abschneiden.</p>
<p>Zu Ihrem Ergebnis: Sie sind au&szlig;ergew&ouml;hnlich sprachbegabt und kommunizieren auf h&ouml;chstem Niveau. Ihr hoch entwickeltes Sprachzentrum verarbeitet komplexeste Sachverhalte und deckt jeden auch noch so verborgenen Sinn dahinter zuverl&auml;ssig auf. Dabei l&ouml;sen Sie Probleme au&szlig;ergew&ouml;hnlich kreativ - Sie sind ein Vision&auml;r, st&auml;ndig fliegen Ihnen neue und phantasievolle Ideen zu. Sie sind vom Stamme der Vokabelkrieger. Nat&uuml;rlich k&ouml;nnen Sie sich ausgezeichnet mitteilen. Dabei ist Ihr riesiger Wortschatz Ihre gr&ouml;&szlig;te St&auml;rke. Und diesen verstehen Sie zu nutzen. Sie sind in der einzigartigen Position, Situationen und Dinge originell zu beschreiben, sowie die Zukunft vor Ihrem inneren Auge durchzuspielen. Kurzum, Ihre St&auml;rken machen Sie zu einem Vision&auml;r, einem Trendsetter, der Unmengen frischer Ideen verspr&uuml;ht.</p>
</blockquote>
<p>Das ist doch mal ein geiles Wort. Armin, ich brauche sofort ein T-Shirt "Vokabelkrieger"!</p>
