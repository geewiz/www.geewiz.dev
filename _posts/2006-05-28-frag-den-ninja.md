---
layout: post
title: "Frag den Ninja"
microblog: false
guid: http://geewiz.micro.blog/2006/05/28/frag-den-ninja.html
date: 2006-05-28T05:42:32+0200
type: post
url: /2006/05/28/frag-den-ninja.html
---

<p>Als Kind war ich ein Ninja-Fan, heute mag ich surrealen Humor. Beides in Kombination ist <a href="http://askaninja.com/:">Ask A Ninja</a> in kurzen Videocasts beantwortet der Ninja wichtige Fragen wie "Wie t&ouml;tet man einen Ninja?", "Bekommen Ninjas Erk&auml;ltungen?" oder "Wie ist das Verh&auml;ltnis von Ninjas zur Physik?".</p>
<p>Sp&auml;testens beim Spruch "...faster than Tara Reid can destroy a viable career" hing ich mit Tr&auml;nen in meinem Stuhl.</p>
