---
layout: post
title: "Another use for Evernote: Collect conference material"
microblog: false
guid: http://geewiz.micro.blog/2009/11/22/another-use-for.html
date: 2009-11-22T05:17:02+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwjochenli_fiial-scaled500.jpg?w=85
photos:
- /assets/wp-content/2011/05/media_httpwwwjochenli_fiial-scaled500.jpg?w=85
url: /2009/11/22/another-use-for.html
---

<p>Before I'll leave for <a href="http://drupalcamp.at/">DrupalCamp Vienna</a> on Thursday night, I'll have some preparations left to do. Where should I gather all the stuff &mdash; the hotel confirmation, the train reservation, links to places in Vienna I'd like to see? While many items land inside my email inbox, there also are website snapshots and other documents. And then, there's the material I'll collect at the conference itself: photos, notes, slides etc. </p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwjochenli_fiial" height="85" src="/assets/wp-content/2011/05/media_httpwwwjochenli_fiial-scaled500.jpg?w=85" width="85" />
</div>
<p>It's great timing that Michael Gray just published his article,<br />
<a href="http://www.wolf-howl.com/tools/how-to-use-evernote-to-create-the-ultimate-post-conference-reference-guide/">How to Use Evernote to Create the Ultimate Post Conference Reference Guide</a>.</p>
<p>Although I'm already an <a href="http://www.evernote.com">Evernote</a> premium subscriber, that's a use case I haven't thought of until now. But of course: Evernote will willingly store all the stuff I drop into or email to the notebook "DrupalCamp Vienna" that I just created. With my iPhone, I can shoot snapshots of presentation slides with my iPhone and have them made searchable through the Evernote text recognition mechanism. I'll even be able to record sound snippets to transcribe later. This really makes sense.</p>
<p>Thanks for the tip, Michael!</p>
