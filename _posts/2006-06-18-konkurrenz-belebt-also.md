---
layout: post
title: "Konkurrenz belebt also doch das Gesch&auml;ft"
microblog: false
guid: http://geewiz.micro.blog/2006/06/18/konkurrenz-belebt-also.html
date: 2006-06-18T07:01:34+0200
type: post
url: /2006/06/18/konkurrenz-belebt-also.html
---

<p>Das Web 2.0 boomt, die Artenvielfalt neuer Online-Anwendungen ist &uuml;berw&auml;ltigend. Egal, ob es um pers&ouml;nliche Portalseiten oder um Online-Fotoalben geht, hat der Benutzer die Qual der Wahl unter vielen Alternativen. W&auml;hrend man aber nach dem Wechsel der eigenen Portalseite diese dank AJAX mit wenigen Klicks neu zusammenbauen kann, ist der Umzug zu einem anderen Fotospeicher schwierig. Jedenfalls muss die Motivation zum Wechseln schon ungew&ouml;hnlich gro&szlig; sein, dass man es auf sich nimmt, hunderte von Fotos neu hochzuladen und m&uuml;hsam wieder mit Meta-Informationen wie Tags und Kommentaren zu versehen.</p>
<p>Doch theoretisch ginge ein Umzug der eigenen Galerie auch einfacher: viele Webdienste bieten n&auml;mlich eine Schnittstelle f&uuml;r externe Software, das sog. API(Application Programming Interface), an. Dieses erm&ouml;glicht es, von aussen auf Funktionen des Webdienstes zuzugreifen. Per API kann eine Desktop-Software neue Eintr&auml;ge im Online-Kalender machen oder den aktuellen Projektstatus abfragen -- per API k&ouml;nnte aber auch ein Fotodienst direkt auf die Bilder zugreifen, die der Benutzer bei einem anderen Fotodienst abgelegt hat, und diese mit allen Meta-Informationen importieren.</p>
<p><a href="http://www.flickr.com">Flickr</a> bietet ein solches API an, war aber &uuml;ber die Anfrage, dieses f&uuml;r den Konkurrenten <a href="http://www.zoomr.com">Zoomr</a> zug&auml;nglich zu machen, alles andere als begeistert: <a href="http://flickr.com/groups/central/discuss/72157594165399644/">Warum sollten wir Bandbreite und CPU-Kapazit&auml;t verbrennen, nur, um Zeug direkt auf ihre Server zu schicken?", lautete zun&auml;chst die "Antwort von Flickr-Gr&uuml;nder Stewart Butterfield</a>#comment72157594166309763.</p>
<p>Die angeregte Diskussion &uuml;ber Wettbewerb und Eigentum an Bildern f&uuml;hrte aber zu einem Umdenken bei Butterfield:</p>
<blockquote class="posterous_short_quote">
<p>I actually had a change of heart and was convinced by Eric's position that we definitely should approve requests from direct competitors as long as they do the same.</p>
</blockquote>
<p>Fairness ist also die Basis, auf der Flickr bereit ist, sich Konkurrenten zu &ouml;ffnen. Klingt, als sei "Social software" wirklich ein treffender Name f&uuml;r das Neue am Web 2.0!</p>
<p>(via <a href="http://www.techcrunch.com/2006/06/16/why-is-flickr-afraid-of-zoomr/">TechCrunch</a>, <a href="http://radar.oreilly.com/archives/2006/06/api_keys_for_direct_competitor.html">O'Reilly Radar</a>)</p>
