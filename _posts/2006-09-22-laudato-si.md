---
layout: post
title: "Laudato Si"
microblog: false
guid: http://geewiz.micro.blog/2006/09/22/laudato-si.html
date: 2006-09-22T19:35:13+0200
type: post
url: /2006/09/22/laudato-si.html
---

<p>Seit drei&szlig;ig Jahren begleitet mich "Laudato Si" durch Jugendgottesdienste und Agape-Feiern. Und ja, es ist nett, aber auch seeeeehr abgedroschen. Als ich letztens einen Ausschnitt aus Mickie Krauses Ballermann-Version des Lieds h&ouml;rte ("Sei gepriesen f&uuml;r zwei- und dreimal Niesen", "Sei gepriesen f&uuml;r Poldi und f&uuml;r Schweini"), war ich hin- und hergerissen zwischen "Oh nee, jetzt verwursten sie schon Kirchenlieder!" und "Interessant, dass sowas offenbar einen Markt hat."</p>
<p>Ich war umso mehr erstaunt, als ich auf ein <a href="http://www.catholicism-wow.de/pivot/entry.php?id=214">Interview mit Mickie Krause</a> stie&szlig;, in dem er sich als ehemaliger Jugendleiter outete und Ansichten wiedergab, die sich mit meinen in weiten Z&uuml;gen decken.</p>
<blockquote class="posterous_medium_quote">
<p>Wir brauchen Menschen, die Kirche machen, Kirche bewegen und ver&auml;ndern! Deshalb habe ich fr&uuml;her jahrelang Jugendarbeit gemacht und kann behaupten, Kirche bewegt zu haben. Wer Kirche ver&auml;ndern will, muss Mitanpacken und nicht darauf warten, dass andere dieses erledigen. Ich m&ouml;chte aber zum Schluss auch noch erw&auml;hnen, dass es viele tolle Leute in der Kirche gibt. Unser Pastor zum Beispiel ist der absolute Hammer. Er geh&ouml;rt zu den Priestern, die es verstehen, ihre Gemeinde zu begeistern und zu bewegen. Mit ihm werde ich &uuml;brigens bei der Taufe meiner dritten Tochter Laudato Si singen, allerdings dann die Originalstrophen.</p>
</blockquote>
<p>Ich h&auml;tte nie gedacht, dass ich sowas mal sage: Respekt, Herr Krause! :-)</p>
