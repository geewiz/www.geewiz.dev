---
layout: post
title: "Weeknote W5 2021"
microblog: false
guid: http://geewiz.micro.blog/2021/01/30/weeknote-w.html
date: 2021-01-30T20:32:00+0200
type: post
categories:
- "Weeknotes"
- "DEV"
tags: [weeknote]
url: /2021/01/30/weeknote-w.html
---
## To tile or not to tile

Looking at Twitch streams and Youtube videos around Linux, it seemed to me that everyone was into tiling window managers. This week, I decided to finally try this approach of filling and subdividing the whole screen area. On macOS, I installed [yabai](https://github.com/koekeishiya/yabai/wiki), and on Linux (more on that below), I chose the popular [i3](https://i3wm.org/).

After a few days of the tiling life, my conclusion is that I don't see much benefit in forcing my application windows to fill up all the available screen real estate. Maybe it's because I already have a habit of tiling my windows manually -- when it makes sense. However, often it just doesn't make sense. Some windows really don't like to be squeezed into a tiny space, and even on a 13" screen, a full-size terminal window isn't of much use to me. I'll keep my windows floating. If I really want to tile them, there's always [BetterTouchTool](https://folivora.ai/docs/docs/102_window_snapping_advanced.html).

## Linux on Apple hardware

Another item of curiosity this week (is this some kind of lockdown syndrome?) was if 2021 could be the year of Linux on the Desktop for me. While I still enjoy the stability and user-friendliness of the macOS ecosystem, some of its constraints do annoy me from time to time. I have an unused Macbook Pro 13" from 2015, and it's now running Ubuntu 20.04. My go-to applications like Brave, VS Code and Obsidian worked fine out of the box. But when it came to more specialised use cases, I quickly realised how much macOS spoils me with its choice of well-polished applications.

Maybe I will pursue a "best of both worlds" approach, where I do my management work on the Mac and my DevOps work on Linux. That is, if the hardware issues that drove me into Apple's arms more than a decade ago don't spoil it for me again. For example, there's a 50:50 chance that after powering up the laptop, Bluetooth doesn't work, preventing me from using a proper mouse and keyboard. Sigh.
