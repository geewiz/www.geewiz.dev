---
layout: post
title: "Erst mal kein Korea-Urlaub"
microblog: false
guid: http://geewiz.micro.blog/2005/02/01/erst-mal-kein.html
date: 2005-02-02T01:07:00+0200
type: post
url: /2005/02/01/erst-mal-kein.html
---

<p>Unter dem Titel <a href="http://www.spiegel.de/panorama/0">Korea sagt Langhaarigen den Kampf an</a>,1518,339676,00.html ist bei Spiegel Online zu lesen, dass die Regierung Nordkoreas Wert darauf legt, dass M&auml;nner ihre Haare in "sozialistischer Manier" tragen.</p>
<p>Wer sich der offiziellen Haar-Doktrin nicht beugt, wird als "blinder Anh&auml;nger der Bourgeoisie" und "unhygienischer, antisozialistischer Dummkopf" tituliert. Und das geschieht, mit Hilfe des f&uuml;hrenden Fernsehsenders Nordkoreas, in breiter &Ouml;ffentlichkeit.</p>
<p>Es sind aber nicht nur &auml;sthetische und hygienische, sondern auch biologische Gr&uuml;nde, die daf&uuml;r ins Feld gef&uuml;hrt werden: Lange Haare behinderten n&auml;mlich die Gehirnaktivit&auml;t, indem sie den Nerven Sauerstoff entz&ouml;gen.</p>
<p>Ah ja. Das erkl&auml;rt nat&uuml;rlich die Mail unserer Teamassistentin, die gerade vorhin per Mail auf einen neuen Fris&ouml;r in Durlach hinwies.</p>
<p>(Dank an Loco)</p>
