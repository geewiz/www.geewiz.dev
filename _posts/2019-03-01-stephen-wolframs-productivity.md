---
layout: post
title: "Stephen Wolfram's productivity systems"
microblog: false
guid: http://geewiz.micro.blog/2019/03/01/stephen-wolframs-productivity.html
date: 2019-03-01T10:56:15+0200
type: post
url: /2019/03/01/stephen-wolframs-productivity.html
---
I'm amazed, especially by the amount of things you can do with Wolfram Notebooks.
