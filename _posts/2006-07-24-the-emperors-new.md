---
layout: post
title: "The emperor's new clothes"
microblog: false
guid: http://geewiz.micro.blog/2006/07/24/the-emperors-new.html
date: 2006-07-24T06:16:29+0200
type: post
url: /2006/07/24/the-emperors-new.html
---

<p>It's one thing to create hype about some secret new product that "will revolutionize internet communications". It's another thing to actually have something on his hand when cards are called. And a third one to know how to behave in online space.</p>
<p><a href="http://www.combots.com">COMBOTS</a>, the new project of my former employer WEB.DE AG (before <a href="http://www.heise.de/english/newsticker/news/61522">selling the WEB.DE portal</a> to United Internet) uncovered their secret  product at this year's stockholders' meeting in mid July. Well, lo and behold -- it's an... instant messaging application. Wow. But wait, it's got funny avatars instead of simple buddy names! And you have to pay if you want a new one. Or a new bodily expression for your current one. Now, that's just great.</p>
<p>Stockholders thought so, too. Their stock value instantly dropped by more than 10%. Screwing the stock options I got when I was hired.</p>
<p>But what concerns me quite a bit more is that, for a company that wants to revolutionize the internet, COMBOTS doesn't seem to know how to behave in that space. Some of their reactions to the dismissal iof their product were as stupid as can be: critical parts in the <a href="http://de.wikipedia.org/wiki/Combots">COMBOTS Wikipedia article</a> were removed several times. The origins of these alterations, as well as these of <a href="http://netzhure.de/archives/47-ComBOTS-Mitarbeiter-sorgen-sich.html">abating forum and blog comments</a> could be traced back to the COMBOTS company gateway. How boneheaded do you have to be to believe that you can get away with such a threadbare behaviour?</p>
<p>I'm curious how that story will go on. But I don't think I'll ever make some money selling those stock options.</p>
<p>For some more comments (in german), see <a href="http://praegnanz.de/weblog/braucht-man-combots">praegnanz.de</a>, <a href="http://www.werbeblogger.de/2006/07/13/decke-vom-kopf-">Werbeblogger</a>%e2%80%93-wenig-drunter-3/, or <a href="http://netzhure.de/archives/46-ComBOTS-Jamba-im-Internet.html.">Netzhure</a></p>
