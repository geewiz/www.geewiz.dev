---
layout: post
title: "Menu shortcuts on the E61"
microblog: false
guid: http://geewiz.micro.blog/2006/08/07/menu-shortcuts-on.html
date: 2006-08-07T18:35:33+0200
type: post
url: /2006/08/07/menu-shortcuts-on.html
---

<p>I'm still trying out my Nokia E61 and its ability to fully replace my trusted Time/System management book.</p>
<p>One thing that I found a bit cumbersome is the menu navigation with the joystick that I have to do when I want to use applications that are not on the <a href="http://www.e-series.org/archives/77">active standby" screen. On the "E-Series blog</a>, I found a nifty hint today solving that problem pretty much: every position on the main menu has its correspondig number key.</p>
<p>So, by strategically positioning the menu entries and using the number keys, you can quickly choose the folder or application you need without being reminded of Decathlon[1].</p>
<p>fn1. Is there a Competition Pro for the E61? ;-)</p>
