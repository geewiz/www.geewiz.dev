---
layout: post
title: Get an AeroPress gasket to seal properly again
date: 2023-03-31
description: How to get a leaking AeroPress gasket back into shape.
image:
category: tips
tags:
  - living
published: true
sitemap: true
---

I've been a user and fan of the AeroPress since I get one for my birthday a few
years ago. Unfortunately, it's starting to show its age.

The AeroPress is a popular coffee brewing device shaped like a huge syringe that
uses a combination of immersion and pressure to create a rich and smooth cup of
coffee. It consists of a plastic chamber, a screw-on filter cap and a plunger
with a rubber seal. It comes with paper filters, but I replaced them with a
reusable metal filter. I'm using the "inverted" brewing method in which I stand
the AeroPress on the plunger handle, with the open chamber already on top. I
grind 18g of beans (which I get via a [Badger & Dodo
subscription](https://badgeranddodo.ie/payg-coffee-subscriptions/), add the
coffee to the chamber along with 100g of hot water, stir the mixture, put the
filter cap on and let the coffee extract for 90 seconds before I turn the
AeroPress over on my cup and press the plunger down. The result is a strong,
clean and flavorful coffee that I enjoy with frothed milk. 

Recently, my fun of brewing started to get spoiled by the rubber gasket not
creating a proper seal any more. I noticed a few drips of coffee running down
the plunger during the extraction phase, and when I pressed it down upon my cup,
the air in the chamber came out upwards around the gasket instead of down
through the filter.

I went and researched replacement gaskets, and they're only \$10 on
aeropress.com. But since we're quite the "zero waste" family, I asked my wife if
she had another idea. She did!

The first solution is to "massage" the gasket, ideally right after brewing when
it's still warm. Simply press the gasket down on a hard surface, and rotate the
plunger at a slightly tilted angle to exercise the edge of the gasket. This
helps counteract the shrinkage that occurs over weeks of use.

Alternatively, put the plunger in the dish washer once in a while. The heat
makes the gasket expand and get back into shape. However, I wouldn't recommend
putting the chamber piece in the dish washer with the plunger, because it'll
lose the markings printed on it.

Thanks to these two simple methods, I can enjoy my daily coffee routine again
without any mess. And I saved $10 that I can spend on delicious coffee beans.
