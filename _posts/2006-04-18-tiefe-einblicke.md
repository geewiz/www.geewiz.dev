---
layout: post
title: "Tiefe Einblicke"
microblog: false
guid: http://geewiz.micro.blog/2006/04/18/tiefe-einblicke.html
date: 2006-04-18T20:22:06+0200
type: post
url: /2006/04/18/tiefe-einblicke.html
---

<p>Bislang war ich kein gro&szlig;er Fan bemalter W&auml;nde. Aber f&uuml;r solch <a href="http://www.techeblog.com/index.php/amazing-3d-murals/">beeindruckende 3D-Wandgem&auml;lde</a> w&uuml;rde ich sogar richtig Geld ausgeben.</p>
