---
layout: post
title: "Korg-Synthi mit Linux"
microblog: false
guid: http://geewiz.micro.blog/2005/01/24/korgsynthi-mit-linux.html
date: 2005-01-24T20:28:00+0200
type: post
url: /2005/01/24/korgsynthi-mit-linux.html
---

<p>Sieht so aus, als k&ouml;nnte ich Linux bald &uuml;ber 2 ganz verschiedene Tastaturen benutzen: Korgs neuestes Keyboard namens <a href="http://www.korg.com/gear/product_info.asp?A_PROD_NO=OASYS">OASYS</a> nutzt Linux als Betriebssystem, erweitert durch eine propret&auml;re Eigenentwicklung. Es bietet unter anderem 16-Spur HD-Recording, einen 10.4" Touchscreen und einen CD-Brenner. Informationen &uuml;ber Preis und Verf&uuml;gbarkeit liegen noch nicht vor.</p>
<p>(via <a href="http://linux.slashdot.org/article.pl?sid=05/01/21/012244">Slashdot</a>&amp;tid=141&amp;tid=106)</p>
