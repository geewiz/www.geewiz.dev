---
layout: post
title: "Neulich im Supermarkt"
microblog: false
guid: http://geewiz.micro.blog/2004/10/28/neulich-im-supermarkt.html
date: 2004-10-28T02:58:00+0200
type: post
url: /2004/10/28/neulich-im-supermarkt.html
---

<p>Nitrozac von "The Joy of Tech" hat sich ein Produkt einfallen lassen, das in allen amerikanischen Malls erh&auml;ltlich sein sollte.</p>
<p>Anders als das Rhino-Dings von neulich ist das eine echt brauchbare Sache. Denn <a href="http://www.joyoftech.com/joyoftech/joyarchives/610.html">damit</a> k&ouml;nnte wirklich sichergestellt werden, dass die US-Bev&ouml;lkerung sinnvoll auf die kommenden Wahlen vorbereitet ist.</p>
