---
layout: post
title: "Coke unter Beschuss"
microblog: false
guid: http://geewiz.micro.blog/2006/01/07/coke-unter-beschuss.html
date: 2006-01-07T03:30:00+0200
type: post
url: /2006/01/07/coke-unter-beschuss.html
---

<p>Beim rot-weissen Getr&auml;nkekonzern ist die Werbefassade allein nicht <a href="http://blog.jochen-lillich.de/index.php?/archives/6-Armut-in-mehrfachem-Sinn.html">the real thing". Im Oktober habe ich die "Vorw&uuml;rfe gegen den Coca-Cola-Konzern</a> entdeckt und mich entschieden, seine Produkte in Zukunft zu meiden. Dass er ausbeuterisch mit Umwelt und Angestellten umgehen soll, hat sich f&uuml;r den Getr&auml;nkekonzern inzwischen zu einem ernsthaften Imageproblem ausgewachsen.Bereits mehrere US-Universit&auml;ten sind inzwischen in den <a href="http://www.spiegel.de/unispiegel/studium/0">Boykott gegen Coca-Cola</a>,1518,393574,00.html getreten. Bemerkenswert finde ich dabei nicht nur das politische Bewusstsein der amerikanischen Studenten, sondern auch, dass sich hier tats&auml;chlich weitfl&auml;chiger Protest ausgerechnet gegen eine traditionsreiche US-Ikone regt. </p>
<p>Dem Getr&auml;nkeabf&uuml;ller entstehen durch den Boykott noch keine nennenswerten Umsatzeinbu&szlig;en, aber ich bin gespannt, wann dem Konzern eine bessere Reaktion einf&auml;llt, als nur stur die Vorw&uuml;rfe von sich zu weisen.</p>
