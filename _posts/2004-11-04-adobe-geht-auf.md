---
layout: post
title: "Adobe geht auf Linux zu"
microblog: false
guid: http://geewiz.micro.blog/2004/11/04/adobe-geht-auf.html
date: 2004-11-04T21:39:00+0200
type: post
url: /2004/11/04/adobe-geht-auf.html
---

<p>Wie <a href="http://www.pro-linux.de/news/2004/7457.html">Pro-Linux</a> meldet, ist <a href="http://www.adobe.com/">Adobe</a> den OSDL(Open Source Development Labs) beigetreten.</p>
<p>Dass Adobe diesen Schritt tun w&uuml;rde, h&auml;tte ich nicht geglaubt. Obwohl sich viele Linux-Anwender die Finger nach Anwendungen wie Photoshop, Freehand oder Indesign lecken w&uuml;rden, hat sich Adobe lange, lange Zeit von Linux fern gehalten. Mit dem Betatest einer Linux-Version von Framemaker weckte der Layout-Spezialist erst gro&szlig;e Hoffnungen, die dann aber nach kurzer Zeit mit dem Abbruch des Tests auch wieder begraben wurden.</p>
<p>Inzwischen scheint man aber von den Vorteilen freier Software &uuml;berzeugter zu sein: "Von einer technischen Sicht der Dinge gesehen ist die (Linux-) Plattform robust." Und es wird sogar nach einem "Director, Linux Market Development" und einem "Linux Desktop Architect" gesucht.</p>
<p>Guten Morgen, Adobe! Und willkommen in der Linux-Welt!</p>
