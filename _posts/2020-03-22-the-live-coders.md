---
layout: post
title: "The Live Coders Conference"
microblog: false
guid: http://geewiz.micro.blog/2020/03/22/the-live-coders.html
date: 2020-03-22T18:21:00+0200
type: post
categories:
- "DevOps"
- "DEV"
tags: [devops]
url: /2020/03/22/the-live-coders.html
---
There won't be many conferences in the next few weeks, at least not the sort that you need to attend in person. However, with COVID-19 handing out lemons, people start to discover the lemonade of online conferences! 

I'm happy to be part of an initiative by The Live Coders, a community of people broadcasting their software development on Twitch. On 9th April, we're going to broadcast more than 12 hours of presentations on a wide range of topics. With my talk about burnout prevention, I'm probably going to have a slot in the (European) afternoon.

For details, check out [The Live Coders Conference](https://conf.livecoders.dev/). Andy, one of the organisers, also did a write-up on [Setting up an online conference](https://dev.to/luckynos7evin/setting-up-an-online-conference-part-1-2o).
