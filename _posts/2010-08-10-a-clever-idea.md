---
layout: post
title: "A clever idea to keep customers up to date"
microblog: false
guid: http://geewiz.micro.blog/2010/08/10/a-clever-idea.html
date: 2010-08-10T21:02:41+0200
type: post
images:
- /assets/wp-content/2011/05/screen_shot_2010-08-10_at_20-2.png?w=300
photos:
url: /2010/08/10/a-clever-idea.html
---

<div class='p_embed p_image_embed'>
<a href="/assets/wp-content/2011/05/screen_shot_2010-08-10_at_20-2.png"><img alt="Screen_shot_2010-08-10_at_20" height="382" src="/assets/wp-content/2011/05/screen_shot_2010-08-10_at_20-2.png?w=300" width="500" /></a>
</div>
<p><a href="http://wufoo.com">Wufoo</a> greets customers who haven't logged in for some time with a list of interesting new developments.</p>
