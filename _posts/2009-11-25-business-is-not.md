---
layout: post
title: "Business is not only about the model"
microblog: false
guid: http://geewiz.micro.blog/2009/11/25/business-is-not.html
date: 2009-11-25T04:06:44+0200
type: post
images:
- /assets/wp-content/2011/05/media_httpwwwjochenli_gcmsf-scaled500.jpg?w=300
- /assets/wp-content/2009/11/media_httpwwwjochenli_ipavd-scaled500.jpg?w=300
- /assets/wp-content/2011/05/media_httpwwwjochenli_cawew-scaled500.jpg?w=300
photos:
- /assets/wp-content/2011/05/media_httpwwwjochenli_gcmsf-scaled500.jpg?w=300
- /assets/wp-content/2009/11/media_httpwwwjochenli_ipavd-scaled500.jpg?w=300
- /assets/wp-content/2011/05/media_httpwwwjochenli_cawew-scaled500.jpg?w=300
url: /2009/11/25/business-is-not.html
---

<p>I just came across Johannes Kleske's recommendation</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwjochenli_gcmsf" height="177" src="/assets/wp-content/2011/05/media_httpwwwjochenli_gcmsf-scaled500.jpg?w=300" width="400" />
</div></p>
<p>and so I took a quick look at the web site of his recommendation, "<a href="http://www.businessmodelgeneration.com/">Business Model Generation</a>". The header says:</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwjochenli_ipavd" height="140" src="/assets/wp-content/2009/11/media_httpwwwjochenli_ipavd-scaled500.jpg?w=300" width="400" />
</div></p>
<p>Because I highly trust Johannes' book taste, I immediately ordered it. But after going through the PayPal transaction process, I got:</p>
<p>
<div class='p_embed p_image_embed'>
<img alt="Media_httpwwwjochenli_cawew" height="103" src="/assets/wp-content/2011/05/media_httpwwwjochenli_cawew-scaled500.jpg?w=300" width="400" />
</div></p>
<p>What's the lesson here? <em>Business model</em> is only one part of the equation. The most important part, though, is <em>business execution</em>. </p>
<p>Your claim and your message may be as great as it can be. If you fail at delivery, you're out. (Not that I didn't have to learn this the hard way myself.)</p>
<p>I hope that I'll get the book anyhow and its content trumps their transaction handling.</p>
