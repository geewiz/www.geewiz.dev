---
layout: post
title: "Remote access"
microblog: false
guid: http://geewiz.micro.blog/2006/01/02/remote-access.html
date: 2006-01-03T00:54:25+0200
type: post
url: /2006/01/02/remote-access.html
---

<p>Beim Einkaufen entdeckte ich vorhin im Tchibo-Regal eine "universal-fernbedienung" der Hausmarke TCM. Den Versuch, meine Fernbedienungssammlung zu zentralieren, habe ich vor ein paar Jahren schon mal gemacht. Die "One for All" war nicht billig, verstaubt aber inzwischen, weil sie nichts mit DVD-Playern anfangen kann. Das Tchibo-Modell kostete nur 10 Euro und kannte angeblich alle relevanten Marken, warum also nicht einen neuen Versuch starten?</p>
<p>Und was soll ich sagen? Nach 10 Minuten hatte ich alle meine Ger&auml;te erfolgreich programmiert! Die paar Funktionen, die die neue nicht auf Anhieb konnte, brachte ich ihr per Lernfunktion mit dem Original bei.</p>
<p>Das ist f&uuml;r mich der Unterschied zwischen "billig" und "preiswert".</p>
