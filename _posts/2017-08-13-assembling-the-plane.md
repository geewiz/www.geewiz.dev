---
layout: post
title: "Assembling the plane on the way down"
microblog: false
guid: http://geewiz.micro.blog/2017/08/13/assembling-the-plane.html
date: 2017-08-13T18:43:45+0200
type: post
url: /2017/08/13/assembling-the-plane.html
---
<p>Managing people is an ability that requires practice and learning, just as any other. As the German proverb goes, "No master ever just fell from the sky."</p>
<p>Regardless of how much you've thought about the topic, how much you've read about it or even taken courses, it's a fact that no amount of theory can compensate a lack of management practice. As Jason Fried says in <a href="https://m.signalvnoise.com/on-being-a-bad-manager-e56e1fb3d9dc">"On being a bad manager"</a>:</p>
<blockquote><p>"Sure, you&rsquo;ve listened to music for decades. But your first day on guitar sucks. Just like you may have watched people be managed &mdash; and you were likely managed yourself. That doesn&rsquo;t prepare you to pick up the management instrument and strum a beautiful melody."</p></blockquote>
<p>People are messy. That's why leading people is messy, too.</p>
<p>The problem with getting better at management is that there's no <code>--dry-run</code> option. It's like learning the guitar on stage. You'll get better over time but it comes with screwing things up in public, getting critical (or even devastating) feedback, and leaving the place feeling ashamed for not meeting your own expectations.</p>
<p>Getting better as a manager is like assembling the plane after you've already jumped off the cliff. You might land as a master. Or crash spectacularly.</p>
<p>There are people who are willing and able to deal with this kind of challenge. They're the right candidates for switching from being an individual contributor to a management position. For all the others (probably the majority), we'll have to provide other avenues for growth.</p>
