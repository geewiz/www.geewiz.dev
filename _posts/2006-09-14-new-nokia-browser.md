---
layout: post
title: "New Nokia browser 2.0"
microblog: false
guid: http://geewiz.micro.blog/2006/09/14/new-nokia-browser.html
date: 2006-09-14T21:41:11+0200
type: post
url: /2006/09/14/new-nokia-browser.html
---

<p>At CTIA Fall 2006, Nokia released <a href="http://www.phonescoop.com/articles/ctia_2006_fall/index.php?p=nb.">version 2.0 of their web browser for S60 phones</a></p>
<p>I've already been amazed how well the browser works, and now Nokia has improved it even more:</p>
<ul>
<li>The browser now integrates WAP functions, so there are no more two separate applications.</li>
<li>Browser pages can be saved for offline viewing.</li>
<li>Clicking on empty space displays a context menu (for example with a list of recently visited pages).</li>
<li>Auto-detection of RSS and ATOM feeds has been added.</li>
<li>Feed contents are displayed with the site's favicon and images included in the entry.</li>
<li>Intervals can be specified for automatic feed updates.</li>
</ul>
<p>Two of the few things that are bugging me in the current version are that the browser lacks form autocompletion and storing of access credentials. Version 2.0 removes those flaws, too.</p>
<p>Seems like Nokia is becoming a provider of great mobile phone software, additional to their already successful cell phone hardware business.</p>
