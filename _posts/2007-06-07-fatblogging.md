---
layout: post
title: "Fatblogging: 97"
microblog: false
guid: http://geewiz.micro.blog/2007/06/07/fatblogging.html
date: 2007-06-07T07:47:28+0200
type: post
url: /2007/06/07/fatblogging.html
---

<p>Another kilo has vanished! Should I test if, in fatblogging, weight loss is proportional to entry length?</p>
<p>It's almost as if my mind is losing weight with my body: the <a href="http://www.guenter-antwortet.de">G&uuml;nter</a> book made me realize that my weaker self has much more control over myself than is good for me. But since I learned about the dangerous interplay between carbohydrates, fat, Insulin and glycogen, I choose my diet much more carefully. I eat wholemeal bread instead of croissants, low-fat cream cheese instead of camembert, and fruits instead of sweets. It's a really liberating feeling to wrest more and more control from G&uuml;nter, my weaker self. The 10 Euro for the book were one of my best investments this year.</p>
<p>Today, I ran a different route that has a fewer inclination and everything went according to my buildup plan (now three-minute runs with one minute of walking in between). I found a beautiful way along a little brook under trees, only three minutes from our house! It's slightly uphill, but very managable. Since there aren't many people out at 5:30, it's just me and nature. Maybe I should leave the iPod at home.</p>
