---
layout: post
title: "Advent, Advent, ..."
microblog: false
guid: http://geewiz.micro.blog/2005/11/27/advent-advent.html
date: 2005-11-27T22:00:20+0200
type: post
url: /2005/11/27/advent-advent.html
---

<p>...nicht nur ein Lichtlein brennt: Sollte ich morgen nach einem Besuch im Baumarkt an unserem Haus herumklettern, dann baue ich gerade <a href="http://www.engadget.com/entry/1234000263069055/">diese Weihnachtsbeleuchtung</a> nach!</p>
<p>Es lohnt sich, f&uuml;r das Video die Lautst&auml;rke ordentlich aufzudrehen. Unglaublich cool.</p>
