---
layout: post
title: "Welcome to Cat Central"
microblog: false
guid: http://geewiz.micro.blog/2005/01/01/welcome-to-cat.html
date: 2005-01-02T01:27:00+0200
type: post
url: /2005/01/01/welcome-to-cat.html
---

<p>Daran, dass mich eine Katze aus der Nachbarschaft st&auml;ndig besucht, habe ich mich inzwischen gew&ouml;hnt. Sie ist unheimlich zutraulich und liebesbed&uuml;rftig und weiss das auch mitzuteilen. Ungeachtet meiner Katzenallergie signalisiert sie mir aufdringlich und unmissverst&auml;ndlich: "Streichel mich!" Und ich kann nicht anders, ich muss es einfach tun. Wenn sie dann zufrieden ist, legt sie sich irgendwo in meine N&auml;he und schl&auml;ft.</p>
<p>Dass man sich bei mir als Katze wohl f&uuml;hlen kann, scheint sich inzwischen in der Gegend rumzusprechen. Denn in den letzten zwei Tagen hatte ich 2 weitere Katzen im Wohnzimmer, als ich die Balkont&uuml;r zum L&uuml;ften offen lie&szlig;.</p>
<p>Schade -- diese nette Gesellschaft werde ich vermissen, wenn ich demn&auml;chst nach Philippsburg ziehe.</p>
