---
layout: post
title: "Sachen gebacken bekommen"
microblog: false
guid: http://geewiz.micro.blog/2006/03/22/sachen-gebacken-bekommen.html
date: 2006-03-22T02:30:05+0200
type: post
url: /2006/03/22/sachen-gebacken-bekommen.html
---

<p><a href="http://sushee.geekheim.de/?page_id=404">Faule Ausreden skalieren besser als IBM Maschinen!" stellt Su-Shee in ihrem "Anti-Verpeil-Howto</a> fest. Und Selbstgespr&auml;che wie "Das mach ich morgen.", "Hm, ich muss aber erst mal aufr&auml;umen." und "Heute ist erst mal Party angesagt, ich hab schlie&szlig;lich nicht nur ein schulisches Leben" sind typische Vertreter daf&uuml;r.</p>
<p>"Hallo, mein Name ist Jochen, und ich bin ein Verpeiler." Ja, in vielen Symptomen, die in dieser toll geschriebenen Anleitung zur Selbstorganisation aufgelistet werden, erkenne ich mich wieder. Manches davon habe ich inzwischen dank wachsender Disziplin im Griff, manches f&auml;llt mir immer noch mehr oder weniger regelm&auml;&szlig;ig auf die F&uuml;&szlig;e.</p>
<p>Das Anti-Verpeil-Howto k&ouml;nnte man als deutschsprachiges "Getting Things Done" bezeichnen. Su-Shee zeigt die &uuml;blichen Muster des Verpeilt-Seins auf und erkl&auml;rt, wie man sie in den Griff bekommt. Sehr zu empfehlen f&uuml;r alle Mit-(Zu)-Vielen-B&auml;llen-Jonglierer, Nie-Rechtzeitig-Fertig-Werder und St&auml;ndig-Zu-Sp&auml;t-Kommer!</p>
