---
layout: post
title: "World Wide Werkzeug"
microblog: false
guid: http://geewiz.micro.blog/2006/03/01/world-wide-werkzeug.html
date: 2006-03-01T02:25:00+0200
type: post
url: /2006/03/01/world-wide-werkzeug.html
---

<p>Seit ich nahezu &uuml;berall Zugang zum Internet habe, werde ich mehr ein Fan von Internet-basierten Werkzeugen: ich telefoniere &uuml;ber <a href="http://www.skype.com">Skype</a>, verwalte meine Softwareprojekte auf <a href="http://www.berlios.de">BerliOS</a> und speichere meine Links auf <a href="http://del.icio.us/Geewiz.">Del.icio.us</a></p>
<p>&Uuml;ber ihr Weblog bin ich heute auf die Firma <a href="http://www.37signals.com/">37signals</a> gesto&szlig;en, deren Produkte sich genau an Leute wie mich richten.37signals entwickelt Web-basierte Organisationshilfen:</p>
<ul>
<li>
<a href="http://www.basecamphq.com/">Basecamp</a> dient der Projektorganisation und bietet Werkzeuge zur Planung und Zusammenarbeit.</li>
<li>"Campfire:"[www.campfirenow.com](http://www.campfirenow.com) ist ein Chatdienst f&uuml;r Teams.</li>
<li>
<a href="http://backpackit.com/?referrer=BPBMZZ9">Backpack</a> unterst&uuml;tzt die Selbstorganisation durch Notizfelder, Linklisten, Bildersammlungen usw. Die unterste Leistungsvariante ist kostenlos.</li>
<li>
<a href="http://www.writeboard.com/">Writeboard</a> ist eine kostenlose Art Mini-Wiki f&uuml;r die gemeinsame Textbearbeitung und kann mit Backpack kombiniert werden.</li>
<li>
<a href="http://www.tadalist.com/">Ta-da-list</a>, ebenfalls kostenlos, erlaubt die Verwaltung von Listen mit Aufgaben, Vorhaben, Eink&auml;ufen usw.</li>
</ul>
<p>Alle genannten Web-Anwendungen zeichnen sich durch gef&auml;lliges Design und den vern&uuml;nftigen Einsatz moderner Technologien wie DHTML und RSS aus. Wo es sinnvoll ist, kann man weiteren Personen Zugriff auf die gespeicherten Informationen gew&auml;hren, damit sie Einblick nehmen oder gar mitarbeiten k&ouml;nnen.</p>
<p>Als "Ich leb' online"-Mensch bin ich von den Produkten sichtlich angetan[1], weil mir f&uuml;r sie auf Anhieb viele praktische Anwendungsf&auml;lle einfallen. Zun&auml;chst werde ich mal die kostenlosen Angebote ausprobieren, um dann zu entscheiden, ob ich f&uuml;r manche Hilfe sogar Geld ausgeben m&ouml;chte.</p>
<p>fn1. Ausserdem finde ich es nett ironisch, dass die meisten Namen aus dem Outdoor-Bereich stammen.</p>
