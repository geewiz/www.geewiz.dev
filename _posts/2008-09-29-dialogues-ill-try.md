---
layout: post
title: "Dialogues I'll try to avoid"
microblog: false
guid: http://geewiz.micro.blog/2008/09/29/dialogues-ill-try.html
date: 2008-09-30T01:58:59+0200
type: post
url: /2008/09/29/dialogues-ill-try.html
---

<p>He:</p>
<blockquote class="posterous_medium_quote">
<p>Please never let me vegetate in such a situation -- dependent on machines, fed from a bottle of fluid nutriment. If you experience me in that condition, have the mercy to shut off the machines keeping me alive.</p>
</blockquote>
<p>She:</p>
<blockquote class="posterous_short_quote">
<p>Well, I'll rather throw away the telly and the computer and pour away your beer then.</p>
</blockquote>
<p>(Via <a href="http://www.thilo-baum.de/lounge/alltagsphilosophie/patientenverfuegung/">Thilo Baum Lounge - Patientenverf&uuml;gung</a>)</p>
