---
layout: post
title: "Warum Nerds unpopul&auml;r sind"
microblog: false
guid: http://geewiz.micro.blog/2004/12/13/warum-nerds-unpopulaumlr.html
date: 2004-12-13T22:48:00+0200
type: post
url: /2004/12/13/warum-nerds-unpopulaumlr.html
---

<p>Auf Wired findet sich ein kleiner <a href="http://www.wired.com/wired/archive/12.12/view.html">Artikel</a>, den ich ganz gut nachempfinden kann.</p>
<p>Worin ich mich vor allem wiederfinde, ist der folgende Absatz:</p>
<blockquote class="posterous_medium_quote">
<p>"Of course I wanted to be popular. But, in fact, I didn't - not enough. There was something else I wanted more: to be smart. Not simply to do well in school, though that counted for something, but to design beautiful rockets, or to write well, or to understand how to program computers. In general, to make great things."</p>
</blockquote>
<p>p. "Gro&szlig;e Dinge" zu tun war schon immer mein st&auml;rkster Antrieb. Das Problem dabei ist nur, neben den gro&szlig;en Dingen die wichtigen kleinen nicht aus den Augen zu verlieren. Das misslingt mir gelegentlich immer noch. Ich bin sehr froh und von Herzen dankbar, dass es Leute gibt, die mir helfen, das in Balance zu bekommen.</p>
