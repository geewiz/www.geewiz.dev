---
layout: post
title: "Es regt sich etwas"
microblog: false
guid: http://geewiz.micro.blog/2006/06/07/es-regt-sich.html
date: 2006-06-07T22:30:03+0200
type: post
url: /2006/06/07/es-regt-sich.html
---

<p>In Berlin wird am 17. Juni eine <a href="http://www.freiheitstattsicherheitswahn.de/.">Protestkundgebung gegen die drohende Total&uuml;berwachung durch den Gro&szlig;en Bruder und seine kleinen Geschwister in der Wirtschaft" stattfinden. Ihr Motto lautet "Freiheit statt Sicherheitswahn</a></p>
<p>B&uuml;rgerrechtsgruppen wie der <a href="http://initiative.stoppt-die-vorratsdatenspeicherung.de/">Arbeitskreis Vorratsdatenspeicherung</a>, Attac, der Chaos Computer Club (CCC), das Forum InformatikerInnen f&uuml;r Frieden und gesellschaftliche Verantwortung, das Netzwerk Neue Medien, der FoeBuD und der FFII wollen mit der Demonstration ein Zeichen gegen die immer weiter um sich greifende Verletzung der informationellen Selbstbestimmung deutscher B&uuml;rger aufmerksam machen. Diese Total&uuml;berwachung bringe "enorme Missbrauchs- und Fehlerrisiken" mit sich, erkl&auml;ren die Veranstalter.</p>
<p>Ein zweiter Punkt, den die B&uuml;rgerrechtler aufgreifen, ist das fehlende Problembewusstsein der B&uuml;rger zu diesem Thema. Deshalb wendet sich die Demonstration auch gegen die Entstehung einer "unkritischen Konsumgesellschaft von Menschen, die 'nichts zu verbergen' haben und vom Staat die Gew&auml;hrleistung totaler Sicherheit fordern, koste es, was es wolle".</p>
<p>Ich finds klasse, dass sich der Widerstand gegen den &Uuml;berwachungsstaat immer mehr formiert.</p>
