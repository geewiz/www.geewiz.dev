---
layout: post
title: "Netzsperren - \"Ihnen ist egal, was wir denken\" | ZEIT ONLINE"
microblog: false
guid: http://geewiz.micro.blog/2009/06/25/netzsperren-ihnen-ist.html
date: 2009-06-25T16:49:26+0200
type: post
url: /2009/06/25/netzsperren-ihnen-ist.html
---

<div class="posterous_bookmarklet_entry">
<blockquote class="posterous_short_quote"><p>Wo beginnt Zensur im Netz? Internetaktivistin Franziska Heine und Familienministerin Ursula von der Leyen streiten &uuml;ber die Stoppschilder vor Kinderpornoseiten</p></blockquote>
<div class="posterous_quote_citation">via <a href="http://www.zeit.de/online/2009/26/leyen-heine-netzsperren">zeit.de</a></div>
<p>Franziska Heine bestreitet die Diskussion hervorragend!</p>
</div>
