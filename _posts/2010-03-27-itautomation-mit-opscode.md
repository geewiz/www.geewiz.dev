---
layout: post
title: "IT-Automation mit Opscode Chef"
microblog: false
guid: http://geewiz.micro.blog/2010/03/27/itautomation-mit-opscode.html
date: 2010-03-27T10:22:58+0200
type: post
url: /2010/03/27/itautomation-mit-opscode.html
---

<div class="posterous_bookmarklet_entry">[blip.tv [blip.tv/play/g8sR...](http://blip.tv/play/g8sRgcqKOQA%2Em4v)]</p>
<div class="posterous_quote_citation">via <a href="http://www.building43.com/videos/2010/03/11/opscode-gives-small-development-firms-enterprise-level-it/">building43.com</a></div>
<p>In diesem Interview von Robert Scoble erz&auml;hlen John Willis und Seth Chisamore, wie die freie Software <a href="http://www.opscode.com/chef">Chef</a> die t&auml;gliche Systemadministration vereinfacht.</p>
</div>
