---
layout: post
title: "Weeknote #-4 (week 10, 2010)"
microblog: false
guid: http://geewiz.micro.blog/2010/03/16/weeknote-week.html
date: 2010-03-16T19:18:24+0200
type: post
categories:
- "Weeknotes"
tags: [weeknote]
url: /2010/03/16/weeknote-week.html
---
<br />
<h3>Business planning</h3>
<p>Much time this week went into the business plan for Freistil-Consulting. When I tweeted about it, <a href="http://twitter.com/jkleske/status/10262169432">@jkleske</a> promptly teased me: "business plan? haven't you read Rework yet? ;-)" What he meant was that, according to 37Signals' latest business book, "planning is guessing". And the numbers I use in the business plan actually are vague projections for the next three years. But first: I do have to present a business plan with a long term financial perspective to get state subsidies. And, more important: By replacing those guesses with the actual numbers, I'll get my first financial controlling instrument for the company. And boy, do I wish I had such a thing while running my previous businesses!</p>
<p>BTW, Johannes, I started reading "Rework" this morning. And I look forward to reading your very own <a href="http://www.johanneskleske.com/blog/starting-week-notes.html">weeknotes</a>!</p>
<h3>IT Infrastructure</h3>
<p>I'm having fun playing with all those open source solutions that enable us to run an IT infrastructure business. There's <a href="http://www.opscode.com/chef">Chef</a> for automation, <a href="http://www.gluster.org/">GlusterFS</a> for data replication, <a href="http://olivier.sessink.nl/jailkit/">JailKit</a> for securing customer access, and so much more. I really enjoy learning to use (utilize, even!) those tools for the lean operation of our IT.</p>
<p>Already after some hours of Chef hacking, I'm able to have a Drupal server running in under 5 minutes, from launching the EC2 instance over installing the necessary packages and configuring user access to starting all the services. Thank you, Opscode!</p>
