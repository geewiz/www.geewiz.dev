---
layout: post
title: "Great earbuds that suck: AKG K 324 P"
microblog: false
guid: http://geewiz.micro.blog/2007/06/19/great-earbuds-that.html
date: 2007-06-19T04:32:36+0200
type: post
url: /2007/06/19/great-earbuds-that.html
---

<p>When I started using my iPod for running, I bought the <a href="http://www.sennheiser.com/uk/icm.nsf/root/500499">Sennheiser OMX 70</a> headphones because they're made especially for athletes. They have adjustable clip-on earphones and are sweat- and water-resistant. But over time, I discovered two problems. First, they're not as rugged as they should be. A few weeks ago, one of the cables got twisted and tore open right at the earphone. And yesterday, the grip of the plug got loose.</p>
<p>I already had a replacement, because there's a second issue I had with the OMX 70: because the earbuds hang outside over the ears, not inside the ears, they don't irritate them -- but they irritate the people around me on the train if I turn the volume up too much. And I hate being an annoyance that people have to ask to please turn down my music.</p>
<p>That's why I bought the <a href="http://p.jochen-lillich.de/amazon/B000ISP3QA">AKG Acoustics K 324 P</a> in-ear headphones. They were advertised as having a great sound and effectively blocking outside noise. And that's true. When I made my first steps outside with them on, I almost walked into a car because I couldn't hear it coming. (I normally rely quite heavily on my hearing when I'm in traffic.) If you press them slightly into your ear canal (and there are three different sets of rubber buds to fit many ears), outside noise gets almost completely blocked, and on the other hand, the music only goes into your head, not into the environment. And when I apply a bit more pressure, I even get the feeling that there's a kind of suction that keeps the earbuds tight in the ear.</p>
<p>There could even be an effect on battery life, because while with the Sennheisers I had to fullly turn up the iPod when I was listening to a podcast in traffic, the AKG earbuds get really loud already at the half setting.</p>
<p>After the first day of using the K 324 P as an Environmental Stupidity Shield on the train and in the office, I can really recommend them. Let's hope they last longer than their predecessors.</p>
