---
layout: post
title: "Upgrading from Kubuntu \"Dapper Drake\" to \"Edgy Eft\""
microblog: false
guid: http://geewiz.micro.blog/2007/06/18/upgrading-from-kubuntu.html
date: 2007-06-18T23:50:00+0200
type: post
url: /2007/06/18/upgrading-from-kubuntu.html
---

<p>When it was time to upgrade my work laptop to a newer Linux distribution some months ago, I decided to install Kubuntu 6.06. I've been using the "Dapper Drake" release on my web servers for more than half a year now and never had any problems. The combination of Debian tools and Ubuntu application repositories is awesome.</p>
<p>When I realized on Friday that some applications only come in packages for newer Ubuntu versions, I decided to ditch the Long Term Support release and try to upgrade to Kubuntu "Edgy Eft" 6.10. At first, I wasn't sure if I'd regret starting an upgrade on an Friday afternoon, but soon gave in to the challenge.</p>
<p>Having a really big pipe at work has its advantages. After issuing the <code>apt-get dist-upgrade</code>, it took exactly 3 minutes and 31 seconds to download the 1002MB of upgrade packages. Installing them took about an hour and went without a flaw, but when the system finally rebooted, I got a mild shock that my encrypted home volume (that I didn't take the time to backup, of course) didn't get mounted. A little web search revealed that the kernel options "quiet" and "splash" prevented the passphrase necessary to decrypt the data from being displayed. Removing both options from <code>/boot/grub/menu.lst</code> took away the pretty boot splash screen but the problem, too.</p>
<p>My desktop looks a a whole lot more shiny now. I could finally install Firefox 2 from the official repositories, and when it first encountered a Flash applet, I was surprised to be able to download and activate the plugin successfully with just one mouse click. I was used to having to do a manual download and installation. As easily, I replaced GAIM with its successor Pidgin, so my online life has an up-to-date foundation again. WiFi and Bluetooth continued working without a change.</p>
<p>This upgrade went even faster and more smoothly than I had expected. You just have to love Ubuntu Linux. Knowing myself, it probably won't be long until I make the next step up to "Feisty Fawn".</p>
