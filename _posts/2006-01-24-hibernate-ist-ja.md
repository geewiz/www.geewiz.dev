---
layout: post
title: "Hibernate ist ja sowas von faul"
microblog: false
guid: http://geewiz.micro.blog/2006/01/24/hibernate-ist-ja.html
date: 2006-01-24T21:51:00+0200
type: post
url: /2006/01/24/hibernate-ist-ja.html
---

<p>Das O/R-Mapping von Hibernate ist eine feine Sache, insbesondere bei Relationen: Hat eine Klasse <code>Schrank</code> eine 1:n-Beziehung mit einer Klasse <code>Schublade</code>, dann liefert mir <code>einSchrank.getSchubladen()</code> eine Collection von Objekten der Klasse <code>Schublade</code>. So sieht es jedenfalls aus, aber das muss man mit Vorsicht genie&szlig;en.In Wirklichkeit handelt es sich um Proxy-Objekte, die mir Hibernate unterschiebt. Diese Objekte tun nur so, als w&auml;ren sie Schubladen. Das Framework muss n&auml;mlich vermeiden, durch das Aufl&ouml;sen von Relationen im Endeffekt die gesamte Datenbank einzulesen. Ein gutes Beispiel daf&uuml;r ist ein Familienstammbaum, in dem alle Personen durch Eltern-, Kind- oder sonstige Relationen verbunden sind. W&uuml;rde Hibernate die alle aufl&ouml;sen, nur weil ich von einer Person den Namen haben will, h&auml;tte ich sofort die ganze Familiengeschichte am Hals.</p>
<p>Deshalb realisiert Hibernate mittels der erw&auml;hnten Proxyobjekte eine sog. "Lazy initialization": Bei der Initialisierung abh&auml;ngiger Objekte schiebt Hibernate das Nachladen der Daten so lange auf, bis sie von der Anwendung wirklich abgefragt werden. Das <code>SELECT</code> auf die Schubladen-Tabelle wird nicht schon bei <code>einSchrank.getSchubladen</code> abgesetzt, sondern erst, wenn ich auf die Daten einer dieser Schubladen zugreife.</p>
<p>Und da wartet eine Falle: Dieses "Nachladen auf den letzten Dr&uuml;cker" muss in der gleichen Session passieren wie das Laden des &uuml;bergeordneten Objekts. Wurde die Session nach <code>getSchubladen</code> geschlossen, k&ouml;nnen die Daten einzelner Schubladen nicht mehr abgefragt werden. Hibernate wirft dann eine LazyInitializationException.</p>
<p>Dieses Problem l&ouml;st man am besten durch eine gekapselte Sessionverwaltung, die f&uuml;r die Anwendung transparent eine Hibernate-Session erzeugt und diese am Leben h&auml;lt.</p>
