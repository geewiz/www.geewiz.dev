---
layout: post
title: "Confixx auf Debian GNU/Linux"
microblog: false
guid: http://geewiz.micro.blog/2007/09/24/confixx-auf-debian.html
date: 2007-09-24T22:04:16+0200
type: post
url: /2007/09/24/confixx-auf-debian.html
---

<p>(This is a German article from my old homepage.)</p>
<h2>Apache</h2>
<p>Das <code>suexec</code> Kommando, durch das Apache CGI-Programme unter sicherheitstechnischen Einschr&auml;nkungen laufen l&auml;sst, ist im Originalpaket auf den Pfad <code>/var/www</code> konfiguriert. Confixx legt seine WWW-Homes aber unter <code>/home/htdocs</code> an. Deshalb muss Apache neu erzeugt werden:</p>
<div class="CodeRay">
<div class="code">
<pre># cd /tmp
 # apt-get source apache2
 # cd apache2-*
 # vi debian/rules</pre>
</div>
</div>
<p>In <code>debian/rules</code> muss die Zeile</p>
<div class="CodeRay">
<div class="code">
<pre>--with-suexec-docroot=/var/www </pre>
</div>
</div>
<p>durch</p>
<div class="CodeRay">
<div class="code">
<pre>--with-suexec-docroot=/home/htdocs </pre>
</div>
</div>
<p>ersetzt und danach ein neues Apache-Paket gebaut werden:</p>
<div class="CodeRay">
<div class="code">
<pre># dpkg-buildpackage</pre>
</div>
</div>
<p>Die entstandenen Pakete k&ouml;nnen dann per <code>dpkg -i</code> installiert werden.</p>
<h2>vsftpd</h2>
<p>Der FTP-Server vsftpd muss, wie im folgenden beschrieben, konfiguriert werden, dass er </p>
<ul>
<li>anonyme Logins verbietet,</li>
<li>Logins lokaler Benutzer ohne valide Shell erlaubt,</li>
<li>Schreiben erlaubt und</li>
<li>sinnvolle Dateirechte vergibt.</li>
</ul>
<p>Wichtige Einstellungen in <code>/etc/vsftpd.conf</code>:</p>
<div class="CodeRay">
<div class="code">
<pre>anonymous_enable=NO
 local_enable=YES
 write_enable=YES
 local_umask=022</pre>
</div>
</div>
<p>Wichtige Einstellungen in <code>/etc/pam.d/vsftpd</code>:</p>
<div class="CodeRay">
<div class="code">
<pre>#auth   required        pam_shells.so</pre>
</div>
</div>
<h2>Tests</h2>
<p>Ob alle Scripts sauber ausgef&uuml;hrt werden, l&auml;sst sich mit folgendem Scriptaufruf testen:</p>
<div class="CodeRay">
<div class="code">
<pre>./confixx_counterscript.pl -fa -dbg</pre>
</div>
</div>
<p>Die Funktion des Statistikscripts kann folgenderma&szlig;en getestet werden:</p>
<div class="CodeRay">
<div class="code">
<pre>./httpdtraffik.sh DEBUG</pre>
</div>
</div>
<h2>Weitere Infos</h2>
<ul>
<li>
<a href="http://download1.sw-soft.com/Confixx/ConfixxPro3.1/docs/debian_sarge_setup_guide.txt">Confixx Setup Guide</a> for Debian Sarge</li>
</ul>
