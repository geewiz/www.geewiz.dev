---
layout: post
title: "Going grazy"
microblog: false
guid: http://geewiz.micro.blog/2006/07/16/going-grazy.html
date: 2006-07-16T21:21:44+0200
type: post
url: /2006/07/16/going-grazy.html
---

<p>I just replaced my ever growing blogroll list by a <a href="http://grazr.com/">Grazr</a> panel. </p>
<p>Grazr is a nifty Javascript applet that not only shows every blog I read ordered by category but also lets you browse every blog's entries right on my page. Reducing clutter while increasing functionality? That decision was a no-brainer.</p>
