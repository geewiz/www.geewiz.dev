---
layout: post
title: "The one little thing that makes the difference"
microblog: false
guid: http://geewiz.micro.blog/2007/01/18/the-one-little.html
date: 2007-01-18T19:56:18+0200
type: post
url: /2007/01/18/the-one-little.html
---

<p>In his S60 blog, Tommi explains why <a href="http://blogs.s60.com/tommi/2007/01/google_reader_rocks.html:">Google Reader rocks</a></p>
<blockquote class="posterous_medium_quote">
<p>Google Reader and its mobile version rock. I find myself using it more than any other functionality on my Nokia N73. And I love the fact that I can access the same feeds via work computer, via home computer, and via mobile. In most other RSS readers, the items are stuck in that particular client.</p>
</blockquote>
<p>Having access to your single source of feeds everywhere really is great. There seem to be many great newsreaders like NetNewsWire, but they can't provide me with news when I'm on the train or on the toilet.</p>
<p>It's obvious that Tommi doesn't subscribe to high volume feeds like Engadget or one from a web forum, though. Otherwise, he would know that it's a giant PITA to weed through dozens of product announcements or forum entry headlines on your mobile.</p>
<p>That's why I still stick to <a href="http://www.bloglines.com:">Bloglines</a> I can choose whether a feed shows up in its mobile version. It's a small simple checkbox in the feed settings, but it makes all the difference why in my eyes Google Reader Mobile doesn't rock yet.</p>
