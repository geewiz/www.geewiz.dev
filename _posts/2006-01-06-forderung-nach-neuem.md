---
layout: post
title: "Forderung nach neuem Datenschutzgesetz"
microblog: false
guid: http://geewiz.micro.blog/2006/01/06/forderung-nach-neuem.html
date: 2006-01-07T01:06:43+0200
type: post
url: /2006/01/06/forderung-nach-neuem.html
---

<p>Hab ich schon mal erw&auml;hnt, dass ich Peter Schaar mag? Ich finde, unser Bundesbeauftragter f&uuml;r den Datenschutz nimmt seinen Job sehr ernst und macht ihn gut. In einem <a href="http://www.berlinonline.de/berliner-zeitung/politik/515174.html">Interview der Berliner Zeitung</a> forderte er jetzt ein neues Datenschutzgesetz, das auf die neuen technischen Entwicklungen abgestimmt ist.Was heutzutage &uuml;ber uns Normalb&uuml;rger an Daten gesammelt wird, geht schon lange auf keine Kuhhaut mehr, geschweige denn auf eine handels&uuml;bliche Festplatte: Einkaufsgewohnheiten, Telefonkontakte, Produktvorlieben, Fahrzeugbewegungen, Gesundheitsdaten und vieles mehr wird heute online und IRL(In Real Life) erhoben, gespeichert und ausgewertet. Insbesondere mit den kommenden RFID-Funketiketten werden unsere Bewegungen im Alltag noch viel detaillierter &uuml;berwacht werden.</p>
<p>Wie leider so oft halten unsere Gesetze mit der rapiden Entwicklung der Technik aber nicht Schritt. Deshalb macht sich Schaar f&uuml;r eine Anpassung des Datenschutzgesetzes stark: "Wir brauchen ein neues, umfassendes Datenschutzgesetz. Das alte ist nicht mehr in der Lage, der allgegenw&auml;rtigen Datenflut angemessen zu begegnen. Die Defizite sind gro&szlig; und offensichtlich."</p>
<p>Was mit diesen st&auml;ndig anwachsenden Daten geschieht, ist f&uuml;r den B&uuml;rger l&auml;ngst nicht mehr transparent. Schaar erkl&auml;rt: "Es entsteht eine neue Infrastruktur, bei der immerzu und &uuml;berall Daten verarbeitet werden. Die Frage, wer eigentlich f&uuml;r diese Daten verantwortlich ist, l&auml;sst sich dabei kaum noch beantworten."</p>
<p>Das ist ein besorgniserregende Entwicklung, die schon lange im Gange ist. Ich bin offensichtlich nicht allein in der Wahrnehmung, dass die SPD-Regierung den Datensammlungs - und &Uuml;berwachungskurs ihrer Vorg&auml;ngerin nicht ge&auml;ndert, sondern fortgesetzt hat. Und Schaars Bemerkung, dass unser neuer Big Brother Wolfgang Sch&auml;uble die &Uuml;berwachung auch nicht schlimmer macht als sein Vorg&auml;nger, zeigt, dass hier weiter leider seit vielen Jahren Kontinuit&auml;t herrscht. Denn das Gef&uuml;hl hatte ich schon beim Wechsel von Kanther zu Schily.</p>
<p>Ich hoffe, dass unser oberster Datensch&uuml;tzer mit seinen Bem&uuml;hungen Erfolg hat -- damit das Thema der allgegenw&auml;rtigen &Uuml;berwachung, Kontrolle und Steuerung weiterhin auf unterhaltsame B&uuml;cher und Filme beschr&auml;nkt bleibt.</p>
