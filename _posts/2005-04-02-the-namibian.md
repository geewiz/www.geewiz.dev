---
layout: post
title: "The Namibian"
microblog: false
guid: http://geewiz.micro.blog/2005/04/02/the-namibian.html
date: 2005-04-02T10:52:25+0200
type: post
url: /2005/04/02/the-namibian.html
---

<p>Nachdem im letzten Jahr schon ihre "Alpaca News" aus Peru tollen Anklang fanden, hat Carolin begonnen, auch aus Namibia Reiseberichte zu schicken. Dieses Mal hat sie als Titel "The Namibian" gew&auml;hlt, den Namen einer dortigen Zeitung. Weil das Internet vor Ort schlecht angebunden und teuer ist, schickt sie ihre kurzweiligen Reisenachrichten als E-Mail an Freunde, Verwandte und Bekannte.</p>
<p>Ich bin der Meinung, dass sie es wert sind, archiviert und einer breiteren &Ouml;ffentlichkeit zug&auml;nglich gemacht zu werden. Also habe ich nach R&uuml;cksprache f&uuml;r sie ein <a href="http://www.carolins-tagebuch.de">Weblog</a> eingerichtet.</p>
