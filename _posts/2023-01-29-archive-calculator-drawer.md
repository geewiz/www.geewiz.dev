---
layout: post
title: "archive.org now has a calculator drawer"
date: 2023-01-29 13:45 +0000
description: "You can now use vintage calculators online on archive.org"
image:
category:
tags:
  - retrocomputing
published: true
sitemap: true
---

Today I've learned that the MAME emulator project did a cooperation with the
Internet Archive, Texas Instruments and HP to create software replicas of
historic pocket calculators. The result is the [Calculator
Drawer](https://archive.org/details/calculatordrawer).

While there are emulators for the HP 38G, 48G+ and 48GX, my trusty HP 28S on
whose software and hardware design they are based, that I bought for uni in the
early 90s and am still using, isn't available (yet?). When it'll meet its
inevitable demise, I'll have to make do with "RPN 28x Calc", a lovingly crafted
iOS app.

I learned about MAME as an emulator software for arcade games, but it's much
more than that. The MAME community builds software replicas of all kinds of
vintage hardware, including the Apple II as well as amazing synthesizers such as
the [Fairlight CMI IIx](https://wiki.mamedev.org/index.php/Synthesizers) music
workstation of the 1980s!
