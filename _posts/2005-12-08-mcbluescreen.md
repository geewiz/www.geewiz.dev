---
layout: post
title: "McBluescreen"
microblog: false
guid: http://geewiz.micro.blog/2005/12/08/mcbluescreen.html
date: 2005-12-09T00:50:38+0200
type: post
url: /2005/12/08/mcbluescreen.html
---

<p>Nachdem Kai das <a href="http://blog.ksile.de/index.php?/archives/379-Jetzt-bin-ich-mal-gespannt.html">Wirtshaus zum Goldenen M" schon lange aus Geschmacksgr&uuml;nden ablehnt, vermutet er nun, dass ich "aus religi&ouml;sen Gr&uuml;nden</a> in dessen Boykott treten werde.</p>
<p>Aber da bin ich entspannt. Welches Betriebssystem ein Laden f&uuml;r seine Kassen einsetzt, ist mir herzlich egal[1]. Ohnehin hat es mich zu McDonald's seit meiner Fastenkur im November nicht mehr hingezogen.</p>
<p>fn1. Jedenfalls, solange ich nicht in einer Schlange stehen muss, weil vorne der Bildschirm gerade blau wurde.</p>
