---
layout: post
title: "Devops podcasts"
microblog: false
guid: http://geewiz.micro.blog/2012/05/13/devops-podcasts.html
date: 2012-05-13T22:33:10+0200
type: post
url: /2012/05/13/devops-podcasts.html
---
<p>While there hasn&#8217;t been a shortage on IT engineering blogs for a long time, podcasts that deal with devops topics are a rare sight. That&#8217;s why I&#8217;d like to recommend the ones that I currently subscribe to:</p>
<ul>
<li><a href="http://devopscafe.org/">DevOps Cafe Podcast</a>: Damon Edwards, John Willis and guests talk about interesting news in the datacenter world.</li>
<li><a href="http://thechangelog.com/">The Changelog</a>: A show that &#8220;covers what&#8217;s fresh and new in Open Source&#8221;, hosted by Wynn Netherland and Adam Stacoviak</li>
<li><a href="http://www.foodfightshow.org/">The Food Fight Show</a> is a bi-weekly podcast for the Chef community, or, as hosts Bryan Berry and Matt Ray put it, &#8220;The Podcast where DevOps chefs do battle&#8221;</li>
<li><a href="http://itkanban.com/podcast/">itkanban&#8217;s podcast</a> comes also bi-weekly and covers news about lean and agile IT management methods.</li>
</ul>
<p>Do you know any other podcasts a self-respecting system administrator should listen to? Please post them in the comments!</p>
