---
layout: post
title: "Die Schattenseite Maven-generierter Projektwebsites"
microblog: false
guid: http://geewiz.micro.blog/2006/03/21/die-schattenseite-mavengenerierter.html
date: 2006-03-21T23:06:28+0200
type: post
url: /2006/03/21/die-schattenseite-mavengenerierter.html
---

<p>In <a href="http://www.oreillynet.com/onjava/blog/2006/03/maven_project_info_reports_con.html">Maven Project Info Reports Considered Dangerous</a> l&auml;sst sich Timothy M. O'Brien bitter &uuml;ber die Website mit Projektinformationen aus, die die Site-Funktion von Maven ausspuckt.<br />
Mavens Projekt-Website (ich nutze sie selbst f&uuml;r mein Spielprojekt <a href="http://comatose.berlios.de">COMATOSE</a>) kann nur mit gro&szlig;en Schmerzen individuell gestaltet werden. Ohne diese Gestaltung sieht aber jede Maven-Website wie die andere aus. Zus&auml;tzlich enth&auml;lt jede Maven-Site vorgefertigte Floskeln wie "Get involved today", die in der Masse kaum ernstzunehmen sind.</p>
<p>Will man jedoch diese Missst&auml;nde durch ein eigenes Design beheben, stellt man fest, dass es nicht nur an Eingriffsm&ouml;glichkeiten, sondern auch an Dokumentation fehlt. Timothys harsche Kritik ("Losing all hope; open ridicule") erscheint mir daher durchaus gerechtfertigt.</p>
<p>Sollte mein Projekt mal ernstzunehmende Formen annehmen, werde ich daf&uuml;r auch eine ernstzunehmende Website bauen m&uuml;ssen. Das Werkzeug daf&uuml;r wird aber wohl nicht Maven sein.</p>
