---
layout: post
title: "5 reasons IT projects fail"
microblog: false
guid: http://geewiz.micro.blog/2008/08/15/reasons-it-projects.html
date: 2008-08-15T18:11:14+0200
type: post
url: /2008/08/15/reasons-it-projects.html
---

<p>In <a href="http://blogs.zdnet.com/projectfailures/?p=774">5 ways to prevent IT failure</a>, I found five important reasons that IT projects go down the drain: </p>
<ol>
<li>Lack of preparation.</li>
<li>Business misfit</li>
<li>Unilateral decision making</li>
<li>Inflexibility</li>
<li>Scope creep</li>
</ol>
<p>Boy, do they remind me of some projects I had to endure. Especially the explanation of the last point, scope creep, hits home:</p>
<blockquote class="posterous_short_quote">
<p>Lack of preparation typically begets the kitchen-sink syndrome, where project leaders add in every kind of feature and the kitchen sink to boot.</p>
</blockquote>
<p>A project that doesn't have a clearly defined goal will most certainly be frustrating. And it will hurt the company since it'll be far from effective.</p>
<p>The ZDnet article also talks about what you can do to make sure the problems above don't occur. Let's ask the audience: When you're taking part in projects, what do you do to prevent those project killers?</p>
