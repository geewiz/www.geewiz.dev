---
layout: post
title: "500 Burgers, please. No Coke."
microblog: false
guid: http://geewiz.micro.blog/2007/01/11/burgers-please-no.html
date: 2007-01-12T00:27:14+0200
type: post
url: /2007/01/11/burgers-please-no.html
---

<p>What happens when people organize themselves to make one huge order at McDonald's? </p>
<p><a href="http://500burger.de.vu/">See here</a>!</p>
<p>Maybe, before going to McDonald's the next time, I should google the restaurant first. I'd hate to wait in such a queue.</p>
<p>(Thanks, Andy!)</p>
