---
layout: post
title: "Video-Einladung zum Webinar \"Getting Things Done\""
microblog: false
guid: http://geewiz.micro.blog/2009/08/01/videoeinladung-zum-webinar.html
date: 2009-08-01T18:02:23+0200
type: post
url: /2009/08/01/videoeinladung-zum-webinar.html
---

<div class="posterous_bookmarklet_entry">
<div class="posterous_quote_citation">via <a href="http://www.youtube.com/watch?v=iKAAaGOVnoU">youtube.com</a></div>
<p>Webinar-Homepage: <a href="http://www.freistil-consulting.de/webinar/getting-things-done">http://www.freistil-consulting.de/webinar/getting-things-done</a></p>
<p>Freistil-Twitter: <a href="http://www.twitter.com/freistil">http://www.twitter.com/freistil</a></p>
</div>
