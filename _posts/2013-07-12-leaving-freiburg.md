---
layout: post
title: "Leaving Freiburg"
microblog: false
guid: http://geewiz.micro.blog/2013/07/12/leaving-freiburg.html
date: 2013-07-12T15:00:00+0200
type: post
url: /2013/07/12/leaving-freiburg.html
---
<p>Our days in Freiburg are running out. Over the weekend, we're going to move our remaining belongings to our parents, renovate the flat and hand over the keys. We'll spend the rest of the month at our parents' and prepare for our arrival in Ireland.</p>
<p>It's been seven great years here in southwest Germany, with the amenities of a bigger city and the awesome nature of the Black Forest. We've become a family here, growing from two people loving each other to four (without the love spreading thin at all). Many happy moments connect us with Freiburg and we'll be missing this place.</p>
<p>At this occasion, I'd like to say "good bye" to everyone I've spent a few of these happy moments here, and "thank you". I'm grateful to have met you and wish you all the best!</p>
