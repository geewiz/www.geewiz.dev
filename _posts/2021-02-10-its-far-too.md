---
layout: post
microblog: true
guid: http://geewiz.micro.blog/2021/02/10/its-far-too.html
date: 2021-02-10T13:01:36+0200
type: post
url: /2021/02/10/its-far-too.html
---
It's far too cold to even consider buying a barbecue set. But it's a good time to think about reclaiming the web.

➡ [Why the Indieweb?](https://briefs.video/videos/why-the-indieweb/)
