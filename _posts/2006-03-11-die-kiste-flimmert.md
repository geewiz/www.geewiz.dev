---
layout: post
title: "Die Kiste flimmert wieder"
microblog: false
guid: http://geewiz.micro.blog/2006/03/11/die-kiste-flimmert.html
date: 2006-03-11T07:16:18+0200
type: post
url: /2006/03/11/die-kiste-flimmert.html
---

<p>Heute mittag hab ich mir erst die Batterie meines Autos ersetzen lassen (es ging gar nichts mehr, nicht mal mehr Starthilfe) und dann bin ich gleich zum MediaMarkt gefahren und hab einen Quad-LNB gekauft. Als ich zur&uuml;ckkam, machte der Regen gerade Pause, also hab ich ihn gleich installiert. Nach <a href="http://blog.jochen-lillich.de/archives/280-Sat-gesehen.html">zwei Wochen Abstinenz</a> bekommt meine dbox jetzt wieder Daten.</p>
