---
layout: post
title: "Office for Symbian phones"
microblog: false
guid: http://geewiz.micro.blog/2006/08/27/office-for-symbian.html
date: 2006-08-27T07:51:21+0200
type: post
url: /2006/08/27/office-for-symbian.html
---

<p><a href="http://www.sept-solutions.de/English/office.php">Mobile Office</a> is a new application for smartphones based on Symbian S60. It is an Office suite for the OpenDocument format used by OpenOffice and KOffice. Momentarily, it's still in development.</p>
<p>It can open files with OpenDocument text (.odt), spreadsheet (.ods) and presentation (.odp).</p>
<p>If you want to try it out, there's a <a href="http://www.sept-solutions.de/English/office_download.php">beta version of Mobile Office</a> available for download that'll work until August 31st.</p>
<p>(via <a href="http://blogs.s60.com/tommi/2006/08/mobile_office_in_free_beta.html">Tommi's S60 applications blog</a>)</p>
