---
layout: post
title: "YT channel recommendation for neovim and zsh users"
microblog: false
guid: http://geewiz.micro.blog/2022/01/10/i-dont-think.html
date: 2022-01-10T15:32:00+0200
type: post
categories:
- "DevOps"
- "DEV"
tags: [devops]
url: /2022/01/10/i-dont-think.html
---
I don't think I ever copied more vim or zsh configuration from anyone than from ChrisAtMachine. Looks like I found a soulmate. 😊 His [YouTube channel](https://www.youtube.com/channel/UCS97tchJDq17Qms3cux8wcA) is well worth watching!
