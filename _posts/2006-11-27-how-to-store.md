---
layout: post
title: "How to store passwords safely on Windows, Mac OS X and Linux"
microblog: false
guid: http://geewiz.micro.blog/2006/11/27/how-to-store.html
date: 2006-11-27T23:00:14+0200
type: post
url: /2006/11/27/how-to-store.html
---

<p>If you're anything like me, your user accounts collection keeps growing week by week. All those Web 2.0 applications require a personal account with user name and password. Some let you choose the login name yourself, others generate it automatically from other information you input, e.g. your real name. Passwords make the whole thing even more complicated. But I finally found a solution that puts security in balance with ease of use.<br />
There are many websites that offer advice on choosing a good password. Some of the basic rules are:</p>
<ul>
<li>Don't take a simple word because that can be guessed by working through a dictionary.</li>
<li>Don't use just lower case, and also put some numbers or even interpunction characters in.</li>
<li>Don't use the same password at different places -- if one website gets compromised, so could be all your accounts.</li>
</ul>
<p>The problem is that most hard to find passwords are also hard to remember. There's a way around that problem by deriving the password from an easy to remember sentence. For example, from the initial characters in "Another two pints of Guinness, please", you get "A2poGp". But the number of easy to remember sentences is limited, too.</p>
<p>Because I wanted to get rid of the nagging feeling I got from using the same password over and over, I decided on getting a password management software that securely stores my account collection and lets me easily retrieve them.</p>
<p>Tom Raftery swears by <a href="http://www.tomrafteryit.net/keychain-saves-my-life-and-my-passwords/.">Keychain as password manager</a> But, additional to my Mac Mini, I have an IBM laptop with Linux on it, so I was searching for a cross-platform solution.</p>
<p><img width='250' height='277' style=<a href="http://keepass.sourceforge.net/download.php.">float: right; border: 0px; padding-left: 5px; padding-right: 5px;" src="/uploads/keepass.png" alt="" />I found "KeePass</a> KeePass is available for Windows, MacOS X and Linux and it stores all your login data in a securely encrypted file that you can put on an USB stick. </p>
<p>The USB stick with the KeePass file on belongs to those items I carry around in my backpack all the time. So, if I'm at work, I can plug the thumb drive into my Linux laptop. At home, I plug it into my Mac's USB port. Since KeePass uses the same file format and encryption algorithm on all platforms, I have access to my login data everywhere, just by entering my master password at the KeePass startup screen. (That's the one password I actually have to remember although it's a really tough one.)</p>
<p>KeePass is easy to use and has many sensible functions:</p>
<ul>
<li>Organize all records in a group hierarchy (Web, Web/Email, Web/News, Bank, Bank/CreditCards, ...)</li>
<li>Icons for groups and records</li>
<li>Suggest a new password (length and character set definable)</li>
<li>Display how secure the chosen password is</li>
<li>Enter additional information like a comment or an account expiration date</li>
</ul>
<p>And, keeping the best for last: KeePass is free software. So, what's your lame excuse for still using your girlfriend's name as your single password? ;-)</p>
