---
layout: post
title: "It's the employees that keep innovation alive"
microblog: false
guid: http://geewiz.micro.blog/2007/03/29/its-the-employees.html
date: 2007-03-29T17:35:00+0200
type: post
url: /2007/03/29/its-the-employees.html
---

<p>There's a theory named <a href="http://en.wikipedia.org/wiki/Peter_principle">The Peter Principle</a> that says that, provided that you get promoted because you deliver above-average performance, everyone will finally end up occupying a position they are not fully qualified for any more. Therefore, the real work is done by those who still haven't passed their performance peak.</p>
<p>Nitin Borwankar from the blog <a href="http://walruscarpenter.wordpress.com/2007/03/24/the-peter-principle-of-innovation/">The Walrus and the Carpenter" now applied this principle to a company's innovation. "The Peter Principle of Innovation</a> goes as follows:</p>
<blockquote class="posterous_medium_quote">
<p>Every company innovates until it finds a cash cow. At that point only innovation that supports the cash cow is promoted. Further, any innovation that threatens or does not support the cash cow languishes or is actively killed. Eventually, most of the true innovation ceases as the innovators leave and start new companies and the cycle repeats.</p>
</blockquote>
<p>Most of the time, a company puts a lot of work into competing for a place in the market and into developing a success product. You can't blame management if it then focuses on said product and tries to protect it from detrimental influences, be it from inside the company or external. But every product has its lifespan and at some point the time to let go will come. The market moves on and companies have to keep up. If management misses that time to define and aim at new goals, it will eventually lose its competitive advantage as well as its innovative minds. The Peter Principle of Innovation will set in with full force.</p>
<p>But is it inevitable that this kind of focusing on the cash cow will bring innovation to a halt? I don't think so. Just like it's not a law of nature that we have to get grumpy and closed-minded as we get older, a company doesn't have to lose its innovative drive. It just has to keep the necessary flexibility to adapt successfully to changes.</p>
<p>The key to that kind of flexibility is the workforce, as Kathy Sierra points out in her article <a href="http://headrush.typepad.com/creating_passionate_users/2006/10/knocking_the_ex.html.">Knocking the exuberance out of employees</a> If management prefers lifeless robots, there's not much innovation to expect. If, on the other hand, a company gives its employees enough room to be creative, they will in turn generate the energy to drive innovation. Take Google, for example: People there get the opportunity to spend 20% of their paid time for playing with new ideas. And look how the company has long ago left the confines of the search engine market to reinvent the application service business.</p>
<p>If a company sees far enough to embrace new opportunities, it's because management is standing on the shoulders of giants. Leaders that are aware of this won't have to fear the Peter Principle of Innovation.</p>
