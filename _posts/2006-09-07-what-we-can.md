---
layout: post
title: "What we can learn from Steve Irwin"
microblog: false
guid: http://geewiz.micro.blog/2006/09/07/what-we-can.html
date: 2006-09-07T23:31:55+0200
type: post
url: /2006/09/07/what-we-can.html
---

<p>The death of Crocodile Hunter Steve Irwin caused a great wave of grief and regret around the globe. The blogosphere was buzzing with articles and condolences only hours after the message of his fatal accident. But why is that? What made Steve more loved or admirable than other TV celebrities?</p>
<p>I'd say it's about respect, passion, credibility and fun.<br />
For one, it was the enormous respect for his animals that Steve displayed. He put them in the lead role, not himself. He didn't get weary of pointing out the beauty and diligence mother nature put into her creatures. It wasn't about him, it was all about the animals. He put his health at risk (and it was quite a close call more than once) to give people the opportunity to learn about all those different species, to show his viewers the importance of preserving wildlife for usselves, the "crown of creation". That way, he in turn gained the respect of his audience. </p>
<p>Another thing is credibility. Steve not only put his money where his mouth was, he put himself there. He chose to get out into the wilderness, to go where his beloved creatures lived, instead of transporting them into a safe TV studio where they don't belong. That meant that he was likely to get hurt, but he accepted that. And he always emphasized that such injuries were his fault, not the animals'.</p>
<p>If you ask people why they watch "Crocodile Hunter" time after time, they will probably tell you "It's interesting, and he's funny". But I won't say that he was making fun of animals, I'd say he just had plain fun being with them. Just like I have fun teasing and joking around with friends and colleagues without the intention of making them ridiculous, Steve did with his "colleagues".</p>
<p>Finally, everyone of us could be jealous of how much fun Steve had with his job in general. He didn't hold back in showing how much he enjoyed his work.</p>
<p>I guess that's how he became that australian original that gained a great fan base all around the world by making people appreciate the wonders of nature and respect their fellow creatures. An original that will be missed.</p>
<p>What I learned from Steve Irwin is that while it helps to survive if you just blend in, if you really want to move something, you have to stand out by showing knowledge, respect for others and credibility. And that you can have fun doing so, too.</p>
