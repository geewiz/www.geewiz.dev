---
layout: post
title: "Complaining? You're doing it wrong."
microblog: false
guid: http://geewiz.micro.blog/2013/05/31/complaining-youre-doing.html
date: 2013-05-31T11:40:00+0200
type: post
url: /2013/05/31/complaining-youre-doing.html
---
<p>I'm a stoic. Have been for a long time, will ever be. "Love it, change it, or leave it." That's my motto. I hate listening to people complain. Complain about their job, complain about their neighbours' kids, complain about their car mechanic, complain about the economy, complain about the weather. Please stop it. STFU.</p>
<p>Fortunately, D. Keith Robinson wrote <a href="http://howtomakelightning.com/complaining-a-short-manual">"A Short Manual"</a> on how to effectively stop ineffective complaining. It comes down mostly to being honest with yourself and with the people around you.</p>
