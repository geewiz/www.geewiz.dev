---
layout: post
title: "Skype goes WiFi"
microblog: false
guid: http://geewiz.micro.blog/2006/07/21/skype-goes-wifi.html
date: 2006-07-21T23:39:40+0200
type: post
url: /2006/07/21/skype-goes-wifi.html
---

<p>Skype gets ready to throw several brands of WiFi phones at their users (after they throw some money at Skype, of course). There are four models in the wings:</p>
<ul>
<li>the F1PP000GN-SK from Belkin</li>
<li>Edge-Core's WM4201</li>
<li>SMC's WSKP100, coming in August,</li>
<li>and of course Netgear's SPH101, due this month.</li>
</ul>
<p>Handset prices will range from about $300 down to (more acceptable) $189. </p>
<p>You should keep in mind that some public hotspots require a web-based login first, which quite rules out the SPH101 for travel usage.</p>
<p>(via <a href="http://www.engadget.com/2006/07/20/skype-readies-four-wifi-phones-for-voiping-public/">Engadget</a>)</p>
