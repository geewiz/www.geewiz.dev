---
layout: post
title: "Another year of sequels"
microblog: false
guid: http://geewiz.micro.blog/2007/01/15/another-year-of.html
date: 2007-01-15T18:20:19+0200
type: post
url: /2007/01/15/another-year-of.html
---

<p>2007 will again be full of new movies. Well, sequels of new movies at least.</p>
<p>There's Spiderman 3, Pirates Of The Caribbean 3, Shrek 3 and many others. If you want a roundup, take a look at Andy's <a href="http://blog.bumsdiekuh.net/archives/42-Das-Jahr-der-Fortsetzungen-Part-II.html">summary of 2007's sequels</a> (german).</p>
<p>Well, I like great sequels better than crappy new flicks, but come on movie makers, there's that thing called creativity, you know?</p>
