---
layout: post
title: "Free book download: Mastering EJB 3.0"
microblog: false
guid: http://geewiz.micro.blog/2006/08/07/free-book-download.html
date: 2006-08-07T02:25:43+0200
type: post
url: /2006/08/07/free-book-download.html
---

<p>At the moment, I don't have the spare time to do Java development. But when I'll continue, I'll have some good documentation: Java community website <a href="http://www.theserverside.com">TheServerSide.com</a> offers a free PDF download of the book <a href="http://www.theserverside.com/news/thread.tss?thread_id=41363.">Mastering EJB</a> It's already in its forth edition and now also covers EJB 3.0:</p>
<blockquote class="posterous_medium_quote">
<p>Published in July 2006, the best selling book Mastering EJB is now in its fourth edition and has been updated for EJB 3.0. This edition features chapters on session beans and message-driven beans, EJB-Java EE integration and advanced persistence concepts. In-depth coverage of the Java Persistence API and using POJO entities with EJB is also included.</p>
</blockquote>
