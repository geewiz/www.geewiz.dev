---
layout: post
title: <% tp.file.title %>
date: <% tp.date.now("YYYY-MM-DD HH:mm") %>
description: 
image: 
category: 
tags: 
published: true
sitemap: true
---
<%*
const kebap = (str) => 
  str &&
  str
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map(x => x.toLowerCase())
    .join('-');
let title = kebap(tp.file.title);
let date = tp.file.creation_date("YYYY-MM-DD");
await tp.file.rename(`${date}-${title}`);
%>
