---
title: "About"
layout: page
date: 2024-10-05
permalink: /about
position: 2
---

**Hi, I'm Jochen Lillich.**

A few bits about myself:

1. I'm the founder and CTO of freistil IT, a small team of web operations experts running [freistilbox](https://www.freistilbox.com), a managed hosting platform for Drupal and WordPress.
1. I'm teaching software engineers DevOps and SRE skills as the [Monospace Mentor](https://monospacementor.com).
1. You can [watch me work](https://mspc.sh/live) on my live
   stream.
1. I love exchanging thoughts. Here's how to [reach me](/contact).
1. Mechanical keyboards are dope.
