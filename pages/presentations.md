---
title: "Talks & Presentations"
layout: page
date: 2022-02-03T15:24:09+0200
permalink: /presentations
position: 4
---

I have more than two decades of experience in public speaking about technology and IT leadership topics.

My first presentation, held at LinuxTag Kaiserslautern in 1999, was "Dynamic Web Pages with PHP 3.0". Since then, I got to speak at conferences such as DrupalCon Baltimore, DevOps Days London, Open Source Datacenter Conference Berlin and many other meetups and international events. As Track Chair, I also helped curating the DevOps track for DrupalCon Europe 2013 in Prague.

## Slide decks

You can find the slides to my talks on [SpeakerDeck](https://speakerdeck.com/geewiz).

## Learning videos

- [DIRECT — The seven core topics of remote team communication](https://youtu.be/BC6vzRiOy-Q)

## Conference talks

Here's a small selection of talks that I gave over the years:

- ["Under Pressure — How to build up resilience and keep burnout at bay"](https://youtu.be/lgNd7GE1eMs), The Live Coders Conference 2020, with a Twitch live audience of more than 13,000 viewers.
- ["Mono-repo vs Multi-repo -- which approach is superior?"](https://youtu.be/oULBMGbltEc), DrupalCamp Scotland 2019
- ["Monitoring with Sensu — it’s the sensuble thing to do"](https://www.youtube.com/watch?v=PA7tbjOrNTM), Open Source Monitoring Conference 2017
- ["Lean Web Operations — Planning for the unpredictable"](https://www.youtube.com/watch?v=i8pagLiTWTk), DrupalCon Vienna 2017
- ["Docker Orchestration with Kontena"](https://youtu.be/B6O04boY3js?list=LLREWiBdak-OhVvMsfJjCJJg), DrupalCon Baltimore 2017
- ["MonitoringLove with Sensu"](https://www.youtube.com/watch?v=KRkeqjcp4Es), Open Source Monitoring Conference 2014
- ["Getting the Most Out Of Varnish"](https://www.youtube.com/watch?v=u0G6PUe-n30), DrupalCamp Galway 2014
- ["Dynamic Infrastructure Orchestration with Chef"](https://www.youtube.com/watch?v=T0alwRerZUA), Open Source Datacentre Conference 2014
- ["Operations with Kanban"](https://www.youtube.com/watch?v=VgkRaMqeV5s), Open Source Datacentre Conference 2012
- ["Systems Automation with Chef"](https://www.youtube.com/watch?v=COBtNct93m8), Open Source Datacentre Conference 2011
