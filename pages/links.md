---
title: "Links"
layout: page
date: 2022-02-03T15:24:09+0200
permalink: /links
position: 3
---

Here are other places on the web where you can find my stuff.

## Knowlege Base

I'm publishing [my collection of notes](/notes/) via [Obsidian 
Publish](https://obsidian.md/publish) so I can reference and share them with 
other people.

## Live Stream

On my live coding channel [Full Stack Live](https://www.twitch.tv/fullstacklive), I'm doing live broadcasts of my DevOps work and courses I'm doing via my learning platform [Opsitive](https://www.opsitive.com). Join me when I'm online, and let's have a chat!

You can find recordings of my sessions, as well as other video content, on my [Youtube channel](https://www.youtube.com/@opsitive). Go ahead and subscribe to get notified of new uploads!
