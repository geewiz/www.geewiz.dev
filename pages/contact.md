---
title: "Contact"
layout: page
date: 2024-10-05
permalink: /contact
position: 5
---

Here's how you can reach me:

- To send me a message, email me at [hello@geewiz.dev](mailto:hello@geewiz.dev)!
- To get updates on what I'm up to, follow me on
  [Mastodon](https://mstdn.social/@geewiz)!
